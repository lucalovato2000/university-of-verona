/// @file semaphore.h
/// @brief Contiene la definizioni di variabili e funzioni
///         specifiche per la gestione dei semaphores.

#pragma once
#include <sys/stat.h>
#include <sys/sem.h>

typedef int semSet;
typedef unsigned short semNum;
typedef unsigned short semVal;

typedef union {
    int val;
    struct semid_ds *buf;
    semVal *array;
} semun;

typedef struct {
    semSet set;
    semNum num;
} Semaphore;

semSet semCreateSet(key_t, int);
void semInit(semSet, semVal *);
void semOperation(int, unsigned short, short);
void printSemaphoresValue(int);
void print3SemaphoresValue(int);