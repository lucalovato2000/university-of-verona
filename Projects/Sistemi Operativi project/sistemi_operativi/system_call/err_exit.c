/// @file err_exit.c
/// @brief Contiene l'implementazione della funzione di stampa degli errori.

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include "err_exit.h"

void ErrExit(const char *msg){
    perror(msg);
    exit(1);
}

void pErrExit(const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);
    printf(": %s\n", strerror(errno));
    exit(1);
}