/// @file shared_memory.c
/// @brief Contiene l'implementazione delle funzioni
///         specifiche per la gestione della shared_memory.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/shm.h>
#include "defines.h"
#include "err_exit.h"
#include "shared_memory.h"

// Get, or create, a shared memory segment
int shmCreate(key_t shmKey, int size) {
    int shmid = shmget(shmKey, size, IPC_CREAT | S_IRUSR | S_IWUSR);
    if (shmid FAILS) {
        ErrExit("Unable to allocate shared memory");
    }
    return shmid;
}

// Attach the shared memory
void *shmAttach(int shmid, int shmflg) {
    void *ptr = shmat(shmid, 0, shmflg);
    if ((long)ptr FAILS) {
        pErrExit("Unable to attach to shared memory %d", shmid);
    }
    return ptr;
}

// Detach the shared memory segment
void shmDetach(void *ptr) {
    if (shmdt(ptr) FAILS) {
        ErrExit("Unable to detach shared memory");
    }
}

// Remove the shared memory segment
void removeSH(int shmid) {
    if (shmctl(shmid, IPC_RMID, NULL) FAILS)
        pErrExit("Unable to remove shared memory: %d\n", shmid);
}

// Print the shared memory on terminal
void printOnTerminalSH(sender_traffic_struct *y, int position) {
    printf("*******************SH******************* \n");

    printf(
            "%d, %s, %d, %d, %s, %s\n", 
            y[position].id, 
            y[position].message, 
            y[position].id_sender, 
            y[position].id_receiver, 
            y[position].timeArrival, 
            y[position].timeDeparture
        );
    printf("*******************SH******************* \n");

}