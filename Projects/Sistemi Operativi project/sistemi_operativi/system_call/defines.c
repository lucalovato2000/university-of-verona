/// @file defines.c
/// @brief Contiene l'implementazione delle funzioni
///        specifiche del progetto.

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <signal.h>
#include "defines.h" 
#include "err_exit.h"

char nextfield = ';', nextline = '\n';

// Get the PID of processes in writed fileD 
pid_list getPIDFromFile(int fileD, int totalCharInFile, int offset) {
    int ncycle = 0, i = 0, j = 0, fieldP1 = 0, fieldP2 = 0, fieldP3 = 0;
    char c, field[10];
    pid_list pidSender; 

    lseek(fileD, offset + 1, SEEK_SET);
    totalCharInFile -= (offset + 1);

    while (i < totalCharInFile) {
        while (1) {
            read(fileD, &c, sizeof(char));
            
            if (c == ';' || c == '\n'){
                break;
            } 

            field[j] = c;

            j++;
        }

        field[j + 1] = '\0';
        ncycle++;

        if (ncycle == 2) {
            fieldP1 = atoi(field);
            pidSender.pid1 = fieldP1;
        } else if(ncycle == 4) {
            fieldP2 = atoi(field);
            pidSender.pid2 = fieldP2;
        } else if(ncycle == 6) {
            fieldP3 = atoi(field);
            pidSender.pid3 = fieldP3;
        }
        
        j = 0;
        i++;

        for (int k = 0; k< 10; k++){
            field[k] = '\0';
        }
    }
    return pidSender;
}

// Write in fileD the characters into charArr
void writeFile(int fileS, int fileD, int counter, char * charArr) {

    for (int i = counter; i >= 0; i--) {
        char ch = charArr[i];

        if (counter > 2) {
            // write the char
            if (write(fileD, &ch, sizeof(char)) != sizeof(char)) {
                ErrExit("Write failed\n");
            }
        }
    }
}

// Clean charArr for reading the next line
void cleanArray(char * charArr, int counter) {

    for (int i = 0; i <= counter; i++) {
        charArr[i] = '\0';
    }
}

// Error open file without create
void errorOpenWithoutCreate(int file) {
    if (file FAILS) {
        ErrExit("File opening:\t\t [FAIL]\n");
    } else {
        printf("File opening:\t\t [%sOK%s]\n", GREEN, RESET);
    }
}

// Check that the file has been opened correctly and output the result
void errorOpen(int file) {
    if (file FAILS) {
        ErrExit("File creation:\t\t [FAIL]\n");
    } else {
        printf("File creation:\t\t [%sOK%s]\n", GREEN, RESET);
    }
}

// Write the header into the file F2.csv
void headerOnFile(int f1, int f2, int total_read) {
    off_t currentOffset = lseek(f1, 0, SEEK_SET);
    char c;

    while (currentOffset <= total_read) {
        ssize_t bR = read(f1, &c, sizeof(char));
        if (bR == sizeof(char)) {
            if (write(f2, &c, sizeof(char)) != sizeof(char)) {
                ErrExit("Write failed\n");
            }
        }
        currentOffset++;
    }
}

// Search and replace the timeDestruction in fileD
void searchAndReplaceOnFile(int fileD, int totalFieldInFile, char * search, char * replace) {
    int j = 0, foundIt = 0;
    char c, field[50];
    lseek(fileD, 0, SEEK_SET);

    for (int i = 0; i < totalFieldInFile; i++) {
        while (j < totalFieldInFile) {
            if (read(fileD, &c, sizeof(char)) == sizeof(char)) {
                if (c == ';' || c == '\n') {
                    break;
                } 
                else {
                    field[j] = c;
                }   
            }
            else {
                printf("Error occured!\n");
                break;
            }
            j++; // lenght of the field     
        }

        if (strcmp(field, search) == 0) {
            foundIt = 1;
        } 

        if (foundIt == 1) {
            if (strcmp(field, "HH:MM:SS") == 0) {
                lseek(fileD, - (j + 1), SEEK_CUR);
                
                if (write(fileD, replace, strlen(replace)) != strlen(replace)) {
                    printf("I've write wrong!\n");
                }
                foundIt = 0;
            }
        }

        j = 0;

        // Clear the filed buffer
        for (int k = 0; k < 50; k++) {
            field[k] = '\0';
        }
    }
}

// Calculate the number of field in fileD
int numberOfFieldInFile(int fileD, int totalCharInFile) {
    int numberOfFieldInFile = 0;
    char c;
    lseek(fileD, 0, SEEK_SET); 
    for (int i = 0; i < totalCharInFile; i++) {
        if (read(fileD, &c, sizeof(char)) == sizeof(char)) {
            if (c == ';') {
                numberOfFieldInFile++;
            }
        }
        else {
            printf("Error occured!\n");
            break;
        }
    }
    return numberOfFieldInFile;
}

// Count the total chat into a file
int totalCharInFile(int file, char *buffer, int nr, int tot){
    while ((nr = read(file, buffer + tot, SIZE - tot)) > 0) {
        tot += nr;
    }
    return ((nr < 0) ? nr : tot);
}

// Obtain the char in the field of the line
const char* getfield(char* line, int num) {
    
    const char* tok;

    for (tok = strtok(line, ";"); tok && *tok; tok = strtok(NULL, ";\n")) {
        if (!--num) {
            return tok;
        }
    }
    return NULL;
}

// Count the number of rows in the file
int numberOfRows(int file, int totalchar){
    int nRows = 0;
    char c;
    off_t currentOffset = lseek(file, 0, SEEK_SET);

    while (currentOffset <= totalchar) {
        read(file, &c, sizeof(char));
        
        if (c == nextline) {
            nRows++;
        }
        currentOffset++;
    }
    return nRows;
}

// Remove char from a string
void removeChar(char *str, char garbage) {
    
    char *src, *dst;

    for (src = dst = str; *src != '\0'; src++) {
        *dst = *src;
        if (*dst != garbage) {
            dst++;
        }
    }
    *dst = '\0';
}

// Print the struct in the terminal
void printSenderTraffic(sender_traffic_struct *y, int position) {
    printf(
            "%d, %s, %d, %d, %s, %s\n", 
            y[position].id, 
            y[position].message, 
            y[position].id_sender, 
            y[position].id_receiver, 
            y[position].timeArrival, 
            y[position].timeDeparture
        );
}

// Add '0' to input value
char itoa(int x){
    return x += '0';
}

// Add '0' to input value
char itoaKey(key_t x){
    return x += '0';
}

// Print the content of file
void printStruct(int file, sender_traffic_struct *y, int position) {
    int nid = y[position].id; 
    char fd1 = itoa(nid);   // id

    int lmessage = strlen(y[position].message); 
    
    char nsender = y[position].id_sender;
    char fd3 = itoa(nsender);   // id_sender

    char nreceiver = y[position].id_receiver;
    char fd4 = itoa(nreceiver); // id_receiver

    int lTimeArrival = strlen(y[position].timeArrival); 
    int lTimeDeparture = strlen(y[position].timeDeparture); 

    write(file, &fd1, sizeof(char)); 
    write(file, &nextfield, sizeof(char));

    write(file, &y[position].message, lmessage);
    write(file, &nextfield, sizeof(char));

    write(file, &fd3, sizeof(char));
    write(file, &nextfield, sizeof(char));

    write(file, &fd4, sizeof(char));
    write(file, &nextfield, sizeof(char));

    write(file, &y[position].timeArrival, lTimeArrival);
    write(file, &nextfield, sizeof(char));

    write(file, &y[position].timeDeparture, lTimeDeparture);
    write(file, &nextfield, sizeof(char));

    write(file, &nextline, sizeof(char));   // nextline    
}

// Print a queue struct in file
void printStructQueue(int file, sender_traffic_queue_struct * y, int position) {
    int nid = y[position].id; 
    char fd1 = itoa(nid);   // id

    int lmessage = strlen(y[position].message); 
    
    char nsender = y[position].id_sender;
    char fd3 = itoa(nsender);   // id_sender

    char nreceiver = y[position].id_receiver;
    char fd4 = itoa(nreceiver); // id_receiver

    int lTimeArrival = strlen(y[position].timeArrival); 
    int lTimeDeparture = strlen(y[position].timeDeparture); 

    write(file, &fd1, sizeof(char)); 
    write(file, &nextfield, sizeof(char));

    write(file, &y[position].message, lmessage);
    write(file, &nextfield, sizeof(char));

    write(file, &fd3, sizeof(char));
    write(file, &nextfield, sizeof(char));

    write(file, &fd4, sizeof(char));
    write(file, &nextfield, sizeof(char));

    write(file, &y[position].timeArrival, lTimeArrival);
    write(file, &nextfield, sizeof(char));

    write(file, &y[position].timeDeparture, lTimeDeparture);
    write(file, &nextfield, sizeof(char));

    write(file, &nextline, sizeof(char));   // nextline    
}

// Print the content of file
void printIPCStruct(int file, ipc_struct * y, int position) {
    int strlenIPC, strlenCreator, strlenCreationTime, strlenDistructionTime, i = 0;
    char IDKey[50];

    strlenIPC = strlen(y[position].IPC);
    sprintf(IDKey, "%d", y[position].IDKey);

    strlenCreator = strlen(y[position].creator);
    strlenCreationTime = strlen(y[position].creationTime);
    strlenDistructionTime = strlen(y[position].distructionTime);

    while (y[position].creationTime[i] != '\0') {
        if (y[position].creationTime[i] == ' ') {
            y[position].creationTime[i] = '0';
        }
        i++;
    }

    write(file, y[position].IPC, strlenIPC); 
    write(file, &nextfield, sizeof(char)); // nextfield (;)

    write(file, &IDKey, strlen(IDKey)); 
    write(file, &nextfield, sizeof(char)); // nextfield (;)

    write(file, y[position].creator, strlenCreator); 
    write(file, &nextfield, sizeof(char)); // nextfield (;)

    write(file, y[position].creationTime, strlenCreationTime); 
    write(file, &nextfield, sizeof(char)); // nextfield (;)

    write(file, y[position].distructionTime, strlenDistructionTime); 
    write(file, &nextfield, sizeof(char)); // nextfield (;)

    write(file, &nextline, sizeof(char)); // nextline (\n)  
}

// Add delay to a time
struct tm *add_time(struct tm *y, int addition){       
    int min = 0;

    y->tm_hour += addition / 3600; 
    addition %= 3600; 

    y->tm_min += addition / 60 ; 
    addition %= 60; 
    
    y->tm_sec += addition; 
    
    if (y->tm_sec == 60){
        y->tm_min+=1;
        y->tm_sec=0;
    }

    if (y->tm_sec > 60){
        min = y->tm_sec / 60; 
        y->tm_min += min;
        y->tm_sec -= min*60;
    }
    
    return y;
}

// Add delay to a time
void getFormattedTimewithDelay(sender_traffic_struct * y, int addition, int index) {       
    char formattedTime[100];
    int min = 0, i = 0; 

    y[index].hoursDeparture += addition / 3600; 
    addition %= 3600; 

    y[index].minutesDeparture += addition / 60; 
    addition %= 60; 
    
    y[index].secondsDeparture += addition; 
    
    if (y[index].secondsDeparture == 60) {
        y[index].minutesDeparture += 1;
        y[index].secondsDeparture  = 0;
    }

    if (y[index].secondsDeparture > 60) {
        min = y[index].secondsDeparture / 60; 
        y[index].minutesDeparture += min;
        y[index].secondsDeparture -= min * 60;
    }

    sprintf(formattedTime, "%2d:%2d:%2d", y[index].hoursDeparture, y[index].minutesDeparture, y[index].secondsDeparture);

    while (formattedTime[i] != '\0') {
        if (formattedTime[i] == ' ') {
            formattedTime[i] = '0';
        }
        i++;
    }
    
    memcpy(y[index].timeDeparture, formattedTime, sizeof(y[index].timeDeparture));
}

// Add delay to a time
void getFormattedTimewithDelayMessageQueue(sender_traffic_queue_struct * y, int addition, int index) {       
    char formattedTime[200];

    int min = 0; 

    y[index].hoursDeparture += addition / 3600; 
    addition %= 3600; 

    y[index].minutesDeparture += addition / 60 ; 
    addition %= 60; 
    
    y[index].secondsDeparture += addition; 
    
    if (y[index].secondsDeparture == 60){
        y[index].minutesDeparture += 1;
        y[index].secondsDeparture  = 0;
    }

    if (y[index].secondsDeparture > 60){
        min = y[index].secondsDeparture / 60; 
        y[index].minutesDeparture += min;
        y[index].secondsDeparture -= min * 60;
    }

    sprintf(formattedTime, "%2d:%2d:%2d", y[index].hoursDeparture, y[index].minutesDeparture, y[index].secondsDeparture);

    int i = 0;
    while (formattedTime[i] != '\0') {
        if (formattedTime[i] == ' ') {
            formattedTime[i] = '0';
        }
        i++;
    }
    
    memcpy(y[index].timeDeparture, formattedTime, sizeof(y[index].timeDeparture));
}

// Clear the struct senderTraffic
void clearSenderTraffic(sender_traffic_struct *y, int max) {
    for (int i = 0; i < max; i++){
        y[i].id = '\0';
        y[i].id_receiver = '\0';
        y[i].id_sender = '\0';
        *y[i].message = '\0';
        *y[i].timeArrival = '\0';
        *y[i].timeDeparture = '\0';
        y[i].hoursArrival = '\0';
        y[i].minutesArrival = '\0';
        y[i].secondsArrival = '\0';
        y[i].hoursDeparture = '\0';
        y[i].minutesDeparture = '\0';
        y[i].secondsDeparture = '\0';
        y[i].delayS1 = '\0';
        y[i].delayS2 = '\0';
        y[i].delayS3 = '\0';
    }
}

// Clear the struct senderTraffic
void clearSenderTrafficQueue(sender_traffic_queue_struct *y, int max) {
    for (int i = 0; i < max; i++){
        y[i].id = '\0';
        y[i].id_receiver = '\0';
        y[i].id_sender = '\0';
        *y[i].message = '\0';
        *y[i].timeArrival = '\0';
        *y[i].timeDeparture = '\0';
        y[i].hoursArrival = '\0';
        y[i].minutesArrival = '\0';
        y[i].secondsArrival = '\0';
        y[i].hoursDeparture = '\0';
        y[i].minutesDeparture = '\0';
        y[i].secondsDeparture = '\0';
        y[i].delayS1 = '\0';
        y[i].delayS2 = '\0';
        y[i].delayS3 = '\0';
    }
}

// Print the senderIstruction in the terminal
void printSenderInstruction(sender_struct * y, int position){
    printf(
        "%d, %s, %c, %c, %d, %d, %d, %s\n", 
        y[position].id, 
        y[position].message, 
        y[position].id_sender, 
        y[position].id_receiver, 
        y[position].delayS1, 
        y[position].delayS2,
        y[position].delayS3,
        y[position].type
    );
}

// Print the ipcStruct in the terminal
void printIPC(ipc_struct * y, int position) {
    printf(
        "%s, %d, %s, %s, %s\n", 
        y[position].IPC,
        y[position].IDKey,
        y[position].creator,
        y[position].creationTime,
        y[position].distructionTime
    );
}

// Count the digit of n
int countDigit(int n) {
    int count = 0;

    while (n != 0) {
        n /= 10;
        ++count;
    }

    return count;
}

// Update the ipc_struct writing in the fields
void updateIPC(ipc_struct *y, int i, char *name, int IDKey, char *creator, char *creationTime, char *destructionTime){
    y[i].IPC = name;
    y[i].IDKey = IDKey;
    y[i].creator = creator;
    strcpy(y[i].creationTime, creationTime);
    strcpy(y[i].distructionTime, destructionTime);
}

// Set the localtime for getting the creationTime & destrcutionTime
void timef(char *timeIPC) {
    time_t clock = 0;
    struct tm *time_clock = 0;
    char formattedTime[10];
    int i = 0;

    time(&clock);
    time_clock = localtime(&clock);
    sprintf(formattedTime, "%2d:%2d:%2d", time_clock->tm_hour, time_clock->tm_min, time_clock->tm_sec);

    while (formattedTime[i] != '\0') {
        if (formattedTime[i] == ' ') {
            formattedTime[i] = '0';
        }
        i++;
    }

    memcpy(timeIPC, formattedTime, sizeof(formattedTime));
}

// Obtatin the timeCreation and formatting the struct time_clock 
void timeDeparture(time_t clock, struct tm *time_clock, char *timeIPC) {
    char formattedTime[20];
    int i = 0;

    time(&clock);
    time_clock = localtime(&clock);
    sprintf(formattedTime, "%2d:%2d:%2d", time_clock->tm_hour, time_clock->tm_min, time_clock->tm_sec);

    while (formattedTime[i] != '\0') {
        if (formattedTime[i] == ' ') {
            formattedTime[i] = '0';
        }
        i++;
    }

    memcpy(timeIPC, formattedTime, sizeof(formattedTime));
}

// Obtatin the timeTermination and formatting the struct time_clock 
void timeTermination(time_t clock, struct tm *time_clock, char *timeIPC) {
    char formattedTime[20];
    int i = 0;

    time(&clock);
    time_clock = localtime(&clock);
    sprintf(formattedTime, "%2d:%2d:%2d", time_clock->tm_hour, time_clock->tm_min, time_clock->tm_sec);

    while (formattedTime[i] != '\0') {
        if (formattedTime[i] == ' ') {
            formattedTime[i] = '0';
        }
        i++;
    }
    
    memcpy(timeIPC, formattedTime, sizeof(formattedTime));
}

void copyToSenderTrafficSH(sender_traffic_struct *paste, sender_traffic_struct *copy, int i, int nm, int nmpipe){
    paste[i+nmpipe].id = copy[i-nm].id;
    strcpy(paste[i+nmpipe].message, copy[i-nm].message);
    paste[i+nmpipe].id_sender = copy[i-nm].id_sender;
    paste[i+nmpipe].id_receiver = copy[i-nm].id_receiver;
    strcpy(paste[i+nmpipe].timeArrival, copy[i-nm].timeArrival);
    strcpy(paste[i+nmpipe].timeDeparture, copy[i-nm].timeDeparture);
    paste[i+nmpipe].hoursArrival = copy[i-nm].hoursArrival;
    paste[i+nmpipe].minutesArrival = copy[i-nm].minutesArrival;
    paste[i+nmpipe].secondsArrival = copy[i-nm].secondsArrival;
    paste[i+nmpipe].hoursDeparture = copy[i-nm].hoursDeparture;
    paste[i+nmpipe].minutesDeparture = copy[i-nm].minutesDeparture;
    paste[i+nmpipe].secondsDeparture = copy[i-nm].secondsDeparture;
    paste[i+nmpipe].delayS1 = copy[i-nm].delayS1;
    paste[i+nmpipe].delayS2 = copy[i-nm].delayS2;
    paste[i+nmpipe].delayS3 = copy[i-nm].delayS3;    
}