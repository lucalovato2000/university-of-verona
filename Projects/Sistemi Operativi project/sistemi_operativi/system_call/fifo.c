/// @file fifo.c
/// @brief Contiene l'implementazione delle funzioni
///         specifiche per la gestione delle FIFO.

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "defines.h"
#include "err_exit.h"
#include "fifo.h"

// Create a FIFO
void makeFIFO(const char *pathname) {
    if (mkfifo(pathname, O_CREAT | S_IRUSR | S_IWUSR) FAILS) {
        pErrExit("Unable to create FIFO '%s'\n", pathname);
    }
}

// Remove FIFO
void removeFIFO(const char *pathname){
    if (unlink(pathname) != 0) {
        ErrExit("FIFO unlink failed");
    }
}