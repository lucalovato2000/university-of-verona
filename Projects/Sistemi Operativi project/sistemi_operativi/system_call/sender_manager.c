/// @file sender_manager.c
/// @brief Contiene l'implementazione del sender_manager.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <signal.h>
#include <ctype.h>
#include "defines.h"
#include "err_exit.h"
#include "semaphore.h"
#include "shared_memory.h"
#include "msg_queue.h"
#include "pipe.h"
#include "fifo.h"

#define MAX_COL 8

char buffer[SIZE + 1];
unsigned short idSemaphore = 0;

pid_t sender1, sender2, sender3;
key_t key_queue = 01110001, key_sh = 01101101, key_sm = 01110011, key_sm2 = 11110011;
int sender1Pid = 0, sender2Pid = 0, sender3Pid = 0, idShSender = 0, idQueue = 0, stopwatch = 0, addDelay = 0,
    *pointerToAddDelay = &addDelay, secondTime = 0, secondTime300 = 0, *pointerToSecondTime = &secondTime, *pointerToSecondTime300 = &secondTime300;

// Manage the signal SIGUSER1, SIGCONT and SIGTERM
void sigSHandler(int sig) {
    if (sig == SIGUSR1) {
        printf("\x1b[30m\x1b[35m<Sender Manager>\t Received signal IncreaseDelay\x1b[0m\n");

        *pointerToAddDelay = 5;

        printf("<Sender Manager>\t I'm starting sleeping for the added delay\n");
        sleep(*pointerToAddDelay);
        printf("<Sender Manager>\t I'm finished sleeping for the added delay\n");
    }

    if (sig == SIGCONT) {
        printf("\x1b[36m<Sender Manager>\t Received signal SIGCONT\x1b[0m\n");
    }
 
    if (sig == SIGTERM) {
        printf("<Sender Manager>\t Signal terminates the target process\n");  
        exit(0); // terminate the process
    } 
}

// Manage the signal SIGUSER2 for the action "RemoveMsg"
void sigSHandler300(int sig, sender_struct * toModify, int numberOfElements, int target) {
    if (sig == SIGHUP) {
        printf("\x1b[36m<Sender Manager>\t Received signal SendMsg\x1b[0m\n");
        *pointerToSecondTime300 = 1;
    }

    if (*pointerToSecondTime300 == 0) {
        for (int i = 0; i < numberOfElements; i++) {
            switch (target) {
                case 1:
                    toModify[i].delayS1 = toModify[i].delayS1;
                    break;
                case 2:
                    toModify[i].delayS2 = toModify[i].delayS2;
                    break;
                case 3:
                    toModify[i].delayS3 = toModify[i].delayS3;
                    break;
                default:
                    break;
            }
        }
    }
    else {
        for (int i = 0; i < numberOfElements; i++) {
            switch (target) {
                case 1:
                    toModify[i].delayS1 = 0;
                    break;
                case 2:
                    toModify[i].delayS2 = 0;
                    break;
                case 3:
                    toModify[i].delayS3 = 0;
                    break;
                default:
                    break;
            }
        }
    }
}

// Manage the signal SIGHUP for the action "SendMsg"
int sigSHandler200(int sig, int numberOfMessage) {   
    int zero = 0;

    if (sig == SIGUSR2) {
        printf("\x1b[30m\x1b[35m<Sender Manager>\t Received signal RemoveMsg\x1b[0m\n");
        *pointerToSecondTime = 1;
        return zero;
    }
    if (*pointerToSecondTime == 0) {
        return numberOfMessage;
    }
    else {
        return zero;
    }
}

int main(int argc, char * argv[]) {
    char *file0, *file1, *file2, *file3, *file8, *file10, bodyF8[100], *bodyF10, nextline = '\n', buffer_read[SIZE],
        *pathname = "OutputFiles/my_fifo.txt", *SH = "SH", *Q = "Q", *FIFO = "FIFO", *SM = "SM",
        timeIPCSH[10], timeIPCQ[10], timeIPCP1[10], timeIPCDesP1[10], timeIPCP2[10], timeIPCDesP2[10], timeIPCPSS1[10], 
        timeIPCDesPSS1[10], timeIPCPSS2[10], timeIPCP12[10], timeIPCP13[10], timeIPCPToS2[10], timeIPCPToS3[10], timeIPCDesPSS2[10], 
        timeIPCDesP12[10], timeIPCDesP13[10], timeIPCDesPToS2[10], timeIPCDesPToS3[10], timeIPCSem[10], timeIPCSem2[10];

    int PIPE1[2], PIPE2[2], pipeForSizeS2[2], PIPE1_2[2], PIPE1_3[2], pipeForSizeS1[2], pipeForSendPidToS2[2], pipeForSendPidToS3[2], values[3],
        filesHeader, senderHeader, file0D, file1D, file2D, file3D, file8D, FIFO_D, file10D, numberOfIPC = 0, tot = 0, nr = 0, 
        status, filesTotalRead, max_char, rows = 0, numberOfMessageToSendToS2 = 0, numberOfMessageToSendToS3 = 0;
    
    ssize_t wB, wB2, wB_sender_S2, rB, rB2, wB2_sender_S2,wF, sizeOfSenderTrafficStruct, sizeStructInstructions, sizeStructInstructions2,
        rB_2, rB_3, sizesender2Pid, sizesender3Pid, wP1, rB1, wP3, rB3, sizeStruct, sizePipeS1, sizePipeS2, wB_2, wB_3, mSize, shSize;

    ipc_struct IPCSender[10];
    semun argSender, arg0, arg1;

    time_t clock;

    // Set of signals
    sigset_t senderSet, senderPrevSet;

    // Initialize senderSet to contain all signals
    sigfillset(&senderSet);

    // Remove signals 
    sigdelset(&senderSet, SIGCONT);
    sigdelset(&senderSet, SIGUSR1);
    sigdelset(&senderSet, SIGUSR2);
    sigdelset(&senderSet, SIGHUP);
    sigdelset(&senderSet, SIGTERM);

    // Blocking the remaning signals
    sigprocmask(SIG_SETMASK, &senderSet, &senderPrevSet);

    file0 = "InputFiles/F0.csv";
    file1 = "OutputFiles/F1.csv";
    file2 = "OutputFiles/F2.csv";
    file3 = "OutputFiles/F3.csv"; 
    file8 = "OutputFiles/F8.csv";
    file10 = "OutputFiles/F10.csv";

    // Create the PIPEs and set the creation times
    createPipe(PIPE1);
    timef(timeIPCP1);

    createPipe(PIPE2);
    timef(timeIPCP2);

    createPipe(pipeForSizeS1);
    timef(timeIPCPSS1);

    createPipe(pipeForSizeS2);
    timef(timeIPCPSS2);

    createPipe(PIPE1_2);
    timef(timeIPCP12);

    createPipe(PIPE1_3);
    timef(timeIPCP13);

    createPipe(pipeForSendPidToS2);
    timef(timeIPCPToS2);

    createPipe(pipeForSendPidToS3);
    timef(timeIPCPToS3);

    // Create and set the creation time of semaphore set
    semSet semId;
    semId = semCreateSet(key_sm, 6);
    timef(timeIPCSem);

    // Initialize the semaphore set with semctl
    semVal semInitVal[] = {1, 0, 0, 0, 0, 0};
    semInit(semId, semInitVal);

    // Create and set the creation time of second semaphore set
    semSet semId2;
    semId2 = semCreateSet(key_sm2, 3);
    timef(timeIPCSem2);

    // Initialize the second semaphore set with semctl
    semVal semInitVal2[] = {1, 0, 0};
    semInit(semId2, semInitVal2);

    // Create and set the creation time of the shared memory
    shSize = sizeof(sender_traffic_struct);
    idShSender = shmCreate(key_sh, shSize);
    timef(timeIPCSH);
    
    // Read F0.csv and get max_char, total rows and current offset
    file0D = open(file0, O_RDWR);  
    errorOpenWithoutCreate(file0D);
    max_char = totalCharInFile(file0D, buffer_read, nr, tot);
    
    // Calculate the number of rows in F0.csv
    rows = numberOfRows(file0D, max_char);

    if (close(file0D) FAILS) {
        ErrExit("close failed");
    }
    
    // Initialize the structs for the traffic in sender_manager
    sender_struct sender_instructions[rows], sender_instructions_two[rows], sender_instructions_three[rows];
    sender_traffic_struct sender_traffic[rows], sender_traffic_two[rows], sender_traffic_three[rows];
    sender_traffic_queue_struct sender_traffic_queue[rows];   

    // Sleep to wait for the creation of the message queue, get the message queue
    sleep(1);
    idQueue = msgget(key_queue, S_IRUSR | S_IWUSR);

    if (idQueue FAILS) {
        ErrExit("<Sender Manager>\t msgget failed\n");
    }
    timef(timeIPCQ);

    // Open the files that contains the header to write in the other files
    filesHeader = open("InputFiles/filesHeader.csv", O_RDWR);
    errorOpenWithoutCreate(filesHeader);

    senderHeader = open("InputFiles/senderHeader.csv", O_RDWR);
    errorOpenWithoutCreate(senderHeader);

    file8D = open(file8, O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
    errorOpen(file8D);

    // Read and count the size of filesHeader
    filesTotalRead = totalCharInFile(filesHeader, buffer, nr, tot);

    /////////////////////////////////////////////////////////////////////// CREATE THE FILE F10.CSV

    file10D = open(file10, O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
    errorOpen(file10D);
    
    // Write the header into F10.csv
    bodyF10 = "IPC;IDKey;Creator;CreationTime;DestructionTime\n\0";

    for (int i = 0; bodyF10[i] != '\0'; i++) {
        if (write(file10D, &bodyF10[i], sizeof(char)) != sizeof(char)) {
            ErrExit("<Sender Manager>\t Write failed \n");
        }
    }

    //================================================================================
    // SENDER 1
    //================================================================================

    sender1 = fork(); // Generate a subprocess, make a fork for creating child process

    if (sender1 FAILS) {
        ErrExit("<Sender Manager 1>\t Fork failed\n");
    }
    if (sender1 == 0) {
        closeReadEndPipe(PIPE1);
        closeReadEndPipe(pipeForSizeS1);
        closeReadEndPipe(PIPE1_2);
        closeWriteEndPipe(pipeForSendPidToS2);

        int index = 0, maxDelayS1 = 0, stopwatch = 0, counter = 1, col = 9, sizeMessage, ctMessage, pidS2;
        char *tmp, *lettura, out[20];
        struct tm *info, *infoDelay1;
        time_t start, end;
        
        idSemaphore = 0;

        // Get the pid of the process
        sender1Pid = getpid();

        // Open F1 and write the header with the next line
        file1D = open(file1, O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
        errorOpen(file1D);
        headerOnFile(filesHeader, file1D, filesTotalRead);
        
        if (write(file1D, &nextline, sizeof(char)) != sizeof(char)) {
            ErrExit("<Sender Manager 1>\t Write failed\n");
        } 

        // Read F0.csv and get max_char, total rows and current Offset
        file0D = open(file0, O_RDWR);  
        errorOpenWithoutCreate(file0D);
    
        // Cycle for the lines (Read all the messages from file and populate the struct)
        while (index < rows) {
            // Cycle for the columns
            for (counter = 1; counter <= MAX_COL; counter++) {
                tmp = strdup(buffer_read);
                lettura = strdup(getfield(tmp, col));

                if (counter == 1) {
                    sender_instructions[index].id = atoi(lettura);
                    sender_traffic[index].id = sender_instructions[index].id;
                }   
                if (counter == 2) {
                    time(&clock);
                    info = localtime(&clock);

                    sprintf(out, "%2d:%2d:%2d", info->tm_hour, info->tm_min, info->tm_sec);
                    sender_traffic[index].hoursArrival = info->tm_hour;
                    sender_traffic[index].minutesArrival = info->tm_min;
                    sender_traffic[index].secondsArrival = info->tm_sec;

                    sizeMessage = sizeof(out);

                    ctMessage = 0;

                    while (ctMessage < sizeMessage) {
                        if (out[ctMessage] == ' ') {
                            sender_traffic[index].timeArrival[ctMessage] = '0';
                        }
                        else {  
                            sender_traffic[index].timeArrival[ctMessage] = out[ctMessage];
                        }
                        ctMessage++;
                    }

                    sender_traffic[index].timeArrival[ctMessage] = '\0';

                    sprintf(sender_instructions[index].message, "%s", lettura);

                    sizeMessage = sizeof(sender_instructions[index].message);
                    
                    ctMessage = 0;

                    while (ctMessage < sizeMessage) {
                        sender_traffic[index].message[ctMessage] = sender_instructions[index].message[ctMessage];
                        ctMessage++;
                    }

                    sender_traffic[index].message[ctMessage] = '\0';
                }
                if (counter == 3) {
                    removeChar(lettura, 'S');
                    sender_instructions[index].id_sender = atoi(lettura);
                    sender_traffic[index].id_sender = sender_instructions[index].id_sender;
                }
                if (counter == 4) {
                    removeChar(lettura, 'R');
                    sender_instructions[index].id_receiver = atoi(lettura);
                    sender_traffic[index].id_receiver = sender_instructions[index].id_receiver;
                }
                if (counter == 5) {
                    sender_instructions[index].delayS1 = (strcmp(lettura, "-") != 0) ? atoi(lettura) : 0;

                    if (sender_instructions[index].delayS1 > maxDelayS1) {
                        maxDelayS1 = sender_instructions[index].delayS1;
                    }
                    
                    infoDelay1 = add_time(info, sender_instructions[index].delayS1);

                    sprintf(sender_traffic[index].timeDeparture , "%2d:%2d:%2d", infoDelay1->tm_hour, infoDelay1->tm_min, infoDelay1->tm_sec);
                    sender_traffic[index].hoursDeparture = infoDelay1->tm_hour;
                    sender_traffic[index].minutesDeparture = infoDelay1->tm_min;
                    sender_traffic[index].secondsDeparture = infoDelay1->tm_sec;

                    sizeMessage = sizeof(sender_traffic[index].timeDeparture);

                    ctMessage = 0;

                    while (ctMessage < sizeMessage){ 
                        if (sender_traffic[index].timeDeparture[ctMessage] == ' ') {
                            sender_traffic[index].timeDeparture[ctMessage] = '0';
                        } 
                        else {  
                            sender_traffic[index].timeDeparture[ctMessage] = sender_traffic[index].timeDeparture[ctMessage];
                        }
                        ctMessage++;
                    }

                    sender_traffic[index].timeDeparture[ctMessage] = '\0';
                }
                if (counter == 6) {
                    sender_instructions[index].delayS2 = (strcmp(lettura, "-") != 0) ? atoi(lettura) : 0;
                }
                if (counter == 7) {
                    sender_instructions[index].delayS3 = (strcmp(lettura, "-") != 0) ? atoi(lettura) : 0;
                }
                if (counter == 8) {
                    sprintf(sender_instructions[index].type, "%s", lettura);
                }
                col++; 
            } 
            index++;
        } // end lines while

        // Sending signal for stopping itself
        if (kill(sender1Pid, SIGSTOP) == -1) {
            ErrExit("<Sender Manager 1>\t SIGSTOP in S1 failed\n");
        }
        printf("<Sender Manager 1>\t Resume the execution\n");

        ///////////////////////////////////////////////////////////////// SIGNAL

        maxDelayS1++;

        /********* SIGUSR1 *********/
        if (signal(SIGUSR1, sigSHandler) == SIG_ERR) {
            ErrExit("<Sender Manager 1>\t change signal SIGUSR1 handler failed\n");
        }
        
        if (signal(SIGHUP, (void (*)(int))sigSHandler300) == SIG_ERR) {
            ErrExit("<Sender Manager 1>\t change signal SIGHUP handler failed\n");
        }

        /********* SIGCONT *********/
        if (signal(SIGCONT, sigSHandler) == SIG_ERR) {
            ErrExit("<Sender Manager 1>\t change signal SIGCONT handler failed\n");
        }

        /********* SIGUSR2 *********/
        if (signal(SIGUSR2, (void (*)(int))sigSHandler200) == SIG_ERR){
            ErrExit("<Sender Manager 1>\t change signalHandler200 failed\n");
        }
        
        printf("<Sender Manager 1>\t I'm ready to catch for a signal from someone...  \n");

        pause();  

        sigSHandler300(FAKE_SIGNAL, sender_instructions, index, 1);

        index = sigSHandler200(FAKE_SIGNAL, index);
        printf("<Sender Manager 1>\t The new number of message to send to SENDER2 is %d\n", index);

        if (index == 0) {
            maxDelayS1 = 0;
            numberOfMessageToSendToS2 = 0;
        }

        while (stopwatch < maxDelayS1) {
            time(&start);

            do {
                time(&end); 
            }
            while (difftime(end, start) < 1);

            for (int i = 0; i < index; i++) {

                // Copy from timeDeparture into timeArrival
                memcpy(sender_traffic[i].timeArrival, sender_traffic[i].timeDeparture, sizeof(sender_traffic[i].timeArrival));

                sender_traffic[i].hoursArrival = sender_traffic[i].hoursDeparture;
                sender_traffic[i].minutesArrival = sender_traffic[i].minutesDeparture;
                sender_traffic[i].secondsArrival = sender_traffic[i].secondsDeparture;

                // Update new struct field for sending to R3 the delays value
                sender_traffic[i].delayS1 = sender_instructions[i].delayS1;
                sender_traffic[i].delayS2 = sender_instructions[i].delayS2;
                sender_traffic[i].delayS3 = sender_instructions[i].delayS3;

                if (sender_traffic[i].id_sender == 1 && strcmp(sender_instructions[i].type, SH) == 0 && stopwatch == sender_instructions[i].delayS1) {
                    printf("<Sender Manager 1>\t Sending to RM by Shared Memory\n");

                    // Added delay to the message that pass by shared memory
                    getFormattedTimewithDelay(sender_traffic, sender_instructions[i].delayS1 + *pointerToAddDelay, i);  // warning zone

                    // Attach to the shared memory SH
                    sender_traffic_struct *SHpointer = (sender_traffic_struct *)shmAttach(idShSender, 0);

                    // Copy the struct to the shared memory SH
                    memcpy(SHpointer, &sender_traffic[i], sizeof(sender_traffic_struct));

                    shmDetach(SHpointer);

                    printStruct(file1D, sender_traffic, i);
                }
                else if (sender_traffic[i].id_sender == 1 && strcmp(sender_instructions[i].type, Q) == 0 && stopwatch == sender_instructions[i].delayS1) {
                    printf("<Sender Manager 1>\t Sending to RM by Message Queue\n");

                    // Copy sender_traffic into sender_traffic_queue
                    copyToSenderTrafficQueue(sender_traffic_queue, sender_traffic, i);

                    // Added delay to the message that pass by message queue
                    getFormattedTimewithDelayMessageQueue(sender_traffic_queue, sender_instructions[i].delayS1 + *pointerToAddDelay, i);

                    // Size of the sender_traffic_queue_struct
                    mSize = sizeof(sender_traffic_queue_struct) - sizeof(long); 

                    if (sender_traffic_queue[i].id_receiver == 1) {
                        sender_traffic_queue[i].mtype = 1;
                    } else if (sender_traffic_queue[i].id_receiver == 2) {
                        sender_traffic_queue[i].mtype = 2;
                    } else {
                        sender_traffic_queue[i].mtype = 3;
                    }

                    // Sending the sender_traffic_queue by the queue Q
                    if (msgsnd(idQueue, &sender_traffic_queue[i], mSize, 0) FAILS) {
                        ErrExit("<Sender Manager 1>\t msgsnd failed\n");
                    }

                    printStructQueue(file1D, sender_traffic_queue, i);
                }
                else {
                    if (stopwatch == sender_instructions[i].delayS1) {
                        printf("<Sender Manager 1>\t Sending to S2 by PIPE1\n");

                        numberOfMessageToSendToS2++;

                        // Added delay to the message that pass by PIPE to S2
                        getFormattedTimewithDelay(sender_traffic, sender_instructions[i].delayS1 + *pointerToAddDelay, i); 

                        sizeStruct = sizeof(sender_traffic_struct);
                        wB = write(PIPE1[1], &sender_traffic[i], sizeStruct);
                        
                        if (wB != sizeStruct) {
                            ErrExit("<Sender Manager 1>\t PIPE1 write failed\n");
                        }

                        // Send the sender_instructions to S2
                        sizeStructInstructions = sizeof(sender_struct); 
                        wB_2 = write(PIPE1_2[1], &sender_instructions[i], sizeStructInstructions);

                        if (wB_2 != sizeStructInstructions) {
                            ErrExit("<Sender Manager 1>\t PIPE1_2 write failed\n");
                        }

                        printStruct(file1D, sender_traffic, i); 
                    }
                }
            }
            stopwatch++;
        }

        // Unlock the (i-1)-th semaphore.
        // Child is equal to 0, then the fourth child is unlocked
        semOperation(semId, idSemaphore, -1);
        semOperation(semId, idSemaphore + 1, 1);

        // Send the counter numberOfMessageToSendToS2 to S2
        sizePipeS1 = sizeof(numberOfMessageToSendToS2);
        wB2 = write(pipeForSizeS1[1], &numberOfMessageToSendToS2, sizePipeS1);  
    
        if (wB2 != sizePipeS1) {
            ErrExit("<Sender Manager 1>\t sizePipeS1 write failed\n");
        }
        
        if (close(file1D) FAILS) {
            ErrExit("<Sender Manager 1>\t F1.csv close failed\n");
        }       

        // Read the pid of sender2
        rB1 = read(pipeForSendPidToS2[0], &pidS2, sizeof(pidS2));

        if (rB1 <= 0) {
            ErrExit("<Sender Manager 1>\t Pipe pipeForSendPidToS2 read failed\n"); 
        }
        
        printf("<Sender Manager 1>\t Sending SIGCONT to S2\n");

        // Resume the process 2 of sender manager
        if (kill(pidS2, SIGCONT) == -1) {
            ErrExit("SIGCONT to S2 failed\n");
        }

        sleep(1);

        if (signal(SIGTERM, sigSHandler) == SIG_ERR) {
            ErrExit("<Sender Manager 1>\t Change signal handler failed\n");
        }
        pause();
    }

    //================================================================================
    // SENDER 2
    //================================================================================

    sender2 = fork();

    if (sender2 FAILS) {
        ErrExit("<Sender Manager 2>\t Fork failed\n");
    }
    if (sender2 == 0) {
        closeReadEndPipe(pipeForSendPidToS2);
        closeWriteEndPipe(pipeForSendPidToS3);
        closeWriteEndPipe(PIPE1);
        closeWriteEndPipe(PIPE1_2);
        closeWriteEndPipe(pipeForSizeS1);

        int stopwatch = 0, pidS3, numberOfMessageToSendToS2 = 0, maxDelayS2 = 0;
        time_t start, end;
        
        idSemaphore = 1;

        sender2Pid = getpid();

        // Write into pipeForSendPidToS2 the pid of sender2
        sizesender2Pid = sizeof(sender2Pid);
        wP1 = write(pipeForSendPidToS2[1], &sender2Pid, sizesender2Pid); 
    
        if (wP1 != sizesender2Pid) {
            ErrExit("<Sender Manager 2>\t sizesender2Pid write failed\n");
        }

        // Sending signal for stopping itself
        if (kill(sender2Pid, SIGSTOP) == -1) {
            ErrExit("<Sender Manager 2>\t SIGSTOP failed\n");
        }
        printf("<Sender Manager 2>\t Resume the execution\n");

        file2D = open(file2, O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
        errorOpen(file2D);
        headerOnFile(filesHeader, file2D, filesTotalRead);

        if (write(file2D, &nextline, sizeof(char)) != sizeof(char)) {
            ErrExit("<Sender Manager 2>\t F2.csv write failed \n");
        }

        // pipeForSizeS1 for reading the numberOfMessageToSendToS2
        rB2 = read(pipeForSizeS1[0], &numberOfMessageToSendToS2, sizeof(numberOfMessageToSendToS2));
        if (rB2 <= 0){
            ErrExit("<Sender Manager 2>\t pipeForSizeS1 read failed\n"); 
        }

        if (numberOfMessageToSendToS2 != 0) {
            // PIPE1_2 for reading the sender_instructions_two
            rB_2 = read(PIPE1_2[0], &sender_instructions_two, sizeof(sender_instructions_two));
            if (rB_2 <= 0) {
                ErrExit("<Sender Manager 2>\t PIPE1_2 read failed\n"); 
            }

            // PIPE1 for reading the sender_traffic_two
            rB = read(PIPE1[0], &sender_traffic_two, sizeof(sender_traffic_two));
            if (rB <= 0) {
                ErrExit("<Sender Manager 2>\t PIPE1 read failed\n"); 
            }

            for (int i = 0; i < numberOfMessageToSendToS2; i++) {
                if (sender_instructions_two[i].delayS2 > maxDelayS2) {
                    maxDelayS2 = sender_instructions_two[i].delayS2;
                }
            }
            maxDelayS2++;
        }

        if (signal(SIGHUP, (void (*)(int))sigSHandler300) == SIG_ERR) {
            ErrExit("<Sender Manager 2>\t change signal SIGHUP handler failed\n");
        }

        /********* SIGUSR1 *********/
        if (signal(SIGUSR1, sigSHandler) == SIG_ERR){ 
            ErrExit("<Sender Manager 2>\t change signal handler failed\n");
        }
        
        /********* SIGCONT *********/
        if (signal(SIGCONT, sigSHandler) == SIG_ERR) {
            ErrExit("<Sender Manager 2>\t change signal SIGCONT handler failed\n");
        }

        /********* SIGUSR2 *********/
        if (signal(SIGUSR2, (void (*)(int))sigSHandler200) == SIG_ERR){
            ErrExit("<Sender Manager 2>\t change signalHandler200 failed\n");
        }
        
        printf("<Sender Manager 2>\t I'm ready to catch for a signal from someone...  \n");

        pause();   

        sigSHandler300(FAKE_SIGNAL, sender_instructions_two, numberOfMessageToSendToS2, 2);
        
        numberOfMessageToSendToS2 = sigSHandler200(FAKE_SIGNAL, numberOfMessageToSendToS2);
        printf("<Sender Manager 2>\t The new number of message to send to SENDER3 is %d\n", numberOfMessageToSendToS2);

        if (numberOfMessageToSendToS2 == 0) {
            maxDelayS2 = 0;
            numberOfMessageToSendToS3 = 0;
        }
        
        while (stopwatch < maxDelayS2) {
            time(&start);

            do {
                time(&end);
            }
            while (difftime(end, start) < 1);

            for (int i = 0; i < numberOfMessageToSendToS2; i++) {

                // Copy from timeDeparture into timeArrival
                memcpy(sender_traffic_two[i].timeArrival, sender_traffic_two[i].timeDeparture, sizeof(sender_traffic_two[i].timeArrival));

                sender_traffic_two[i].hoursArrival = sender_traffic_two[i].hoursDeparture;
                sender_traffic_two[i].minutesArrival = sender_traffic_two[i].minutesDeparture;
                sender_traffic_two[i].secondsArrival = sender_traffic_two[i].secondsDeparture;

                // Update new struct field for sending to R3 the delays value
                sender_traffic_two[i].delayS1 = sender_instructions_two[i].delayS1;
                sender_traffic_two[i].delayS2 = sender_instructions_two[i].delayS2;
                sender_traffic_two[i].delayS3 = sender_instructions_two[i].delayS3;

                if (sender_traffic_two[i].id_sender == 2 && strcmp(sender_instructions_two[i].type, SH) == 0 && stopwatch == sender_instructions_two[i].delayS2) {
                    printf("<Sender Manager 2>\t Sending to RM by Shared Memory\n");
                 
                    // Added delay to the message that pass by shared memory
                    getFormattedTimewithDelay(sender_traffic_two, sender_instructions_two[i].delayS2 + *pointerToAddDelay, i); 

                    // Attach to the shared memory SH
                    sender_traffic_struct *SHpointer2 = (sender_traffic_struct *)shmAttach(idShSender, 0);

                    // Copy the struct into the shared memory SH
                    memcpy(SHpointer2, &sender_traffic_two[i], sizeof(sender_traffic_struct));

                    // Detach to the shared memory SH
                    shmDetach(SHpointer2);

                    printStruct(file2D, sender_traffic_two, i);
                }
                else if (sender_traffic_two[i].id_sender == 2 && strcmp(sender_instructions_two[i].type, Q) == 0 && stopwatch == sender_instructions_two[i].delayS2) {
                    printf("<Sender Manager 2>\t Sending to RM by Message Queue\n");

                    // Copy sender_traffic_two into sender_traffic_queue
                    copyToSenderTrafficQueue(sender_traffic_queue, sender_traffic_two, i);

                    // Added delay to the message that pass by message queue
                    getFormattedTimewithDelayMessageQueue(sender_traffic_queue, sender_instructions_two[i].delayS2 + *pointerToAddDelay, i); 
                    
                    // Size of the sender_traffic_queue_struct
                    mSize = sizeof(sender_traffic_queue_struct) - sizeof(long);
                    
                    if (sender_traffic_queue[i].id == 1) {
                        sender_traffic_queue[i].mtype = 1;
                    } else if (sender_traffic_queue[i].id_receiver == 2) {
                        sender_traffic_queue[i].mtype = 2;
                    } else {
                        sender_traffic_queue[i].mtype = 3;
                    }

                    // Sending the message by the message queue Q
                    if (msgsnd(idQueue, &sender_traffic_queue[i], mSize, 0) FAILS) {
                        ErrExit("<Sender Manager 2>\t msgsnd failed\n");
                    }
                    
                    printStructQueue(file2D, sender_traffic_queue, i);
                }
                else {
                    if (stopwatch == sender_instructions_two[i].delayS2) {
                        printf("<Sender Manager 2>\t Sending to S3 by PIPE2\n");

                        // Added delay to the message that pass by pipe to s3
                        getFormattedTimewithDelay(sender_traffic_two, sender_instructions_two[i].delayS2 + *pointerToAddDelay, i); 

                        // PIPE2 for sending the sender_traffic_struct to S3
                        numberOfMessageToSendToS3++;

                        sizeStruct = sizeof(sender_traffic_struct); 
                        wB_sender_S2 = write(PIPE2[1], &sender_traffic_two[i], sizeStruct);

                        if (wB_sender_S2 != sizeStruct) {
                            ErrExit("<Sender Manager 2>\t write failed\n");
                        }

                        sizeStructInstructions2 = sizeof(sender_struct); 
                        wB_3 = write(PIPE1_3[1], &sender_instructions_two[i], sizeStructInstructions2);

                        if (wB_3 != sizeStructInstructions2) {
                            ErrExit("<Sender Manager 2>\t PIPE1_3 write failed\n");
                        }

                        printStruct(file2D, sender_traffic_two, i);   
                    }
                }
            }
            stopwatch++;
        }

        // Lock idSemaphore & unlock idSemaphore + 1
        semOperation(semId, idSemaphore, -1);
        semOperation(semId, idSemaphore + 1, 1);

        // sizePipeS2 for sending the numberOfMessageToSendToS3
        sizePipeS2 = sizeof(numberOfMessageToSendToS3);
        wB2_sender_S2 = write(pipeForSizeS2[1], &numberOfMessageToSendToS3, sizePipeS2);  
    
        if (wB2_sender_S2 != sizePipeS2) {
            ErrExit("<Sender Manager 2>\t sizePipeS2 write failed\n");
        }
        
        if (close(file2D) FAILS) {
            ErrExit("<Sender Manager 2>\t F2.csv close failed\n"); 
        }

        // Read the pid of the sender3
        rB3 = read(pipeForSendPidToS3[0], &pidS3, sizeof(pidS3));

        if (rB3 <= 0){
            ErrExit("<Sender Manager 2>\t pipeForSendPidToR3 read failed\n"); 
        }

        printf("<Sender Manager 2>\t sending SIGCONT to S3\n");
        
        // Resume the sender3 
        if (kill(pidS3, SIGCONT) == -1) {
            ErrExit("<Sender Manager 2>\t SIGCONT to S3 failed\n");
        }

        sleep(2);

        if (signal(SIGTERM, sigSHandler) == SIG_ERR) {
            ErrExit("<Sender Manager 2>\t change signal handler failed\n");
        }
        pause();
    }
    // PIPE1 destructionTIme 
    timef(timeIPCDesP1);

    //PIPE pipeForSendPidToS2 destructionTIme
    timef(timeIPCDesPToS2);

    // PIPE pipeForSizeS1 destructionTIme
    timef(timeIPCDesPSS1);
    
    // PIPE PIPE_12 destructionTIme
    timef(timeIPCDesP12);

    //================================================================================
    // SENDER 3
    //================================================================================

    sender3 = fork();

    if (sender3 FAILS) {
        ErrExit("<Sender Manager 3>\t Fork failed\n");
    }
    if (sender3 == 0) {
        closeReadEndPipe(pipeForSendPidToS3);
        closeReadEndPipe(pipeForSizeS1);
        closeReadEndPipe(PIPE1_2);

        closeWriteEndPipe(PIPE2);
        closeWriteEndPipe(pipeForSizeS2);
        closeWriteEndPipe(PIPE1_3);

        int numberOfMessageToSendToS3 = 0, stopwatch = 0, maxDelayS3 = 0, voidFifo = 1;
        time_t start, end;

        idSemaphore = 2;
        
        sender3Pid = getpid();

        // Write into pipeForSendPidToS3 the pid of sender3
        sizesender3Pid = sizeof(sender3Pid);
        wP3 = write(pipeForSendPidToS3[1], &sender3Pid, sizesender3Pid); 
    
        if (wP3 != sizesender3Pid) {
            ErrExit("<Sender Manager 3>\t sender3Pid write failed\n");
        }

        // Use signal for stopping itself
        if (kill(sender3Pid, SIGSTOP) == -1) {
            ErrExit("<Sender Manager 3>\t SIGSTOP in Sender 3 failed\n");
        }
        printf("<Sender Manager 3>\t Resume the execution\n");

        file3D = open(file3, O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
        errorOpen(file3D);
        headerOnFile(filesHeader, file3D, filesTotalRead);

        if (write(file3D, &nextline, sizeof(char)) != sizeof(char)) {
            ErrExit("<Sender Manager 3>\t F3.csv write failed \n");
        }

        // pipeForSizeS3 for reading the numberOfMessageToSendToS3
        ssize_t rB2_sender_S3 = read(pipeForSizeS2[0], &numberOfMessageToSendToS3, sizeof(numberOfMessageToSendToS3));

        if (rB2_sender_S3 <= 0){
            ErrExit("<Sender Manager 3>\t pipeForSizeS2 read failed\n"); 
        }

        if (numberOfMessageToSendToS3 != 0) {
            // PIPE1_3 for reading the sender_traffic_three
            rB_3 = read(PIPE1_3[0], &sender_instructions_three, sizeof(sender_instructions_three));
            if (rB_3 <= 0) {
                ErrExit("<Sender Manager 3>\t PIPE1_3 read failed\n"); 
            }

            // pipeForSizeS2 for reading the sender_traffic_three
            ssize_t rB_sender_S3 = read(PIPE2[0], &sender_traffic_three, sizeof(sender_traffic_three));
            if (rB_sender_S3 <= 0) {
                ErrExit("<Sender Manager 3>\t PIPE2 read failed\n"); 
            }

            for (int i = 0; i < numberOfMessageToSendToS3; i++) {
                if (sender_instructions_three[i].delayS3 > maxDelayS3) {
                    maxDelayS3 = sender_instructions_three[i].delayS3;
                }
            }
            maxDelayS3++;
        }
       
        /********* SIGUSER1 *********/
        if (signal(SIGUSR1, sigSHandler) == SIG_ERR) {
            ErrExit("<Sender Manager 3>\t change signal handler failed");
        }
        
        /********* SIGHUP *********/
        if (signal(SIGHUP, (void (*)(int))sigSHandler300) == SIG_ERR) {
            ErrExit("<Sender Manager 3>\t change signal SIGHUP handler failed\n");
        }

        /********* SIGCONT *********/
        if (signal(SIGCONT, sigSHandler) == SIG_ERR) {
            ErrExit("<Sender Manager 3>\t change signal SIGCONT handler failed\n");
        }

        /********* SIGUSR2 *********/
        if (signal(SIGUSR2, (void (*)(int))sigSHandler200) == SIG_ERR){
            ErrExit("<Sender Manager 3>\t change signalHandler200 failed\n");
        }
        
        printf("<Sender Manager 3>\t I'm ready to catch for a signal from someone...  \n");

        pause();   

        sigSHandler300(FAKE_SIGNAL, sender_instructions_three, numberOfMessageToSendToS3, 3);

        numberOfMessageToSendToS3 = sigSHandler200(FAKE_SIGNAL, numberOfMessageToSendToS3);
        printf("<Sender Manager 3>\t The new number of message to send to RECEIVER3 is %d\n", numberOfMessageToSendToS3);

        if (numberOfMessageToSendToS3 == 0) {
            maxDelayS3 = 0;
        }
        
        while (stopwatch < maxDelayS3) { 
            time(&start);

            do {
                time(&end); 
            }
            while (difftime(end, start) < 1);

            for (int i = 0; i < numberOfMessageToSendToS3; i++) {
                // Copy from timeDeparture into timeArrival
                memcpy(sender_traffic_three[i].timeArrival, sender_traffic_three[i].timeDeparture, sizeof(sender_traffic_three[i].timeArrival));

                sender_traffic_three[i].hoursArrival = sender_traffic_three[i].hoursDeparture;
                sender_traffic_three[i].minutesArrival = sender_traffic_three[i].minutesDeparture;
                sender_traffic_three[i].secondsArrival = sender_traffic_three[i].secondsDeparture;

                // Update new struct field for sending to R3 the delays value
                sender_traffic_three[i].delayS1 = sender_instructions_three[i].delayS1;
                sender_traffic_three[i].delayS2 = sender_instructions_three[i].delayS2;
                sender_traffic_three[i].delayS3 = sender_instructions_three[i].delayS3;

                if (sender_traffic_three[i].id_sender == 3 && strcmp(sender_instructions_three[i].type, FIFO) == 0 && stopwatch == sender_instructions_three[i].delayS3) {
                    printf("<Sender Manager 3>\t Sending to RM by FIFO\n");
                    voidFifo = 0;

                    // Added stopwatch delay to sender_traffic_three
                    getFormattedTimewithDelay(sender_traffic_three, sender_instructions_three[i].delayS3 + *pointerToAddDelay, i); 

                    // Open FIFO in write-end
                    FIFO_D = open(pathname, O_WRONLY);    
                    if (FIFO_D FAILS) {
                        ErrExit("<Sender Manager 3>\t FIFO open failed\n");
                    }

                    sizeOfSenderTrafficStruct = sizeof(sender_traffic_struct);
                    printf("<Sender Manager 3>\t SENDING sender_traffic_three\n");

                    wF = write(FIFO_D, &sender_traffic_three[i], sizeOfSenderTrafficStruct);
                    if (wF != sizeOfSenderTrafficStruct) {
                        ErrExit("<Sender Manager 3>\t FIFO write failed\n");
                    }

                    if (close(FIFO_D) != 0) {
                        ErrExit("<Sender Manager 3>\t FIFO close failed\n");
                    }

                    printStruct(file3D, sender_traffic_three, i);
                }
                if (sender_traffic_three[i].id_sender == 3 && strcmp(sender_instructions_three[i].type, SH) == 0 && stopwatch == sender_instructions_three[i].delayS3) {
                    printf("<Sender Manager 3>\t Sending to RM by Shared Memory SH\n");

                    // Added stopwatch delay to sender_traffic_three
                    getFormattedTimewithDelay(sender_traffic_three, sender_instructions_three[i].delayS3 + *pointerToAddDelay, i); 

                    // Attach to the shared memory SH
                    sender_traffic_struct *SHpointer3 = (sender_traffic_struct *)shmAttach(idShSender, 0);

                    // Copy the struct into the shared memory SH
                    memcpy(SHpointer3, &sender_traffic_three[i], sizeof(sender_traffic_struct));

                    // Detach to the shared memory SH
                    shmDetach(SHpointer3);

                    printStruct(file3D, sender_traffic_three, i);
                }
                if (sender_traffic_three[i].id_sender == 3 && strcmp(sender_instructions_three[i].type, Q) == 0 && stopwatch == sender_instructions_three[i].delayS3) {
                    printf("<Sender Manager 3>\t Sending to RM by Message Queue Q\n");

                    // Copy sender_traffic_three into sender_traffic_queue
                    copyToSenderTrafficQueue(sender_traffic_queue, sender_traffic_three, i);
                   
                    // Added stopwatch delay to sender_traffic_three
                    getFormattedTimewithDelayMessageQueue(sender_traffic_queue, sender_instructions_three[i].delayS3 + *pointerToAddDelay, i); 

                    // Size of the sender_traffic_queue_struct
                    mSize = sizeof(sender_traffic_queue_struct) - sizeof(long);

                    if (sender_traffic_queue[i].id_receiver == 1) {
                        sender_traffic_queue[i].mtype = 1;
                    } else if (sender_traffic_queue[i].id_receiver == 2) {
                        sender_traffic_queue[i].mtype = 2;
                    } else {
                        sender_traffic_queue[i].mtype = 3;
                    }

                    // Sending the message in the queue
                    if (msgsnd(idQueue, &sender_traffic_queue[i], mSize, IPC_NOWAIT) FAILS) {
                        ErrExit("<Sender Manager 3>\t msgsnd failed\n");
                    }

                    printStructQueue(file3D, sender_traffic_queue, i);
                } 
            }
            stopwatch++;
        }

        if (voidFifo) {
            // Open FIFO in write-end
            FIFO_D = open(pathname, O_WRONLY);    
            if (FIFO_D FAILS) {
                ErrExit("<Sender Manager 3>\t FIFO open failed\n");
            }

            sizeOfSenderTrafficStruct = sizeof(sender_traffic_struct);
            printf("<Sender Manager 3>\t SENDING sender_traffic_three\n");

            sender_traffic_three[0].id = -500;

            wF = write(FIFO_D, &sender_traffic_three[0].id, sizeOfSenderTrafficStruct);
            if (wF != sizeOfSenderTrafficStruct) {
                ErrExit("<Sender Manager 3>\t FIFO write failed\n");
            }

            if (close(FIFO_D) != 0) {
                ErrExit("<Sender Manager 3>\t FIFO close failed\n");
            }
        }

        semOperation(semId, idSemaphore, -1);
        semOperation(semId, idSemaphore + 1, 1); 

        closeReadEndPipe(PIPE2);
        closeReadEndPipe(pipeForSizeS2);
        closeReadEndPipe(PIPE1_3);

        closeWriteEndPipe(pipeForSendPidToS3);

        if (close(file3D) FAILS) {
            ErrExit("<Sender Manager 3>\t F3.csv close failed\n");
        }
        
        sleep(3);

        if (signal(SIGTERM, sigSHandler) == SIG_ERR) {
            ErrExit("<Sender Manager 3>\t change signal handler failed\n");
        }
        pause();
    }

    // PIPE pipeForSendPidToS3 destructionTIme
    timef(timeIPCDesPToS3);

    // PIPE PIPE13 destructionTIme
    timef(timeIPCDesP13);

    // PIPE pipeForSizeS2 destructionTIme
    timef(timeIPCDesPSS2);
    
    // PIPE2 destructionTIme 
    timef(timeIPCDesP2);

    // Write the Processes ID into F8.csv
    sprintf(bodyF8, "SenderID;PID\nS1;%d\nS2;%d\nS3;%d\n"+ '\0', (int)sender1, (int)sender2, (int)sender3);

    for (int i = 0; bodyF8[i] != '\0'; i++) {
        if (write(file8D, &bodyF8[i], sizeof(char)) != sizeof(char)) {
            ErrExit("<Sender Manager>\t Write failed \n");
        }
    }

    argSender.array = (semVal *)values;

    // Obtain all semaphore values of semId2
    if (semctl(semId2, 0, GETALL, argSender) == -1) {
        ErrExit("<Sender Manager>\t Semctl GETALL failed\n");
    }

    // Set the first semaphore to zero
    arg0.val = 0;
    if (semctl(semId2, 0, SETVAL, arg0) == -1) {
        ErrExit("<Sender Manager>\t Semctl GETALL failed\n");
    }

    // Set the second semaphore to one
    arg1.val = 1;
    if (semctl(semId2, 1, SETVAL, arg1) == -1) {
        ErrExit("<Sender Manager>\t Semctl GETALL failed\n");
    }

    // Wait sender1 & sender2 & sender3
    while (wait(&status) != -1);

    idSemaphore = 3;

    // Lock the third semaphore  
    semOperation(semId, idSemaphore, -1);
    
    ////////////////////////////////////////////////////////////////////////////////////////////////// WRITE F10.csv
    
    updateIPC(IPCSender, numberOfIPC++, "PIPE1", *PIPE1, SM, timeIPCP1, timeIPCDesP1);
    updateIPC(IPCSender, numberOfIPC++, "PIPEFORSIZES1", *pipeForSizeS1, SM, timeIPCPSS1, timeIPCDesPSS1);
    updateIPC(IPCSender, numberOfIPC++, "PIPE1_2", *PIPE1_2, SM, timeIPCP12, timeIPCDesP12);
    updateIPC(IPCSender, numberOfIPC++, "PIPEFORSENDPIDTOS2", *pipeForSendPidToS2, SM, timeIPCPToS2, timeIPCDesPToS2);
    updateIPC(IPCSender, numberOfIPC++, "PIPE2", *PIPE2, SM, timeIPCP2, timeIPCDesP2);
    updateIPC(IPCSender, numberOfIPC++, "PIPEFORSIZES2", *pipeForSizeS2, SM, timeIPCPSS2, timeIPCDesPSS2);
    updateIPC(IPCSender, numberOfIPC++, "PIPE1_3", *PIPE1_3, SM, timeIPCP13, timeIPCDesP13);
    updateIPC(IPCSender, numberOfIPC++, "PIPEFORSENDPIDTOS3", *pipeForSendPidToS3, SM, timeIPCPToS3, timeIPCDesPToS3);
    updateIPC(IPCSender, numberOfIPC++, "IDSHSENDER", idShSender, SM, timeIPCSH, "HH:MM:SS");
    updateIPC(IPCSender, numberOfIPC++, "Q", idQueue, SM, timeIPCQ, "HH:MM:SS");
    updateIPC(IPCSender, numberOfIPC++, "SEMID", semId, SM, timeIPCSem, "HH:MM:SS");
    updateIPC(IPCSender, numberOfIPC++, "SEMID2", semId2, SM, timeIPCSem2, "HH:MM:SS");

    //////////////////////////////////////////////////////////////////////////////////////////////////
   
    for (int i = 0; i < numberOfIPC; i++){
        printIPCStruct(file10D, IPCSender, i);
    }

    // Unlock sem fourth for RM
    semOperation(semId, idSemaphore + 1, 1);
    
    // Close F8.csv & senderHeader.csv & filesHeader.csv
    if (close(file8D) FAILS || close(senderHeader) FAILS || close(filesHeader) FAILS || close(file10D) FAILS)
        ErrExit("<Sender Manager>\t close failed\n");

    return 0;
}