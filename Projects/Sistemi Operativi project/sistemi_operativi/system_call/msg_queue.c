/// @file msg_queue.c
/// @brief Contiene l'implementazione delle funzioni
///         specifiche per la gestione delle message queue.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/msg.h>
#include "defines.h"
#include "err_exit.h"
#include "msg_queue.h"

// Create the message queue with the syscall msgget
int messageCreate(key_t key) {
    int id = msgget(key, IPC_CREAT | S_IRUSR | S_IWUSR);
    if (id FAILS) {
        pErrExit("Unable to create message queue");
    }
    return id;
}

// Print the message queue on terminal
void printQueueOnTerminal(sender_traffic_queue_struct *queue, int position){
    printf("===================Q=================== \n");
    printf(
            "%d, %s, %d, %d, %s, %s\n", 
            queue[position].id, 
            queue[position].message, 
            queue[position].id_sender, 
            queue[position].id_receiver, 
            queue[position].timeArrival, 
            queue[position].timeDeparture
        );
    printf("===================Q=================== \n");
}

// Write the message queue on file
void printQueueOnFile(int file, sender_traffic_queue_struct *y, int position) {

    char nextfield = ';', nextline = '\n';

    int nid = y[position].id; 
    char fd1 = itoa(nid);   // id

    int lmessage = strlen(y[position].message); 
    
    char nsender = y[position].id_sender;
    char fd3 = itoa(nsender);   // id_sender

    char nreceiver = y[position].id_receiver;
    char fd4 = itoa(nreceiver); // id_receiver

    int lTimeArrival = strlen(y[position].timeArrival); 
    int lTimeDeparture = strlen(y[position].timeDeparture); 

    write(file, &fd1, sizeof(char)); 
    write(file, &nextfield, sizeof(char));

    write(file, &y[position].message, lmessage);
    write(file, &nextfield, sizeof(char));

    write(file, &fd3, sizeof(char));
    write(file, &nextfield, sizeof(char));

    write(file, &fd4, sizeof(char));
    write(file, &nextfield, sizeof(char));

    write(file, &y[position].timeArrival, lTimeArrival);
    write(file, &nextfield, sizeof(char));

    write(file, &y[position].timeDeparture, lTimeDeparture);
    write(file, &nextfield, sizeof(char));

    write(file, &nextline, sizeof(char));   // nextline    
}

// Copy the sender_traffic_struct into sender_traffic_queue_struct
void copyToSenderTrafficQueue(sender_traffic_queue_struct *paste, sender_traffic_struct *copy, int i){
    paste[i].mtype = 1;
    paste[i].id = copy[i].id;
    strcpy(paste[i].message, copy[i].message);
    paste[i].id_sender = copy[i].id_sender;
    paste[i].id_receiver = copy[i].id_receiver;
    strcpy(paste[i].timeArrival, copy[i].timeArrival);
    strcpy(paste[i].timeDeparture, copy[i].timeDeparture);
    paste[i].hoursArrival = copy[i].hoursArrival;
    paste[i].minutesArrival = copy[i].minutesArrival;
    paste[i].secondsArrival = copy[i].secondsArrival;
    paste[i].hoursDeparture = copy[i].hoursDeparture;
    paste[i].minutesDeparture = copy[i].minutesDeparture;
    paste[i].secondsDeparture = copy[i].secondsDeparture;
    paste[i].delayS1 = copy[i].delayS1;
    paste[i].delayS2 = copy[i].delayS2;
    paste[i].delayS3 = copy[i].delayS3;    
}

// Copy the sender_traffic_struct into sender_traffic_queue_struct
void copyToSenderTrafficQueue2(sender_traffic_queue_struct *copy, sender_traffic_struct *paste, int i, int nm, int nmpipe){
    paste[i+nmpipe].id = copy[i-nm].id;
    strcpy(paste[i+nmpipe].message, copy[i-nm].message);
    paste[i+nmpipe].id_sender = copy[i-nm].id_sender;
    paste[i+nmpipe].id_receiver = copy[i-nm].id_receiver;
    strcpy(paste[i+nmpipe].timeArrival, copy[i-nm].timeArrival);
    strcpy(paste[i+nmpipe].timeDeparture, copy[i-nm].timeDeparture);
    paste[i+nmpipe].hoursArrival = copy[i-nm].hoursArrival;
    paste[i+nmpipe].minutesArrival = copy[i-nm].minutesArrival;
    paste[i+nmpipe].secondsArrival = copy[i-nm].secondsArrival;
    paste[i+nmpipe].hoursDeparture = copy[i-nm].hoursDeparture;
    paste[i+nmpipe].minutesDeparture = copy[i-nm].minutesDeparture;
    paste[i+nmpipe].secondsDeparture = copy[i-nm].secondsDeparture;
    paste[i+nmpipe].delayS1 = copy[i-nm].delayS1;
    paste[i+nmpipe].delayS2 = copy[i-nm].delayS2;
    paste[i+nmpipe].delayS3 = copy[i-nm].delayS3;    
}

// Remove the message queue
void removeQ(int idQueue) {
    if (msgctl(idQueue, IPC_RMID, NULL) != 0) {
        ErrExit("Message queue removed failed\n");
    }
}