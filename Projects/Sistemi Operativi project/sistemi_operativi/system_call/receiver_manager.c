/// @file receiver_manager.c
/// @brief Contiene l'implementazione del receiver_manager.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <signal.h>
#include "defines.h"
#include "err_exit.h"
#include "semaphore.h"
#include "shared_memory.h"
#include "msg_queue.h"
#include "pipe.h"
#include "fifo.h"

char buffer[SIZE];
unsigned short idSemaphore = 3;

pid_t receiver1, receiver2, receiver3;
int receiver1Pid = 0, receiver2Pid = 0, receiver3Pid = 0, idQueue = 0, idShReceiver = 0, FIFO_D = 0, addDelay2 = 0, 
    *pointerToAddDelay2 = &addDelay2, secondTime = 0, *pointerToSecondTime = &secondTime,  
    secondTime300 = 0, *toModifyZero = &secondTime300;
    
// Manage the signal SIGUSER1, SIGCONT and SIGTERM
void sigRHandler(int sig) {
    if (sig == SIGUSR1) {
        printf("\x1b[30m\x1b[35m<Receiver Manager>\t Received signal IncreaseDelay\n");
        printf("\x1b[0m\n");

        *pointerToAddDelay2 = 5;
        printf("<Receiver Manager>\t I'm sleeping for the added delay\n");

        sleep(*pointerToAddDelay2);
        printf("<Receiver Manager>\t I'm finished sleeping\n");
    }

    if (sig == SIGCONT) {
        printf("\x1b[36m<Receiver Manager>\t Received signal SIGCONT\x1b[0m\n");
    }

    if (sig == SIGTERM) {
        printf("<Receiver Manager>\t Signal terminates the target process\n");  
        exit(0); // terminate the process
    } 
}

// Manage the signal SIGUSER2 for the action "RemoveMsg"
int sigRHandler200(int sig, int numberOfMessage) {   
    int zero = 0;

    if (sig == SIGUSR2) {
        printf("\x1b[30m\x1b[35m<Receiver Manager>\t Received signal RemoveMsg\x1b[0m\n");
        *pointerToSecondTime = 1;
        return zero;
    }
    if (*pointerToSecondTime == 0) {
        return numberOfMessage;
    }
    else {
        return zero;
    }
}

// Manage the signal SIGHUP for the action "SendMsg"
void sigSHandler300(int sig) {
    if (sig == SIGHUP) {
        printf("\x1b[36m<Receiver Manager>\t Received signal SendMsg\x1b[0m\n");
        *toModifyZero = 1;
    }
}

key_t key_queue = 01110001, key_sh = 01101101, key_sm = 01110011, key_sm2 = 11110011;

int main(int argc, char * argv[]) {
    char buffer[SIZE], *file4, *file5, *file6, *file9, *file10, bodyF9[SIZE], *pathname = "OutputFiles/my_fifo.txt",
        timeIPCF[20], timeIPCDesF[20], timeIPCP4[20], timeIPCDesP4[20], timeIPCP3[20], timeIPCDesP3[20], timeIPCPToR3[20], 
        timeIPCDesPToR3[20], timeIPCPSR2[20], timeIPCDesPSR2[20], timeIPCPToR2[20], timeIPCDesPToR2[20], timeIPCPSR1[20], 
        timeIPCDesPSR1[20], timeIPCDesSH[20], timeIPCQ[20], timeIPCDesQ[20];

    int PIPE3[2], PIPE4[2], pipeForSizeR2[2], pipeForSendPidToR3[2], pipeForSendPidToR2[2], pipeForSizeR1[2], values[6], 
        filesHeader, receiverHeader, file4D, file5D, file6D, file9D, file10D, nextline = '\n', tot = 0, nr = 0, filesTotalRead, values2[3],  
        numberOfMessageToSendToR2 = 0, numberOfChar = 0, numberOfField = 0, status, numberOfIPC = 0;
       
    ssize_t wP, rP, sizereceiver2Pid, sizeStructR3, sizePipeR2, sizereceiver1Pid, wP1, rP1, sizeStructR1, sizePipeR1, bR, eM, mSize;

    sender_traffic_struct *SHpointer, sender_traffic_four_sh[50], sender_traffic_five_sh[50], sender_traffic_six_sh[50], 
        sender_traffic_four[50], sender_traffic_five[50], sender_traffic_six[50];

    sender_traffic_queue_struct sender_traffic_queue[100];
    ipc_struct IPCReceiver[10];

    semun argReceiver, argReceiver2, arg0, arg1;
    
    // Set of signals
    sigset_t receiverSet, receiverPrevSet;

    // Initialize receiverSet to contain all signals
    sigfillset(&receiverSet);

    // Remove signals 
    sigdelset(&receiverSet, SIGCONT);
    sigdelset(&receiverSet, SIGUSR1);
    sigdelset(&receiverSet, SIGUSR2);
    sigdelset(&receiverSet, SIGHUP);
    sigdelset(&receiverSet, SIGTERM);

    // Blocking the remaning signals
    sigprocmask(SIG_SETMASK, &receiverSet, &receiverPrevSet);

    file4 = "OutputFiles/F4.csv";
    file5 = "OutputFiles/F5.csv";
    file6 = "OutputFiles/F6.csv";
    file9 = "OutputFiles/F9.csv";
    file10 = "OutputFiles/F10.csv";

    // Create the PIPEs and set the creation times
    createPipe(PIPE4);
    timef(timeIPCP4);

    createPipe(PIPE3);
    timef(timeIPCP3);

    createPipe(pipeForSendPidToR3);
    timef(timeIPCPToR3);

    createPipe(pipeForSizeR2);
    timef(timeIPCPSR2); 

    createPipe(pipeForSendPidToR2);
    timef(timeIPCPToR2); 

    createPipe(pipeForSizeR1);
    timef(timeIPCPSR1); 

    // Create the message queue Q and set the creation time
    idQueue = msgget(key_queue, IPC_CREAT | S_IRUSR | S_IWUSR);
    if (idQueue FAILS) {
        ErrExit("<Receiver Manager>\t msgget failed\n");
    }
    timef(timeIPCQ);

    // Create the FIFO and set the creation time
    makeFIFO(pathname);
    timef(timeIPCF);

    // Create the shared memory SH
    size_t shSize = sizeof(sender_traffic_struct);
    idShReceiver = shmCreate(key_sh, shSize);

    filesHeader = open("InputFiles/filesHeader.csv", O_RDWR);
    errorOpenWithoutCreate(filesHeader);

    receiverHeader = open("InputFiles/receiverHeader.csv", O_RDWR);
    errorOpenWithoutCreate(receiverHeader);

    // Useful for the synchronization of the processes
    sleep(10);

    // Open F9
    file9D = open(file9, O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
    errorOpen(file9D);
    
    // Read and count the size of filesHeader 
    filesTotalRead = totalCharInFile(filesHeader, buffer, nr, tot);

    // PIPE4 destructionTIme 
    timef(timeIPCDesP4);

    // PIPE pipeForSizeR1 destructionTIme 
    timef(timeIPCDesPSR1);

    //================================================================================
    // RECEIVER 1
    //================================================================================

    receiver1 = fork(); // Generate a subprocess, make a fork for creating child process
    
    if (receiver1 FAILS) {
        ErrExit("Fork failed\n");
    }
    if (receiver1 == 0) {
        closeWriteEndPipe(PIPE4);
        closeWriteEndPipe(pipeForSizeR1);

        int i = 0, delayS1 = 0, numberOfMessageToSendToR1 = 0, numberOfMessageReceived = 0;

        receiver1Pid = getpid();

        // Send pid receiver1 to process receiver2
        sizereceiver1Pid = sizeof(receiver1Pid);

        wP1 = write(pipeForSendPidToR2[1], &receiver1Pid, sizereceiver1Pid);  

        if (wP1 != sizereceiver1Pid) {
            ErrExit("<Receiver Manager 1>\t pipeForSendPidToR3 write failed\n");
        }
        
        if (kill(receiver1Pid, SIGSTOP) == -1) {
            ErrExit("<Receiver Manager 1>\t SIGSTOP in R1 failed\n");
        }
        printf("<Receiver Manager 1>\t Resume the execution\n");

        // Open F6 and write the header with the next line
        file6D = open(file6, O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR); 
        errorOpen(file6D);
        headerOnFile(filesHeader, file6D, filesTotalRead);
        
        if (write(file6D, &nextline, sizeof(char)) != sizeof(char)) {
            ErrExit("write failed\n");
        }

        /********* SIGUSER1 *********/
        if (signal(SIGUSR1, sigRHandler) == SIG_ERR) {
            ErrExit("<Receiver Manager 1>\t change signal handler failed\n");
        } 

        if (signal(SIGHUP, (void (*)(int))sigSHandler300) == SIG_ERR) {
            ErrExit("<Receiver Manager 1>\t change signal SIGHUP handler failed\n");
        }
        
        /********* SIGCONT *********/
        if (signal(SIGCONT, sigRHandler) == SIG_ERR) {
            ErrExit("<Receiver Manager 1>\t change signal SIGCONT handler failed\n");
        }

        /********* SIGUSR2 *********/
        if (signal(SIGUSR2, (void (*)(int))sigRHandler200) == SIG_ERR){
            ErrExit("Receiver 1> change signalHandler200 failed\n");
        }
        printf("<Receiver Manager 1>\t I'm ready to catch for a signal from someone...  \n");

        pause();  

        numberOfMessageToSendToR1 = sigRHandler200(FAKE_SIGNAL, numberOfMessageToSendToR1);
        
        /////////////////////////////////////////////////////////////////////// READ PIPE FROM R2

        // Read from pipeForSizeR1 and save into char numberOfMessageToSendToR1
        ssize_t rB2 = read(pipeForSizeR1[0], &numberOfMessageToSendToR1, sizeof(numberOfMessageToSendToR1));
        if (rB2 <= 0){
            ErrExit("<Receiver Manager 1>\t read failed\n"); 
        }

        // Read from PIPE4 and save into struct sender_traffic_six
        if (numberOfMessageToSendToR1 != 0) {
            ssize_t rB1 = read(PIPE4[0], &sender_traffic_six, sizeof(sender_traffic_six));
            if (rB1 <= 0) {
                ErrExit("<Receiver Manager 1>\t read failed\n"); 
            }
        }

        for (int i = 0; i < numberOfMessageToSendToR1; i++) {
            
            if(*toModifyZero == 1) {
                delayS1 = 0;
            }
            else {
                delayS1 = sender_traffic_six[i].delayS1;
            }
        
            memcpy(sender_traffic_six[i].timeArrival, sender_traffic_six[i].timeDeparture, sizeof(sender_traffic_six[i].timeArrival));
            getFormattedTimewithDelay(sender_traffic_six, delayS1 + *pointerToAddDelay2, i); 
            
            sleep(delayS1);

            printStruct(file6D, sender_traffic_six, i);  
        }
        
        /////////////////////////////////////////////////////////////////////// SHARED MEMORY R1

        i = 0;

        while (i >= 0) {
            SHpointer = (sender_traffic_struct *)shmAttach(idShReceiver, SHM_RDONLY);

            if (SHpointer < 0) {
                printf("<Receiver Manager 1>\t Unable to receive struct on the shared memory\n");
                break;
            } else if (SHpointer[i].id > 0 && SHpointer[i].id_receiver == 1) {
                memcpy(sender_traffic_six_sh, &SHpointer[i], sizeof(sender_traffic_struct));

                if(*toModifyZero == 1) {
                    delayS1 = 0;
                }
                else {
                    delayS1 = sender_traffic_six_sh[i].delayS1;
                }

                memcpy(sender_traffic_six_sh[i].timeArrival, sender_traffic_six_sh[i].timeDeparture, sizeof(sender_traffic_six_sh[i].timeArrival));
                getFormattedTimewithDelay(sender_traffic_six_sh, delayS1 + *pointerToAddDelay2, i); 

                printStruct(file6D, sender_traffic_six_sh, i);

                copyToSenderTrafficSH(sender_traffic_six, sender_traffic_six_sh, i, 0, numberOfMessageToSendToR1);  

                sleep(delayS1);

            } else {
                break;
            }
            i++;
        }
        shmDetach(SHpointer);

        if (i != 0){
            numberOfMessageReceived = i;
        }
        
        /////////////////////////////////////////////////////////////////////// MESSAGE QUEUE R1

        // Size of the message queue Q
        mSize = sizeof(sender_traffic_queue_struct) - sizeof(long);
        
        i = 0;
        // Reads from message queue Q the sender_traffic_queue
        while (i >= 0) {
            eM = msgrcv(idQueue, &sender_traffic_queue[i], mSize, 1, IPC_NOWAIT);
            
            if (eM < 0 && (errno != ENOMSG) && (errno != EINTR)) {
                printf("<Receiver Manager 1>\t Unable to receive message on queue\n");
                break;
            } else if (sender_traffic_queue[i].id > 0 && sender_traffic_queue[i].id_receiver == 1) {

                if(*toModifyZero == 1) {
                    delayS1 = 0;
                }
                else {
                    delayS1 = sender_traffic_queue[i].delayS1;
                }

                memcpy(sender_traffic_queue[i].timeArrival, sender_traffic_queue[i].timeDeparture, sizeof(sender_traffic_queue[i].timeArrival));
                getFormattedTimewithDelayMessageQueue(sender_traffic_queue, delayS1 + *pointerToAddDelay2, i); 

                printQueueOnFile(file6D, sender_traffic_queue, i);

                i = numberOfMessageReceived; 
                copyToSenderTrafficQueue2(sender_traffic_queue, sender_traffic_six, i, numberOfMessageReceived, numberOfMessageToSendToR1);

                sleep(delayS1);

            } else {
                break;
            }
            i++;
        }

        if (i != 0){
            numberOfMessageReceived = i;
        }

        clearSenderTraffic(sender_traffic_six, 50);
        clearSenderTraffic(sender_traffic_six_sh, 50);
        clearSenderTrafficQueue(sender_traffic_queue, 50);

        if (close(file6D) FAILS) {
            ErrExit("<Receiver Manager 1>\t close failed\n"); 
        }

        sleep(1);

        if (signal(SIGTERM, sigRHandler) == SIG_ERR) {
            ErrExit("<Receiver Manager 1>\t change signal handler failed\n");
        }
        pause();
    }

    // PIPE pipeForSizeR2 destructionTIme
    timef(timeIPCDesPSR2);

    //================================================================================
    // RECEIVER 2
    //================================================================================

    receiver2 = fork(); 

    if (receiver2 FAILS) {
        ErrExit("<Receiver Manager 2>\t Fork failed");
    }
    if (receiver2 == 0) {
        closeReadEndPipe(pipeForSendPidToR3);
        closeReadEndPipe(pipeForSizeR1);
        closeReadEndPipe(PIPE4);

        closeWriteEndPipe(PIPE3);
        closeWriteEndPipe(pipeForSizeR2);
        closeWriteEndPipe(pipeForSendPidToR2);
        
        int i = 0, pidR1, numberOfMessageToSendToR2 = 0, numberOfMessageToSendToR1 = 0, numberOfMessageReceived = 0, delayS2 = 0;

        receiver2Pid = getpid();

        // Read receiver1Pid from pipeForSendPidToR2
        rP1 = read(pipeForSendPidToR2[0], &pidR1, sizeof(pidR1));

        if (rP1 <= 0){
            ErrExit("<Receiver Manager 2>\t pipeForSendPidToR3 read failed\n"); 
        }   

        // Write receiver2Pid into pipeForSendPidToR3
        sizereceiver2Pid = sizeof(receiver2Pid);
        wP = write(pipeForSendPidToR3[1], &receiver2Pid, sizereceiver2Pid);  
    
        if (wP != sizereceiver2Pid) {
            ErrExit("<Receiver Manager 2>\t pipeForSendPidToR3 write failed\n");
        }

        if (kill(receiver2Pid, SIGSTOP) == -1) {
            ErrExit("<Receiver Manager 2>\t SIGSTOP in R2 failed\n");
        }
        printf("<Receiver Manager 2>\t Resume the execution\n");

        file5D = open(file5, O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR); 
        errorOpen(file5D);
        headerOnFile(filesHeader, file5D, filesTotalRead);

        if (write(file5D, &nextline, sizeof(char)) != sizeof(char)) {
            ErrExit("<Receiver Manager 2>\t F5.csv write failed \n");
        }

        /********* SIGUSER1 *********/
        if (signal(SIGUSR1, sigRHandler) == SIG_ERR) {
            ErrExit("<Receiver Manager 2>\t change signal handler failed\n");
        } 

        /********* SIGHUP *********/
        if (signal(SIGHUP, sigSHandler300) == SIG_ERR) {
            ErrExit("<Receiver Manager 3>\t change signal SIGHUP handler failed\n");
        }

        /********* SIGCONT *********/
        if (signal(SIGCONT, sigRHandler) == SIG_ERR) {
            ErrExit("<Receiver Manager 2>\t change signal SIGCONT handler failed\n");
        }

        /********* SIGUSR2 *********/
        if (signal(SIGUSR2, (void (*)(int))sigRHandler200) == SIG_ERR){
            ErrExit("<Receiver Manager 2>\t change signalHandler200 failed\n");
        }
        printf("<Receiver Manager 2>\t I'm ready to catch for a signal from someone...  \n");

        pause();   
        
        numberOfMessageToSendToR2 = sigRHandler200(FAKE_SIGNAL, numberOfMessageToSendToR2);

        /////////////////////////////////////////////////////////////////////// READ PIPE FROM R3

        // Read numberOfMessageToSendToR2 from pipeForSizeR2 
        ssize_t rB2 = read(pipeForSizeR2[0], &numberOfMessageToSendToR2, sizeof(numberOfMessageToSendToR2));
        if (rB2 <= 0){
            ErrExit("<Receiver Manager 2>\t read failed\n"); 
        }

        if (numberOfMessageToSendToR2 != 0) {
            // Read sender_traffic_five from PIPE3 
            ssize_t rB1 = read(PIPE3[0], &sender_traffic_five, sizeof(sender_traffic_five));
            if (rB1 <= 0){
                ErrExit("<Receiver Manager 2>\t read failed\n"); 
            }
        }

        for (int i = 0; i < numberOfMessageToSendToR2; i++) {

            if(*toModifyZero == 1) {
                delayS2 = 0;
            }
            else {
                delayS2 = sender_traffic_five[i].delayS2;
            }

            memcpy(sender_traffic_five[i].timeArrival, sender_traffic_five[i].timeDeparture, sizeof(sender_traffic_five[i].timeArrival));
            getFormattedTimewithDelay(sender_traffic_five, delayS2 + *pointerToAddDelay2, i); 
            
            sleep(delayS2);

            printStruct(file5D, sender_traffic_five, i);  
        }

        /////////////////////////////////////////////////////////////////////// SHARED MEMORY R2
        i = 0;
        
        while (i >= 0) {    
            SHpointer = (sender_traffic_struct *)shmAttach(idShReceiver, SHM_RDONLY);

            if (SHpointer < 0) {
                printf("<Receiver Manager 2>\t Unable to receive struct on the shared memory\n");
                break;
            } else if (SHpointer[i].id > 0 && SHpointer[i].id_receiver == 2) {

                memcpy(sender_traffic_five_sh, &SHpointer[i], sizeof(sender_traffic_struct));
               
                if(*toModifyZero == 1) {
                    delayS2 = 0;
                }
                else {
                    delayS2 = sender_traffic_five_sh[i].delayS2;
                }

                memcpy(sender_traffic_five_sh[i].timeArrival, sender_traffic_five_sh[i].timeDeparture, sizeof(sender_traffic_five_sh[i].timeArrival));
                getFormattedTimewithDelay(sender_traffic_five_sh, delayS2 + *pointerToAddDelay2, i); 

                printStruct(file5D, sender_traffic_five_sh, i);

                copyToSenderTrafficSH(sender_traffic_five, sender_traffic_five_sh, i, 0, numberOfMessageToSendToR2);  
                
                sleep(delayS2);

            } else {
                break;
            }
            i++;
        }
        shmDetach(SHpointer);

        if (i != 0){
            numberOfMessageReceived = i;
        }

        /////////////////////////////////////////////////////////////////////// MESSAGE QUEUE R2

        // Size of the message queue Q
        mSize = sizeof(sender_traffic_queue_struct) - sizeof(long);
        
        i = 0;
        // Reads from message queue Q the sender_traffic_queue
        while(i >= 0) {
            eM = msgrcv(idQueue, &sender_traffic_queue[i], mSize, 2, IPC_NOWAIT);
            
            if (eM < 0 && (errno != ENOMSG) && (errno != EINTR)) {
                printf("<Receiver Manager 2>\t Unable to receive message on queue\n");
                break;
            } else if(sender_traffic_queue[i].id > 0 && sender_traffic_queue[i].id_receiver == 2) {

                if(*toModifyZero == 1) {
                    delayS2 = 0;
                }
                else {
                    delayS2 = sender_traffic_queue[i].delayS2;
                }

                memcpy(sender_traffic_queue[i].timeArrival, sender_traffic_queue[i].timeDeparture, sizeof(sender_traffic_queue[i].timeArrival));
                getFormattedTimewithDelayMessageQueue(sender_traffic_queue, delayS2 + *pointerToAddDelay2, i); 
                printStructQueue(file5D, sender_traffic_queue, i); 

                i = numberOfMessageReceived; 
                copyToSenderTrafficQueue2(sender_traffic_queue, sender_traffic_five, i, numberOfMessageReceived, numberOfMessageToSendToR2);

                sleep(delayS2);
            } 
            else {
                break;
            }
            i++;
        }

        if (i != 0){
            numberOfMessageReceived = i;
        }
        
        if (close(file5D) FAILS) {
            ErrExit("<Receiver Manager 2>\t F5.csv close failed\n"); 
        }

        /////////////////////////////////////////////////////////////////////// WRITE PIPE TO SEND TO R1

        // Write sender_traffic_five into PIPE4
        for (int i = 0; i < numberOfMessageToSendToR2 + numberOfMessageReceived; i++){
            numberOfMessageToSendToR1++;

            sizeStructR1 = sizeof(sender_traffic_struct); 
            ssize_t wB = write(PIPE4[1], &sender_traffic_five[i], sizeStructR1);

            if (wB != sizeStructR1) {
                ErrExit("<Receiver Manager 2>\t PIPE4 write failed\n");
            }
        }

        // Sending numberOfMessageToSendToR1 with PIPE4 for process sizePipeR1
        sizePipeR1 = sizeof(numberOfMessageToSendToR1);
        ssize_t wB2 = write(pipeForSizeR1[1], &numberOfMessageToSendToR1, sizePipeR1); 

        if (wB2 != sizePipeR1) {
            ErrExit("<Receiver Manager 2>\t sizePipeR1 write failed\n");
        }
        printf("<Receiver Manager 2>\t sending SIGCONT to R1\n");

        // Resume the receiver1
        if (kill(pidR1, SIGCONT) == -1) {
            ErrExit("SIGCONT to R1 failed\n");
        }

        sleep(2);

        if (signal(SIGTERM, sigRHandler) == SIG_ERR) {
            ErrExit("<Receiver Manager 2>\t change signal handler failed\n");
        }
        pause();
    }

    // PIPE3 destructionTIme 
    timef(timeIPCDesP3);

    // PIPE pipeForSendPidToR2 destructionTIme 
    timef(timeIPCDesPToR2);

    // PIPE pipeForSendPidToR3 destructionTIme 
    timef(timeIPCDesPToR3);

    //================================================================================
    // RECEIVER 3
    //================================================================================

    receiver3 = fork(); 

    if (receiver3 FAILS) {
        ErrExit("<Receiver Manager 3>\t Fork failed\n");
    }

    if (receiver3 == 0) {
        closeReadEndPipe(PIPE3);
        closeReadEndPipe(pipeForSizeR2);
        closeWriteEndPipe(pipeForSendPidToR3);

        int i = 0, pidR2 = 0, numberOfMessage = 0, delayS3 = 0;
        
        receiver3Pid = getpid();

        rP = read(pipeForSendPidToR3[0], &pidR2, sizeof(pidR2));
        
        if (rP <= 0){
            ErrExit("<Receiver Manager 3>\t pipeForSendPidToR3 read failed\n"); 
        }

        // Sending signal for stopping itself
        if (kill(receiver3Pid, SIGSTOP) == -1) {
            ErrExit("<Receiver Manager 3>\t SIGSTOP in R3 failed\n");
        }
        printf("<Receiver Manager 3>\t Resume the execution\n"); 

        // Open FIFO in read-only mode
        FIFO_D = open(pathname, O_RDONLY); 
        if (FIFO_D FAILS) {
            ErrExit("<Receiver Manager 3>\t FIFO read failed\n");
        }
  
        // Open F4 and write the header with the next line
        file4D = open(file4, O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR); 
        errorOpen(file4D);
        headerOnFile(filesHeader, file4D, filesTotalRead);

        if (write(file4D, &nextline, sizeof(char)) != sizeof(char)) {
            ErrExit("<Receiver Manager 3>\t F4.csv write failed\n");
        }

        /////////////////////////////////////////////////////////////////////// FIFO FOR READING FROM S3

        /********* SIGUSR1 *********/
        if (signal(SIGUSR1, sigRHandler) == SIG_ERR) {
            ErrExit("<Receiver Manager 3>\t change signal handler failed\n");
        } 

        /********* SIGHUP *********/
        if (signal(SIGHUP, sigSHandler300) == SIG_ERR) {
            ErrExit("<Receiver Manager 3>\t change signal SIGHUP handler failed\n");
        }
        /********* SIGCONT *********/
        if (signal(SIGCONT, sigRHandler) == SIG_ERR) {
            ErrExit("<Receiver Manager 3>\t change signal SIGUSR1 handler failed\n");
        }

        /********* SIGUSR2 *********/
        if (signal(SIGUSR2, (void (*)(int))sigRHandler200) == SIG_ERR){
            ErrExit("<Receiver Manager 3>\t change signalHandler200 failed\ns");
        }
        printf("<Receiver Manager 3>\t I'm ready to catch for a signal from someone...  \n");

        pause();   
        
        numberOfMessage = sigRHandler200(FAKE_SIGNAL, numberOfMessage);

        // Waiting for struct for sender_manager
        printf("<Receiver Manager 3>\t waiting for struct sender_traffic_three... \n");

        i = 0;
        while (i >= 0) {
            bR = read(FIFO_D, &sender_traffic_four[i], sizeof(sender_traffic_four));

            if (bR FAILS) {
                printf("<Receiver Manager 3>\t FIFO is broken\n");
                break;
            } else if(sender_traffic_four[i].id_receiver == 3) { 
                
                if(*toModifyZero == 1) {
                    delayS3 = 0;
                }
                else {
                    delayS3 = sender_traffic_four[i].delayS3;
                }

                memcpy(sender_traffic_four[i].timeArrival, sender_traffic_four[i].timeDeparture, sizeof(sender_traffic_four[i].timeArrival));

                getFormattedTimewithDelay(sender_traffic_four, delayS3 + *pointerToAddDelay2, i); 
                
                sleep(delayS3);

                printStruct(file4D, sender_traffic_four, i);
            } else {
                break;
            }
            i++;
        }

        if (i != 0){
            numberOfMessage = i;
        }

        if (close(FIFO_D) != 0) {
            ErrExit("<Receiver Manager 3>\t FIFO close failed\n");
        }

        /////////////////////////////////////////////////////////////////////// SHARED MEMORY R3
        
        i = 0; 
        while (i >= 0) {    
            SHpointer = (sender_traffic_struct *)shmAttach(idShReceiver, SHM_RDONLY);
            
            if (SHpointer < 0) {
                printf("<Receiver Manager 3>\t Unable to receive struct on the shared memory\n");
            } else if (SHpointer[i].id > 0 && SHpointer[i].id_receiver == 3) {
                memcpy(sender_traffic_four_sh, &SHpointer[i], sizeof(sender_traffic_struct));

                if(*toModifyZero == 1) {
                    delayS3 = 0;
                }
                else {
                    delayS3 = sender_traffic_four_sh[i].delayS3;
                }

                memcpy(sender_traffic_four_sh[i].timeArrival, sender_traffic_four_sh[i].timeDeparture, sizeof(sender_traffic_four_sh[i].timeArrival));
                getFormattedTimewithDelay(sender_traffic_four_sh, delayS3 + *pointerToAddDelay2, i); 
                
                printStruct(file4D, sender_traffic_four_sh, i);

                i = numberOfMessage;    
                copyToSenderTrafficSH(sender_traffic_four, sender_traffic_four_sh, i, numberOfMessage, 0);  

                sleep(delayS3);
            } else {
                break;
            }
            i++;
        }
        shmDetach(SHpointer);

        if (i != 0){
            numberOfMessage = i;
        }

        /////////////////////////////////////////////////////////////////////// MESSAGE QUEUE R3

        // Size of the message queue
        mSize = sizeof(sender_traffic_queue_struct) - sizeof(long);
        
        i = 0;
        // Reads from message queue Q the sender_traffic_queue
        while (i >= 0) {
            eM = msgrcv(idQueue, &sender_traffic_queue[i], mSize, 3, IPC_NOWAIT);
            
            if (eM < 0 && (errno != ENOMSG) && (errno != EINTR)) {
                printf("<Receiver Manager 3>\t Unable to receive message on queue\n");
                break;
            } else if (sender_traffic_queue[i].id > 0 && sender_traffic_queue[i].id_receiver == 3) {
                
                if (*toModifyZero == 1) {
                    delayS3 = 0;
                }
                else {
                    delayS3 = sender_traffic_queue[i].delayS3;
                }

                memcpy(sender_traffic_queue[i].timeArrival, sender_traffic_queue[i].timeDeparture, sizeof(sender_traffic_queue[i].timeArrival));
                 
                getFormattedTimewithDelayMessageQueue(sender_traffic_queue, delayS3 + *pointerToAddDelay2, i);

                printStructQueue(file4D, sender_traffic_queue, i); 

                i = numberOfMessage; 
                copyToSenderTrafficQueue2(sender_traffic_queue, sender_traffic_four, i, numberOfMessage, 0);
                
                sleep(delayS3);
            } else {
                break;
            }
            i++;
        }
        
        if (i != 0){
            numberOfMessage = i;
        }

        // Sending sender_traffic_four by PIPE3 for process R2
        for (int i = 0; i < numberOfMessage; i++){
            numberOfMessageToSendToR2++;

            sizeStructR3 = sizeof(sender_traffic_struct); 
            ssize_t wB = write(PIPE3[1], &sender_traffic_four[i], sizeStructR3);
            
            if (wB != sizeStructR3) {
                ErrExit("<Receiver Manager 3>\t PIPE3 write failed\n");
            }
        }
    
        // Sending numberOfMessageToSendToR2 with PIPE3 for process pipeForSizeR2
        if (sender_traffic_four[0].id == -500) {
            numberOfMessageToSendToR2 = 0;
        }

        sizePipeR2 = sizeof(numberOfMessageToSendToR2);
        ssize_t wB2 = write(pipeForSizeR2[1], &numberOfMessageToSendToR2, sizePipeR2); 
    
        if (wB2 != sizePipeR2) {
            ErrExit("<Receiver Manager 3>\t sizePipeR3 write failed\n");
        }
       
        closeWriteEndPipe(PIPE3);
        closeWriteEndPipe(pipeForSizeR2);

        if (close(file4D) FAILS) {
            ErrExit("<Receiver Manager 3>\t F4.csv close failed\n"); 
        } 
        printf("<Receiver Manager 3>\t sending SIGCONT to R2\n");
        
        // Resume the receiver2
        if (kill(pidR2, SIGCONT) == -1) {
            ErrExit("<Receiver Manager 3>\t SIGCONT to R2 failed\n");
        }

        sleep(3);

        if (signal(SIGTERM, sigRHandler) == SIG_ERR) {
            ErrExit("<Receiver Manager 3>\t change signal handler failed\n");
        }

        pause();
    }
    
    // Write the Processes ID into F9.csv
    sprintf(bodyF9, "ReceiverID;PID\nR1;%d\nR2;%d\nR3;%d\n" + '\0', (int)receiver1, (int)receiver2, (int)receiver3);

    for (int i = 0; bodyF9[i] != '\0'; i++) {
        if (write(file9D, &bodyF9[i], sizeof(char)) != sizeof(char)) {
            ErrExit("<Receiver Manager>\t write failed\n");
        }
    }

    // GET set semaphore set of key_sm2
    semSet semId2 = semget(key_sm2, 3, IPC_CREAT | S_IRUSR | S_IWUSR);

    if (semId2 FAILS) {
        ErrExit("<Receiver Manager>\t Semget error\n");
    }

    argReceiver2.array = (semVal *)values2;

    // Obtain all semaphore values of semId2
    if (semctl(semId2, 0, GETALL, argReceiver2) == -1) {
        ErrExit("<Receiver Manager>\t semctl GETALL failed\n");
    }
    
    // Set the second semaphore to zero
    arg0.val = 0;
    if (semctl(semId2, 1, SETVAL, arg0) == -1) {
        ErrExit("<Receiver Manager>\t semctl GETALL failed\n");
    }

     // Set the third semaphore to one
    arg1.val = 1;
    if (semctl(semId2, 2, SETVAL, arg1) == -1) {
        ErrExit("<Receiver Manager>\t semctl GETALL failed\n");
    }

    // Wait receiver1 & receiver2 & receiver3
    while (wait(&status) != -1);

    // Remove the FIFO and set the destruction time
    removeFIFO(pathname);
    timef(timeIPCDesF); 

    // Remove the SH and set the destruction time
    removeSH(idShReceiver);
    timef(timeIPCDesSH);

    // Remove the Q and set the destruction time
    removeQ(idQueue);
    timef(timeIPCDesQ);

    sleep(5);
    // GET set semaphore set of key_sm
    semSet semId = semget(key_sm, 6, IPC_CREAT | S_IRUSR | S_IWUSR);

    if (semId FAILS) {
        ErrExit("<Receiver Manager>\t Semget error\n");
    }

    argReceiver.array = (semVal *)values;

    // Obtain all semaphore values of semId
    if (semctl(semId, 0, GETALL, argReceiver) == -1) {
        ErrExit("<Receiver Manager>\t Semctl GETALL failed\n");
    }

    // Lock the semaphore 4 to zero
    arg0.val = 0;

    if (semctl(semId, 4, SETVAL, arg0) FAILS) {
        ErrExit("<Receiver Manager>\t Semctl error\n");
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////// OPEN F10.CSV & DO SOMETHING

    file10D = open(file10, O_APPEND | O_RDWR);
    errorOpenWithoutCreate(file10D);
    
    updateIPC(IPCReceiver, numberOfIPC++, "PIPE4", *PIPE4, "RM", timeIPCP4 , timeIPCDesP4);
    updateIPC(IPCReceiver, numberOfIPC++, "PIPEFORSIZER1", *pipeForSizeR1, "RM", timeIPCPSR1 , timeIPCDesPSR1);
    updateIPC(IPCReceiver, numberOfIPC++, "PIPEFORSENDPIDTOR2", *pipeForSendPidToR2, "RM", timeIPCPToR2 , timeIPCDesPToR2);
    updateIPC(IPCReceiver, numberOfIPC++, "PIPE3", *PIPE3, "RM", timeIPCP3 , timeIPCDesP3);
    updateIPC(IPCReceiver, numberOfIPC++, "PIPEFORSIZER2", *pipeForSizeR2, "RM", timeIPCPSR2 , timeIPCDesPSR2);
    updateIPC(IPCReceiver, numberOfIPC++, "PIPEFORSENDPIDTOR3", *pipeForSendPidToR3, "RM", timeIPCPToR3 , timeIPCDesPToR3);
    updateIPC(IPCReceiver, numberOfIPC++, "FIFO", FIFO_D, "RM", timeIPCF, timeIPCDesF);

    for (int i = 0; i < numberOfIPC; i++) {
        printIPCStruct(file10D, IPCReceiver, i);
    }
    
    if (close(file10D) FAILS) {
        ErrExit("<Receiver Manager>\t Close failed"); 
    }

    file10D = open(file10, O_RDWR);
    errorOpenWithoutCreate(file10D);
   
    numberOfChar = totalCharInFile(file10D, buffer, 0, 0);
    numberOfField = numberOfFieldInFile(file10D, numberOfChar);

    searchAndReplaceOnFile(file10D, numberOfField + 1, "Q", timeIPCDesQ);
    searchAndReplaceOnFile(file10D, numberOfField + 1, "IDSHSENDER", timeIPCDesSH);

    //////////////////////////////////////////////////////////////////////////////////////////////////

    // Set the 5th semaphore to one
    arg1.val = 1;
    
    if (semctl(semId, 5, SETVAL, arg1) FAILS) {
        ErrExit("<Receiver Manager>\t Semctl error\n");
    }

    // Close F9.csv & receiverHeader.csv & filesHeader.csv
    if (close(file9D) FAILS || close(receiverHeader) FAILS || close(filesHeader) FAILS) {
        ErrExit("<Receiver Manager>\t Close failed\n"); 
    }
    
    return 0;
}