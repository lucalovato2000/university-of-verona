/// @file hackler.h
/// @brief Contiene la definizioni di variabili
///        e funzioni specifiche del hackler.

#pragma once

#define MAX_COL 4

typedef struct {
    int id;
    int delay;
    char *target;
    char *action;
} disturbing_actions; // disturbing_actions struct

void printHacklerTraffic(disturbing_actions *, int);
int checkInputValues(char *);
void populateStructFromHacklerFile(char *, disturbing_actions *, int);
void checkError(ssize_t);
void checkError(ssize_t);
void checkError(ssize_t);
void printHacklerTraffic(disturbing_actions *, int);
int checkInputValues(char *);
void populateStructFromHacklerFile(char *, disturbing_actions *, int);
void checkError(ssize_t);
void sigHandlerChild(int);
int numberOfSignal(disturbing_actions * , int ); 
void processSignal(char *, pid_list *, pid_list *, int);
int getCurrentPId(char *, pid_list, pid_list);
int getNumberFromTarget(char *);