/// @file hackler.c
/// @brief Contiene l'implementazione dell'hackler.
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <signal.h>
#include <ctype.h>
#include "semaphore.h"
#include "shared_memory.h"
#include "defines.h"
#include "hackler.h"
#include "err_exit.h"

// Actions
#define INCREASEDELAY 100
#define REMOVEMSG     200
#define SENDMSG       300
#define SHUTDOWN      400
#define CONT          500

pid_t pidHackler = 0;
pid_list pidSender, pidReceiver;

key_t key_sm = 01110011, key_sm2 = 11110011;

int main(int argc, char * argv[]) {

    sleep(15); 

    char *file7, *file8, *file9, *file10, buffer_read[SIZE], buffer[SIZE], timeIPCDesSem[20], timeIPCDesSem2[20];

    int file7D, file8D, file9D, file10D, max_char, rows = 0, nr = 0, tot = 0, index = 0, col = 5,
        totalCharF8, totalCharF9, numberOfChar = 0, numberOfField = 0, offset1 = 12, offset2 = 14, values[6], values2[3],
        rowsWithoutTerminateSignal = 0, S1 = 0, S2 = 0, S3 = 0, R3 = 0, R2 = 0, R1 = 0, all = 0, actionS1 = 0, actionS2 = 0, actionS3 = 0, 
        actionR3 = 0, actionR2 = 0, actionR1 = 0, delayS1 = 5, delayS2 = 10, delayS3 = 15, delayR3 = 20, delayR2 = 25, delayR1 = 30, delayAll = 0;

    ssize_t bR = 0;
    semun argHackler, argHackler2, arg0;
    sigset_t hacklerSet, hacklerPrevSet;

    file7 = "InputFiles/F7.csv";
    file8 = "OutputFiles/F8.csv";
    file9 = "OutputFiles/F9.csv";
    file10 = "OutputFiles/F10.csv";

    pidHackler = getpid();

    // Open the source file for only reading
    file7D = open(file7, O_RDONLY);
    errorOpenWithoutCreate(file7D);

    max_char = totalCharInFile(file7D, buffer_read, nr, tot);

    // Calculate the number of rows in F7.csv
    rows = numberOfRows(file7D, max_char);

    rowsWithoutTerminateSignal = rows - 1;
    
    disturbing_actions disturbing[rows];
 
    // Clean the struct if there are no lines in the file F7
    if (rows < 1) {
        disturbing->id = 0;
        disturbing->delay = 0;
        disturbing->target = NULL;
        disturbing->action =  NULL;
    }

    while (index < rows) {
        checkError(bR);
        populateStructFromHacklerFile(buffer_read, &disturbing[index], col);
        index++;
        col += 4;
    }

    semSet semId2 = semget(key_sm2, 3, IPC_CREAT | S_IRUSR | S_IWUSR);

    if (semId2 FAILS) {
        ErrExit("<Hackler>\t Semget error\n");
    }

    argHackler2.array = (semVal *)values2;

    // Obtain all sem values of semId2
    if (semctl(semId2, 0, GETALL, argHackler2) == -1) {
        ErrExit("<Hackler>\t Semctl GETALL failed\n");
    }

    // Set the second semaphore to zero
    arg0.val = 0;
    if (semctl(semId2, 2, SETVAL, arg0) == -1) {
        ErrExit("<Hackler>\t Semctl GETALL failed\n");
    }

    // Start to desturb the other process
    file8D = open(file8, O_RDWR); 
    totalCharF8 = totalCharInFile(file8D, buffer_read, nr, tot);
    pidSender = getPIDFromFile(file8D, totalCharF8, offset1);

    // Open the output file F9 for only reading
    file9D = open(file9, O_RDWR);  
    totalCharF9 = totalCharInFile(file9D, buffer_read, nr, tot);
    pidReceiver = getPIDFromFile(file9D, totalCharF9, offset2);

    // Initialize hacklerSet to contain all signals
    sigfillset(&hacklerSet);

    // Blocking the remaning signals
    sigprocmask(SIG_SETMASK, &hacklerSet, &hacklerPrevSet);  // commented for blocking ctrl+c signals

    // Unlock the process 3 of the receiver manager
    if (kill(pidReceiver.pid3, SIGCONT) FAILS) {
        ErrExit("<Hackler>\t SIGCONT to process Receiver 3 failed\n");
    }
    else {
        printf("%s<Hackler>\t\t Unlock process Receiver 3 %s\n", GREEN, RESET);
    }

    // Unlock the process 1 of the sender manager
    if (kill(pidSender.pid1, SIGCONT) FAILS) {
        ErrExit("<Hackler>\t\t SIGCONT to S1 failed");
    }
    else {
        printf("%s<Hackler>\t\t Unlock process Sender 1 %s\n", GREEN, RESET);
    }
  
    // Gets the signal and delay from attaure based on the target field
    for (int i = 0; i < rowsWithoutTerminateSignal; i++) {
        if (strcmp(disturbing[i].target, "S1") == 0) {
            S1 = 1;
            actionS1 = numberOfSignal(disturbing, i);
            delayS1 = disturbing[i].delay;
        }
        if (strcmp(disturbing[i].target, "S2") == 0) {
            S2 = 1;
            actionS2 = numberOfSignal(disturbing, i);
            delayS2 = disturbing[i].delay;
        }
        if (strcmp(disturbing[i].target, "S3") == 0) {
            S3 = 1;
            actionS3 = numberOfSignal(disturbing, i);
            delayS3 = disturbing[i].delay; 
        }
        if (strcmp(disturbing[i].target, "R3") == 0) {
            R3 = 1;
            actionR3 = numberOfSignal(disturbing, i);
            delayR3 = disturbing[i].delay; 
        }
        if (strcmp(disturbing[i].target, "R2") == 0) {
            R2 = 1;
            actionR2 = numberOfSignal(disturbing, i);
            delayR2 = disturbing[i].delay;
        }
        if (strcmp(disturbing[i].target, "R1") == 0) {
            R1 = 1;
            actionR1 = numberOfSignal(disturbing, i);
            delayR1 = disturbing[i].delay;
        }
        if (strcmp(disturbing[i].action, "all") == 0) {
            all = 1;
            delayAll = disturbing[i].delay;
        }
    }

    // For each process, it checks if it has at least one signal from the F7 input file and if so, executes it. 
    // If it does not have a signal given by F7, then the process continues. 
    // The SIGCONT signal is sent to it
    if (S1 == 1) {
        sleep(delayS1);

        if (actionS1 == INCREASEDELAY) {
            kill(pidSender.pid1, SIGUSR1);
        }
        if (actionS1 == SENDMSG) {
            kill(pidSender.pid1, SIGHUP);
        }
        if (actionS1 == REMOVEMSG) {
            kill(pidSender.pid1, SIGUSR2);
        }
    }
    else {
        sleep(delayS1);
        kill(pidSender.pid1, SIGCONT);
    }

    if (S2 == 1) {
        sleep(delayS2);
        sleep(10);

        if (actionS2 == INCREASEDELAY) {
            kill(pidSender.pid2, SIGUSR1);
        }
        if (actionS2 == SENDMSG) {
            kill(pidSender.pid2, SIGHUP);
        }
        if (actionS2 == REMOVEMSG) {
            kill(pidSender.pid2, SIGUSR2);
        }
    }
    else {
        sleep(delayS2);
        kill(pidSender.pid2, SIGCONT);
    }

    if (S3 == 1) {
        sleep(delayS3);
        sleep(15);

        if (actionS3 == INCREASEDELAY) {
            kill(pidSender.pid3, SIGUSR1);
        }
        if (actionS3 == SENDMSG) {
            kill(pidSender.pid3, SIGHUP);
        }
        if (actionS3 == REMOVEMSG) {
            kill(pidSender.pid3, SIGUSR2);
        }
    }
    else {
        sleep(delayS3);
        kill(pidSender.pid3, SIGCONT);
    }

    if (R3 == 1) {
        sleep(delayS1);
        sleep(20);

        if (actionR3 == INCREASEDELAY) {
            kill(pidReceiver.pid3, SIGUSR1);
        }
        if (actionR3 == SENDMSG)  {
            kill(pidReceiver.pid3, SIGHUP);
        }
        if (actionR3 == REMOVEMSG) {
            kill(pidReceiver.pid3, SIGUSR2);
        }
    }
    else {
        sleep(delayR3);
        kill(pidReceiver.pid3, SIGCONT);
    }

    if (R2 == 1) {
        sleep(delayR2);
        sleep(25);

        if (actionR2 == INCREASEDELAY) {
            kill(pidReceiver.pid2, SIGUSR1);
        }
        if (actionR2 == SENDMSG) {
            kill(pidReceiver.pid2, SIGHUP);
        }
        if (actionR2 == REMOVEMSG) {
            kill(pidReceiver.pid2, SIGUSR2);
        }
    }
    else {
        sleep(delayR2);
        kill(pidReceiver.pid2, SIGCONT);
    }

    if (R1 == 1) {
        sleep(delayR1);
        sleep(30);

        if (actionR1 == INCREASEDELAY) {
            kill(pidReceiver.pid1, SIGUSR1);
        }
        if (actionR1 == SENDMSG) {
            kill(pidReceiver.pid1, SIGHUP);
        }
        if (actionR1 == REMOVEMSG) {
            kill(pidReceiver.pid1, SIGUSR2);
        }
    }
    else {
        sleep(delayR1);
        kill(pidReceiver.pid1, SIGCONT);
    }

    if (all) {
        sleep(delayAll);
        
        kill(pidSender.pid1, SIGTERM);
        kill(pidSender.pid2, SIGTERM);
        kill(pidSender.pid3, SIGTERM);

        kill(pidReceiver.pid1, SIGTERM);
        kill(pidReceiver.pid2, SIGTERM);
        kill(pidReceiver.pid3, SIGTERM);
    }

    if (close(file7D) FAILS) {
        ErrExit("<Hackler>\t File F7 close failed\n"); 
    }

    sleep(20);
    
    kill(pidSender.pid1, SIGTERM);
    kill(pidSender.pid2, SIGTERM);
    kill(pidSender.pid3, SIGTERM);

    kill(pidReceiver.pid1, SIGTERM);
    kill(pidReceiver.pid2, SIGTERM);
    kill(pidReceiver.pid3, SIGTERM);

    //////////////////////////////////////////////////////////////// SET SEMAPHORES
  
    sleep(10); // synchronize the semaphores for writing into F10.csv

    semSet semId = semget(key_sm, 6, IPC_CREAT | S_IRUSR | S_IWUSR);

    if (semId FAILS) {
        ErrExit("<Hackler>\t Semget error\n");
    }

    argHackler.array = (semVal *)values;

    if (semctl(semId, 0, GETALL, argHackler) == -1) {
        ErrExit("<Hackler>\t Semctl GETALL failed\n");
    }
    
    // Set the 5th semaphore to zero
    arg0.val = 0;

    if (semctl(semId, 5, SETVAL, arg0) == -1) {
        ErrExit("<Hackler>\t Semctl GETALL failed\n");
    }

    // Remove the semaphore set & take the destruction time
    if (semctl(semId2, 0, IPC_RMID, 0) == -1) {
        ErrExit("<Hackler>\t Semctl IPC_RMID failed\n");
    }

    timef(timeIPCDesSem2);

    // Remove the semaphore set & take the destruction time
    if (semctl(semId, 0, IPC_RMID, 0) == -1) {
        ErrExit("<Hackler>\t Semctl IPC_RMID failed\n");
    }

    timef(timeIPCDesSem);

    /////////////////////////////////////////////////////////////////////// OPEN F10.CSV & DO SOMETHING

    // Open the source file for only reading
    file10D = open(file10, O_RDWR);
    errorOpenWithoutCreate(file10D);

    numberOfChar = totalCharInFile(file10D, buffer, nr, tot);
    numberOfField = numberOfFieldInFile(file10D, numberOfChar);

    searchAndReplaceOnFile(file10D, numberOfField + 1, "SEMID", timeIPCDesSem);
    searchAndReplaceOnFile(file10D, numberOfField + 1, "SEMID2", timeIPCDesSem2);
   
    sleep(2);

    kill(pidHackler, SIGTERM); // To be tested
    
    return 0;  // ... not so useful
}

// Send the signal to the target
void processSignal(char * target, pid_list * pidReceiver, pid_list * pidSender, int sig) {
    int pid = getCurrentPId(target, *pidReceiver, *pidSender);

    kill(pid, sig);
    printf("\x1b[30m\x1b[47m<Hackler>\t Send signal to %d\x1b[0m\n", pid);
}

//  Get the number of target
int getNumberFromTarget(char * target) {
    if (strcmp(target, "S1") == 0) {
        return 1;
    } 
    if (strcmp(target, "S2") == 0) {
        return 2;
    } 
    if (strcmp(target, "S3") == 0) {
        return 3;
    }
    if (strcmp(target, "R3") == 0) {
        return 4;
    }
    if (strcmp(target, "R2") == 0) {
        return 5;
    } 
    if (strcmp(target, "R1") == 0) {
        return 6;
    }
    else{
        return 0;
    }
}

// Return the current pid of the target
int getCurrentPId(char * target, pid_list pidReceiver, pid_list pidSender) {
    if (strcmp(target, "S1") == 0) {
        return pidSender.pid1;
    } 
    if (strcmp(target, "S2") == 0) {
        sleep(8);
        return pidSender.pid2;
    } 
    if (strcmp(target, "S3") == 0) {
        sleep(10);
        return pidSender.pid3;
    }
    if (strcmp(target, "R3") == 0) {
        sleep(15);
        return pidReceiver.pid3;
    }
    if (strcmp(target, "R2") == 0) {
        sleep(20);
        return pidReceiver.pid2;
    } 
    if (strcmp(target, "R1") == 0) {
        sleep(25);
        return pidReceiver.pid1;
    }
    return 0;
}

// Print the struct in the terminal
void printHacklerTraffic(disturbing_actions * y, int position) {
    printf(
        "%d, %d, %s, %s,\n", 
        y[position].id, 
        y[position].delay, 
        y[position].target, 
        y[position].action
    );
}

// Check the input values. 
// Return true if the first char is a character and the second char is a integer.
int checkInputValues(char * read) {
    char target_second_position;
    target_second_position = read[1];
    return (read[0] == 'S' || read[0] == 'R') && isdigit(target_second_position) != 0;
}

// Populate the hackler struct after reading from file
void populateStructFromHacklerFile(char * buffer_read, disturbing_actions * disturbing, int col) {
    int hasTarget = 1;
    char *tmp, *lettura;

    // Cycle for the columns
    for (int counter = 1; counter <= MAX_COL; counter++) {
        if (hasTarget) {
            tmp = strdup(buffer_read);
            lettura = strdup(getfield(tmp, col));
        }

        switch (counter) {
            case 1:
                disturbing->id = atoi(lettura); 
                break;
            case 2:
                disturbing->delay = atoi(lettura);
                break;
            case 3:
                // If the fisrt char it's a character and the second char it's a integer...
                if (checkInputValues(lettura)) {
                    disturbing->target = lettura; 
                } else {
                    disturbing->target = NULL;
                    disturbing->action = lettura;
                    hasTarget = 0;
                }
                break;
            case 4:
                if (hasTarget) {
                    disturbing->action = lettura;
                }
                break;
        }
        col++;
    }
}

// Error open file
void checkError(ssize_t bR) {
    if (bR FAILS) {
        ErrExit("<Hackler>\t Read failed \n"); 
    }
}

// Get the number of signal in disturbing struct
int numberOfSignal(disturbing_actions * disturbing, int i) {
    if (strcmp(disturbing[i].action, "IncreaseDelay") == 0) {
        return INCREASEDELAY;
    }
    if (strcmp(disturbing[i].action, "RemoveMsg") == 0 || strcmp(disturbing[i].action, "RemoveMSG") == 0) {
        return REMOVEMSG;
    }
    if (strcmp(disturbing[i].action, "SendMsg") == 0 || strcmp(disturbing[i].action, "SendMSG") == 0) {
        return SENDMSG;
    }
    if (strcmp(disturbing[i].action, "all") == 0 || strcmp(disturbing[i].action, "ALL") == 0) {
        return SHUTDOWN;
    }
    else {
        return CONT;
    }
}