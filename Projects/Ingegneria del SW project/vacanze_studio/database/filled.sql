-- phpMyAdmin SQL Dump
-- version 4.9.7deb1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 28, 2021 at 03:25 PM
-- Server version: 8.0.26-0ubuntu0.21.04.3
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `study_trip`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_college`
--

CREATE TABLE `activity_college` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `id_college` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `activity_college`
--

INSERT INTO `activity_college` (`id`, `name`, `description`, `id_college`) VALUES
(1, 'Aperitivo linguistico', 'Lo scopo dell\'aperitivo è quello di conoscere nuova gente e dialogare in una lingua straniera.', 1),
(2, 'Corsa', 'Ogni giorno alle 18 con partenza dalla piazza principale si corre tutti assieme!', 9);

-- --------------------------------------------------------

--
-- Table structure for table `allergy`
--

CREATE TABLE `allergy` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `id_student` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `allergy`
--

INSERT INTO `allergy` (`id`, `name`, `description`, `id_student`) VALUES
(1, 'Polline', 'Antistaminico', '1'),
(2, 'Mandorle', 'Antistaminico', '2');

-- --------------------------------------------------------

--
-- Table structure for table `certificate`
--

CREATE TABLE `certificate` (
  `id` int NOT NULL,
  `level` varchar(255) NOT NULL,
  `language` varchar(255) NOT NULL,
  `id_student` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `certificate`
--

INSERT INTO `certificate` (`id`, `level`, `language`, `id_student`) VALUES
(1, 'B1', 'Tedesco', 1),
(2, 'B2', 'Tedesco', 2);

-- --------------------------------------------------------

--
-- Table structure for table `college`
--

CREATE TABLE `college` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `college`
--

INSERT INTO `college` (`id`, `name`, `address`) VALUES
(1, 'Europe', 'Rimbachstr. 19b 98527 Suhl Germania'),
(2, 'The Queen', '8 Gainsborough Rd, Bushwood, London E11 1HT, UK'),
(3, 'Belvedere', 'Via Rosmini 43b, Roma'),
(4, 'Városrendezési', 'Taban, Budapest, 1013 Ungheria'),
(5, 'Université libre de Bruxelles', 'Av. Franklin Roosevelt 50, 1050 Bruxelles, Belgio');

-- --------------------------------------------------------

--
-- Table structure for table `hobby`
--

CREATE TABLE `hobby` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `id_student` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `hobby`
--

INSERT INTO `hobby` (`id`, `name`, `id_student`) VALUES
(1, 'Escursionismo', 1),
(2, 'Pianoforte', 2);

-- --------------------------------------------------------

--
-- Table structure for table `holiday`
--

CREATE TABLE `holiday` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `departure_date` date NOT NULL,
  `duration` int NOT NULL,
  `destination` varchar(255) NOT NULL,
  `language` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `holiday`
--

INSERT INTO `holiday` (`id`, `name`, `departure_date`, `duration`, `destination`, `language`) VALUES
(1, 'Viaggio culturale a Roma', '2021-10-24', 4, 'Roma (Italia)', 'Italiano'),
(2, 'Settimana a Londra', '2021-11-16', 7, 'Londra (Inghilterra)', 'Inglese'),
(3, 'Capodanno in Germania', '2021-12-31', 3, 'Berlino (Germania)', 'Tedesco'),
(4, 'Un mese a Budapest', '2021-11-01', 31, 'Budapest (Ungheria)', 'Ungherese'),
(5, 'Dicembre in Belgio', '2021-12-01', 31, 'Brugge (Belgio)', 'Inglese');

-- --------------------------------------------------------

--
-- Table structure for table `holiday_family`
--

CREATE TABLE `holiday_family` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `n_of_rooms` int NOT NULL,
  `n_of_bathrooms` int NOT NULL,
  `animals` int NOT NULL,
  `family_members` int NOT NULL,
  `distance_from_city_center` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `holiday_family`
--

INSERT INTO `holiday_family` (`id`, `name`, `surname`, `n_of_rooms`, `n_of_bathrooms`, `animals`, `family_members`, `distance_from_city_center`) VALUES
(1, 'Franz', 'Gruber', 4, 2, 3, 0, 8),
(2, 'Roberto', 'Frinniti', 1, 1, 2, 1, 9),
(3, 'Rob', 'Jannes', 3, 1, 0, 2, 1),
(4, 'Mary', 'Goodman', 1, 1, 1, 2, 8);

-- --------------------------------------------------------

--
-- Table structure for table `parent`
--

CREATE TABLE `parent` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `id_student` int NOT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone_number` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `parent`
--

INSERT INTO `parent` (`id`, `name`, `id_student`, `surname`, `email`, `phone_number`) VALUES
(1, 'Enrico', 1, 'Rossi', 'enrico.rossi@gmail.com', '329788431'),
(2, 'Guido', 2, 'Righetti', 'g.righetti@gmail.com', '321866743');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int NOT NULL,
  `id_reservation` int NOT NULL,
  `method` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `id_reservation`, `method`) VALUES
(1, 3, 'Credit card'),
(2, 2, 'Credit card');

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE `reservation` (
  `id` int NOT NULL,
  `accomodation_preference` varchar(255) NOT NULL DEFAULT 'NULL',
  `room_type` varchar(255) NOT NULL DEFAULT 'NULL',
  `friend_name` varchar(255) NOT NULL DEFAULT 'NULL',
  `friend_surname` varchar(255) NOT NULL DEFAULT 'NULL',
  `friend_email` varchar(255) NOT NULL DEFAULT 'NULL',
  `confirmed` varchar(255) NOT NULL DEFAULT 'NULL',
  `id_holiday` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `reservation`
--

INSERT INTO `reservation` (`id`, `accomodation_preference`, `room_type`, `friend_name`, `friend_surname`, `friend_email`, `confirmed`, `id_holiday`) VALUES
(1, 'College', 'Single', 'NULL', 'NULL', 'NULL', 'NULL', 3),
(2, 'College', 'Single', 'NULL', 'NULL', 'NULL', 'NULL', 2);

-- --------------------------------------------------------

--
-- Table structure for table `responsible`
--

CREATE TABLE `responsible` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `address` varchar(225) DEFAULT NULL,
  `crypted_password` varchar(255) NOT NULL,
  `username` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `responsible`
--

INSERT INTO `responsible` (`id`, `name`, `surname`, `email`, `phone_number`, `address`, `crypted_password`, `username`) VALUES
(1, 'Massimo', 'Bianchi', 'massimo.bianchi@gmail.com', '3296554188', 'Via Roma 12, Milano', '38729edbdf329c5493c8dd3ab3d2de509418f9e17486916ce6982fde0dbdd26b', 'maxbianchi');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `crypted_password` varchar(255) NOT NULL,
  `username` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `name`, `surname`, `address`, `phone_number`, `email`, `crypted_password`, `username`) VALUES
(1, 'Stefano', 'Rossi', 'Via Milano 41, Roma', '3298447611', 'stefano.rossi@gmail.com', 'b2c29a32b96eb6d21110ec7d2f739acc5d2adae8fb989e4725c6e81767887aa3', 'stefanorossi'),
(2, 'Mario', 'Righetti', 'Via Parma 43, Bolzano', '3219887455', 'mario.righetti@gmail.com', '841520a619127595315949654da13768299533fb219493b30e800c7747dc058c', 'mariorighetti');

-- --------------------------------------------------------

--
-- Table structure for table `survey`
--

CREATE TABLE `survey` (
  `id` int NOT NULL,
  `id_student` int NOT NULL,
  `id_holiday` int NOT NULL,
  `grade` int NOT NULL,
  `comments` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `survey`
--

INSERT INTO `survey` (`id`, `id_student`, `id_holiday`, `grade`, `comments`) VALUES
(1, 1, 1, 9, 'Esperienza stupenda!'),
(2, 2, 1, 8, 'Mi resterà per sempre un bel ricordo di questa esperienza.');

-- --------------------------------------------------------

--
-- Table structure for table `trip`
--

CREATE TABLE `trip` (
  `id` int NOT NULL,
  `destination` varchar(255) NOT NULL,
  `cost` int NOT NULL,
  `hours` int NOT NULL,
  `description` varchar(255) NOT NULL,
  `id_holiday` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `trip`
--

INSERT INTO `trip` (`id`, `destination`, `cost`, `hours`, `description`, `id_holiday`) VALUES
(1, 'Colosseo', 12, 2, 'Visita guidata', 1),
(2, 'Brugges', 16, 3, 'Visita guidata al centro storico', 5),
(3, 'Monumento ai caduti', 6, 1, 'Visita al monumento ai caduti in guerra', 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_college`
--
ALTER TABLE `activity_college`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `allergy`
--
ALTER TABLE `allergy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `certificate`
--
ALTER TABLE `certificate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `college`
--
ALTER TABLE `college`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobby`
--
ALTER TABLE `hobby`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `holiday`
--
ALTER TABLE `holiday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `holiday_family`
--
ALTER TABLE `holiday_family`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parent`
--
ALTER TABLE `parent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `responsible`
--
ALTER TABLE `responsible`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `survey`
--
ALTER TABLE `survey`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trip`
--
ALTER TABLE `trip`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_college`
--
ALTER TABLE `activity_college`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `allergy`
--
ALTER TABLE `allergy`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `certificate`
--
ALTER TABLE `certificate`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `college`
--
ALTER TABLE `college`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `hobby`
--
ALTER TABLE `hobby`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `holiday`
--
ALTER TABLE `holiday`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `holiday_family`
--
ALTER TABLE `holiday_family`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `parent`
--
ALTER TABLE `parent`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `responsible`
--
ALTER TABLE `responsible`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `survey`
--
ALTER TABLE `survey`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `trip`
--
ALTER TABLE `trip`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
