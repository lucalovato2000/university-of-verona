package application.model;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * This class implements the singleton pattern for database connection
 */

public class DBConnect {

	Connection databaseLink;

	// Create the instance DBConnect
	private static DBConnect instance = new DBConnect();

	private DBConnect() {
	}

	public Connection getConnection() {
		String DBName = "study_trip";
		String DBUser = "root";
		String DBPass = "root";
		String url = "jdbc:mysql://localhost/" + DBName + "?enabledTLSProtocols=TLSv1.2";

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			databaseLink = DriverManager.getConnection(url, DBUser, DBPass);
		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
		return databaseLink;
	}

	/**
	 * This method create an object while making sure that only single object gets
	 * created
	 * 
	 * @return the only instance
	 */
	public static DBConnect getInstance() {
		return instance;
	}
}
