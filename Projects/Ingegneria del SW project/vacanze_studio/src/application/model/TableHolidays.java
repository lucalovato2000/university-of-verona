package application.model;

import java.sql.Date;

public class TableHolidays {

	Integer duration;
	Date departure_date;
	String id, name, destination, language;

	public TableHolidays(String id, String name, Date departure_date, Integer duration, String destination,
			String language) {

		this.id = id;
		this.name = name;
		this.departure_date = departure_date;
		this.duration = duration;
		this.destination = destination;
		this.language = language;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDeparture_date() {
		return departure_date;
	}

	public void setDeparture_date(Date departure_date) {
		this.departure_date = departure_date;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
}
