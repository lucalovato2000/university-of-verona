package application.control;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Statement;

import application.Main;
import application.model.DBConnect;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ControllerRegistrationAdmin {

	@FXML
	private Button registerMeButton;
	@FXML
	private Button resetButton;
	@FXML
	private TextField firstnameTextField;
	@FXML
	private TextField lastnameTextField;
	@FXML
	private TextField emailTextField;
	@FXML
	private TextField addressTextField;
	@FXML
	private TextField phoneNumberTextField;
	@FXML
	private TextField usernameTextField;
	@FXML
	private PasswordField passwordTextField;
	@FXML
	private PasswordField confirmPasswordTextField;
	@FXML
	private Label registrationMessageLabel;
	@FXML
	private Label confirmPasswordLabel;
	@FXML
	private Label asteriskfirstnameLabel;
	@FXML
	private Label asterisklastnameLabel;
	@FXML
	private Label asteriskemailLabel;
	@FXML
	private Label asteriskaddressLabel;
	@FXML
	private Label asteriskphonenumberLabel;
	@FXML
	private Label asteriskusernameLabel;
	@FXML
	private Label asteriskpasswordLabel;
	@FXML
	private Label asteriskconfirmpasswordLabel;
	@FXML
	private Label requiredfieldLabel;
	@FXML
	private Label confirmphonenumberLabel;

	public ControllerRegistrationAdmin() {
	}

	/**
	 * Return the current stage
	 */
	public void createAdminAccountForm() {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/application/view/registration_admin.fxml"));

			Stage registerStage = new Stage();

			registerStage.initStyle(StageStyle.UNDECORATED);
			registerStage.setScene(new Scene(root, 536, 549));
			registerStage.show();

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
	}

	/**
	 * Reset the textField in the scene
	 *
	 */
	public void resetButtonOnAction() {
		firstnameTextField.setText("");
		lastnameTextField.setText("");
		emailTextField.setText("");
		phoneNumberTextField.setText("");
		addressTextField.setText("");
		usernameTextField.setText("");
		passwordTextField.setText("");
		confirmPasswordTextField.setText("");
	}

	/**
	 * Check if the values in the textfield are valid
	 * 
	 * @param event return to the opening scene
	 * @throws NoSuchAlgorithmException
	 */
	public void registerMeButtonOnAction(ActionEvent event) throws NoSuchAlgorithmException {

		registrationMessageLabel.setText("");

		if (requiredFieldFirstName() == false && requiredFieldLastName() == false && requiredFieldEmail() == false
				&& requestFiledAddress() == false && requestFiledNumber() == false && requestFiledUserName() == false
				&& requestFiledPassword() == false && requestFiledConfirmPassword() == false) {

			if (isNumeric(phoneNumberTextField.getText().trim()) == true) {
				confirmphonenumberLabel.setText("");

				if (passwordTextField.getText().equals(confirmPasswordTextField.getText())) {
					registrationAdmin();
					confirmPasswordLabel.setText("Password \nmatch correctly!");
				} else {
					confirmPasswordLabel.setText("Password \ndoes not match!");
					registrationMessageLabel.setText("");
				}
			} else {
				confirmphonenumberLabel.setText("Please enter only numbers");
			}
		}
	}

	/**
	 * Return to the homepage
	 * 
	 * @param event return to the opening scene
	 * @throws IOException
	 */
	public void homeButtonOnAction(ActionEvent event) throws IOException {
		Main homepage = new Main();
		homepage.changeSceneWithDimension("/application/view/admin_or_user.fxml", 536, 560);
	}

	/**
	 * Add a new responsible in the mysql database
	 * 
	 * @thows NoSuchAlgorithmException
	 */
	public void registrationAdmin() throws NoSuchAlgorithmException {

		DBConnect connectNow = DBConnect.getInstance();
		Connection connection = connectNow.getConnection();

		String firstname = firstnameTextField.getText();
		String lastname = lastnameTextField.getText();
		String email = emailTextField.getText();
		String phonenumber = phoneNumberTextField.getText();
		String address = addressTextField.getText();
		String username = usernameTextField.getText();
		String password = passwordTextField.getText();

		if (firstnameTextField.getText() == null || firstnameTextField.getText().trim().isEmpty()) {
			firstname = null;
		}

		if (lastnameTextField.getText() == null || lastnameTextField.getText().trim().isEmpty()) {
			lastname = null;
		}

		if (emailTextField.getText() == null || emailTextField.getText().trim().isEmpty()) {
			email = null;
		}

		if (phoneNumberTextField.getText() == null || phoneNumberTextField.getText().trim().isEmpty()) {
			phonenumber = null;
		}

		if (addressTextField.getText() == null || addressTextField.getText().trim().isEmpty()) {
			address = null;
		}

		if (usernameTextField.getText() == null || usernameTextField.getText().trim().isEmpty()) {
			username = null;
		}

		if (passwordTextField.getText() == null || passwordTextField.getText().trim().isEmpty()) {
			password = null;
		}

		// Password encryption
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(password.getBytes(StandardCharsets.UTF_8));
		byte[] digest = md.digest();
		String cryptedPassword = String.format("%064x", new BigInteger(1, digest));

		// Insert into mysql database the responsible
		String insertFields = " INSERT INTO responsible(name, surname, email, phone_number, address, username, crypted_password) VALUES ('";
		String insertValues = firstname + "','" + lastname + "','" + email + "','" + phonenumber + "','" + address
				+ "','" + username + "','" + cryptedPassword + "')";

		String insertToRegister = insertFields + insertValues;

		try {
			Statement statement = connection.createStatement();
			statement.executeUpdate(insertToRegister);

			registrationMessageLabel.setText("Admin has been registred successfully!");
		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
	}

	/**
	 * This method checks if the firstname field is empty
	 * 
	 * @return true if field is empty
	 * @return false if field is not empty
	 */
	public boolean requiredFieldFirstName() {
		if (firstnameTextField.getText().trim().isEmpty()) {
			asteriskfirstnameLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskfirstnameLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the lastname field is empty
	 * 
	 * @return true if field is empty
	 * @return false if field is not empty
	 */
	public boolean requiredFieldLastName() {
		if (lastnameTextField.getText().trim().isEmpty()) {
			asterisklastnameLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asterisklastnameLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the email field is empty
	 * 
	 * @return true if field is empty
	 * @return false if field is not empty
	 */
	public boolean requiredFieldEmail() {
		if (emailTextField.getText().trim().isEmpty()) {
			asteriskemailLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskemailLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if address field is empty
	 * 
	 * @return true if field is empty
	 * @return false if field is not empty
	 */
	public boolean requestFiledAddress() {
		if (addressTextField.getText().trim().isEmpty()) {
			asteriskaddressLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskaddressLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the phone_number field is empty
	 * 
	 * @return true if field is empty
	 * @return false if field is not empty
	 */
	public boolean requestFiledNumber() {
		if (phoneNumberTextField.getText().trim().isEmpty()) {
			asteriskphonenumberLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskphonenumberLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the username field is empty
	 * 
	 * @return true if field is empty
	 * @return false if field is not empty
	 */
	public boolean requestFiledUserName() {
		if (usernameTextField.getText().trim().isEmpty()) {
			asteriskusernameLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskusernameLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the password field is empty
	 * 
	 * @return true if field is empty
	 * @return false if field is not empty
	 */
	public boolean requestFiledPassword() {
		if (passwordTextField.getText().trim().isEmpty()) {
			asteriskpasswordLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskpasswordLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the confirm password field is empty
	 * 
	 * @return true if field is empty
	 * @return false if field is not empty
	 */
	public boolean requestFiledConfirmPassword() {
		if (confirmPasswordTextField.getText().trim().isEmpty()) {
			asteriskconfirmpasswordLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskconfirmpasswordLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the string is only numbers
	 * 
	 * @param str
	 * @return true if only number
	 */
	public static boolean isNumeric(String str) {
		try {
			Long.parseLong(str);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
}
