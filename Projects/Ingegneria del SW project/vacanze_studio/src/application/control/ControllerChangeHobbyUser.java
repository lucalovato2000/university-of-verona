package application.control;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ResourceBundle;

import application.Main;
import application.model.DBConnect;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ControllerChangeHobbyUser extends ControllerUserReservedArea {

	@FXML
	private TextField searchusernameTextField;
	@FXML
	private Label nowUsernameLabel;
	@FXML
	private Label requiredfieldLabel;
	@FXML
	private TableView<TableViewHobby> HobbyTableView;
	@FXML
	private TableColumn<TableViewHobby, String> hobbyTableView;
	@FXML
	private TableColumn<TableViewHobby, String> idHobbyTableView;

	ObservableList<TableViewHobby> oblistTableViewHobby = FXCollections.observableArrayList();

	@FXML
	private TextField hobbyTextField;
	@FXML
	private TextField idHobby;

	@FXML
	private Button viewButton;
	@FXML
	private Button showData;
	@FXML
	private Button deleteButton;

	public void createAllergiesAndParentForm() {
		try {

			Parent root = FXMLLoader.load(getClass().getResource("/application/view/change_hobby_user.fxml"));

			Stage changeAllergyHobby = new Stage();

			changeAllergyHobby.initStyle(StageStyle.UNDECORATED);
			changeAllergyHobby.setScene(new Scene(root, 536, 560));
			changeAllergyHobby.show();

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
	}

	public ControllerChangeHobbyUser() {
		super();
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		idHobbyTableView.setCellValueFactory(new PropertyValueFactory<>("id"));
		hobbyTableView.setCellValueFactory(new PropertyValueFactory<>("hobby"));
		HobbyTableView.setItems(oblistTableViewHobby);

	}

	/**
	 * This method reveals whether the field has been clicked
	 * 
	 * @param event click with mouse the field
	 */
	public void searchusernameTextFieldOnAction(MouseEvent event) {
		if (searchusernameTextField.getText().isEmpty()) {
			TextField(searchusernameTextField, nowUsernameLabel, requiredfieldLabel);
		} else {
			requiredfieldLabel.setText("");
			nowUsernameLabel.setText("");
		}
	}

	/**
	 * This method allows you to visualize current allergies and hobbies
	 * 
	 * @param event
	 */
	public void viewButtonOnAction(ActionEvent event) {
		String idUser, hobbyQuery = null;
		if (requestTextField(searchusernameTextField, nowUsernameLabel, requiredfieldLabel) == false) {

			if (userExists(searchusernameTextField.getText()) == false) {

				requiredfieldLabel.setVisible(true);
				requiredfieldLabel.setText("Invalid user!");
				nowUsernameLabel.setText("*");

			} else {

				idUser = searchIntoDataBase("id", "student", "username", searchusernameTextField.getText());

				DBConnect connectNow = DBConnect.getInstance();
				Connection connectDB = connectNow.getConnection();

				String queryHobby = "SELECT * FROM hobby WHERE id_student = " + idUser;

				String hobby = null;
				String idHobby = null;
				try {
					Statement statement = connectDB.createStatement();
					ResultSet resultRecord = statement.executeQuery(queryHobby);

					while (resultRecord.next()) {
						idHobby = resultRecord.getString("id");
						hobby = resultRecord.getString("name");

						hobbyQuery += hobby + "|";
						hobbyQuery = hobbyQuery.substring(2);
						oblistTableViewHobby.add(new TableViewHobby(idHobby, hobby));
					}

				} catch (Exception e) {
					e.printStackTrace();
					e.getCause();
				}

				viewButton.setVisible(false);
			}
		}
	}

	/**
	 * This method allows delete record into table allergy or hobby
	 * 
	 * @param event
	 */
	public void deleteButtonOnAction(ActionEvent event) {

		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();

		// Lambda Expressions
		deleteButton.setOnAction(ev -> {
			TableViewHobby selectedItemHobby = HobbyTableView.getSelectionModel().getSelectedItem();
			HobbyTableView.getItems().remove(selectedItemHobby);

			try {
				Statement statement = connectDB.createStatement();
				statement
						.execute("DELETE FROM hobby WHERE id = " + "\'" + selectedItemHobby.getId() + "\'" + "LIMIT 1");
			} catch (Exception error) {
				error.printStackTrace();
				error.getCause();
			}
		});
	}

	/**
	 * This method allows change record into table allergy or hobby
	 * 
	 * @param event
	 */
	public void changeButtonOnAction(ActionEvent event) {

		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();

		String queryHobby;

		if (!(idHobby.getText().isEmpty() && hobbyTextField.getText().isEmpty())) {
			queryHobby = " UPDATE hobby " + " SET " + "name " + "=" + "\"" + hobbyTextField.getText() + "\""
					+ " WHERE id =" + "\"" + idHobby.getText() + "\";";

			try {
				Statement statement = connectDB.createStatement();
				statement.execute(queryHobby);
			} catch (Exception e) {
				e.printStackTrace();
				e.getCause();
			}
		}
	}

	/**
	 * This method allows change scene
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void showDataOnAction(ActionEvent event) throws IOException {
		Main showdata = new Main();
		showdata.changeSceneWithDimension("/application/view/view_change_user.fxml", 536, 561);
	}

	/**
	 * This class allows you to manage the table TableViewHobby
	 *
	 */
	public class TableViewHobby {
		String id, hobby;

		public TableViewHobby(String id, String hobby) {
			this.id = id;
			this.hobby = hobby;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getHobby() {
			return hobby;
		}

		public void setHobby(String hobby) {
			this.hobby = hobby;
		}
	}

}
