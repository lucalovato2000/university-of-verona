package application.control;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import application.Main;
import application.model.DBConnect;
import application.model.TableStudents;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ControllerViewStudents implements Initializable {

	@FXML
	private TableView<TableStudents> table;
	@FXML
	private TableColumn<TableStudents, String> col_id;
	@FXML
	private TableColumn<TableStudents, String> col_name;
	@FXML
	private TableColumn<TableStudents, String> col_surname;
	@FXML
	private TableColumn<TableStudents, String> col_address;
	@FXML
	private TableColumn<TableStudents, String> col_email;
	@FXML
	private TableColumn<TableStudents, Long> col_phone_number;

	ObservableList<TableStudents> oblist = FXCollections.observableArrayList();

	/**
	 * Return the current stage
	 */
	public void createViewStudentsForm() {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/application/view/view_students.fxml"));

			Stage registerStage = new Stage();

			registerStage.initStyle(StageStyle.UNDECORATED);
			registerStage.setScene(new Scene(root, 682, 422));
			registerStage.show();

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
	}

	/**
	 * Get from the mysql database the data for the instances
	 */
	public void initialize(URL location, ResourceBundle resources) {

		try {

			DBConnect connectNow = DBConnect.getInstance();
			Connection connection = connectNow.getConnection();

			ResultSet rs = connection.createStatement().executeQuery(" SELECT * FROM student ");

			while (rs.next()) {
				oblist.add(new TableStudents(rs.getString("id"), rs.getString("name"), rs.getString("surname"),
						rs.getString("address"), rs.getString("email"), rs.getLong("phone_number")));
			}

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}

		col_id.setCellValueFactory(new PropertyValueFactory<>("id"));
		col_name.setCellValueFactory(new PropertyValueFactory<>("name"));
		col_surname.setCellValueFactory(new PropertyValueFactory<>("surname"));
		col_address.setCellValueFactory(new PropertyValueFactory<>("address"));
		col_email.setCellValueFactory(new PropertyValueFactory<>("email"));
		col_phone_number.setCellValueFactory(new PropertyValueFactory<>("phone_number"));

		table.setItems(oblist);
	}

	/**
	 * Move to the previous scene
	 * 
	 * @param event return to the opening scene
	 * @throws IOException
	 */
	public void backButtonOnAction(ActionEvent event) throws IOException {
		Main homepage = new Main();
		homepage.changeSceneWithDimension("/application/view/registration_language_certification.fxml", 536, 327);
	}
}