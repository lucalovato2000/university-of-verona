package application.control;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ResourceBundle;

import application.Main;
import application.model.DBConnect;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.StageStyle;

/**
 * This class allows views and change allergy
 *
 */
public class ControllerChangeAllergyUser extends ControllerUserReservedArea {

	@FXML
	private TextField searchusernameTextField;
	@FXML
	private Label nowUsernameLabel;
	@FXML
	private Label requiredfieldLabel;
	@FXML
	private TableView<TableViewAllergy> TableViewAllergy;
	@FXML
	private TableColumn<TableViewAllergy, String> idTableView;
	@FXML
	private TableColumn<TableViewAllergy, String> allergyTableView;
	@FXML
	private TableColumn<TableViewAllergy, String> precautionsTableView;

	ObservableList<TableViewAllergy> oblistTableViewAllergy = FXCollections.observableArrayList();

	@FXML
	private TextArea precautionsTextField;
	@FXML
	private TextField allergyTextField;
	@FXML
	private TextField idTextFiled;
	@FXML
	private Button viewButton;
	@FXML
	private Button deleteButton;
	@FXML
	private Button changeButton;

	public void createAllergiesAndParentForm() {
		try {

			Parent root = FXMLLoader.load(getClass().getResource("/application/view/change_allergy_hobby_user.fxml"));

			Stage changeAllergyHobby = new Stage();

			changeAllergyHobby.initStyle(StageStyle.UNDECORATED);
			changeAllergyHobby.setScene(new Scene(root, 536, 560));
			changeAllergyHobby.show();

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
	}

	public ControllerChangeAllergyUser() {
		super();
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		idTableView.setCellValueFactory(new PropertyValueFactory<>("id"));
		allergyTableView.setCellValueFactory(new PropertyValueFactory<>("allergy"));
		precautionsTableView.setCellValueFactory(new PropertyValueFactory<>("precautions"));

		TableViewAllergy.setItems(oblistTableViewAllergy);
	}

	/**
	 * This method reveals whether the field has been clicked
	 * 
	 * @param event click with mouse the field
	 */
	public void searchusernameTextFieldOnAction(MouseEvent event) {
		if (searchusernameTextField.getText().isEmpty()) {
			TextField(searchusernameTextField, nowUsernameLabel, requiredfieldLabel);
		} else {
			requiredfieldLabel.setText("");
			nowUsernameLabel.setText("");
		}
	}

	/**
	 * This method allows you to visualize current allergies
	 * 
	 * @param event
	 */
	public void viewButtonOnAction(ActionEvent event) {
		String idUser, allergyQuery = null;
		if (requestTextField(searchusernameTextField, nowUsernameLabel, requiredfieldLabel) == false) {

			if (userExists(searchusernameTextField.getText()) == false) {

				requiredfieldLabel.setVisible(true);
				requiredfieldLabel.setText("Invalid user!");
				nowUsernameLabel.setText("*");

			} else {

				idUser = searchIntoDataBase("id", "student", "username", searchusernameTextField.getText());

				DBConnect connectNow = DBConnect.getInstance();
				Connection connectDB = connectNow.getConnection();

				String queryAllergy = "SELECT * FROM allergy WHERE id_student = " + idUser;

				String allergy = null;
				String precautions = null;
				String idAllergy = null;

				try {
					Statement statement = connectDB.createStatement();
					ResultSet resultRecord = statement.executeQuery(queryAllergy);

					while (resultRecord.next()) {
						idAllergy = resultRecord.getString("id");
						allergy = resultRecord.getString("name");
						precautions = resultRecord.getString("description");

						allergyQuery += allergy + "|" + precautions;
						allergyQuery = allergyQuery.substring(2);
						oblistTableViewAllergy.add(new TableViewAllergy(idAllergy, allergy, precautions));
					}

				} catch (Exception e) {
					e.printStackTrace();
					e.getCause();
				}

				viewButton.setVisible(false);
			}
		}
	}

	/**
	 * This method allows delete record into table allergy or hobby
	 * 
	 * @param event
	 */
	public void deleteButtonOnAction(ActionEvent event) {

		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();

		deleteButton.setOnAction(e -> {
			TableViewAllergy selectedItemAllergy = TableViewAllergy.getSelectionModel().getSelectedItem();
			TableViewAllergy.getItems().remove(selectedItemAllergy);

			try {
				Statement statement = connectDB.createStatement();
				statement.execute(
						"DELETE FROM allergy WHERE id = " + "\'" + selectedItemAllergy.getId() + "\'" + "LIMIT 1");
			} catch (Exception error) {
				error.printStackTrace();
				error.getCause();
			}
		});
	}

	/**
	 * This method allows change record into table allergy
	 * 
	 * @param event
	 */
	public void changeButtonOnAction(ActionEvent event) {

		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();

		String queryName;
		String queryDescription;

		if (!(idTextFiled.getText().isEmpty() && allergyTextField.getText().isEmpty()
				&& precautionsTextField.getText().isEmpty())) {
			queryName = " UPDATE allergy" + " SET " + "name " + "=" + "\"" + allergyTextField.getText() + "\""
					+ " WHERE id =" + "\"" + idTextFiled.getText() + "\";";

			try {
				Statement statement = connectDB.createStatement();
				statement.execute(queryName);
			} catch (Exception e) {
				e.printStackTrace();
				e.getCause();
			}

			queryDescription = " UPDATE allergy " + " SET " + "description " + "=" + "\""
					+ precautionsTextField.getText() + "\"" + " WHERE id =" + "\"" + idTextFiled.getText() + "\";";

			try {
				Statement statement = connectDB.createStatement();
				statement.execute(queryDescription);

			} catch (Exception e) {
				e.printStackTrace();
				e.getCause();
			}

		}
	}

	/**
	 * This method allows change scene
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void hobbyOnAction(ActionEvent event) throws IOException {
		Main hobby = new Main();
		hobby.changeSceneWithDimension("/application/view/change_hobby_user.fxml", 536, 561);
	}

	/**
	 * This class allows you to manage the table TableViewAllergy
	 *
	 */
	public class TableViewAllergy {

		String id, allergy, precautions;

		public TableViewAllergy(String id, String allergy, String precautions) {
			this.id = id;
			this.allergy = allergy;
			this.precautions = precautions;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getAllergy() {
			return allergy;
		}

		public void setAllergy(String allergy) {
			this.allergy = allergy;
		}

		public String getPrecautions() {
			return precautions;
		}

		public void setPrecautions(String precautions) {
			this.precautions = precautions;
		}
	}
}
