package application.control;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;

import application.model.DBConnect;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;

/**
 * This class allows you to view the questionnaire
 * 
 */
public class ControllerUserQuestionnarie extends ControllerUserReservedArea {

	@FXML
	private TextField searchusernameTextField;
	@FXML
	private Label requiredfieldLabel;
	@FXML
	private Label nowUsernameLabel;
	@FXML
	private TableView<TableViewQuestionnaire> TableView;
	@FXML
	private TableColumn<TableViewQuestionnaire, String> nameTableView;
	@FXML
	private TableColumn<TableViewQuestionnaire, String> gradeTableView;
	@FXML
	private TableColumn<TableViewQuestionnaire, String> commentsTableView;
	ObservableList<TableViewQuestionnaire> oblistTableView = FXCollections.observableArrayList();

	String grade = null;
	String comments = null;
	String name = null;

	public void createQuestionnarieForm() {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/application/view/view_questionnaire_user.fxml"));

			Stage questionnaire = new Stage();

			questionnaire.titleProperty();
			questionnaire.setTitle("Questionnaire");
			questionnaire.initStyle(StageStyle.UNDECORATED);
			questionnaire.setScene(new Scene(root, 536, 560));
			questionnaire.show();

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
	}

	public ControllerUserQuestionnarie() {
		super();
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		super.initialize(arg0, arg1);
		nameTableView.setCellValueFactory((new PropertyValueFactory<>("name")));
		gradeTableView.setCellValueFactory((new PropertyValueFactory<>("grade")));
		commentsTableView.setCellValueFactory((new PropertyValueFactory<>("comments")));
		TableView.setItems(oblistTableView);
	}

	/**
	 * This method reveals whether the field has been clicked
	 * 
	 * @param click with mouse the field
	 */
	public void searchusernameTextFieldOnAction(MouseEvent event) {
		if (searchusernameTextField.getText().isEmpty()) {
			TextField(searchusernameTextField, nowUsernameLabel, requiredfieldLabel);
		} else {
			requiredfieldLabel.setText("");
			nowUsernameLabel.setText("");
		}
	}

	/**
	 * This method allows you to visualize the questionnaire
	 * 
	 * @param event
	 */
	public void viewButtonOnAction(ActionEvent event) {
		String username = searchusernameTextField.getText();
		if ((requestTextField(searchusernameTextField, nowUsernameLabel, requiredfieldLabel) == true)) {
			requiredfieldLabel.setVisible(true);

		} else if (userExists(searchusernameTextField.getText()) == false) {
			requiredfieldLabel.setVisible(true);
			requiredfieldLabel.setText("Invalid user!");
			nowUsernameLabel.setText("*");

		} else {
			viewTableQuestionnarie(username);
		}
	}

	/**
	 * This method searches the database for the requested value
	 * 
	 * @param select return parameter
	 * @param from   search the table
	 * @param type   field to change
	 * @param where  new field
	 * @return id of the found record
	 */
	public Set<String> IdHoliday(String select, String from, String type, String where) {
		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();

		String query = "SELECT " + select + "\r\n" + " FROM " + from + "\r\n" + " WHERE " + type + " = \"" + where
				+ "\"";

		Set<String> result = new HashSet<String>();
		String Id = null;

		try {
			Statement statement = connectDB.createStatement();
			ResultSet resultRecord = statement.executeQuery(query);

			while (resultRecord.next()) {
				Id = resultRecord.getString("id_holiday");
				result.add(Id);
			}

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
		return result;
	}

	/**
	 * This method searches the database for the requested value
	 * 
	 * @param select return parameter
	 * @param from   search the table
	 * @param type   field to change
	 * @param where  new field
	 * @return id of the found record
	 */
	public Set<String> IdQuestionnaire(String select, String from, String type, String where, String typeAnd,
			String and) {
		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();

		String query = "SELECT " + select + "\r\n" + " FROM " + from + "\r\n" + " WHERE " + type + " = \"" + where
				+ "\"" + " AND " + typeAnd + " = \"" + and + "\"";

		Set<String> result = new HashSet<String>();
		String Id = null;

		try {
			Statement statement = connectDB.createStatement();
			ResultSet resultRecord = statement.executeQuery(query);

			while (resultRecord.next()) {
				Id = resultRecord.getString("id");
				result.add(Id);
			}

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
		return result;
	}

	/**
	 * This method populates the questionnaire view table
	 * 
	 */
	public void viewTableQuestionnarie(String username) {
		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();

		HashSet<String> Holiday = new HashSet<String>(IdHoliday("id_holiday", "survey", "id_student",
				searchIntoDataBase("id", "student", "username", username)));
		HashSet<String> Survey;
		String record = null;
		String query = "SELECT name FROM holiday WHERE id = ";
		char c = '|';

		for (String idHoliday : Holiday) {
			Survey = new HashSet<String>(IdQuestionnaire("id", "survey", "id_holiday", String.valueOf(idHoliday),
					"id_student", searchIntoDataBase("id", "student", "username", username)));

			for (String idSurvey : Survey) {
				record = searchRecordIntoDataBaseQuestionnaire("*", "survey", "id",
						idSurvey + "ID HOLIDAY " + idHoliday);
				grade = String.valueOf(record.subSequence(0, record.indexOf(c)));
				comments = String.valueOf(record.substring(record.indexOf(c) + 1));

				query += idHoliday;

				try {
					Statement statement = connectDB.createStatement();
					ResultSet resultRecord = statement.executeQuery(query);
					while (resultRecord.next()) {
						name = resultRecord.getString("name");
					}
				} catch (Exception e) {
					e.printStackTrace();
					e.getCause();
				}

				oblistTableView.add(new TableViewQuestionnaire(name, grade, comments));
				query = String.valueOf(query.subSequence(0, query.indexOf("= ") + 2)); // Truncate the id holiday to
																						// free the string and then add
																						// the new id at the next
																						// iteration
			}
		}
	}

	/**
	 * This class allows you to manage the table TableViewQuestionnaire
	 *
	 */
	public static class TableViewQuestionnaire {

		String name, grade, comments;

		public TableViewQuestionnaire(String name, String grade, String comments) {

			this.name = name;
			this.grade = grade;
			this.comments = comments;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getGrade() {
			return grade;
		}

		public void setGrade(String grade) {
			this.grade = grade;
		}

		public String getComments() {
			return comments;
		}

		public void setComments(String comments) {
			this.comments = comments;
		}
	}

}
