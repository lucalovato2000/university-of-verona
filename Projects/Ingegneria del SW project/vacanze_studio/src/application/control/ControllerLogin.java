package application.control;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import application.Main;
import application.model.DBConnect;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public class ControllerLogin {
	@FXML
	private TextField usernameTextField;
	@FXML
	private PasswordField passwordTextField;
	@FXML
	private Button loginButton;
	@FXML
	private ToggleGroup permit;
	@FXML
	private Label wrongDataLabel;
	@FXML
	private ImageView backButton;
	@FXML
	private Button resetButton;

	@FXML
	void resetOnAction(ActionEvent event) {
		usernameTextField.setText("");
		passwordTextField.setText("");
		wrongDataLabel.setText("");
		permit.selectToggle(null);
	}

	/**
	 * Perform the login into the application
	 * 
	 * @param event return to the opening scene
	 * @throws NoSuchAlgorithmException
	 */
	@FXML
	public void login(ActionEvent event) throws NoSuchAlgorithmException {
		// Connect to the database
		DBConnect DBInstance = DBConnect.getInstance();
		Connection connection = DBInstance.getConnection();

		// Set the label color to RED and clear the content
		wrongDataLabel.setStyle("-fx-text-fill: #dc3545; -fx-font-weight: bold; -fx-font-size:12");
		wrongDataLabel.setText("");

		String username = usernameTextField.getText();
		String password = passwordTextField.getText();
		String passwordDB = null;
		int rightUsername = 0;
		int error = 0;

		// Get the information about the role of the user
		RadioButton selectedRadioButton = (RadioButton) permit.getSelectedToggle();

		// check if the user has entered all the filds
		if (username == null || username.trim().isEmpty() || password == null || password.trim().isEmpty()
				|| selectedRadioButton == null) {
			wrongDataLabel.setText("Please, enter all the required information");
		} else {
			String toogleGroupValue = selectedRadioButton.getText();

			// Build the query to perform to the database
			String table = (toogleGroupValue.equals("I'm a student")) ? "student" : "responsible";
			String query = "SELECT * FROM " + table + " WHERE username = '" + username + "' LIMIT 1;";

			// Get the crypted password
			String securePassword = getSecurePassword(password);

			try {
				// Create the java database statement
				Statement statement = connection.createStatement();

				// Execute the query, and get a java resultset
				ResultSet result = statement.executeQuery(query);

				// Iterate through the java resultset
				while (result.next()) {
					rightUsername++;
					passwordDB = result.getString("crypted_password");
				}

				statement.close();

				// Check if the inserted information are correct
				if (rightUsername > 0) {
					if (passwordDB.equals(securePassword)) {
						if (toogleGroupValue.equals("I'm a student")) {
							goToReservedAreaUser(event);
						} else {
							goToReservedAreaAdmin(event);
						}
					} else {
						error = 1; // Wrong password
					}
				} else {
					error = 1; // Wrong username
				}

				if (error == 1) {
					wrongDataLabel.setText("The username or password is incorrect");
				}
			} catch (Exception e) {
				e.printStackTrace();
				e.getCause();
			}
		}
	}

	/**
	 * Given a password string, returns his SHA-256 crypted verion
	 * 
	 * @param password the string to crypt
	 * @return the crypted password
	 * @throws NoSuchAlgorithmException
	 */
	private String getSecurePassword(String password) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(password.getBytes(StandardCharsets.UTF_8));
		byte[] digest = md.digest();
		return String.format("%064x", new BigInteger(1, digest));
	}

	/**
	 * If the logged user is a student, go to the user reserved area
	 * 
	 * @param event return to the opening scene
	 * @throws IOException
	 */
	@FXML
	public void goToReservedAreaUser(ActionEvent event) throws IOException {
		Main infouser = new Main();
		infouser.changeSceneWithDimension("/application/view/reserved_area_user.fxml", 536, 560);
	}

	/**
	 * If the logged user is an admin, go to the user admin reserved area
	 * 
	 * @param event return to the opening scene
	 * @throws IOException
	 */
	@FXML
	public void goToReservedAreaAdmin(ActionEvent event) throws IOException {
		Main infouser = new Main();
		infouser.changeSceneWithDimension("/application/view/reserved_area_admin.fxml", 536, 560);
	}

	/**
	 * Forward the user into the first page of the application
	 * 
	 * @param event return to the opening scene
	 * @throws IOException
	 */
	@FXML
	void backButtonAction(MouseEvent event) throws IOException {
		Main homePage = new Main();
		homePage.changeSceneWithDimension("/application/view/login_or_registration.fxml", 536, 560);
	}
}