package application.control;

import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;

import application.Main;
import application.model.DBConnect;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public class ControllerAddHostFamily {
	@FXML
	private Button closeButton;
	@FXML
	private TextField nameTextField;
	@FXML
	private Button addHostFamilyButton;
	@FXML
	private Label wrongDataLabel;
	@FXML
	private TextField surnameTextField;
	@FXML
	private Spinner<Integer> numberOfRoomsSpinner;
	@FXML
	private Spinner<Integer> numberOfBathroomsSpinner;
	@FXML
	private CheckBox animalsCheckBox;
	@FXML
	private Spinner<Integer> numberOfFamilyMembersSpinner;
	@FXML
	private Spinner<Integer> cityCenterDistanceSpinner;
	@FXML
	private Button addAndGoHomeButton;
	@FXML
	private ImageView goBackButton;
	@FXML
	private Button resetButton;

	/**
	 * Clear all the input data
	 * 
	 * @param event return to the opening scene
	 */
	@FXML
	void resetButtonOnAction(ActionEvent event) {
		nameTextField.setText("");
		surnameTextField.setText("");
		cityCenterDistanceSpinner.getValueFactory().setValue(0);
		numberOfRoomsSpinner.getValueFactory().setValue(0);
		numberOfBathroomsSpinner.getValueFactory().setValue(0);
		numberOfFamilyMembersSpinner.getValueFactory().setValue(0);
		animalsCheckBox.isSelected();
	}

	/**
	 * Go back to the admin reserved area
	 * 
	 * @param event return to the opening scene
	 * @throws IOException
	 */
	@FXML
	void goBackButtonOnAction(MouseEvent event) throws IOException {
		Main adminArea = new Main();
		adminArea.changeSceneWithDimension("/application/view/reserved_area_admin.fxml", 536, 560);
	}

	/**
	 * Add the inserted information into the database and go back to the admin page
	 * 
	 * @param event return to the opening scene
	 */
	@FXML
	void addHostFamilyOnAction(ActionEvent event) throws IOException {
		if (addHostFamily()) {
			// Set the label color to GREEN and clear the content
			wrongDataLabel.setStyle("-fx-text-fill: #198754; -fx-font-weight: bold; -fx-font-size:12");
			wrongDataLabel.setText("Host family added successfully");

			resetButtonOnAction(event);
		} else {
			// Set the label color to RED and clear the content
			wrongDataLabel.setStyle("-fx-text-fill: #dc3545; -fx-font-weight: bold; -fx-font-size:12");
			wrongDataLabel.setText("Please, enter all the required information");
		}
	}

	/**
	 * Add the inserted information into the database
	 * 
	 * @param event return to the opening scene
	 * @return true if the data are correctly inserted into the database, false
	 *         otherwise.
	 */
	@FXML
	Boolean addHostFamily() {
		// Connect to the database
		DBConnect DBInstance = DBConnect.getInstance();
		Connection connection = DBInstance.getConnection();

		String name = nameTextField.getText();
		String surname = surnameTextField.getText();
		int cityCenterDistance = (int) cityCenterDistanceSpinner.getValue();
		int numberOfRooms = (int) numberOfRoomsSpinner.getValue();
		int numberOfBathrooms = (int) numberOfBathroomsSpinner.getValue();
		int numberOfFamilyMembers = (int) numberOfFamilyMembersSpinner.getValue();
		int thereAreAnimals = (animalsCheckBox.isSelected()) ? 1 : 0;

		// Clean the wrong data alert
		wrongDataLabel.setText("");

		// Check that the data inserted by the user are not void
		if (name.equals("") || surname.equals("") || String.valueOf(numberOfRooms).length() == 0
				|| String.valueOf(numberOfBathrooms).length() == 0
				|| String.valueOf(numberOfFamilyMembers).length() == 0
				|| String.valueOf(cityCenterDistance).length() == 0) {
			return false;
		} else {
			// Build the query to perform to the database
			String query = "INSERT INTO holiday_family (name, surname, n_of_rooms, n_of_bathrooms, animals, family_members, distance_from_city_center) ";
			query += "VALUES ('" + name + "', '" + surname + "', " + numberOfRooms + ", " + numberOfBathrooms + ", "
					+ numberOfFamilyMembers + ", " + thereAreAnimals + ", " + cityCenterDistance + ");";

			try {
				// Create the java database statement
				Statement statement = connection.createStatement();

				// Execute the insert query
				statement.execute(query);
				statement.close();
			} catch (Exception e) {
				e.printStackTrace();
				e.getCause();
			}
			return true;
		}
	}
}