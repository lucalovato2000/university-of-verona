package application.control;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ResourceBundle;

import application.Main;
import application.model.DBConnect;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * This class allows to view user personal data
 *
 */
public class ControllerUserViewChange implements Initializable {

	@FXML
	private TableView<TableViewPersonalData> TableViewPersonalData;
	@FXML
	private TableColumn<TableViewPersonalData, String> nameTableView;
	@FXML
	private TableColumn<TableViewPersonalData, String> surnameTableView;
	@FXML
	private TableColumn<TableViewPersonalData, String> emailTableView;
	@FXML
	private TableColumn<TableViewPersonalData, String> addressTableView;
	@FXML
	private TableColumn<TableViewPersonalData, String> phoneTableView;
	@FXML
	private TableColumn<TableViewPersonalData, String> usernameTableView;
	ObservableList<TableViewPersonalData> oblistTableViewPersonalData = FXCollections.observableArrayList();

	@FXML
	private TableView<TableViewAllergy> TableViewAllergy;
	@FXML
	private TableColumn<TableViewAllergy, String> allergyTableView;
	@FXML
	private TableColumn<TableViewAllergy, String> precautionsTableView;
	ObservableList<TableViewAllergy> oblistTableViewAllergy = FXCollections.observableArrayList();

	@FXML
	private TableView<TableViewHobby> HobbyTableView;
	@FXML
	private TableColumn<TableViewHobby, String> hobbyTableView;
	ObservableList<TableViewHobby> oblistTableViewHobby = FXCollections.observableArrayList();

	@FXML
	private TextField usernameTextField;
	@FXML
	private Label requiredfieldLabel;
	@FXML
	private Label asteriskusernameLabel;
	@FXML
	private Button viewButton;

	public void createViewChangeForm() {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/application/view/view_change_user.fxml"));

			Stage viewChange = new Stage();

			viewChange.titleProperty();
			viewChange.setTitle("View Change");
			viewChange.initStyle(StageStyle.UNDECORATED);
			viewChange.setScene(new Scene(root, 536, 560));
			viewChange.show();

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		nameTableView.setCellValueFactory(new PropertyValueFactory<>("name"));
		surnameTableView.setCellValueFactory(new PropertyValueFactory<>("surname"));
		emailTableView.setCellValueFactory(new PropertyValueFactory<>("email"));
		addressTableView.setCellValueFactory(new PropertyValueFactory<>("address"));
		phoneTableView.setCellValueFactory(new PropertyValueFactory<>("phone"));
		usernameTableView.setCellValueFactory(new PropertyValueFactory<>("username"));
		TableViewPersonalData.setItems(oblistTableViewPersonalData);

		allergyTableView.setCellValueFactory(new PropertyValueFactory<>("allergy"));
		precautionsTableView.setCellValueFactory(new PropertyValueFactory<>("precautions"));
		TableViewAllergy.setItems(oblistTableViewAllergy);

		hobbyTableView.setCellValueFactory(new PropertyValueFactory<>("hobby"));
		HobbyTableView.setItems(oblistTableViewHobby);
	}

	/**
	 * This method allows you to go back to the previous scene
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void backButtonOnAction(ActionEvent event) throws IOException {
		Main backButton = new Main();
		backButton.changeSceneWithDimension("/application/view/change_personal_data_user.fxml", 536, 561);
	}

	/**
	 * This method allows you to go to the reserved area user
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void confirmButtonOnAction(ActionEvent event) throws IOException {
		Main backButton = new Main();
		backButton.changeSceneWithDimension("/application/view/reserved_area_user.fxml", 536, 561);
	}

	/**
	 * This method shows the errors of TextField
	 * 
	 * @param newChangeTextField
	 * @param asterisk
	 * @param error
	 * @return true if field is empty
	 */
	public boolean requestTextField(TextField newChangeTextField, Label asterisk, Label error) {
		if (!(newChangeTextField.getText().isEmpty())) {
			asterisk.setText("");
			error.setText("");
			return false;
		} else {
			asterisk.setText("*");
			error.setText("(*) Required field");
			return true;
		}
	}

	/**
	 * This method shows the errors of TextField
	 * 
	 * @param newChangeTextField
	 * @param asteriskLabel
	 * @param requiredLabel
	 */
	public void TextField(TextField newChangeTextField, Label asteriskLabel, Label requiredLabel) {
		if (requestTextField(newChangeTextField, asteriskLabel, requiredLabel) == true) {
			asteriskLabel.setText("");
			requiredLabel.setVisible(false);
		}
		requiredLabel.setText("(*) Required field");
	}

	/**
	 * This method searches the database for the requested value
	 * 
	 * @param select return parameter
	 * @param from   search the table
	 * @param type   field to change
	 * @param where  new field
	 * @return id of the found record
	 */
	public String searchIntoDataBase(String select, String from, String type, String where) {
		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();

		String query = "SELECT " + select + "\r\n" + " FROM " + from + "\r\n" + " WHERE " + type + " = \"" + where
				+ "\"";

		String idSearched = null;

		try {
			Statement statement = connectDB.createStatement();
			ResultSet resultRecord = statement.executeQuery(query);

			while (resultRecord.next()) {
				idSearched = resultRecord.getString("id");
			}

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
		return idSearched;
	}

	/**
	 * This method reveals whether the field has been clicked
	 * 
	 * @param click with mouse the field
	 */
	public void usernameTextFieldOnAction(MouseEvent event) {
		if (usernameTextField.getText().isEmpty()) {
			TextField(usernameTextField, asteriskusernameLabel, requiredfieldLabel);
		} else {
			requiredfieldLabel.setText("");
			asteriskusernameLabel.setText("");
		}
	}

	/**
	 * This method allows you to populate the table
	 * 
	 */
	public void showPersonalData() {
		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();

		String idStudent = searchIntoDataBase("id", "student", "username", usernameTextField.getText());

		String query = "SELECT * FROM student WHERE id = " + idStudent;
		String name = null;
		String surname = null;
		String email = null;
		String address = null;
		String phone = null;
		String username = null;

		try {
			Statement statement = connectDB.createStatement();
			ResultSet resultRecord = statement.executeQuery(query);
			while (resultRecord.next()) {
				name = resultRecord.getString("name");
				surname = resultRecord.getString("surname");
				email = resultRecord.getString("email");
				address = resultRecord.getString("address");
				phone = resultRecord.getString("phone_number");
				username = resultRecord.getString("username");
			}

			oblistTableViewPersonalData.add(new TableViewPersonalData(name, surname, email, address, phone, username));
		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
	}

	/**
	 * This method allows you to populate the table
	 * 
	 */
	public void showOther() {
		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();

		String idStudent = searchIntoDataBase("id", "student", "username", usernameTextField.getText());

		String queryAllergy = "SELECT * FROM allergy WHERE id_student = " + idStudent;

		String allergy = null;
		String precautions = null;
		String allergyQuery = null;

		try {
			Statement statement = connectDB.createStatement();
			ResultSet resultRecord = statement.executeQuery(queryAllergy);

			while (resultRecord.next()) {
				allergy = resultRecord.getString("name");
				precautions = resultRecord.getString("description");

				allergyQuery += allergy + "|" + precautions;
				allergyQuery = allergyQuery.substring(2);
				oblistTableViewAllergy.add(new TableViewAllergy(allergy, precautions));
			}

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}

		String queryHobby = "SELECT * FROM hobby WHERE id_student = " + idStudent;
		String hobby = null;
		String hobbyQuery = null;

		try {
			Statement statement = connectDB.createStatement();
			ResultSet resultRecord = statement.executeQuery(queryHobby);

			while (resultRecord.next()) {
				hobby = resultRecord.getString("name");

				hobbyQuery += hobby + "|";
				hobbyQuery = hobbyQuery.substring(2);
				oblistTableViewHobby.add(new TableViewHobby(hobby));
			}

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
	}

	/**
	 * This method looks for whether the user exists
	 * 
	 * @param username
	 * @return
	 */
	public boolean userExists(String username) {
		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();

		String query = "SELECT username FROM student WHERE username = " + "\"" + username + "\"";

		try {
			Statement statement = connectDB.createStatement();
			ResultSet resultRecord = statement.executeQuery(query);

			if (resultRecord.next()) {
				return true;
			} else {
				return false;
			}

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();

		}
		return false;
	}

	/**
	 * This method allows you to visualize the tables
	 * 
	 * @param event
	 */
	public void viewButtonOnAction(ActionEvent event) {
		if ((requestTextField(usernameTextField, asteriskusernameLabel, requiredfieldLabel) == true)) {
			requiredfieldLabel.setVisible(true);
		} else if (userExists(usernameTextField.getText()) == false) {

			requiredfieldLabel.setVisible(true);
			requiredfieldLabel.setText("Invalid user!");
			asteriskusernameLabel.setText("*");

		} else {
			showPersonalData();
			showOther();
		}
	}

	/**
	 * This class allows you to manage the table TableViewPersonalData
	 *
	 */
	public class TableViewPersonalData {

		String name, surname, email, address, phone, username;

		public TableViewPersonalData(String name, String surname, String email, String address, String phone,
				String username) {

			this.name = name;
			this.surname = surname;
			this.email = email;
			this.address = address;
			this.phone = phone;
			this.username = username;

		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getSurname() {
			return surname;
		}

		public void setSurneame(String surname) {
			this.surname = surname;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public String getPhone() {
			return phone;
		}

		public void setPhone(String phone) {
			this.phone = phone;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}
	}

	/**
	 * This class allows you to manage the table TableViewAllergy
	 *
	 */
	public class TableViewAllergy {

		String allergy, precautions;

		public TableViewAllergy(String allergy, String precautions) {
			this.allergy = allergy;
			this.precautions = precautions;
		}

		public String getAllergy() {
			return allergy;
		}

		public void setAllergy(String allergy) {
			this.allergy = allergy;
		}

		public String getPrecautions() {
			return precautions;
		}

		public void setPrecautions(String precautions) {
			this.precautions = precautions;
		}
	}

	/**
	 * This class allows you to manage the table TableViewHobby
	 *
	 */
	public class TableViewHobby {
		String hobby;

		public TableViewHobby(String hobby) {
			this.hobby = hobby;
		}

		public String getHobby() {
			return hobby;
		}

		public void setHobby(String hobby) {
			this.hobby = hobby;
		}
	}
}