package application.control;

import java.io.IOException;

import application.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class ControllerLoginOrRegistration {

	@FXML
	private Button registerButton;
	@FXML
	private Button loginButton;

	public ControllerLoginOrRegistration() {
	}

	/**
	 * Move to the next scene for choosing admin or user
	 * 
	 * @param event go to scene registration
	 * @throws IOException
	 */
	public void RegistrationOnAction(ActionEvent event) throws IOException {
		Main register = new Main();
		register.changeSceneWithDimension("/application/view/admin_or_user.fxml", 536, 560);
	}

	/**
	 * Move to the next scene for login
	 * 
	 * @param event go to scene login
	 * @throws IOException
	 */
	public void LoginOnAction(ActionEvent event) throws IOException {
		Main login = new Main();
		login.changeSceneWithDimension("/application/view/login.fxml", 536, 445);
	}
}
