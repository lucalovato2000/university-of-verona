package application.control;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ResourceBundle;

import application.Main;
import application.model.DBConnect;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ControllerHolidayBooking implements Initializable {

	@FXML
	private Button confirmButton;
	@FXML
	private ComboBox<String> combobox;
	@FXML
	private AnchorPane collegeAnchorPane;
	@FXML
	private AnchorPane familyAnchorPane;
	@FXML
	private RadioButton singleRadioButton;
	@FXML
	private RadioButton sharedRadioButton;
	@FXML
	private TextField namefriendTextField;
	@FXML
	private TextField surnamefriendTextField;
	@FXML
	private TextField emailfriendTextField;
	@FXML
	private TextField idHolidayTextField;
	@FXML
	private Label asterisklastnameLabel;
	@FXML
	private Label asterisklastsurnameLabel;
	@FXML
	private Label asterisklastemailLabel;
	@FXML
	private Label requiredfieldLabel;
	@FXML
	private Label preferenceLabel;
	@FXML
	private Label idHolidayLabel;
	@FXML
	private Label registrationMessageLabel;
	@FXML
	private RadioButton creditCardRadioButton;
	@FXML
	private RadioButton banckTransferRadioButton;
	@FXML
	private Label payLabel;

	private String preference;
	private boolean options, notOptions;

	private ObservableList<String> list = FXCollections.observableArrayList("College", "Family");

	public void createHolidayBookingForm() {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/application/view/holiday_booking.fxml"));

			Stage holidaybookingStage = new Stage();

			holidaybookingStage.titleProperty();
			holidaybookingStage.setTitle("Holiday Booking");
			holidaybookingStage.initStyle(StageStyle.UNDECORATED);
			holidaybookingStage.setScene(new Scene(root, 459, 403));
			holidaybookingStage.show();

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
	}

	/**
	 * Allows you to initialize objects at the beginning
	 */
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		combobox.setItems(list);
		combobox.setButtonCell(new ListCell<String>() {
			@Override
			protected void updateItem(String item, boolean empty) {
				super.updateItem(item, empty);

				if (empty || item == null) {
					setStyle("-fx-text-fill: WHITE; -fx-font-weight: bold; -fx-font-size:12");
				} else {
					setStyle("-fx-text-fill: WHITE; -fx-font-weight: bold; -fx-font-size:12");
					setText(item.toString()); // Allows you to see the selection in the combobox
				}
			}
		});

		collegeAnchorPane.setVisible(false);
		familyAnchorPane.setVisible(false);
	}

	/**
	 * Make objects you need visible and objects you don't need invisible
	 * 
	 * @param event the choose in radio button
	 */
	public boolean comboboxOnActionEvent(ActionEvent event) {
		preference = combobox.getValue();

		if (preference.isEmpty() == false) {
			if (preference.equals("College")) {
				collegeAnchorPane.setVisible(true);
				familyAnchorPane.setVisible(false);
				options = true;

			}

			if (preference.equals("Family")) {
				familyAnchorPane.setVisible(true);
				collegeAnchorPane.setVisible(false);
				options = false;
			}

			return true;
		} else {
			preference = null;
			return false;
		}
	}

	/**
	 * Insert the preference you have chosen into the system
	 * 
	 * @param event click the button register me
	 * @throws IOException
	 */
	public void registerButtonOnAction(ActionEvent event) throws IOException {

		preferenceLabel.setText("Enter the required data");
		registrationMessageLabel.setText("");

		if (isNumeric(idHolidayTextField.getText().trim()) == true) {

			// Not type College and Family
			if (preference == null) {

				preferenceLabel.setText("");
				registrationId(idHolidayTextField.getText());
				idHolidayLabel.setText("");

				if (banckTransferRadioButton.isSelected() == true || creditCardRadioButton.isSelected() == true) {
					if (payHolidays() == true) {
						registrationMessageLabel.setText("Holiday has been booked successfully!");
						reservationCompleted(event);
					}

				} else {
					payLabel.setText("Select method pay");
				}
			}

			// College
			else if (preference.equals("College")) {
				preferenceLabel.setText("");
				if (mandatoryChoiceSingle() == true || mandatoryChoiceShared() == true) {
					if (preferenceRegistration() == true) {
						idHolidayLabel.setText("");

						if (registrationOptionCollege() == true) {
							preferenceLabel.setText("");
							requiredfieldLabel.setText("");
							registrationMessageLabel.setText("");
						}

						if (banckTransferRadioButton.isSelected() == true
								|| creditCardRadioButton.isSelected() == true) {

							if (payHolidays() == true) {
								registrationMessageLabel.setText("Holiday has been booked successfully!");
								reservationCompleted(event);
							}

						} else {
							payLabel.setText("Select method pay");
						}
					}
				}
			}

			// Family
			else if (preference.equals("Family")) {
				preferenceLabel.setText("");
				if (requiredFieldLastName() == false && requiredFieldLastSurname() == false
						&& requiredFieldLastEmail() == false) {

					if (preferenceRegistration() == true) {
						idHolidayLabel.setText("");

						if (registrationOptionFamily() == true) {
							preferenceLabel.setText("");
							requiredfieldLabel.setText("");
							registrationMessageLabel.setText("");
						}

						if (banckTransferRadioButton.isSelected() == true
								|| creditCardRadioButton.isSelected() == true) {
							if (payHolidays() == true) {
								registrationMessageLabel.setText("Holiday has been booked successfully!");
								reservationCompleted(event);
							}
						} else {
							payLabel.setText("Select method pay");
						}
					}
				}
			}
		} else {
			idHolidayLabel.setText("Please insert only numbers");
		}

	}

	public void reservationCompleted(ActionEvent event) throws IOException {
		Main reservationCompleted = new Main();
		reservationCompleted.changeSceneWithDimension("/application/view/register_vacation_successfully.fxml", 536,
				560);
	}

	/**
	 * This method add id holiday into record when vacation is not selected
	 * 
	 * @param text
	 */
	private void registrationId(String idHoliday) {
		DBConnect connectNow = DBConnect.getInstance();
		Connection connection = connectNow.getConnection();

		String query = "INSERT INTO reservation (id_holiday) VALUES (" + "\"" + idHoliday + "\")";

		try {
			Statement statement = connection.createStatement();
			statement.executeUpdate(query);
		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
	}

	/**
	 * This method allows register to preference pay vacation
	 * 
	 */
	private boolean payHolidays() {
		DBConnect connectNow = DBConnect.getInstance();
		Connection connection = connectNow.getConnection();

		String payMethod = null;
		String query = "INSERT INTO payment (id_reservation, method) VALUES (" + idHolidayTextField.getText() + ",";

		if (banckTransferRadioButton.isSelected() == true) {
			payMethod = banckTransferRadioButton.getText();
		}

		if (creditCardRadioButton.isSelected() == true) {
			payMethod = creditCardRadioButton.getText();
		}

		query += "\"" + payMethod + "\");";

		try {
			Statement statement = connection.createStatement();
			statement.executeUpdate(query);
			return true;

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
		return false;
	}

	/**
	 * Return to the home page
	 * 
	 * @param event return to the opening scene
	 * @throws IOException
	 */
	public void backButtonOnAction(ActionEvent event) throws IOException {
		Main adminArea = new Main();
		adminArea.changeSceneWithDimension("/application/view/view_holidays_user.fxml", 600, 422);
	}

	/**
	 * 
	 * @param query
	 * @return returns the id at the bottom of the table
	 */
	private int getTheLatestId(String query) {
		DBConnect connectNow = DBConnect.getInstance();
		Connection connection = connectNow.getConnection();

		int reservation_id = 0;
		// Get id of the last record
		try {
			Statement statement = connection.createStatement();
			ResultSet resultRecord = statement.executeQuery(query);

			while (resultRecord.next()) {
				reservation_id = resultRecord.getInt("id");
			}

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}

		return reservation_id;
	}

	/**
	 * Register the family option
	 */
	private boolean registrationOptionFamily() {

		DBConnect connectNow = DBConnect.getInstance();
		Connection connection = connectNow.getConnection();

		String namefriend = namefriendTextField.getText();
		String surnamefriend = surnamefriendTextField.getText();
		String emailfriend = emailfriendTextField.getText();
		String idHoliday = idHolidayTextField.getText();

		String querySelectId = "SELECT * FROM reservation ORDER BY id DESC LIMIT 1;";

		int LastId = getTheLatestId(querySelectId);

		String querySelectFamily = "UPDATE reservation" + " SET friend_name=\"" + namefriend + "\","
				+ " friend_surname=\"" + surnamefriend + "\"," + " friend_email=\"" + emailfriend + "\" WHERE id="
				+ LastId + ";";

		String idHolidayQuery = "UPDATE reservation SET id_holiday=\"" + idHoliday + "\" WHERE id=" + LastId + ";";

		try {
			Statement statement = connection.createStatement();
			statement.executeUpdate(querySelectFamily);
			statement.executeUpdate(idHolidayQuery);

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
			return false;
		}
	}

	/**
	 * This method allows you to register the college option
	 */
	private boolean registrationOptionCollege() {

		DBConnect connectNow = DBConnect.getInstance();
		Connection connection = connectNow.getConnection();

		String querySelectId = "SELECT * FROM reservation ORDER BY id DESC LIMIT 1;";

		int LastId = getTheLatestId(querySelectId);

		String idHoliday = idHolidayTextField.getText();

		String single = singleRadioButton.getText().substring(0, 6);
		String querySelectRecordSingle = "UPDATE reservation SET room_type=\"" + single + "\" WHERE id=" + LastId + ";";

		String shared = sharedRadioButton.getText().substring(0, 6);
		String querySelectRecordShared = "UPDATE reservation SET room_type=\"" + shared + "\" WHERE id=" + LastId + ";";

		String idHolidayQuery = "UPDATE reservation SET id_holiday=\"" + idHoliday + "\" WHERE id=" + LastId + ";";

		// If the radiobutton is single, I update the room_type field in the record
		// selected by id
		if (singleRadioButton.isSelected()) {
			try {
				Statement statement = connection.createStatement();
				statement.executeUpdate(querySelectRecordSingle);
				statement.executeUpdate(idHolidayQuery);

				registrationMessageLabel.setText("Holiday has been booked successfully!");

				return true;
			} catch (Exception e) {
				e.printStackTrace();
				e.getCause();
			}
		}

		// If the radiobutton is shared, I update the room_type field in the record
		// selected by id
		if (sharedRadioButton.isSelected()) {
			try {
				Statement statement = connection.createStatement();
				statement.executeUpdate(querySelectRecordShared);
				statement.executeUpdate(idHolidayQuery);

				registrationMessageLabel.setText("Holiday has been booked successfully!");

				return true;
			} catch (Exception e) {
				e.printStackTrace();
				e.getCause();
			}
		}
		return false;
	}

	/**
	 * Record your choice in the mysql database
	 */
	private boolean preferenceRegistration() {

		DBConnect connectNow = DBConnect.getInstance();
		Connection connection = connectNow.getConnection();

		String insertFields = "INSERT INTO reservation(accomodation_preference) VALUES ('";
		String insertValues = preference + "')";
		String preferenceValues = insertFields + insertValues;

		try {
			Statement statement = connection.createStatement();
			statement.executeUpdate(preferenceValues);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
			return false;
		}
	}

	/**
	 * This method checks if the last email field is empty
	 * 
	 * @return true if field is empty
	 * @return false if field is not empty
	 */
	private boolean requiredFieldLastEmail() {
		if (emailfriendTextField.getText().trim().isEmpty()) {
			asterisklastemailLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asterisklastemailLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the last surname field is empty
	 * 
	 * @return true if field is empty
	 * @return false if field is not empty
	 */
	private boolean requiredFieldLastSurname() {
		if (surnamefriendTextField.getText().trim().isEmpty()) {
			asterisklastsurnameLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asterisklastsurnameLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the lastname field is empty
	 * 
	 * @return true if field is empty
	 * @return false if field is not empty
	 */
	private boolean requiredFieldLastName() {
		if (namefriendTextField.getText().trim().isEmpty()) {
			asterisklastnameLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asterisklastnameLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * 
	 * @return true if radio button refering to single room is selected
	 */
	private boolean mandatoryChoiceSingle() {
		if (singleRadioButton.isSelected()) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @return true if radio button refering to shared room is selected
	 */
	private boolean mandatoryChoiceShared() {
		if (sharedRadioButton.isSelected()) {
			return true;
		}
		return false;
	}

	/**
	 * This method checks if the string is only numbers
	 * 
	 * @param str
	 * @return true if only number
	 */
	public static boolean isNumeric(String str) {
		try {
			Long.parseLong(str);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
}
