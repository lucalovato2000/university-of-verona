package application.control;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ResourceBundle;

import application.model.DBConnect;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * This class allows you to view the certificate
 *
 */
public class ControllerUserCertificate extends ControllerUserReservedArea {

	@FXML
	private TableView<TableViewCertificate> TableView;
	@FXML
	private TableColumn<TableViewCertificate, String> nameTableView;
	@FXML
	private TableColumn<TableViewCertificate, String> levelTableView;
	@FXML
	private TableColumn<TableViewCertificate, String> languageTableView;
	@FXML
	private TextField searchusernameTextField;
	@FXML
	private Label requiredfieldLabel;
	@FXML
	private Label nowUsernameLabel;
	private String level = null, language = null, name = null;

	ObservableList<TableViewCertificate> oblistTableView = FXCollections.observableArrayList();

	public void createCertificateForm() {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/application/view/view_certificate.fxml"));

			Stage certificate = new Stage();

			certificate.titleProperty();
			certificate.setTitle("Certificate");
			certificate.initStyle(StageStyle.UNDECORATED);
			certificate.setScene(new Scene(root, 536, 560));
			certificate.show();

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
	}

	public ControllerUserCertificate() {
		super();
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		super.initialize(arg0, arg1);
		nameTableView.setCellValueFactory(new PropertyValueFactory<>("name"));
		levelTableView.setCellValueFactory(new PropertyValueFactory<>("level"));
		languageTableView.setCellValueFactory(new PropertyValueFactory<>("language"));
		TableView.setItems(oblistTableView);
	}

	/**
	 * This method reveals whether the field has been clicked
	 * 
	 * @param click with mouse the field
	 */
	public void searchusernameTextFieldOnAction(MouseEvent event) {
		if (searchusernameTextField.getText().isEmpty()) {
			TextField(searchusernameTextField, nowUsernameLabel, requiredfieldLabel);
		} else {
			requiredfieldLabel.setText("");
			nowUsernameLabel.setText("");
		}
	}

	/**
	 * This method allows you to visualize the certificate
	 * 
	 * @param event
	 */
	public void viewButtonOnAction(ActionEvent event) {
		if ((requestTextField(searchusernameTextField, nowUsernameLabel, requiredfieldLabel) == true)) {
			requiredfieldLabel.setVisible(true);
		} else if (userExists(searchusernameTextField.getText()) == false) {

			requiredfieldLabel.setVisible(true);
			requiredfieldLabel.setText("Invalid user!");
			nowUsernameLabel.setText("*");

		} else {
			viewTableCertificate();
		}
	}

	/**
	 * This method populates the certificate view table
	 * 
	 */
	public void viewTableCertificate() {
		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();
		String idHoliday = searchIdHoliday("id_holiday", "survey", "id_student",
				searchIntoDataBase("id", "student", "username", searchusernameTextField.getText()));
		String idRecord = searchIdCertificate("id", "certificate", "id_student",
				searchIntoDataBase("id", "student", "username", searchusernameTextField.getText()));
		String query = "SELECT name FROM holiday WHERE id = ";
		String Certificate = null;
		char c = '|';

		for (int i = 0, j = 0; i < idRecord.length() && j < idHoliday.length(); i++, j++) {

			Certificate = searchRecordIntoDataBaseCertificate("*", "certificate", "id",
					String.valueOf(idRecord.charAt(i)));
			level = String.valueOf(Certificate.subSequence(0, Certificate.indexOf(c)));
			language = String.valueOf(Certificate.substring(Certificate.indexOf(c) + 1));

			query += idHoliday.charAt(j);

			try {
				Statement statement = connectDB.createStatement();
				ResultSet resultRecord = statement.executeQuery(query);
				while (resultRecord.next()) {
					name = resultRecord.getString("name");
				}
			} catch (Exception e) {
				e.printStackTrace();
				e.getCause();
			}

			query = String.valueOf(query.subSequence(0, query.indexOf("= ") + 2));
			oblistTableView.add(new TableViewCertificate(name, level, language));
		}
	}

	/**
	 * This class allows you to manage the table TableViewCertificate
	 *
	 */
	public class TableViewCertificate {

		String name, level, language;

		public TableViewCertificate(String name, String level, String language) {

			this.name = name;
			this.level = level;
			this.language = language;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getLevel() {
			return level;
		}

		public void setLevel(String level) {
			this.level = level;
		}

		public String getLanguage() {
			return language;
		}

		public void setLanguage(String language) {
			this.language = language;
		}
	}
}
