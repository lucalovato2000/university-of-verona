package application.control;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import application.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;

public class ControllerAdminReservedArea implements Initializable {

	@FXML
	private Button viewHolidayButton;
	@FXML
	private Button registerHolidayButton;
	@FXML
	private Button questionnaireButton;
	@FXML
	private Button logoutButton;
	@FXML
	private Button addHostFamilyButton;
	@FXML
	private Button addACollegeButton;
	@FXML
	private Button viewStatisticsButton;

	public ControllerAdminReservedArea() {
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		viewHolidayButton.setEffect(new DropShadow(2, Color.WHITE));
		registerHolidayButton.setEffect(new DropShadow(2, Color.WHITE));
		questionnaireButton.setEffect(new DropShadow(2, Color.WHITE));
		addHostFamilyButton.setEffect(new DropShadow(2, Color.WHITE));
		addACollegeButton.setEffect(new DropShadow(2, Color.WHITE));
		viewStatisticsButton.setEffect(new DropShadow(2, Color.WHITE));
	}

	/**
	 * Move to next stage for viewing the statistics
	 * 
	 * @param event return to the opening scene
	 * @throws IOException
	 */
	public void viewStatisticsButtonOnAction(ActionEvent event) throws IOException {
		Main viewStatistics = new Main();
		viewStatistics.changeSceneWithDimension("/application/view/view_statistics_admin.fxml", 536, 560);
	}

	/**
	 * Move to next stage for adding a new college
	 * 
	 * @param event return to the opening scene
	 * @throws IOException
	 */
	public void addACollegeOnAction(ActionEvent event) throws IOException {
		Main addCollege = new Main();
		addCollege.changeSceneWithDimension("/application/view/add_college.fxml", 536, 325);
	}

	/**
	 * Move to next stage for adding a host Family
	 * 
	 * @param event return to the opening scene
	 * @throws IOException
	 */
	public void addHostFamilyOnAction(ActionEvent event) throws IOException {
		Main addHostFamily = new Main();
		addHostFamily.changeSceneWithDimension("/application/view/add_host_family.fxml", 536, 560);
	}

	/**
	 * Move to next stage for viewing the holidays
	 * 
	 * @param event return to the opening scene
	 * @throws IOException
	 */
	public void viewHolidayOnAction(ActionEvent event) throws IOException {
		Main listholidays = new Main();
		listholidays.changeSceneWithDimension("/application/view/view_holidays_admin.fxml", 600, 422);
	}

	/**
	 * Move to next stage for register a holiday
	 * 
	 * @param event return to the opening scene
	 * @throws IOException
	 */
	public void registerHolidayOnAction(ActionEvent event) throws IOException {
		Main newHoliday = new Main();
		newHoliday.changeSceneWithDimension("/application/view/registration_holidays.fxml", 536, 433);
	}

	/**
	 * Move to next stage for register the language certification
	 * 
	 * @param event return to the opening scene
	 * @throws IOException
	 */
	public void questionnaireButtonOnAction(ActionEvent event) throws IOException {
		Main listholidays = new Main();
		listholidays.changeSceneWithDimension("/application/view/registration_language_certification.fxml", 536, 327);
	}

	/**
	 * Move to the previous stage
	 * 
	 * @param event return to the opening scene
	 * @throws IOException
	 */
	public void logoutButtonOnAction(ActionEvent event) throws IOException {
		Main logout = new Main();
		logout.changeSceneWithDimension("/application/view/login_or_registration.fxml", 536, 560);
	}
}
