package application.control;

import java.io.IOException;

import application.Main;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class ControllerRegistrationCompleted {

	public ControllerRegistrationCompleted() {
	}

	@FXML
	private Button closeButton;

	public void closeButtonOnAction(ActionEvent event) {
		Stage stage = (Stage) closeButton.getScene().getWindow();
		stage.close();
		Platform.exit();
	}

	public void loginScreenOnAction(ActionEvent event) throws IOException {
		Main login = new Main();
		login.changeSceneWithDimension("/application/view/login_or_registration.fxml", 536, 560);
	}
}
