package application.control;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

import application.Main;
import application.model.DBConnect;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ControllerAllergiesAndParent implements Initializable {

	@FXML
	private Button closeButton;
	@FXML
	private TextField allergyTextField;
	@FXML
	private TextField parentnameTextField;
	@FXML
	private TextField parentsurnameTextField;
	@FXML
	private TextField parentphonenumberTextField;
	@FXML
	private TextField parentemailTextField;
	@FXML
	private Label asterisknameLabel;
	@FXML
	private Label asterisksurnameLabel;
	@FXML
	private Label asteriskphonenumberLabel;
	@FXML
	private Label asteriskemailLabel;
	@FXML
	private Label requiredfieldLabel;
	@FXML
	private Label confirmphonenumberLabel;
	@FXML
	private TextField studenthobbyTextField;
	@FXML
	private Label asteriskhobbyLabel;
	@FXML
	private TextArea descriptionprecautionsTextArea;
	@FXML
	private Label asterisksprecautionsLabel;
	@FXML
	private AnchorPane informationParentanchorPane;
	@FXML
	private Label registerSuccessfullyLabel;
	@FXML
	private Button confirmButton;
	@FXML
	private Button registerButton;

	Timer timer = new Timer();

	/**
	 * This timer allows you to wait a second for the confirmation label to appear
	 * on the screen
	 * 
	 */
	TimerTask task = new TimerTask() {
		int counter = 1;

		public void run() {

			Platform.runLater(() -> {

				if (counter > 0) {
					counter--;
				} else {
					registerSuccessfullyLabel.setStyle("-fx-text-fill: BLACK; -fx-font-weight: bold; -fx-font-size:12");
					registerSuccessfullyLabel.setText("Other allergies or hobbies to declare?");
					informationParentanchorPane.setVisible(false);
					confirmButton.setVisible(true);
					registerButton.setVisible(false);
					timer.purge();
					timer.cancel();
				}
			});

		};
	};

	public ControllerAllergiesAndParent() {

	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		confirmButton.setVisible(false);
	}

	public void createAllergiesAndParentForm() {
		try {

			Parent root = FXMLLoader.load(getClass().getResource("/application/view/user_parent_preferences.fxml"));

			Stage registerStage = new Stage();

			registerStage.initStyle(StageStyle.UNDECORATED);
			registerStage.setScene(new Scene(root, 536, 560));
			registerStage.show();

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
	}

	/**
	 * This method triggers user registration allergies and parent's information
	 * 
	 * @throws IOException
	 * @throws InterruptedException
	 * 
	 */
	public void registerButtonOnAction(ActionEvent event) throws IOException, InterruptedException {

		// This condition checks if the field is filled in, otherwise it gives an error
		if (!(allergyTextField.getText().isEmpty())) {
			if (requestFiledPrecautions() == false) {
				registrationAllergies();
			}
		}

		// This condition checks if the field is filled in, otherwise it gives an error
		if (requestFiledHobby() == false) {
			registerStudentHobby();
		} else {
			asteriskhobbyLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
		}

		// This condition checks if the field is filled in, otherwise it gives an error
		if (requiredFieldName() == false && requiredFieldSurname() == false && requestFiledNumber() == false
				&& requiredFieldEmail() == false) {

			if (isNumeric(parentphonenumberTextField.getText().trim()) == true) {
				confirmphonenumberLabel.setText("");

				registrationParent();

				resetField(allergyTextField, descriptionprecautionsTextArea, parentnameTextField,
						parentsurnameTextField, parentemailTextField, parentphonenumberTextField,
						studenthobbyTextField);
				registerSuccessfullyLabel.setText("Parent information successfully registered");

				// Call the scheduler to wait a second from now
				timer.schedule(task, 0, 1000);

			} else {
				confirmphonenumberLabel.setText("Enter numbers only");
			}
		}
	}

	/**
	 * This method allows you to return to the admin or user choice
	 * 
	 * @param event return to the opening scene
	 * @throws IOException
	 */
	public void backButtonOnAction(ActionEvent event) throws IOException {
		Main back = new Main();
		back.changeSceneWithDimension("/application/view/admin_or_user.fxml", 536, 560);
	}

	/**
	 * This method allows you to confirm the change
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void confirmButtonOnAction(ActionEvent event) throws IOException {
		Main infouser = new Main();
		infouser.changeSceneWithDimension("/application/view/register_successfully.fxml", 536, 560);
	}

	/**
	 * This method allows you to clear the fields
	 * 
	 * @param allergy
	 * @param precautions
	 * @param name
	 * @param surname
	 * @param email
	 * @param phone
	 * @param hobby
	 */
	private void resetField(TextField allergy, TextArea precautions, TextField name, TextField surname, TextField email,
			TextField phone, TextField hobby) {
		allergy.setText("");
		precautions.setText("");
		name.setText("");
		surname.setText("");
		email.setText("");
		phone.setText("");
		hobby.setText("");
	}

	/**
	 * This method takes from the database the id of the last user registered in the
	 * system (i.e. the one who is now registering)
	 * 
	 * @param query
	 * @return returns the id at the bottom of the table
	 */
	private int getTheLatestId(String query) {
		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();
		int reservation_id = 0;

		// Get id of the last record
		try {
			Statement statement = connectDB.createStatement();
			ResultSet resultRecord = statement.executeQuery(query);

			while (resultRecord.next()) {
				reservation_id = resultRecord.getInt("id");
			}

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}

		return reservation_id;
	}

	/**
	 * This method allows you to register the hobby in the system
	 * 
	 */
	private void registerStudentHobby() {
		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();

		String hobby = studenthobbyTextField.getText();
		String querySelectId = "SELECT * FROM student ORDER BY id DESC LIMIT 1;";
		int lastIdStudent = getTheLatestId(querySelectId);
		String query = "INSERT INTO hobby(name, id_student) VALUES ('" + hobby + "','" + lastIdStudent + "')";

		try {
			Statement statement = connectDB.createStatement();
			statement.executeUpdate(query);

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();

		}
	}

	/**
	 * This method allows you to register the parent in the system
	 * 
	 */
	private void registrationParent() {
		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();

		String name = parentnameTextField.getText();
		String surname = parentsurnameTextField.getText();
		String email = parentemailTextField.getText();
		String phone = parentphonenumberTextField.getText();
		String querySelectId = "SELECT * FROM student ORDER BY id DESC LIMIT 1;";
		int lastIdStudent = getTheLatestId(querySelectId);

		String query = "INSERT INTO parent(name, surname, email, phone_number, id_student) VALUES ('" + name + "','"
				+ surname + "','" + email + "','" + phone + "','" + lastIdStudent + "')";

		try {
			Statement statement = connectDB.createStatement();
			statement.executeUpdate(query);

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();

		}
	}

	/**
	 * This method allows you to register the allergies in the system
	 * 
	 */
	private void registrationAllergies() {
		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();

		String allergy = allergyTextField.getText();
		String descriptionprecautions = descriptionprecautionsTextArea.getText();
		String querySelectId = "SELECT * FROM student ORDER BY id DESC LIMIT 1;";
		int lastIdStudent = getTheLatestId(querySelectId);

		String query = "INSERT INTO allergy(name, description, id_student) VALUES ('" + allergy + "','"
				+ descriptionprecautions + "','" + lastIdStudent + "')";

		try {
			Statement statement = connectDB.createStatement();
			statement.executeUpdate(query);

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();

		}
	}

	/**
	 * This method checks if the name field is empty
	 * 
	 * @return true if filed is empty
	 * @return false if field is not empty
	 */
	public boolean requiredFieldName() {
		if (parentnameTextField.getText().trim().isEmpty()) {
			asterisknameLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asterisknameLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the surname field is empty
	 * 
	 * @return true if filed is empty
	 * @return false if field is not empty
	 */
	public boolean requiredFieldSurname() {
		if (parentsurnameTextField.getText().trim().isEmpty()) {
			asterisksurnameLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asterisksurnameLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the email field is empty
	 * 
	 * @return true if filed is empty
	 * @return false if field is not empty
	 */
	public boolean requiredFieldEmail() {
		if (parentemailTextField.getText().trim().isEmpty()) {
			asteriskemailLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskemailLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the phone number field is empty
	 * 
	 * @return true if filed is empty
	 * @return false if field is not empty
	 */
	public boolean requestFiledNumber() {
		if (parentphonenumberTextField.getText().trim().isEmpty()) {
			asteriskphonenumberLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskphonenumberLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the string is only numbers
	 * 
	 * @param str
	 * @return true if only number
	 */
	public static boolean isNumeric(String str) {
		try {
			Integer.parseInt(str);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	/**
	 * This method checks if the hobby field is empty
	 * 
	 * @return true if filed is empty
	 * @return false if field is not empty
	 */
	public boolean requestFiledHobby() {
		if (studenthobbyTextField.getText().trim().isEmpty()) {
			asteriskhobbyLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskhobbyLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the precautions field is empty
	 * 
	 * @return true if filed is empty
	 * @return false if field is not empty
	 */
	public boolean requestFiledPrecautions() {
		if (descriptionprecautionsTextArea.getText().trim().isEmpty()) {
			asterisksprecautionsLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asterisksprecautionsLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

}
