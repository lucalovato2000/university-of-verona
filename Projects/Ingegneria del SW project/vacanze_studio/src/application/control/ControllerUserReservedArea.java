package application.control;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ResourceBundle;

import application.Main;
import application.model.DBConnect;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * This class allows you to control the user area
 * 
 */
public class ControllerUserReservedArea implements Initializable {

	@FXML
	private Button holidayButton;
	@FXML
	private Button certificateButton;
	@FXML
	private Button questionnaireButton;
	@FXML
	private Button logoutButton;
	@FXML
	private Button profilechangesButton;
	@FXML
	private Button viewHolidaysMadeButton;
	@FXML
	private Button areauserButton;
	@FXML
	private Button compileSurvey;

	public ControllerUserReservedArea() {
	}

	public void createReservedAreaForm() {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/application/view/reserved_area_user.fxml"));

			Stage reservedArea = new Stage();

			reservedArea.titleProperty();
			reservedArea.setTitle("Reserved Area");
			reservedArea.initStyle(StageStyle.UNDECORATED);
			reservedArea.setScene(new Scene(root, 536, 560));
			reservedArea.show();

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		holidayButton.setEffect(new DropShadow(5, Color.WHITE));
		certificateButton.setEffect(new DropShadow(5, Color.WHITE));
		questionnaireButton.setEffect(new DropShadow(5, Color.WHITE));
		profilechangesButton.setEffect(new DropShadow(5, Color.WHITE));
		viewHolidaysMadeButton.setEffect(new DropShadow(5, Color.WHITE));
		areauserButton.setEffect(new DropShadow(5, Color.WHITE));
		compileSurvey.setEffect(new DropShadow(5, Color.WHITE));
		areauserButton.setVisible(false);
	}

	/**
	 * This method allows you to return home page (area user)
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void areauserButtonOnAction(ActionEvent event) throws IOException {
		Main areauser = new Main();
		areauser.changeSceneWithDimension("/application/view/reserved_area_user.fxml", 536, 560);
	}

	/**
	 * This method allows you to switch to the holiday view
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void bookingHolidayOnAction(ActionEvent event) throws IOException {
		Main listholidays = new Main();
		listholidays.changeSceneWithDimension("/application/view/view_holidays_user.fxml", 600, 422);
	}

	/**
	 * This method allows you to go back
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void backButtonOnAction(ActionEvent event) throws IOException {
		Main backButton = new Main();
		backButton.changeSceneWithDimension("/application/view/reserved_area_user.fxml", 536, 560);
	}

	/**
	 * This method allows you to log out of the scene
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void logoutButtonOnAction(ActionEvent event) throws IOException {
		Main logout = new Main();
		logout.changeSceneWithDimension("/application/view/login_or_registration.fxml", 536, 560);
	}

	/**
	 * This method allows you to go to the credentials editing screen
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void nextProfileChangesOnAction(ActionEvent event) throws IOException {
		Main profilechanges = new Main();
		profilechanges.changeSceneWithDimension("/application/view/change_personal_data_user.fxml", 536, 561);
	}

	/**
	 * This method allows you to view the holidays you have taken
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void viewHolidaysMadeButtonOnAction(ActionEvent event) throws IOException {
		Main viewHolidaysMade = new Main();
		viewHolidaysMade.changeSceneWithDimension("/application/view/view_holiday_made_user.fxml", 536, 561);
	}

	/**
	 * This method allows you to view language certificates
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void certificateButtonOnAction(ActionEvent event) throws IOException {
		Main certificate = new Main();
		certificate.changeSceneWithDimension("/application/view/view_certificate.fxml", 536, 560);
	}

	/**
	 * This method allows you to view questionnaire
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void questionnaireButtonOnAction(ActionEvent event) throws IOException {
		Main questionnaire = new Main();
		questionnaire.changeSceneWithDimension("/application/view/view_questionnaire_user.fxml", 536, 560);
	}

	/**
	 * This method shows the errors of TextField
	 * 
	 * @param newChangeTextField
	 * @param asterisk
	 * @param error
	 * @return true if field is empty
	 */
	public boolean requestTextField(TextField newChangeTextField, Label asterisk, Label error) {
		if (!(newChangeTextField.getText().isEmpty())) {
			asterisk.setText("");
			error.setText("");
			return false;
		} else {
			asterisk.setText("*");
			error.setText("(*) Required field");
			return true;
		}
	}

	/**
	 * This method shows the errors of TextField
	 * 
	 * @param newChangeTextField
	 * @param asteriskLabel
	 * @param requiredLabel
	 */
	public void TextField(TextField newChangeTextField, Label asteriskLabel, Label requiredLabel) {
		if (requestTextField(newChangeTextField, asteriskLabel, requiredLabel) == true) {
			asteriskLabel.setText("");
			requiredLabel.setVisible(false);
		}
		requiredLabel.setText("(*) Required field");
	}

	/**
	 * This method searches the database for the requested value
	 * 
	 * @param select return parameter
	 * @param from   search the table
	 * @param type   field to change
	 * @param where  new field
	 * @return id of the found record
	 */
	public String searchIntoDataBase(String select, String from, String type, String where) {
		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();

		String query = "SELECT " + select + "\r\n" + " FROM " + from + "\r\n" + " WHERE " + type + " = \"" + where
				+ "\"";

		String idSearched = null;

		try {
			Statement statement = connectDB.createStatement();
			ResultSet resultRecord = statement.executeQuery(query);

			while (resultRecord.next()) {
				idSearched = resultRecord.getString("id");
			}

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
		return idSearched;
	}

	/**
	 * This method searches the database for the requested value
	 * 
	 * @param select return parameter
	 * @param from   search the table
	 * @param type   field to change
	 * @param where  new field
	 * @return id of the found record
	 */
	public String searchID(String select, String from, String type, String where) {
		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();

		String query = "SELECT " + select + "\r\n" + " FROM " + from + "\r\n" + " WHERE " + type + " = \"" + where
				+ "\"";

		String result = null;
		String Id = null;

		try {
			Statement statement = connectDB.createStatement();
			ResultSet resultRecord = statement.executeQuery(query);

			while (resultRecord.next()) {
				Id = resultRecord.getString("id");
				result += Id;
				Id = result.substring(4);
			}

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
		return Id;
	}

	/**
	 * This method searches the database for the requested value
	 * 
	 * @param select return parameter
	 * @param from   search the table
	 * @param type   field to change
	 * @param where  new field
	 * @return id of the found record
	 */
	public String searchIdHoliday(String select, String from, String type, String where) {
		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();

		String query = "SELECT " + select + "\r\n" + " FROM " + from + "\r\n" + " WHERE " + type + " = \"" + where
				+ "\"";

		String result = null;
		String Id = null;

		try {
			Statement statement = connectDB.createStatement();
			ResultSet resultRecord = statement.executeQuery(query);

			while (resultRecord.next()) {
				Id = resultRecord.getString("id_holiday");
				result += Id;
				Id = result.substring(4);
			}

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
		return Id;
	}

	/**
	 * This method searches the database for the requested value
	 * 
	 * @param select return parameter
	 * @param from   search the table
	 * @param type   field to change
	 * @param where  new field
	 * @return id of the found record
	 */
	public String searchRecordIntoDataBaseHoliday(String select, String from, String type, String where) {
		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();

		String query = "SELECT " + select + "\r\n" + " FROM " + from + "\r\n" + " WHERE " + type + " = \"" + where
				+ "\"";

		String name = null;
		Date departure_date = null;
		String duration = null;
		String destination = null;
		String language = null;
		String result = null;

		try {
			Statement statement = connectDB.createStatement();
			ResultSet resultRecord = statement.executeQuery(query);

			while (resultRecord.next()) {
				name = resultRecord.getString("name");
				departure_date = resultRecord.getDate("departure_date");
				duration = resultRecord.getString("duration");
				destination = resultRecord.getString("destination");
				language = resultRecord.getString("language");

				result = name + "|" + departure_date + "|" + duration + "|" + destination + "|" + language;
			}

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
		return result;
	}

	/**
	 * This method searches the database for the requested value
	 * 
	 * @param select return parameter
	 * @param from   search the table
	 * @param type   field to change
	 * @param where  new field
	 * @return id of the found record
	 */
	public String searchIdCertificate(String select, String from, String type, String where) {
		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();

		String query = "SELECT " + select + "\r\n" + " FROM " + from + "\r\n" + " WHERE " + type + " = \"" + where
				+ "\"";

		String result = null;
		String Id = null;

		try {
			Statement statement = connectDB.createStatement();
			ResultSet resultRecord = statement.executeQuery(query);

			while (resultRecord.next()) {
				Id = resultRecord.getString("id");
				result += Id;
				Id = result.substring(4);
			}

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
		return Id;
	}

	/**
	 * This method searches the database for the requested value
	 * 
	 * @param select return parameter
	 * @param from   search the table
	 * @param type   field to change
	 * @param where  new field
	 * @return id of the found record
	 */
	public String searchRecordIntoDataBaseCertificate(String select, String from, String type, String where) {
		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();

		String query = "SELECT " + select + "\r\n" + " FROM " + from + "\r\n" + " WHERE " + type + " = \"" + where
				+ "\"";

		String level = null;
		String language = null;
		String result = null;

		try {
			Statement statement = connectDB.createStatement();
			ResultSet resultRecord = statement.executeQuery(query);

			while (resultRecord.next()) {
				level = resultRecord.getString("level");
				language = resultRecord.getString("language");
				result = level + "|" + language;
			}

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
		return result;
	}

	/**
	 * This method searches the database for the requested value
	 * 
	 * @param select return parameter
	 * @param from   search the table
	 * @param type   field to change
	 * @param where  new field
	 * @return id of the found record
	 */
	public String searchRecordIntoDataBaseQuestionnaire(String select, String from, String type, String where) {
		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();

		String query = "SELECT " + select + "\r\n" + " FROM " + from + "\r\n" + " WHERE " + type + " = \"" + where
				+ "\"";

		String grade = null;
		String comments = null;
		String result = null;

		try {
			Statement statement = connectDB.createStatement();
			ResultSet resultRecord = statement.executeQuery(query);

			while (resultRecord.next()) {
				grade = resultRecord.getString("grade");
				comments = resultRecord.getString("comments");
				result = grade + "|" + comments;
			}

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
		return result;
	}

	/**
	 * This method looks for whether the user exists
	 * 
	 * @param username
	 * @return
	 */
	public boolean userExists(String username) {
		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();

		String query = "SELECT username FROM student WHERE username = " + "\"" + username + "\"";

		try {
			Statement statement = connectDB.createStatement();
			ResultSet resultRecord = statement.executeQuery(query);

			if (resultRecord.next()) {
				return true;
			} else {
				return false;
			}

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();

		}
		return false;

	}

	/**
	 * Logout from this stage
	 * 
	 * @param event return to the opening scene
	 * @throws IOException
	 */
	public void compileSurveyOnAction(ActionEvent event) throws IOException {
		Main survey = new Main();
		survey.changeSceneWithDimension("/application/view/survey.fxml", 536, 560);
	}

}
