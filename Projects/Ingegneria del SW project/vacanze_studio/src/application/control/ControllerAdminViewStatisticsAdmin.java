package application.control;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.Set;

import application.Main;
import application.model.DBConnect;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * This class allows view statistics holiday of students
 *
 */
public class ControllerAdminViewStatisticsAdmin implements Initializable {

	@FXML
	private TableView<TableViewStatistics> TableView;
	@FXML
	private TableColumn<TableViewStatistics, String> nameTableView;
	@FXML
	private TableColumn<TableViewStatistics, String> gradeTableView;
	@FXML
	private TableColumn<TableViewStatistics, String> commentTableView;
	@FXML
	private LineChart<String, Double> lineChart;
	@FXML
	private Button showButton;
	@FXML
	private Button logoutButton;
	@FXML
	private Label avarageLabel;
	@FXML
	private Button backButton;

	XYChart.Series<String, Double> series = new Series<String, Double>();
	ObservableList<TableViewStatistics> oblistTableView = FXCollections.observableArrayList();

	public void createStatisticsForm() {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/application/view/view_statistics_admin.fxml"));

			Stage statistics = new Stage();

			statistics.titleProperty();
			statistics.setTitle("Statistics");
			statistics.initStyle(StageStyle.UNDECORATED);
			statistics.setScene(new Scene(root, 536, 560));
			statistics.show();

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
	}

	public ControllerAdminViewStatisticsAdmin() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		nameTableView.setCellValueFactory((new PropertyValueFactory<>("name")));
		gradeTableView.setCellValueFactory((new PropertyValueFactory<>("grade")));
		commentTableView.setCellValueFactory((new PropertyValueFactory<>("comments")));
		TableView.setItems(oblistTableView);

		series.setName("Average rating for vacation");

	}

	/**
	 * This method allows the administrator to return to the previous scene
	 * 
	 * @param event
	 * @throws IOException
	 */
	public void backButtonOnAction(ActionEvent event) throws IOException {
		Main back = new Main();
		back.changeSceneWithDimension("/application/view/reserved_area_admin.fxml", 538, 560);
	}

	/**
	 * Logout from the stage
	 * 
	 * @param event return to the opening scene
	 * @throws IOException
	 */
	public void logoutButtonOnAction(ActionEvent event) throws IOException {
		Main loginOrRegistration = new Main();
		loginOrRegistration.changeSceneWithDimension("/application/view/login_or_registration.fxml", 536, 560);
	}

	/**
	 * This method allows view into objects
	 * 
	 */
	public void showButtonOnAction() {

		// Save all holiday ids
		ArrayList<String> idHoliday = searchIdHoliday();

		// Create a set with no repetitions, unordered
		Set<String> set = new HashSet<>(idHoliday);

		popularTable(set);

		popularLineChart(set);

		showButton.setVisible(false);
	}

	/**
	 * This method allows populate the graphic
	 * 
	 * @param set
	 */
	public void popularLineChart(Set<String> set) {
		ArrayList<Double> grade = null;
		ArrayList<String> nameHoliday = null;
		double average = 0;
		double sum = 0;
		String holidayName = null;

		for (String value : set) {
			grade = searchGradeHoliday(value);
			nameHoliday = searchNameHoliday(value);

			for (String name : nameHoliday) {
				holidayName = name;
				for (Double vote : grade) {
					sum = sum + vote;
					average = calculateAverage(sum, grade.size());
				}
			}
			series.getData().add(new XYChart.Data<String, Double>(holidayName, average));
			average = 0;
			sum = 0;
		}

		lineChart.setAnimated(false);
		lineChart.getData().add(series);
	}

	/**
	 * This method allows populate the table
	 * 
	 * @param set
	 */
	public void popularTable(Set<String> set) {
		ArrayList<Double> grade = null;
		ArrayList<String> comments = null;
		ArrayList<String> nameHoliday = null;
		double average = 0;
		double sum = 0;
		String holidayName = null;
		String note = null;
		for (String value : set) {
			grade = searchGradeHoliday(value);
			comments = searchCommentsHoliday(value);
			nameHoliday = searchNameHoliday(value);

			for (String name : nameHoliday) {
				holidayName = name;
				for (Double vote : grade) {
					sum = sum + vote;
					average = calculateAverage(sum, grade.size());
				}
			}

			for (String comment : comments) {
				note = comment;
				oblistTableView.add(new TableViewStatistics(holidayName, average, note));
			}
			average = 0;
			sum = 0;
		}
	}

	/**
	 * This method allows you to have the average of the students satisfaction marks
	 * on vacation
	 * 
	 * @param number
	 * @param size
	 * @return
	 */
	private double calculateAverage(Double number, Integer size) {
		double result = 0;
		result = number / size;
		return result;
	}

	/**
	 * This method searches the database for the requested value
	 * 
	 * @param select return parameter
	 * @param from   search the table
	 * @param type   field to change
	 * @param where  new field
	 * @return id of the found record
	 */
	public ArrayList<String> searchNameHoliday(String idHoliday) {
		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();

		String query = "SELECT  name FROM  holiday WHERE id = " + idHoliday;

		ArrayList<String> result = new ArrayList<>();
		String comments = null;

		try {
			Statement statement = connectDB.createStatement();
			ResultSet resultRecord = statement.executeQuery(query);

			while (resultRecord.next()) {
				comments = resultRecord.getString("name");
				result.add(comments);
			}

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
		return result;
	}

	/**
	 * This method searches the database for the requested value
	 * 
	 * @param select return parameter
	 * @param from   search the table
	 * @param type   field to change
	 * @param where  new field
	 * @return id of the found record
	 */
	public ArrayList<String> searchCommentsHoliday(String idHoliday) { // WHERE id_holiday = " + idHoliday;
		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();

		String query = "SELECT  comments FROM  survey WHERE id_holiday =" + idHoliday;

		ArrayList<String> result = new ArrayList<>();
		String comments = null;

		try {
			Statement statement = connectDB.createStatement();
			ResultSet resultRecord = statement.executeQuery(query);

			while (resultRecord.next()) {
				comments = resultRecord.getString("comments");
				result.add(comments);
			}

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
		return result;
	}

	/**
	 * This method searches the database for the requested value
	 * 
	 * @param select return parameter
	 * @param from   search the table
	 * @param type   field to change
	 * @param where  new field
	 * @return id of the found record
	 */
	public ArrayList<Double> searchGradeHoliday(String idHoliday) {
		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();

		String query = "SELECT  grade FROM  survey WHERE id_holiday = " + idHoliday;

		ArrayList<Double> result = new ArrayList<>();
		double grade = 0;

		try {
			Statement statement = connectDB.createStatement();
			ResultSet resultRecord = statement.executeQuery(query);

			while (resultRecord.next()) {
				grade = resultRecord.getInt("grade");
				result.add(grade);
			}

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
		return result;
	}

	/**
	 * This method searches the database for the requested value
	 * 
	 * @param select return parameter
	 * @param from   search the table
	 * @param type   field to change
	 * @param where  new field
	 * @return id of the found record
	 */
	public ArrayList<String> searchIdHoliday() {
		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();

		String query = "SELECT  id_holiday FROM  survey ";

		ArrayList<String> result = new ArrayList<>();
		String Id = null;

		try {
			Statement statement = connectDB.createStatement();
			ResultSet resultRecord = statement.executeQuery(query);

			while (resultRecord.next()) {
				Id = resultRecord.getString("id_holiday");
				result.add(Id);
			}

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
		return result;
	}

	/**
	 * This class allows you to manage the table TableViewQuestionnaire
	 *
	 */
	public class TableViewStatistics {

		String name, comments;
		Double grade;

		public TableViewStatistics(String name, Double grade, String comments) {

			this.name = name;
			this.grade = grade;
			this.comments = comments;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Double getGrade() {
			return grade;
		}

		public void setGrade(Double grade) {
			this.grade = grade;
		}

		public String getComments() {
			return comments;
		}

		public void setComments(String comments) {
			this.comments = comments;
		}
	}
}
