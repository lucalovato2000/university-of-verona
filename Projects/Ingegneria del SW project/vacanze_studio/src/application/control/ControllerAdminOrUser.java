package application.control;

import java.io.IOException;

import application.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class ControllerAdminOrUser {

	@FXML
	private Button registerAdminButton;
	@FXML
	private Button registerUserButton;

	public ControllerAdminOrUser() {
	}

	/**
	 * This method allows you to connect to a new scene
	 * 
	 * @param event go to scene registration admin
	 * @throws IOException
	 */
	public void registerAdminButtonOnAction(ActionEvent event) throws IOException {
		Main login = new Main();
		login.changeSceneWithDimension("/application/view/registration_admin.fxml", 536, 560);
	}

	/**
	 * This method allows you to connect to a new scene
	 * 
	 * @param event go to scene registration user
	 * @throws IOException
	 */
	public void registerUserButtonOnAction(ActionEvent event) throws IOException {
		Main login = new Main();
		login.changeSceneWithDimension("/application/view/registration_user.fxml", 536, 560);
	}

	/**
	 * Return to the home page
	 * 
	 * @param event return to the opening scene
	 * @throws IOException
	 */
	public void backButtonOnAction(ActionEvent event) throws IOException {
		Main init = new Main();
		init.changeSceneWithDimension("/application/view/login_or_registration.fxml", 536, 560);
	}
}
