package application.control;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Timer;
import java.util.TimerTask;

import application.Main;
import application.model.DBConnect;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * This class allows you to register the user in the system
 */
public class ControllerRegistrationUser {

	@FXML
	private Button closeButton;
	@FXML
	private Label registrationMessageLabel;
	@FXML
	private PasswordField setPasswordField;
	@FXML
	private PasswordField confirmPasswordField;
	@FXML
	private Label confirmPasswordLabel;
	@FXML
	private TextField firstnameTextField;
	@FXML
	private TextField lastnameTextField;
	@FXML
	private TextField emailTextField;
	@FXML
	private TextField addressTextField;
	@FXML
	private TextField phonenumberTextField;
	@FXML
	private TextField usernameTextField;
	@FXML
	private Label asteriskfirstnameLabel;
	@FXML
	private Label asterisklastnameLabel;
	@FXML
	private Label asteriskemailLabel;
	@FXML
	private Label asteriskaddressLabel;
	@FXML
	private Label asteriskphonenumberLabel;
	@FXML
	private Label asteriskusernameLabel;
	@FXML
	private Label asteriskpasswordLabel;
	@FXML
	private Label asteriskconfirmpasswordLabel;
	@FXML
	private Label requiredfieldLabel;
	@FXML
	private Label confirmphonenumberLabel;
	@FXML
	private Button registerButton;
	@FXML
	private Button resetfieldButton;

	public ControllerRegistrationUser() {

	}

	/**
	 * This method allows you to register a new user
	 * 
	 * @param theName
	 * @param theSurname
	 * @param theEmail
	 * @param theAddress
	 * @param thePhone
	 * @param theUsername
	 * @param thePassword
	 * @return new registered user
	 * @throws NoSuchAlgorithmException
	 */
	public static Boolean setValue(String theName, String theSurname, String theEmail, String theAddress,
			String thePhone, String theUsername, String thePassword) throws NoSuchAlgorithmException {

		String name = theName;
		String surname = theSurname;
		String email = theEmail;
		String address = theAddress;
		String phone = thePhone;
		String username = theUsername;
		String password = thePassword;

		return registrationUser(name, surname, email, address, phone, username, password);

	}

	Timer timer = new Timer();

	/**
	 * This timer allows you to wait a second for the confirmation label to appear
	 * on the screen
	 * 
	 */
	TimerTask task = new TimerTask() {
		int counter = 1;

		public void run() {

			Platform.runLater(() -> {

				if (counter > 0) {
					counter--;
				} else {

					Main infouser = new Main();

					try {
						infouser.changeSceneWithDimension("/application/view/user_parent_preferences.fxml", 536, 560);
					} catch (IOException e) {
						e.printStackTrace();
					}

					timer.purge();
					timer.cancel();
				}
			});
		};
	};

	public void createAccountForm() {
		try {

			Parent root = FXMLLoader.load(getClass().getResource("/application/view/registration_user.fxml"));

			Stage registerStage = new Stage();

			registerStage.titleProperty();
			registerStage.setTitle("Registration User");
			registerStage.initStyle(StageStyle.UNDECORATED);
			registerStage.setScene(new Scene(root, 536, 560));
			registerStage.show();

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
	}

	/**
	 * This method allows you to reset field
	 * 
	 * @param event
	 */
	public void resetfieldButtonOnAction(ActionEvent event) {
		firstnameTextField.setText("");
		lastnameTextField.setText("");
		emailTextField.setText("");
		addressTextField.setText("");
		phonenumberTextField.setText("");
		usernameTextField.setText("");
		setPasswordField.setText("");
		confirmPasswordField.setText("");
		asteriskfirstnameLabel.setText("");
		asterisklastnameLabel.setText("");
		asteriskemailLabel.setText("");
		asteriskaddressLabel.setText("");
		asteriskphonenumberLabel.setText("");
		asteriskusernameLabel.setText("");
		asteriskpasswordLabel.setText("");
		asteriskconfirmpasswordLabel.setText("");
		requiredfieldLabel.setText("");
	}

	/**
	 * This method triggers close window
	 * 
	 * @param event close the button
	 */
	public void closeButtonOnAction(ActionEvent event) {
		Stage stage = (Stage) closeButton.getScene().getWindow();
		stage.close();
		Platform.exit();
	}

	/**
	 * This method triggers user registration
	 * 
	 * @param event confirm password or error password
	 * @throws NoSuchAlgorithmException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void registerButtonOnAction(ActionEvent event)
			throws NoSuchAlgorithmException, IOException, InterruptedException {

		String name = firstnameTextField.getText();
		String surname = lastnameTextField.getText();
		String email = emailTextField.getText();
		String phone = phonenumberTextField.getText();
		String username = usernameTextField.getText();
		String password = setPasswordField.getText();
		String address = addressTextField.getText();

		// This condition checks if the field is filled in, otherwise it gives an error
		if (requiredFieldFirstName() == false && requiredFieldLastName() == false && requiredFieldEmail() == false
				&& requestFiledAddress() == false && requestFiledNumber() == false && requestFiledUserName() == false
				&& requestFiledPassword() == false && requestFiledConfirmPassword() == false) {

			// This condition checks if the phone is a number
			if (isNumeric(phonenumberTextField.getText().trim()) == true) {
				confirmphonenumberLabel.setText("");

				// This condition checks if the password matches with the password confirmation
				if (setPasswordField.getText().equals(confirmPasswordField.getText())) {
					registrationUser(name, surname, email, address, phone, username, password);
					confirmPasswordLabel.setText("");

					registrationMessageLabel.setText("User has been registred successfully!");

					// Call the scheduler to wait a second from now
					timer.schedule(task, 0, 1000);

				} else {
					confirmPasswordLabel.setText("Password does not match");
				}
			} else {
				confirmphonenumberLabel.setText("Enter numbers only");
			}
		}
	}

	/**
	 * This method allows you to return to the admin or user choice
	 * 
	 * @param event return to the opening scene
	 * @throws IOException
	 */
	public void backButtonOnAction(ActionEvent event) throws IOException {
		Main back = new Main();
		back.changeSceneWithDimension("/application/view/admin_or_user.fxml", 536, 560);
	}

	/**
	 * This method takes care of registering the user in the database
	 * 
	 * @param name
	 * @param surname
	 * @param email
	 * @param address
	 * @param phone
	 * @param username
	 * @param password
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public static Boolean registrationUser(String name, String surname, String email, String address, String phone,
			String username, String password) throws NoSuchAlgorithmException {

		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();

		if (name.isEmpty() || name.trim().isEmpty()) {
			return null;
		}

		if (surname.isEmpty() || surname.trim().isEmpty()) {
			surname = null;
		}

		if (email.isEmpty() || email.trim().isEmpty()) {
			email = null;
		}

		if (phone.isEmpty() || phone.trim().isEmpty()) {
			phone = null;
		}

		if (username.isEmpty() || username.trim().isEmpty()) {
			username = null;
		}

		if (password.isEmpty() || password.trim().isEmpty()) {
			password = null;
		}

		if (address.isEmpty() || address.trim().isEmpty()) {
			address = null;
		}

		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(password.getBytes(StandardCharsets.UTF_8));
		byte[] digest = md.digest();
		String cryptedPassword = String.format("%064x", new BigInteger(1, digest));

		String insertFields = "INSERT INTO student(name, surname, email, phone_number, address, username, crypted_password) VALUES ('";
		String insertValues = name + "','" + surname + "','" + email + "','" + phone + "','" + address + "','"
				+ username + "','" + cryptedPassword + "')";
		String insertToRegister = insertFields + insertValues;

		try {
			Statement statement = connectDB.createStatement();
			statement.executeUpdate(insertToRegister);

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();

		}

		return true;
	}

	/**
	 * This method checks if the first name field is empty
	 * 
	 * @return true if filed is empty
	 * @return false if field is not empty
	 */
	public boolean requiredFieldFirstName() {
		if (firstnameTextField.getText().trim().isEmpty()) {
			asteriskfirstnameLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskfirstnameLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the last name field is empty
	 * 
	 * @return true if filed is empty
	 * @return false if field is not empty
	 */
	public boolean requiredFieldLastName() {
		if (lastnameTextField.getText().trim().isEmpty()) {
			asterisklastnameLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asterisklastnameLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the email field is empty
	 * 
	 * @return true if filed is empty
	 * @return false if field is not empty
	 */
	public boolean requiredFieldEmail() {
		if (emailTextField.getText().trim().isEmpty()) {
			asteriskemailLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskemailLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if address field is empty
	 * 
	 * @return true if filed is empty
	 * @return false if field is not empty
	 */
	public boolean requestFiledAddress() {
		if (addressTextField.getText().trim().isEmpty()) {
			asteriskaddressLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskaddressLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the phone number field is empty
	 * 
	 * @return true if filed is empty
	 * @return false if field is not empty
	 */
	public boolean requestFiledNumber() {
		if (phonenumberTextField.getText().trim().isEmpty()) {
			asteriskphonenumberLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskphonenumberLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the user name field is empty
	 * 
	 * @return true if filed is empty
	 * @return false if field is not empty
	 */
	public boolean requestFiledUserName() {
		if (usernameTextField.getText().trim().isEmpty()) {
			asteriskusernameLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskusernameLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the password field is empty
	 * 
	 * @return true if filed is empty
	 * @return false if field is not empty
	 */
	public boolean requestFiledPassword() {
		if (setPasswordField.getText().trim().isEmpty()) {
			asteriskpasswordLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskpasswordLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the confirm password field is empty
	 * 
	 * @return true if filed is empty
	 * @return false if field is not empty
	 */
	public boolean requestFiledConfirmPassword() {
		if (confirmPasswordField.getText().trim().isEmpty()) {
			asteriskconfirmpasswordLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskconfirmpasswordLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the string is only numbers
	 * 
	 * @param str
	 * @return true if only number
	 */
	public static boolean isNumeric(String str) {
		try {
			Long.parseLong(str);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
}
