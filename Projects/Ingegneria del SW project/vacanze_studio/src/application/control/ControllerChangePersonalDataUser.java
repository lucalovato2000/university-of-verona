package application.control;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ResourceBundle;

import application.Main;
import application.model.DBConnect;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

/**
 * This class allow user to change personal data
 */
public class ControllerChangePersonalDataUser extends ControllerUserReservedArea {

	@FXML
	private TextField searchusernameTextField;
	@FXML
	private TextField newNameTextField;
	@FXML
	private TextField newSurnameTextField;
	@FXML
	private TextField newEmailTextField;
	@FXML
	private TextField newAddressTextField;
	@FXML
	private TextField newPhoneNumberTextField;
	@FXML
	private TextField newUsernameTextField;
	@FXML
	private PasswordField newPasswordField;
	@FXML
	private PasswordField newConfirmPasswordField;
	@FXML
	private CheckBox FirstNameCheckBox;
	@FXML
	private CheckBox LastNameCheckBox;
	@FXML
	private CheckBox EmailCheckBox;
	@FXML
	private CheckBox AddressCheckBox;
	@FXML
	private CheckBox PhoneNumberCheckBox;
	@FXML
	private CheckBox UsernameCheckBox;
	@FXML
	private CheckBox NewPasswordCheckBox;
	@FXML
	private Label nowUsernameLabel;
	@FXML
	private Label newFirstNameLabel;
	@FXML
	private Label newLastNameLabel;
	@FXML
	private Label newEmailLabel;
	@FXML
	private Label newAddressLabel;
	@FXML
	private Label newPhoneNumberLabel;
	@FXML
	private Label newUsernameLabel;
	@FXML
	private Label newPasswordLabel;
	@FXML
	private Label newConfirmPasswordLabel;
	@FXML
	private Label requiredfieldLabel;
	@FXML
	private Label PasswordDoNotMatchLabel;
	@FXML
	private Button changeButton;
	@FXML
	private Label nameNotExistLabel;
	@FXML
	private Label ChangeSuccessfullyLabel;
	@FXML
	private Button viewChangeButton;
	@FXML
	private Label confirmNewPassowrdLabel;

	public String idUser = null;
	public String UsernameUser = null;

	public ControllerChangePersonalDataUser() {
		super();
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		super.initialize(arg0, arg1);
		newNameTextField.setVisible(false);
		newSurnameTextField.setVisible(false);
		newEmailTextField.setVisible(false);
		newAddressTextField.setVisible(false);
		newPhoneNumberTextField.setVisible(false);
		newUsernameTextField.setVisible(false);
		newPasswordField.setVisible(false);
		newConfirmPasswordField.setVisible(false);
		requiredfieldLabel.setVisible(false);
		newFirstNameLabel.setVisible(false);
		newLastNameLabel.setVisible(false);
		newEmailLabel.setVisible(false);
		newAddressLabel.setVisible(false);
		newPhoneNumberLabel.setVisible(false);
		newUsernameLabel.setVisible(false);
		newPasswordLabel.setVisible(false);
		newConfirmPasswordLabel.setVisible(false);

		// viewChangeButton.setVisible(false);

		confirmNewPassowrdLabel.setVisible(false);

	}

	/**
	 * This method lets you know if the checkbox has been selected
	 * 
	 * @param checkbox
	 * @return true if @param is selected
	 */
	private boolean CheckBoxIsSelect(CheckBox checkbox) {
		if (checkbox.isSelected() == true) {
			return true;
		}
		return false;
	}

	/**
	 * This method allows you to show objects when the checkbox is activated
	 * 
	 * @param event
	 */
	public void shownewNameTextFieldOnAction(ActionEvent event) {
		if (CheckBoxIsSelect(FirstNameCheckBox) == true) {
			newNameTextField.setVisible(true);
			newFirstNameLabel.setVisible(true);
			newFirstNameLabel.setText("");
			requiredfieldLabel.setVisible(true);
			requiredfieldLabel.setText("");
		}

		if (CheckBoxIsSelect(FirstNameCheckBox) == false) {
			newNameTextField.setVisible(false);
			newFirstNameLabel.setVisible(false);
			newNameTextField.setText("");
			requiredfieldLabel.setVisible(false);
		}
	}

	/**
	 * This method allows you to show objects when the checkbox is activated
	 * 
	 * @param event
	 */
	public void shownewSurnameTextFieldOnAction(ActionEvent event) {
		if (CheckBoxIsSelect(LastNameCheckBox) == true) {
			newSurnameTextField.setVisible(true);
			newLastNameLabel.setVisible(true);
			newLastNameLabel.setText("");
			requiredfieldLabel.setVisible(true);
			requiredfieldLabel.setText("");
		}

		if (CheckBoxIsSelect(LastNameCheckBox) == false) {
			newSurnameTextField.setVisible(false);
			newLastNameLabel.setVisible(false);
			newSurnameTextField.setText("");
			requiredfieldLabel.setVisible(false);
		}
	}

	/**
	 * This method allows you to show objects when the checkbox is activated
	 * 
	 * @param event
	 */
	public void shownewEmailTextFieldOnAction(ActionEvent event) {
		if (CheckBoxIsSelect(EmailCheckBox) == true) {
			newEmailTextField.setVisible(true);
			newEmailLabel.setVisible(true);
			newEmailLabel.setText("");
			requiredfieldLabel.setVisible(true);
			requiredfieldLabel.setText("");
		}

		if (CheckBoxIsSelect(EmailCheckBox) == false) {
			newEmailTextField.setVisible(false);
			newEmailLabel.setVisible(false);
			newEmailTextField.setText("");
			requiredfieldLabel.setVisible(false);
		}
	}

	/**
	 * This method allows you to show objects when the checkbox is activated
	 * 
	 * @param event
	 */
	public void shownewAddressTextFieldOnAction(ActionEvent event) {
		if (CheckBoxIsSelect(AddressCheckBox) == true) {
			newAddressTextField.setVisible(true);
			newAddressLabel.setVisible(true);
			newAddressLabel.setText("");
			requiredfieldLabel.setVisible(true);
			requiredfieldLabel.setText("");

		}

		if (CheckBoxIsSelect(AddressCheckBox) == false) {
			newAddressTextField.setVisible(false);
			newAddressLabel.setVisible(false);
			newAddressTextField.setText("");
			requiredfieldLabel.setVisible(false);
		}
	}

	/**
	 * This method allows you to show objects when the checkbox is activated
	 * 
	 * @param event
	 */
	public void shownewPhoneNumberTextFieldOnAction(ActionEvent event) {
		if (CheckBoxIsSelect(PhoneNumberCheckBox) == true) {
			newPhoneNumberTextField.setVisible(true);
			newPhoneNumberLabel.setVisible(true);
			newPhoneNumberLabel.setText("");
			requiredfieldLabel.setVisible(true);
			requiredfieldLabel.setText("");
		}

		if (CheckBoxIsSelect(PhoneNumberCheckBox) == false) {
			newPhoneNumberTextField.setVisible(false);
			newPhoneNumberLabel.setVisible(false);
			newPhoneNumberTextField.setText("");
			requiredfieldLabel.setVisible(false);
		}
	}

	/**
	 * This method allows you to show objects when the checkbox is activated
	 * 
	 * @param event
	 */
	public void shownewUsernameTextFieldOnAction(ActionEvent event) {
		if (CheckBoxIsSelect(UsernameCheckBox) == true) {
			newUsernameTextField.setVisible(true);
			newUsernameLabel.setVisible(true);
			newUsernameLabel.setText("");
			requiredfieldLabel.setVisible(true);
			requiredfieldLabel.setText("");

		}

		if (CheckBoxIsSelect(UsernameCheckBox) == false) {
			newUsernameTextField.setVisible(false);
			newUsernameLabel.setVisible(false);
			newUsernameTextField.setText("");
			requiredfieldLabel.setVisible(false);
		}
	}

	/**
	 * This method allows you to show objects when the checkbox is activated
	 * 
	 * @param event
	 */
	public void shownewPasswordTextFieldOnAction(ActionEvent event) {
		if (CheckBoxIsSelect(NewPasswordCheckBox) == true) {
			newPasswordField.setVisible(true);
			newPasswordLabel.setVisible(true);
			newPasswordLabel.setText("");
			requiredfieldLabel.setVisible(true);
			requiredfieldLabel.setText("");
			confirmNewPassowrdLabel.setVisible(true);
			newConfirmPasswordField.setVisible(true);
			newConfirmPasswordLabel.setVisible(true);
			newConfirmPasswordLabel.setText("");

		}

		if (CheckBoxIsSelect(NewPasswordCheckBox) == false) {
			newPasswordField.setVisible(false);
			newPasswordLabel.setVisible(false);
			newPasswordField.setText("");
			newConfirmPasswordField.setText("");
			requiredfieldLabel.setVisible(false);
			confirmNewPassowrdLabel.setVisible(false);
			newConfirmPasswordField.setVisible(false);
			newConfirmPasswordLabel.setVisible(false);
		}
	}

	/**
	 * This method reveals whether the field has been clicked
	 * 
	 * @param click with mouse the field
	 */
	public void searchusernameTextFieldOnAction(MouseEvent event) {
		if (searchusernameTextField.getText().isEmpty()) {
			TextField(searchusernameTextField, nowUsernameLabel, requiredfieldLabel);
		} else {
			requiredfieldLabel.setText("");
			nowUsernameLabel.setText("");
		}
	}

	/**
	 * This method reveals whether the field has been clicked
	 * 
	 * @param click with mouse the field
	 */
	public void newNameTextFieldOnAction(MouseEvent event) {
		TextField(newNameTextField, newFirstNameLabel, requiredfieldLabel);
	}

	/**
	 * This method reveals whether the field has been clicked
	 * 
	 * @param click with mouse the field
	 */
	public void newSurnameTextFieldOnAction(MouseEvent event) {
		TextField(newSurnameTextField, newLastNameLabel, requiredfieldLabel);
	}

	/**
	 * This method reveals whether the field has been clicked
	 * 
	 * @param click with mouse the field
	 */
	public void newEmailTextFieldOnAction(MouseEvent event) {
		TextField(newEmailTextField, newEmailLabel, requiredfieldLabel);
	}

	/**
	 * This method reveals whether the field has been clicked
	 * 
	 * @param click with mouse the field
	 */
	public void newAddressTextFieldOnAction(MouseEvent event) {
		TextField(newAddressTextField, newAddressLabel, requiredfieldLabel);
	}

	/**
	 * This method reveals whether the field has been clicked
	 * 
	 * @param click with mouse the field
	 */
	public void newPhoneNumberTextFieldOnAction(MouseEvent event) {
		TextField(newPhoneNumberTextField, newPhoneNumberLabel, requiredfieldLabel);
	}

	/**
	 * This method reveals whether the field has been clicked
	 * 
	 * @param click with mouse the field
	 */
	public void newUsernameTextFieldOnAction(MouseEvent event) {
		TextField(newUsernameTextField, newUsernameLabel, requiredfieldLabel);
	}

	/**
	 * This method reveals whether the field has been clicked
	 * 
	 * @param click with mouse the field
	 */
	public void newPasswordTextFieldOnAction(MouseEvent event) {
		TextField(newPasswordField, newPasswordLabel, requiredfieldLabel);
		PasswordDoNotMatchLabel.setText("");
		requiredfieldLabel.setText("");
	}

	/**
	 * This method reveals whether the field has been clicked
	 * 
	 * @param click with mouse the field
	 */
	public void newConfirmPasswordTextFieldOnAction(MouseEvent event) {
		TextField(newConfirmPasswordField, newConfirmPasswordLabel, requiredfieldLabel);
		PasswordDoNotMatchLabel.setText("");
		requiredfieldLabel.setText("");
	}

	/**
	 * This method records the data in the database
	 * 
	 * @param event click with mouse the button
	 * @throws NoSuchAlgorithmException
	 */
	public void changeButtonOnAction(ActionEvent event) throws NoSuchAlgorithmException {

		if ((requestTextField(searchusernameTextField, nowUsernameLabel, requiredfieldLabel) == true)) {

			requestTextField(newNameTextField, newFirstNameLabel, requiredfieldLabel);
			requestTextField(newSurnameTextField, newLastNameLabel, requiredfieldLabel);
			requestTextField(newEmailTextField, newEmailLabel, requiredfieldLabel);
			requestTextField(newAddressTextField, newAddressLabel, requiredfieldLabel);
			requestTextField(newPhoneNumberTextField, newPhoneNumberLabel, requiredfieldLabel);
			requestTextField(newUsernameTextField, newUsernameLabel, requiredfieldLabel);
			requestTextField(newPasswordField, newPasswordLabel, requiredfieldLabel);
			requestTextField(newConfirmPasswordField, newConfirmPasswordLabel, requiredfieldLabel);
			requiredfieldLabel.setVisible(true);

		} else if (userExists(searchusernameTextField.getText()) == false) {
			requiredfieldLabel.setVisible(true);
			requiredfieldLabel.setText("Invalid user!");
			nowUsernameLabel.setText("*");
		} else if ((requestTextField(searchusernameTextField, nowUsernameLabel, requiredfieldLabel) == false)) {

			requiredfieldLabel.setVisible(false); // default

			// If at least one checkbox has not been selected

			if (CheckBoxIsSelect(FirstNameCheckBox) == false && CheckBoxIsSelect(LastNameCheckBox) == false
					&& CheckBoxIsSelect(EmailCheckBox) == false && (CheckBoxIsSelect(AddressCheckBox)) == false
					&& (CheckBoxIsSelect(PhoneNumberCheckBox)) == false
					&& CheckBoxIsSelect(NewPasswordCheckBox) == false && CheckBoxIsSelect(UsernameCheckBox) == false) {

				requiredfieldLabel.setVisible(true);
				requiredfieldLabel.setText("");
				requiredfieldLabel.setText("Select an item");

			} else {

				// If at least one check box is selected

				requiredfieldLabel.setVisible(true);

				if (requestTextField(newNameTextField, newFirstNameLabel, requiredfieldLabel) == false) {
					RegisterNewName();
					newNameTextField.setVisible(false);
					FirstNameCheckBox.setSelected(false);
					requiredfieldLabel.setVisible(false);
					newNameTextField.setText("");
					Successfully();
				}

				if (requestTextField(newSurnameTextField, newLastNameLabel, requiredfieldLabel) == false) {
					RegisterNewSurname();
					newSurnameTextField.setVisible(false);
					LastNameCheckBox.setSelected(false);
					requiredfieldLabel.setVisible(false);
					newSurnameTextField.setText("");
					Successfully();
				}
				if (requestTextField(newEmailTextField, newEmailLabel, requiredfieldLabel) == false) {
					RegisterNewEmail();
					newEmailTextField.setVisible(false);
					EmailCheckBox.setSelected(false);
					requiredfieldLabel.setVisible(false);
					newEmailTextField.setText("");
					Successfully();
				}

				if (requestTextField(newAddressTextField, newAddressLabel, requiredfieldLabel) == false) {
					RegisterNewAddress();
					newAddressTextField.setVisible(false);
					AddressCheckBox.setSelected(false);
					requiredfieldLabel.setVisible(false);
					newAddressTextField.setText("");
					Successfully();
				}

				if (requestTextField(newPhoneNumberTextField, newPhoneNumberLabel, requiredfieldLabel) == false) {
					if (isNumeric(newPhoneNumberTextField.getText()) == true) {
						RegisterNewPhone();
						newPhoneNumberTextField.setVisible(false);
						PhoneNumberCheckBox.setSelected(false);
						requiredfieldLabel.setVisible(false);
						newPhoneNumberTextField.setText("");
						Successfully();
					} else {
						newPhoneNumberLabel.setText("*");
						requiredfieldLabel.setVisible(true);
					}
				}

				if (requestTextField(newUsernameTextField, newUsernameLabel, requiredfieldLabel) == false) {
					RegisterNewUsername();
					newUsernameTextField.setVisible(false);

					UsernameCheckBox.setSelected(false);
					requiredfieldLabel.setVisible(false);
					newUsernameLabel.setVisible(false);
					Successfully();
				}

				if (requestTextField(newConfirmPasswordField, newConfirmPasswordLabel, requiredfieldLabel) == false
						|| requestTextField(newPasswordField, newPasswordLabel, requiredfieldLabel) == false) {
					if (PasswordCorrect() == true) {
						RegisterNewPassword();
						NewPasswordCheckBox.setSelected(false);
						newPasswordField.setVisible(false);
						newConfirmPasswordField.setVisible(false);

						NewPasswordCheckBox.setSelected(false);
						requiredfieldLabel.setVisible(false);
						confirmNewPassowrdLabel.setVisible(false);
						newConfirmPasswordLabel.setVisible(false);
						Successfully();
					}
				}

				newUsernameTextField.setText("");
				searchusernameTextField.setText("");
				newPasswordField.setText("");
				newConfirmPasswordField.setText("");
			}
		}
	}

	/**
	 * This method write into label "Modified successfully" and ability the button
	 * viewChangeButton
	 * 
	 */
	private void Successfully() {
		ChangeSuccessfullyLabel.setText("Modified successfully!");
		// viewChangeButton.setVisible(true);
	}

	public void viewChangeOnAction(ActionEvent event) throws IOException {
		Main nextStep = new Main();
		nextStep.changeSceneWithDimension("/application/view/change_allergy_user.fxml", 536, 561);
	}

	/**
	 * This method gets a substring with the id inside
	 * 
	 * @param result string query
	 * @return id
	 */
	private String ValueId(String result) {
		int k = 0;
		String id = result.substring(4);
		while (k < id.length()) {
			if (id.charAt(k) == 'U') {
				break;
			}
			k++;
		}
		id = result.substring(4, 4 + k);
		return id;
	}

	/**
	 * Select the username in the string
	 * 
	 * @param result string query
	 * @return username
	 */
	private String ValueUsername(String result) {
		int k = 0;
		String username = result.substring(4);
		while (k < username.length()) {
			if (username.charAt(k) == 'U') {
				break;
			}
			k++;
		}
		username = result.substring(4 + k + 10);
		return username;
	}

	/**
	 * This method search username into database
	 * 
	 * @param select
	 * @param from
	 * @param type
	 * @param where
	 * @return
	 */
	private String searchUsernameIntoDataBase(String select, String from, String type, String where) {
		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();

		String query = "SELECT " + select + "\r\n" + " FROM " + from + "\r\n" + " WHERE " + type + " = \"" + where
				+ "\"";
		String usernameSearched = null;
		int idSearched = 0;

		try {
			Statement statement = connectDB.createStatement();
			ResultSet resultRecord = statement.executeQuery(query);

			while (resultRecord.next()) {
				usernameSearched = resultRecord.getString("username");
				idSearched = resultRecord.getInt("id");
			}

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
		String result = "Id: " + idSearched + " Username: " + usernameSearched;
		return result;
	}

	/**
	 * This method allows you to register a new field in the database
	 * 
	 * @param table
	 * @param nowusername
	 * @param field
	 * @param newfield
	 * @param id
	 */
	private void RegisterNewField(String table, String nowusername, String field, String newfield, int id) {
		DBConnect connectNow = DBConnect.getInstance();
		Connection connectDB = connectNow.getConnection();

		if (!(nowusername.equals(""))) {

			String query = "UPDATE " + table + " SET " + field + "=" + "\"" + newfield + "\"" + " WHERE id =" + "\""
					+ id + "\";";

			try {
				Statement statement = connectDB.createStatement();
				statement.executeUpdate(query);
			} catch (Exception e) {
				e.printStackTrace();
				e.getCause();
			}
		}
	}

	/**
	 * This method register the new name
	 * 
	 */
	public void RegisterNewName() {
		RegisterNewField("student", searchusernameTextField.getText(), "name", newNameTextField.getText(),
				Integer.parseInt(searchIntoDataBase("id", "student", "username", searchusernameTextField.getText())));
	}

	/**
	 * This method register the new surname
	 * 
	 */
	public void RegisterNewSurname() {
		RegisterNewField("student", searchusernameTextField.getText(), "surname", newSurnameTextField.getText(),
				Integer.parseInt(searchIntoDataBase("id", "student", "username", searchusernameTextField.getText())));
	}

	/**
	 * This method register the new email
	 * 
	 */
	public void RegisterNewEmail() {
		RegisterNewField("student", searchusernameTextField.getText(), "email", newEmailTextField.getText(),
				Integer.parseInt(searchIntoDataBase("id", "student", "username", searchusernameTextField.getText())));
	}

	/**
	 * This method register the new address
	 * 
	 */
	public void RegisterNewAddress() {
		RegisterNewField("student", searchusernameTextField.getText(), "address", newAddressTextField.getText(),
				Integer.parseInt(searchIntoDataBase("id", "student", "username", searchusernameTextField.getText())));
	}

	/**
	 * This method register the new phone
	 * 
	 */
	public void RegisterNewPhone() {
		RegisterNewField("student", searchusernameTextField.getText(), "phone_number",
				newPhoneNumberTextField.getText(),
				Integer.parseInt(searchIntoDataBase("id", "student", "username", searchusernameTextField.getText())));
	}

	/**
	 * This method registers and returns the id of the new username
	 * 
	 */
	public void RegisterNewUsername() {
		String result = searchUsernameIntoDataBase("*", "student", "username", searchusernameTextField.getText());
		String id = ValueId(result);
		String username = ValueUsername(result);
		idUser = id;
		UsernameUser = username;
		RegisterNewField("student", searchusernameTextField.getText(), "username", newUsernameTextField.getText(),
				Integer.parseInt(searchIntoDataBase("id", "student", "username", searchusernameTextField.getText())));
	}

	/**
	 * This method register the new password
	 * 
	 */
	public void RegisterNewPassword() throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(newPasswordField.getText().getBytes(StandardCharsets.UTF_8));
		byte[] digest = md.digest();
		String cryptedPassword = String.format("%064x", new BigInteger(1, digest));

		if (newUsernameTextField.getText().isEmpty()) {
			RegisterNewField("student", searchusernameTextField.getText(), "crypted_password", cryptedPassword, Integer
					.parseInt(searchIntoDataBase("id", "student", "username", searchusernameTextField.getText())));
		} else {
			RegisterNewField("student", newUsernameTextField.getText(), "crypted_password", cryptedPassword,
					Integer.parseInt(searchIntoDataBase("id", "student", "username", newUsernameTextField.getText())));
		}
	}

	/**
	 * This method checks if the string is only numbers
	 * 
	 * @param str
	 * @return true if only number
	 */
	private static boolean isNumeric(String str) {
		try {
			Long.parseLong(str);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	/**
	 * This method detects if the password matches the password confirmation
	 * 
	 * @return true if password matches the password confirmation
	 */
	private boolean PasswordCorrect() {
		if (newPasswordField.getText().trim().equals(newConfirmPasswordField.getText().trim())) {
			newPasswordLabel.setText("");
			newConfirmPasswordLabel.setText("");
			PasswordDoNotMatchLabel.setVisible(false);
			return true;
		}
		newPasswordLabel.setText("*");
		newConfirmPasswordLabel.setText("*");
		PasswordDoNotMatchLabel.setVisible(true);
		PasswordDoNotMatchLabel.setText("(*) Password do not match");
		return false;
	}
}
