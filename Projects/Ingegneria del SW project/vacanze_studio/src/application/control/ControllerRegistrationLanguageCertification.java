package application.control;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Statement;

import application.Main;
import application.model.DBConnect;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ControllerRegistrationLanguageCertification {

	@FXML
	private Button registerMeButton;
	@FXML
	private Button closeButton;
	@FXML
	private TextField levelTextField;
	@FXML
	private TextField languageTextField;
	@FXML
	private TextField idStudentTextField;
	@FXML
	private Label registrationMessageLabel;
	@FXML
	private Label asteriskLevelLabel;
	@FXML
	private Label asteriskLanguageLabel;
	@FXML
	private Label asteriskIdStudentLabel;
	@FXML
	private Label requiredfieldLabel;
	@FXML
	private Label confirmIdStudentLabel;

	public ControllerRegistrationLanguageCertification() {
	}

	/**
	 * Return the current stage
	 */
	public void createLanguageCertificationAccountForm() {
		try {
			Parent root = FXMLLoader
					.load(getClass().getResource("/application/view/registration_language_certification.fxml"));

			Stage registerStage = new Stage();

			registerStage.initStyle(StageStyle.UNDECORATED);
			registerStage.setScene(new Scene(root, 536, 327));
			registerStage.show();

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
	}

	/**
	 * Change the stage to the next scene
	 * 
	 * @param event return to the opening scene
	 */
	public void nextButtonOnAction(ActionEvent event) throws IOException {
		Main listStudents = new Main();
		listStudents.changeSceneWithDimension("/application/view/view_students.fxml", 682, 422);
	}

	/**
	 * Cheched if the values in the textfield are valid
	 * 
	 * @param event return to the opening scene
	 * @throws NoSuchAlgorithmException
	 */
	public void registerMeButtonOnAction(ActionEvent event) throws NoSuchAlgorithmException {

		registrationMessageLabel.setText("");

		if (requiredFieldFirstLevel() == false && requiredFieldLanguage() == false
				&& requiredFieldIdStudent() == false) {

			if (isNumeric(idStudentTextField.getText().trim()) == true) {
				confirmIdStudentLabel.setText("");
				registrationLanguageCertification();

			} else {
				confirmIdStudentLabel.setText("Please enter only numbers");
			}
		}
	}

	/**
	 * This method allows you to return to the home page
	 * 
	 * @param event return to the opening scene
	 * @throws IOException
	 */
	public void homeButtonOnAction(ActionEvent event) throws IOException {
		Main homepage = new Main();
		homepage.changeSceneWithDimension("/application/view/reserved_area_admin.fxml", 536, 560);
	}

	/**
	 * Add a new language certification in the mysql database
	 * 
	 * @thows NoSuchAlgorithmException
	 */
	public void registrationLanguageCertification() throws NoSuchAlgorithmException {

		// Connect to the database
		DBConnect connectNow = DBConnect.getInstance();
		Connection connection = connectNow.getConnection();

		String level = levelTextField.getText();
		String language = languageTextField.getText();
		String idStudent = idStudentTextField.getText();

		if (levelTextField.getText() == null || levelTextField.getText().trim().isEmpty()) {
			level = null;
		}

		if (languageTextField.getText() == null || languageTextField.getText().trim().isEmpty()) {
			language = null;
		}

		if (idStudentTextField.getText() == null || idStudentTextField.getText().trim().isEmpty()) {
			idStudent = null;
		}

		// Insert into mysql database the language certification
		String insertFields = " INSERT INTO certificate(level, language, id_student) VALUES ('";
		String insertValues = level + "','" + language + "','" + idStudent + "')";

		String insertToRegister = insertFields + insertValues;

		try {
			Statement statement = connection.createStatement();
			statement.executeUpdate(insertToRegister);

			registrationMessageLabel.setText("Language certificate has been registred successfully!");
		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
	}

	/**
	 * This method checks if the level field is empty
	 * 
	 * @return true if field is empty
	 * @return false if field is not empty
	 */
	public boolean requiredFieldFirstLevel() {
		if (levelTextField.getText().trim().isEmpty()) {
			asteriskLevelLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskLevelLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the language field is empty
	 * 
	 * @return true if field is empty
	 * @return false if field is not empty
	 */
	public boolean requiredFieldLanguage() {
		if (languageTextField.getText().trim().isEmpty()) {
			asteriskLanguageLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskLanguageLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the id_student field is empty
	 * 
	 * @return true if field is empty
	 * @return false if field is not empty
	 */
	public boolean requiredFieldIdStudent() {
		if (idStudentTextField.getText().trim().isEmpty()) {
			asteriskIdStudentLabel.setText("*");
			requiredfieldLabel.setText("(*) Required field");
			return true;
		}
		asteriskIdStudentLabel.setText("");
		requiredfieldLabel.setText("");
		return false;
	}

	/**
	 * This method checks if the string is only numbers
	 * 
	 * @param str
	 * @return true if only number
	 */
	public static boolean isNumeric(String str) {
		try {
			Long.parseLong(str);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
}
