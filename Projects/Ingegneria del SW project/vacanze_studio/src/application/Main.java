package application;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

	private static Stage stg;

	@Override
	public void start(Stage primaryStage) throws Exception {

		try {

			stg = primaryStage;
			primaryStage.setResizable(false);

			Parent root = FXMLLoader.load(getClass().getResource("/application/view/login_or_registration.fxml"));

			primaryStage.setTitle("Travel Agency");
			primaryStage.setScene(new Scene(root, 536, 560));
			primaryStage.show();

		} catch (Exception e) {
			e.printStackTrace();
			e.getCause();
		}
	}

	// Change scene given a custom width and heigth
	public void changeSceneWithDimension(String fxml, int width, int heigth) throws IOException {
		Parent pane = FXMLLoader.load(getClass().getResource(fxml));
		stg.setScene(new Scene(pane, width, heigth));
		stg.getScene().setRoot(pane);
	}

	public static void main(String[] args) {
		launch(args);
	}
}
