grammar HaveFun;

prog : (fun)* com EOF ;     // * optional

fun : FUN ID LPAR (ID (COMMA ID)*)* RPAR LBRACE (com SEMICOLON)? RET exp RBRACE         // fun id (id ,,, id)* {(com;)* return exp}
    ;

com : IF LPAR exp RPAR THEN LBRACE com RBRACE ELSE LBRACE com RBRACE    # if            // if (exp) then {com} else {com}
    | ID ASSIGN exp                                                     # assign        // id = exp
    | SKIPP                                                             # skip          // skip
    | com SEMICOLON com                                                 # seq           // com ; com
    | WHILE LPAR exp RPAR LBRACE com RBRACE                             # while         // while (exp) {com}
    | OUT LPAR exp RPAR                                                 # out           // out (exp)
    | LBRACE com RBRACE ND LBRACE com RBRACE                            # nd            // {com} nd {com}
    | GLOBAL ID ASSIGN exp                                              # global        // global id = exp
    | ID DOTG ASSIGN exp                                                # globalassign  // id.g = exp
    ;

exp : NAT                                 # nat
    | BOOL                                # bool
    | LPAR exp RPAR                       # parExp
    | <assoc=right> exp POW exp           # pow
    | NOT exp                             # not
    | exp op=(DIV | MUL | MOD) exp        # divMulMod
    | exp op=(PLUS | MINUS) exp           # plusMinus
    | exp op=(LT | LEQ | GEQ | GT) exp    # cmpExp
    | exp op=(EQQ | NEQ) exp              # eqExp
    | exp op=(AND | OR) exp               # logicExp
    | ID                                  # id
    | ID LPAR (exp (COMMA exp)*)? RPAR    # function
    | ID DOTG                             # idGlobal
    ;

NAT : '0' | [1-9][0-9]* ;
BOOL : 'true' | 'false' ;

PLUS  : '+' ;
MINUS : '-' ;
MUL   : '*' ;
DIV   : '/' ;
MOD   : 'mod' ;
POW   : '^' ;

AND : '&' ;
OR  : '|' ;

EQQ : '==' ;
NEQ : '!=' ;
LEQ : '<=' ;
GEQ : '>=' ;
LT  : '<' ;
GT  : '>' ;
NOT : '!' ;

IF     : 'if' ;
THEN   : 'then' ;
ELSE   : 'else' ;
WHILE  : 'while' ;
SKIPP  : 'skip' ;
ASSIGN : '=' ;
OUT    : 'out' ;
ND     : 'nd' ;
GLOBAL : 'global' ;

LPAR      : '(' ;
RPAR      : ')' ;
LBRACE    : '{' ;
RBRACE    : '}' ;
SEMICOLON : ';' ;
COMMA     : ',' ;
DOTG      : '.g' ;

FUN : 'fun' ;
RET : 'return' ;

ID : [a-z]+ ;

WS : [ \t\r\n]+ -> skip ;

COMMENT : '/*' .*? '*/' -> skip ;
LINE_COMMENT : '//' ~[\r\n]* -> skip ;