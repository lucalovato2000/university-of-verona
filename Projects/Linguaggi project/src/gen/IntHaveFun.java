package gen;

import main.*;
import org.antlr.v4.runtime.tree.TerminalNode;
import value.*;

import java.util.*;

public class IntHaveFun extends HaveFunBaseVisitor<Value> {

    private final LinkedList<Conf> variables, variablesglob;    // list of variables
    private final Set<FunValue> funs = new HashSet<>();         // set of functions

    public IntHaveFun() {
        variables = new LinkedList<>();
        variablesglob = new LinkedList<>();
        variables.add(new Conf());
        variablesglob.add(new Conf());
    }

    @Override
    public ComValue visitProg(HaveFunParser.ProgContext ctx) {
        for (HaveFunParser.FunContext f : ctx.fun()) {
            visit(f);   // reads the functions
        }
        return visitCom(ctx.com());
    }

    @Override
    public ComValue visitFun(HaveFunParser.FunContext ctx) {
        String id = ctx.ID(0).getText();              // function ID

        List<TerminalNode> allParam;                    // list of all parameters
        List<String> trueParam = new ArrayList<>();     // filter for catch double parameters

        allParam = ctx.ID();          // read the parameters
        allParam.remove(0);     // remove ID from allParam

        for (FunValue f : funs) {
            if (f.getNameFun().equals(id)) {
                System.err.println("Function " + ctx.ID(0).getText() + " already defined");
                System.err.println("@" + ctx.start.getLine() + ":" + ctx.start.getCharPositionInLine());
                System.exit(1);     // exit for error
            }
        }

        for (TerminalNode param : allParam) {
            if (trueParam.contains(param.getText())) {      // catch the equal param
                System.err.println("Parameter name " + param.getText() + " clashes with previous parameters");
                System.err.println("@" + ctx.start.getLine() + ":" + ctx.start.getCharPositionInLine());
                System.exit(1);     // exit for error
            }
            trueParam.add(param.getText());
        }

        FunValue newFunction = new FunValue(id, trueParam, ctx.com(), ctx.exp());
        funs.add(newFunction);      // add new function to funs

        return ComValue.INSTANCE;
    }

    private ComValue visitCom(HaveFunParser.ComContext ctx) {
        return (ComValue) visit(ctx);
    }

    @Override
    public ComValue visitIf(HaveFunParser.IfContext ctx) {
        if (visitBoolExp(ctx.exp())) {
            return visitCom(ctx.com(0));
        } else {
            return visitCom(ctx.com(1));
        }
    }

    @Override
    public ComValue visitAssign(HaveFunParser.AssignContext ctx) {
        String id = ctx.ID().getText();
        ExpValue<?> v = visitExp(ctx.exp());

        variables.getLast().update(id, v);

        return ComValue.INSTANCE;
    }

    @Override
    public ComValue visitSkip(HaveFunParser.SkipContext ctx) {
        return ComValue.INSTANCE;
    }

    @Override
    public ComValue visitSeq(HaveFunParser.SeqContext ctx) {
        visitCom(ctx.com(0));

        return visitCom(ctx.com(1));
    }

    @Override
    public ComValue visitWhile(HaveFunParser.WhileContext ctx) {
        if (!visitBoolExp(ctx.exp())) {
            return ComValue.INSTANCE;
        }
        visitCom(ctx.com());

        return visitWhile(ctx);
    }

    @Override
    public ComValue visitOut(HaveFunParser.OutContext ctx) {
        System.out.println(visitExp(ctx.exp()));

        return ComValue.INSTANCE;
    }

    @Override
    public ComValue visitNd(HaveFunParser.NdContext ctx) {
        Random rnd = new Random();
        int n = rnd.nextInt(2);

        if (n == 0) {
            return visitCom(ctx.com(0));
        } else {
            return visitCom(ctx.com(1));
        }
    }

    @Override
    public ComValue visitGlobal(HaveFunParser.GlobalContext ctx) {
        String id = ctx.ID().getText();
        ExpValue<?> v = visitExp(ctx.exp());

        variablesglob.getLast().update(id, v);

        return ComValue.INSTANCE;
    }

    @Override
    public ComValue visitGlobalassign(HaveFunParser.GlobalassignContext ctx) {
        String id = ctx.ID().getText();
        ExpValue<?> v = visitExp(ctx.exp());

        variablesglob.getLast().update(id, v);

        return ComValue.INSTANCE;
    }

    /* ------------------------------------------------------------------------------------------- EXP */

    private ExpValue<?> visitExp(HaveFunParser.ExpContext ctx) {
        return (ExpValue<?>) visit(ctx);
    }

    private int visitNatExp(HaveFunParser.ExpContext ctx) {
        try {
            return ((NatValue) visitExp(ctx)).toJavaValue();
        } catch (ClassCastException e) {
            System.err.println("Type mismatch exception!");
            System.err.println("@" + ctx.start.getLine() + ":" + ctx.start.getCharPositionInLine());
            System.err.println("-------------------");
            System.err.println(ctx.getText());
            System.err.println("-------------------");
            System.err.println("> Natural expression expected");
            System.exit(1);     // exit for error
        }

        return 0; // unreachable code
    }

    private boolean visitBoolExp(HaveFunParser.ExpContext ctx) {
        try {
            return ((BoolValue) visitExp(ctx)).toJavaValue();
        } catch (ClassCastException e) {
            System.err.println("Type mismatch exception!");
            System.err.println("@" + ctx.start.getLine() + ":" + ctx.start.getCharPositionInLine());
            System.err.println("-------------------");
            System.err.println(ctx.getText());
            System.err.println("-------------------");
            System.err.println("> Boolean expression expected");
            System.exit(1);     // exit for error
        }

        return false; // unreachable code
    }

    @Override
    public NatValue visitNat(HaveFunParser.NatContext ctx) {
        return new NatValue(Integer.parseInt(ctx.NAT().getText()));
    }

    @Override
    public BoolValue visitBool(HaveFunParser.BoolContext ctx) {
        return new BoolValue(Boolean.parseBoolean(ctx.BOOL().getText()));
    }

    @Override
    public ExpValue<?> visitParExp(HaveFunParser.ParExpContext ctx) {
        return visitExp(ctx.exp());
    }

    @Override
    public NatValue visitPow(HaveFunParser.PowContext ctx) {
        int base = visitNatExp(ctx.exp(0));
        int exp = visitNatExp(ctx.exp(1));

        return new NatValue((int) Math.pow(base, exp));
    }

    @Override
    public BoolValue visitNot(HaveFunParser.NotContext ctx) {
        return new BoolValue(!visitBoolExp(ctx.exp()));
    }

    @Override
    public NatValue visitDivMulMod(HaveFunParser.DivMulModContext ctx) {
        int left = visitNatExp(ctx.exp(0));
        int right = visitNatExp(ctx.exp(1));

        return switch (ctx.op.getType()) {
            case HaveFunParser.DIV -> new NatValue(left / right);
            case HaveFunParser.MUL -> new NatValue(left * right);
            case HaveFunParser.MOD -> new NatValue(left % right);
            default -> null;    // unreachable code
        };
    }

    @Override
    public NatValue visitPlusMinus(HaveFunParser.PlusMinusContext ctx) {
        int left = visitNatExp(ctx.exp(0));
        int right = visitNatExp(ctx.exp(1));

        return switch (ctx.op.getType()) {
            case HaveFunParser.PLUS -> new NatValue(left + right);
            case HaveFunParser.MINUS -> new NatValue(Math.max(left - right, 0));
            default -> null;    // unreachable code
        };
    }

    @Override
    public BoolValue visitCmpExp(HaveFunParser.CmpExpContext ctx) {
        int left = visitNatExp(ctx.exp(0));
        int right = visitNatExp(ctx.exp(1));

        return switch (ctx.op.getType()) {
            case HaveFunParser.GEQ -> new BoolValue(left >= right);
            case HaveFunParser.LEQ -> new BoolValue(left <= right);
            case HaveFunParser.LT -> new BoolValue(left < right);
            case HaveFunParser.GT -> new BoolValue(left > right);
            default -> null;    // unreachable code
        };
    }

    @Override
    public BoolValue visitEqExp(HaveFunParser.EqExpContext ctx) {
        ExpValue<?> left = visitExp(ctx.exp(0));
        ExpValue<?> right = visitExp(ctx.exp(1));

        return switch (ctx.op.getType()) {
            case HaveFunParser.EQQ -> new BoolValue(left.equals(right));
            case HaveFunParser.NEQ -> new BoolValue(!left.equals(right));
            default -> null;    // unreachable code
        };
    }

    @Override
    public BoolValue visitLogicExp(HaveFunParser.LogicExpContext ctx) {
        boolean left = visitBoolExp(ctx.exp(0));
        boolean right = visitBoolExp(ctx.exp(1));

        return switch (ctx.op.getType()) {
            case HaveFunParser.AND -> new BoolValue(left && right);
            case HaveFunParser.OR -> new BoolValue(left || right);
            default -> null;    // unreachable code
        };
    }

    @Override
    public ExpValue<?> visitId(HaveFunParser.IdContext ctx) {
        String id = ctx.ID().getText();

        if (!variables.getLast().contains(id)) {
            System.err.println("Variable " + id + " used but never instantiated");
            System.err.println("@" + ctx.start.getLine() + ":" + ctx.start.getCharPositionInLine());

            System.exit(1);     // exit for error
        }

        return variables.getLast().obtain(id);
    }

    @Override
    public ExpValue<?> visitFunction(HaveFunParser.FunctionContext ctx) {
        int i = 0;

        String id = ctx.ID().getText();                             // function ID
        Conf memory = new Conf();                                   // function memory
        List<HaveFunParser.ExpContext> parameters = ctx.exp();      // function parameters

        for (FunValue function : funs) {

            if (function.getNameFun().equals(id) && function.getNamePar().size() != parameters.size()) {
                System.err.println("Function " + id + " called with the wrong number of arguments");
                System.err.println("@" + ctx.start.getLine() + ":" + ctx.start.getCharPositionInLine());

                System.exit(1);     // exit for error
            }
            if (function.getNameFun().equals(id)) {
                for (String param : function.getNamePar()) {
                    memory.update(param, visitExp(parameters.get(i)));
                    i++;
                }

                variables.add(memory);
                HaveFunParser.ComContext comContext = function.getComContext();
                if (!(comContext == null)) {
                    visitCom(comContext);
                }

                ExpValue<?> value = visitExp(function.getExpContext());
                variables.removeLast();

                return value;
            }
        }

        System.err.println("Function " + id + " used but never declared");
        System.err.println("@" + ctx.start.getLine() + ":" + ctx.start.getCharPositionInLine());
        System.exit(1);     // exit for error

        return null;
    }

    @Override
    public ExpValue<?> visitIdGlobal(HaveFunParser.IdGlobalContext ctx) {
        String id = ctx.ID().getText();

        if (!variablesglob.getLast().contains(id)) {
            System.err.println("Variable " + id + " used but never instantiated");
            System.err.println("@" + ctx.start.getLine() + ":" + ctx.start.getCharPositionInLine());

            System.exit(1);     // exit for error
        }
        return variablesglob.getLast().obtain(id);
    }
}
