package value;

import gen.HaveFunParser;

import java.util.List;

public class FunValue extends Value {

    private final String nameFun;
    private final List<String> namePar;
    private final HaveFunParser.ComContext comContext;
    private final HaveFunParser.ExpContext expContext;

    // store the information
    public FunValue(String nameFun, List<String> namePar, HaveFunParser.ComContext comContext, HaveFunParser.ExpContext expContext) {
        this.nameFun = nameFun;
        this.namePar = namePar;
        this.comContext = comContext;
        this.expContext = expContext;
    }

    public String getNameFun() {
        return nameFun;
    }

    public List<String> getNamePar() {
        return namePar;
    }

    public HaveFunParser.ComContext getComContext() {
        return comContext;
    }

    public HaveFunParser.ExpContext getExpContext() {
        return expContext;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof FunValue && ((FunValue) obj).nameFun.equals(this.nameFun);
    }

    @Override
    public int hashCode() {
        return nameFun.hashCode();
    }
}