%{
    #include <stdio.h>
    #include "y.tab.h"
%}

INTEGERS 0|[1-9]+[0-9]*
EXIT exit|EXIT

%%

{EXIT}                  exit(0);
{INTEGERS}              { yylval.string = strndup(yytext, yyleng); return INTEGERS; }

[ \t\r]

\n                      return '\n';
.                       return yytext[0];

%%
