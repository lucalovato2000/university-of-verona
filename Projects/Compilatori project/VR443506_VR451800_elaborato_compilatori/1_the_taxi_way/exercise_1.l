%{
    #include <stdio.h>
    #include "y.tab.h"
%}

INTEGERS 0|[1-9]+[0-9]*
EXIT exit|EXIT
%%

{EXIT}                  exit(0);
{INTEGERS}              { yylval.integer = atoi(yytext); return NUMBER; }
UP                      { yylval.up = yytext; return UP; }
DOWN                    { yylval.down = yytext; return DOWN; } 
RIGHT                   { yylval.right = yytext; return RIGHT; }
LEFT                    { yylval.left = yytext; return LEFT; }
;                       { yylval.end = yytext; return END; }

[ \t\r]

\n                      return '\n';
.                       return yytext[0];

%%
