# Compilatori laboratorio - (1) The taxi way

## Il programma: 

Il programma è un analizzatore sintattico che permette di riconoscere i movimenti all'interno di un piano cartesiano tramite percorsi acquisiti da standard input. 

Le direzioni che si possono utilizzare (solo con valori **positivi**) sono:
- UP, DOWN, RIGHT, LEFT

Il programma deve controllare che vengano inserite in input delle frasi valide. Inoltre deve controllare che il percorso per raggiungere il punto finale sia corretto specificando all'utente, nel caso in cui non lo fosse, un percorso per raggiungere la destinazione dal punto in cui è arrivato. Il programma può accettare più input, quindi termina quando viene deciso dall'utente oppure quando l'utente arriva a destinazione.


## Compilazione: 

```
yacc -d exercise_1.y
lex exercise_1.l
gcc y.tab.c lex.yy.c -lfl
```


## File LEX:

Il file LEX contiene due macro: 

- INTEGERS, utilizzata per leggere numeri interi 

- EXIT, utilizzata per terminare il programma

Inoltre sono presenti i comandi UP, DOWN, RIGHT, LEFT che devono essere riconosciuti


## File YACC:

Il file YACC contiene:

- la definizione dei tipi per ogni comando
- la grammatica per riconoscere i comandi inseriti
- la funzione check per controllare se l'utente è arrivato a destinazione
- la funzione update per controllare che i comandi inseriti siano corretti e aggiornare la posizione sul piano cartesiano



## Esempi di codice **funzionante** con relativo output:
-------

**Input**
```
UP 5 ;
```
**Output**

```
Coordinates to reach the end position: [x: 5, y: 6]
Oh no! You didn't get to the end point! To get there, you missed: [ RIGHT 5 UP 1 ]
```

-------

**Input**
```
UP 5 RIGHT 10 ;
```
**Output**

```
Coordinates to reach the end position: [x: 5, y: 6]
Oh no! You didn't get to the end point! To get there, you missed: [ LEFT 5 UP 1 ]
```

-------

**Input**
```
RIGHT 5 UP 6 ;
```
**Output**

```
Coordinates to reach the end position: [x: 5, y: 6]
Congratulations! You have arrived at the final position
```

-------

## Esempi di codice **NON funzionante** con relativo output:

**Input**
```
UPPP 6 ;
```
**Output**

```
syntax error, unexpected invalid token, expecting NUMBER
```

-------

**Input**
```
LEFT -5 ;
```
**Output**

```
syntax error, unexpected invalid token, expecting NUMBER
```

-------

**Input**
```
RIGHT 3 LEFT 8 
```
**Output**

```
syntax error, unexpected '\n'
```
