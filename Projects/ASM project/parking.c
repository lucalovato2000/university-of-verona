/*
  LUCA LOVATO - SAMUELE MARZOLA

  project: automated-parking
  title: parking.c

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <stdint.h>
#include <sys/time.h>


/* Inserite eventuali extern modules qui */

extern void controllore(char in[], char out[]);   //  ASM modulo esterno

/* ************************************* */

enum { MAXLINES = 200 };
enum { LIN_LEN = 5 };
enum { LOUT_LEN = 15 };

long long current_timestamp() {

  struct timespec tp;
	clock_gettime(CLOCK_REALTIME, &tp);
	/* te.tv_nsec nanoseconds divide by 1000 to get microseconds*/
	long long nanoseconds = tp.tv_sec*1000LL + tp.tv_nsec; // caculate nanoseconds
  
  return nanoseconds;
}


int main(int argc, char *argv[]) {
    
  int i = 0;
  char bufferin[MAXLINES*LIN_LEN+1] ;
  char line[1024];
  long long tic_c, toc_c, tic_asm, toc_asm;

  char bufferout_c[MAXLINES*LOUT_LEN+1] = "" ;
  char bufferout_asm[MAXLINES*LOUT_LEN+1] = "" ;

  FILE *inputFile = fopen(argv[1], "r");

  if(argc != 3)
  {
      fprintf(stderr, "Syntax ./test <input_file> <output_file>\n");
      exit(1);
  }

  if (inputFile == 0)
  {
      fprintf(stderr, "failed to open the input file. Syntax ./test <input_file> <output_file>\n");
      exit(1);
  }

  while (i < MAXLINES && fgets(line, sizeof(line), inputFile))
  {
      i = i + 1;
      strcat( bufferin, line) ;
  }

  bufferin[MAXLINES*LIN_LEN] = '\0' ;

  fclose(inputFile);


  /* ELABORAZIONE in C */
  tic_c = current_timestamp();

  toc_c = current_timestamp();
  
  controllore(bufferin, bufferout_asm);

  long long c_time_in_nanos = toc_c - tic_c;

  /* FINE ELABORAZIONE C */

  /* INIZIO ELABORAZIONE ASM */

  tic_asm = current_timestamp();

  toc_asm = current_timestamp();

  long long asm_time_in_nanos = toc_asm - tic_asm;

  /* FINE ELABORAZIONE ASM */

  printf("C time elapsed: %lld ns\n", c_time_in_nanos);
  printf("ASM time elapsed: %lld ns\n", asm_time_in_nanos);

  /* Salvataggio dei risultati ASM */
  FILE *outputFile;
  outputFile = fopen (argv[2], "w");
  fprintf (outputFile, "%s", bufferout_asm);
  fclose (outputFile);

  return 0;
}
