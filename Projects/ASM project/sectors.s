/*>
  LUCA LOVATO - SAMUELE MARZOLA 

  project: automated-parking
  title: sectors.s
<*/

.section .text
	.global controllore


controllore:

	pushl %ebp   	# salva ebp corrente
	movl %esp, %ebp   # ebp prende il valore di esp

  # salva stato dei registri
  pushl %eax
  pushl %ebx
  pushl %edx
  pushl %edi
  pushl %esi

  movl 8(%ebp), %esi    # esi contiene indirizzo di bufferin
  movl 12(%ebp), %edi   # edi contiene indirizzo di bufferout_asm

  # azzera contenuto
  xorl %eax, %eax   
  xorl %ebx, %ebx  
  xorl %ecx, %ecx  
  xorl %edx, %edx   

  movl $0, %ecx  # ecx è il contatore inizializzato a 0

  cmpb $0, (%esi) # verifia se il bufferin è vuoto
  
  je end  # se vuoto termina esecuzione


start: 

  # ######### INSERIMENTO NUMERO AUTOMOBILI NEI REGISTRI #########

  cmpl $0, %ecx  # se contatore 0 inserisce il valore letto nel registro del settore a
  je fr_insert

  cmpl $1, %ecx  # se contatore 1 inserisce il valore letto nel registro del settore b
  je sc_insert

  cmpl $2, %ecx  # se contatore 2 inserisce il valore letto nel registro del settore c
  je td_insert

  # ######### GESTIONE INGRESSO / USCITA AUTOMOBILI #########

  cmpb $73, (%esi)  # alza la sbarra in ingresso (OC) se inizia con I
  je pr_enter

  cmpb $79, (%esi)  # alza la sbarra in uscita (CO) CO se inizia con O
  je pr_exit

  # ######### GESTIONE EVENTUALI ERRORI IN INPUT #########

  cmpb $105, (%esi)  # lascia chiuse le sbarre se legge un dato errato (CC) [se inizia con i]
  je inotuppercase

  cmpb $111, (%esi)  # lascia chiuse le sbarre se legge un dato errato (CC) [se inizia con o]
  je onotuppercase


fr_insert:
  
  inc %ecx   # incrementa contatore 

  cmpb $51, 2(%esi)  # maggiore di 3
  ja minore10a

  cmpb $48, 2(%esi)  #  uguale 0
  je ugualeazeroa

  cmpb $49, 2(%esi)  #  uguale 1
  je ugualediecia

  cmpb $50, 2(%esi)  # uguale 2
  je ugualeventia

  cmpb $51, 2(%esi)  # uguale 3
  je ugualetrentaa

  jmp end


minore10a:

  # cmp $47, 3(%esi)
  # ja superatomassimoA

  mov $48, %al  # scrittura forzata dello zero (input a una cifra)
  mov 2(%esi), %ah

  addl $4, %esi  # salta alla riga successiva
  jmp end


ugualeazeroa:
  
  mov $48, %al  # scrittura forzata dello zero (input a una cifra)
  mov 2(%esi), %ah

  addl $4, %esi  # salta alla riga successiva
  jmp end


ugualediecia:
  
  cmpb $47, 3(%esi)
  ja secondacifradiecia
  
  mov $48, %al  # scrittura forzata dello zero (input a una cifra)
  mov 2(%esi), %ah

  addl $4, %esi  # salta alla riga successiva
  jmp end


secondacifradiecia:

  mov 2(%esi), %al  # scrittura forzata dello zero (input a una cifra)
  mov 3(%esi), %ah

  addl $5, %esi  # salta alla riga successiva
  jmp end


ugualeventia:
  
  cmpb $47, 3(%esi)
  ja secondacifraventia
  
  mov $48, %al  # scrittura forzata dello zero (input a una cifra)
  mov 2(%esi), %ah

  addl $4, %esi  # salta alla riga successiva
  jmp end


secondacifraventia:

  mov 2(%esi), %al  # scrittura forzata dello zero (input a una cifra)
  mov 3(%esi), %ah

  addl $5, %esi  # salta alla riga successiva
  jmp end


ugualetrentaa:
  
  # cmpb $49, 3(%esi)
  # ja superatomassimoA

  cmpb $48, 3(%esi)
  je trentaa

  cmpb $49, 3(%esi)
  je trentunoa
  
  mov $48, %al  # scrittura forzata dello zero (input a una cifra)
  mov 2(%esi), %ah

  addl $4, %esi  # salta alla riga successiva
  jmp end


superatomassimoA:

  mov $51, %al  # scrittura forzata dello zero (input a una cifra)
  mov $49, %ah

  addl $5, %esi  # salta alla riga successiva
  jmp end


trentaa:

  mov $51, %al  # scrittura forzata dello zero (input a una cifra)
  mov $48, %ah

  addl $5, %esi  # salta alla riga successiva
  jmp end


trentunoa:

  mov $51, %al  # scrittura forzata dello zero (input a una cifra)
  mov $49, %ah

  addl $5, %esi  # salta alla riga successiva
  jmp end


sc_insert:

  inc %ecx   # incrementa contatore 

  cmpb $51, 2(%esi)  # maggiore di 3
  ja minore10b

  cmpb $48, 2(%esi)  #  uguale 0
  je ugualeazerob

  cmpb $49, 2(%esi)  #  uguale 1
  je ugualediecib

  cmpb $50, 2(%esi)  # uguale 2
  je ugualeventib

  cmpb $51, 2(%esi)  # uguale 3
  je ugualetrentab

  jmp end


minore10b:

  # cmp $47, 3(%esi)
  # ja superatomassimoB

  mov $48, %bl  # scrittura forzata dello zero (input a una cifra)
  mov 2(%esi), %bh

  addl $4, %esi  # salta alla riga successiva
  jmp end


ugualeazerob:
  
  mov $48, %bl  # scrittura forzata dello zero (input a una cifra)
  mov $48, %bh

  addl $4, %esi  # salta alla riga successiva
  jmp end


ugualediecib:
  
  cmpb $47, 3(%esi)
  ja secondacifradiecib
  
  mov $48, %bl  # scrittura forzata dello zero (input a una cifra)
  mov 2(%esi), %bh

  addl $4, %esi  # salta alla riga successiva
  jmp end


secondacifradiecib:

  mov 2(%esi), %bl  # scrittura forzata dello zero (input a una cifra)
  mov 3(%esi), %bh

  addl $5, %esi  # salta alla riga successiva
  jmp end


ugualeventib:
  
  cmpb $47, 3(%esi)
  ja secondacifraventib
  
  mov $48, %bl  # scrittura forzata dello zero (input a una cifra)
  mov 2(%esi), %bh

  addl $4, %esi  # salta alla riga successiva
  jmp end


secondacifraventib:

  mov 2(%esi), %bl  # scrittura forzata dello zero (input a una cifra)
  mov 3(%esi), %bh

  addl $5, %esi  # salta alla riga successiva
  jmp end


ugualetrentab:
  
  cmpb $49, 3(%esi)
  ja superatomassimoB

  cmpb $48, 3(%esi)
  je trentab

  cmpb $49, 3(%esi)
  je trentunob
  
  mov $48, %bl  # scrittura forzata dello zero (input a una cifra)
  mov 2(%esi), %bh

  addl $4, %esi  # salta alla riga successiva
  jmp end


superatomassimoB:

  mov $51, %bl  # scrittura forzata dello zero (input a una cifra)
  mov $49, %bh

  addl $5, %esi  # salta alla riga successiva
  jmp end

trentab:

  mov $51, %bl  # scrittura forzata dello zero (input a una cifra)
  mov $48, %bh

  addl $5, %esi  # salta alla riga successiva
  jmp end

trentunob:

  mov $51, %bl  # scrittura forzata dello zero (input a una cifra)
  mov $49, %bh

  addl $5, %esi  # salta alla riga successiva
  jmp end


td_insert:

  inc %ecx   # incrementa contatore 
  
  cmpb $50, 2(%esi)  # maggiore di 2
  ja minore10c
  
  cmpb $48, 2(%esi)  #  uguale 0
  je ugualeazeroc

  cmpb $49, 2(%esi)  #  uguale 1
  je ugualediecic

  cmpb $50, 2(%esi)  # uguale 2
  je ugualeventic

  jmp end


minore10c:

  # cmp $32, 3(%esi)
  # je superatomassimoC

  mov $48, %dl  # scrittura forzata dello zero (input a una cifra)
  mov 2(%esi), %dh

  addl $4, %esi  # salta alla riga successiva
  jmp end


ugualeazeroc:
  
  mov $48, %dl  # scrittura forzata dello zero (input a una cifra)
  mov $48, %dh

  addl $4, %esi  # salta alla riga successiva
  jmp end

ugualediecic:
  
  cmpb $47, 3(%esi)
  ja secondacifradiecic
  
  mov $48, %dl  # scrittura forzata dello zero (input a una cifra)
  mov 2(%esi), %dh

  addl $4, %esi  # salta alla riga successiva
  jmp end


secondacifradiecic:

  mov 2(%esi), %dl  # scrittura forzata dello zero (input a una cifra)
  mov 3(%esi), %dh

  addl $5, %esi  # salta alla riga successiva
  jmp end


ugualeventic:
  
  cmpb $52, 3(%esi)  # maggiore 4
  ja superatomassimoC

  cmpb $48, 3(%esi)
  je venti

  cmpb $49, 3(%esi)
  je ventuno

  cmpb $50, 3(%esi)
  je ventidue

  cmpb $51, 3(%esi)
  je ventitre

  cmpb $52, 3(%esi)
  je ventiquattro
  
  mov $48, %dl  # scrittura forzata dello zero (input a una cifra)
  mov 2(%esi), %dh

  addl $4, %esi  # salta alla riga successiva
  jmp end


superatomassimoC:

  mov $50, %dl  # scrittura forzata dello zero (input a una cifra)
  mov $52, %dh

  addl $5, %esi  # salta alla riga successiva
  jmp end

venti:

  mov $50, %dl  # scrittura forzata dello zero (input a una cifra)
  mov $48, %dh

  addl $5, %esi  # salta alla riga successiva
  jmp end

ventuno:

  mov $50, %dl  # scrittura forzata dello zero (input a una cifra)
  mov $49, %dh

  addl $5, %esi  # salta alla riga successiva
  jmp end

ventidue:

  mov $50, %dl  # scrittura forzata dello zero (input a una cifra)
  mov $50, %dh

  addl $5, %esi  # salta alla riga successiva
  jmp end

ventitre:

  mov $50, %dl  # scrittura forzata dello zero (input a una cifra)
  mov $51, %dh

  addl $5, %esi  # salta alla riga successiva
  jmp end

ventiquattro:

  mov $50, %dl  # scrittura forzata dello zero (input a una cifra)
  mov $52, %dh

  addl $5, %esi  # salta alla riga successiva
  jmp end


# ####################### #
# #       ENTER         # #
# ####################### #

pr_enter:
  
  cmpb $45, 2(%esi)  # se seconda cifra non e' -
  jne enter_error2   # salta a enter_error
  
  # Controlla in quale settore la macchina vuole parcheggiare
  cmpb $65, 3(%esi)
  je inc_a

  cmpb $66, 3(%esi)
  je inc_b

  cmpb $67, 3(%esi)
  je inc_c

  cmpb $96, 3(%esi)
  ja enter_error

  
inc_a:

  cmpb $57, %ah  # controlla se la cifra è uguale a 9
  je inc_pra

  cmpb $57, %ah  # controlla se la cifra è minore di 9
  jl inc_sca


# La seguente funzione permette di stampare la riga in output 
inc_pra:

  mov $48, %ah

  inc %al

  movb $79,  (%edi)   # O = OPEN
  movb $67,  1(%edi)  # C = CLOSED

  movb $45, 2(%edi)   # stampa del simbolo -

  mov %ax, 3(%edi)   # stampa numero auto settore a

  movb $45, 5(%edi)   # stampa del simbolo -

  mov %bx, 6(%edi)   # stampa numero auto settore b

  movb $45, 8(%edi)   # stampa del simbolo -

  mov %dx, 9(%edi)   # stampa numero auto settore c

  movb $45, 11(%edi)  # stampa del simbolo -

  # Semaforo
  movb $48, 12(%edi)  # 0
  movb $48, 13(%edi)  # 0
  movb $48, 14(%edi)  # 0

  movb $10, 15(%edi)  # riga nuova

  cmp $49, %ah
  je checkparka

  cmp $49, %ah
  jne not_full_parka

  addl $5, %esi
  addl $16, %edi

  jmp end


# La seguente funzione permette di stampare la riga in output 
inc_sca:

  inc %ah

  cmp $50, %ah
  je not_32a
  
  movb $79,  (%edi)   # O = OPEN
  movb $67,  1(%edi)  # C = CLOSED

  movb $45, 2(%edi)   # stampa del simbolo -

  mov %ax, 3(%edi)   # stampa numero auto settore a

  movb $45, 5(%edi)   # stampa del simbolo -

  mov %bx, 6(%edi)   # stampa numero auto settore b

  movb $45, 8(%edi)   # stampa del simbolo -

  mov %dx, 9(%edi)   # stampa numero auto settore c

  movb $45, 11(%edi)  # stampa del simbolo -
  
  # Semaforo
  movb $48, 12(%edi)  # 0
  movb $48, 13(%edi)  # 0
  movb $48, 14(%edi)  # 0

  movb $10, 15(%edi)  # riga nuova
  
  cmp $49, %ah
  je checkparka

   cmp $49, %ah
  jne not_full_parka
  
  addl $5, %esi
  addl $16, %edi

  jmp end


inc_b:

  cmpb $57, %bh   # controlla se la cifra è uguale a 9
  je inc_prb

  cmpb $57, %bh   # controlla se la cifra è minore a 9
  jl inc_scb


# La seguente funzione permette di stampare la riga in output 
inc_prb:

  mov $48, %bh

  inc %bl

  movb $79,  (%edi)   # O = OPEN
  movb $67,  1(%edi)  # C = CLOSED

  movb $45, 2(%edi)   # stampa del simbolo -

  mov %ax, 3(%edi)   # stampa numero auto settore a

  movb $45, 5(%edi)   # stampa del simbolo -

  mov %bx, 6(%edi)   # stampa numero auto settore b

  movb $45, 8(%edi)   # stampa del simbolo -

  mov %dx, 9(%edi)    # stampa numero auto settore c

  movb $45, 11(%edi)  # stampa del simbolo -

  # Semaforo
  movb $48, 12(%edi)  # 0
  movb $48, 13(%edi)  # 0
  movb $48, 14(%edi)  # 0

  movb $10, 15(%edi)  # riga nuova

  cmp $49, %ah
  je checkparka

  cmp $49, %ah
  jne not_full_parka
  
  addl $5, %esi
  addl $16, %edi

  jmp end


# La seguente funzione permette di stampare la riga in output 
inc_scb:
  
  inc %bh

  cmp $50, %bh
  je not_32b

  movb $79,  (%edi)   # O = OPEN
  movb $67,  1(%edi)  # C = CLOSED

  movb $45, 2(%edi)   # stampa del simbolo -

  mov %ax, 3(%edi)   # stampa numero auto settore a

  movb $45, 5(%edi)   # stampa del simbolo -

  mov %bx, 6(%edi)   # stampa numero auto settore b

  movb $45, 8(%edi)   # stampa del simbolo -

  mov %dx, 9(%edi)    # stampa numero auto settore c

  movb $45, 11(%edi)  # stampa del simbolo -

  # Semforo
  movb $48, 12(%edi)  # 0
  movb $48, 13(%edi)  # 0
  movb $48, 14(%edi)  # 0

  movb $10, 15(%edi)  # riga nuova
  
  cmp $49, %ah
  je checkparka

  cmp $49, %ah
  jne not_full_parka

  addl $5, %esi
  addl $16, %edi

  jmp end


inc_c:

  cmpb $57, %dh    # controlla se la cifra è uguale a 9
  je inc_prc

  cmpb $57, %dh    # controlla se la cifra è minore a 9
  jl inc_scc


# La seguente funzione permette di stampare la riga in output 
inc_prc:

  mov $48, %dh

  inc %dl

  movb $79, (%edi)   # O = OPEN
  movb $67, 1(%edi)  # C = CLOSED

  movb $45, 2(%edi)  # stampa del simbolo -

  mov %ax, 3(%edi)  # stampa numero auto settore a

  movb $45, 5(%edi)  # stampa del simbolo -

  mov %bx, 6(%edi)  # stampa numero auto settore a

  movb $45, 8(%edi)  # stampa del simbolo -

  mov %dx, 9(%edi)  # stampa numero auto settore a

  movb $45, 11(%edi) # stampa del simbolo -

  # Semaforo
  movb $48, 12(%edi)  # 0
  movb $48, 13(%edi)  # 0
  movb $48, 14(%edi)  # 0

  movb $10, 15(%edi)  # riga nuova

  cmp $49, %ah
  je checkparka

   cmp $49, %ah
  jne not_full_parka
  
  addl $5, %esi
  addl $16, %edi

  jmp end


# La seguente funzione permette di stampare la riga in output 
inc_scc:

  inc %dh

  movb $79,  (%edi)  # O = OPEN
  movb $67,  1(%edi) # C = CLOSED

  movb $45, 2(%edi)  # stampa del simbolo -

  mov %ax, 3(%edi)  # stampa numero auto settore a

  movb $45, 5(%edi)  # stampa del simbolo -

  mov %bx, 6(%edi)  # stampa numero auto settore b

  movb $45, 8(%edi)  # stampa del simbolo -

  mov %dx, 9(%edi)  # stampa numero auto settore c

  movb $45, 11(%edi) # stampa del simbolo -

  # Semaforo
  movb $48, 12(%edi)  # 0
  movb $48, 13(%edi)  # 0
  movb $48, 14(%edi)  # 0

  movb $10, 15(%edi)  # riga nuova

  cmp $49, %ah
  je checkparka

   cmp $49, %ah
  jne not_full_parka

  addl $5, %esi
  addl $16, %edi

  jmp end


# La seguente funzione permette di stampare la riga in output in caso di input errato
enter_error:

  movb $67, (%edi)    # C = CLOSE
  movb $67, 1(%edi)   # C = CLOSE

  movb $45, 2(%edi)   # stampa del simbolo -

  mov %ax, 3(%edi)   # stampa numero auto settore a

  movb $45, 5(%edi)   # stampa del simbolo -

  mov %bx, 6(%edi)   # stampa numero auto settore b

  movb $45, 8(%edi)   # stampa del simbolo -

  mov %dx, 9(%edi)   # stampa numero auto settore c

  movb $45, 11(%edi)  # stampa del simbolo -

  # Semaforo
  movb $48, 12(%edi)  # 0
  movb $48, 13(%edi)  # 0
  movb $48, 14(%edi)  # 0

  movb $10, 15(%edi)  # riga nuova

  cmp $49, %ah
  je checkparka

   cmp $49, %ah
  jne not_full_parka

  addl $5, %esi   # messo a 6 perche logica come exit
  addl $16, %edi

  jmp end


enter_error2:

  movb $67, (%edi)    # C = CLOSE
  movb $67, 1(%edi)   # C = CLOSE

  movb $45, 2(%edi)   # stampa del simbolo -

  mov %ax, 3(%edi)   # stampa numero auto settore a

  movb $45, 5(%edi)   # stampa del simbolo -

  mov %bx, 6(%edi)   # stampa numero auto settore b

  movb $45, 8(%edi)   # stampa del simbolo -

 mov %dx, 9(%edi)    # stampa numero auto settore c

  movb $45, 11(%edi)  # stampa del simbolo -

  # Semaforo
  movb $48, 12(%edi)  # 0
  movb $48, 13(%edi)  # 0
  movb $48, 14(%edi)  # 0

  movb $10, 15(%edi)  # riga nuova

  cmp $49, %ah
  je checkparka_exit

  cmp $49, %ah
  jne not_full_parka_exit

  addl $6, %esi   # messo a 6 perche logica come exit
  addl $16, %edi

  jmp end


not_32a:

  dec %ah

  movb $67,  (%edi)   # O = OPEN
  movb $67,  1(%edi)  # C = CLOSED

  movb $45, 2(%edi)   # stampa del simbolo -

  mov %ax, 3(%edi)   # stampa numero auto settore a

  movb $45, 5(%edi)   # stampa del simbolo -

  mov %bx, 6(%edi)   # stampa numero auto settore b

  movb $45, 8(%edi)   # stampa del simbolo -

 mov %dx, 9(%edi)    # stampa numero auto settore c

  movb $45, 11(%edi)  # stampa del simbolo -

  # Semaforo
  movb $48, 12(%edi)  # 0
  movb $48, 13(%edi)  # 0
  movb $48, 14(%edi)  # 0

  movb $10, 15(%edi)  # riga nuova
  
  cmp $49, %ah
  je checkparka

   cmp $49, %ah
  jne not_full_parka

  addl $5, %esi
  addl $16, %edi

  jmp end


not_32b:

  dec %bh

  movb $67,  (%edi)   # O = OPEN
  movb $67,  1(%edi)  # C = CLOSED

  movb $45, 2(%edi)   # stampa del simbolo -

  mov %ax, 3(%edi)   # stampa numero auto settore a

  movb $45, 5(%edi)   # stampa del simbolo -

  mov %bx, 6(%edi)   # stampa numero auto settore b

  movb $45, 8(%edi)   # stampa del simbolo -

  mov %dx, 9(%edi)    # stampa numero auto settore c

  movb $45, 11(%edi)  # stampa del simbolo -

  # Semaforo
  movb $48, 12(%edi)  # 0
  movb $48, 13(%edi)  # 0
  movb $48, 14(%edi)  # 0

  movb $10, 15(%edi)  # riga nuova
  
  cmp $49, %ah
  je checkparka

  cmp $49, %ah
  jne not_full_parka

  addl $5, %esi
  addl $16, %edi

  jmp end


not_25c:

  dec %dl

  movb $67,  (%edi)   # O = OPEN
  movb $67,  1(%edi)  # C = CLOSED

  movb $45, 2(%edi)   # stampa del simbolo -

  mov %ax, 3(%edi)   # stampa numero auto settore a

  movb $45, 5(%edi)   # stampa del simbolo -

  mov %bx, 6(%edi)   # stampa numero auto settore b

  movb $45, 8(%edi)   # stampa del simbolo -

  mov %dx, 9(%edi)   # stampa numero auto settore c

  movb $45, 11(%edi)  # stampa del simbolo -

  # Semaforo
  movb $48, 12(%edi)  # 0
  movb $48, 13(%edi)  # 0
  movb $48, 14(%edi)  # 0

  movb $10, 15(%edi)  # riga nuova
  
  cmp $49, %ah
  je checkparka

  cmp $49, %ah
  jne not_full_parka

  addl $5, %esi
  addl $16, %edi

  jmp end


# ####################### #
# #        EXIT         # #
# ####################### #


pr_exit:

  cmpb $45, 3(%esi)  # se il quarto carattere non e' -
  jne exit_error     # salta a exit_error
  
  cmpb $65, 4(%esi)
  je dec_a

  cmpb $66, 4(%esi)
  je dec_b

  cmpb $67, 4(%esi)
  je dec_c

  cmpb $96, 4(%esi)
  ja exit_error


dec_a:

  cmpb $48, %ah # controlla se la cifra è uguale a 0
  je dec_pra

  cmpb $48, %ah # controlla se la cifrà è maggiore a 0
  ja dec_sca


dec_pra:

  cmp $48, %al
  je dec_sca_zero

  cmp $48, %al
  jne dec_sca_not_zero

  jmp end


dec_sca_zero:
 

  movb $67,  (%edi)   # C = CLOSED
  movb $79,  1(%edi)  # O = OPEN

  movb $45, 2(%edi)   # stampa del simbolo -

  mov %ax, 3(%edi)   # stampa numero auto settore b

  movb $45, 5(%edi)   # stampa del simbolo -

  mov %bx, 6(%edi)   # stampa numero auto settore b

  movb $45, 8(%edi)   # stampa del simbolo -

  mov %dx, 9(%edi)   # stampa numero auto settore c

  movb $45, 11(%edi)  # stampa del simbolo -

  # Semaforo
  movb $48, 12(%edi)  # 0
  movb $48, 13(%edi)  # 0
  movb $48, 14(%edi)  # 0

  movb $10, 15(%edi)  # riga nuova
  
  cmp $49, %ah
  je checkparka_exit

  cmp $49, %ah
  jne not_full_parka_exit

  addl $6, %esi
  addl $16, %edi
  
  jmp end


dec_sca_not_zero:
  
  dec %al
  mov $57, %ah
  
  movb $67,  (%edi)   # C = CLOSED
  movb $79,  1(%edi)  # O = OPEN

  movb $45, 2(%edi)   # stampa del simbolo -

  mov %ax, 3(%edi)   # stampa numero auto settore a

  movb $45, 5(%edi)   # stampa del simbolo - 

  mov %bx, 6(%edi)   # stampa numero auto settore b

  movb $45, 8(%edi)   # stampa del simbolo -

  mov %dx, 9(%edi)   # stampa numero auto settore c

  movb $45, 11(%edi)  # stampa del simbolo -

  # Semaforo
  movb $48, 12(%edi)  # 0
  movb $48, 13(%edi)  # 0
  movb $48, 14(%edi)  # 0

  movb $10, 15(%edi)  # riga nuova

  cmp $49, %ah
  je checkparka_exit

  cmp $49, %ah
  jne not_full_parka_exit
  
  addl $6, %esi
  addl $16, %edi
  
  jmp end

  
dec_sca:

  dec %ah
  
  movb $67,  (%edi)   # C = CLOSED
  movb $79,  1(%edi)  # O = OPEN

  movb $45, 2(%edi)   # stampa del simbolo -

  mov %ax, 3(%edi)   # stampa numero auto settore b

  movb $45, 5(%edi)   # stampa del simbolo -

  mov %bx, 6(%edi)   # stampa numero auto settore b

  movb $45, 8(%edi)   # stampa del simbolo -

  mov %dx, 9(%edi)   # stampa numero auto settore c

  movb $45, 11(%edi)  # stampa del simbolo -

  # Semaforo
  movb $48, 12(%edi)  # 0
  movb $48, 13(%edi)  # 0
  movb $48, 14(%edi)  # 0

  movb $10, 15(%edi)  # riga nuova
  
  cmp $49, %ah
  je checkparka_exit

  cmp $49, %ah
  jne not_full_parka_exit

  addl $6, %esi
  addl $16, %edi
  
  jmp end


dec_b:

  # Compare bh equal 0
  cmpb $48, %bh
  je dec_prb

  # Compare ch greater than 0
  cmpb $48, %bh
  ja dec_scb


dec_prb:

  cmp $48, %bl
  je dec_scb_zero

  cmp $48, %bl
  jne dec_scb_not_zero

  jmp end

dec_scb_zero:
  
  movb $67,  (%edi)   # C = CLOSED
  movb $79,  1(%edi)  # O = OPEN

  movb $45, 2(%edi)   # stampa del simbolo -

  mov %ax, 3(%edi)   # stampa numero auto settore a

  movb $45, 5(%edi)   # stampa del simbolo -

  mov %bx, 6(%edi)   # stampa numero auto settore b

  movb $45, 8(%edi)   # stampa del simbolo -

  mov %dx, 9(%edi)    # stampa numero auto settore c

  movb $45, 11(%edi)  # stampa del simbolo -

  # Semaforo
  movb $48, 12(%edi)  # 0
  movb $48, 13(%edi)  # 0
  movb $48, 14(%edi)  # 0

  movb $10, 15(%edi)  # riga nuova
  
  cmp $49, %ah
  je checkparka_exit

  cmp $49, %ah
  jne not_full_parka_exit

  addl $6, %esi
  addl $16, %edi

  jmp end


dec_scb_not_zero:

  dec %bl
  mov $57, %bh
  
  movb $67,  (%edi)   # C = CLOSED
  movb $79,  1(%edi)  # O = OPEN

  movb $45, 2(%edi)   # stampa del simbolo -

  mov %ax, 3(%edi)   # stampa numero auto settore a

  movb $45, 5(%edi)   # stampa del simbolo -

  mov %bx, 6(%edi)   # stampa numero auto settore b

  movb $45, 8(%edi)   # stampa del simbolo -

 mov %dx, 9(%edi)    # stampa numero auto settore c

  movb $45, 11(%edi)  # stampa del simbolo -

  # Semaforo
  movb $48, 12(%edi)  # 0
  movb $48, 13(%edi)  # 0
  movb $48, 14(%edi)  # 0

  movb $10, 15(%edi)  # riga nuova
  
  cmp $49, %ah
  je checkparka_exit

  cmp $49, %ah
  jne not_full_parka_exit

  addl $6, %esi
  addl $16, %edi

  jmp end
 

dec_scb:

  dec %bh
  
  movb $67,  (%edi)   # C = CLOSED
  movb $79,  1(%edi)  # O = OPEN

  movb $45, 2(%edi)   # stampa del simbolo -

  mov %ax, 3(%edi)   # stampa numero auto settore a

  movb $45, 5(%edi)   # stampa del simbolo -

  mov %bx, 6(%edi)   # stampa numero auto settore b

  movb $45, 8(%edi)   # stampa del simbolo -

  mov %dx, 9(%edi)   # stampa numero auto settore c

  movb $45, 11(%edi)  # stampa del simbolo -

  # Semaforo
  movb $48, 12(%edi)  # 0
  movb $48, 13(%edi)  # 0
  movb $48, 14(%edi)  # 0

  movb $10, 15(%edi)  # riga nuova
  
  cmp $49, %ah
  je checkparka_exit

  cmp $49, %ah
  jne not_full_parka_exit

  addl $6, %esi
  addl $16, %edi
  
  jmp end


dec_c:

  # Compara dh, controlla se è uguale 0
  cmpb $48, %dh
  je dec_prc

  # Compara dh, controlla se è maggiore di 0
  cmpb $48, %dh
  ja dec_scc


dec_prc:

  cmp $48, %dl
  je dec_scc_zero

  cmp $48, %dl
  jne dec_scc_not_zero

  jmp end


dec_scc_zero:
  
  movb $67, (%edi)   # C = CLOSED
  movb $79, 1(%edi)  # O = OPEN

  movb $45, 2(%edi)  # stampa del simbolo -

  mov %ax, 3(%edi)  # stampa numero auto settore a

  movb $45, 5(%edi)  # stampa del simbolo -

  mov %bx, 6(%edi)  # stampa numero auto settore b

  movb $45, 8(%edi)  # stampa del simbolo -

  mov %dx, 9(%edi)  # stampa numero auto settore c

  movb $45, 11(%edi) # stampa del simbolo -

  # Semaforo
  movb $48, 12(%edi)  # 0
  movb $48, 13(%edi)  # 0
  movb $48, 14(%edi)  # 0

  movb $10, 15(%edi)  # riga nuova

  cmp $49, %ah
  je checkparka_exit

  cmp $49, %ah
  jne not_full_parka_exit
  
  addl $6, %esi
  addl $16, %edi
  
  jmp end


dec_scc_not_zero:

  dec %dl
  mov $57, %dh
  
  movb $67, (%edi)   # C = CLOSED
  movb $79, 1(%edi)  # O = OPEN

  movb $45, 2(%edi)  # stampa del simbolo -

  mov %ax, 3(%edi)  # stampa numero auto settore a

  movb $45, 5(%edi)  # stampa del simbolo -

  mov %bx, 6(%edi)  # stampa numero auto settore b

  movb $45, 8(%edi)  # stampa del simbolo -

  mov %dx, 9(%edi)  # stampa numero auto settore c

  movb $45, 11(%edi) # stampa del simbolo -

  # Semaforo
  movb $48, 12(%edi)  # 0
  movb $48, 13(%edi)  # 0
  movb $48, 14(%edi)  # 0

  movb $10, 15(%edi)  # riga nuova

  cmp $49, %ah
  je checkparka_exit

  cmp $49, %ah
  jne not_full_parka_exit
  
  addl $6, %esi
  addl $16, %edi
  
  jmp end

  
dec_scc:

  dec %dh
  
  movb $67,  (%edi)   # C = CLOSED
  movb $79,  1(%edi)  # O = OPEN

  movb $45, 2(%edi)   # stampa del simbolo -

  mov %ax, 3(%edi)   # stampa numero auto settore a

  movb $45, 5(%edi)   # stampa del simbolo -

  mov %bx, 6(%edi)   # stampa numero auto settore b 

  movb $45, 8(%edi)   # stampa del simbolo -

 mov %dx, 9(%edi)    # stampa numero auto settore b

  movb $45, 11(%edi)  # stampa del simbolo -

  # Semaforo
  movb $48, 12(%edi)  # 0
  movb $48, 13(%edi)  # 0
  movb $48, 14(%edi)  # 0

  movb $10, 15(%edi)  # riga nuova

  cmp $49, %ah
  je checkparka_exit

  cmp $49, %ah
  jne not_full_parka_exit
  
  addl $6, %esi
  addl $16, %edi

  jmp end


exit_error:

  movb $67, (%edi)    # C = CLOSE
  movb $67, 1(%edi)   # C = CLOSE

  movb $45, 2(%edi)   # stampa del simbolo -

  mov %ax, 3(%edi)   # stampa numero auto settore a

  movb $45, 5(%edi)   # stampa del simbolo -

  mov %bx, 6(%edi)   # stampa numero auto settore b

  movb $45, 8(%edi)   # stampa del simbolo -

  mov %dx, 9(%edi)   # stampa numero auto settore c

  movb $45, 11(%edi)  # stampa del simbolo - 

  # Semaforo
  movb $48, 12(%edi)  # 0
  movb $48, 13(%edi)  # 0
  movb $48, 14(%edi)  # 0

  movb $10, 15(%edi)  # riga nuova

  cmp $49, %ah
  je checkparka_exit

  cmp $49, %ah
  jne not_full_parka_exit

  addl $6, %esi   # messo a 6 perche logica come exit
  addl $16, %edi


  jmp end


inotuppercase:

  movb $67, (%edi)    # C = CLOSE
  movb $67, 1(%edi)   # C = CLOSE 

  movb $45, 2(%edi)   # stampa del simbolo -

  mov %ax, 3(%edi)   # stampa numero auto settore a

  movb $45, 5(%edi)   # stampa del simbolo -

  mov %bx, 6(%edi)   # stampa numero auto settore b

  movb $45, 8(%edi)   # stampa del simbolo -

  mov %dx, 9(%edi)   # stampa numero auto settore c

  movb $45, 11(%edi)  # stampa del simbolo -

  movb $48, 12(%edi)  # 0
  movb $48, 13(%edi)  # 0
  movb $48, 14(%edi)  # 0

  movb $10, 15(%edi)  # riga nuova

  cmp $49, %ah
  je checkparka

  cmp $49, %ah
  jne not_full_parka

  addl $5, %esi   # messo a 5 perche logica come enter
  addl $16, %edi

  jmp end


onotuppercase:

  movb $67, (%edi)    # C = CLOSE
  movb $67, 1(%edi)   # C = CLOSE 

  movb $45, 2(%edi)   # stampa del simbolo -

  mov %ax, 3(%edi)   # stampa numero auto settore a

  movb $45, 5(%edi)   # stampa del simbolo -

  mov %bx, 6(%edi)   # stampa numero auto settore b

  movb $45, 8(%edi)   # stampa del simbolo -

  mov %dx, 9(%edi)   # stampa numero auto settore c

  movb $45, 11(%edi)  # stampa del simbolo -

  movb $48, 12(%edi)  # 0
  movb $48, 13(%edi)  # 0
  movb $48, 14(%edi)  # 0

  movb $10, 15(%edi)  # riga nuova

  cmp $49, %ah
  je checkparka

  cmp $49, %ah
  jne not_full_parka

  addl $6, %esi   # messo a 6 perche logica come exit
  addl $16, %edi

  jmp end


checkparka:

  cmp $51, %al
  je full_parka

  cmp $51, %al
  jl not_full_parka

  jmp end

checkparka_exit:

  cmp $51, %al
  je full_parka_exit

  cmp $51, %al
  jl not_full_parka_exit

  jmp end


full_parka:

  movb $49, 12(%edi)  # 1
  movb $10, 15(%edi)  # riga nuova

  cmp $49, %bh    # se la cifra è 1 (31) 
  je parkbcheck

  addl $5, %esi
  addl $16, %edi

  jmp end


not_full_parka:

  movb $48, 12(%edi)  # 0
  movb $10, 15(%edi)  # riga nuova

  cmp $49, %bh    # se la cifra è 1 (31) 
  je parkbcheck

  addl $5, %esi
  addl $16, %edi

  jmp end


parkbcheck:

  cmp $51, %bl    # se la cifra è 3
  je full_parkb

  jmp end


full_parkb:

  movb $49, 13(%edi)  # 1
  movb $10, 15(%edi)  # riga nuova

  cmp $52, %dh    # se la cifra è 4 (24)
  je parkccheck

  addl $5, %esi
  addl $16, %edi
  
  jmp end


parkccheck:

  cmp $50, %dl  # 2
  je full_parkc

  jmp end


full_parkc:

  movb $49, 14(%edi)  # 1
  movb $10, 15(%edi)  # riga nuova

  addl $5, %esi
  addl $16, %edi
  
  jmp end


not_full_parka_exit:

  movb $48, 12(%edi)  # 0
  movb $10, 15(%edi)  # riga nuova

  cmp $49, %bh    # se la cifra è 1 (31) 
  je park2bcheck

  addl $6, %esi
  addl $16, %edi
  
  jmp end


full_parka_exit:

  movb $49, 12(%edi)  # 1
  movb $10, 15(%edi)  # riga nuova

  cmp $49, %bh    # se la cifra è 1 (31) 
  je park2bcheck

  addl $6, %esi
  addl $16, %edi
  
  jmp end


park2bcheck:

  cmp $51, %bl    # se la cifra è 1 (31) 
  je full_parkb_exit

  jmp end


full_parkb_exit:

  movb $49, 13(%edi)  # 1
  movb $10, 15(%edi)  # riga nuova

  cmp $52, %dh    # 4
  je park2ccheck

  addl $6, %esi
  addl $16, %edi
  
  jmp end


park2ccheck:

  cmp $50, %dl
  je full_parkc_exit

  jmp end


full_parkc_exit:

  movb $49, 14(%edi)  # 1
  movb $10, 15(%edi)  # riga nuova

  addl $6, %esi
  addl $16, %edi
  
  jmp end


end:

  cmpb $0, (%esi)
  jne start   # se non e' alla fine del file ricomincia

  # ripristina registri
  popl %esi
  popl %edi
  popl %ecx 
  popl %ebx
  popl %eax
  popl %ebp

  ret
