#-------------------------------------------------------------------------
#   Samuele Marzola - Luca Lovato
#   Automated Park- File : elaborazione.blif
#-------------------------------------------------------------------------

.model elaborazione
.inputs A B C D E INSB OUTSB Z1 Z2 Z3
.outputs SBARRAIN SBARRAOUT PA PB PC

#--------------------------------#
#--------- COSTANTI -------------#
#--------------------------------#

# Costante 5 bit tutti a 1
.subckt	COST_O COST_O4=COST1_4 COST_O3=COST1_3 COST_O2=COST1_2 COST_O1=COST1_1 COST_O0=COST1_0

# Costante 5 bit tutti a 0
.subckt	COST_Z COST_Z4=COST0_4 COST_Z3=COST0_3 COST_Z2=COST0_2 COST_Z1=COST0_1 COST_Z0=COST0_0

#--------------------------------#
#--------- REGISTRI -------------#
#--------------------------------#

# Registro COUNT_r 
.subckt registro5 A4=COUNT_IN4 A3=COUNT_IN3 A2=COUNT_IN2 A1=COUNT_IN1 A0=COUNT_IN0 O4=COUNT_OUT4 O3=COUNT_OUT3 O2=COUNT_OUT2 O1=COUNT_OUT1 O0=COUNT_OUT0

# Registro Sector A
.subckt registro5 A4=SECTORA_IN4 A3=SECTORA_IN3 A2=SECTORA_IN2 A1=SECTORA_IN1 A0=SECTORA_IN0 O4=SECTORA_OUT4 O3=SECTORA_OUT3 O2=SECTORA_OUT2 O1=SECTORA_OUT1 O0=SECTORA_OUT0

# Registro Sector B
.subckt registro5 A4=SECTORB_IN4 A3=SECTORB_IN3 A2=SECTORB_IN2 A1=SECTORB_IN1 A0=SECTORB_IN0 O4=SECTORB_OUT4 O3=SECTORB_OUT3 O2=SECTORB_OUT2 O1=SECTORB_OUT1 O0=SECTORB_OUT0

# Registro Sector C
.subckt registro5 A4=SECTORC_IN4 A3=SECTORC_IN3 A2=SECTORC_IN2 A1=SECTORC_IN1 A0=SECTORC_IN0 O4=SECTORC_OUT4 O3=SECTORC_OUT3 O2=SECTORC_OUT2 O1=SECTORC_OUT1 O0=SECTORC_OUT0

#----------------------------------------------------#
#--------- AGGIORNAMENTO REGISTRO COUNT -------------#
#----------------------------------------------------#

# (input) ABCDE == 11111 ? FIRST = 1 : FIRST = 0
.subckt uguale A4=A A3=B A2=C A1=D A0=E B4=COST1_4 B3=COST1_3 B2=COST1_2 B1=COST1_1 B0=COST1_0 O=FIRST

# (output) count == 00000 ? INIT_COUNT_R = 1 : INIT_COUNT_R = 0
.subckt uguale A4=COUNT_OUT4 A3=COUNT_OUT3 A2=COUNT_OUT2 A1=COUNT_OUT1 A0=COUNT_OUT0 B4=COST0_4 B3=COST0_3 B2=COST0_2 B1=COST0_1 B0=COST0_0 O=INIT_COUNT_R

# FIRST && INIT_COUNT_R => START
.subckt and a=FIRST b=INIT_COUNT_R z=START

# FASE INSERIMENTO A (output) count == 00001 ? FASE_A = 1 : FASE_A = 0 
.subckt uguale A4=COUNT_OUT4 A3=COUNT_OUT3 A2=COUNT_OUT2 A1=COUNT_OUT1 A0=COUNT_OUT0 B4=COST0_4 B3=COST0_3 B2=COST0_2 B1=COST0_1 B0=COST1_0 O=FASE_A

# FASE INSERIMENTO B (output) count == 00010 ? FASE_B = 1 : FASE_B = 0 
.subckt uguale A4=COUNT_OUT4 A3=COUNT_OUT3 A2=COUNT_OUT2 A1=COUNT_OUT1 A0=COUNT_OUT0 B4=COST0_4 B3=COST0_3 B2=COST0_2 B1=COST1_1 B0=COST0_0 O=FASE_B

# FASE INSERIMENTO C (output) count == 00011 ? FASE_C = 1 : FASE_C = 0 
.subckt uguale A4=COUNT_OUT4 A3=COUNT_OUT3 A2=COUNT_OUT2 A1=COUNT_OUT1 A0=COUNT_OUT0 B4=COST0_4 B3=COST0_3 B2=COST0_2 B1=COST1_1 B0=COST1_0 O=FASE_C

# OR tra tutti i bit di controllo
.subckt or5 A=START B=FASE_A C=FASE_B D=FASE_C O=TO_SUM_COUNT_R

# Incrementa o meno COUNT
.subckt sommatore5 i4=COST0_4 i3=COST0_4 i2=COST0_4 i1=COST0_4 i0=TO_SUM_COUNT_R k4=COUNT_OUT4 k3=COUNT_OUT3 k2=COUNT_OUT2 k1=COUNT_OUT1 k0=COUNT_OUT0 cin=COST0_0 o4=COUNT_IN4 o3=COUNT_IN3 o2=COUNT_IN2 o1=COUNT_IN1 o0=COUNT_IN0

#----------------------------------------#
#--------- GESTIONE SBARRE --------------#
#----------------------------------------#

# TO DO meglio fare un or a tre entrate!
.subckt or A=SBARRAIN2 B=SBARRAIN3 O=SBARRAIN_AB
.subckt or A=SBARRAOUT2 B=SBARRAOUT3 O=SBARRAOUT_AB
.subckt or A=SBARRAIN_AB B=SBARRAIN4 O=SBARRAIN
.subckt or A=SBARRAOUT_AB B=SBARRAOUT4 O=SBARRAOUT

##################################
########## SECTOR_A ##############
##################################

# 0 0 Z1 Z2 Z3 == 0 0 1 0 0 ? SELA = 1 : SELA = 0
.subckt uguale A4=COST0_4 A3=COST0_3 A2=Z1 A1=Z2 A0=Z3 B4=COST0_0 B3=COST0_1 B2=COST1_0 B1=COST0_3 B0=COST0_4 O=SELA

# SELAA = 1 se si deve aggiornare il registro di A
.subckt or A=FASE_A B=SELA O=SELAA

.subckt mux5 P4=SECTORA_OUT4 P3=SECTORA_OUT3 P2=SECTORA_OUT2 P1=SECTORA_OUT1 P0=SECTORA_OUT0 B4=A B3=B B2=C B1=D B0=E SEL=NPI O4=PP4 O3=PP3 O2=PP2 O1=PP1 O0=PP0

# Mux che riscrive il valore di REG A o inserisce i 5 bin in input
.subckt mux5 P4=SECTORB_OUT4 P3=SECTORA_OUT3 P2=SECTORA_OUT2 P1=SECTORA_OUT1 P0=SECTORA_OUT0 B4=A B3=B B2=C B1=D B0=E SEL=SELAA O4=SECAIN_4 O3=SECAIN_3 O2=SECAIN_2 O1=SECAIN_1 O0=SECAIN_0

# SBARRAIN2 = 1 se una macchina vuole entrare nel parcheggio
.subckt and a=SELAA  b=SBARRAIN_A  z=SBARRAIN2

# SBARRAOUT2 = 1 se una macchina vuole uscire dal parcheggio
.subckt and a=SELAA  b=SBARRAOUT_A  z=SBARRAOUT2

# Se una macchina vuole entrare, restituisce il valore in output dal sommatore 
.subckt mux5 P4=SECAIN_4 P3=SECAIN_3 P2=SECAIN_2 P1=SECAIN_1 P0=SECAIN_0 B4=SMT_4 B3=SMT_3 B2=SMT_2 B1=SMT_1 B0=SMT_0 SEL=SBARRAIN2 O4=PPP4 O3=PPP3 O2=PPP2 O1=PPP1 O0=PPP0

.subckt mux5 P4=PPP4 P3=PPP3 P2=PPP2 P1=PPP1 P0=PPP0 B4=COST1_0 B3=COST1_2 B2=COST1_3 B1=COST1_4 B0=COST1_4 SEL=PI2 O4=SECA2IN_4 O3=SECA2IN_3 O2=SECA2IN_2 O1=SECA2IN_1 O0=SECA2IN_0

# Se una macchina vuole uscire, restituisce il valore in ouput dal sottrattore 
.subckt mux5 P4=SECA2IN_4 P3=SECA2IN_3 P2=SECA2IN_2 P1=SECA2IN_1 P0=SECA2IN_0 B4=SOTT4 B3=SOTT3 B2=SOTT2 B1=SOTT1 B0=SOTT0 SEL=SBARRAOUT2 O4=SECTORA_IN4 O3=SECTORA_IN3 O2=SECTORA_IN2 O1=SECTORA_IN1 O0=SECTORA_IN0

# Somma 1 all'output del registro SECTA
.subckt sommatore5 i4=SECTORA_OUT4 i3=SECTORA_OUT3 i2=SECTORA_OUT2 i1=SECTORA_OUT1 i0=SECTORA_OUT0 k4=COST0_4 k3=COST0_3 k2=COST0_2 k1=COST0_1 k0=COST1_0 cin=COST0_0 o4=SMT_4 o3=SMT_3 o2=SMT_2 o1=SMT_1 o0=SMT_0 cout=cout

# Sottrae 1 all'output del registro SECTA
.subckt sottrattore5 ina1=SECTORA_OUT4 ina2=SECTORA_OUT3 ina3=SECTORA_OUT2 ina4=SECTORA_OUT1 ina5=SECTORA_OUT0 inb1=COST0_4 inb2=COST0_3 inb3=COST0_2 inb4=COST0_1 inb5=COST1_0 out1=SOTT4 out2=SOTT3 out3=SOTT2 out4=SOTT1 out5=SOTT0 cerr=cout_sot

#------------------------------------------------#
#------ CHECK SECTOR_A DISPONIBILE O PIENO ------#
#------------------------------------------------#

# >= 011111
.subckt uguale A4=SECTORA_IN4 A3=SECTORA_IN3 A2=SECTORA_IN2 A1=SECTORA_IN1 A0=SECTORA_IN0 B4=COST1_4 B3=COST1_3 B2=COST1_2 B1=COST1_1 B0=COST1_0 O=PA

# >= 011111
.subckt uguale A4=SECTORA_OUT4 A3=SECTORA_OUT3 A2=SECTORA_OUT2 A1=SECTORA_OUT1 A0=SECTORA_OUT0 B4=COST1_4 B3=COST1_3 B2=COST1_2 B1=COST1_1 B0=COST1_0 O=PI2

# = 00000
.subckt uguale A4=SECTORA_OUT4 A3=SECTORA_OUT3 A2=SECTORA_OUT2 A1=SECTORA_OUT1 A0=SECTORA_OUT0 B4=COST0_4 B3=COST0_3 B2=COST0_2 B1=COST0_1 B0=COST0_0 O=PO2

#-------------------------------------#
#------- SE PIENO NON ALZARE ---------#
#-------------------------------------#
.subckt not C=PI2 R=NPI
.subckt and a=NPI  b=INSB  z=SBARRAIN_A

#-------------------------------------#
#------- SE VUOTO NON ALZARE ---------#
#-------------------------------------#
.subckt not C=PO2 R=NPO
.subckt and a=NPO b=OUTSB  z=SBARRAOUT_A


##################################
########## SECTOR_B ##############
################################## 

# 0 0 Z1 Z2 Z3 == 0 0 0 1 0 ? SELB = 1 : SELB = 0
.subckt uguale A4=COST0_4 A3=COST0_3 A2=Z1 A1=Z2 A0=Z3 B4=COST0_0 B3=COST0_1 B2=COST0_0 B1=COST1_3 B0=COST0_4 O=SELB

# SELBB = 1 se si deve aggiornare il registro di B
.subckt or A=FASE_B B=SELB O=SELBB

# Mux che riscrive il valore di REG B o inserisce i 5 bin in input
.subckt mux5 P4=SECTORB_OUT4 P3=SECTORB_OUT3 P2=SECTORB_OUT2 P1=SECTORB_OUT1 P0=SECTORB_OUT0 B4=A B3=B B2=C B1=D B0=E SEL=SELBB O4=SECBIN_4 O3=SECBIN_3 O2=SECBIN_2 O1=SECBIN_1 O0=SECBIN_0

# SBARRAIN3 = 1 se una macchina vuole entrare nel parcheggio
.subckt and a=SELBB  b=SBARRAIN_B  z=SBARRAIN3

# SBARRAOUT3 = 1 se una macchina vuole uscire dal parcheggio
.subckt and a=SELBB  b=SBARRAOUT_B  z=SBARRAOUT3

# Se una macchina vuole entrare, restituisce il valore in output dal sommatore 
.subckt mux5 P4=SECBIN_4 P3=SECBIN_3 P2=SECBIN_2 P1=SECBIN_1 P0=SECBIN_0 B4=SMT2_4 B3=SMT2_3 B2=SMT2_2 B1=SMT2_1 B0=SMT2_0 SEL=SBARRAIN3 O4=PPP5 O3=PPP6 O2=PPP7 O1=PPP8 O0=PPP9

.subckt mux5 P4=PPP5 P3=PPP6 P2=PPP7 P1=PPP8 P0=PPP9 B4=COST1_0 B3=COST1_2 B2=COST1_3 B1=COST1_4 B0=COST1_4 SEL=PI3 O4=SECB2IN_4 O3=SECB2IN_3 O2=SECB2IN_2 O1=SECB2IN_1 O0=SECB2IN_0

# Se una macchina vuole uscire, restituisce il valore in ouput dal sottrattore 
.subckt mux5 P4=SECB2IN_4 P3=SECB2IN_3 P2=SECB2IN_2 P1=SECB2IN_1 P0=SECB2IN_0 B4=SOTT2_4 B3=SOTT2_3 B2=SOTT2_2 B1=SOTT2_1 B0=SOTT2_0 SEL=SBARRAOUT3 O4=SECTORB_IN4 O3=SECTORB_IN3 O2=SECTORB_IN2 O1=SECTORB_IN1 O0=SECTORB_IN0

# Somma 1 all'output del registro SECTB
.subckt sommatore5 i4=SECTORB_OUT4 i3=SECTORB_OUT3 i2=SECTORB_OUT2 i1=SECTORB_OUT1 i0=SECTORB_OUT0 k4=COST0_4 k3=COST0_3 k2=COST0_2 k1=COST0_1 k0=COST1_0 cin=COST0_0 o4=SMT2_4 o3=SMT2_3 o2=SMT2_2 o1=SMT2_1 o0=SMT2_0 cout=cout2

# Sottrae 1 all'output del registro SECTB
.subckt sottrattore5 ina1=SECTORB_OUT4 ina2=SECTORB_OUT3 ina3=SECTORB_OUT2 ina4=SECTORB_OUT1 ina5=SECTORB_OUT0 inb1=COST0_4 inb2=COST0_3 inb3=COST0_2 inb4=COST0_1 inb5=COST1_0 out1=SOTT2_4 out2=SOTT2_3 out3=SOTT2_2 out4=SOTT2_1 out5=SOTT2_0 cerr=coutsot2

#------------------------------------------------#
#------ CHECK SECTOR_B DISPONIBILE O PIENO ------#
#------------------------------------------------#

# >= 011111
.subckt uguale A4=SECTORB_IN4 A3=SECTORB_IN3 A2=SECTORB_IN2 A1=SECTORB_IN1 A0=SECTORB_IN0 B4=COST1_4 B3=COST1_3 B2=COST1_2 B1=COST1_1 B0=COST1_0 O=PB

# >= 011111
.subckt uguale A4=SECTORB_OUT4 A3=SECTORB_OUT3 A2=SECTORB_OUT2 A1=SECTORB_OUT1 A0=SECTORB_OUT0 B4=COST1_4 B3=COST1_3 B2=COST1_2 B1=COST1_1 B0=COST1_0 O=PI3

# = 00000
.subckt uguale A4=SECTORB_OUT4 A3=SECTORB_OUT3 A2=SECTORB_OUT2 A1=SECTORB_OUT1 A0=SECTORB_OUT0 B4=COST0_4 B3=COST0_3 B2=COST0_2 B1=COST0_1 B0=COST0_0 O=PO3

#-------------------------------------#
#------- SE PIENO NON ALZARE ---------#
#-------------------------------------#
.subckt not C=PI3 R=NPI2
.subckt and a=NPI2  b=INSB  z=SBARRAIN_B

#-------------------------------------#
#------- SE VUOTO NON ALZARE ---------#
#-------------------------------------#
.subckt not C=PO3 R=NPO2
.subckt and a=NPO2 b=OUTSB  z=SBARRAOUT_B

##################################
########## SECTOR_C ##############
################################## 

# 0 0 Z1 Z2 Z3 == 0 0 0 0 1 ? SELC = 1 : SELC = 0
.subckt uguale A4=COST0_4 A3=COST0_3 A2=Z1 A1=Z2 A0=Z3 B4=COST0_4 B3=COST0_3 B2=COST0_2 B1=COST0_1 B0=COST1_0 O=SELC

# ABCDE > 1 1 0 0 0 ? MORETHAN = 1 : MORETHAN = 0 
.subckt maggiore5 i4=A i3=B i2=C i1=D i0=E k4=COST1_4 k3=COST1_3 k2=COST0_2 k1=COST0_1 k0=COST0_0 o0=MORETHAN

# MUX Maggiore
.subckt mux5 P4=A P3=B P2=C P1=C P0=D B4=COST1_4 B3=COST1_3 B2=COST0_2 B1=COST0_1 B0=COST0_0 SEL=MORETHAN O4=MUXOUT_4 O3=MUXOUT_3 O2=MUXOUT_2 O1=MUXOUT_1 O0=MUXOUT_0

# SELCC = 1 se si deve aggiornare il registro di C
.subckt or A=FASE_C B=SELC O=SELCC

# (output) COUNTER == 00011 ? (input) SECTORC = [UscitaMUX] : (input) SECTORC = SECTORC
.subckt mux5 P4=SECTORC_OUT4 P3=SECTORC_OUT3 P2=SECTORC_OUT2 P1=SECTORC_OUT1 P0=SECTORC_OUT0 B4=MUXOUT_4 B3=MUXOUT_3 B2=MUXOUT_2 B1=MUXOUT_1 B0=MUXOUT_0 SEL=SELCC O4=SECCIN_4 O3=SECCIN_3 O2=SECCIN_2 O1=SECCIN_1 O0=SECCIN_0

# SBARRAIN4 = 1 se una macchina vuole entrare nel parcheggio C
.subckt and a=SELCC  b=SBARRAIN_C  z=SBARRAIN4

# SBARRAOUT4 = 1 se una macchina vuole uscire dal parcheggio C
.subckt and a=SELCC  b=SBARRAOUT_C  z=SBARRAOUT4

# Se una macchina vuole entrare, restituisce il valore in output dal sommatore 
.subckt mux5 P4=SECCIN_4 P3=SECCIN_3 P2=SECCIN_2 P1=SECCIN_1 P0=SECCIN_0 B4=SMT3_4 B3=SMT3_3 B2=SMT3_2 B1=SMT3_1 B0=SMT3_0 SEL=SBARRAIN4 O4=PPPP4 O3=PPPP3 O2=PPPP2 O1=PPPP1 O0=PPPP0

.subckt mux5 P4=PPPP4 P3=PPPP3 P2=PPPP2 P1=PPPP1 P0=PPPP0 B4=COST1_0 B3=COST1_2 B2=COST0_2 B1=COST0_2 B0=COST0_2 SEL=PI4 O4=SECC2IN_4 O3=SECC2IN_3 O2=SECC2IN_2 O1=SECC2IN_1 O0=SECC2IN_0

# Se una macchina vuole uscire, restituisce il valore in ouput dal sottrattore 
.subckt mux5 P4=SECC2IN_4 P3=SECC2IN_3 P2=SECC2IN_2 P1=SECC2IN_1 P0=SECC2IN_0 B4=SOTT3_4 B3=SOTT3_3 B2=SOTT3_2 B1=SOTT3_1 B0=SOTT3_0 SEL=SBARRAOUT4 O4=SECTORC_IN4 O3=SECTORC_IN3 O2=SECTORC_IN2 O1=SECTORC_IN1 O0=SECTORC_IN0

# Somma 1 all'output del registro SECTC
.subckt sommatore5 i4=SECTORC_OUT4 i3=SECTORC_OUT3 i2=SECTORC_OUT2 i1=SECTORC_OUT1 i0=SECTORC_OUT0 k4=COST0_4 k3=COST0_3 k2=COST0_2 k1=COST0_1 k0=COST1_0 cin=COST0_0 o4=SMT3_4 o3=SMT3_3 o2=SMT3_2 o1=SMT3_1 o0=SMT3_0 cout=cout3

# Sottrae 1 all'output del registro SECTC
.subckt sottrattore5 ina1=SECTORC_OUT4 ina2=SECTORC_OUT3 ina3=SECTORC_OUT2 ina4=SECTORC_OUT1 ina5=SECTORC_OUT0 inb1=COST0_4 inb2=COST0_3 inb3=COST0_2 inb4=COST0_1 inb5=COST1_0 out1=SOTT3_4 out2=SOTT3_3 out3=SOTT3_2 out4=SOTT3_1 out5=SOTT3_0 cerr=coutsot3

#------------------------------------------------#
#------ CHECK SECTOR_C DISPONIBILE O PIENO ------#
#------------------------------------------------#

# >= 11000
.subckt uguale A4=SECTORC_IN4 A3=SECTORC_IN3 A2=SECTORC_IN2 A1=SECTORC_IN1 A0=SECTORC_IN0 B4=COST1_4 B3=COST1_3 B2=COST0_2 B1=COST0_1 B0=COST0_0 O=PC

# >= 11000
.subckt uguale A4=SECTORC_OUT4 A3=SECTORC_OUT3 A2=SECTORC_OUT2 A1=SECTORC_OUT1 A0=SECTORC_OUT0 B4=COST1_4 B3=COST1_3 B2=COST0_2 B1=COST0_1 B0=COST0_0 O=PI4

# = 00000
.subckt uguale A4=SECTORC_OUT4 A3=SECTORC_OUT3 A2=SECTORC_OUT2 A1=SECTORC_OUT1 A0=SECTORC_OUT0 B4=COST0_4 B3=COST0_3 B2=COST0_2 B1=COST0_1 B0=COST0_0 O=PO4

#-------------------------------------#
#------- SE PIENO NON ALZARE ---------#
#-------------------------------------#
.subckt not C=PI4 R=NPI3
.subckt and a=NPI3  b=INSB  z=SBARRAIN_C

#-------------------------------------#
#------- SE VUOTO NON ALZARE ---------#
#-------------------------------------#
.subckt not C=PO4 R=NPO3
.subckt and a=NPO3 b=OUTSB  z=SBARRAOUT_C


.search Componenti/Algebra/and.blif
.search Componenti/Algebra/not.blif
.search Componenti/Algebra/or.blif
.search Componenti/Algebra/or5.blif
.search Componenti/Algebra/sommatore5.blif
.search Componenti/Algebra/sottrattore5.blif
.search Componenti/Algebra/maggiore5.blif
.search Componenti/Algebra/uguale.blif
.search Componenti/Algebra/mux5.blif
.search Componenti/Registri/registro5.blif
.search Componenti/Costanti/cost_z.blif
.search Componenti/Costanti/cost_o.blif

.end	
