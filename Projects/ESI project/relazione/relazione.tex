%-------------------------------------
%- Documentazione di progetto
%- Analisi di difetti di tessiture
%-------------------------------------
%- Università degli Studi di Verona
%- Elaborazione di segnali e immagini
%- AA 2021-22
%------------------------------------

\documentclass[a4paper, 11pt, manychapters, dottedtoc]{report}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{classicthesis}
\usepackage[italian]{babel}
\usepackage{tabularx}	           % Required for the inclusion of tables
\usepackage{graphicx}	       	   % Required for the inclusion of images
\usepackage{booktabs}            % Quality tables in LATEX
\usepackage{eurosym}             % European currency symbol for the Euro
\usepackage{cochineal}           % Cochineal fonts with LATEX support
\usepackage{verbatim}
\usepackage{hyperref}            % Produce hypertext links in the document
\usepackage{xcolor}              % Driver-independent color extensions 
\usepackage{amsmath}             % Mathematical facilities
\usepackage{geometry}
\usepackage{array}
\usepackage{caption}

\newenvironment{absolutelynopagebreak}
  {\par\nobreak\vfil\penalty0\vfilneg
   \vtop\bgroup}
  {\par\xdef\tpd{\the\prevdepth}\egroup
   \prevdepth=\tpd}

\setlength{\parskip}{0.5em}

\clearpairofpagestyles{}
\ohead{\leftmark}
\cfoot[\pagemark]{\pagemark}

\newcolumntype{L}[1]{>{\raggedright\arraybackslash}p{#1}}
\newcolumntype{C}[1]{>{\centering\arraybackslash}p{#1}}
\newcolumntype{R}[1]{>{\raggedleft\arraybackslash}p{#1}}

\geometry{a4paper, top=3cm, bottom=3cm, left=3.5cm, right=3.5cm}

\hypersetup{colorlinks, linkcolor={black}, citecolor={black}, urlcolor={black}}

\begin{document}

\thispagestyle{empty} 

\begin{center}

\begin{figure}[t]
\centering
\includegraphics[width=0.9\textwidth]{images/univr_logo.png}
\vspace{3cm}
\end{figure}

\Huge
\textbf{ANALISI DIFETTI DI TESSITURE}

\Huge 

\vspace{3cm}
Documentazione di progetto

\huge

\vspace{1cm}

Elaborazione di segnali e immagini

\large

\vspace{3cm}

Luca LOVATO - VR443506

\vspace{0.2cm}

Matteo BAUCE - VR451800

\vspace{3cm}

Anno accademico: 2021 / 2022

\end{center}

\setcounter{page}{1} 
\chapter{Analisi difetti di tessiture}
\section{Introduzione}
\noindent Lo scopo di questa documentazione è quello di riassumere quanto sviluppato e fornire spiegazioni e dettagli sulle scelte 
progettuali ed implementative. 

\section{Scelte progettuali}
Il codice sviluppato si suddivide in \textbf{macro aree}: 

\begin{enumerate}
  \item acquisire e modificare le immagini
  \item selezionare i patterns
  \item svolgere la cross-correlazione normalizzata 2D
  \item sviluppare le maschere 
  \item filtrare e visualizzare il difetto 
\end{enumerate}

\subsection{Acquisire e modificare le immagini}
Le immagini selezionate per valutare la bontà dell'algoritmo sono suddivise in 4 cartelle. Esse vengono acquisite tramite 
un path interpretato dal comando \textbf{dir} e convertite in scala di grigi tramite il comando \textbf{rgb2gray}. Inoltre, utilizzando il comando \textbf{size} vengono salvate 
separatamente il numero di righe e il numero di colonne dell'immagine. 

\noindent Operazioni eseguite sulle immagini:
\begin{itemize}
  \item \textbf{stretching}, amplifica il range della scala di grigi per aumentare l'intensità del colore
  \item \textbf{trasformazione non lineare di potenza}, migliora la qualità e il contrasto dell'immagine
\end{itemize}

\newpage

\subsection{Selezionare i patterns}
I patterns sono tratti di regione dell'immagine (tipicamente in zone decentrate in cui si ipotizza che non sia presente il difetto)
che vengono confrontati con l'immagine originale. Tali patterns sono salvati all'interno di un cell array per rendere più immediato ed efficace 
il loro utilizzo.\newline 
Abbiamo ritenuto opportuno, dopo le dovute considerazioni, non modificare i patterns precedentemente studiati.

\subsection{Svolgere la cross-correlazione normalizzata 2D}
La cross-correlazione normalizzata 2D viene utilizzata per confrontare, volta per volta, ogni pattern con l'immagine selezionata per
evidenziare forti variazioni. Questa operazione genera l'effetto bordo che viene rimosso.  

\subsection{Sviluppare le maschere}
La maschera utilizzata inizialmente per trovare il difetto viene chiamata \textbf{firstMask}. Il comando \textbf{graythresh} applica 
la binarizzazione con il metodo di Otsu, calcolando la varianza d'interclasse tra pixel neri e pixel bianchi. Il valore ottenuto, viene dimezzato
e confrontato con l'immagine a cui viene applicata la deviazione standard.
Questa maschera, dopo le seguenti operazioni, contiene al suo interno tutti i pixel che hanno un valore compreso tra zero e il primo percentile. 

\subsection{Filtrare e visualizzare difetto}
La maschera utilizzata per trovare il difetto finale viene chiamata \textbf{secondMask}. Essa si differenzia dalla precedente perchè al suo 
interno non sono presenti i falsi positivi. Viene trovato il difetto e valutato su scala di grigi, creando un montaggio tramite comando \textbf{imshowpair}. \newline 
Infine, vengono visualizzati in modalità screensize due plot suddivisi in 6 subplot. Il primo contiene l'immagine originale
confrontata con l'immagine che risalta il difetto e il valore di threshold, il secondo contiene la secondMask di ogni immagine. \\
L'animazione di queste figure viene gestita tramite due pause, che permettono all'utente di 
osservare la maschera, il valore di threshold e infine il risultato finale. 

\newpage

\section{Esempi di immagini}

\begin{center}
  \includegraphics[width=0.7\textwidth]{images/difetto1.png} \par \medskip 
  %\includegraphics[width=0.7\textwidth]{images/difetto2.png} \par \medskip
  \includegraphics[width=0.7\textwidth]{images/difetto3.png} \par \medskip
\end{center}

\section{Modalità di sviluppo}
L'elaborato è stato svolto tramite meeting quotidiani su Google Meet, concordati con un calendario aggiornato settimanalmente.
\`E stato sviluppato utilizzando la tecnica agile di sviluppo pair programming e il software GIT 
per il controllo della versione del codice. Ciò, ci ha permesso di avere uno storico del codice sviluppato.
La presente documentazione di progetto è stata scritta in linguaggio \LaTeX.

\section{Conclusioni}
L'elaborato realizzato per il riconoscimento di difetti di tessiture, testato con le immagini presenti nelle quattro cartelle run funziona correttamente.
Intuitivamente, non si tratta di un programma senza errori, poichè tratta un argomento complesso. 
Perciò, in caso di situazioni limite, quali difetti molto piccoli o immagini molto scure, abbiamo notato che l'algoritmo ha difficoltà 
nel trovare i difetti oppure emergono falsi positivi. 

\end{document}