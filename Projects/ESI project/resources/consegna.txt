L’idea è di creare una versione più avanzata del codice visto a lezione sul tema in oggetto, ed in particolare si articola nei
seguenti sotto obiettivi:

1. Individuare un numero di casi di studio elevato (20 immagini almeno) dove evidenziare i difetti presenti

2. Trovare un’euristica che permetta di rendere il processo di rilevamento del difetto automatico, senza la necessità di selezione di soglie manuali

3. In fase di discussione, il docente potrà proporre una tessitura di test da analizzare, con cui verrà verificata la bontà del metodo Il codice deve poter elaborare quella immagine