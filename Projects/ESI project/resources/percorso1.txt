Il progetto si può fare da soli o in gruppo max 3 persone

• oltre al codice MATLAB e ai dati elaborati nel progetto, si richiede una breve relazione max 2 pag che illustri le principali
operazioni svolte e risultati ottenuti, insieme ad eventuali difficoltà/limitazioni riscontrate

• il progetto verrà presentato al docente di laboratorio attraverso una discussione dello stesso, individuale durante la quale verrà
verificata anche la conoscenza di MATLAB

la discussione del progetto avverrà durante la sessione d’esame, generalmente circa una settimana dopo ogni appello scritto Date
per la sessione invernale (1 appello 8 Febbraio 2022 • 2 appello 23 Febbraio 2022)

Nota: in caso di eventuali esami concomitanti o impegni lavorativi contattare il docente di laboratorio di riferimento

Entro i due giorni precedenti all’esame, si comunica al docente via email la partecipazione all’esame, indicando il progetto scelto
e l’eventuale composizione del gruppo nome, cognome, matricola, email per ogni componente) Si allega inoltre tutto il materiale richiesto 
( dati e breve relazione), utilizzando OneDrive UNIVR per condividere eventuali file pesanti

Alcune considerazioni importanti:

• Il progetto di laboratorio può essere discusso anche in sessioni diverse rispetto allo scritto 
• Il voto rimane valido per tutto l’anno accademico
• Per svolgere l’esame, è sufficiente inviare la mail al docente di riferimento seguendo le tempistiche/modalità indicate nella
slide precedente e risultare iscritti ad un appello scritto
