% Corso di Elaborazione dei Segnali e Immagini
% Docente: Manuele Bicego
% Docente coordinatore: Marco Cristani

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Materiale extra: alcuni ulteriori esercizi per approfondire.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% Esercizio Svolto 1 --- Thresholding & Sostituzione Background 
% Scopo dell'esercizio e' quello di applicare una sogliatura ad un'immagine
% RGB e sostuire il background con uno a piacere. Attenzione a non
% rimuovere pixel anche all'interno dell'immagine stessa! 
clear all
close all
clc

img = imread('Tigre.jpeg');
bkgr = imread('BG.png');
[nr,nc,ns]=size(img);
J = imresize(bkgr,[nr nc]);

figure(1), subplot(221), imshow(img), title('Immagine Originale')
img_r = img(:,:,1);
img_g = img(:,:,2);
img_b = img(:,:,3);
subplot(222), imshow(img_r), title('Red'), colorbar
subplot(223), imshow(img_g), title('Green'), colorbar
subplot(224), imshow(img_b), title('Blue'), colorbar

% Idea: trovo l'oggetto sui tre canali R,G,B con la binarizzazione
% Calcolo le soglie col metodo di Otsu
thr_r = 255*graythresh(img_r);
thr_g = 255*graythresh(img_g);
thr_b = 255*graythresh(img_b);

% visualizzo la binarizzazione 
figure(1), subplot(221), imshow(img), title('Immagine Originale')
subplot(222), imshow(img_r>thr_r), title('Red'), colorbar
subplot(223), imshow(img_g>thr_g), title('Green'), colorbar
subplot(224), imshow(img_b>thr_b), title('Blue'), colorbar

% Cerco di identificare il background:
% Per red è sotto la soglia, per blue e green è sopra

img_mod = img;
for i=1:nr
    for j=1:nc
        %  controllo se ho il background per tutti e tre i canali
        if (img_r(i,j)<=thr_r) & (img_g(i,j)>thr_g) & (img_b(i,j)>thr_b)
            % background: sostituisco con J
            img_mod(i,j,:) = J(i,j,:);
        end
    end
end

figure,set(gcf,'name','Sostituzione Background'),imshow(img_mod,[]), colorbar 
% Risultato non perfetto, provate ad aggiustare a mano le soglie


%% Esercizio Svolto 2 --- Equalizzazione istogramma 
% Massimizzare il contrasto dell'istogramma attraverso l'equalizzazione
% dell'istogramma. Implemento l'algoritmo visto a lezione, e confronto con histeq.
% Confrontare inoltre con uno stretching lineare dell'istogramma, 
% visualizzando in tutti i casi immagine e istogramma.  

clear all
close all
clc

img = imread('china.png');
img = rgb2gray(img);
figure;
set(gcf,'name','Equalizzazione istogramma: Originale');
subplot(121); imshow(img); title('Originale')
subplot(122); imhist(img); title('Istogramma')
H = imhist(img);

% Implementazione automatica con histeq
[newimg,LUT] = histeq(img);
figure; set(gcf,'name','Equalizzazione istogramma: Equalizzata Auto');
subplot(121); imshow(newimg);  title('Equalizzata Auto')
subplot(122); imhist(newimg);  title('Istogramma')
H = imhist(newimg);

% Implementazione manuale  
L = 256;
[m,n] = size(img);
V = img(:);
H = imhist(V);  

% Step 1 = somma cumulativa 
H_iniziale = H; 
vettore_sum_cumul = cumsum(H_iniziale); 

% Step 2 = normalizzazione
n_pixel = sum(H_iniziale); 
vettore_sum_cumul_norm = vettore_sum_cumul/n_pixel;

% Step 3 = moltiplicazione per L-1 e arrotondamento
H_equal = uint8(vettore_sum_cumul_norm*(L-1));

% Step 4 = mappatura 
im_mapping = reshape(H_equal(V+1),m,n);
figure(3), set(gcf,'name','Equalizzazione istogramma: Equalizzata manuale');
subplot(121); imshow(im_mapping);title('Equalizzata Manuale')
subplot(122); imhist(im_mapping);title('Istogramma')

% Stretching lineare 
[rows,cols]=size(img);
r_max = double(max(img(:))); 
r_min = double(min(img(:))); 
a=0;   
b=255;
newimg = imadjust(img,[r_min,r_max]./255,[a,b]./255);

figure; set(gcf,'name','Stretching lineare istogramma');
subplot(121); imshow(uint8(newimg)); title('Immagine con stretching') 
subplot(122); imhist(uint8(newimg)); title('Istogramma') 
H = imhist(newimg); 


%% Esercizio Svolto 3 --- Equalizzazione istogramma per immagini RGB ---
% Scopo dell'esercizio e' quello di applicare ad una immagine RGB varie
% operazioni locali, in particolare: negativo, stretching ed equalizzazione
% istogramma. 
clear all
close all

img = imread('WindowPainting.png');
img_r = img(:,:,1);
img_g = img(:,:,2);
img_b = img(:,:,3);
img11 = img;
t1 = 20;  
t2 = 150;  

for i=1
    img11_1(:,:,i) = 255 - img11( :, :, i ); % negativo
    img11_3(:,:,i) = imadjust(img11(:,:,i),[t1,t2]./255,[0,255]./255); % stretching
    img11_4( :, :, i ) = histeq(img11(:,:,i));  % histogram equalization
end

% Visualizzo e confronto i risultati 
figure(26), subplot(2, 2, 1), imshow(uint8(img11),[]), title('Original Picture'), colorbar
subplot(2, 2, 2), imshow(uint8(img11_1),[]), title('Negative Film'), colorbar
subplot(2, 2, 3), imshow(img11_3,[]), title('Contrast Clip & Stretch'), colorbar
subplot(2, 2, 4), imshow(img11_4,[]), title('Histogram Equalization'), colorbar

%% Esercizio Svolto 4 ---  Rumore & SNR 

% L'obiettivo di questo esercizio e' di studiare il rumore presente
% nell'immagine. 
% Una dispensa interessante per approfondire si trova in http://www.imatest.com/docs/noise/, 
% che fa chiarezza sulle cause di molti tipi di rumore su immagini. 
% Questa tratta il rumore digitale https://www.techhive.com/article/2033684/how-to-minimize-noise-in-digital-photos.html
% Per l'audio, consiglio questa fonte https://www.quora.com/What-are-various-types-of-noise-in-digital-image-processing

clear all;
close all;

% Come misurare in termini di SNR il miglioramento effettuato da un' operazione di filtraggio.

I = imread('eight.tif');
figure, imshow(I)
J = imnoise(I,'salt & pepper',0.02);
K = medfilt2(J,[3,3]);
imshowpair(J,K,'montage');
I = double(I);
J = double(J);
K = double(K);
SNR1 = var(I(:))/var(J(:))
SNR2 = var(I(:))/var(K(:))









