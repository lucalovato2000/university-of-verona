% Corso di Elaborazione dei Segnali e Immagini
% Docente: Manuele Bicego
% Docente coordinatore: Marco Cristani


%% Alcuni comandi utili disponibili nell'Image Processing Toolbox 
% imshow(I) -> mostra l'immagine I, assumendo che I contenga gia' valori
% quantizzati. 
% imhist(I) -> mostra l'istogramma dei valori di grigio dell'immagine I
% uint8(I) -> trasforma valori a doppia precisione in interi a 8 bit (0-255)
% double(I) -> passa da interi a valori in doppia precisione


%% Operazione puntuale 1: Negativo
% Caricare l'immagine "FLAIR.png" e una volta convertita in scala di grigi
% da RGB farne il negativo. Visualizzare in un'unica figura con piu' subplot le due immagini e 
% i corrispondenti istogrammi. 

clear all; 
close all; 
clc; 

MRI = imread('FLAIR.png');
MRI = rgb2gray(MRI);
MRI_neg = 255-MRI;

figure; 
subplot(221);imshow(MRI); colorbar, title ('Immagine Originale')
subplot(222);imshow(MRI_neg);colorbar, title ('Immagine Negativo')
subplot(223);imhist(MRI); title ('Hist Immagine Originale')
subplot(224);imhist(MRI_neg); title ('Hist Immagine Negativo')


%% Operazione puntuale 2: Clamping 
% Caricare l'immagine di saturno ("saturn2") dal file "imdemos.mat" 
% (il file contiene una serie di immagini in B/N). 
% Fare l'operazione di clamping e visualizzare la LUT relativa a questa operazione. 
% Infine, visualizzare le due immagini (originale/modificata) e i corrispondenti istogrammi 
clear all; 
close all; 
clc; 
load ('imdemos.mat','saturn2')
I = saturn2;

% Creo una LUT per operazione di clamping e la visualizzo
a = 110;
b = 190;
LUT=[]; 
for i = 0:255
    r = i;
    if r<a
        LUT(i+1) = a; % --> MATLAB inizia l'indicizzazione a 1, mentre le intensita' iniziano da 0
    elseif r<=b & r>=a
            LUT(i+1) = r;
        elseif r>b
            LUT(i+1) = b;
        end
end

figure;
plot([0:255],LUT), xlim([0 255]), grid on, title ('Clamping LUT')
xlabel('Valori originali')
ylabel('Valori per Clamping LUT')

% Applico la LUT e visualizzo l'immagine dopo operazione di clamping 
[r,c] = size(I);
Inew = zeros(size(I));
for i = 1:r
    for j = 1:c
        ldg = I(i,j);
        newldg = LUT(ldg+1);% --> MATLAB inizia l'indicizzazione a 1, mentre le intensita' iniziano da 0
        Inew(i,j) = newldg; 
    end
end
Inew = uint8(Inew);
% I due cicli for si possono scrivere in forma compatta:
Inew2 = uint8(LUT(I+1)); % --> MATLAB inizia l'indicizzazione a 1, mentre le intensita' iniziano da 0
% % To check
% sum(abs(Inew2-Inew),'all')
% % ans =
% %      0

figure;
subplot(221);imshow(saturn2); colorbar, title ('Immagine Originale')
subplot(223);imhist(saturn2); title ('Istogramma')
hold on; plot([a a],[0 max(imhist(saturn2))]); 
plot([b b],[0 max(imhist(saturn2))]) 
subplot(222);imshow(Inew); colorbar, title ('Immagine Clamping')
subplot(224);imhist(Inew); title ('Istogramma')




%% Operazione puntuale 3: Stretching/Shrinking ---
clear all; 
close all; 
clc; 

% Auto nella nebbia:  come fare a visualizzare meglio i numeri della targa? Data l'operazione
% puntuale costruita, visualizzarne la Look Up Table (LUT)
load fog;
figure; set(gcf,'name','Immagine originale auto nella nebbia')
imshow(img); colorbar, title('Immagine originale')
figure; set(gcf,'name','Istogramma originale auto nella nebbia')
imhist(img); title ('Istogramma')

% Individuo i limiti massimo e minimo dell'istogramma, noto che tali limiti
% non sono dovuti al rumore (non sono elementi distaccati dal corpo
% principale dell'istogramma )
r_max = double(max(img(:))); % 153
r_min = double(min(img(:)));% 128
a = 0;   % voglio usare tutti i livelli di grigio a disposizione, ovvero 256
b = 255;

% Creo una LUT per operazione di stretching
LUT=[]; 
for i = 0:255
    r = i;
    if r<=r_max & r>=r_min
        s = ((r-r_min)/(r_max-r_min))*(b-a)+a;
    else
        s = r; % fuori dal range non ho modifiche
    end
    LUT(i+1) = s;
end


newimg = LUT(img+1); % come prima: applico la LUT
newimg = uint8(newimg); % rounding 
figure; set(gcf,'name','Immagine auto nella nebbia trattata')
imshow(newimg);colorbar; title('Immagine con stretching') 
figure; set(gcf,'name','Istogramma auto nella nebbia trattata')
imhist(newimg); title('Istogramma stretchato')

% NOTA IMPORTANTE: l'operazione di stretching si può fare con imadjust
% vedi doc imadjust
% L'operazione fatta sopra si ottiene come:
% newimg2 = imadjust(img,[r_min,r_max]./255,[a,b]./255);
% % check
% sum(abs(newimg-newimg2),'all')
% % ans =
% %    0


%% Operazione puntuale 4: Trasformazione Non lineare (Log/Potenza) ---
% La trasformazione lineare espande in modo uniforme la dinamica originale dell’immagine, 
% producendo un effetto globale di miglioramento del contrasto.
% In alcuni casi pero' si ha l’esigenza di effettuare una trasformazione non uniforme,
% che agisca differentemente sui livelli di grigio. In particolare: 
% Immagine sottoesposta = i particolari interessanti sono poco evidenti e concentrati nelle zone scure; 
% in tal caso possiamo espandere la dinamica associata ai livelli scuri e comprimere quella dei livelli chiari;
% Immagine sovraesposta = i particolari interessanti sono poco evidenti e concentrati nelle zone chiare; 
% in tal caso possiamo espandere la dinamica associata ai livelli chiari e comprimere quella dei livelli scuri.
% L’espansione non uniforme della dinamica dell’immagine si puo' ottenere mediante
% elevazione a potenza dei livelli originari. 
clear all; 
close all; 
clc;

% carico immagine "Spine.png" e applico trasformazione a potenza per migliore il contrasto
img = imread('Spine.png'); 
img = (rgb2gray(img));
figure, imshow(img),colorbar
figure,  imhist(img),colorbar

% I dettagli che mi interessano sono nei livelli di grigio scuri -- 
% --> gamma <1 
gamma = 0.4;
s_tilde = double(img).^gamma;
s_tilde_min = double(min(s_tilde(:)));
s_tilde_max = double(max(s_tilde(:)));
MAX = 255;
MIN = 0; 
s = ((s_tilde - s_tilde_min)./(s_tilde_max-s_tilde_min))*(MAX-MIN) + MIN;

s = uint8(s);
figure, imshow(s),colorbar
figure, imhist(s),colorbar


%
% carico immagine "satellite.jpg" e applico trasformazione di potenza per migliore il contrasto
img = imread('satellite.jpg'); 
img = (rgb2gray(img));
figure, imshow(img),colorbar
figure, imhist(img),colorbar

% I dettagli che mi interessano sono nei livelli di grigio chiari -- 
% --> gamma >1 
gamma = 4;
s_tilde = double(img).^gamma;
s_tilde_min = double(min(s_tilde(:)));
s_tilde_max = double(max(s_tilde(:)));
MAX = 255;
MIN = 0; 
s = ((s_tilde - s_tilde_min)./(s_tilde_max-s_tilde_min))*(MAX-MIN) + MIN;

s = uint8(s);

figure,  imshow(s),colorbar
figure, imhist(s),colorbar


%% Operazione puntuale 5: Binarizzazione ---
% Produce una immagine che ha solo due livelli: nero e bianco. 
% Si ottiene scegliendo una soglia T e mettendo a nero tutti i pixel 
% il cui valore è minore a T e a bianco tutti gli altri.
% La difficoltà risiede nel saper scegliere la soglia T più ragionevole
% Ci sono metodi per scegliere automaticamente la soglia (Metodo di Otsu)
clear all; 
close all; 
clc;


I = imread('coins.png');
figure
set(gcf,'name','Binarizzazione di immagini')
subplot(121); imshow(I); title('Immagine originale');


T = 80; % soglia
BW = zeros(size(I)); % immagine binaria
BW(I>T) = 1;
subplot(122); imshow(BW);title('Immagine binarizzata');

% Nota: si può ottenere lo stesso risultato utilizzando
% il comando imbinarize. 
% La soglia deve essere tra 0 e 1
% Si veda doc imbinarize

% BW2 = imbinarize(I,T/255);
% % check che siano uguali
% sum(abs(BW-BW2),'all')
% % ans =
%   %  0


%%%%%%%%%%%%%%%%%%%%%
%%%% ESERCIZI %%%%%%%
%%%%%%%%%%%%%%%%%%%%%


%% Esercizio 1: provare a eliminare il rumore impulsivo da butterfly.jpg 
% Cercare di capire qual'è operazione puntuale più adatta

clear all
close all
clc

I = imread('butterfly.jpg');
I = rgb2gray(I);
figure, imshow(I), title('Immagine originale')

% Soluzione: clamping
% Visualizzo l'istogramma per capire i limiti
figure, imhist(I), title('Istogramma Immagine originale')

a = 43;
b = 103;
LUT=[]; 
for i = 0:255
    r = i;
    if r<a
        LUT(i+1) = a;
    elseif r<=b & r>=a
            LUT(i+1) = r;
        elseif r>b
            LUT(i+1) = b;
        end
end
% visualizzo la LUT
figure;
plot([0:255],LUT), xlim([0 255]), grid on

% clamping 
I_clip = uint8(LUT(I+1));
% visualizzo l'immagine risultante
figure;
imshow(I_clip)


%% Esercizio 2: provare a recuperare dettagli non visibili 
% In 'xray.png' ho un'immagine ai raggi x per la valutazione di tumori
% ai polmoni. Un'ulteriore analisi senza aggiungere altre radiazioni
% vorrebbe controllare i dischi vertebrali, in particolare il loro aspetto.
% Si vuole quindi applicare una operazione puntuale per evidenziare le
% suddivisioni orizzontali della spina vertebrale. 
% Suggerimento: uno stretching lineare non e' sufficiente...

clear all
close all
clc

img = imread('xray.png');
img = rgb2gray(img);

figure; set(gcf,'name','Immagine originale raggi x')
imshow(img); colorbar


% immagine originale  
figure; set(gcf,'name','Immagine e istogramma')
subplot(121);imshow(img); colorbar
subplot(122);imhist(img);

% stretching lineare
newimg_lineare = imadjust(img,[100,230]./255,[0,1]);
figure; set(gcf,'name','Immagine con stretching lineare raggi x')
subplot(121);imshow(newimg_lineare); colorbar
subplot(122);imhist(newimg_lineare);

% stretching non lineare
% i dettagli che mi interessano sono nei livelli 
% di grigio chiari -> gamma > 1
gamma = 10;
s_tilde = double(img).^gamma;
s_tilde_min = double(min(s_tilde(:)));
s_tilde_max = double(max(s_tilde(:)));
MAX = 255;
MIN = 0; 
s = ((s_tilde - s_tilde_min)./(s_tilde_max-s_tilde_min))*(MAX-MIN) + MIN;
s = uint8(s);
figure; set(gcf,'name','Immagine con stretching non lineare raggi x')
subplot(121);imshow(s); colorbar
subplot(122);imhist(s);

% confronto tra le diverse immagini 
figure; set(gcf,'name','Confronto immagini')
subplot(131);imshow(img); colorbar, title('Immagine Orig')
subplot(132);imshow(newimg_lineare); colorbar, title('Immagine Str.Lineare')
subplot(133);imshow(s); colorbar, title('Immagine Str.Non Lineare')



%% Esercizio 3  -  Provare a segmentare l'oggetto dallo sfondo con la binarizzazione 
% Applicare l'algoritmo di binarizzazione alle immagini nella lista seguente 
% con lo scopo di identificare l'oggetto dallo sfondo.
% Utilizzare sia soglie settate a mano che la soglia determinata con il
% metodo di Otsu:
% T = graythresh(I) -> ritorna la soglia ottimale per la binarizzazione
% si veda help graythresh
% Vedere l'effetto della segmentazione oggetto/sfondo 
% al variare delle soglie

clear all
close all
clc

% Lista immagini, li metto in un array di celle
Im{1} = imread('coins.png');
Im{2} = rgb2gray(imread('pag136.jpg'));
Im{3} = imread('moon.tif');
load('imdemos.mat');
Im{4} = eight;
Im{5} = quarter;
Im{6} = rice2;
Im{7} = rice3;
Im{8} = saturn2;

% Provo con la soglia automatica
% di Otsu, se non funziona provare ad aggiustarla a mano
for i = 1:8
    I = Im{i};
    figure
    subplot(121); imshow(I);title('Immagine Originale');
    T(i) = graythresh(I);
    BW = imbinarize(I,T(i));
    subplot(122); imshow(BW);title('Immagine binarizzata');
end
% Otsu non funziona bene per 1 e 4, provo soglie a mano
I = Im{1};
newT = 80;
figure,
subplot(121); imshow(I);title('Immagine Originale');
BW = imbinarize(I,newT./255);
subplot(122); imshow(BW);title('Immagine binarizzata');

I = Im{4};
newT = 215;
figure,
subplot(121); imshow(I);title('Immagine Originale');
BW = imbinarize(I,newT./255);
subplot(122); imshow(BW);title('Immagine binarizzata');


%% Esercizio Extra --- Binarizzazione con metodo di Otsu ---
% L'esercizio consiste nell'implementare a mano l'algoritmo di binarizzazione 
% secondo il metodo di Otsu visto a lezione e confrontare la soglia ottenuta 
% con quella identificata
% automaticamente da MATLAB con il comando "graythresh" 

clear all
close all
clc

B = imread('moon.tif');
H = imhist(B(:));
Index=[1:256]';

risultato = zeros(1,256);
for i = 1:256
    [wbk,varbk]= Otsu(1,i,H,Index);
    [wfg,varfg]= Otsu(i+1,256,H,Index);
    risultato(i)=(wbk*varbk)+(wfg*varfg);
end

[value,thr_pos]=min(risultato);
tval=(thr_pos-1)/255;
bin_im = imbinarize(B,tval);
figure,imshow(bin_im);


