% Corso di Elaborazione dei Segnali e Immagini
% Docente: Manuele Bicego
% Docente coordinatore: Marco Cristani
% MATERIALE EXTRA

clear all;
close all;
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Esempio EXTRA 1 - Filtraggio Immagini in Frequenza ----
% Implementazione A BASSO LIVELLO del filtraggio di immagini come descritto
% sul Gonzalez Sez.4.7.3. Basso livello significa che:  
% 1) lo zero-padding viene eseguito manualmente nelle immagini;
% 2) La funzione FFTshift non viene applicata come fa la funzione di MATLAB
% (nello spazio delle frequenze, mediante shift circolare) ma DIRETTAMENTE
% nello spazio, attraverso la moltiplicazione con una funzione di
% centramento. 

clear all; close all; clc

% 1) Carico immagine selezionata
I = rgb2gray(imread('peacock.jpg'));
I = double(I);
[M,N] = size(I);

% 2) Operazione di padding, ricordando che tipicamente P = 2M, Q = 2N. Inoltre, 
% poiché lo spettro di ampiezza della trasformata di Fourier è  
% insensibile alle traslazioni, per semplicità gli zeri sono attaccati
% tutti da una parte e in basso all'immagine

P = 2*M; 
Q = 2*N;
Ipad = zeros(P,Q);
Ipad(1:M,1:N) = I; 

% 3) Eseguo l'analogo dell'fftshift ma nello spazio (la spiegazione di questo passaggio la si trova in sezione 4.6.3 del Gonzalez): 
for i = 1:P
    for j = 1:Q
        Ipad_center(i,j) = Ipad(i,j)*((-1)^(i+j));
    end
end

% 4) Calcolo la DFT dell'immagine appena creata al punto 3 
FFT_Ipad_center = fft2(Ipad_center);

% 5) Genero un filtro passa-basso di butterworth, centrato in (P/2,Q/2): 
n = 1; % ordine del filtro di Butterworth
thresh = 100; % Raggio di cutoff nel dominio delle frequenze, valido sia per Butterworth che per il Gaussiano

% PASSA BASSO
H = blpf(FFT_Ipad_center,thresh,n); % filtro butterworth
figure; imshow(H,[]); title('Filtro')

% 6) Eseguo la moltiplicazione tra filtro al punto 5 e con risultato della
% DFT al punto 4 
him = H.*FFT_Ipad_center; 

% 7) Ricostruisco l'immagine tramite FFT inversa e ricentratura (immagine
% di dimensione PxQ)
IFFT_Ipad_center = real(ifft2(him));
 
for i=1:P
    for j=1:Q
         ifim(i,j)=IFFT_Ipad_center(i,j)*((-1)^(i+j));
    end
end

% 8) Rimozione del padding, in modo da tornare ad una immagine di
% dimensioni MxN

rim = ifim(1:M,1:N);
rim=uint8(rim);
figure,imshow(rim,[]); title ('Immagine dopo filtraggio passa-basso')

% 9) Visualizzare graficamente tutte le fasi del filtraggio:

figure;
subplot(2,3,1);imshow(uint8(I));title('Original image');
subplot(2,3,2);imshow(uint8(Ipad));title('Padding');
subplot(2,3,3);imshow(uint8(Ipad_center)); title('Transform centering');
subplot(2,3,4);imshow(log10(1+abs(him)),[]); title('Fourier Transform');
subplot(2,3,5);imshow(uint8(ifim),[]);title('Inverse Fourier transform with padding');
subplot(2,3,6);imshow(uint8(rim),[]);title('Filtered image');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Esempio Extra 2 - Rimozione specifiche frequenze tramite filtri ferma-banda
% Rimuovere dall'immagine "cameraman.tif" il rumore sinusoidale aggiunto attraverso un 
% opportuno filtro ferma-banda. 

clear all
close all
clc

% Carico immagine selezionata
I = imread('cameraman.tif');

I = double(I);
[M,N] = size(I);

% Applico rumore
xCoords = linspace(1,256,256);
yCoords = xCoords';
noise = -sin(xCoords*1.5)-sin(yCoords*1.5);
I = I + 25*noise;

figure, imagesc(I);

% 1. Creare il padding (funzione padarray)
Ipad = padarray(I,[M N],0,'post');

% 2. Calcolare la DFT dell'immagine appena creata e applicare lo shift
% (fftshift)
FFT_Ipad_center = fftshift(fft2(Ipad));

% 3. Generare un filtro ferma-banda di Butterworth (funzione bbsf)
n = 1; % ordine del filtro di Butterworth
thresh_min = 120; % Raggio di cutoff minimo nel dominio delle frequenze, valido sia per Butterworth che per il Gaussiano
thresh_max = 150; % Raggio di cutoff massimo nel dominio delle frequenze, valido sia per Butterworth che per il Gaussiano
W = thresh_max - thresh_min; % Spessore banda

H = bbsf(FFT_Ipad_center,thresh_min,W,n); % filtro butterworth
figure; imshow(H,[]); title('Filtro')

% 4. Eseguire la moltiplicazione tra filtro e il risultato della
% DFT 
him = H.*FFT_Ipad_center; 

% 5. Ricostruire l'immagine tramite FFT inversa e ricentratura (immagine
% di dimensione PxQ) (funzione ifftshift per la ricentratura)
ifim = real(ifft2(ifftshift(him)));

% 6. Rimozione del padding, in modo da tornare ad una immagine di
% dimensioni MxN
rim = ifim(1:M,1:N);
rim=uint8(rim);
figure,imshow(rim,[]); title ('Immagine dopo filtraggio ferma-banda')

% 7. Visualizzare graficamente tutte le fasi del filtraggio
figure;
subplot(2,2,1);imshow(uint8(I));title('Original image');
subplot(2,2,2);imshow(uint8(Ipad));title('Padding');
subplot(2,2,3);imshow(log10(1+abs(him)),[]); title('Fourier Transform');
subplot(2,2,4);imshow(uint8(rim),[]);title('Filtered image');




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Esempio EXTRA 3 - In questo esempio vedremo come fare la trasformata di Hough
% usando le funzioni MATLAB già implementate
% Sono tre le funzioni di riferimento in MATLAB:

% [H,theta,rho] = hough(BW) -> esegue la trasformata di Hough e ritorna:
% "rho" = distanza dall'origine fino alla linea lungo una
% retta perpendicolare alla linea stessa
% "theta" = l'angolo tra l'asse x e la retta perpendicolare
% "H" = la matrice di Hough le cui righe e colonne corrispondono ai valori
% di rho e theta 
%
% peaks = houghpeaks(H,numpeaks) -> identifica i picchi nella matrice H
% derivante dalla funzione hough. Numpeaks specifica il numero massimo di
% picchi da considerare. L'output, "peaks", ritorna una matrice contenente
% nelle righe/colonne le coordinate x/y dei picchi
%
% lines = houghlines(BW,theta,rho,peaks) -> estrae le linee nell'immagine
% binaria a partire dalle coordinate dei picchi contenute in peaks e dai
% valori di rho/theta. L'output, "lines", e' una struttura contenente
% informazioni sulle linee estratte. 

% Creo una immagine simulata, di dimensione 50x50 con valori tutti a 0 tranne
% lungo una diagonale di dimensione 31
image = zeros(50,50);
image(10:40, 10:40) = eye(31);
[M,N] = size(image);
figure(1), imagesc(image), title('Immagine Binaria Simulata')

% 1. Eseguo la trasformata di Hough e la visualizzo 
[H_M,theta_M,rho_M] = hough(image);
figure(2), imagesc(H_M), colormap jet, title('Trasformata di Hough')

% 2. Identifico i picchi nella trasformata di Hough
peaks = houghpeaks(H_M,5);
display(peaks)

% 3. Estraggo e visualizzo le linee sovrapposte all'immagine di partenza 
lines = houghlines(image,theta_M,rho_M,peaks);
figure(3), imagesc(image), hold on
for k = 1:length(lines)
   xy = [lines(k).point1; lines(k).point2];
   plot(xy(:,1),xy(:,2),'LineWidth',2,'Color','green');
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Esempio EXTRA 4 - Applicazione ad immagini reali 
% Testare la trasformata di Hough, sia quella implementata manualmente che
% quella disponibile in MATLAB, per identificare linee su immagini reali, come ad esempio
% "building.jpg" oppure "Crane.png". Valutare come cambiano i risultati al variare del 
% numero di picchi/soglia utilizzati per identificare le linee. 

% Soluzione - 
% Implementazione manuale: 
% 1. Carico immagine ed estraggo i contorni con algoritmo Canny 
RGB = imread('Crane.png');
I  = rgb2gray(RGB);
image = edge(I,'canny');
[M,N]= size(image);
figure(1), subplot(121), imshow(I), title('Immagine di Partenza'),axis square
subplot(122), imagesc(image), title('Edge - Canny'),axis square

% 2. Definisco due vettori per rho e theta 
theta = [-90:89]; % range per theta varia tra –90° ≤ θ < 90°
diagonal = sqrt(M^2+N^2);
rho = [-floor(diagonal):floor(diagonal)];

% 3. Creo una matrice 2D che rappresenta l'accumulatore 
A = zeros(2*floor(diagonal),length(theta));

% 4. Trasformata di Hough
for i = 1:M
    for j = 1:N
        if image(i,j) == 1
            for angles = 1:length(theta)
                rho_values = j*cosd(theta(angles)) + i*sind(theta(angles));
                t = find(rho==round(rho_values));
                A(t, angles) = A(t,angles)+1;
            end
        end
    end
end
figure(2)
imagesc(A), colormap jet, title('Trasformata di Hough')  

% 5. Trovo i picchi nella trasformata di Hough
thr = max(A(:))*0.4;
[nr,nc] = find(A>thr);     

% 6. Visualizzo le linee sovrapposte all'immagine di partenza 
x_temp = [1,M*N]; 
figure(3)
imagesc(image), title('Linee identificate')  
for index = 1:length(nr)
    d = rho(nr(index));
    y_temp = (d-x_temp.*cosd(theta(nc(index))))./sind(theta(nc(index)));
    hold on
    plot([x_temp(1) x_temp(end)],[round(y_temp(1)) ceil(y_temp(2))],'-g')
end

%%
close all
% Funzioni MATLAB: 
% 1. Eseguo la trasformata di Hough e la visualizzo 
[H_M,theta_M,rho_M] = hough(image);
figure(1), imagesc(H_M), colormap jet, title('Trasformata di Hough')

% 2. Identifico i picchi nella trasformata di Hough
peaks = houghpeaks(H_M,15);

% 3. Estraggo e visualizzo le linee sovrapposte all'immagine di partenza 
lines = houghlines(image,theta_M,rho_M,peaks);
figure(2), imagesc(image), title('Linee identificate'), hold on
for k = 1:length(lines)
   xy = [lines(k).point1; lines(k).point2];
   plot(xy(:,1),xy(:,2),'LineWidth',2,'Color','green');
end



