% Corso di Elaborazione dei Segnali e Immagini
% Docente: Manuele Bicego
% Docente coordinatore: Marco Cristani

clear all;
close all;
clc

% PARTE 1: trasformata di Fourier 2D

%% Esempio 1 - Trasformata di Fourier discreta 2D in immagini reali ---- 
% In questo esercizio vedremo come calcolare la trasformata di fourier per
% un'immagine, visualizzare lo spettro delle ampiezze e lo spettro di fase
% e come ricostruire l'immagine a partire dalle frequenze 

imdata = imread('imageA1.png');
imdata = rgb2gray(imdata);
figure(1); imshow(imdata), title('Original Image');

% Calcolo la trasformata di Fourier dell'immagine 
F = fft2(imdata);

% FFT dell'immagine: spettro di ampiezza
S = abs(F);
figure(2);imshow(S,[20 100000]), 
title('Magnitude Spectrum');

% Centro lo spettro delle ampiezze per aiutare la visualizzazione 
Fsh = fftshift(F);
figure(3);imshow(abs(Fsh),[20 100000]), 
title('Centered Magnitude Spectrum'); 

% Applico il logaritmo 
S2 = log10(1+abs(Fsh));
figure(4);imshow(S2,[]), 
title('Log transformed Magnitude Spectrum');

% FFT dell'immagine: spettro di fase
figure(5);imshow(angle(fftshift(F)),[-pi pi]), 
title('Phase Spectrum');

% Ricostruzione dell'immagine a partire dalle frequenze 
F = ifftshift(Fsh);
f = ifft2(F);
f = real(f);
figure(6);imshow(f,[]),
title('Reconstructed Image');



%% ESERCIZIO 1 
% A partire dalla stessa immagine o da un'altra a piacere, visualizzare
% lo spettro delle ampiezze centrato (e visualizzato con il logaritmo),
% estrarre una sola porzione delle frequenze e ricostruire l'immagine a 
% partire da queste frequenze selezionate. 
%
% Osservare cosa succede se cambiamo il contenuto in frequenza scelto. 
% In particolare: cosa succede se escludo le basse frequenze? E se escludo
% le alte?
%
% Suggerimento per selezionare le sequenze:
% creare una matrice, della stessa dimensione della fft2, contenente tutti 
% zeri; copiare nella posizione della porzione considerata le frequenze 
% corrispondenti dalla matrice della fft2
%
% Porzioni da considerare (sulla trasformata shiftata):
% 1) righe: 260-360, colonne: 410-520
% 2) righe: 150-460, colonne: 270-670
% 3) righe: 105-310, colonne: 175-460
% 4) ...
% 
% Nota: provare anche ad usare getrect per selezionare la porzione: 
%    (La funzione getrect era stata usata anche nell'esercizio 1 della lezione 
%     Lab2 parte 2 (file Lab2_parte2_soluzione.zip in moodle))


imdata = imread('imageA1.png');
imdata = rgb2gray(imdata);


% SOLUZIONE
% Calcolo FFT2 e visualizzo lo spettro di ampiezza centrato (con logaritmo)
F = fft2(imdata);
Fsh = fftshift(F);
figure(1);imshow(log10(1+abs(Fsh)),[]); 

% Seleziono la porzione di frequenze.

% Selezione manuale (decommentare e mettere i valori giusti)
% 1) righe: 260-360, colonne: 410-520
% 2) righe: 150-460, colonne: 270-670
% 3) righe: 105-310, colonne: 175-460
% 4) ...
% rangeX = 260:360;
% rangeY = 410:520;


% Selezione con getrect 
rect = getrect;
x = int32(rect(2));
X = int32(rect(2)+rect(4));
rangeX = x:X;
y = int32(rect(1));
Y = int32(rect(1)+rect(3));
rangeY = y:Y;


% creo un'immagine delle stesse dimensioni della trasformata:
croppedSection = zeros(size(Fsh));
croppedSection(rangeX,rangeY) = Fsh(rangeX,rangeY);

figure (2), imshow(log10(1+abs(croppedSection)),[])
axis image off
colormap gray
title('Section')

% Ricostruisco immagine a partire dalla sola selezione di frequenze fatta
Test = ifft2(ifftshift(croppedSection));

% Visualizzo le due immagini
figure(3), imshowpair(imdata, real(Test),'montage')

% FINE SOLUZIONE


%%%%%%%%%%%%%%%%%%
%% Esempi EXTRA
% Ulteriori esempi su filtraggi in frequenza possono essere trovati nel
% file ESILab5_Extra.m (nello zip delle soluzioni)
% In particolare:
% -- Esempio EXTRA 1 - Filtraggio Immagini in Frequenza ----
%         Implementazione A BASSO LIVELLO del filtraggio di immagini come 
%         descritto sul Gonzalez Sez.4.7.3.
% -- Esempio EXTRA 2 - Rimozione specifiche frequenze tramite filtri 
%         ferma-banda
%%%%%%%%%%%%%%%%%%



%% Parte 2 - Trasformata di Hough per le linee

%% Esercizio 1 - Implementazione manuale della trasformata di Hough per linee
% Obiettivo di questo esercizio e' quello di implementare manualmente la
% trasformata di Hough, partendo da un'immagine simulata (binaria). 
% Generalmente questa si ottiene attraverso l'applicazione di un algoritmo 
% di estrazione degli edge all'immagine di partenza (per esempio Canny).


% 1. Creo una immagine simulata, di dimensione 50x50 con valori tutti a 0 tranne
% lungo una diagonale di dimensione 31

image = zeros(50,50);
image(10:40, 10:40) = eye(31);
[M,N] = size(image);
figure(1), imagesc(image), title('Immagine Binaria Simulata')


% SOLUZIONE

% 2. Definisco due vettori per rho e theta con lo spazio dei parametri
% discretizzato
% theta varia tra –90° ≤ θ ≤ 90°
% rho varia tra -diagonal ≤ rho ≤ diagonal
% dove diagonal è la lunghezza  della diagonale -- sqrt(M^2+N^2)
% Occorre decidere il passo di discretizzazione (quanto approssimato
% è lo spazio di accumulazione)
% Inizialmente metterlo a 1, poi provare a vedere cosa succede variandolo)

Steptheta = 1;
Steprho = 1;
thetad = -90:Steptheta:90; 
diagonal = sqrt(M^2+N^2);
rhod = -round(diagonal):Steprho:round(diagonal); 

% 3. Creo una matrice 2D che rappresenta lo spazio di accumulazione
% La matrice avrà dimensione RxT, dove R e T sono il numero di diversi
% valori di rho e di theta
A = zeros(length(rhod),length(thetad));

% 4. Calcolo lo spazio di accumulazione dei parametri
for i = 1:M
    for j = 1:N
        % per tutti i punti dell'immagine che sono a 1
        if image(i,j) == 1
            x = i;
            y = j;
            % x e y sono le coordinate del punto
            % per ogni valore di theta 
            for angles = 1:length(thetad)
                theta = thetad(angles);
                % calcolo rho
                rho = j*cosd(theta) + i*sind(theta);
                % trovo il rhod più vicino
                [dist,t] = min(abs(rho-rhod));
                A(t, angles) = A(t,angles)+1;
            end
        end
    end
end
figure(2)
imagesc(A), colormap jet, title('Trasformata di Hough')  

% 5. Trovo il max nella trasformata di Hough
[nr,nc] = find(A==max(A(:)));
% In generale devo trovare tutti i massimi locali, e si fa
% [nr,nc] = find(A>thr) dove thr deve essere definita sulla base
% dei valori a disposizione. Maggiore e' la soglia, piu' selettivo sara' il
% risultato. Ad esempio possiamo utilizzare thr = max(A(:))*0.8
        
% 6. Visualizzo le linee sovrapposte all'immagine di partenza 
% scelgo due punti x e calcolo le corrispondenti y, poi uso plot
% le y si trovano ricavando la y dalla formula 
% rho = x*cos(theta) + y*sin(theta)
% y = (rho-x*cos(theta))/sin(theta)

x_temp = [1,M*N]; 
figure(3)
imagesc(image)

for index = 1:length(nr)
    rho = rhod(nr(index));
    theta = thetad(nc(index));
    y_temp = (rho-x_temp.*cosd(theta))./sind(theta);
    hold on
    plot([x_temp(1) x_temp(end)],[round(y_temp(1)) ceil(y_temp(2))],'-g')
end


%%%%%%%%%%%%%%%%%%
%% Esempi EXTRA
% Ulteriori esempi sulla trasformata di Hough possono essere trovati nel
% file ESILab5_Extra.m (nello zip delle soluzioni)
% In particolare:
% -- Esempio EXTRA 3 - Calcolo dellatrasformata di Hough
%         usando le funzioni MATLAB già implementate
% -- Esempio EXTRA 4 - Applicazione della trasformata di Hough
%         ad immagini reali 
%%%%%%%%%%%%%%%%%%






