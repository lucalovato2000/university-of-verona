function res = checksym(A)

%   CHECKSYM tells whether the matrix A is symmetric or not
%   CHECKSYM takes as input the matrix A, gives res=1 if A is symmetric,
%   res=0 if A is not symmetric, res=-1 if A cannot be checked against symmetry.

[m,n,z]=size(A);

if z>1
    fprintf('Matrix A is more than 2D, symmetry not defined');
    res=-1;
    return
elseif    (m-n)~=0
    fprintf('Matrix A is not squared, symmetry not defined');
    res=-1;
    return
end

% Idea: simmetria = matrice uguale alla sua trasposta

B = A-A'; % if A is symmetric, this will give a matrix of zeros. 
Bsum = sum(abs(B(:))); % B(:) makes B in rasterscan (single column) and "sum" should sum to 0 in the case of symmetry

if Bsum==0
    res = 1;
else
    res = 0;
end

end
    

