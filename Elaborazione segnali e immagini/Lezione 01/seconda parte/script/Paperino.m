% Corso di Elaborazione dei Segnali e Immagini
% Docente: Manuele Bicego 
% Docente Coordinatore: Marco Cristani
% LAB1 - parte 2 

clear all
close all
clc

% Esercizi 5 e 6 - Immagine volto & indicizzazione matrice 
A = imread('Paperino.jpg');
figure(1); 
imagesc(A);
axis square
title('Immagine Originale')

B = A;
B(163:209,90:136,:)=0;
B(163:209,164:211,:)=0;
B(185:188,137:162,:)=0;

figure(2); 
imagesc(B);
axis square
title('Immagine Modificata')

figure(3);
subplot(1,2,1)
surf(rgb2gray(A))
shading('flat')
title('Immagine Originale')
subplot(1,2,2)
surf(rgb2gray(B))
surf(rgb2gray(A))
title('Immagine Modificata')

% Esercizio 7 - Immagine Grayscale
Immagine = imread('moon.tif');
figure(1)
imshow(Immagine,[]), colorbar
title ('Immagine a livelli di grigi')
[m,n] = size(Immagine);

for index = 0:1:255
    r = find(Immagine(:)==index);
    vettore_count(index+1,1) = length(r);
end

figure(2)
bar(vettore_count)
xlabel('Valori')
ylabel('Numero di pixels') 


