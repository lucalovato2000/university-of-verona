function [out] = MYflip(in)
%  MYFLIP mirrors a 1D row vector, concatenating the mirrored version as prefix of the input
%   Input: in, a row vector
%   Output: out

[m,n]= size(in);

if m>1 & n>1
    error('input is not 1D')
elseif m>1 & n==1
    error('input is a column: row is required instead')
end

% prima idea: scorro il vettore al contrario
out = [];
for i = n:-1:1
    out = [out in(i)];
end
% concateno il vettore iniziale
out = [out in];



% altra idea: scrivo davanti a in
out  = in;
for i = 1:n
    out = [in(i) out];
end



end

