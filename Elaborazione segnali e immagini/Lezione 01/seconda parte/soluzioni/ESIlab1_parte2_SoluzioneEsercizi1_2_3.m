% Corso di Elaborazione dei Segnali e Immagini
% Docente: Manuele Bicego 
% Docente Coordinatore: Marco Cristani
% LAB1 - parte 2 

clear all
close all
clc

% Esercizio 1 - MEDIA 
% Creare una function chiamata Mymean che dato un vettore o 
% una matrice in ingresso restituisca il valore medio. 
% In particolare, nel caso di vettori la function restituisce un singolo valore medio, 
% mentre per le matrici un vettore riga contenente il valor medio di ogni colonna. 

% vettore riga
vec = [1:2:30];            
vec_media = MYmean(vec);    
media_Matlab = mean(vec);   
confronto = [vec_media; media_Matlab]   

% vettore colonna
vec1 = [2:2:30]';            
vec_media1 = MYmean(vec1);    
media_Matlab1 = mean(vec1);   
confronto1 = [vec_media1; media_Matlab1]   

% matrice
A = [vec' vec1];            
mat_media = MYmean(A);
media_Matlab = mean(A);   
confronto1 = [mat_media; media_Matlab] 

% Esercizio 2 - SIMMETRIA
% Scrivere una funzione, checksym, che dia 1 se la matrice inserita è simmetrica, 0 altrimenti.

clc
matrice_A = [1 2 3; 4 5 6];
res = checksym(matrice_A);
disp([newline, 'Il controllo della simmetria ha riportato il risultato: ', num2str(res)])

% Esercizio 3 - FLIP 
% Scrivere una funzione, MYflip, che dato un vettore mu, ne crei una copia “riflessa” 
% (p.e., da [1 2 3] ottengo [3,2,1]) concatenandola a sinistra a mu (ossia [3,2,1,1,2,3]), 
% restituendo il risultato come output. Questa funziona tornerà utile nell’analisi frequenziale di segnali.

clc
vettore_A = [1 2 3];
res = MYflip(vettore_A);



