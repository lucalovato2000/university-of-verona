% Corso di Elaborazione dei Segnali e Immagini
% Docente: Manuele Bicego 
% Docente Coordinatore: Marco Cristani
% LAB1 - parte 1 

clear all
close all
clc

% Esercizio 2
%   Generare un numero casuale con il comando randn (distribuzione normale standard)
%   Assegnare alla variabile y il valore 1 se tale numero e' compreso tra -1 e 1 (media +- deviazione standard), 0 altrimenti.
%   Se ripeto il procedimento 10000 volte, quante volte il numero casuale cade nell'intervallo [-1 1]?
%   Risolvere l'esercizio senza usare cicli (suggerimento: consultate l'help della funzione randn)

% Generare un numero casuale con il comando randn (distribuzione normale
% standard). 
x = randn;

% Assegnare alla variabile y il valore 1 se tale numero e' compreso tra 
% -1 e 1 (media +- deviazione standard), 0 altrimenti.
if x < 1 & x > -1
    y = 'yes';
else
    y = 'nope';
end
% visualizziamo il numero generato
fprintf('Numero generato: %f\n', x) % funzione fprintf per stampare, utillzzare %d, %f, /n come in C

% visualizziamo y
fprintf('E'' compreso fra -1 e 1? %s\n\n', y)


%   Se ripeto il procedimento 10000 volte, quante volte il numero casuale cade nell'intervallo [-1 1]?
%   Risolvere l'esercizio senza usare cicli (suggerimento: consultate l'help della funzione randn)
x = randn([1 10000]); % Genero un vettore con 10000 numeri casuali
y = (x < 1) & (x > -1); 
totale = sum(y);
fprintf('Quante volte il numero è compreso fra -1 e 1? %d\n', totale)

