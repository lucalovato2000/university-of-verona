% Corso di Elaborazione dei Segnali e Immagini
% Docente: Manuele Bicego 
% Docente Coordinatore: Marco Cristani
% LAB2 - parte 1

clear all; 
close all; 
clc; 

%% Esempio 1 sintetico di cross-correlazione - rettangolo e triangolo

f1 = [1 1 1 1 1 1 1 1]; %box 
f2 = [1 2 3 4 5 6 7 8]; %triangolo

figure; 
set(gcf,'name','Cross Correlazione','IntegerHandle','off');
subplot(311); stem(f1,'Markersize',5,'MarkerFaceColor','b','Linewidth',1.1); grid on
subplot(312); stem(f2,'Markersize',5,'MarkerFaceColor','b','Linewidth',1.1); grid on
[f1xf2,lag] = xcorr(f1,f2);
subplot(313); 
plot(lag,f1xf2,'Linewidth',2); 
hold on; 
stem(lag,f1xf2,'Markersize',5,'MarkerFaceColor','r','Linewidth',1.1);
title('Cross-correlation');
xlabel('Lags');
ylabel('Corr Values');

%% Esercizio 1: provare a capire e completare l'animazione descritta nel codice sotto per la cross-correlazione

f1 = [1 1 1 1 1 1 1 1]; %box 
f2 = [1 2 3 4 5 6 7 8]; %triangolo

M = length(f1);
N = length(f2);

% Faccio zero padding: operazione per rendere uguali le dimensioni dei vettori (segnali) in ingresso
if N>M 
    f1 = cat(2,f1,zeros(1,N-M)); % concatena due vettori: cat(DIM,A,B)
    M=N;
elseif N<M
    f2 = cat(2,f2,zeros(1,M-N));
end

figure; set(gcf,'name','Cross Correlazione animazione','IntegerHandle','off');
subplot(511); stem(f1); title('f1')
subplot(512); stem(f2); title('f2')

% Allineo i due segnali 
tf1 = [zeros(1,M-1),f1,zeros(1,M-1)];
tf2 = [f2,zeros(1,2*M-2)];

lag = [-M+1:M-1];
MYf1xf2 = [];
for i=1:2*M-1
    subplot(513); stem(tf1); title('f1 allineato')
    subplot(514); stem(tf2); title('f2 allineato')
    % COMPLETARE CON I PASSI MANCANTI
    tf3 = xcorr(tf1, tf2);
    
end

% VISUALIZZARE CON PLOT ANCHE LA CROSS-CORRELAZIONE RISULTANTE, SOVRAPPOSTA
% AL GRAFICO STEM 
subplot(515); stem(tf3); title('Cross-correlazione')


%% Esercizio 2: provare la cross correlazione con differenti segnali definiti a mano.
% Analizzate help di xcorr per adottare anche la versione normalizzata

figure; 

a = [1 1 1 1 1 1 1 1 1]; %box 
b = [1 2 3 4 5 6 7 8]; 


[c,lags] = xcorr(a, b, 10, 'normalized');
stem(lags,c)


%% Esempio 2 - Cross correlazione tra segnali di vibrazione su un ponte emessi da un 
% veicolo in prossimita' di due diversi sensori. Obiettivo e' capire quanto
% sono diversi tra loro.

load noisysignals s1 s2;  % I segnali sono simili a quanto ottenuto da questi sensori
% https://www.luchsinger.it/it/sensori/vibrazioni/

[acor,lag] = xcorr(s2,s1);
[~,I] = max(abs(acor)); 
timeDiff = lag(I);         
figure;
subplot(411); plot(s2); title('s2');
subplot(412); plot(s1); title('s1');
subplot(413); plot(lag,acor);
subplot(414); plot([zeros(1,abs(timeDiff)) s2']); % Perche' timeDiff = -350
hold on; plot(s1);
title('Cross-correlation between s1 and s2')
    
       
%% Esercizio 3: Cross-correlazione su segnali audio: riconiscemento del suono
% Attraverso la cross-correlazione confrontate l'esempio di test con le varie canzoni della galleria 
% e verificate a quale essa appartenga.

[Y1,fs1] = audioread('funky.mp3',[1,96000*20]);
[Y2,fs2] = audioread('lost.mp3',[1,96000*20]);
[Y3,fs3] = audioread('Diana.mp3',[1,96000*20]);
[Y4,fs4] = audioread('never.mp3',[1,96000*20]);
[Y5,fs5] = audioread('T69.mp3',[1,96000*20]);

%sound(Y4(1:96000*3,1),fs4) % Per ascoltare

figure; set(gcf,'name','Dataset canzoni','IntegerHandle','off');
subplot(2,3,1); plot(Y1(1:96000*1,1));
subplot(2,3,2); plot(Y2(1:96000*1,1));
subplot(2,3,3); plot(Y3(1:96000*1,1));
subplot(2,3,4); plot(Y4(1:96000*1,1));
subplot(2,3,5); plot(Y5(1:96000*1,1));

%%Array di celle: un metodo piu'veloce per raccogliere sequenze di lunghezza diversa.

gallery{1}=Y1(:,1);
gallery{2}=Y2(:,1);
gallery{3}=Y3(:,1);
gallery{4}=Y4(:,1);
gallery{5}=Y5(:,1);

test=Y2(96000*2:96000*7,:);

% DA COMPLETARE CON I PASSI MANCANTI: 

% Calcolare la cross-correlazione tra la galleria e il segnale di test
[c1, lags1] = xcorr(gallery{1}, test(:, 1));
[c2, lags2] = xcorr(gallery{2}, test(:, 1));
[c3, lags3] = xcorr(gallery{3}, test(:, 1));
[c4, lags4] = xcorr(gallery{4}, test(:, 1));
[c5, lags5] = xcorr(gallery{5}, test(:, 1));


% Visualizzare in un'unica figura la cross-correlazione per ogni caso
figure(); set(gcf,'name','TEST 1','IntegerHandle','off');
plot(lags1, c1)

figure(); set(gcf,'name','TEST 2','IntegerHandle','off');
plot(lags2, c2)

figure(); set(gcf,'name','TEST 3','IntegerHandle','off');
plot(lags3, c3)

figure(); set(gcf,'name','TEST 4','IntegerHandle','off');
plot(lags4, c4)

figure(); set(gcf,'name','TEST 5','IntegerHandle','off');
plot(lags5, c5)

% Cercare il match e infine visualizzare,sovrapposti in un plot,il segnale di test con
% la porzione corrispondente del segnale originario 

[~,I] = max(abs(c2)); 
timeDiff = lag(I);         
figure;
subplot(411); plot(test); title('test');
subplot(413); plot(lags2, c2); title('Segnale di test 2');
subplot(414); plot([zeros(1,abs(timeDiff)) test']); % Perche' timeDiff = -350
hold on; plot(c2);
title('Sovrapposizione segnali')


%% Esercizio 4 (FACOLTATIVO): Cross-correlazione 1D tra segnali di risonanza magnetica funzionale. 
% Carico i segnali medi fMRI di un soggetto, misurati per 200 istanti in
% 100 regioni e utilizzo questa informazione per estrarre delle matrici di
% connettivita', che spiegano come le aree cerebrali comunicano tra di
% loro. 

clear all
close all
clc

load Ab_pASL_Yeo_Average.txt
[nr,nc] = size(Ab_pASL_Yeo_Average);

figure(1)
plot(Ab_pASL_Yeo_Average(:,3),'r','LineWidth',2)
hold on
plot(Ab_pASL_Yeo_Average(:,4),'b','LineWidth',2)
xlim([0 200])
grid on

figure(2)
plot(Ab_pASL_Yeo_Average(:,3)-mean(Ab_pASL_Yeo_Average(:,3)),'r','LineWidth',2)
hold on
plot(Ab_pASL_Yeo_Average(:,4)-mean(Ab_pASL_Yeo_Average(:,4)),'b','LineWidth',2)
xlim([0 200])
grid on

matrice_corr = corr(Ab_pASL_Yeo_Average,'Rows','all');

% Calcola i segnali demeaned, ovvero dopo la rimozione della media 

% Calcola per ogni coppia di segnali la cross-correlazione normalizzata.
% Salvare in matrici separate il valore massimo della cross-correlazione, la
% posizione (lag) corrispondente, e il valore della xcorr in corrispondenza
% di lag=0


% Visualizzare le tre matrici in un'unica figura, utilizzando il range 0-1 
% per i valori e una colormap jet(imagesc) 


% Visualizzare la matrice di lag (imagesc)



%% Esempio 3: Cross-correlazione 2D
% Controllate rispetto a quanto visto in teoria se tornano i conti: 
 
X1 =[1     2     5     2     5;
     3     4     5     4     2;
     5     2     3     2     2;
     2     3     2     4     2;
     3     4     2     2     3];
     
     
X2 =[3     3     2;
     2     3     4;
     4     5     4];
 
Corr1 = xcorr2(X1,X2);

% Per fare prove ulteriori, si decommenti il codice qui sotto; 
X1 = round(unifrnd(1,5,5,5));
X2 = round(unifrnd(1,5,3,3));
Corr = xcorr2(X1,X2);

%% Esercizio 5: Implementazione manuale della cross-correlazione 2D 
% Attenzione all'ordine, ovvero xcorr2(X1,X2) e' diverso da xcorr2(X2,X1).




%% Esempio 4: Matching 2D
template = 0.2*ones(11);
template(6,3:9) = 0.6;
template(3:9,6) = 0.6;
offsetTemplate = 0.2*ones(22);
offset = [8 6];
offsetTemplate((1:size(template,1))+offset(1), ...
    (1:size(template,2))+offset(2)) = template;

figure; set(gcf,'name','Cross correlazione 2D','IntegerHandle','off');
subplot(211); imagesc(template); title('template originale');
colormap gray
axis equal
subplot(212); imagesc(offsetTemplate); title('template shiftato');
axis equal

figure; set(gcf,'name','Template: Orig e Shiftato','IntegerHandle','off');
imagesc(offsetTemplate)
colormap gray
hold on
imagesc(template)
axis equal

cc = xcorr2(offsetTemplate,template);
figure; imagesc(cc);
set(gcf,'name','Cross correlazione 2D applicazione','IntegerHandle','off');

[max_cc, imax] = max(abs(cc(:)));
[ypeak, xpeak] = ind2sub(size(cc),imax);
corr_offset = [(ypeak-size(template,1)) (xpeak-size(template,2))];

isequal(corr_offset,offset)

%% Esercizio 6: provare la cross-correlazione 2D con differenti segnali 
% definiti a mano e diversi shift











