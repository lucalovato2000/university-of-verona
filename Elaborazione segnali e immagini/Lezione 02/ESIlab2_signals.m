% Corso di Elaborazione dei Segnali e Immagini
% Docente: Manuele Bicego
% Docente Coordinatore: Marco Cristani
% LAB2 

clear all 
close all
clc

%% Segnali Seno e Coseno

t = linspace(0,1,1000);
f=1000; 
x=sin(2*pi*f*t); 
figure; 
set(gcf,'name','Sinwave','IntegerHandle','off');
plot(t,x,'Color','m','Linewidth',1.2); 
grid;
title('Sinewave');
xlabel('Time');
ylabel('Amplitude');

t = linspace(0,1,1000);
f=1000; 
y=cos(2*pi*f*t); 
figure; 
set(gcf,'name','Coswave','IntegerHandle','off');
plot(t,y,'Color','r','Linewidth',1.2); 
grid;
title('Coswave');
xlabel('Time');
ylabel('Amplitude');

%% Segnale Esponenziale

figure; 
set(gcf,'name','Segnale Esponenziale','IntegerHandle','off');
t=linspace(0,40,200);
alpha=0.3;
x=exp(-alpha*t);
plot(t,x,'Color','k','Linewidth',1.2);
grid;
title('Exponential Signal');
xlabel('Time');
ylabel('Amplitude');


%% Segnale impulso unitario

N=100; 
samples= [-N/2:N/2]; 
x=zeros(1,N+1); 
x((N/2)+1)=1; 
figure; 
set(gcf,'name','Segnale impulso unitario','IntegerHandle','off');
stem(samples,x,'Markersize',5,'MarkerFaceColor','b','Linewidth',1.1); % Plot the impulse
grid;
title('Unit Impulse Signal');
xlabel('Sample Number');
ylabel('Amplitude');

%% Segnale gradino unitario

N=50; % Define the number of samples
samples=[-N/2:N/2]; % Define a suitable discrete time axis
u1 = zeros(1,(N/2));
u2 = ones(1,(N/2)+1);
u=[u1 u2]; 
figure; 
set(gcf,'name','Segnale gradino unitario','IntegerHandle','off');
stem(samples,u,'Markersize',5,'MarkerFaceColor','b','Linewidth',1.1); % Plot the signal
axis([-N/2,N/2,-0.5,1.5]); % Scale axis
grid;
title('Unit Step Signal');
xlabel('Sample Number');
ylabel('Amplitude');

%% Segnale rettangolare

t = -1:0.01:1;
w = 0.8;
x = rectpuls(t,w);
figure; 
set(gcf,'name','Segnale rettangolare','IntegerHandle','off');
stem(t,x,'Markersize',5,'MarkerFaceColor','b','Linewidth',1.1)
title('Rectangular Pulse');
grid on
xlabel('Samples');
ylabel('Amplitude');


%% Segnale dente di sega
figure; 
set(gcf,'name','Segnale dente di sega','IntegerHandle','off');
T = 2;
passo = 1/1000;
t = 0:passo:(T/2-passo);
alpha = 2;
x = sawtooth(2*pi*alpha*t);
figure(1)
plot(t,x)

%% Segnali Random 

N=1056; 
Vec1=randn(1,N); 
Vec2=rand(1,N); 

figure; 
set(gcf,'name','Segnali Random','IntegerHandle','off');
subplot(2,2,1); 
plot(Vec1); 
grid;
title('Normally Distributed Random Signal');
xlabel('Samples');
ylabel('Amplitude');

subplot(2,2,2); % Select the second qudrant
histogram(Vec1,20); % Plot the histogram of Vec1
grid;
title('Histogram');
xlabel('Samples');
ylabel('Total');

subplot(2,2,3);
plot(Vec2);
grid;
title('Uniformly Distributed Random Signal');
xlabel('Sample Number');
ylabel('Amplitude');

subplot(2,2,4);
histogram(Vec2,20);
grid;
title('Histogram');
xlabel('Samples');
ylabel('Total');
