% Corso di Elaborazione dei Segnali e Immagini
% Docente: Manuele Bicego
% Docente Coordinatore: Marco Cristani
% LAB2 - parte 1

clear all; 
close all; 
clc; 

%% Esempio sintetico di cross-correlazione - rettangolo e triangolo

f1 = [1 1 1 1 1 1 1 1]; %box 
f2 = [1 2 3 4 5 6 7 8]; %triangolo
%f2 = [8 7 6 5 4 3 2 1]; %triangolo "ribaltato"

figure; 
set(gcf,'name','Cross Correlazione','IntegerHandle','off');
subplot(311); stem(f1,'Markersize',5,'MarkerFaceColor','b','Linewidth',1.1); grid on
subplot(312); stem(f2,'Markersize',5,'MarkerFaceColor','b','Linewidth',1.1); grid on
[f1xf2,lag] = xcorr(f1,f2);
subplot(313); 
plot(lag,f1xf2,'Linewidth',2); 
hold on; 
stem(lag,f1xf2,'Markersize',5,'MarkerFaceColor','r','Linewidth',1.1);
title('Cross-correlation');
xlabel('Lags');
ylabel('Corr Values');

%% Esercizio 1: provare a capire e completare l'animazione descritta nel codice sotto per la cross-correlazione

f1 = [1 1 1 1 1 1 1 1]; %box 
f2 = [1 2 3 4 5 6 7 8]; %triangolo

%f1 = [0.1 0.2  -0.1 4.1 -2 1.5 0 ];
%f2 = [0.1 4 -2.2 1.6 0.1 0.1 0.2];

M = length(f1);
N = length(f2);

% Faccio zero padding: operazione per rendere uguali le dimensioni dei vettori (segnali) in ingresso
if N>M 
    f1 = cat(2,f1,zeros(1,N-M)); % concatena due vettori: cat(DIM,A,B)
    M=N;
elseif N<M
    f2 = cat(2,f2,zeros(1,M-N));
end

figure; set(gcf,'name','Cross Correlazione animazione','IntegerHandle','off');
subplot(511); stem(f1); title('f1')
subplot(512); stem(f2); title('f2')

% Allineo i due segnali 
tf1 = [zeros(1,M-1),f1,zeros(1,M-1)];
tf2 = [f2,zeros(1,2*M-2)];

lag = [-M+1:M-1];
MYf1xf2 = [];
for i=1:2*M-1
    subplot(513); stem(tf1); title('f1 allineato')
    subplot(514); stem(tf2); title('f2 allineato')
    MYf1xf2 = [MYf1xf2 sum(tf1.*tf2)];
    tf2 = circshift(tf2,1,2);
    subplot(515); stem(lag(1:i),MYf1xf2); xlim([-M+1 M-1]);
    pause(1);
end
hold on; subplot(515); plot(lag(1:i),MYf1xf2); 

%% Esercizio 2 (FACOLTATIVO): provare la cross correlazione con differenti segnali definiti a mano.
% Analizzate help di xcorr per adottare anche la versione normalizzata

f1 = [1 1 1 1 1 1 1 1]; %box 
f2 = [1 2 3 4 5 6 7 8]; %triangolo

[autocor_norm,lags] = xcorr(f1,f2,'normalized');
[autocor_nonorm,lags] = xcorr(f1,f2);
figure; 
set(gcf,'name','Esempi cross-correlazione','IntegerHandle','off');
subplot(211)
plot(lags,autocor_norm)
grid on
xlabel('Lags');
ylabel('Corr Values');
subplot(212)
plot(lags,autocor_nonorm)
grid on
xlabel('Lags');
ylabel('Corr Values');


%% Esempio 2 - Cross correlazione tra segnali di vibrazione su un ponte emessi da un 
% veicolo in prossimita' di due diversi sensori. Obiettivo e' capire quanto
% sono diversi tra loro.

load noisysignals s1 s2;  % caricamento segnali. I segnali sono simili a quanto ottenuto da questi sensori
% https://www.luchsinger.it/it/sensori/vibrazioni/
[acor,lag] = xcorr(s2,s1);
[~,I] = max(abs(acor));
timeDiff = lag(I);         
figure;
subplot(411); plot(s1); title('s1');
subplot(412); plot(s2); title('s2');
subplot(413); plot(lag,acor);
subplot(414); plot([zeros(1,-timeDiff) s2']); % Perche' timeDiff = -350
hold on; plot(s1);
title('Cross-correlation between s1 and s2')
    
    
%% Esercizio 3: Cross-correlazione su segnali audio: riconiscemento del suono
% Attraverso la cross-correlazione confrontate l'esempio di test con le varie canzoni della galleria 
% e verificate a quale essa appartenga.

[Y1,fs1] = audioread('funky.mp3',[1,96000*20]);
[Y2,fs2] = audioread('lost.mp3',[1,96000*20]);
[Y3,fs3] = audioread('Diana.mp3',[1,96000*20]);
[Y4,fs4] = audioread('never.mp3',[1,96000*20]);
[Y5,fs5] = audioread('T69.mp3',[1,96000*20]);

sound(Y4(1:96000*3,1),fs4)

figure; set(gcf,'name','Dataset canzoni','IntegerHandle','off');
subplot(2,3,1); plot(Y1(1:96000*1,1));
subplot(2,3,2); plot(Y2(1:96000*1,1));
subplot(2,3,3); plot(Y3(1:96000*1,1));
subplot(2,3,4); plot(Y4(1:96000*1,1));
subplot(2,3,5); plot(Y5(1:96000*1,1));

%%Array di celle: un metodo piu'veloce per raccogliere sequenze di lunghezza diversa.

gallery{1}=Y1(:,1);
gallery{2}=Y2(:,1);
gallery{3}=Y3(:,1);
gallery{4}=Y4(:,1);
gallery{5}=Y5(:,1);

test=Y2(96000*2:96000*7,:);

for g=1:5
    [xc{g},lagc{g}]= xcorr(gallery{g},test(:,1));
end

figure; set(gcf,'name','Risultati di matching','IntegerHandle','off');

for g=1:5
    subplot(2,3,g); plot(lagc{g},xc{g});
end

[maxcorr,maxli]=max(xc{2});

sound(Y2(lagc{2}(maxli)+1:lagc{2}(maxli)+480001,:),fs2);
sound(test,fs2);

figure
plot(test(:,1),'-r','LineWidth',2);
hold on 
plot(Y2(lagc{2}(maxli)+1:lagc{2}(maxli)+480001,1),'-k','LineWidth',1);

%% Esercizio 4 (FACOLTATIVO): Cross-correlazione 1D tra segnali di risonanza magnetica funzionale.
% Carico i segnali medi fMRI di un soggetto, misurati per 200 istanti in
% 100 regioni e utilizzo questa informazione per estrarre delle matrici di
% connettivita', che spiegano come le aree cerebrali comunicano tra di
% loro. 

clear all
close all
clc

load Ab_pASL_Yeo_Average.txt
[nr,nc] = size(Ab_pASL_Yeo_Average);

figure(1)
plot(Ab_pASL_Yeo_Average(:,3),'r','LineWidth',2)
hold on
plot(Ab_pASL_Yeo_Average(:,4),'b','LineWidth',2)
xlim([0 200])
grid on
title ('Segnali 3 e 4')

figure(2)
plot(Ab_pASL_Yeo_Average(:,3)-mean(Ab_pASL_Yeo_Average(:,3)),'r','LineWidth',2)
hold on
plot(Ab_pASL_Yeo_Average(:,4)-mean(Ab_pASL_Yeo_Average(:,4)),'b','LineWidth',2)
xlim([0 200])
grid on
title ('Segnali 3 e 4 - Demeaned')

% Calcola la correlazione
matrice_corr = corr(Ab_pASL_Yeo_Average,'Rows','all');

% Calcola i segnali demeaned, ovvero dopo la rimozione della media 
Signals_demean = Ab_pASL_Yeo_Average - mean(Ab_pASL_Yeo_Average,1);

% Calcola per ogni coppia di segnali la cross-correlazione normalizzata.
% Salvare in matrici separate il valore massimo della cross-correlazione, la
% posizione (lag) corrispondente, e il valore della xcorr in corrispondenza
% di lag=0

for i = 1:nc
    for j = 1:nc
        [xcorrel, lags] = xcorr(Signals_demean(:,i),Signals_demean(:,j),'normalized');
        [max_val,pos] = max(abs(xcorrel));
        matrice_lags(i,j) = lags(pos);
        matrice_xcorr_max(i,j) = max_val;
        matrice_xcorr_zero(i,j) = xcorrel(nr);
    end
end

% Visualizzare le tre matrici in un'unica figura, utilizzando il range 0-1 
% per i valori e una colormap jet(imagesc) 

figure(3)
subplot(131)
imagesc(matrice_corr,[0 1]),colormap jet, axis square, axis off
title ('Matrice Correlazione')
subplot(132)
imagesc(matrice_xcorr_max,[0 1]),colormap jet, axis square, axis off
title ('Matrice Cross-Correlazione (Max)')
subplot(133)
imagesc(matrice_xcorr_zero,[0 1]),colormap jet, axis square, axis off
title ('Matrice Cross-Correlazione (Zero)')

% Visualizzare la matrice di lag (imagesc)
figure(4)
imagesc(matrice_lags),colormap jet, colorbar, axis square, axis off
title ('Lags')

%% Esempio 3 - Cross-correlazione 2D
% Controllate rispetto a quanto visto in teoria se tornano i conti: 
 
X1 =[1     2     5     2     5;
     3     4     5     4     2;
     5     2     3     2     2;
     2     3     2     4     2;
     3     4     2     2     3];
     
     
X2 =[3     3     2;
     2     3     4;
     4     5     4];
 
Corr = xcorr2(X1,X2);
 
% Per fare prove ulteriori, si decommenti il codice qui sotto; 
%X1 = round(unifrnd(1,5,5,5));
%X2 = round(unifrnd(1,5,3,3));
%Corr = xcorr2(X1,X2)

%% Esercizio 5: Implementazione manuale della cross-correlazione 2D 
[m,n] = size(X1); 
[r,c] = size(X2);

method = 'xcorr: X1 vs X2';
 
switch method
    
    case 'xcorr: X1 vs X2'
        Temp = [zeros(r-1,n);X1;zeros(r-1,n)];
        X1_padded = [zeros(size(Temp,1),c-1) Temp zeros(size(Temp,1),c-1)];
        [m_padded, n_padded] = size(X1_padded);
        
        for i = 1:(m_padded-(r-1))
            for j = 1:(n_padded-(c-1))
                matrice_xcorr(i,j) = sum(X1_padded(i:i+r-1,j:j+c-1).*X2,'all');  
            end
        end
        
    case 'xcorr: X2 vs X1'
        Temp = [zeros(m-1,c);X2;zeros(m-1,c)];
        X2_padded = [zeros(size(Temp,1),n-1) Temp zeros(size(Temp,1),n-1)];
        [r_padded, c_padded] = size(X2_padded);
        
        for i = 1:(r_padded-(m-1))
            for j = 1:(c_padded-(n-1))
                matrice_xcorr(i,j) = sum(X2_padded(i:i+m-1,j:j+n-1).*X1,'all');
            end
        end
end
% check con xcorr2
mc = xcorr2(X1,X2);
sum(abs(matrice_xcorr-mc),'all') 
 
%% Esempio 4 - Matching 2D
template = 0.2*ones(11);
template(6,3:9) = 0.6;
template(3:9,6) = 0.6;
offsetTemplate = 0.2*ones(22);
offset = [8 6];
offsetTemplate((1:size(template,1))+offset(1), ...
    (1:size(template,2))+offset(2)) = template;

figure; set(gcf,'name','Cross correlazione 2D','IntegerHandle','off');
subplot(211); imagesc(template); title('template originale');
colormap gray
axis equal
subplot(212); imagesc(offsetTemplate); title('template shiftato');
axis equal

figure; set(gcf,'name','Template: Orig e Shiftato','IntegerHandle','off');
imagesc(offsetTemplate)
colormap gray
hold on
imagesc(template)
axis equal

cc = xcorr2(offsetTemplate,template);
figure; imagesc(cc);
set(gcf,'name','Cross correlazione 2D applicazione','IntegerHandle','off');

[max_cc, imax] = max(abs(cc(:)));
[ypeak, xpeak] = ind2sub(size(cc),imax(1));

corr_offset = [(ypeak-size(template,1)) (xpeak-size(template,2))];

% % Posizione del massimo
% pos_temp = [(ypeak-size(template,1) +1) (xpeak-size(template,2) +1)];
% % L'offset è la posizione del massimo -1 
% % Se il massimo è in posizione 1 non c'è offset
% corr_offset = pos_temp -1

isequal(corr_offset,offset)




%% Esercizio extra: provare la cross-correlazione 2D con differenti segnali definiti a mano e diversi shift










