% Corso di Elaborazione dei Segnali e Immagini
% Docente: Manuele Bicego
% Docente Coordinatore: Marco Cristani
% LAB2 - parte 1

clear all; 
close all; 
clc; 

%% Esercizio 5: Implementazione manuale della cross-correlazione 2D 

 
X1 =[1     2     5     2     5;
     3     4     5     4     2;
     5     2     3     2     2;
     2     3     2     4     2;
     3     4     2     2     3];
     
     
X2 =[3     3     2;
     2     3     4;
     4     5     4];

[m,n] = size(X1); 
[r,c] = size(X2);

method = 'xcorr: X1 vs X2';
 
switch method
    
    case 'xcorr: X1 vs X2'
        Temp = [zeros(r-1,n);X1;zeros(r-1,n)];
        X1_padded = [zeros(size(Temp,1),c-1) Temp zeros(size(Temp,1),c-1)];
        [m_padded, n_padded] = size(X1_padded);
        
        for i = 1:(m_padded-(r-1))
            for j = 1:(n_padded-(c-1))
                matrice_xcorr(i,j) = sum(X1_padded(i:i+r-1,j:j+c-1).*X2,'all');  
            end
        end
        
    case 'xcorr: X2 vs X1'
        Temp = [zeros(m-1,c);X2;zeros(m-1,c)];
        X2_padded = [zeros(size(Temp,1),n-1) Temp zeros(size(Temp,1),n-1)];
        [r_padded, c_padded] = size(X2_padded);
        
        for i = 1:(r_padded-(m-1))
            for j = 1:(c_padded-(n-1))
                matrice_xcorr(i,j) = sum(X2_padded(i:i+m-1,j:j+n-1).*X1,'all');
            end
        end
end

