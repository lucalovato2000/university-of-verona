% Corso di Elaborazione dei Segnali e Immagini
% Docente: Manuele Bicego
% Docente Coordinatore: Marco Cristani
% LAB2 - parte 1

clear all; 
close all; 
clc; 

%% Esercizio 1: provare a capire e completare l'animazione descritta nel codice sotto per la cross-correlazione

f1 = [1 1 1 1 1 1 1 1]; %box 
f2 = [1 2 3 4 5 6 7 8]; %triangolo

%f1 = [0.1 0.2  -0.1 4.1 -2 1.5 0 ];
%f2 = [0.1 4 -2.2 1.6 0.1 0.1 0.2];

M = length(f1);
N = length(f2);

% Faccio zero padding: operazione per rendere uguali le dimensioni dei vettori (segnali) in ingresso
if N>M 
    f1 = cat(2,f1,zeros(1,N-M)); % concatena due vettori: cat(DIM,A,B)
    M=N;
elseif N<M
    f2 = cat(2,f2,zeros(1,M-N));
end

figure; set(gcf,'name','Cross Correlazione animazione','IntegerHandle','off');
subplot(511); stem(f1); title('f1')
subplot(512); stem(f2); title('f2')

% Allineo i due segnali 
tf1 = [zeros(1,M-1),f1,zeros(1,M-1)];
tf2 = [f2,zeros(1,2*M-2)];

lag = [-M+1:M-1];
MYf1xf2 = [];
for i=1:2*M-1
    subplot(513); stem(tf1); title('f1 allineato')
    subplot(514); stem(tf2); title('f2 allineato')
    MYf1xf2 = [MYf1xf2 sum(tf1.*tf2)];
    tf2 = circshift(tf2,1,2);
    subplot(515); stem(lag(1:i),MYf1xf2); xlim([-M+1 M-1]);
    pause(1);
end
hold on; subplot(515); plot(lag(1:i),MYf1xf2); 

