% Corso di Elaborazione dei Segnali e Immagini
% Docente: Manuele Bicego
% Docente Coordinatore: Marco Cristani
% LAB2 - parte 1

clear all; 
close all; 
clc; 

    
%% Esercizio 3: Cross-correlazione su segnali audio: riconiscemento del suono
% Attraverso la cross-correlazione confrontate l'esempio di test con le varie canzoni della galleria 
% e verificate a quale essa appartenga.

[Y1,fs1] = audioread('funky.mp3',[1,96000*20]);
[Y2,fs2] = audioread('lost.mp3',[1,96000*20]);
[Y3,fs3] = audioread('Diana.mp3',[1,96000*20]);
[Y4,fs4] = audioread('never.mp3',[1,96000*20]);
[Y5,fs5] = audioread('T69.mp3',[1,96000*20]);

sound(Y4(1:96000*3,1),fs4)

figure; set(gcf,'name','Dataset canzoni','IntegerHandle','off');
subplot(2,3,1); plot(Y1(1:96000*1,1));
subplot(2,3,2); plot(Y2(1:96000*1,1));
subplot(2,3,3); plot(Y3(1:96000*1,1));
subplot(2,3,4); plot(Y4(1:96000*1,1));
subplot(2,3,5); plot(Y5(1:96000*1,1));

%%Array di celle: un metodo piu'veloce per raccogliere sequenze di lunghezza diversa.

gallery{1}=Y1(:,1);
gallery{2}=Y2(:,1);
gallery{3}=Y3(:,1);
gallery{4}=Y4(:,1);
gallery{5}=Y5(:,1);

test=Y2(96000*2:96000*7,:);

for g=1:5
    [xc{g},lagc{g}]= xcorr(gallery{g},test(:,1));
end

figure; set(gcf,'name','Risultati di matching','IntegerHandle','off');

for g=1:5
    subplot(2,3,g); plot(lagc{g},xc{g});
end

[maxcorr,maxli]=max(xc{2});

sound(Y2(lagc{2}(maxli)+1:lagc{2}(maxli)+480001,:),fs2);
sound(test,fs2);

figure
plot(test(:,1),'-r','LineWidth',2);
hold on 
plot(Y2(lagc{2}(maxli)+1:lagc{2}(maxli)+480001,1),'-k','LineWidth',1);






