% Corso di Elaborazione dei Segnali e Immagini
% Docente: Manuele Bicego
% Docente Coordinatore: Marco Cristani

%%%% PARTE 1

clear all 
close all 
clc 


%% Esercizio 1 -  SINUSOIDE A 20 Hz 1-D CAMPIONATA A 100 Hz 
% Obiettivo di questo esercizio � quello di avere la possibilit� di
% generare segnali e controllarne lo spettro. In particolare,
% definite una sinusoide a 20 Hz, campionatela a 100Hz e osservarne lo spettro di 
% magnitudo con picco a 20 Hz. Controllatene anche lo spettro di fase.

% FUNZIONI RICHIESTE: fft, fftshift, abs, angle

% Definisco le principali variabili di interesse e il segnale 
mu = 20;        % frequenza del segnale sinusoidale
mu_s = 100;     % frequenza di campionamento 
Dt = 1/mu_s;    % delta T visto a lezione
t = 0:Dt:1-Dt;  % prendo un secondo di durata
N = length(t);  % numero di campioni

f = sin(2*pi*mu*t); % segnale sinusoidale

figure
subplot(221)
plot(t,f,'-b.','MarkerSize',9)
xlabel ('tempo (sec.)')
ylabel ('f(t)')
title('Segnale campionato');

% Calcolo la trasformata di Fourier e definisco il vettore delle frequenze
F = fft(f);           % Fast Fourier Transform � l'implementazione della DFT
mu_sampling = mu_s/N; % passo
mu = 0:mu_sampling:mu_s-mu_sampling; % campioni nello spazio delle frequenze

subplot(222)
stem(mu,abs(F));
xlabel ('frequenza (Hz)')
ylabel ('|F|')
grid
title('DFT (abs) senza riordinamento');

% Eseguo operazione di centratura dello spettro e visualizzo spettro centrato
Fs = fftshift(F); % centratura 
mu_max = mu_s/2;  % frequenza di Nyquist ? 
mu = -mu_max:mu_sampling:mu_max-mu_sampling; % nuovo vettore frequenze 

subplot(223)
stem(mu,abs(Fs))
xlabel ('frequenza (Hz)')
ylabel ('|Fs|')
grid
title('DFT (abs) con riordinamento');

% Eseguo pulizia per eliminare le componenti con magnitudo bassa e calcolo la fase 
th = 1e-6;
Fs(abs(Fs) < th) = 0; 

subplot(224)
stem(mu,angle(Fs)/pi) % Posso moltiplicare per 180 per avere gradi 
xlabel ('frequenza (Hz)')
ylabel ('fase/\pi')
grid
title('DFT (fase) con riordinamento');

%%%%%%%%%%%%%%%%%%%%%%%
% Sottoesercizio - Provare lo stesso esercizio con il seguente segnale: f = cos(2*pi*10*t) - sin(2*pi*40*t)

f = cos(2*pi*10*t) - sin(2*pi*40*t); % segnale sinusoidale

figure
subplot(221)
plot(t,f,'-b.','MarkerSize',9)
xlabel ('tempo (sec.)')
ylabel ('f(t)')
title('Segnale campionato');

% Calcolo la trasformata di Fourier e definisco il vettore delle frequenze
F = fft(f);           % Fast Fourier Transform � l'implementazione della DFT
mu_sampling = mu_s/N; % passo
mu = 0:mu_sampling:mu_s-mu_sampling; % campioni nello spazio delle frequenze

subplot(222)
stem(mu,abs(F));
xlabel ('frequenza (Hz)')
ylabel ('|F|')
grid
title('DFT (abs) senza riordinamento');

% Eseguo operazione di centratura dello spettro e visualizzo spettro centrato
Fs = fftshift(F); % centratura 
mu_max = mu_s/2;  % frequenza di Nyquist ? 
mu = -mu_max:mu_sampling:mu_max-mu_sampling; % nuovo vettore frequenze 

subplot(223)
stem(mu,abs(Fs))
xlabel ('frequenza (Hz)')
ylabel ('|Fs|')
grid
title('DFT (abs) con riordinamento');

% Eseguo pulizia per eliminare le componenti con magnitudo bassa e calcolo la fase 
th = 1e-6;
Fs(abs(Fs) < th) = 0; 

subplot(224)
stem(mu,angle(Fs)/pi) % Posso moltiplicare per 180 per avere gradi 
xlabel ('frequenza (Hz)')
ylabel ('fase/\pi')
grid
title('DFT (fase) con riordinamento');


%%%%%%%% RIASSUNTO:
% Passi:
% calcolo la fft e la fft riordinata
% calcolo il vettore frequenze: 
%       fft riordinata: (N valori da -mu_s/2 a mu_s/2-step)
%       fft non riordinata: (N valori da 0 a mu_s-step)
%       step = mu_s/N
% visualizzo lo spettro di ampiezza
% Se segnale rumoroso: pulisco lo spettro e tolgo le frequenze troppo basse
% visualizzo lo spettro di fase
%%%%%%%%%%%%%%%%%%%



%% Esercizio 2 -  SEGNALE RETTANGOLO e FFT 
% Obiettivo dell'esercizio e' quello di verificare lo spettro di
% ampiezza/spettro di fase per un segnale rettangolare. Utilizzando la
% funzione "rectpuls" creare un'onda rettangolare di 1 secondo con una
% frequenza di campionamento di 500 Hz e una lunghezza di 0.2 s 

mu_s = 500; % Frequenza di campionamento 
T = 0.2;    % lunghezza del rect in secondi 

time = -0.5-1/mu_s:1/mu_s:0.5; % Vettore dei tempi per il segnale rect 
x = rectpuls(time,T); % Genero l'onda rettangolare
% Guardare il doc di rectpuls

figure(1) 
plot(time,x,'k');
title(['Rectangular Pulse ampiezza =', num2str(T),'s']);
xlabel('tempo(s)');
ylabel('ampiezza');


% Passi:
% calcolo il vettore frequenze (N valori da -mu_s/2 a mu_s/2)
% calcolo la fft e la fft riordinata
% visualizzo lo spettro di ampiezza
% visualizzo lo spettro di fase


N = length(x); % N campioni, N freqs nella fft
mu_sampling = mu_s/N; % una ogni mu_s/N 
mu_max = mu_s/2; % met� a sx e met� a dx dello zero

% vettore delle frequenze: N valori da -mu_s/2 a mu_s/2 
mu = -mu_max:mu_sampling:mu_max-mu_sampling; 

X = fft(x); 

% Eseguo operazione di centratura dello spettro 
% e calcolo spettro di ampiezza
shifted = fftshift((X));
X1 = abs(shifted); 

figure(2)
plot(mu,X1,'r');
title('Spettro di Magnitudo');
xlabel('frequenza (Hz)')
ylabel('magnitudo |X(f)|');

figure(3)
plot(mu,180*angle(X)/pi,'r');
title('Spettro di Fase');
xlabel('frequenze (Hz)')
ylabel('fase/\pi');





%% Esercizio 3 - SEGNALE REGISTRATO E FFT
% Obiettivo dell'esercizio e' quello di registrare un segnale audio
% direttamente in MATLAB, della durata di 4 secondi, e visualizzare poi lo
% spettro di ampiezza. 

% Registro segnale audio
recObj = audiorecorder;
disp('Start')
recordblocking(recObj,4);
disp ('End')

play(recObj)
myVoice = getaudiodata(recObj);

% in recObj posso recuperare la frequenza di campionamento
% e il numero di campioni
mu_s = recObj.SampleRate; % Frequenza di campionamento 
N = recObj.TotalSamples; % Numero di campioni 

% Passi:
% calcolo il vettore frequenze (N valori da -mu_s/2 a mu_s/2)
% calcolo la fft e la fft riordinata
% visualizzo lo spettro di ampiezza
% pulisco lo spettro e tolgo le frequenze troppo basse
% visualizzo lo spettro di fase

mu_m = mu_s/2;
freq = -mu_m:mu_s/N:mu_m-mu_s/N; % vettore frequenze
F = fft(myVoice);
FF = fftshift(F);

figure(1)
subplot(131)
plot([1:N],myVoice);
xlabel('#campioni')
ylabel ('ampiezza')
title ('Segnale registrato') 

subplot(132)
plot(freq,(abs(FF)));
xlabel('frequenze (Hz)')
ylabel ('magnitudo')
title ('DFT (abs) del segnale ') 

% Per la fase: ho un risultato molto rumoroso, quindi devo applicare un filtraggio restrittivo 
th = max(abs(FF))/4; % decido di pulire lo spettro cancellando i residui di frequenza sotto un quarto del massimo valore
FF(abs(FF) < th) = 0; 
theta = angle(FF);

subplot(133);
stem(freq,180*(theta)/pi)
xlabel ('frequenza (Hz)')
ylabel ('fase/\pi')
grid
title('Spettro di fase');

%% Esercizio 4 -  COSENO CON SFASAMENTO E OPERAZIONE DI PADDING 
% Obiettivo di questo esercizio � quello di testare un segnale coseno con sfasamento. In particolare,
% utilizzare una frequenza di 5 Hz, campionare il segnale a 150 Hz e osservare lo spettro di 
% ampiezza/spettro di fase. Eseguire poi un'operazione di padding. 

% Definisco le principali variabili
mu_s = 150; 
mu = 5;
Dt = 1/mu_s;
t = 0:Dt:1-Dt;
phs = 1/3*pi; 

signal = cos(2*pi*mu*t + phs);

figure
subplot(131)
plot(t,signal);
xlabel ('tempo (sec.)')
ylabel ('f(t)')
title('Segnale campionato');

X = fft(signal);
N = length(X);
mu_m = mu_s/2;
freq = -mu_m:mu_s/N:mu_m-mu_s/N;
FF = fftshift(X);


subplot(132)
plot(freq,(abs(FF)));
xlabel ('frequenza (Hz)')
ylabel ('|Fs|')
grid
title('DFT (abs) con riordinamento');

% Eseguo operazione di zero padding al segnale nel tempo. 
% Questa e' applicata automaticamente in Matlab con il comando X = fft(x,N) 
% quando la dimensione N e' maggiore della lunghezza del segnale x 
Npad = 1024;
X_padded = fft(signal,Npad);
FF_padded = fftshift(X_padded);
freq_padded = -mu_m:mu_s/Npad:mu_m-mu_s/Npad; 

subplot(133)
plot(freq_padded,(abs(FF_padded)));
xlabel ('frequenza (Hz)')
ylabel ('|Fs|')
grid
title('DFT (abs) con riordinamento - Padding ');



%% Esercizio 5 - ANALISI FFT SU SEGNALE REALE 
% Analizzare lo spettro del suono contenuto nel file Unknown_Sound attraverso la DFT. 
% Che tipo di suono abbiamo?
% Visualizzare sia lo spettro di ampiezza che di fase
close all
clear all
clc

load Unknown_Sound % contiene [f,fs] dove f = suono, fs = freq. di campionamento

% Questa situazione (ho un segnale che non conosco in partenza e lo devo
% analizzare) � particolarmente importante in un recentissimo ambito,
% quello della manutenzione predittiva. In pratica, l'idea � di ascoltare
% un sistema (p.e. un motore di aereo) a capire dal suono quando si stanno
% per verificare problemi, facendo partire la manutenzione prima che
% avvenga il guasto. La manutenzione predittiva � uno dei cardini dell'industria 4.0. 
% Vedasi https://www.processingmagazine.com/predictive-maintenance-airborne-sound-analysis/ 

% a) Valutare l'andamento generale del segnale e la sua durata

figure; set(gcf,'name','Analisi preliminare segnale');
subplot(211)
plot(f); % utilizzo lo strumento di zoom per analizzare una porzione di segnale;
xlim([0 length(f)])
xlabel ('#campioni')
ylabel ('ampiezza')
title('Segnale audio completo');

subplot(212)
plot(f(1:200)); 
xlabel ('#campioni')
ylabel ('ampiezza')
title('Segnale audio - 200 campioni');

N = length(f); 
durata = N/fs; % N = fs * T; 
sprintf('Durata del segnale audio = %f', durata)

% b) Eseguire l'analisi di Fourier e visualizzare gli spettri 

t = 0:1/fs:1-1/fs; % Prendo un secondo (segnale con andamento regolare nel tempo, ho piu' di 44100 campioni) 
f = (f(1:fs,1)); % In un secondo ho fs campioni
% trasformata di fourier
F = fft(f);

% calcolo vettore frequenze
mu_sampling = fs/length(f); 
mu_max = fs/2;
mu = 0:mu_sampling:fs-mu_sampling; 
mu_shift = -mu_max:mu_sampling:mu_max-mu_sampling;

figure; set(gcf,'name','Analisi spettrale segnale 1D');

subplot(231); 
plot(t,f);
xlabel ('tempo (sec.)')
ylabel ('f(t)')
title ('Segnale - 1 sec')

subplot(232); 
plot(t(1:2000),f(1:2000));
xlabel ('tempo (sec.)')
ylabel ('f(t)')
title ('Segnale ~ 0.05 sec')

subplot(233); 
stem(mu, abs(F));
xlabel ('frequenza (Hz)')
ylabel ('|F|')
title('La DFT come vista a lezione');

% shifto e visualizzo
FF = fftshift(F);
subplot(234); 
stem(mu_shift, abs(FF))
xlabel ('frequenza (Hz)')
ylabel ('|F|')
title('La DFT centrata');

th = max(abs(FF))/100; % decido di pulire lo spettro cancellando i residui di frequenza sotto un 100esimo del massimo valore
FF(abs(FF) < th) = 0; 
theta = angle(FF);

subplot(235);
stem(mu_shift,theta/pi)
xlabel ('frequenza (Hz)')
ylabel ('fase/\pi')
grid
title('Spettro di fase');



