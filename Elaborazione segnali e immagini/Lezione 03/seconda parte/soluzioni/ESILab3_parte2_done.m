% Corso di Elaborazione dei Segnali e Immagini
% Docente: Manuele Bicego
% Docente Coordinatore: Marco Cristani

% PARTE 2

clear all 
close all 
clc 

%% Esercizio 6 - FENOMENO DI ALIASING, SEGNALE SINTETICO 
% Questo esempio ha lo scopo di illustrare il fenomeno di
% aliasing, che si verifica quando il teorema del campionamento non e'
% rispettato 

Fc_vector = [200,100,40,25,14]; 

for i = 1:5
    Fs = Fc_vector(i);        % frequenza di campionamento
    Dt = 1/Fs;                % deltaT, passo
    t = 0:Dt:1-Dt;            % prendo un secondo di durata
    N = length(t);            % numero di campioni
    fsig = 10;                % frequenza sinusoide
    y = sin(2*pi*fsig*t);     % segnale 
    
    Y = fft(y);                  
    mu_sampling = Fs/N;
    mu = -Fs/2:mu_sampling:Fs/2-mu_sampling; 
    YY = fftshift(Y);
    
    
    figure(1)
    subplot(211)
    stem(t,y,'filled')
    hold on;
    plot(t,y)
    title(['Segnale campionato con Fs = ',num2str(Fs)])
    

    subplot(212)
    stem(mu,abs(YY),'filled')
    title('DFT del segnale')
    xlabel('frequenze (Hz)')
    ylabel('magnitudo')
      pause
    close    
end

%% Esercizio 7 - Aliasing 
% Capite il codice qui sotto, a parte le prime 4 righe. L'obiettivo dell'esercizio � quello di capire 
% fino a che fattore di sottocampionamento si puo' evitare aliasing. 
sil = [0 0.25 0.5 0.75 7/8 1.0];
A = [1 0 0 0 0 0];
order = 799;
f1 = fir2(order,sil,A);
%Qui sopra, non deve interessare; importante, abbiamo un segnale in banda limitata 

% Fattore di sottocampionamento: provate a cambiarlo: cosa succede allo
% spettro di ampiezza?

D = 2; %2,4,5
% Frequenza di campionamento
mu_s1 = 800; %Hz
mu_s2 = mu_s1/D;
% Tempo di campionamento
DT1 = 1/mu_s1;
DT2 = 1/mu_s2;
t1 = 0:DT1:1-1/mu_s1;
t2 = 0:DT2:1-1/mu_s2;
f2 = f1(1:D:end);

figure;
subplot(221)
stem(t1,f1); hold on; plot(t1,f1,'r'); grid on; hold off;
xlabel 'tempo (sec)'
ylabel 'f(t)'
title('segnale originale campionato >Nyquist')

subplot(222)
stem(t2,f2); hold on; plot(t2,f2,'r'); grid on; hold off;
xlabel 'tempo (sec)'
ylabel 'f(t)'
title('segnale originale sottocampionato')

%FFT
F1=abs(fft(f1));
FF1 = fftshift(F1);
step = mu_s1/length(f1);
freq1 = -mu_s1/2:step:mu_s1/2-step;

F2=abs(fft(f2));
FF2 = fftshift(F2);
step = mu_s2/length(f2);
freq2 = -mu_s2/2:step:mu_s2/2-step;

%
subplot(223)
stem(freq1,FF1); grid on;
xlabel 'frequenza (Hz)'
ylabel '|F|'
title('spettro segnale campionato originale')
%
subplot(224)
stem(freq2,FF2,'r'); grid on; hold on
xlabel 'frequenza (Hz)'
ylabel '|F|'
title('spettro segnale sottocampionato')


%% Esercizio 8 -  ALIASING SU SEGNALE REALE
% Analizzare il segnale in voice.m4a.
% Operate sottocampionamento, sino ad avvertire aliasing a livello sonoro
[f,fs] = audioread('Voice.m4a');

% Definisco i parametri fondamentali 
N = length(f);
Dt = 1/fs; 
T = N*Dt;
t = (0:Dt:T-1/fs);

figure; 
subplot(221);
set(gcf,'name','Analisi preliminare segnale');
plot(t,f); 
grid
xlabel ('Tempo (sec)')
ylabel ('ampiezza')
xlim([0 T])

% Calcolo la trasformata di Fourier 
F = fft(f);
mu_sampling = fs/N;
mu = 0:mu_sampling:fs-mu_sampling; % campioni nello spazio delle frequenze

% Visualizzo lo spettro di ampiezza non centrato
subplot(222)
plot(mu./10^3,abs(F))
xlabel ('Frequenza (kHz)')
ylabel ('|y|')
grid

% Visualizzo lo spettro di ampiezza centrato
mu_max = fs/2; 
mu = -mu_max:mu_sampling:mu_max-mu_sampling; 

subplot(223)
plot(mu'./10^3,fftshift(abs(F)))
xlabel 'Frequenza (kHz)'
ylabel '|y|'
grid

% Operazione di sottocampionamento (hint: prendo un sottoinsieme di punti,
% scartando gli altri e definendo un fattore di downsampling). Visualizzo il nuovo spettro
% di ampiezza e ascolto il segnale sottocampionato 

downsampling = 2; % Posso definire un array con i vari valori di sottocampionamento e poi testarli 
fsub = f(1:downsampling:end,:);

% Calcolare e visualizzare il nuovo spettro
% di ampiezza centrato
% Suggerimento: utilizzare la fft con zero padding
% per avere comunque N bins: fft(fsub,N)

Fsub = fft(fsub,N);
subplot(224)
plot(mu'./10^3,fftshift(abs(Fsub)))
xlabel ('Frequenza (kHz)')
ylabel ('|y|')
grid

% ascolto il segnale sottocampionato 
Freqsub = fs/downsampling;
sound(fsub,Freqsub)

% DOMANDA: fino a che livello di sottocampionamento non ho aliasing?

%% Esercizio 9 - FILTRAGGIO DI UN SEGNALE AUDIO UTILIZZANDO LA CONVOLUZIONE 
% Obiettivo dell'esercizio e' quello di caricare una traccia audio, per
% esempio il file "Voice.m4a", e visualizzare lo spettro delle ampiezze
% shiftato. Una volta progettato un filtro passa-basso o passa-alto (non ci
% soffermeremo sui dettagli dell'implementazione) filtrare il segnale di partenza 
% utilizzando la convoluzione. Visualizzare lo spettro del segnale filtrato e ascoltare
% il suono pulito 

% Segnale audio da filtrare
[x,Fs]=audioread('Voice.m4a'); 

T = 1;      % considero una porzione di segnale pari ad 1 sec di
N = Fs*T;   % durata del segnale in campioni
x = x(1:N); % prendo i primi N campioni del segnale (48000x1)
Dt = 1/Fs;  % passo 
t = 0:Dt:T-Dt; % vettore dei tempi 

figure(1)
plot(t,x)
xlabel('tempo (sec)')
ylabel ('ampiezza')
title('Segnale audio')

X = fft(x);
Fmax = Fs/2;
Fsampl = Fs/N;
f = -Fmax:Fsampl:Fmax-Fsampl;

figure(2)
plot(f,abs(fftshift(X)))
xlabel('frequenza (Hz)')
ylabel ('magnitudo')
title('DFT (abs) con ordinamento')

% Progetto del filtro passa-basso (da prendere come noto) 
f_cut = 1/20; 
hPB = fir1(32,f_cut,'low'); % dimensione = 1x33
HPB = fft(hPB,1024); % zero-padding 

figure(3)
subplot(211)
plot(hPB)
xlabel('campioni')
title('Filtro Passa-Basso')
subplot(212)
plot(linspace(-Fs/2,Fs/2,1024),abs(fftshift((HPB))))
xlabel('f [Hz]')
title('Filtro Passa-Basso in frequenza')

% Filtraggio con operazione di convoluzione 
xPB = conv(x,hPB); % dimensione = 48032x1

figure(1)
hold on
plot(t,xPB(33:length(xPB)),'r')
xlabel('tempo (sec)')
title('Segnale filtrato')
hold off

figure(2)
XPB = fft(xPB(33:length(xPB)));
hold on
plot(f./10^3,abs(fftshift(XPB)),'r')
xlabel('frequenze (kHz)')
title('DFT (abs) del segnale filtrato')
hold off

% Ascolto del segnale prima e dopo il filtraggio
disp('Click per sentire il segnale originale:')
pause
soundsc(x,Fs)

disp('Click per sentire il segnale filtrato:')
pause
soundsc(xPB(33:length(xPB)),Fs)






