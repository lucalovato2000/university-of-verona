% Corso di Elaborazione dei Segnali e Immagini
% Docente: Manuele Bicego
% Docente coordinatore: Marco Cristani

%% Alcuni comandi utili disponibili nell'Image Processing Toolbox 
% imnoise(I, 'gaussian',m) -> aggiunge rumore gaussiano con media m e
% varianza pari a 0.01 (puo' essere cambiata)
% 
% normrnd(mu,sigma,sz1,...,szN) -> genero un vettore di numeri random a partire da una
% distribuzione gaussiana con media mu e standard deviation sigma, avente
% dimensione data da sz1, sz2 etc 
%
% fspecial(type) -> crea un filtro H 2D di uno specifico tipo (e.g.
% 'average', 'gaussian', etc). Nota = fspecial ritorna un "correlation
% kernel", che puo' essere utilizzato insieme al comando imfilter
%
% imfilter(I,H,'replicate') -> filtra la matrice I con il filtro H. Di default il
% filtraggio e' eseguito con la correlazione, ma si puo' scegliere anche
% convoluzione. Replicate e' una delle opzioni piu' usate per la gestione
% dei bordi (padding). 
%
% medfilt2 (I,[m n]) -> esegue un filtraggio mediano in due dimensioni. Se
% non si specificano le dimensioni m ed n, di default viene scelto un
% intorno attorno al pixel di 3x3. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ESEMPI: creazione e applicazione di diversi filtri di smoothing 
% e calcolo SNR
%
% Definisco l'immagine simulata I a cui poi aggiungo rumore gaussiano

clear all
close all
clc

I = ones(128,128).*128;
J = I +normrnd(0,2,128,128);    % creo rumore gaussiano
figure; set(gcf,'name','Studio sintetico rumore e filtraggio');
subplot(211); imagesc(I), colormap gray, axis image; title('Originale')
colorbar
subplot(212); imagesc(J), colormap gray, axis image; title('Noisy')
colorbar

% Calcolo il SNR_{MSE} e SNR_{VAR} nell'immagine di partenza
% f = I (immagine senza rumore)
% ftilde = J (immagine con rumore)
I = double(I);
J = double(J); 
num = sum(J(:).^2);
den = sum((J(:)-I(:)).^2);
SNR1mse = num/den;
SNR1var = var(I(:))/var(J(:));
fprintf('SNR img noisy (Gauss)\nMSE: %g\nVAR: %g\n',SNR1mse,SNR1var);

%% Esempio 1: Filtro Media
% Per creare un filtro locale media: fspecial 
% si veda doc fspecial
% Creo un filtro media 3x3
H = fspecial('average',3); 
% Per applicare il filtro all'immagine: imfilter
Km = imfilter(J,H,'replicate');
% 'replicate' è un'opzione per gestire il "problema dei bordi"
% Visualizzo il risultato
figure, set(gcf,'name','Immagine dopo Smoothing Media'); imshow(uint8(Km)); colorbar 
% Calcolo SNR
SNRMmse = sum(Km(:).^2)/sum((Km(:)-I(:)).^2);
SNRMvar = var(I(:))/var(Km(:));
fprintf('SNR img noisy + Mean smoothing \nMSE: %g\nVAR: %g\n',SNRMmse,SNRMvar);


%% Esempio 2: Filtro Gaussiano 
% Per creare un filtro locale gaussiano: fspecial
% filtro gaussiano KxK con standard deviation 0.6
H = fspecial('gaussian',3,0.6);
% Importante: relazione tra K (W nella teoria) e sigma:
% W = 5*sigma
Kg = imfilter(J,H,'replicate');
figure, set(gcf,'name','Smoothing Gaussiano'); imshow(uint8(Kg)); colorbar
SNRGmse = sum(Kg(:).^2)/sum((Kg(:)-I(:)).^2);
SNRGvar = var(I(:))/var(Kg(:));
fprintf('SNR img noisy + Gaussian smoothing \nMSE: %g\nVAR: %g\n',SNRGmse,SNRGvar);


%% Esempio 3: Filtro Mediano
% In questo esempio si vedra' come e' possibile eliminare il rumore
% impulsivo utilizzando un filtro di smoothing mediano.
clear all
close all
clc

load imdemos 
I = saturn2;
% aggiungo rumore sale e pepe all'immagine originale 
J = imnoise(I,'salt & pepper',0.02);
figure; set(gcf,'name','Studio sintetico rumore e filtraggio');
subplot(211); imagesc(I), colormap gray, axis image; title('Originale')
colorbar
subplot(212); imagesc(J), colormap gray, axis image; title('Noisy')
colorbar
I = double(I);
J = double(J); 
SNR1mse = sum(J(:).^2)/sum((J(:)-I(:)).^2);
SNR1var = var(I(:))/var(J(:));
fprintf('SNR img noisy (Salt-pepper)\nMSE: %g\nVAR: %g\n\n',SNR1mse,SNR1var);


% per applicare il filtro mediano si usa
% la funzione medfilt2
Kmed = medfilt2(J);
% senza parametri si applica un filtro 3x3
figure, set(gcf,'name','Smoothing Mediano'); imshow(uint8(Kmed)); colorbar

SNRMedmse = sum(Kmed(:).^2)/sum((Kmed(:)-I(:)).^2);
SNRMedvar = var(I(:))/var(Kmed(:));
fprintf('SNR img noisy (Salt-pepper) + Median smoothing\nMSE: %g\nVAR: %g\n\n',SNRMedmse,SNRMedvar);



%% Esempio 4 -  Filtraggio Locale: Sharpening ---
% Esempio di applicazione di sharpening. 

clear all; close all; clc 

A = imread('moon.tif');
figure; imshow(A); title ('Immagine partenza')

alpha = 0.2; %valore di default
H = fspecial('laplacian',alpha);
B = imfilter(A,H);
figure; imshow(uint8(B)); title (['Filtraggio sharpening con Laplaciano Alpha = ', num2str(alpha)])

% Basic highpass spatial filtering
% L'immagine finale è l'immagine originale
% sommata con l'immagine filtrata con il laplaciano
cost = -1; % valore centrale di H è negativo
C = double(A) + cost*double(B);
figure; imshow(uint8(C)); title ('Immagine finale (basic highpass)')


%% ESERCIZIO 1: Smoothing
% Applicare diversi filtri di smoothing ad alcune immagini reali.
% Confrontare i risultati sia in modo qualitativo (quali ritornano 
% un'immagine migliore?) e quantitativo (quali ritornano il valore di SNR 
% più alto?)

% Utilizzare SNR versione VAR

% Filtri da provare:
% media: 3x3, 5x5, 9x9
% gaussiano: 3x3, 5x5, 9x9 (attenzione a stimare correttamente la
%           deviazione standard del filtro con la regola W/5)
% mediano: 3x3, 5x5, 9x9

% decommentare la parte che carica l'immagine su cui si vuol fare l'analisi
% (Eventualmente usare anche altre immagini)

clear all 
close all
clc

% Immagine 1. peacock con rumore gaussiano
I = rgb2gray(imread('peacock.jpg'));
J = imnoise(I,'gaussian',0.01);
figure(1)
subplot(231); imshow(I); title('Immagine originale')
subplot(232); imshow(J); title('Immagine con rumore gaussiano')

% Applico filtro Media e visualizzo l'immagine
mediaFilter = fspecial('average',3); 
imageAfterMediaFilter = imfilter(J, mediaFilter,'replicate');
subplot(233); imshow(imageAfterMediaFilter); title('Immagine dopo aver applicato filtro media'); 

% Applico filtro Gaussiano e visualizzo l'immagine
gaussianFilter = fspecial('gaussian',3,0.6);
imageAfterGaussianFilter = imfilter(J,gaussianFilter,'replicate');
subplot(234); imshow(imageAfterGaussianFilter); title('Immagine dopo aver applicato filtro gaussiano'); 

imageAfterMedianFilter = medfilt2(J);
subplot(235); imshow(imageAfterMedianFilter);  title('Immagine dopo aver applicato filtro mediano'); 

I = double(I); J = double(J); imageAfterMediaFilter = double(imageAfterMediaFilter); imageAfterGaussianFilter = double(imageAfterGaussianFilter); imageAfterMedianFilter = double(imageAfterMedianFilter);

SNRvar = var(I(:))/var(J(:));
SNR1var = var(I(:))/var(imageAfterMediaFilter(:));
SNR2var = var(I(:))/var(imageAfterGaussianFilter(:));
SNR3var = var(I(:))/var(imageAfterMedianFilter(:));

fprintf("SNR IMMAGINE ORIGINALE: %f\n", SNRvar);
fprintf("SNR FILTRO MEDIA: %f\n", SNR1var);
fprintf("SNR FILTRO GAUSSIANO: %f\n", SNR2var);
fprintf("SNR FILTRO MEDIANO: %f\n", SNR3var);




% % Immagine 2. pepper con rumore impulsivo
% load imdemos 
% I = pepper;
% % aggiungo rumore sale e pepe all'immagine originale 
% J = imnoise(I,'salt & pepper',0.05);
% figure, imshow(I); set(gcf,'name','Originale');
% figure, imshow(J); set(gcf,'name','Rumore Gaussiano');





%% ESERCIZIO 2: Sharpening
% Applicare il filtraggio di sharpening all'immagine Pavone dell'esercizio
% precedente. Provare ad applicarlo direttamente all'immagine con rumore
% oppure dopo aver applicato un filtraggio media o gaussiano. 
% Che differenza si nota?

clear all; close all; clc

I = rgb2gray(imread('peacock.jpg'));
J = imnoise(I,'gaussian',0.01);

alpha = 0.2; H = fspecial('laplacian',alpha);
B = imfilter(J,H);

cost = -1; % valore centrale di H è negativo
shaperningWithNoise = double(I) + cost*double(B);

% Applico filtro Media e visualizzo l'immagine
mediaFilter = fspecial('average',3); 
imageAfterMediaFilter = imfilter(J, mediaFilter,'replicate');

alpha = 0.2; T = fspecial('laplacian',alpha);
B2 = imfilter(imageAfterMediaFilter,T);

cost = -1; % valore centrale di H è negativo
shaperningWithNoNoise = double(I) + cost*double(B2);


figure
subplot(231); imshow(I); title("Image original");
subplot(232); imshow(J); title("Image with Gaussian noise");
subplot(233); imshow(uint8(shaperningWithNoise)); title("Sharpening with noise");
subplot(234); imshow(imageAfterMediaFilter); title('Image after media filter'); 
subplot(235); imshow(uint8(shaperningWithNoNoise)); title('Image after media filter & sharpening'); 

I = double(I); shaperningWithNoise = double(shaperningWithNoise); shaperningWithNoNoise = double(shaperningWithNoNoise);

SNRvar = var(I(:))/var(shaperningWithNoise(:));
SNR1var = var(I(:))/var(shaperningWithNoNoise(:));

fprintf("SNR NOISE SHARPENING : %f\n", SNRvar);
fprintf("SNR NO NOISE SHARPENING: %f\n", SNR1var);


%% Esercizio 3 -  Casi di studio reali ---
% a) Trovare piu' informazioni possibili dall'immagine in esame, ed in
% particolare rendere massimamente visibili le informazioni sulla honda, 
% la volkswagen, il camion (targhe in primis) usando tutti gli strumenti
% visti finora.
% Alcune domande a cui rispondere: In che stato ci troviamo? Che targhe 
% hanno le due macchine? 
% Suggerimento: fare un crop della parte da evidenziare e applicare
% streching e sharpening

img = imread('fog.jpg');
img = rgb2gray(img);
figure;
set(gcf,'name','Analisi targhe: Originale');
subplot(121); imshow(img); title('Originale')
subplot(122); imhist(img); title('Istogramma')




%%
% b) Riuscire a migliorare il piu' possibile la qualita' di questa
% scrittura, utilizzando gli strumenti visti fino ad ora. 

img = imread('hand.tiff');
img = rgb2gray(img(:,:,1:3));
figure;
set(gcf,'name','Analisi scrittura a mano: Originale');
subplot(121); imshow(img); title('Originale')
subplot(122); imhist(img); title('Istogramma')






%% Esercizio EXTRA -  Implementazione filtraggio locale
% Implementare il filtraggio locale senza usare medfilt2 per un filtro 
% Mediano 3x3, e applicarla all’immagine butterfly.jpg
% Suggerimenti:
% - inizializzare una nuova immagine della stessa dimensione dell’immagine 
%   originale
% - fare due cicli for per scorrere tutti i pixel
% - per ogni pixel dell’immagine (i,j), estrarre l’intorno e calcolare 
%   il nuovo valore
% - salvare il nuovo valore nella nuova immagine in posizione (i,j)
% 
% Nota Importante: occorre gestire i problemi ai bordi 
% usare l’opzione replicate
% Idea: replico la prima/ultima riga/colonna, calcolo il filtraggio, 
% rimuovo l'informazione in più

clear all
close all
clc

A = imread('butterfly.jpg');
A = rgb2gray(A);
figure; imshow(A); set(gcf,'name','Immagine Partenza');

