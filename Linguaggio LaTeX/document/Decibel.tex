%
% Titolo: Diffusione sonora e Decibel
% Luca Lovato - Matricola: VR443506
%
% Università degli Studi di Verona
% Dipartimento di Informatica
% AA 2020-21
%

%----------------------------------------------------------------------------------------
%	PACCHETTI E CONFIGURAZIONI
%----------------------------------------------------------------------------------------
\documentclass[a4paper,12pt]{article}
\usepackage[top=3.5cm,right=3.75cm,bottom=3.5cm,left=3.75cm]{geometry}
\usepackage[italian]{babel}
\usepackage{graphicx} 	% richiesto per inclusione immagini
\usepackage{amsmath} 	% richiesto per elementi matematici
\usepackage{fancyhdr}
\usepackage{verbatim}
\usepackage{textcomp}
\usepackage[T1]{fontenc}

\pagestyle{fancy}
\setlength{\headheight}{28pt}

%----------------------------------------------------------------------------------------
%	INFORMAZIONI DEL DOCUMENTO
%----------------------------------------------------------------------------------------
\title{Linguaggio \LaTeX{}}
\author{Luca Lovato - Matricola: VR443506} % autore
\date{}	% data odierna

%----------------------------------------------------------------------------------------
%	TITOLO DEL DOCUMENTO
%----------------------------------------------------------------------------------------
\begin{document}
\begin{titlepage}
	\newcommand{\HRule}{\rule{\linewidth}{0.5mm}} % definisci un nuovo comando per le righe orizzontali
									
	\center % centra tutto nella pagina
											
	%------------------------------------------------
	%	INTESTAZIONE
	%------------------------------------------------
											
	%------------------------------------------------
	%	LOGO
	%------------------------------------------------
											
	\includegraphics[width=0.8\textwidth]{immagini/univr_logo.png}
											
	\vspace{3cm}
											
	\textsc{\LARGE Linguaggio LaTeX{}}\\[2cm] 
											
	%------------------------------------------------
	%	TITOLO
	%------------------------------------------------				
	\HRule\\[0.8cm]				
	{\huge\bfseries Diffusione sonora e Decibel}\\[0.4cm] % titolo del documento						
	\HRule\\[0.5cm]
											
	%------------------------------------------------
	%	AUTORE
	%------------------------------------------------				
	\vspace{1cm}
	\begin{minipage}[t]{0.5\textwidth}
		Luca \textsc{Lovato} \hspace{50pt} \large{VR443506} 
	\end{minipage}
	\hfill
				
	%------------------------------------------------
	%	DATA
	%------------------------------------------------				
	\vspace{8cm}
	\large Anno accademico 2020/2021	 
	%----------------------------------------------------------------------------------------
											
	\vfill % riempie 1/4 di pagina restante
											
\end{titlepage}

%----------------------------------------------------------------------------------------

%----------------------------------------------------------------------------------------
%	HEADER AND FOOTER
%----------------------------------------------------------------------------------------
\pagestyle{fancy}
\thispagestyle{empty} 
\lhead{ \textbf{Suono e Decibel} \\ Luca Lovato - VR443506}
\chead{}
\rhead{Università degli Studi di Verona \\ Dipartimento Informatica}
\lfoot{}
\cfoot{\thepage}
\rfoot{}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}
\tableofcontents
\newpage

%----------------------------------------------------------------------------------------
%	CONSIDERAZIONI SULLA PERCEZIONE DEI SUONI
%----------------------------------------------------------------------------------------
\setcounter{page}{1}
\section{Considerazioni sulla percezione dei suoni}

La \textbf{propagazione di un suono} è affidata allo spostamento della massa d'aria che circonda la 
sorgente sonora che lo ha generato. 

\vspace{1cm}		
\includegraphics[width=0.5\textwidth]{immagini/propagazione.png}
\vspace{1cm}

Tale spostamento d'aria fa sì che la pressione ambientale 
misurata in una certa posizione risulti variabile nel tempo, sia pure in modo microscopico, 
rispetto al valore della pressione atmosferica. 
\medskip

A livello del mare, la pressione atmosferica 
è pari a 100000 Pascal. Una sorgente sonora che emette un suono molto forte, apporta una 
variazione massima di qualche decina di Pascal; all'estremo opposto, un suono appena percettibile
determina una variazione di poche decine di milionesimi di Pascal. \par
Il nostro orecchio percepisce la presenza di una emissione sonora proprio rilevando la variazione 
di pressione ambientale. \\ Tale variazione prende il nome di \textbf{pressione sonora.}
\bigskip

L'energia che possiede un'onda sonora viene definita intensità, essa viene espressa dall'ampiezza
di oscillazione dell'onda. Onde sonore aventi maggiori differenze di pressione locali hanno
un'intensità maggiore. \\
Come introdotto precedentemente, l'unità di misura che viene utilizzata per misurare l'intensità 
delle onde di pressione acustica è il \textbf{Pascal}. 

\begin{center}	
	\includegraphics[width=0.7\textwidth]{immagini/propagazione_suono.png}
\end{center}

\clearpage

%----------------------------------------------------------------------------------------
%	IL LIVELLO SONORO: DAL PASCAL AL DECIBEL
%----------------------------------------------------------------------------------------
\section{Il livello sonoro: dal Pascal al dB$_{SPL}$}

L'unità di misura della pressione sonora assoluta è il Pascal (simbolo Pa, equivalente a 1 $N/m^2$) ma, 
in considerazione delle caratteristiche di percezione dell'apparato uditivo di un \textbf{essere umano}, 
è spesso consigliabile ricorrere ad una descrizione in termini relativi, cioè posta in relazione con un 
valore di riferimento.
\medskip

In generale, come riferimento viene adottato quello che si usa definire la soglia di percezione 
uditiva, numericamente pari a \textmu 20 Pa, ossia 20 milionesimi di pascal, valore che si riferisce ad
un suono al centro della gamma percettibile. Questo significa che il nostro sistema uditivo è 
sensibile ad un intervallo costituito da un milione di variazioni di pressione.
\medskip

L'unità di misura della pressione sonora relativa è il \textbf{Decibel} (simbolo dB spesso 
accompagnato dal suffisso \textit{"SPL"}), acronimo del termine inglese \textit{Sound Pressure Level}.
\medskip

In termini matematici, il livello della pressione sonora relativa è pari a 20 volte il logaritmo 
del rapporto tra la pressione sonora assoluta \textit{P} e la pressione sonora di riferimento 
\textit{$P_{rif}$}. 
\bigskip	

\begin{center}
	$dB_{SPL}$ = $20*\log{P/P_{rif}}$
	\vspace{0.5cm}
	\includegraphics[width=0.7\textwidth]{immagini/scala_log_db.png}
\end{center}

Un livello sonoro corrispondente a $P_{rif}$ = 20 Pa darà come risultato 0 $dB_{SPL}$.
L'apparato uditivo di un essere umano è in grado di operare in presenza di livelli relativi di
pressione sonora compresi tra la soglia di uditibilità (0 $dB_{SPL}$ o anche meno, come si vedrà 
più oltre) sino ad un massimo compreso tra i 120 e i 130 dB$_{SPL}$, livello cui corrisponde la 
cosiddetta \textbf{soglia del dolore}. \par
\medskip

Le ragioni per ricorrere ad una formulazione relativa della pressione sonora in luogo di una 
descrizione in termini assoluti sono due:
\begin{itemize}
	\item La sensazione di percezione del volume di un suono segue leggi logaritmiche più che 
	      lineari
	\item Drastica riduzione della scala dei valori
\end{itemize}

\clearpage

%----------------------------------------------------------------------------------------
%	LA PERCEZIONE DEI SUONI
%----------------------------------------------------------------------------------------
\section{La percezione dei suoni}

Salvo che in presenza di livelli sonori molto elevati, l'apparato uditivo di un essere umano
non è ugualmente sensibile a suoni di diversa frequenza, a parità di intensità degli stessi.

A parità di livello di pressione sonora, l'essere umano percepisce con maggiore facilità suoni
di media frequenza piuttosto che suoni acuti o gravi. \par
\medskip

Ciò è messo in risalto dalle cosiddette \textbf{curve di Fletcher e Munson:}
\begin{center}
	\vspace{0.5cm}
	\includegraphics[width=0.7\textwidth]{immagini/curve_fletcher_e_munson.png}
	\vspace{0.5cm}
\end{center}

La curva più in basso fornisce l'andamento della soglia di sensibilità dell'apparato uditivo
umano in funzione della frequenza dell'emissione sonora. Ogni curva è definita per uniformità
di sensazione uditiva alle varie frequenze, ed a ciascuna di esse viene associata ad un valore
in phon, una unità di misura appositamente introdotta per quantificare \textbf{l'intensità della 
sensazione di ascolto} (in inglese \textit{Ludness Level}).
\medskip

Si noti che all'aumentare dei livelli sonori le curve tendono a divenire sempre meno arcuate
soprattutto verso l'estremo inferiore della banda audio. \par
\smallskip

A livelli sonori elevati, prossimi alla cosiddetta soglia del dolore (120 dB circa a 1 KHz), 
l'orecchio umano presenta differenze di sensibilità alle varie frequenze quantificabili in 
soli 10 dB. Per contro, a livelli sonori appena percettibili, le differenze di sensibilità 
raggiungono quasi i 60 dB. \\
Livelli sonori dell'ordine prossimi alla soglia del dolore 
possono causare \textbf{danni permanenti} all'udito.
\medskip

\clearpage

%----------------------------------------------------------------------------------------
%	LIVELLI SONORI E RELATIVE SENSAZIONI PERCETTIVE
%----------------------------------------------------------------------------------------
\section{Livelli sonori e relative sensazioni percettive}
A seconda della sua intensità, o volume, un suono può produrre un ampio ventaglio di effetti 
sul piano percettivo, più o meno piacevoli, più o meno fastidiosi, come risulta dalla 
seguente tabella.

\vspace{1cm}

\begin{tabular}{lcr}
	Evento o situazione            & Livello sonoro & Effetto acustico       \\
	\hline
	Jet in fase di decollo a 60 mt & 120            & Soglia del dolore      \\
	Lavori in cantiere edile       & 110            & Insopportabile         \\
	Sparo a 1,5 m                  & 100            &                        \\
	Camion pesante a 15 m          & 90             & Molto rumoroso         \\
	Strada urbana trafficata       & 80             &                        \\
	Abitacolo di un'automobile     & 70             & Rumoroso               \\
	Voce di tono normale a 1 m     & 60             &                        \\
	Interno di un ufficio          & 50             & Moderato               \\
	Ambiente domestico             & 40             &                        \\
	Camera da letto di notte       & 30             & Quieto                 \\
	Studio di registrazione        & 20             &                        \\
	Foglie che cadono              & 10             & Appena percebilile     \\
	Voci in lontananza             & 0              & Soglia di uditibilità  \\
\end{tabular}

\vspace{1cm}

%----------------------------------------------------------------------------------------
%	PERCEZIONE DEL PARLATO
%----------------------------------------------------------------------------------------
\section{Percezione del parlato}

\`E ampiamente dimostrato che la sensibilità del nostro apparato uditivo è fortemente votata
alla percezione di suoni di frequenze proprie della voce umana. Ciò è sempre da tenere in 
considerazione quando si parla di riproduzione o diffusione sonora. \par
Tendenzialmente, il sistema di generazione della voce è costituito da una sorgente sonora, 
costituita della corde vocali, e da un sistema di filtraggio che viceversa è rappresentato
dal tratto vocale, ossia da tutto ciò che va dalle corde vocali sino alle "uscite primarie"
del sistema, la bocca e le narici. 

Sia nel canto come nel parlato, le corde vocali controllano il tono della voce mentre il 
tratto vocale articola le consonanti, determina le vocali e stabilisce il \textit{timbro della voce}.

\'E proprio nella pronuncia delle vocali che si ha modo di apprezzare le peculiarità della
menzionata azione di filtraggio del tratto vocale. Analizzando la voce di una persona, non 
importa se di sesso maschile o femminle, all'atto della pronuncia delle vocali, si può infatti
constatare che questa è costituita da vari "pacchetti" di energia sonora concentrati in corrispondenza
di ben precise frequenze. \par
Queste concentrazioni di energia sono denominate \textbf{formanti} e la loro creazione viene 
attribuita, con peso variamente distribuito a seconda del genere di vocale, alle diverse cavità 
risonanti di cui l'organo della voce dispone. 
\medskip

Nel parlato, la gamma delle fondamentali è compresa tra 110 e 165 Hz per una voce maschile e tra 
220 e 330 Hz per voci femminili. La gamma di frequenze della voce è completata da un gran numero 
di parziali, il che porta il limite superiore della gamma ben oltre i 10 KHz per la voce maschile 
e oltre i 15KHz per la voce femminile. 
\medskip

Il livello medio di una conversazione è compreso tra 60 e 70 dB. La distribuzione spettrale della 
voce umana varia in funzione del volume adottato da chi parla. 

\begin{center}
	\includegraphics[width=0.6\textwidth]{immagini/male.png}
\end{center}

%----------------------------------------------------------------------------------------
%	DECIBEL APPLICATO AD APPARATI AUDIO
%----------------------------------------------------------------------------------------
\section{Decibel applicato ad apparati audio}

\textbf{Perché su alcuni apparati il livello di $\pm$ 3 dB è evidenziato?} 
\medskip

Ricordiamo che i dB misurano un livello in scala logaritmica. Perciò, per esempio, un aumento di 3 dB 
nell'intensità di un suono corrisponde circa al \textbf{raddoppio} della sua intensità, e una diminuzione 
di 3 dB corrisponde ad un'intensità quasi \textbf{dimezzata.} 
\medskip

Infatti, detto $I_0$ il livello di riferimento dell'intensità I. Dati due livelli $I_1$ e $I_2$ tali 
che $I_1$ - $I_2$ = 3 dB significa che  $10*\log{I_1/I_0}$ - $10*\log{I_2/I_0}$ = 3 e cioè 
$\log{I_1/I_2}$ = 0.3 da cui si ottiene $I_1/I_2 = 10^{0,3} \approx 1.995$  

\clearpage

%----------------------------------------------------------------------------------------
%	BIBLIOGRAFIA
%----------------------------------------------------------------------------------------
\begin{thebibliography}{1}
	\bibitem{Nicolao} Umberto Nicolao, \textit{Acustica Applicata per la Diffusione sonora} 
	\bibitem{JBL Professional} JBL Professional, \textit{Speech Intelligibility}
\end{thebibliography}

\end{document}
