“Linguaggio di programmazione LaTeX”



Istruzioni per la consegna dell'elaborato



Iscriversi alla lista corrispondente su Essetre



Inviare l'elaborato all'indirizzo istituzionale

   enrico.gregorio@univr.it

con subject

   Corso LaTeX 2020 - Elaborato Finale - VR123456

(al posto di 123456 scrivere il proprio numero di matricola,

mi raccomando; ne ho ricevuto uno con quel numero...).



Non mettete il nome nel subject, perché ai filtri antispam

non piacciono i subject con nomi.



Nel corpo del messaggio scrivete nome, cognome e numero di

matricola e allegate il file .tex, il file .pdf e i file

grafici necessari (un archivio compresso va benissimo).



L'elaborato consiste in un documento scritto in classe

'book' (o altra che abbia i capitoli), con due o più

capitoli divisi in sezioni.



È necessario che il documento contenga: liste numerate

e puntate, formule matematiche e altri ambienti a scelta.

Vanno adoperati anche comandi definiti nel documento stesso.



L'argomento è del tutto libero. Non c'è scadenza temporale,

fatelo quando avete tempo e fantasia per non rendere la

lettura troppo noiosa.



Buon lavoro

Enrico Gregorio