/* Si consideri una lista dinamica di interi, i cui elementi sono del tipo definito come di seguito riportato: 
	typedef struct El { 
		int dato1; 
		int dato2; 
		struct El *next; } elemento; 
Si codifichi in C le funzioni contamultipli e multiplo. La funzione contamultipli riceve come parametro la testa della lista e restituisce il 
numero di elementi della lista che hanno il primo valore numerico (dato1) multiplo del secondo (dato2). Se la lista è vuota, la funzione 
restituisce il valore -1. Per verificare se un valore intero è multiplo di un altro la funzione deve richiamare la funzione ricorsiva multiplo definita dallo studente. 

*/

#include <stdio.h>
#include <stdlib.h>

typedef struct el { 
	int dato1; 
	int dato2; 
	struct el *next;
}elemento; 


int multiplo(int n, int m){
	if(n==m || n==0)
		return (1);
	if(n<m)
		return(0);
	else
		return(multiplo(n-m,m));	
}


elemento* inserisciInCoda(elemento* lista, int num1, int num2){
  elemento* prec;
  elemento* tmp;

  tmp = (elemento*) malloc(sizeof(elemento));
  if(tmp != NULL){
    tmp->next = NULL;
    tmp->dato1 = num1;
	tmp->dato2 = num2;
    if(lista == NULL)
      lista = tmp;
    else{
      /*raggiungi il termine della lista*/
      for(prec=lista;prec->next!=NULL;prec=prec->next);
      prec->next = tmp;
    }
  } else
      printf("Memoria esaurita!\n");
  return lista;
}
 
int contamultipli(elemento* l){
	
  int somma=0;

  while (l!=NULL){
  	  if(multiplo(l->dato1,l->dato2))
		  somma++;
	  l=l->next;
  }
  return somma;
}


int main(){
	elemento* testa=NULL;
	int mul;
	
	testa=inserisciInCoda(testa,1,2);
	testa=inserisciInCoda(testa,2,2); 
	testa=inserisciInCoda(testa,3,3);
	testa=inserisciInCoda(testa,4,2);
	testa=inserisciInCoda(testa,5,4);
	testa=inserisciInCoda(testa,6,7);
		
	
	mul=contamultipli(testa);
	
	printf("I multipli sono %d",mul);	
}
