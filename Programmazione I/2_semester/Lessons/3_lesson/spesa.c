/*

Si consideri la spesa al supermercato dove ogni articolo comprato è caratterizzato da un codice, un prezzo e un eventuale sconto espresso in percentuale (che potrebbe essere 0). 
Si scriva un programma C che permetta di:

Inserire da tastiera una sequenza a priori illimitata di articoli comprati (la sequenza termina quando viene inserito il valore -1 come codice di un articolo, tale codice ovviamente
non fa parte della sequenza).

Calcolare e stampare il numero di articoli scontati acquistati e la spesa totale.

*/

#include <stdio.h>

typedef struct{
	int codice;
	float prezzo;
}prodotto;
	
int main(){

	int cod;
	float tot=0;

	prodotto mela={1,2.35};
	prodotto pera={2,1.96};
	
	while(cod!=-1){
		printf("Inserisci il codice del prodotto acquistato[es. 1]: ");
		scanf("%d", &cod);
	
		if(cod==mela.codice)
			tot+=mela.prezzo;
		if(cod==pera.codice)
			tot+=pera.prezzo;

	}
	printf("La spesa totale risulta: %.2f\n", tot);
}
