/*

Definire un tipo di dato strutturato per rappresentare una frazione in termini di numeratore e denominatore (due numeri interi). Scrivere un programma che acquisisce due frazioni ed esegue la somma. Il programma deve in seguito semplificare il risultato dell'operazione rappresentando l'eventuale segno meno nel numeratore.

*/

#include <stdio.h>
#include <stdlib.h>

typedef struct {
  int n;
  int d;
} fraction_t;


void main(){

    fraction_t a, b, sum;
    int i, segno;

    do
      scanf("%d %d", &a.n, &a.d);
    while(a.d==0);

    do
      scanf("%d %d", &b.n, &b.d);
    while(b.d==0);

    sum.n = a.n * b.d + b.n * a.d;
    sum.d = a.d * b.d;

    segno=1;
    
    if(sum.n<0){
      sum.n=-sum.n;
      segno=-segno;
    }

    if(sum.d<0){
      sum.d=-sum.d;
      segno=-segno;  
    }

    if(sum.n<sum.d) 
      i=sum.n;
    else
      i=sum.d;

    while(sum.n%i!=0 || sum.d%i!=0)
      i--;

    sum.n = sum.n/i * segno;
    sum.d = sum.d/i;

    printf("\nSomma: %d/%d\n", sum.n, sum.d);
}