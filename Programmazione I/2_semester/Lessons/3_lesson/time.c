/*

Scrivere un programma C che calcola il tempo trascorso (in ore, minuti e secondi) tra due istanti di tempo

*/

#include <stdio.h>

#define S 59
#define M 59
#define H 23

typedef struct{
	int ore;
	int minuti;
	int secondi;
}time;
 
time insertTime(){
	
	time clock;
	
	scanf("%d:%d:%d", &clock.ore, &clock.minuti, &clock.secondi);
	
	return clock;
}

void checkTime(time fclock, time sclock){
	
	if(fclock.ore>0 || fclock.ore<H || fclock.minuti>0 || fclock.minuti<M || fclock.secondi>0 || fclock.secondi<S)
		printf("Primo orario inserito CORRETTO\n");
	else
		printf("Primo orario inserito ERRATO\n");
	if(fclock.ore>0 || fclock.ore<H || fclock.minuti>0 || fclock.minuti<M || fclock.secondi>0 || fclock.secondi<S)
		printf("Secondo orario inserito CORRETTO\n");
	else
		printf("Secondo orario inserito ERRATO\n");
}

void elapsedTime(time fclock, time sclock){
	
	int ss;
	
	ss=(((fclock.ore*3600)+(fclock.minuti*60)+fclock.secondi)-((sclock.ore*3600)+(sclock.minuti*60)+sclock.secondi));

	printf("Differenza in secondi fra i due orari: %d\n", ss);
}

int main(){
	
	time fclock;
	time sclock;
	
	fclock = insertTime();
	sclock = insertTime();
	checkTime(fclock,sclock);
	elapsedTime(fclock, sclock);
}