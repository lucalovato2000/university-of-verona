/*

Definire un nuovo tipo di dato per memorizzare una durata temporale in termini di numero di giorni, ore, minuti e secondi.
Scrivere un programma che riceve da tastiera i dati sulla durata di una serie di intervalli di tempo (massimo 31). Ciascun intervallo di tempo è specificato in termini di ore, minuti e secondi trascorsi. L'utente specifica quanti intervalli desidera inserire e poi procede al loro inserimento. Il programma deve assicurarsi che i dati di ciascun intervallo inserito siano validi; in particolare, l'ora deve essere inclusa nell'intervallo [0; 23], i minuti in [0; 59], ed i secondi in [0; 59]. Successivamente, il programma calcola la somma di tutti gli intervalli e la stampa a video nel formato: giorni, ore, minuti, secondi (massimo 23 ore, massimo 59 minuti, massimo 59 secondi.
Nessun limite al numero di giorni).
Esempio: per le seguenti durate di due intervalli di tempo (ore, minuti, secondi):
- 20 50 15
- 7 20 30
il risultato che dovrà essere stampato sarà:
1 giorno/i, 4 ora/e, 10 minuto/i e 45 secondo/i.

*/

#include <stdio.h>

#define MAX_INTERVALLI 31
#define ORE 24
#define MINUTI 60
#define SECONDI 60

typedef struct{
    int giorni;
    int ore;
    int minuti;
    int secondi;
}intervallo_t;


void main(){
  intervallo_t intervalli[MAX_INTERVALLI], somma;
  int size;
  int i;
  
  do{
    printf("Quanti intervalli vuoi inserire?\n");
    scanf("%d",&size);
  }while(size<=0 || size>=MAX_INTERVALLI);
  
  for(i = 0; i < size; ++i) {
    printf("Inserire intevallo #%d: ", i);

    do{
      printf("ore: ");
      scanf("%d", &intervalli[i].ore);
    }while(intervalli[i].ore<0 || intervalli[i].ore>=ORE);

    do{
      printf("minuti: ");
      scanf("%d", &intervalli[i].minuti);
    }while(intervalli[i].minuti<0 || intervalli[i].minuti>=MINUTI);

    do{
      printf("secondi: ");
      scanf("%d", &intervalli[i].secondi);
    }while(intervalli[i].secondi<0 || intervalli[i].secondi>=SECONDI);
    intervalli[i].giorni = 0;
  }
  
  somma.giorni = 0;
  somma.ore = 0;
  somma.minuti = 0;
  somma.secondi = 0;

  for(i = 0; i < size; ++i) {
    somma.ore += intervalli[i].ore;
    somma.minuti += intervalli[i].minuti;
    somma.secondi += intervalli[i].secondi;
  }

  somma.minuti += somma.secondi / SECONDI;
  somma.secondi = somma.secondi % SECONDI;

  somma.ore += somma.minuti / MINUTI;
  somma.minuti = somma.minuti % MINUTI;
  
  somma.giorni += somma.ore / ORE;
  somma.ore = somma.ore % ORE;
  
  printf("%d giorno/i, %d ora/e, %d minuto/i e %d secondo/i\n", somma.giorni, somma.ore, somma.minuti, somma.secondi);
}
