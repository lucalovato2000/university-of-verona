/*

Dichiarare un tipo di dato per rappresentare una località su una cartina in termini di longitudine e latitudine (due interi) ed un nome (una stringa di al max 30 caratteri). Scrivere un programma che acquisisce i dati di dieci località ed individua la coppia di punti a distanza massima visualizzandone i nomi. Nel caso ci siano più punti con la stessa distanza massima stampare soltanto la prima coppia individuata.
Per il calcolo della radice quadrata utilizzare la funzione sqrt() presente nella libreria math.h. La funzione riceve come parametro un numero in virgola mobile e ne restituisce la radice quadrata, anch'essa un numero in virgola mobile. Esempio di utilizzo: ris = sqrt(dato); dove ris e dato sono due variabili float.
VARIANTE: Nel caso ci siano più punti con la stessa distanza massima stampare TUTTE le coppie individuate.

*/

#include <stdio.h>
#include <math.h>
#include <string.h>

#define MAXPUN 10
#define MAXSTR 30

typedef struct {
  int lon;
  int lat;
  char nome[MAXSTR+1];
} localita_t;


void main() {
  localita_t punti[MAXPUN];
  int i, j;
  float max_dist, curr_dist;
  int max_pos1, max_pos2;
  
  for(i = 0; i < MAXPUN; i++){
    scanf("%d %d %s", &punti[i].lon, &punti[i].lat, punti[i].nome);
  }
  
  max_pos1=0;
  max_pos2=1;
  max_dist = sqrt((punti[0].lon-punti[1].lon)*(punti[0].lon-punti[1].lon) + (punti[0].lat-punti[1].lat)*(punti[0].lat-punti[1].lat));
  
  for(i=0; i<MAXPUN; i++)
    for(j=i+1; j<MAXPUN; j++){
      curr_dist = sqrt((punti[i].lon-punti[j].lon)*(punti[i].lon-punti[j].lon) + (punti[i].lat-punti[j].lat)*(punti[i].lat-punti[j].lat));
      if(curr_dist>max_dist){
        max_dist = curr_dist;
        max_pos1=i;
        max_pos2=j;
      }
    }
  printf("%s -> %s\n",punti[max_pos1].nome,punti[max_pos2].nome); 
}
