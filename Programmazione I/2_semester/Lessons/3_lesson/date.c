/*

Scrivere un programma C che richiede all’utente di inserire due date e calcola il numero di giorni trascorsi tra di esse.

*/

#include <stdio.h>
#define ANNO 2020
#define MESE 12
#define GIORNO 31

typedef struct{
	int anno;
	int mese;
	int giorno;
}date;

date inserisciData(){

	date data;

	printf("Inserisci dd/mm/yyyy: ");
	scanf("%d/%d/%d", &data.giorno, &data.mese, &data.anno);

    return data;
}

void controllaData(date prmdata, date scddata){

	if(prmdata.giorno<1 || prmdata.giorno>GIORNO || prmdata.mese<1 || prmdata.mese>MESE || prmdata.anno<1701 || prmdata.anno>ANNO)
		printf("Prima data inserita ERRATA\n");
	else
		printf("Prima data inserita CORRETTA\n");
	
	if(scddata.giorno<1 || scddata.giorno>GIORNO || scddata.mese<1 || scddata.mese>MESE || scddata.anno<1701 || scddata.anno>ANNO)
		printf("Seconda data inserita ERRATA\n");
	else
		printf("Seconda data inserita CORRETTA\n");
}

int main(){
	
	date prmdata;
	date scddata;
	
	prmdata = inserisciData();
    scddata = inserisciData();

	controllaData(prmdata, scddata);
}
