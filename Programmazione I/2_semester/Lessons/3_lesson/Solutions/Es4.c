#include<stdio.h>

#define MAXCD 100
#define MAXSTR 30
#define SH 3600
#define SM 60

typedef struct{
	int ore;
	int minuti;
	int secondi; 
}durata_t ;

typedef struct{
	char titolo[MAXSTR+1];
	char autore[MAXSTR+1];
	int anno;
	int tracce;
	durata_t durata;
	float prezzo;
	int copiedisponibili;
}cd_t;

typedef struct{ 
	cd_t cd[MAXCD];
	int ncd;
}archivio_t;

int main(){ 
	archivio_t arc ;
	int nins, n, nmax = 0, dmax = 0, dtemp; 
	
	/* inserimento 1cd */
	printf("\n Quanti CD?: ");
	scanf("%d", &nins);
	for(n = 0, arc.ncd = 0; n<nins; n++, arc.ncd++) { 
		printf("\n Inserisci dati CD n. %d\n",n);
		scanf("%d %d %d", &arc.cd[n].durata.ore,&arc.cd[n].durata.minuti,&arc.cd[n].durata.secondi); 
		scanf("%d", &arc.cd[n].anno);
		scanf("%s", arc.cd[n].titolo);
		
		// cosi via per le altre vars
	}

	/* ricerca cd con durata max */ 
	for (n = 0; n<arc.ncd; n++) {
		dtemp = arc.cd[n].durata.ore*SH + arc.cd[n].durata.minuti*SM + arc.cd[n].durata.secondi; 
		if(dtemp > dmax) {
			dmax = dtemp;
			nmax = n; }
		}
printf("CD %d ha durata %d", nmax, dmax);
}