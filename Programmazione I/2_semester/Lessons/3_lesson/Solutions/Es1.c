#include <stdio.h>
#include <stdbool.h>

// ============================================================================

typedef struct{
  int day;
  int month;
  int year;
}date;

// ============================================================================

date insertDate();
bool checkDate(date d );
int maxDays( int month, int year );
bool leapYear( int year );
int elapsedDays(date d1, date d2);
int edValue(date d );

// ============================================================================

int main(void) {
  date d1,d2;
  d1 = insertDate();
  d2 = insertDate();

  printf( "Il numero di giorni trascorsi tra %.2i/%.2i/%.4i e %.2i/%.2i/%.4i e' %i.\n",
          d1.day, d1.month, d1.year,
          d2.day, d2.month, d2.year,
          elapsedDays( d1, d2 ));

  return 0;
}

/********************************************************************
 * Richiede di inserire una data e restitiusce la corrispondente 
 * struttura. Se la data inserita non e' corretta viene richiesta.
 ********************************************************************/
date insertDate(){
  int day, month, year;
  date d;
  bool end = false;

  while( !end ){ 
    printf( "Inserire una data uguale o superiore al 1/3/1970 nel formato d/m/y.\n" );
    scanf( "%i/%i/%i", &day, &month, &year );
    d.day = day;
    d.month = month;
    d.year = year;
    end = checkDate( d );
    if( !end ){
      printf( "La data inserita non e' valida.\n" );
    }
  }
  return d; 
}

/********************************************************************
 * Verifica se la data è corretta.
 ********************************************************************/
bool checkDate(date d ){
  int n;

  // anno 4 cifre
  if( d.year < 999 || d.year > 9999 ){
    return false;
  }
  // mese compreso tra 1 e 12
  if( d.month < 1 || d.month > 12 ){ 
    return false;
  }
  
  // giorno compreso tra 1 e numero massimo giorni del mese
  n = maxDays( d.month, d.year );
  if( d.day < 1 || d.day > n ){
    return false;
  }

  if( d.year < 1700 ){
    return false;
  } else if( d.year == 1700 && d.month < 3 ){
    return false;
  }

  return true;
}

/********************************************************************
 * Ritorna il numero massimo di giorni per il mese e l'anno passati
 * come parametro
 ********************************************************************/
int maxDays( int month, int year ){
  if( month == 1 || month == 3 || month == 5 || 
      month == 7 || month == 8 || 
      month == 10 || month == 12 ){
    return 31;
  } else if( month == 4 || month == 6 ||  
             month == 9 || month == 11 ) {
    return 30;
  } else if( leapYear( year ) ) {
    return 29;
  } else {
    return 28;
  }
}

/********************************************************************
 * Ritorna true se l'anno e' bisestile, false altrimenti
 ********************************************************************/
bool leapYear( int year )  {
  if( year % 4 == 0 && ( year % 100 != 0 || year % 400 == 0 )){
    return true;
  } else {
    return false;
  }
}

/********************************************************************
 * Calcola la differenza tra due date
 ********************************************************************/
int elapsedDays(date d1, date d2 ){
  int n1, n2;
  n1 = edValue( d1 );
  n2 = edValue( d2 );

  return n2 - n1;
}

int edValue(date d ){
  int n, f, g;
  if( d.month <= 2 ){
    f = d.year - 1;
    g = d.month + 13;
  } else {
    f = d.year;
    g = d.month + 1;
  }

  n = 1461 * f/4 + 153*g/5 + d.day;

  if( ( d.year == 1800 && d.month >= 3 && d.day >= 1 ) ||
      ( d.year == 1900 && d.month <= 2 && d.day <= 28 )) {
    n = n+1;
    printf( "%i %i %i + 1", d.day, d.month, d.year );
  } else if( ( d.year == 1700 && d.month >= 3 && d.day >= 1 ) ||
      ( d.year == 1800 && d.month <= 2 && d.day <= 28 )) {
    n = n+2;
    printf( "%i %i %i + 2", d.day, d.month, d.year );
  }
 
  return n;
}


