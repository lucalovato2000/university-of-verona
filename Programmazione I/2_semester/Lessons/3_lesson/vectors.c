/* 
Scrivere un programma C in grado di:
1. Acquisire in ingresso dall'utente i valori di un vettore vet di 10 elementi.
2. Stampare gli elementi del vettore. 
3. Determinare il valore massimo e minimo e la relativa posizione (salvando le informazioni in 2 struct).
4. Ordinare il vettore.
4. Stampare il vettore ordinato.
*/

#include <stdio.h>
#define MAXDIM 10

typedef struct{
	int val;
	int pos;
}info;

int main(){ 
	
	int vet[MAXDIM];
	info min,max;	//minimo e massimo hanno la struttura info con due parametri
	int i,j;
	int scambio;

	for(i=0;i<MAXDIM;i++){
		printf("Inserisci un valore intero: ");
		scanf("%d", &vet[i]);
	}

	printf("\nI valori inseriti sono: ");
	for(i=0;i<MAXDIM;i++){
		printf("%d ", vet[i]);
	}
	printf("\n");
	min.val=vet[0];	//minimo in posizione 0
	min.pos=0;
	max.val=vet[0];	//massimo in posizione 0
	max.pos=0;
  
	//Algoritmo Bubble Sort
	for(i=1;i<MAXDIM;i++){
		if(vet[i]<min.val){
		    min.val=vet[i];
		    min.pos=i;
		 }
		 if(vet[i]>max.val){
		    max.val=vet[i];
		    max.pos=i;
		 }
	}
	printf("Il massimo risulta %d in posizione %d\n",max.val,max.pos);
	printf("Il minimo risulta %d in posizione %d",min.val,min.pos);

	for(i=0;i<MAXDIM-1;i++)
		for(j=0;j<MAXDIM-1;j++)
			if(vet[j]>vet[j+1]){ 
     			//scambia il contenuto dei due elementi 
      			scambio=vet[j];
      			vet[j]=vet[j+1];
     			vet[j+1]=scambio;    
			}
  	printf("\nI valori ordinati sono: ");
	
	for(i=0;i<MAXDIM;i++)
		printf("%d ",vet[i]);
	printf("\n");
}
