/*
Scrivere una funzione sort che ordina 3 interi in ordine crescente. 
La funzione non deve utilizzare un array ma 3 puntatori.
*/

#include <stdio.h>

void sort(int *pa, int *pb, int *pc){
	
	if(*pa>=*pb){
		if(*pa>*pc)
			printf("%d", *pa);
		if(*pc>=*pa)
			printf("%d", *pc);
		
	}if(*pb>*pa){
		if(*pb>=*pc)
			printf("%d", *pb);
		if(*pc>=*pb)
			printf("%d", *pc);
	}
}

int main(){
	
	int a, b, c;
	int *pa=&a, *pb=&b, *pc=&c;
	
	printf("Inserisci un valore: ");
	scanf("%d %d %d", &*pa, &*pb, &*pc);

	sort(pa, pb, pc);
}
