/*Scrivere un sottoprogramma che riceve due numeri interi positivi e calcola l'elevamento a potenza del primo rispetto al secondo, restituendo il risultato.
Scrivere un sottoprogramma che calcola la radice ennesima intera di un numero intero positivo. Il sottoprogramma prende come parametri il numero, il grado della radice, e una variabile, passata per indirizzo, in cui memorizzare la radice intera. Il sottoprogramma restituisce 1 se la radice intera è precisa, in alternativa 0. Scrivere un programma che utilizza tale sottoprogramma per calcolare la radice ennesima intera di un numero e di un grado chiesti all'utente, e ne visualizza il risultato.*/

#include <stdio.h>

int radice(int, int, int*);
int potenza(int, int);

void main() {
  int a,b, ris, intera;
  scanf("%d %d",&a,&b);
  intera=radice(a,b,&ris);
  printf("%d %d\n",ris,intera);
}

int radice(int n, int k, int* ris)
{
  *ris=1;
  while(!(potenza((*ris),k)>n))
    (*ris)++;
  (*ris)--;  
  return potenza(*ris,k)==n;
}

int potenza(int n,int k)
{
  int ris,i;
  ris=1;
  for(i=0;i<k;i++)
    ris=ris*n;
  return ris;
}
