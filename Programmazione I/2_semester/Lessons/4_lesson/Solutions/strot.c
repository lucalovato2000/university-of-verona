/*
Scrivere un programma che acquisisce una stringa s1 di massimo 50 caratteri e un numero intero n. Il programma controlla se n è maggiore di zero e minore della lunghezza della stringa e, se le condizioni non sono verificate continua a richiedere il valore; poi il programma crea una nuova stringa s2 che contiene la rotazione verso destra di s1 di n posizioni e la visualizza.
Esempio: s1="straniero" e n=2 -> s2="ranierost".
*/

#include<stdio.h>
#define MAX 50

void main(){
  int n,l,i;
  char str1[MAX+1],str2[MAX+1];
  scanf("%s", str1);
  for(l=0;str1[l]!='\0';l++);
  
  scanf("%d",&n);
  while(n<=0 || n >= l)
    scanf("%d",&n);  
    
  for(i=0;str1[i]!='\0';i++){
    str2[(i+n)%l]=str1[i];
  }
  str2[i]='\0';
  printf("%s\n",str2);
}
