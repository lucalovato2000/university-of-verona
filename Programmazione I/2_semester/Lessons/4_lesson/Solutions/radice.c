/*Scrivere un sottoprogramma che calcola la radice quadrata intera di un numero intero positivo. Il sottoprogramma riceve come parametro il numero intero ed un'altra variabile, passata per indirizzo, in cui memorizzare la radice intera. Il sottoprogramma restituisce 1 se la radice intera è precisa, in alternativa 0. Scrivere un programma che utilizza tale sottoprogramma per calcolare la radice quadrata intera di un numero chiesto all'utente, e ne visualizza il risultato.
*/

#include <stdio.h>

int radice(int, int*);

void main() {
  int a, ris, intera;
  scanf("%d",&a);
  intera=radice(a,&ris);
  printf("%d %d\n", ris, intera);
}

int radice(int n, int* ris)
{
  //controllo che siano positivi altrimenti ritorna -1
  *ris=1;
  while(!((*ris)*(*ris)>n))
    (*ris)++;
  (*ris)--;
  return (*ris)*(*ris)==n;
}
