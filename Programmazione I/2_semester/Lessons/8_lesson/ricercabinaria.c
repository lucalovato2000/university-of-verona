/*

Ricerca binaria in un array, presuppone che l'array sia ordinato

*/

#include <stdio.h>
#include <stdlib.h>
#define DIM 10

int rb(int v[DIM], int primo, int ultimo, int val){

    int med;

    if(primo>ultimo)
        return -1;
    else{
        med=(primo+ultimo)/2;
        if(val==v[med])
            return med;
        else if(val>v[med])
            return rb(v, med+1, ultimo, val);
        else
            return rb(v, primo, med-1, val);

    }
}

int main(){

    int v[DIM]={0,1,2,3,4,5,6,7,8,9};

    int val, ris;

    printf("Inserisci un valore a scelta: ");
    scanf("%d", &val);

    ris=rb(v, 0, DIM-1, val);

    if(ris==val)
        printf("Il valore inserito si trova nell'array\n");
    else    
        printf("Il valore inserito non si trova nell'array\n");
} 
