/*

Allocare dinamicamente una matrice

*/

#include <stdio.h>
#include <stdlib.h>

int main(){

   int i,j;

   int **p;
   int r,c;

   printf("Inserisci numero di righe e colonne: ");
   scanf("%d %d", &r, &c);

   p=(int **)malloc(r*sizeof(int *));   //heap area per spazi di array

    for(i=0;i<r;i++){
        p[i]=malloc(c*sizeof(int));
    }

    for(i=0;i<r;i++){
        for(j=0;j<c;j++){
            scanf("%d", &p[i][j]);
        }
    }

    printf("Matrice stampata:\n");
    for(i=0;i<r;i++){
        for(j=0;j<c;j++){
            printf("%d", p[i][j]);
        }
        printf("\n");
    }
}
