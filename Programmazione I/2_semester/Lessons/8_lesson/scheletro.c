/*

Data una situaizone in memoria scrivere lo scheletro del codice

*/

#include <stdio.h>
#include <stdlib.h>

long w=10;

void f2(){

    long t;
    //la situazione in memoria in questo punto
}

void f1(long *z){

    f2();   //posso richiamare f2 perche nello stack
}

int main(){
    
    long x=0;
    
    //Due puntatori saranno memory leak mentre l'ultimo corrisponde ad y
    
    long *y=(long *)malloc(sizeof(long));    //leak
    *y=17;

    y=(long *)malloc(sizeof(long));     //leak
    *y=27;

    y=(long *)malloc(sizeof(long));     //heap
    *y=27;

    f1(y);
}
