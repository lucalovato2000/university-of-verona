/*

Allocare dinamicamente una matrice

*/

//trattare la matrice come un array linearizzato

#include <stdio.h>
#include <stdlib.h>

int main(){

    int i,j;
    int *p;
    int r,c;

    printf("Inserisci il numero di righe e colonne: ");
    scanf("%d%d", &r, &c);

    p=(int *)malloc(r*c*sizeof(int));

    for(i=0;i<r;i++){
        for(j=0;j<c;j++){
            scanf("%d", &p[i*c+j]);     //i*c+j come array linearizzato
        }
    }

    printf("Matrice stampata:\n");
    for(i=0;i<r;i++){
        for(j=0;j<c;j++){
            printf("%d", p[i*c+j]);     
        }
        printf("\n");
    }
}
