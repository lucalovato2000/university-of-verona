/*

Anagrammi di una stringa di due caratteri

*/

#include <stdio.h>

void anagrammi(char s[], int n){
   
    char temp;
    int i;

    if(n==0)
        printf("%s", s);
    else{
        for(i=0;i<n;i++){
            temp=s[i];
            s[i]=s[n];
            s[n]=temp;

            anagrammi(s,n-1);   //n-(caratteri stringa-1)
            s[n]=s[i];
            s[i]=temp;
        }
    }
}

int main(){

    char s[]="si";  //possibile inserire anche piu' caratteri
    
    anagrammi(s, 1);    //n-(caratteri stringa-1)
}
