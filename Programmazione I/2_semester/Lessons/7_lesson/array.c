/*

Esercizio su array utilizzando la memoria dinamica

*/

#include <stdio.h>
#include <stdlib.h>

int main(){

    int *p;     //stack
    int n,i,j;
    int cont=0;

    printf("Quanti elementi vuoi inserire? ");
    scanf("%d", &n);

    p=(int *)malloc(n*sizeof(int)); //riserva nello stack un array dinamico

    for(i=0;i<n;i++){
        scanf("%d", &p[i]);
            for(j=0;j<10;j++){
                if(p[i]==j*3){
                    cont++;
                }
            }
    }
    
    printf("Array inserito: ");
    for(i=0;i<n;i++){
        printf("%d", p[i]); 
    }

    printf("\nSono stati trovati %d multipli di 3\n", cont);
    p=realloc(p,(n+2)*sizeof(int));    //rialloco la memoria
    
    printf("Inserisci un ulteriore numero a scelta: ");
    scanf("%d", &p[i]);
       for(j=0;j<10;j++){
            if(p[i]==j*3){
                cont++;
            }
       }
    p[i+1]=cont;

    printf("Array inserito con ultima cifra cont: ");
   
    for(i=0;i<n+2;i++){
        printf("%d", p[i]); 
    }

    printf("\n");

    free(p);    //libero p
}