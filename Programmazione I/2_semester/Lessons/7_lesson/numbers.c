#include <stdio.h>
#include <stdlib.h>     //libreria utilizzata per memoria dinamica

int main(){

    int *p;     //stack
    int nnum,i;

    printf("Quanti elementi vuoi inserire?");
    scanf("%d", &nnum);

    p=(int *)malloc(nnum*sizeof(int));      //heap

    for(i=0;i<nnum;i++){
        scanf("%d", &p[i]);     //equivalenza tra puntatori e array
    }

    for(i=0;i<nnum;i++){
        printf("%3d", p[i]); 
    }

    p=realloc(p,(nnum+1)*sizeof(int));    //rialloco la memoria
    scanf("%d", &p[i]);

    for(i=0;i<nnum+1;i++){
        printf("%3d ", p[i]); 
    }

    free(p); //libera stack

    return 0;
}
