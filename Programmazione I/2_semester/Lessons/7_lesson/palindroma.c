#include <stdio.h>

void leggi( char str[] ) {
  printf( "inserisci una stringa di massimo 80 caratteri: " );
  scanf( "%80s", str ); //forza input
}

int lunghezza( char str[] ){  //oppure utilizzare strlen() con libreria string.h
  int i = 0;
  while( str[i] != '\0' ){
    i++;
  }
  return i;
}

int palindroma(char str[], int inizio, int fine) {
  if (inizio >= fine){
    return 1;  // la stringa e' vuota oppure ha un solo carattere
  } else if ( str[inizio] != str[fine] ) {
    return 0; // primo e ultimo carattere diversi 
  } else {
    return palindroma( str, inizio+1, fine-1 ); // chiamata ricorsiva
  }
}

int main(void) {
  char s[81]; 
  int l;

  leggi( s );
  l = lunghezza( s ) ;
  
  if( palindroma( s, 0, l-1 ) ){
    printf( "La parola %s e' palindroma.\n", s );
  } else {
    printf( "La parola %s non e' palindroma.\n", s );
  }
  return 0;
}
