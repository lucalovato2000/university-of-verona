/*

Scrivere un programma scomposto in funzioni che:

 - Definisce globalmente una matrice 4*4 di interi
 - Riempie la matrice con valori random tra 0 e 10 (estremi compresi)
 - Stampa la matrice
 - Restituisce al main l’indice di riga e di colonna che hanno somma degli elementi massima (se esistono più righe e/o colonne con somma massima restituire la prima incontrata)

*/

#include <stdio.h>
#include <time.h>
#define N 3

int mat[N][N];


void inserisci(){
	int i,j;
	
	for(i=0;i<N;i++){
		for(j=0;j<N;j++){
			mat[i][j]=(rand() % 11);
		}
	}
}


void stampa(){
	int i,j;
	
	for(i=0;i<N;i++){
		for(j=0;j<N;j++){
			printf("%d",mat[i][j]);
		}
		printf("\n");
	}
}


int somma(int *c){
	int i,j,maxr,maxc,r;
	int sommar,sommac;
	sommar=0;
	sommac=0;
	maxr=0;
	maxc=0;
	
	for(i=0;i<N;i++){
		for(sommar=0,j=0;j<N;j++)
			sommar+=mat[i][j];
		if(sommar>maxr){
			maxr=sommar;
			r=i;
		}
		for(sommac=0,j=0;j<N;j++)
			sommac+=mat[j][i];
		if(sommac>maxc){
			maxc=sommac;
			*c=i;
		}
	}	
	return r;
}

int main(){
	
	int r,c;
	
	srand(time(NULL));
	inserisci();
	stampa();
	
	r=somma(&c);
	
	printf("\n Riga con somma max %d, colonna %d",r,c);
}