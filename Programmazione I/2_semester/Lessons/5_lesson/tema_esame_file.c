/*
Il programma chiede una sequenza di numeri terminata da un valore 0 o negativo
Il programma conta per ogni numero i divisori propri e salva il risultato in un file .txt
*/

#include <stdio.h>

int main(){

    FILE *fp;
    int n,ndiv,div;

    fp=fopen("risultato.txt", "w");
   
    if(fp){     //se la connessione e' stata eseguita
        scanf("%d", &n);
        while(n>0){
            for(ndiv=0,div=1;div<=n/2;div++){
                if(n%div==0)
                    ndiv++;
            }
            fprintf(fp,"%d%d\n",n,ndiv);
        }
        fclose(fp);
    }
    return 0;
}