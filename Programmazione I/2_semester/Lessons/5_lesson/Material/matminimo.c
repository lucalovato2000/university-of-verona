/*	Si scriva un programma C opportunamente scomposto in funzioni e procedure che permetta all’utente di inizializzare una matrice di interi di dimensione NxN (con N dato) con numeri positivi presi dal file «input.txt». Supponendo che il file contenga un numero sufficiente di valori positivi.
    Dopo aver stampato la matrice a video, il programma deve trovare il minimo di tutti i valori inseriti nella matrice e stampare in un file di nome “risultato.txt” tale valore.
    Ad esempio, dato N=4, se l’utente inserisse tamite file la seguente matrice:
 	11   42     0	 47
	55    3    45   12
	  2   23   55   55
      1     5    7      0

     Il programma, dopo averla visualizzata a video, dovrebbe scrivere nel file “risultato.txt” il numero 0.


*/

#include<stdio.h>
#define N 4
void leggi(int mat[][N]);
void stampa(int mat[][N]);
void ScrivisuFile(int mat[][N], int min);
int trovaminimo(int mat[][N]);

int main(){
	int m[N][N];
	int minimo;
	
	leggi(m);
	stampa(m);
	minimo=trovaminimo(m);
	ScrivisuFile(m,minimo);
}

void leggi(int mat[N][N]){
	int i,j;
	FILE *fp;
	int primo;
	
	fp=fopen("input.txt","r");
	if(fp){
		for(i=0; i<N; i++)
			for(j=0;j<N;j++)
					fscanf(fp,"%d",&mat[i][j]);
	
		fclose(fp);
	}
	else printf("Errore apertura file");
}


void stampa(int mat[N][N]){
	int i,j;

		for(i=0; i<N; i++){
			for(j=0;j<N;j++)
				printf("%3d",mat[i][j]);
			printf("\n");
		}
		
}

int trovaminimo(int mat[N][N]){
	int min;
	int i,j;
	
	min=mat[0][0];
	for(i=0; i<N; i++)
		for(j=0;j<N;j++)
			if(mat[i][j]<min)
				min=mat[i][j];
	return min;
}

void ScrivisuFile(int mat[N][N], int min){
	int i,j;
	FILE *fp;
	
	fp=fopen("risultato.txt","w");
	if(fp){
			fprintf(fp,"%d\n",min);
			fclose(fp);
	}
	else printf("Errore nell'apertura dei file");
}