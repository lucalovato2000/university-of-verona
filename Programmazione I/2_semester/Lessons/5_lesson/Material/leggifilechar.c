#include <stdio.h> 
//#include <stddef.h> 

int main() {
	
	FILE *fp;
	char c;
	
	fp = fopen("input.txt", "r"); /* file lettura, modalità testo */
	
	if ( fp != NULL ) {	
		fscanf(fp,"%c",&c); 
		while (!feof(fp)) {	
			printf("%c",c);
			fscanf(fp,"%c",&c); 
		}
		fclose(fp);
	}
	else
		printf("Il file non può essere aperto\n");

}