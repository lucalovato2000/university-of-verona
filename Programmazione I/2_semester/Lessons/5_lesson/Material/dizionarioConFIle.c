/*
 Si vuole realizzare un programma per la gestione di un dizionario italiano-inglese.
 Definire un tipo di dato traduzione_t per rappresentare una parola in italiano e la sua corrispondente traduzione in inglese (rappresentate con due stringhe da al massimo 30 caratteri).
 Definire in seguito un tipo dizionario_t che permette di rappresentare un dizionario contenente al massimo 100 coppie parola-traduzione.
 Scrivere un programma che presenta all'utente un menù contenenti le seguenti voci:
 1- inserire una nuova parola
 2- visualizzazione del dizionario
 3- cancellazione di una parola
 4- ricerca di una parola italiana per visualizzare la corrispondente traduzione in inglese
 5- caricare il dizionario da file di testo il cui nome è chiesto all'utente (stringa di la massimo 30 caratteri)
 6- salvare il dizionario in un file di testo il cui nome è chiesto all'utente (stringa di la massimo 30 caratteri)
 7- uscita
 Una volta che l'utente inserisce la sua scelta il programma esegue la funzionalità corrispondente e ripresenta il menu finché non viene chiesto di uscire.
 Note:
 * implementare ciascuna funzionalità con un sottoprogramma che riceve il dizionario come parametro ed esegue il compito (richiesto chiedendo i dati all'utente, modificando se necessario i dati del dizionario e presentando a video gli eventuali risultati);
 * assumere per semplicità che le parole (sia italiane che inglesi) non contengono spazi;
 * nell'inserimento di una nuova parola tener conto del fatto che il dizionario è in grado di contenere al massimo 100 elementi quindi nel caso non ci sia più spazio il programma deve visualizzare un apposito messaggio di errore;
 * eseguire l'inserimento senza considerare un ordine alfabetico ma appendendo la nuova coppia in fondo alla lista;
 * per cancellare una parola è necessario specificare l'indice della linea che si vuole cancellare (partendo da 0);
 * nella ricerca della parola, tener conto del fatto che ciascuna parola potrebbe avere più traduzioni possibili quindi potrebbero esserci più posizioni del dizionario che contengono la stessa parola italiana. Nel caso visualizzarle tutte;
 * nel caricamento del dizionario da file, il precedente contenuto del dizionario viene cancellato. Inoltre controllare durante il caricamento che non finisca lo spazio disponibile in memoria; in tale caso, le parole in eccedenza vengono scartate.

*/

#include <stdio.h>
#include <string.h>
#define DIZ_LEN 100
#define WORD_LEN 30

typedef struct{
  char italiano[WORD_LEN+1];
  char inglese[WORD_LEN+1];
} traduzione_t;

typedef struct{
  traduzione_t parole[DIZ_LEN];
  int size;
} dizionario_t;


void main(int argc, char* argv[]) {
  dizionario_t dizionario;
  int scelta, i, j, uguali;
  char parola[WORD_LEN+1];
  FILE* fp;

  dizionario.size =0;

  do{
    printf("\nmenu:\n1) inserisci parola\n2) visualizza dizionario\n3) cancella parola\n4)"
           " ricerca parola\n5) carica dal file\n6) salva su file\n7) esci\n");
    scanf("%d",&scelta);
    if(scelta==1){
      if(dizionario.size<DIZ_LEN){
        printf("inserisci parola italiana e traduzione inglese: ");
        scanf("%s %s", dizionario.parole[dizionario.size].italiano, dizionario.parole[dizionario.size].inglese);
        dizionario.size++;
      } else
        printf("dizionario pieno\n");
    }
    else if(scelta==2){
      printf("dizionario:\n");
      for(i=0; i<dizionario.size; i++){
        printf("%d- %s %s\n", i, dizionario.parole[i].italiano, dizionario.parole[i].inglese);
      }
    }
    else if(scelta==3){
      printf("inserisci indice parola da eliminare: ");
      scanf("%d", &i);
      if(i>=0 && i<dizionario.size){
        for(; i<dizionario.size-1; i++);
          dizionario.parole[i] = dizionario.parole[i+1];
        dizionario.size--;
      } else
        printf("posizione non valida\n");
    }
    else if(scelta==4){
      printf("inserisci parola italiana da cercare: ");
      scanf("%s",parola);
      for(i=0; i<dizionario.size; i++)
        if(strcmp(parola,dizionario.parole[i].italiano)==0)
          printf("%s\n", dizionario.parole[i].inglese);
    }
    else if(scelta==5){
      printf("inserisci nome del file contenente il dizionario: ");
      scanf("%s",parola);
      fp=fopen(parola, "r");
      if(fp){
        dizionario.size=0;
        fscanf(fp, "%s %s",dizionario.parole[dizionario.size].italiano, dizionario.parole[dizionario.size].inglese);
        while(!feof(fp) && dizionario.size<DIZ_LEN){
          dizionario.size++;
          if(dizionario.size<DIZ_LEN)
            fscanf(fp, "%s %s",dizionario.parole[dizionario.size].italiano, dizionario.parole[dizionario.size].inglese);
        }
        fclose(fp);
      } else
        printf("Errore di apertura del file");
    }
    else if(scelta==6){
      printf("inserisci nome del file contenente il dizionario: ");
      scanf("%s",parola);
      fp=fopen(parola, "w");
      if(fp){
        for(i=0; i<dizionario.size; i++)
          fprintf(fp, "%s %s\n", dizionario.parole[i].italiano, dizionario.parole[i].inglese);
        fclose(fp);
      } else
        printf("Errore di apertura del file");
    }
    else if(scelta==7){
      printf("esci\n");
    }
    else {
      printf("scelta non valida\n");
    }
  }while(scelta!=7);
}

