#include<stdio.h>
#define FNAME "testo.txt"
#define MAXS 15

void main(){
  int i, ok;
  FILE *fp;
  char parola[MAXS+1];

  fp=fopen(FNAME,"r");
  if(fp){
    fscanf(fp,"%s",parola);
    while(!feof(fp)){
      for(i=0, ok=1; parola[i]!='\0' && ok; i++)
        if(parola[i]<'0' || parola[i]>'9')
          ok=0;
      if(ok)
        printf("%s\n", parola);
      fscanf(fp,"%s",parola);
    }
    fclose(fp);
  } else
    printf("Errore di apertura del file\n");
}
