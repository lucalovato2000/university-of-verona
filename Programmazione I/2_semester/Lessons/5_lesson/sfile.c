/*
Crea un nuovo file .c con all'interno tanti valori quanti ne richiede l'utente
*/

#include <stdio.h>
#define MAX 50

int main(){
	
	char nome[MAX+1];	//istanzia la stringa nome
	FILE *f;	//instanzia f che punta al file
	int n, i, v;
	
	printf("Inserisci il nome del file: ");
	scanf("%s", nome);	//ottiene il nome del file
	
	f=fopen(nome, "w");	//apre il file in modalita' scrittura
	if(f){	//apre il file
		printf("Quanti valori vuoi inserire? ");	
		scanf("%d", &n);	//ottiene il numero dei valori da inserire
		for(i=0;i<=n;i++){	
			scanf("%d", &v);	//chiede il numero all'utente
			fprintf(f, "%d\n", v);	//memorizza il numero inserito nel puntatore f che punta al file .txt
		}
		fclose(f);	//chiude il file .txt
	}
} 