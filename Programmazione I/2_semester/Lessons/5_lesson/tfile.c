/*
Leggere da un file .txt un insieme di valori
*/

//feof() serve per sapere se il puntatore si trova alla fine del file .txt

#include <stdio.h>
#define MAX 50

int main(){
	char nome[MAX+1];	//istanzia la stringa nome
	FILE *f;	//istanzia f che punta a NOME
	int n;
	
	printf("Inserisci il nome del file: ");
	scanf("%s", nome);	//ottiene in input il nome del file
	
	f=fopen(nome, "r");	//apre in modalita' di lettura il file
	if(f){	//apre il file
		fscanf(f, "%d", &n);	//ottieni il numero a cui punta f
		while(!feof(f)){	//finche' non si punta alla fine del file
			printf("%3d", n);	//stampa il numero n
			fscanf(f,"%d", &n);	//ottieni il numero successivo
		}
		fclose(f);	//chiudi il file .txt
	}
	else printf("Errore");	//se il file non risulta presente stamperà errore
}