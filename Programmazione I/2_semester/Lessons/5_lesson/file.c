/*
Scrivere un programma che apre il file ciao.txt in scrittura e vi scrive i numeri da 1 a 10
*/

#include <stdio.h>

int main(){

	FILE *fp; //dichiarazione puntatore al file
	int n;
	
	fp = fopen("ciao.txt", "w");	//apertura del file in scrittura
	if(fp){		//apertura del file
		for(n=1;n<=10;n++)
			fprintf(fp, "%d ", n);	//scrittura nel file
		fclose(fp);	//chiusura del file
	}else	
		printf("Errore di apertura del file\n");
}	
