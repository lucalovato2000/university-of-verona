#include <stdio.h>

typedef struct{
	int a;
	int b;

}miotipo;

miotipo f1(miotipo);
void f2(miotipo *);

int main(){
	miotipo var;
	
	printf("Inserisci i valori: ");
	
	scanf("%d %d", &var.a,&var.b);
	
	printf("\n I valori sono: %d %d", var.a,var.b);
	
	printf("\n Chiamo f1 \n");
	
	var=f1(var); 

	printf("\n Ora i valori sono: %d %d", var.a,var.b);
	
	printf("\n Chiamo f2 \n");
	
	f2(&var); 

	printf("\n Ora i valori sono: %d %d", var.a,var.b);
	

}

miotipo f1(miotipo v){
	v.a++;
	v.b++;
	
	return v;
}

void f2(miotipo *v){
	(*v).a++;
	//equivalente
	(v->a)++;
	(*v).b++;	
	//equivalente
	(v->b)++;
	
	//se sono equivalenti i campi aumentano di 2 in f2
}
