/* file.c LEGGE DUE ELENCHI ORDINATI DI NUMERI E STAMPA 
   A VIDEO E SU FILE UN UNICO ELENCO ORDINATO          */

/*   autore           data               */

#include <stdlib.h>
#include <stdio.h>

/* dichiarazione della funzione principale  */

int ciclo(FILE *f, int *x, FILE *fo);

int main()
{

  /* Dichiarazione delle variabili */
  FILE *fa, *fb;  /* Puntatori ai file da aprire        */
  FILE *fc;       /* Puntatori al file su cui scrivere  */
  int a, b;  /* Dati letti dai due file            */
  int na, nb;     /* Numero di dati letti               */

  /* Apre i tre file */
  fa = fopen("elencoa.txt","r");
  if (fa == NULL)
  {
    printf("Errore nell'aprire un file\n"); /* Scrive messaggio di errore */
    exit(0);                                /* Termina il programma       */
  }
  fb = fopen("elencob.txt","r");
  if (fb == NULL)
  {
    printf("Errore nell'aprire un file\n"); /* Scrive messaggio di errore */
    fclose(fa);
    exit(0);                                /* Termina il programma       */
  }

  fc = fopen("risultato.txt","w");
  if (fc == NULL)
  {
    printf("Errore nell'aprire un file\n"); /* Scrive messaggio di errore */
    fclose(fa);
    fclose(fb);
    exit(0);                                /* Termina il programma       */
  }

/* Legge un numero da ciascun file
   I numeri sono memorizzati in "a" e "b" rispettivamente.
   "na" e "nb" valgono 1 se il numero corrispondente e' stato
   letto, 0 altrimenti.                                          */
 
    na = fscanf(fa,"%d",&a);
    nb = fscanf(fb,"%d",&b);

/* Inizia il ciclo principale */
  while ((na > 0) && (nb > 0))
  {
  
	 if (a <= b) {
      na = ciclo(fa,&a,fc);
    } else {
      nb = ciclo(fb,&b,fc);
    }
  }
/* Se e' finito il file "elencob.txt",
   esegue il ciclo su "elencoa.txt"     */
  while (na > 0)
    na = ciclo(fa, &a,fc);
/* Se e' finito il file "elencoa.txt",
   esegue il ciclo su "elencob.txt"     */
  while (nb > 0)
    nb = ciclo(fb, &b,fc);
  fclose(fa);
  fclose(fb);
  fclose(fc);
}


/* Funzione che scrive su video e sul file "*fo" il dato contenuto nella
   variabile "*x", legge un nuovo dato dal file "*f",
   ritorna il numero di dati letti 0 o 1)                */
int ciclo(FILE *f, int *x, FILE *fo)
{
  int n;
  printf("%d\n",*x);
  fprintf(fo,"%d\n",*x);
  n = fscanf(f,"%d",x);
  return n;
}


