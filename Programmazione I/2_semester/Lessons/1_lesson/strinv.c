//Scrivere un programma che acquisisce una stringa di al più 30 caratteri, la copia al rovescio in un secondo array e visualizza il risultato.

#include <stdio.h>
#define N 30
 
int main(){
 
	char s[N+1], s1[N+1];
	int i, n;
 	
	printf("Inserisci una stringa: ");
	scanf("%s", s);

	for(n=0;s[n]!='\0';n++);

	for(i=0;i<n;i++)
	s1[n-1-i]=s[i];
	s1[i]='\0';

	printf("Risultato: %s\n", s1);
}
