//Scrivere un programma che acquisisce una stringa di al più 30 caratteri e calcola e visualizza la sua lunghezza.

#include <stdio.h>
#define N 30

int main(){
	
	char s[N+1];
	int i;

	printf("Inserisci una stringa: ");
	scanf("%s", s);

	for(i=0;s[i]!='\0';i++);

	printf("La lunghezza della stringa è: %d\n", i);
}
