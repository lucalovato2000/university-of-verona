// Data una stringa str di 50 caratteri, scrivere un programma che genera una stringa s2 uguale alla prima eliminando le vocali. 


#include <stdio.h>
#define N 50

void main(){

	int i,j=0;
	
	char str[N+1], str2[N+1];

	printf("Inserisci una stringa di massimo 50 caratteri: ");
	scanf("%s", str);	
	
	for(i=0;str[i]!='\0';i++){
		if(str[i]!='a' && str[i]!='e' && str[i]!='i' && str[i]!='o' && str[i]!='u' && 
		   str[i]!='A' && str[i]!='E' && str[i]!='I' && str[i]!='O' && str[i]!='U'){
			str2[j]=str[i];
			j++;
		}
	}

	str2[j+1]='\0';
	printf("%s\n", str2);
}
