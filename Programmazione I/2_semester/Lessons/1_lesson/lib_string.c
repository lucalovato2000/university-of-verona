#include <stdio.h>
#include <string.h>


int main(){

    char str[]="ciao", str1[]="ciao", str2[]="cc", str3[]="lunghezza";
    int l, ug;

    printf("zero stringa: %s\n", str);
    printf("prima stringa: %s\n", str1);

    ug = strcmp(str, str1);  //CONFRONTA str in str2        restituisce 0 se le due stringhe sono uguali
    printf("%d", ug);
    if(ug==0)
        printf(" - Le stringhe risultano uguali\n");

    l = strlen(str3);    //LUNGHEZZA di str3
    printf("Lunghezza della stringa numero tre risulta: %d\n", l);


    strcpy(str, str2);  //COPIA str in str2
    printf("La seconda stringa viene copiata nella zero: %s\n", str);

    printf("La seconda stringa risulta: %s\n", str2);

    strcat(str2, str);  //CONCATENA str a str2
    printf("La stringa zero viene concatenata alla seconda: %s\n", str2);


    return 0;
}