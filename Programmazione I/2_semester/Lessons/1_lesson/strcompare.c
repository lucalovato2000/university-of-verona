// Scrivere un programma che acquisisce due stringhe di al più 30 caratteri e verifica se sono uguali visualizzando un apposito messaggio

#include <stdio.h>
#define N 30
 
int main(){
  
	char s1[N+1], s2[N+1];
	int i;

	printf("Inserisci una stringa: ");
	scanf("%s", s1);	//bisognerebbe forzare inserimento a massimo 29 valori %29s
	printf("Inserisci una stringa: ");
	scanf("%s", s2);
    
	for(i=0; s1[i]!='\0' && s2[i]!='\0' && s1[i]==s2[i]; i++);
  
	if(s1[i]=='\0' && s2[i]=='\0')
		printf("Le due stringhe sono uguali\n");
	else
		printf("Le due stringhe sono diverse\n");
}
