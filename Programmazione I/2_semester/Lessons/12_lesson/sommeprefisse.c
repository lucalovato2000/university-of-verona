/*
Realizzare il sottoprogramma somme_prefisse che accetta come parametro 
un array di interi a[] di lunghezza N  (e qualsiasi altro parametro ritenuto necessario);
si garantisce che si avrà sempre N >= 1. 
La funzione deve restituire un NUOVO array b[], sempre di lunghezza N, 
contenente le "somme prefisse" di a[].  
Specificamente, per ogni j (j=0,…,N-1)  i valori di  b[j] 
si ottengono dalla somma degli elementi a[0] +…+ a[j]. 
Ad esempio, se a[] = {1,2,1,2,1}, si otterrà b[] = {1,3,4,6,7}.
   
Si scrivano inoltre il prototipo del sottoprogramma, 
la chiamata nel main, e la dichiarazione di tutte le variabili ritenute utili.

(Bonus di 1 punto per la definizione ricorsiva)
*/

#include <stdio.h>
#define N 5

void somme_prefisse(int a[], int n, int b[])
{	// n ultimo indice 
	if (n == 0)
	{
		b[n] = a[0];
	}

	else
		{   // faccio la ricorsione sull'array con una pos in meno e poi aggiungo alla somma messa in b[n-1] a[n]
		somme_prefisse(a, n - 1, b);
		b[n] = a[n] + b[n - 1];
	}
}

int main()
{
	int a1[N];
	int b1[N] = {0};
	int i;
	printf("Inserire %d numeri interi:\n", N);
	for (i = 0; i < N; i++)
	{
		scanf("%d", &a1[i]);
	}
	somme_prefisse(a1, 5, b1);
	for (i = 0; i < N; i++)
	{
		printf("%3d", a1[i]);
	}
	printf("\n");
	for (i = 0; i < N; i++)
	{
		printf("%3d", b1[i]);
	}
	printf("\n");
	return 0;
}

/* versione iterativa
#include <stdio.h>
#include <stdlib.h>
#define N 5


void somme_prefisse(int a[],int b[], int n)
{
    int i;
    b[0] = a[0]; 
    for (i=1; i<n; i++) {
        b[i] = b[i-1] + a[i];  
    }
}


int main(){
int a1[N];
int b1[N]={0};
int i;
for (i=0; i<N; i++)
{scanf("%d",&a1[i]);}
 somme_prefisse(a1, b1,5);

for (i=0; i<N; i++)
{printf("%d",a1[i]);}
printf ("\n");
for (i=0; i<N; i++)
{printf("%d",b1[i]);}
printf ("\n");
return 0;
}
*/