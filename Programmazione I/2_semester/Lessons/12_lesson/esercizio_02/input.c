#include <stdio.h>
#include "input.h"

void chiedi_stringa( char desc[], char s[] ){
  printf( "Inserisci una stringa di massimo 128 caratteri per %s: ", desc );
  scanf( "%128s", s );
}


char chiedi_operazione(){
  char c;
  do{
    printf( "\n\nOperazioni possibili:\n" );
    printf( "a: aggiungi contatto alla rubrica\n" );
    printf( "l: elenca tutti i contatti presenti in rubrica\n" );
    printf( "f: trova un numero in rubrica\n" );
    printf( "e: esci\n" );  
    printf( "Operazione richiesta: " );
    scanf( " %c", &c );
  } while( c != 'a' && c != 'l' && c != 'f' && c != 'e' );
  return c;
}


