#include <stdio.h>
#include <stdbool.h>
#include "rubrica.h"
#include "input.h"

int main( void ){
  char choice;
  struct rubrica r;
  struct contatto c;
  char nome[129];
  
  r.num_contatti = 0;
  
  choice = chiedi_operazione();
  while( choice != 'e' ){    
    switch( choice ){
      case 'a':
        chiedi_stringa( "nome", c.nome );
		chiedi_stringa( "numero", c.numero );
		r = aggiungi_contatto( r, c );
        break;
      case 'l':
        elenca_contatti( r );
        break;
      case 'f':
        chiedi_stringa( "nome", nome );
		trova_numero( r, nome );
		break;
    }
    choice = chiedi_operazione();
  }

  return 0;
}
