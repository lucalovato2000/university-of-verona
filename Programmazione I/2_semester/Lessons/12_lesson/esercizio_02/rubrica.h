#include <stdio.h>
#include "stdbool.h"

struct contatto{
  char nome[129];
  char numero[129];
};

struct rubrica{
  struct contatto contatti[10];
  int num_contatti;
};

struct rubrica aggiungi_contatto( struct rubrica r, struct contatto c );
void elenca_contatti( struct rubrica r );
void trova_numero( struct rubrica r, char nome[] );
