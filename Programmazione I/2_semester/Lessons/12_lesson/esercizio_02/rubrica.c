#include <stdio.h>
#include <stdbool.h>
#include "rubrica.h"

struct rubrica aggiungi_contatto( struct rubrica r, struct contatto c ){
  if( r.num_contatti == 10 ){
    printf( "La rubrica e' piena\n" );
  } else {
    r.contatti[r.num_contatti] = c;
	r.num_contatti++;
	printf( "Contatto aggiunto\n" );
  }
  return r;
}

void elenca_contatti( struct rubrica r ){
  int i;
  if( r.num_contatti == 0 ){
    printf( "Rubrica vuota\n" );
  }
  for( i = 0; i < r.num_contatti; i++ ){
     printf( "%s: %s\n", r.contatti[i].nome, r.contatti[i].numero );
  }
}

bool equals( char s1[], char s2[] ){
  int j;
  bool equals = true;
  
  for( j = 0; s1[j] != '\0' && s2[j] != '\0' && equals == true; j++ ){
	if( s1[j] != s2[j] ){
	  equals = false;
	}
  }
  return equals;
}

void trova_numero( struct rubrica r, char nome[] ){
  int i;
  bool found;
  
  for( i = 0; i < r.num_contatti && found == false; i++ ){
	found = equals( r.contatti[i].nome, nome );
    if( found == true ){
	  printf( "Il numero di %s e' %s\n", nome, r.contatti[i].numero ); 
	  return;
	}
  }
  printf( "Nome %s non presente in rubrica\n", nome );
}




