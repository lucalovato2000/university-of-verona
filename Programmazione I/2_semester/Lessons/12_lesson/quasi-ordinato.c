#include <stdio.h>
#include <stdlib.h>

/*
 * Diciamo che un array a[] e' "quasi ordinato" se tutti i valori
 * negativi (se esistono) precedono i valori maggiori o uguali di zero
 * (se esistono); si noti che i valori negativi possono comparire in
 * ordine qualsiasi, come pure i valori maggiori o uguali di zero. Ad
 * esempio l'array a[] = {-1, -7, 8, 10, 8, 1} e' quasi ordinato, come
 * pure l'array a[] = {-9, -7, -1, -3} e a[] = {13, 9, 0, 21, 33},
 * mentre l'array a[] = {3, -1, 4, 5} (compare un valore >= 0 prima di
 * uno negativo). Si faccia riferimento ai test per ulteriori esempi.
 * Scrivere la funzione quasi_ordinato(a, n) che, dato in input un array
 * a[] non vuoto e la sua lunghezza n, ritorna 1 se e solo se a[] e'
 * quasi ordinato, 0 altrimenti.
 */
int quasi_ordinato( int a[], int n ) 
{
    int i;
    /* qui conviene ragionare al contrario: in base alla definizione
       data, un array NON e' quasi ordinato se esiste un numero
       positivo che compare prima di un numero negativo */
    if ( n == 1 ) {
        return 1; /* un array con un solo elemento e' quasi-ordinato per definizione */
    } else {
        for (i=1; i<n; i++) {
            if ( a[i-1] >= 0 && a[i] < 0 ) {
                return 0;
            }
        }
        return 1;
    }
}

void test(int a[], int n, int expected) 
{
    int r = quasi_ordinato(a, n);
    int i;

    if ( r == expected ) {
        printf("Test OK\n");
    } else {
        printf("Test FALLITO sull'array: ");
        for (i=0; i<n; i++) {
            printf("%d ", a[i]);
        }
        printf(" -- Risultato atteso %d, risultato ottenuto %d\n", expected, r);
    }
}

int main( void )
{
    int a1[] = {-1, -3, 10, 7, 13, 10};
    int a2[] = {-9, 0, 0, 0, 10};
    int a3[] = {-9, 0};
    int a4[] = {-1};
    int a5[] = {5};
    int a6[] = {-1, -3, 10, -2};
    int a7[] = {0, 9, 0, 9, -1};
    int a8[] = {3, 5, 1, -1, 2, 1};
    int a9[] = {3, -3};

    test(a1, sizeof(a1)/sizeof(int), 1);
    test(a2, sizeof(a2)/sizeof(int), 1);
    test(a3, sizeof(a3)/sizeof(int), 1);
    test(a4, sizeof(a4)/sizeof(int), 1);
    test(a5, sizeof(a5)/sizeof(int), 1);
    test(a6, sizeof(a6)/sizeof(int), 0);
    test(a7, sizeof(a7)/sizeof(int), 0);
    test(a8, sizeof(a8)/sizeof(int), 0);
    test(a9, sizeof(a9)/sizeof(int), 0);

    return 0;
}
