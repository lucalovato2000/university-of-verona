#include <stdio.h>
#include "operations.h"
#include "input.h"

int main( void ){
  char c;
  int a, b, r;
  
  c = chiedi_operazione();

  while( c != 'e' ){   
    a = chiedi_numero();
    b = chiedi_numero();
    
    switch( c ){
      case '+':
        r = sum( a, b );
        break;
      case '-':
        r = minus( a, b );
        break;
      case '*':
        r = mult( a, b );
        break;
      case '/':
        r = div( a, b );
        break;
      case '%':
        r = mod( a, b );
        break;
    }

    printf( "%i %c %i = %i\n\n", a, c, b, r );
    c = chiedi_operazione();
  }

  return 0;
}
