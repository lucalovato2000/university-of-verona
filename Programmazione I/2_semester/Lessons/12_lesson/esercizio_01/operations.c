#include <stdio.h>
#include "operations.h"

int sum( int a, int b ){
  return a + b;
}

int minus( int a, int b ){
  return a - b;
}

int mult( int a, int b ){
  return a * b;
}

int div( int a, int b ){
  return a / b;
}

int mod( int a, int b ){
  return a % b;
}
