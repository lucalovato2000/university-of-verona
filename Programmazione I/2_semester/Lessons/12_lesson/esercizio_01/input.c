#include <stdio.h>
#include "input.h"

int chiedi_numero(){
  int n;
  printf( "Inserisci un numero intero: " );
  scanf( "%d", &n );
  return n;
}


char chiedi_operazione(){
  char c;
  do{
    printf( "Operazioni possibili:\n" );
    printf( "+: somma tra interi\n" );
    printf( "-: differenza tra interi\n" );
    printf( "*: moltiplicazione tra interi\n" );
    printf( "/: divisione tra interi\n" );
    printf( "%%: modulo tra interi\n" );
    printf( "e: esci\n" );  
    printf( "Operazione richiesta: " );
    scanf( " %c", &c );
  } while( c != '+' && c != '-' && c != '*' && c != '/' && c != '%' && c != 'e' );
  return c;
}


