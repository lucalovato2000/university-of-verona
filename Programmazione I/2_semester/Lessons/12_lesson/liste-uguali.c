#include <stdio.h>
#include <stdlib.h>

struct list {
    int val;
    struct list *next;
};

/* Dichiarazione di alcune delle funzioni viste nella precedente
   esercitazione sulle liste; le funzioni sono definite sotto
   (non devono essere implementate), e possono essere usate se
   serve. */
   
struct list *list_create(int v, struct list *t);
int list_length(struct list *L);
void list_destroy(struct list *L);
int is_empty(struct list *L);
void list_print(struct list *L);
struct list *list_from_array(int v[], int n);

/* Realizzare la funzione liste_uguali(L1, L2) seguente.  La funzione
   accetta in input due puntatori a liste, che possono anche essere
   vuote. La funzione deve restituire 1 se e solo se le liste hanno lo
   stesso numero di nodi, e l'i-esimo nodo della lista L1 contiene lo
   stesso valore (campo val) dell'i-esimo nodo della lista L2. Si
   faccia riferimento ai test per alcuni esempi */
int list_equal(struct list *L1, struct list *L2)
{
    /* versione ricorsiva */
    if ( is_empty(L1) || is_empty(L2) ) {
        /* Se una delle due liste e' vuota, allora la funzione ritorna
           1 se e solo se entrambe le liste sono vuote; se cosi' non
           fosse, una lista sarebbe vuota e l'altra no, quindi non
           possono avere lo stesso contenuto. */
        return (is_empty(L1) && is_empty(L2));
    } else {
        if (L1->val == L2->val) {
            return list_equal(L1->next, L2->next);
        } else {
            return 0;
        }
    }
}

/**
 ** Le funzioni seguenti sono gia' implementate 
 **/

/* Crea (mediante malloc() ) e restituisce un puntatore ad un nuovo
   nodo di una lista; il nodo contiene il valore v e punta a t come
   elemento successivo. Il chiamante e' responsabile per deallocare
   mediante free() o simili la zona di memoria restituita da questa
   funzione, quando non piu' utilizzata */
   
struct list  *list_create(int v, struct list *t)
{
    struct list *r=(struct list *)malloc(sizeof(struct list));
    r->val = v;
    r->next = t;
    return r;
}

/* Restituisce la lunghezza (numero di nodi) della lista L; se L e' la
   lista vuota, restituisce 0 */
int list_length(struct list *L)
{
    /* Versione ricorsiva */
    if ( L == NULL ) {
        return 0;
    } else {
        return (1 + list_length(L->next));  //incrementa
    }
}

/* Libera la memoria occupata da tutti i nodi della lista L */
void list_destroy(struct list *L)
{
    /* versione ricorsiva */
    if ( L != NULL ) {
        list_destroy(L->next);
        L->next = NULL; /* non necessario... */
        free(L);
    }
}

/* Restituisce 1 se e solo se L e' la lista vuota, 0 altrimenti */
int is_empty(struct list *L)
{
    return (L==NULL);
}

/* Stampa il contenuto della lista L */
void list_print(struct list *L)
{
    printf("(");
    while (L != NULL) {
        printf("%d ", L->val);
        L = L->next;
    }
    printf(")");
}

/* Crea e restituisce il puntatore ad una nuova lista con n nodi, in
   cui il nodo i-esimo contiene il valore v[i] (il nodo 0 è il primo
   nodo della lista) */
struct list *list_from_array(int v[], int n)
{
    /* versione ricorsiva */
    if ( 0 == n ) {
        return NULL;
    } else {
        return list_create( v[0], list_from_array(v+1, n-1));
    }
}

void test(struct list *L1, struct list *L2, int expect)
{
    int r = list_equal(L1, L2);
    if ( r == expect ) {
        printf("Test OK\n");
    } else {
        printf("Test FALLITO sulle liste: ");
        list_print(L1);
        printf(", ");
        list_print(L2);
        printf(" -- Valore restituito %d, valore atteso %d\n", r, expect);
    }
}

int main( void )
{
    struct list *L1 = list_create(1, list_create(2, list_create(-3, list_create(13, list_create(1, NULL)))));
    struct list *L2 = list_create(1, list_create(2, list_create(-3, list_create(13, list_create(1, NULL)))));
    struct list *L3 = NULL;
    struct list *L4 = list_create(13, NULL);
    struct list *L5 = list_create(13, list_create(99, NULL));
    struct list *L6 = list_create(1, list_create(2, list_create(7, list_create(13, list_create(1, NULL)))));

    test(L1, L2, 1);
    test(L1, L3, 0);
    test(L3, L3, 1);
    test(L1, L4, 0);
    test(L4, L5, 0);
    test(L1, L6, 0);

    list_destroy(L1);
    list_destroy(L2);
    list_destroy(L3);
    list_destroy(L4);
    list_destroy(L5);
    list_destroy(L6);
    return 0;
}
