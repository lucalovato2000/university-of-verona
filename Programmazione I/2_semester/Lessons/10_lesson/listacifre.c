/*
Scrivere un sottoprogramma int2list che ricevuto in ingresso un numero intero restituisce una lista in cui ogni cifra del numero in ingresso compare tante volte quanto il suo valore. Nel caso in cui il valore ricevuto in ingresso sia negativo, il sottoprogramma restituisce la lista creata a partire dalla cifre in ordine opposto. Se per esempio il sottoprogramma riceve in ingresso l’intero 3204, il sottoprogramma restituisce la lista seguente: 3 → 3 → 3 → 2 → 2 → 4 → 4 → 4 → 4 
Se il sottoprogramma riceve in ingresso l’intero -3204, il sottoprogramma restituisce la lista seguente: 4 → 4 → 4 → 4 → 2 → 2 → 3 → 3 → 3 
Definire un tipo di dato opportuno per la lista. Scrivere un programma che acquisisce da riga di comando il valore intero e chiama il sottoprogramma int2list
*/


#include<stdio.h>
#include<stdlib.h>

typedef struct elemento{ 
	int dato1;  
	struct elemento *next;
}elemento; 


elemento *inserisciInCoda(elemento *lista, int num1){
  elemento *prec;
  elemento *tmp;

  tmp = (elemento*) malloc(sizeof(elemento));	//alloco dinamicamente solo tmp
  if(tmp != NULL){	//finche' tmp non e' vuoto
    tmp->next = NULL;	//il valore prossimo di tml e' nullo
    tmp->dato1 = num1;	//il dato di tmp e' il nuovo numero
    
	if(lista == NULL)	//finche la lista non e' nulla
      lista = tmp;	//lista corrisponde a tmp (in questo modo aggiugno il nodo tmp in fondo)
    
	else{
      for(prec=lista;prec->next!=NULL;prec=prec->next);	//raggiungi termine della lista
      prec->next=tmp;	//vai a avanti nella lista
    }
  }else
      printf("Memoria esaurita!\n");	//messaggio di errore
  
  return lista;
}
 
//funzione che visvualizza la lista
void visualizza(elemento *lista){

  while(lista != NULL){	//finche' la lista è vuota
    printf("%d ", lista->dato1);	//stampa il numero corrispettivo
    lista=lista->next;	//avanza nei nodi della lista
  }
  printf("\n");  
}

 
elemento* inserisciInTesta(elemento* lista, int num){
  elemento* tmp;

  tmp = (elemento*) malloc(sizeof(elemento));	//alloco dinamicamente lista tmp
  if(tmp != NULL){
    tmp->dato1 = num;	//assegno a tmp il nuovo valore num
    tmp->next = lista;	//assegno al nodo successivo lista
    lista = tmp;    //assegno a lista il valore di tmp
  } else
      printf("Memoria esaurita!\n");
  return lista;
}


int main(){
	
	elemento *testa=NULL;	//definisco la lista vuota
	int i,num;

	printf("Dammi un numero ");
	scanf("%d",&num);

	if(num>0){
		
		while(num!=0){	
			i=num%10;
			
			while(i>0){				
				testa=inserisciInTesta(testa,num%10);	//ritorna una lista
				i--;
			}
			num/=10;
		}
	}
		if(num<0){   
			num=-num;
			
			while(num!=0){	
				i=num%10;
				
				while(i>0){				
					testa=inserisciInCoda(testa,num%10);	//ritorna una lista
					i--;
				}
				num/=10;
			}
	    }
	visualizza(testa);	//void
}
