#include <stdio.h>
#include <stdlib.h>

/*definizione della struttura per la lista concatenata*/
struct nodo{ 
		int valore; 
		struct nodo *next; 
	}; 

typedef struct nodo elem;


/*inserisce un nuovo numero in testa alla lista*/
elem* inserisciInTesta(elem* lista, int num){
  elem *tmp;

  tmp = (elem*) malloc(sizeof(elem));
  if(tmp != NULL){
    tmp->valore = num;
    tmp->next = lista;
    lista = tmp;    
  } else
      printf("Memoria esaurita!\n");
  return lista;
}

/*visualizza i numeri contenuti nella lista*/
void visualizza(elem* lista){
  while(lista != NULL){
    printf("%d ", lista->valore);
    lista = lista->next;
  }
  printf("\n");  
}


elem* cancellatutti(elem* lista, int num){
  elem *curr, *prec, *canc;
  
  curr = lista;
  prec = NULL;      
  while(curr){
    if(curr->valore==num){
      canc = curr;
      curr = curr->next;     // curr punta al successivo del nodo da cancellare
      if(prec!=NULL)
        prec->next = curr;  // il precedente del nodo da cancellare deve avere come next il nodo successivo a quello da cancellare
      else
        lista = curr;
      free(canc); // importante altrimenti genero leak
    }
    else{
      prec=curr;
      curr = curr->next;     
    }
  }
  return lista;

}


int main(){
  int val;
  int r;
  elem* listav=NULL; 
  
  do
  {
	scanf("%d", &val);
    if(val!=-1)
    listav=inserisciInTesta(listav, val);
  }while(val!=-1);
  
  visualizza(listav);
  printf("Quale valore vuoi cancellare?:");
  scanf("%d",&r);
  
  listav=cancellatutti(listav, r);
  printf("\n Nuova lista: \n");
  visualizza(listav);

  return 0;
}
