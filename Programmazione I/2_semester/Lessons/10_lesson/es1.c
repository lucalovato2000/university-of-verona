#include <stdio.h>
#include <stdlib.h>

/*definizione della struttura per la lista concatenata*/
struct nodo{ 
		int valore; 
		struct nodo *next; 
	}; 

typedef struct nodo elem;


/*inserisce un nuovo numero in testa alla lista*/
elem* inserisciInTesta(elem* lista, int num){
  elem *tmp;

  tmp = (elem*) malloc(sizeof(elem));
  if(tmp != NULL){
    tmp->valore = num;
    tmp->next = lista;
    lista = tmp;    
  }else
      printf("Memoria esaurita!\n");
  return lista;
}

/*visualizza i numeri contenuti nella lista*/
void visualizza(elem *lista){
  while(lista != NULL){
    printf("%d ", lista->valore);
    lista = lista->next;
  }
  printf("\n");  
}


int sommanodi(elem* lista){
	int x=0;
	
	while(lista!=NULL){
		x+=lista->valore;
		lista=lista->next;
	}
	return x;
}


int main(){
  
  int val;
  int r;
  elem *listav=NULL; 
  
  do{

	scanf("%d", &val);
  
  if(val!=-1)
    listav=inserisciInTesta(listav, val);
  }while(val!=-1);
  
  visualizza(listav);
  r=sommanodi(listav);
  printf("Somma nodi %d\n",r);
  
  printf("\nInserimento in testa della somma \n");
  listav=inserisciInTesta(listav, r);
  visualizza(listav);

  return 0;
}
