#include <stdio.h>
#include <stdlib.h>

typedef struct{
    int num;
    struct elem *next;
}elem;


//funzione inserisciintesta 
elem * inserisciintesta(elem *lista, int numero){
    elem *tmp;
    tmp=(elem *)malloc(sizeof(elem));

    if(tmp!=NULL){
        tmp -> num=numero;
        tmp -> next=lista;
        lista=tmp;
    }else
        printf("Allocazione non riuscita");
    
    return lista;
}

//funzione inserisciordinato 
elem* inserisciOrdinato(elem* lista, int num){
  elem *tmp, *prec, *prox;

  tmp = (elem*) malloc(sizeof(elem));
  if(tmp != NULL){
    tmp->num = num;
    if(!lista){ /* lista vuota */
    	tmp->next = lista;
	    lista = tmp;    
    } else { 
    	if(tmp->num <= lista->num){ /* nuovo primo elemento della lista */
    		tmp->next = lista;
    		lista = tmp;
    	} else if (!lista->next) {
    		tmp->next = lista->next;
    		lista->next = tmp;
    	} else {
    		for(prox = lista; prox->next && tmp->num > prox->next->num; prox = prox->next)
	    		;
    		tmp->next = prox->next;
    		prox->next = tmp;
    	}
    }
  } else
      printf("Memoria esaurita!\n");
  return lista;
}

//vedere se esiste un elemento in una lista
int esisteelemento(elem *lista, int numero){
    
    int flag=0;

    while(lista!=NULL && flag==0){
        if(lista->num==numero)
            flag=1;
        lista=lista->next;
    }
}

//distruggere una lista intera
elem * distruggi(elem *lista){
    elem *tmp;

    while(lista!=NULL){
        tmp=lista;
        lista=lista->next;
        free(tmp);
    }
    return NULL;
}

//togliere elemento in una lista
elem *rimuovi(elem *lista, int numero){

    int found;
    elem *corr, *prec, *conc;

    found=0;
    corr=lista;
    prec=NULL;

    while(corr && !found){
        if(corr->num==numero){
            found=1;
            conc=corr;
            corr=corr->next;
            if(prec!=NULL)
                prec->next=corr;
            else   
                lista=corr;
            
            free(conc); //libera conc
        }else{
            prec=corr;
            corr=corr->next;
        }
    }
    return(lista);
}
