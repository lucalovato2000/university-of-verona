#include <stdio.h>
#include <stdlib.h>

/*definizione della struttura per la lista concatenata*/
struct nodo{ 
		int valore; 
		struct nodo *next; 
	}; 

typedef struct nodo elem;


/*inserisce un nuovo numero in testa alla lista*/
elem* inserisciInTesta(elem* lista, int num){
  elem *tmp;

  tmp = (elem*) malloc(sizeof(elem));
  if(tmp != NULL){
    tmp->valore = num;
    tmp->next = lista;
    lista = tmp;    
  } else
      printf("Memoria esaurita!\n");
  return lista;
}

/*visualizza i numeri contenuti nella lista*/
void visualizza(elem* lista){
  while(lista != NULL){
    printf("%d ", lista->valore);
    lista = lista->next;
  }
  printf("\n");  
}


int prodottor(elem* lista){
	if(lista==NULL)
		return 1;
    
	else 
    return lista->valore*prodottor(lista->next);
}

int prodottoi(elem* lista){
	int p=1;
	while(lista!=NULL)
	{
		p=p*lista->valore;
		lista=lista->next;
	}
	return p;
}



int main(){
  int val;
  int r;
  elem* listav=NULL; 
  
  do{
	scanf("%d", &val);
    if(val!=-1)
    listav=inserisciInTesta(listav, val);
  }while(val!=-1);
  
  visualizza(listav);
  r=prodottor(listav);
  printf("\nProdotto ottenuto ricorsivamente: %d\n",r);
  r=prodottoi(listav);
  printf("Prodotto ottenuto iterativamente: %d\n",r);
  
  return 0;
}
