/*
Inserimento in testa e in coda di una lista
*/

#include <stdio.h>
#include <stdlib.h>

struct elem_{       //elem_ e' un tipo

    int num;
    struct elem_ *next;
};

typedef struct elem_ elem;


/*
------oppure----------------
typedef struct elem_{       //elem_ e' un tipo

    int num;
    struct elem_ *next;
}elem;
------------------------------
*/

elem* inseriincoda(elem *lista, int numero){
    elem *prec;
    elem *tmp;

    tmp=(elem *)malloc(sizeof(elem));   //alloca dinamicamente nuovo nodo per la lista
    if(tmp==NULL){  //se tmp e' nullo
        tmp->next=NULL; //prossimo valore di tmp nullo
        tmp->num=numero;    //valore di tmp numero ricevuto
    }
    if(lista==NULL){    //se lista nulla associa alla lista il nodo tmp
        lista=tmp;
    }else{
        for(prec=lista;prec->next!=NULL;prec=prec->next); //corpo nullo deve solo scorrere
        prec->next-prec;
    }
}

//funzione per visualizzare la lista
void visualizza(elem *lista){
    while(lista!=NULL){
        printf("%3d", lista->num);
        lista=lista->next;
    }
}

int main(){

    int val;
    elem *lista=NULL;   //inizializzo la lista vuota

    do{
        scanf("%d", &val);
        if(val!=-1);
            lista=inseriincoda(lista, val); //inserimento in coda degli elementi
    }while(val!=-1);

    visualizza(lista);
}
