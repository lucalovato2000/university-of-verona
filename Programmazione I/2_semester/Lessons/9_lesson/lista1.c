/* Si scriva una funzione C che ricevendo in ingresso un puntatore alla lista modifichi la stessa, memorizzando nell’ultimo nodo 
	la somma dei valori di tutti i nodi presenti nella lista.  */

#include <stdio.h>
#include <stdlib.h>

/*definizione della struttura per la lista concatenata*/

  typedef struct nodo{ 
		int valore; 
		struct nodo *next; 
	}elem; 



/*inserisce un nuovo numero in coda alla lista*/
elem* inserisciInCoda(elem* lista, int num){
  elem *prec;
  elem *tmp;

  tmp = (elem*) malloc(sizeof(elem));   //alloca dinamicamente una lista tmp
  if(tmp != NULL){  //se la lista non e' vuota
    tmp->next = NULL; //assegna il prossimo nodo come nullo
    tmp->valore = num;  //assegna al prossimo valore il numero nuovo
    
    if(lista == NULL) 
      lista = tmp;  //aggiungo quando la lista e' vuota il nodo tmp
    else{
      /*raggiungi il termine della lista*/
      for(prec=lista;prec->next!=NULL;prec=prec->next);
      prec->next = tmp; //aumenta il nodo prec avanzando
    }
  }else
      printf("Memoria esaurita!\n");
  return lista;
}

//funzione per stampare la lista
void visualizza(elem* lista){
  while(lista!=NULL){
    printf("%d ", lista->valore);
    lista = lista->next;  //assegna a lista il nodo successivo
  }
  printf("\n");  
}

//funzione per aggiungere la somma dei nodi in fondo
void aggiungisommanodi(elem* lista){
	int somma=0;
	elem *current, *tmp;
	
	current=lista;
	while(current->next!=NULL){	  //finche' la lista non e' vuota
		somma+=current->valore;   //salva la somma dei nodi nel valore
		current=current->next;  //passa al nodo successivo	
	}
    somma+=current->valore; //esco con current che punta all'ultimo nodo (non ha successivo) devo sommarlo e poi agganciare il nuovo nodo
	
	tmp = (elem*) malloc(sizeof(elem)); //alloca dinamicamente un nodo tmp nuovo
    if(tmp != NULL){  //finche' tmp e' diverso da vuoto
      tmp->next = NULL;   //prossimo nodo di tmp e' nullo
      tmp->valore = somma;  //valore di tmp e' la somma
	
	  current->next=tmp;  //current e' tmp
	}
}


int main(){
  
  int val;
  elem* listav=NULL; 
  
  do{
	  scanf("%d", &val);
    if(val!=-1)   //valore per uscire dall'inserimento della lista
      listav=inserisciInCoda(listav, val);
  }while(val!=-1);
  
  visualizza(listav);
  aggiungisommanodi(listav);
  
  visualizza(listav);
  
  return 0;
}
