/*
Scrivere un programma che permette di aggiungere un elemento in coda al vettore
*/

#include <stdio.h>
#include <stdlib.h>

int * modificaVettore(int *punt_vettore, int n, int elem){
 
  int i;

  punt_vettore = (int *) realloc (punt_vettore, (n+1) * sizeof(int));
  for (i = n-1; punt_vettore[i] > elem && i>=0; i--)
    punt_vettore[i+1] = punt_vettore[i];
  punt_vettore[i+1] = elem;     //inserisco elem nella posizione successiva all'elemento piu' piccolo di lui, oppure in posizione 0
  
  return punt_vettore;
}


int main(){ 
  
  int n, i, e, *v;
  
  printf("Dimensione del vettore : ");
  scanf("%d",&n);
  
  v = (int*) malloc(n * sizeof(int));   //assegna memoria dinamica al vettore in base al numero inserito
  printf("Inserisci gli %d elementi : ", n);
  
  for (i = 0; i < n; i++)
    scanf("%d", &v[i]);   //equivalenza puntatori array
  
  printf("Elemento da inserire nel vettore : ");
  scanf("%d",&e);
  
  v=modificaVettore(v,n,e);
  
  printf("Vettore modificato ");
  
  for (i = 0; i < n+1; i++){
    printf("%d ",v[i]);
  }
  printf("\n");
}
