#include <stdio.h>
#include <stdlib.h>

typedef struct { 
	char nome[21]; 
	int eta;
}t_stud;


t_stud * Inserisci(int n){
  
  int i;
  
  t_stud *classe;
  classe = (t_stud *) malloc (n* sizeof(t_stud)); //alloca memoria dinamica in base al numero scelto dall'utente
  
  if(classe!=NULL)
	  for (i=0; i<n; i++){
      printf("\n Nome: ");
      scanf("%s",classe[i].nome);
      printf("\n Eta': ");
      scanf("%d",&classe[i].eta);
    }
  return classe;
}

int main(){ 
  
  int i,n;
  
  t_stud *array;
  printf("Dimensione del vettore : ");
  scanf("%d",&n);
  
  array=Inserisci(n); //inserisci alunni nella classe
  
  for (i = 0; i < n; i++){  //stampa alunni nella classe
    printf("\n Alunno %d : %s, %d",i+1,array[i].nome, array[i].eta);
  }
  printf("\n");
}
