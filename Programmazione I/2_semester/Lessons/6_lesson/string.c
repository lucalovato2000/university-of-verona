#include <stdio.h>
#include <string.h>

int trova(char s[61], char c){
    if(*s=='\0')
        return 0;
    if(*s==c)
        return 1+trova(s+1,c);
    else
        return trova(s+1,c);
}

int main(){

    char s[61], c;
    char temp;
    int l,r;

    printf("Inserisci una stringa: ");
    scanf("%s", s);     //non necessita &
    scanf("%c", &temp);
    printf("Inserisci il carattere da trovare: ");
    scanf("%c", &c);

    r=trova(s, c);
    printf("Ci sono %d occorrenze\n", r);
}
