/*
Programma che calcola in maniera ricorsiva il quoziente fra due numeri
*/

#include <stdio.h>

int quoziente(int n1, int n2){
    int n3;
    n3=n1-n2;
   
    if(n1<n2)
        return 0;
    else
        return 1+quoziente(n3,n2);
}

int leggi(int n){
    
    do{
        printf("Inserisci un numero positivo: ");
        scanf("%d", &n);
    }while(n<=0);
    
    return(n);
}

int main(){

    int n1,n2,n3,ris;

    n1=leggi(n1);
    printf("Primo numero inserito: %d\n", n1);
    n2=leggi(n2);
    printf("Secondo numero inserito: %d\n", n2);
    
    n3=n1-n2;
    ris=quoziente(n1,n2);
    printf("Risultato: %d\n", ris);
}
