#include <stdio.h>

int fib(int n){
    if(n==0 || n==1)
        return 1;
    else 
        return fib(n-1)+fib(n-2);
}

int main(){
    int a;
    int ris;

    printf("Inserisci un valore intero: ");
    scanf("%d", &a);

    ris=fib(a);
    printf("Il valore risulta: %d\n", ris);
}
