/*
Programma che calcola la somma ricorsiva da 0 a n 
*/

#include <stdio.h>

int sommaric(int n){
    if(n==1)
        return 1;
    return n+sommaric(n-1);
}

int main(){

    int n;
    int ris;

    do{
        printf("Inserisci un numero positivo: ");
        scanf("%d", &n);
    }while(n<0);

    ris=sommaric(n);
    printf("La somma risulta: %d\n", ris);
}
