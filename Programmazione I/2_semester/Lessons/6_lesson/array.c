/*

Controllare se in un array risulta presenta il numero inserito dall'utente

*/

#include <stdio.h>
#define N 10

int presente(int a[N], int dim, int pos, int n){
    if(pos==dim)
        return 0;
    if(a[pos]==n)
        return 1;
    else
        return presente(a, dim, pos+1, n);
}

int main(){

    int a[N]={1,2,3,1,4,5,6,0,1,0};
    int n,r;

    printf("Inserisci un numero a scelta: ");
    scanf("%d", &n);
    r=presente(a,N,0,n);

    if(r)   //if r==1
        printf("Il numero %d e' presente\n", n);
    else
        printf("Il numero %d non e' presente\n", n);
}
