#include <stdio.h>

int time(int n){
    if(n==0)
        return 0;
    else if(n==1)
        return 1;
    else
        return 2*time(n-2)+5;
}

int main(){

    int n,r;

    printf("Inserisci un indice: ");
    scanf("%d", &n);

    r=time(n);

    printf("Il numero calcolato all'indice %d risulta: %d\n",n, r);
}
