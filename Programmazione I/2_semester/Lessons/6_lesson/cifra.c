/*

Programma che restituisce la k-esima cifra di num partendo la destra

*/

#include <stdio.h>

int cifra(int num, int k){
    if(k<1)
        return(-1);
    if(k==1)    //caso base
        return(num%10);
    if(k>1 && num/10==0)
        return(-1);
    else        //passo ricorsivo
        return(cifra(num/10, k-1)); 
}

int main(){
    
    int num, k, ris;

    printf("Inserisci un numero: ");
    scanf("%d", &num);
    printf("Inserisci la cifra k: ");
    scanf("%d", &k);
    
    ris=cifra(num, k);

    printf("La %d-esima cifra di %d risulta: %d\n", k, num, ris);
}
