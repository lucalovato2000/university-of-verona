/*
Programma che inverte il numero inserito sia in maniera iterativa sia in maniera ricorsiva
*/
#include <stdio.h>

void inverti(int num){
    
    do{
        printf("%d", num%10);
        num/=10;
    }while(num!=0);
}

void invertiric(int n){
    printf("%d", n%10);
    if(n/10)
        invertiric(n/10);
}

int main(){
    
    int n;

    scanf("%d", &n);

    inverti(n);
    printf("\n");

    invertiric(n);
    printf("\n");
}
