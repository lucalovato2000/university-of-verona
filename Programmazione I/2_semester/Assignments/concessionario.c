/*
-------------------------------

Date le seguenti strutture dati che rappresentano un concessionario con il relativo gestore e le auto vendute,
realizzare una funzione che riceve come parametro un array di tipo t_concessionario (e la sua dimensione).

Per ogni concessionario la funzione stampa a video modello, targa e mese (in numero) delle auto immatricolate nel 2015. 
Il report mostrato dovrà avere il seguente formato:

Codice concessionario 0, gestore: Paolo Rossi
Immatricolazioni 2015:
* mese 9: Punto, MI80980
* mese 6: Marea, TO12567

Infine scrivere uno stralcio di main() in cui va SOLO dichiarato un array di 10 elementi di tipo t_concessionario ed eseguita la chiamata alla funzione precedentemente definita.

-------------------------------
*/

#include <stdio.h>
#define N_CONCESSIONARI 10
#define MAX_STR 30
#define MAX_TRG 7
#define MAX_AUTO_CONCE 50

//struttura auto
typedef struct{

  char modello[MAX_STR+1], targa[MAX_TRG+1];
  int meseImmatricolazione, annoImmatricolazione;
}t_auto;

//struttura gestore
typedef struct{

  char piva[MAX_STR+1];
  char nome[MAX_STR+1], cognome[MAX_STR+1];
}t_persona;

//struttura concessionario
typedef struct{
    
  int codiceConcessionario;
  t_persona gestore;
  t_auto Auto[MAX_AUTO_CONCE];
  int nAuto;        //numero effettivo auto nel concessionario 
}t_concessionario;


//funzione che stampa il concessionario e le sue caratteristiche
void stampaconcessionario(t_concessionario *concessionari, int effconc){
    
    int i,j;

    for(i=0;i<effconc;i++){  //scorre le concessionarie
        printf("Codice concessionario: %d,", concessionari[i].codiceConcessionario);    //stampa codice concessionaria
        printf(" gestore: %s %s\n", concessionari[i].gestore.nome, concessionari[i].gestore.cognome);     //stampa gestore concessionaria
        printf("Immatricolazioni 2015:\n"); 
        
        for(j=0;j<10;j++){  //scorre auto
            if(concessionari[i].Auto[j].annoImmatricolazione==2015){    //se il concessionario possiede auto immatricolate nell'anno 2015
                //stampa caratteristiche auto
                printf("* mese: %d %s %s\n", concessionari[i].Auto[j].meseImmatricolazione, concessionari[i].Auto[j].modello ,concessionari[i].Auto[j].targa);       
            }
        }printf("\n\n");
    }
}

//istanzia concessionari e relative automobili
int main(){
    
    int effconc=10; //numero effettivo di concessionari

    t_concessionario concessionari[N_CONCESSIONARI]={
        {  
            0,
            {"03320900404", "Nephele", "Susanna"},  
            {
                {"RENAULT Clio V", "FA349MN", 2, 2002},
                {"RENAULT Megane", "FR432YO", 9, 2017},
                {"RENAULT Zoe", "GA239TR", 5, 2015}, 
                {"RENAULT Twingo", "EX638TW", 7, 2018}, 
                {"RENAULT Capture", "ER951OL", 4, 2015}, 
            },
            5,
        },
        {
            1,
            {"03320900404", "Aghil", "Jianhong"},
            {
                {"WOLKSWAGEN Up!", "EF160NX", 1, 2008},
                {"WOLKSWAGEN Polo", "AS124DF", 9, 2010},
                {"WOLKSWAGEN Golf", "CW456YU", 6, 2012}, 
                {"WOLKSWAGEN T-Roc", "ER456OK", 1, 2015}, 
                {"WOLKSWAGEN T-Cross", "CT534OU", 6, 2016}, 
            },
            5,
        },
        {
            2,
            {"03320900404", "Hana", "Vladislava"},
            {
                {"HONDA HR-V", "FY870OR", 7, 2015},
                {"HONDA CR-V", "BW245OH", 9, 2017},
                {"HONDA JAZZ", "CV198PO", 4, 2015}, 
                {"HONDA CIVIC", "DE345YU", 4, 2018}, 
                {"HONDA NSX", "DW21795", 2, 2015}, 
            },
            5,
        },
        {
            3,
            {"03320900404", "Shoshannah", "Maxene"},
            {
                {"LOTUS Elise", "CS213TY", 1, 2008},
                {"LOTUS Exige Coupè", "CF456IO", 9, 2008},
                {"LOTUS Exige Roadster", "DT543OK", 3, 2015}, 
                {"LOTUS Evora", "FR230TT", 7, 2009}, 
                {"LOTUS Evija", "DA592TY", 6, 2012}, 
            },
            5,
        },
        {
            4,
            {"03320900404", "Esther", "Tewodros"},
            {
                {"CHEVROLET Camaro Cabrio", "DG683RT", 1, 2001},
                {"CHEVROLET Corvette Z06", "DI519OP", 9, 1999},
                {"CHEVROLET Stingray Coupé", "DB482YT", 8, 2015}, 
                {"CHEVROLET Capiva", "CR692KK", 2, 1985}, 
                {"CHEVROLET Volt", "CU043VV", 6, 2012}, 
            },
            5,
        },
        {
            5,
            {"03320900404", "Michaël", "Hajna"},
            {
                {"AUDI A1", "DN259LM", 1, 1998},
                {"AUDI A4", "DZ222EE", 9, 2015},
                {"AUDI Q3", "DS338GG", 6, 2003}, 
                {"AUDI Q8", "CU990PO", 6, 2012}, 
                {"AUDI A6", "FE345YU", 6, 2015}, 
            },
            5,
        },
        {
            6,
            {"03320900404", "Roeland", "Tafadzwa"},
            {
                {"FIAT Panda", "EF160NX", 1, 2011},
                {"FIAT Stilo", "AS124DF", 9, 2015},
                {"FIAT 600", "CW456YU", 6, 2012}, 
                {"FIAT 500", "CF445TH", 6, 2014}, 
                {"FIAT 500L", "EA989GB", 6, 2012}, 
            },
            5,
        },
        {
            7,
            {"03320900404", "Ismaele", "Anastazija"},
            {
                {"SKODA Fabia", "FQ232FV", 1, 2006},
                {"SKODA Kamiq", "FB666UI", 3, 2015},
                {"SKODA Scala", "FG897PL", 6, 2000}, 
                {"SKODA Octavia", "FJ999LO", 6, 2012}, 
                {"SKODA Karoq", "FP989IJ", 6, 1997}, 
            },
            5,
        },
        {
            8,
            {"03320900404", "Lennon", "Lía"},
            {
                {"PEUGEOT 108 ", "ER555TG", 1, 1990},
                {"PEUGEOT 1008", "EY888BN", 9, 2013},
                {"PEUGEOT 208", "ED232VB", 6, 2012}, 
                {"PEUGEOT Traveller", "EW678OP", 6, 2001}, 
                {"PEUGEOT Rifter", "EH840NJ", 6, 2015}, 
            },
            5,
        },
        {
            9,
            {"03320900404", "Divya", "Ilonka"},
            {
                {"BMW Serie 1", "EB720LL", 1, 2008},
                {"BMW Serie 2 Cabrio", "FT667OP", 9, 2015},
                {"BMW Serie 3 Berlina", "FW234LL", 6, 2003}, 
                {"BMW Serie 5 Touring", "FV221UJ", 6, 2009}, 
                {"BMW Serie 6 Gran Turismo", "FG888TT", 6, 2017}, 
            },
            5,
        },
    };
    stampaconcessionario(concessionari, effconc);    //funzione per stampare dettagli richiesti
    
    return 0;
}