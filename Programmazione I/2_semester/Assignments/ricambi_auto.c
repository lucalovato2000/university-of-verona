/*

-------------------------------

Scrivere un programma C che faciliti il compito di sistemare i pezzi nel magazzino a seconda del loro peso al gestore di un magazzino di ricambio auto. 

I ricambi auto sono di 3 tipi: gomme, paraurti e specchietti. 
Dovrete implementare una bilancia con memoria e con queste funzionalità:

    L'utente ad ogni interazione può scegliere:
        quale tipo di pezzo pesare;
        vedere lo storico delle pesate, per cui almeno N pesate definite staticamente devono rimanere salvate;
        uscire dal programma;
    La bilancia deve rispondere ad ogni pesata dell'utente con:
        un numero progressivo che indica quante pesate sono avvenute finora;
        il peso del pezzo pesato;
    La bilancia deve salvare i dati della pesata appena avvenuta.

Alcuni requisiti tecnici: 

    Il programma deve contenere almeno il main() e una funzione pesa() per la bilancia;
    Utilizzare una variabile statica all'interno di questa funzione pesa() per salvare il numero di pesate avvenute;
    Usare una lista e almeno una variabile globale per salvare lo storico delle pesate;
    Gestire la liberazione della memoria quando il limite massimo N di pesate mantenute in memoria viene raggiunto;

Il peso dei valori dei vari pezzi varia in un range: 

    gomme: tra 9 e 11 kili;
    paraurti = tra 18 e 22 kili;
    specchietti = tra 0.1 e 1.5 kili

La pesata dei vari pezzi deve essere simulata con valori random float nel rispettivo range. 
Esempio: 

    Menu
    1. Pesa
    2. Vedi storico pesate
    0. Esci
    > 1
    Pesa cosa? (1 = gomma, 2 = paraurti, 3 = specchietto)
    > 2
    Pesata #1 => Questo paraurti pesa 20.25 kili

    Menu
    1. Pesa
    2. Vedi storico pesate
    0. Esci
    >1
    Pesa cosa? (1 = gomma, 2 = paraurti, 3 = specchietto)
    > 3
    Pesata #2 => Questo specchietto pesa 1.23 kili

    Menu
    1. Pesa
    2. Vedi storico pesate
    0. Esci
    > 2
    Pesate finora:
    1 => Paraurti, 20 kili
    2 => Specchietto, 1.2 kili

---------------------------------
*/


#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 5 //numero massimo pesate

int cont=0; //inizializza contatore delle pesate a 0

//dichiara la struttura pesata
typedef struct pesata{
    int num;
    char *nome[13];
    float peso;
    struct pesata *next;
}pesata;
 

//funzione che visualizza la lista
void visualizza(pesata *miapesata){

    while(miapesata!=NULL){   //finche' miapesata non e' vuota

        printf("Pesata #%d => %s, %3.2f kili\n\n", miapesata->num, *miapesata->nome, miapesata->peso); //stampa caratteristiche
        miapesata=miapesata->next;    //passa al nodo successivo
    }
}

//funzione per liberare dalla memoria la lista
pesata *distruggiLista(pesata *miapesata){
  
    pesata *tmp;
  
    while(miapesata!=NULL){

        tmp = miapesata;    //passa a tmp i valori di miapesata
        miapesata = miapesata->next;    //passa i valori a miapesata di next
        
        free(tmp);  //elimina dalla memoria il contenuto di tmp
    }
  return NULL;
}

//funzione che permette di pesare gli oggetti
pesata *pesa(pesata *miapesata, int sc2){
   
    srand(time(NULL));  //seme random
    
    cont++; //aumenta contatore pesata
 
    if(cont>N){  //se contatore maggiore del numero massimo di pesate
        miapesata=distruggiLista(miapesata); 
        cont=1; 
    }
       
    pesata *tmp=(pesata*)malloc(sizeof(pesata));    //instanzia una lista tmp
    pesata *prec;   //nodo precedente
   
    tmp->num=cont;  
    tmp->next=NULL;
 
    if(sc2==1){ //scelta 1
        *tmp->nome="gomma"; //assegna il nome dell'oggetto
        tmp->peso=(90+(float)(rand()%20))/10; //assegna il peso dell'oggetto
    }
    if(sc2==2){ //scelta 2
        *tmp->nome="paraurti";
        tmp->peso=(180+(float)(rand()%50))/10;
    }
    if(sc2==3){ //scelta 3
        *tmp->nome="specchietto";
        tmp->peso=(1+(float)(rand()%14))/10;
    }
    
    //se lista risulta vuota
    if(miapesata==NULL)
        miapesata=tmp;  //assegna il valore di tmp alla lista miapesata
    else{
        for(prec=miapesata;prec->next!=NULL;prec=prec->next);
        prec->next=tmp;
    }
   
    printf("\nPesata #%i => Questo %s pesa %3.2f kili\n\n",tmp->num,*tmp->nome,tmp->peso);    //stampa caratteristiche
 
    return miapesata;
}


int main(){
    
    int sc1,sc2;
    
    pesata *miapesata=NULL;     //inizializzo la lista vuota
 
    do{ //finche' non si decide di uscire dal programma

        do{ //finche' non si decide una opzione
            
            printf("Menu\n1. Pesa\n2. Vedi storico pesate\n0. Esci\n> ");
            scanf("%d", &sc1);   //acquisisce la prima scelta
            
        }while(sc1<0 || sc1>2);
    
        if(sc1==1){ //scelta 1

            do{ //finche' non si decide una opzione

                printf("Pesa cosa? (1=gomma, 2=paraurti, 3=specchietto)\n");
                scanf("%d", &sc2);

            }while(sc2<1 || sc2>3);

            miapesata=pesa(miapesata,sc2);  //passo la lista e la scelta alla funzione pesa
        }

        if(sc1==2)  //scelta 2
            visualizza(miapesata); //passo la lista alla funzione visualizza
       
    }while(sc1!=0);
 
    return 0;
}