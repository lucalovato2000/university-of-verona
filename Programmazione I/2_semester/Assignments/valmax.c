/*

-------------------------------

Scrivere un sottoprogramma C che riceve come parametro una matrice quadrata 4x4 di numeri interi positivi e un intero m. 
Il sottoprogramma calcola e restituisce al chiamante il massimo valore ≤ m contenuto nella matrice se esiste; se tale valore non esiste verrà restituito il valore -1 

-------------------------------
*/

#include <stdio.h>
#define N 4

//funzione per inserire i valori della Matrice M
void inserisciM_N(int M[N][N]){

	int i,j;

	for(i=0;i<N;i++){
		for(j=0;j<N;j++){
			//controlla se il valore inserito risulta negativo
			do{
				printf("Inserisci un valore intero positivo della matrice: ");
				scanf("%d", &M[i][j]);
			}while(M[i][j]<=0);
		}
	}
}

//funzione per stampare la matrice M 
void stampaM_N(int M[N][N]){

	int i,j;
	
	printf("Matrice inserita: \n");
	for(i=0;i<N;i++){
		for(j=0;j<N;j++){
			printf("%d ", M[i][j]);
		}
		printf("\n");
	}
}

//funzione per calcolare il valore massimo
int valmax(int M[N][N], int num){

	int i,j;

	int nmax=0;	

	for(i=0;i<N;i++){
		for(j=0;j<N;j++){
			if(M[i][j]<=num && M[i][j]>nmax)	//se il valore della matrice M risulta minore o uguale del numero inserito dall'utente e maggiore del valore di nmax
				nmax=M[i][j];	//assegna a nmax il valore della matrice M
		}
	}
	return(nmax);	
}

//funzione main
int main(){

	int M[N][N];	//dichiara la matrice M 4x4
	int num, ris;	//dichiara num e ris interi

	inserisciM_N(M);	//richiede la matrice M
	printf("Inserisci un valore intero: ");
	scanf("%d", &num);	//richiede il numero num
	
	stampaM_N(M);	//stampa la matrice M
	printf("Numero inserito: %d\n", num);	//stampa il numero num

	ris=valmax(M, num);	//restituisce il valore massimo 

	if(ris==0)	//se il valore massimo risulta zero
		printf("%d\n", -1);	//stampa il valore -1
	else
		printf("Valore massimo risultante: %d\n", ris);	//stampa il valore massimo 
}
