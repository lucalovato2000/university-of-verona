/*

-------------------------------

Scrivere un programma C che faciliti il compito di sistemare i pezzi nel magazzino a seconda del loro peso al gestore di un magazzino di ricambio auto. 

I ricambi auto sono di 3 tipi: gomme, paraurti e specchietti. 
Dovrete implementare una bilancia con memoria e con queste funzionalità:

    L'utente ad ogni interazione può scegliere:
        quale tipo di pezzo pesare;
        vedere lo storico delle pesate, per cui almeno N pesate definite staticamente devono rimanere salvate;
        uscire dal programma;
    La bilancia deve rispondere ad ogni pesata dell'utente con:
        un numero progressivo che indica quante pesate sono avvenute finora;
        il peso del pezzo pesato;
    La bilancia deve salvare i dati della pesata appena avvenuta.

Alcuni requisiti tecnici: 

    Il programma deve contenere almeno il main() e una funzione pesa() per la bilancia;
    Utilizzare una variabile statica all'interno di questa funzione pesa() per salvare il numero di pesate avvenute;
    Usare una lista e almeno una variabile globale per salvare lo storico delle pesate;
    Gestire la liberazione della memoria quando il limite massimo N di pesate mantenute in memoria viene raggiunto;

Il peso dei valori dei vari pezzi varia in un range: 

    gomme: tra 9 e 11 kili;
    paraurti = tra 18 e 22 kili;
    specchietti = tra 0.1 e 1.5 kili

La pesata dei vari pezzi deve essere simulata con valori random float nel rispettivo range. 
Esempio: 

        Menu
        1. Pesa
        2. Vedi storico pesate
        0. Esci
        > 1
        Pesa cosa? (1 = gomma, 2 = paraurti, 3 = specchietto)
        > 2
        Pesata #1 => Questo paraurti pesa 20.25 kili

        Menu
        1. Pesa
        2. Vedi storico pesate
        0. Esci
        >1
        Pesa cosa? (1 = gomma, 2 = paraurti, 3 = specchietto)
        > 3
        Pesata #2 => Questo specchietto pesa 1.23 kili

        Menu
        1. Pesa
        2. Vedi storico pesate
        0. Esci
        > 2
        Pesate finora:
        1 => Paraurti, 20 kili
        2 => Specchietto, 1.2 kili

-------------------------------
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define NPESATE 4   //massimo pesate in memoria

/*
- numpes indica il numero della pesata
- eff indica l'effettiva pesata
- i indica l'indice corrispondente della pesata nella struttura
*/

int i=0, eff=0, numpes=1;   

//struttura per gestire prodotto
typedef struct{
    float peso;
    int num;
    char *nome[NPESATE];  //numero di stringhe massimo uguali 
}pesata;

pesata structvuota;

//funzione per pesare il prodotto ed aggiungerlo alla lista
int pesa(int n, pesata *miapesata){
    
    srand(time(NULL));	//seme random
    int sc; 
   

        if(n==1 && numpes<=NPESATE){    //finche' la scelta risulta 1 e il numero effettivo di pesate e' minore del massimo
            
            printf("Pesa cosa? (1 = gomma, 2 = paraurti, 3 = specchietto)\n>");    //visvualizza il menu
            scanf("%d", &sc);    //ottiene il numero 
            
            switch(sc){
                case 1:
                    miapesata[i].peso=9+(float)(rand()%3)/10;   //assegna a peso il valore generato random tra 9 e 11
                    miapesata[i].num=numpes;    //assegna a num il numero della pesata
                    miapesata[i].nome[i]="gomma";     //assegna al nome il nome del prodotto

                    printf("Pesata #%d => Questa gomma pesa %3.2f\n\n", numpes, miapesata[i].peso);   //visvualizza caratteristiche
                    i++;eff++;  //incrementa indice effettivo prodotto e indice globale
                    numpes++;   //aumenta il contatore dei prodotti pesati
                    break;

                case 2:
                    miapesata[i].peso=18+(float)(rand()%5)/10;   //assegna a peso il valore generato random tra 18 e 22
                    miapesata[i].num=numpes;    //assegna a num il numero della pesata
                    miapesata[i].nome[i]="paraurti";     //assegna al nome il nome del prodotto
                
                    printf("Pesata #%d => Questo paraurti pesa %3.2f\n\n", numpes, miapesata[i].peso);   //visvualizza caratteristiche
                
                    i++;eff++;  //incrementa indice effettivo prodotto e indice globale
                    numpes++;   //aumenta il contatore dei prodotti pesati
                    
                    break;

                case 3:
                    miapesata[i].peso=(0.1)+(float)(rand()%2)/10;   //assegna a peso il valore generato random tra 0.1 e 1.5
                    miapesata[i].num=numpes;    //assegna a num il numero della pesata
                    miapesata[i].nome[i]="specchietto";     //assegna al nome il nome del prodotto
                    
                    printf("Pesata #%d => Questo specchietto pesa %3.2f\n\n", numpes, miapesata[i].peso);   //visvualizza caratteristiche
                    
                    i++;  //incrementa indice effettivo prodotto e indice globale
                    eff++;   //aumenta il contatore dei prodotti pesati
                    numpes++;
                    
                    break;
            }

        }if(n==2){
    
            printf("Pesate finora: \n");

            for(i=0;i<eff;i++){
                //stampa caratteristiche della pesata
                printf("%d=>%s, %3.2f kili\n\n", miapesata[i].num, miapesata[i].nome[i], miapesata[i].peso);
            }
            

        }
        
        if(numpes>NPESATE){  
            *miapesata=structvuota;
            i=0;eff=0;numpes=1;
        }
    return 0;
}


int main(){

    pesata miapesata[NPESATE];    //inizializzo una pesata
    int n;  

    do{
        printf("Menu\n1. Pesa\n2. Vedi storico pesate\n0. Esci\n>");    //visualizza il menu
       
        scanf("%d", &n);    //ottiene il numero 

        switch(n){
            case 1:
                pesa(1, miapesata);    //passa 1 alla funzione pesa
                break;
            case 2:
                pesa(2, miapesata);    //passa 2 alla funzione pesa
                break;
        }

    }while(n==1 || n==2);   //termina il programma se l'input risulta differente dalle due opzioni

    return 0;
}
