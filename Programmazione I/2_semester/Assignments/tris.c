/*

-------------------------------

Scrivere un programma che implementi una versione semplificata di Tris a due giocatori- Ad ogni turno, il programma:
• chiede al giocatore 1 di inserire le coordinate della propria giocata; 
• inserisce il simbolo del giocatore 1 (’X’) nella posizione indicata;
• visualizza la griglia di gioco aggiornata;
• controlla se il giocatore 1 ha vinto;

• ripete le stesse operazioni per il giocatore 2 (simbolo ’O’)-

Il programma prosegue finchè uno dei due giocatori non vince oppure fino a quando viene raggiunto il numero massimo di mosse disponibili- Si assuma che i due giocatori inseriscano sempre coordinate valide 

-------------------------------
*/

#include <stdio.h>
#define N 3

//controlla se il secondo giocatore ha vinto
int checkwing2(char M[N][N]){

    int flag=0;

    if(M[0][0]=='O' && M[0][1]=='O' && M[0][2]=='O')
        flag=1;
    else if(M[1][0]=='O' && M[1][1]=='O' && M[1][2]=='O')
        flag=1;
    else if(M[2][0]=='O' && M[2][1]=='O' && M[2][2]=='O')
        flag=1;
    else if(M[0][0]=='O' && M[1][0]=='O' && M[2][0]=='O')
        flag=1;
    else if(M[0][1]=='O' && M[1][1]=='O' && M[2][1]=='O')
        flag=1;
    else if(M[0][2]=='O' && M[1][2]=='O' && M[2][2]=='O')
        flag=1;
    else if(M[0][0]=='O' && M[1][1]=='O' && M[2][2]=='O')
        flag=1;
    else if(M[0][2]=='O' && M[1][1]=='O' && M[2][0]=='O')
        flag=1;

    return flag;
}

//controlla se il primo giocatore ha vinto
int checkwing1(char M[N][N]){

    int flag=0;

    if((M[0][0]=='X') && (M[0][1]=='X') && (M[0][2]=='X'))
        flag=1;
    else if(M[1][0]=='X' && M[1][1]=='X' && M[1][2]=='X')
        flag=1;
    else if(M[2][0]=='X' && M[2][1]=='X' && M[2][2]=='X')
        flag=1;
    else if(M[0][0]=='X' && M[1][0]=='X' && M[2][0]=='X')
        flag=1;
    else if(M[0][1]=='X' && M[1][1]=='X' && M[2][1]=='X')
        flag=1;
    else if(M[0][2]=='X' && M[1][2]=='X' && M[2][2]=='X')
        flag=1;
    else if(M[0][0]=='X' && M[1][1]=='X' && M[2][2]=='X')
        flag=1;
    else if(M[0][2]=='X' && M[1][1]=='X' && M[2][0]=='X')
        flag=1;

    return flag;
}

//inserisce nella griglia il simbolo X
char insertMatrx(char M[N][N], int g1x, int g1y){

    M[g1x][g1y]='X';
    
    return M[N][N];
}

//inserisce nella griglia il simbolo O
char insertMatrx2(char M[N][N], int g2x, int g2y){

    M[g2x][g2y]='O';
    
    return M[N][N];
}

//stampa la griglia
void returnMatrx(char M[N][N]){

    int i,j;

    for(i=0;i<N;i++){
        for(j=0;j<N;j++){
            printf("%c", M[i][j]);
        }
        printf("\n");
    }
}

//richiede la coordinata X
int insertcx(){

    int x;
    do{
        printf("Inserisci la coordinata X: ");
        scanf("%d", &x);
    }while(x<0 || x>2);
    
    return(x);
}

//richiede la coordinata Y
int insertcy(){
    
    int y;
    do{
        printf("Inserisci la coordinata Y: ");
        scanf("%d", &y);
    }while(y<0 || y>2);
    
    return(y);
}

int main(){

    int g1x,g1y;    //coordinate X,Y del giocatore 1
    int g2x,g2y;    //coordinate X,Y del giocatore 2
    int g1w,g2w;    //variabili per vittora giocatore 1 o vittoria giocatore 2
    int cont=0;     //contatore per esaurimento mosse

    //inizializzazione griglia di gioco
    char M[N][N]={{'-','-','-'}, {'-','-','-'}, {'-','-','-'} };

    //finche' nessuno ha vinto e le mosse non sono esaurite
    while(g1w!=1 && g2w!=1 && cont<9){
        
        g1x=insertcx();  //inserisce coordinata X giocatore 1
        g1y=insertcy();  //inserisce coordinata Y giocatore 1
        cont++; 
        printf("X: %d, Y: %d\n", g1x, g1y);
        M[N][N]=insertMatrx(M,g1x,g1y);     //inserisce coordinate nella griglia
        returnMatrx(M); //stampa la griglia

        g1w=checkwing1(M);  //controlla se ha vinto giocatore 1
        
        if(g1w==1)
            printf("Ha vinto il giocatore 1\n");

        if(g1w==0 && cont<9){ //se giocatore 1 non ha vinto e le mosse non sono esaurite
            g2x=insertcx();  //inserisce coordinata X giocatore 2
            g2y=insertcy();  //inserisce coordinata Y giocatore 2
            cont++;
            printf("X: %d, Y: %d\n", g2x, g2y);
        
            M[N][N]=insertMatrx2(M,g2x,g2y);    //inserisce coordinate nella griglia
            returnMatrx(M); //stampa la griglia

            g2w=checkwing2(M);  //controlla se ha vinto giocatore 2
        
            if(g2w==1)
                printf("Ha vinto il giocatore 2\n");
        }
    }
    return 0;
}