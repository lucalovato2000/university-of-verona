/* 

-------------------------------

Scrivere un programma C che stampa in sequenza i numeri da 1 a 1000 separati da una virgola, sostituendo:
con "CIP" i numeri che sono multipli di 3, 
con "CIOP" i numeri che sono multipli di 7,
con "CIPCIOP" i numeri che sono multipli sia di 3 che di 7. 

-----------------------------------------------------------------------------------------------------------

ES:
 
 1, 2, CIP, 4, 5, CIP, CIOP, 8, CIP, 10, 11, CIP, 13, CIOP, CIP, 16, 17, CIP, 19, 20, CIPCIOP, 22, 23, 
 CIP, 25, 26, CIP, CIOP, 29, CIP, 31, 32, CIP, 34, CIOP, CIP, 37, 38, CIP, 40, 41, CIPCIOP, 43, 44 [ ... ] 
 
-------------------------------
*/

#include <stdio.h>
#define N 1000

int main(){
	
	int i;	
	
	for(i=1;i<=N;i++){	//scorre da 1 a 1000
		
		if(i%3==0 && i%7==0){	//multiplo di 3 e 7
			printf(" CIPCIOP,");	
		}else if(i%3==0){		//multiplo di 3 
			printf(" CIP,");
		}else if(i%7==0){		//multiplo di 7
			printf(" CIOP,");
		}else if(i==1000){		
			printf(" %d", i);	//stampa numero senza virgola 
		}else{
			printf(" %d,", i);	//stampa numero con virgola 
		}
	}
}
