/*

-------------------------------

Scrivere un programma in C (matricola_es2.c) che chiede all'utente 5 valori interi non negativi e ne disegna l'istogramma a barre verticali come mostrato nel seguente esempio.
Se l'utente inserisce i valori: 1 4 5 2 1, il programma visualizzerà  il seguente output:

    *    
  * *    
  * *    
  * * *  
* * * * *
1 4 5 2 1 

-------------------------------
*/

#include <stdio.h>
#define N 5		//N corrisponde alla lunghezza dell'array

int main(){

	int v[N],i,max=0;    	

	//ciclo for per inserire nell'array i valori inseriti dall'utente
	for(i=0;i<N;i++){

		//do while per forzare l'utente ad inserire valori consentiti	
		do{
			printf("Inserisci un valore a scelta: ");
			scanf("%d", &v[i]);  
			
			if(v[i]>max)	//se il valore inserito è maggiore di max
				max=v[i];		//il valore diventa il massimo
		
		}while(v[i]<0);
	}
	
	
	while(max>0){	//finchè max non si azzera
      		
		for(i=0;i<N;i++){	//scorre l'array
			
			//se il valore in posizione v[i] è maggiore uguale al massimo stampa * altrimenti lascia lo spazio vuoto
			if(v[i]>=max)		
				printf("  *");
       		else
          		printf("   ");
		}
		
      	max--;	//decrementa max
      	printf ("\n");	

  	}

	for(i=0;i<N;i++){	//scorre l'array
		printf("%3d", v[i]);	//stampa l'array 
	}

	printf("\n");

	return 0;
}
		
	
