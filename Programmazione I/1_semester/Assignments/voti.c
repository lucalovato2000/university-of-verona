/*

-------------------------------

Scrivere un programma in C (matricola_lab02.c) per calcolare la media, il massimo e il minimo di una sequenza di voti inseriti dall'utente.
L'inserimento dei voti prosegue fino a quando l'utente non immette un valore non valido, cioè minore di 18 o maggiore di 30
(valore di «fine inserimento», che non fa parte della sequenza).
Quando viene immesso il valore di fine inserimento, vengono visualizzati media, massimo,
minimo e il numero di voti inseriti. Qualora il primo valore inserito fosse quello di fine inserimento,
dovrà essere visualizzato un messaggio di errore.

-------------------------------
*/


#include <stdio.h>

int main(){

	int voto,votoi,i=1,mn=0,max=0;
	int nvoti=0;
	float md, sm;


	printf("Inserisci un voto: ");  //Ottiene il primo voto dall'utente
	scanf("%d", &votoi);

	if(votoi<18 || votoi>30)	//Se il voto non è copmpreso fra 18 e 30 
		printf("Voto non corretto \n");			

	else{	
		
		mn=votoi;		//Voto inserito diventa il minimo
		max=votoi;		//Voto inserito diventa il massimo
		sm+=votoi;		//Voto inserito si aggiunge alla somma
		md=votoi;		
		nvoti+=1;		//Numero voti inseriti diventa 1

		do{
			printf("Inserisci un voto: ");
			scanf("%d", &voto);
				
			if(voto>=18 && voto<=30){	//Se il voto non è compreso fra 18 e 30 
			
				if(voto<votoi)		//se è più piccolo del valore precedente diventa il minore
					mn=voto;
	
				else if(voto>votoi)	//se è più grande del valore precendete diventa il maggiore
					max=voto;

				sm+=voto;					
				i++;		//incrementa il contatore
				md=sm/i;	//media calcolata come somma/contatore
				nvoti+=1;	//incrementa il numero di voti inseriti
			}
		
		}while(voto>=18 && voto <=30);		
		
		printf("numero di voti inseriti: %d \n",nvoti);
		printf("Numero massimo inserito: %d \n",max);
		printf("Numero minimo inserito: %d\n",mn);
		printf("Media calcolata: %.1f \n",md);
					
	}	
	return 0;
}

