/*

-------------------------------

Su una scacchiera 8x8 sono posizionati due pezzi:il Re bianco e la Regina nera. Si scriva un
programma in linguaggio C che acquisisce le posizioni del Re e della Regina in termini di
coordinate (x,y) assumendo che la posizione (1,1) sia situata in basso a sinistra rispetto al giocatore.

Il programma controlla prima che le coordinate inserite siano valide; in particolare entrambi i
pezzi devono trovarsi all’interno della scacchiera ed inoltre non possono trovarsi nella stessa
posizione. In seguito il programma determina se la Regina è in posizione tale da poter mangiare il
Re e visualizza un apposito messaggio specificando anche in che direzione e per quante
posizioni deve muoversi per mangiare

-------------------------------
*/

#include <stdio.h>

void main()
{

	int x1, y1, x2, y2;

	

	printf("Inserisci la posizione X del Re: ");
	scanf("\n %d", &x1);

	printf("Inserisci la posizione Y del Re: ");
	scanf("\n %d", &y1);

	printf("Inserisci la posizione X della Regina: ");
	scanf("\n %d", &x2);

	printf("Inserisci la posizione Y della Regina: ");
	scanf("\n %d", &y2);

	

	int or = (x1 - x2);	//spostamento orizzontale
	int vr = (y1 - y2);	//spostamento verticale

	
	if (or < 0) //moltiplica per -1 se il valore di or è negativo
		or *= -1;

	if (vr < 0) //moltiplica per -1 se il valore di vr è negativo
		vr *= -1;

	int dg = (or + vr) / 2; //spostamento diagonale


	//controlla che le coordinate inserite siano all'interno della scacchiera 8x8
	if (x1 > 0 && x1 <= 8 && y1 > 0 && y1 <= 8 && x2 > 0 && x2 <= 8 && y2 > 0 && y2 <= 8)
	{

		if (x1 != x2 || y1 != y2) //controlla che il Re e la Regina abbiano le stesse coordinate
		{
			printf("Le coordinate inserite sono corrette \n");
			if ((y1 == y2) || (x1 == x2) || (x2 + y2 == x1 + y1) || (x2 - y2 == x1 - y1)){
				
				//stessa riga
				if (y1 == y2)
				{

					if (x2 < x1)
						printf("Il Re si trova sotto scacco \nLa regina deve muoversi verso destra di %d posizioni \n", or);
					else
						printf("Il Re si trova sotto scacco \nLa regina deve muoversi verso sinistra di %d posizioni \n", or);
				}

				//stessa colonna
				if (x1 == x2)
				{

					if (y2 < y1)
						printf("Il Re si trova sotto scacco \nLa regina deve muoversi verso l'alto di %d posizioni \n", vr);
					else
						printf("Il Re si trova sotto scacco \nLa regina deve muoversi verso il basso di %d posizioni \n", vr);
				}

				//diagonale discendente del Re
				if (x2 + y2 == x1 + y1)
				{
					if (y1 > y2)
						printf("Il Re si trova sotto scacco \nLa regina deve muoversi verso la diagonale discendente verso l'alto di %d posizioni \n", dg);

					else
						printf("Il Re si trova sotto scacco \nLa regina deve muoversi verso la diagonale discendente verso il basso di %d posizioni \n", dg);
				}


				//diagonale ascendente del Re
				if (x2 - y2 == x1 - y1) 
				{
					if (y1 < y2)
						printf("Il Re si trova sotto scacco \nLa regina deve muoversi verso la diagonale ascendente verso il basso di %d posizioni \n", dg);

					else
						printf("Il Re si trova sotto scacco \nLa regina deve muoversi verso la diagonale ascendente verso l'alto di %d posizioni \n", dg);
				}
			}else
				printf("Il Re non si trova sotto scacco \n");
			
		}
		else
			printf("Il Re e la Regina occupano le stesse posizioni \n");
	}
	else
		printf("Coordinate non valide Devono essere comprese fra 1 e 8 \n");
}
