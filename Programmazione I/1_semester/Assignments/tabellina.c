/*

-------------------------------

Scrivere un programma C che stampa una matrice nxn (n è un valore richiesto all’utente), in cui:
*la prima colonna è formata dai numeri da 1 a n
*ogni riga contiene i primi n numeri della tabellina del numero da cui parte (il primo elemento della riga)

---------------------------------------------------------------------------------------------------------------------

Esempio:

Inserisci un valore: 4
1 2 3 4 
2 4 6 8 
3 6 9 12 
4 8 12 16 

-------------------------------
*/


#include <stdio.h>

int main(){
	
	int n;	//numero chiesto all'utente per righe e colonne della matrice 
	int t; 	//numero risultante presente nella matrice 
	int i,j;

	do{
		printf("Inserisci un valore: ");
		scanf("%d", &n); 	//memorizza la variabile n 
	}while(n<=0);	//ripete se n minore di zero

	n=n+1;	//incrementa la varibile n di uno
	int m[n][n];	//instanzia la matrice con n righe e n colonne decise dall'utente

	for(i=0;i<n;i++){	//per tutte le righe della matrice
		for(j=0;j<n;j++){	//per tutte le colonne della matrice
			t=i*j;	//moltiplica gli indici fra loro 
			m[i][j]=t;	//assegna il risultato nella matrice		
		}
	}
				
	for(i=1;i<n;i++){	//per tutte le righe
		for(j=1;j<n;j++){	//per tutte le colonne
			printf("% 3d", m[i][j]);	//stampa la matrice
		}
		printf("\n");	//nuova riga 
	}	
	return 0;
}
