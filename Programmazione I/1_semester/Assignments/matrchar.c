/*

-------------------------------

Scrivere un sottoprogramma che riceve come parametro una matrice quadrata 5x5 di caratteri (qualsiasi valore char).
Il sottoprogramma individua il carattere che compare più frequentemente e lo restituisce al chiamante (si ipotizzi che sia sempre unico).
Inoltre il sottoprogramma visualizza il contenuto della matrice, mostrando però uno spazio al posto dei caratteri uguali al carattere individuato.

Esempio:


Data la matrice            Il sottoprogramma restituirà ‘f’

in ingresso:               e stamperà a video il seguente testo

a.cde                      a.cde

fdffr                       d  r

tQfrd           ->         tQ rd

yyfwe                      yy we

f1bpf                       1bp 

-------------------------------
*/


#include <stdio.h>
#define N 5	//righe e colonne della matrice M


//sottoprogramma che restituisce valore maggiormente frequente
int frequenza(char M[N][N], char valcheck){
	int i,j;
	int cont=0;	//contatore frequenza inizializzato a 0
	
	for(i=0;i<N;i++){	//scorre le righe della matrice
		for(j=0;j<N;j++){	//scorre le colonne della matrice
			
			if(M[i][j]==valcheck)	//se valore della matrice uguale alla variabile da trovare
				cont++;	//incrementa contatore
		} 
	}
	return(cont);
}


//sottoprogramma che trova il carattere piu frequente nella matrice
void caratterefrequente(char M[N][N]){
	char f;
	int i,j;	//variabili per cicli annidati
	int nfrq=0;	//frequenza trovata nel sottoprogramma richiamato
	int nnum=0;	//frequenza maggiore temporanea
	
	for(i=0;i<N;i++){	//scorre le righe della matrice
		for(j=0;j<N;j++){	//scorre le colonne della matrice
			nfrq=frequenza(M,M[i][j]);	//sottoprogramma per contare la frequenza del valore nella matrice
			
			if(nfrq>=nnum){	//se la frequenza risulta maggiore di quella temporanea salvata
				nnum=nfrq;	//nnum diventa la frequenza piu alta trovata
				f=M[i][j];	//assegna ad f il valore piu frequente trovato
			}
		} 
	}
	
	//sostituisce al carattere maggiormente frequente il carattere spazio
	for(i=0;i<N;i++){
		for(j=0;j<N;j++){
			if(M[i][j]==f)
				M[i][j]=' ';
		}
	}
	
	//stampa la matrice quadrata 5x5
	for(i=0;i<N;i++){
		for(j=0;j<N;j++){
			printf(" %c", M[i][j]);
		}
		printf("\n");
	}
}


//main del programma
void main(){
	
	int i,j;		//variabili per i cicli 
	char M[N][N];	//matrice M
	char ris;		//carattere piu frequente
	

	//cicli annidati per inserimento valori nella matrice quadrata 5x5
	for(i=0;i<N;i++){	
		for(j=0;j<N;j++){
			printf("Inserisci un valore a scelta: ");
			scanf(" %c", &M[i][j]);
		}
	}
	
	//ricerca carattere maggiormente frequente e stampa la matrice sostituendo ad esso il carattere spazio
	return(caratterefrequente(M));
}
