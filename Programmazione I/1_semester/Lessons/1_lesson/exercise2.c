/*

Scrivere un programma che chiede all’utente una velocità
in km/h, la converte e visualizza a schermo in m/s e miglia
per ora (1 km/h= 0.64 Mph)

*/

#include <stdio.h>

int main(){

	float i, ris1, ris2;

	printf("Inserisci la velocità a cui stai andando in KM/H: ");
	scanf("%f",&i);

	ris1=(i/3.6);
	ris2=(i*0.621);

	printf("La tua velocità m/s risulta: %f \n", ris1);
	printf("La tua velocità miglia/ora risulta: %f \n", ris2);
}
