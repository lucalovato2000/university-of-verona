/*

Scrivere un programma che riceve in ingresso un angolo in 
gradi e lo converte in radianti (rad= gradi * π / 180°)

*/

#include <stdio.h>

int main(){

	float a, ris;

	printf("dammi un angolo in gradi: ");
	scanf("%f", &a);  //scanf vuole & 

	ris = (a*3.14/180);
	printf("il risultato in radianti è: %f", ris);

	return 0;
}
