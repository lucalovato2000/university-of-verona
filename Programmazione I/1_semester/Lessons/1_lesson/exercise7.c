/*

Scrivere un programma in C che acquisisce un carattere da tastiera  e lo visualizza. In seguito il programma chiede un altro carattere e lo visualizza.

*/

#include <stdio.h>

int main(){
	
	char c;
	int i = 0;

	while(i<2){
		printf("Inserisci un carattere dalla tastiera: ");
		scanf(" %c", &c);
		printf("Il carattere che hai inserito risulta: %c \n",c);
		i++;
	}
}
