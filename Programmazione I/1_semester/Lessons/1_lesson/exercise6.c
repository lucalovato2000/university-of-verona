/*

Scrivere un programma in C che chiede all’utente un carattere dell’alfabeto minuscolo (assumere non ci siano errori in inserimento). 
Il programma calcola e visualizza il corrispondente carattere maiuscolo.

*/

#include <stdio.h>
#include <ctype.h> 
	
	int main(){

	char c;
	printf("Inserisci un carattere minuscolo: ");
	scanf("%c", &c);

	printf("Il carattere maiscolo risulta: %c \n", toupper(c));
}
