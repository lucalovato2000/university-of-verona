/*

Scrivere in linguaggio C un programma per calcolare il numero di bottigliette d’acqua che si possono comprare alla macchinetta. Il programma, dopo aver chiesto all’utente i soldi a disposizione, stampa a video il numero di bottigliette che possono essere acquistate e il resto da ricevere dopo l’acquisto. Il prezzo di ciascuna bottiglietta è di 40 centesimi. Si supponga che la cifra a disposizione sia
positiva e che la macchinetta abbia in giacenza un numero illimitato di bottigliette. Per erogare il resto, la macchinetta utilizza pezzi da 20, 10, 5 e 1 centesimi: il programma dovrà calcolare il numero di monete di resto di ciascun taglio, prediligendo l’utilizzo di monete di taglio
maggiore (ad esempio, se il resto è di 0.37 €, il programma dovrà restituire: 1*20 cent, 1*10 cent, 1*5 cent, 2*1 cent). Suggerimento: moltiplicare il valore della cifra a disposizione per 100 per ottenere il numero di centesimi ed eseguire il cast del valore ad int

*/

#include <stdio.h>
 
int main() {
    float soldi;
    int bottiglie, resto = 0, prezzoBottiglia = 40, moneta1 = 20, moneta2 = 10, moneta3 = 5, moneta4 = 1;
 
    // ripeto input fino a quando ho un valore maggiore del prezzo della bottiglia
    do {
        printf("Digitare soldi a disposizione: ");
        scanf("%f", &soldi);
 
        soldi = soldi*100;
 
        if (soldi < prezzoBottiglia)
            printf("Inserire un valore maggiore o uguale a %d centesimi! \n", prezzoBottiglia);
    }
    while (soldi < prezzoBottiglia);
 
    // Calcolo numero bottiglie
    bottiglie = soldi/prezzoBottiglia;
    printf("Puoi acquistare %d bottiglie \n", bottiglie);
 
    // Calcolo resto
    resto = soldi - bottiglie*prezzoBottiglia;
 
    if (resto == 1)
        printf("Resto totale %d centesimo \n", resto);
    else
        printf("Resto totale %d centesimi \n", resto);
   
    printf("Resto monete da 20: %d \n", resto/moneta1);
    resto = resto - (moneta1 * (int)(resto/moneta1));
 
    printf("Resto monete da 10: %d \n", resto/moneta2);
    resto = resto - (moneta2 * (int)(resto/moneta2));
 
    printf("Resto monete da 5: %d \n", resto/moneta3);
    resto = resto - (moneta3 * (int)(resto/moneta3));
   
    printf("Resto monete da 1: %d \n", resto/moneta4);    
}
