/*

Scrivere un programma che, acquisito un valore intero da utente, stampi a video 0 se il valore è pari, 1 se è dispari (solo con i costrutti visti a lezione)

*/

#include <stdio.h>

int main(){

	int a,c;

	printf("Inserisci un valore intero: ");
	scanf("%d",&a);
	
	c=a%2;
	
	if(c==0)
		printf("Il numero inserito è pari \n");
	else
		printf("il numero inserito è dispari \n");
	
}
