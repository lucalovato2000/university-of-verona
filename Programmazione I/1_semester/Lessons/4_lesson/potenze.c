/*

Chiamiamo coppia di quadrati (CQ) una coppia <a,b> di numeri interi che sono uno il quadrato dell’altro. Esempio <9,3> oppure <-3,9> (infatti 9=3*3).
Si codifichi un programma C che legge da tastiera una sequenza che termina con 99 (di lunghezza a priori illimitata) di numeri interi e stampa a video quante coppie di numeri consecutivi all’interno della sequenza rappresentano una CQ.

Ad esempio: 2 4 16 0 3 9 99 contiene 3 CQ: <2,4> <4,16> e <3,9>

*/

#include <stdio.h>

int main(){
	
	int n, c, CQ=0;

	do{
		
		printf("Inserisci un valore a scelta: ");
		scanf("%d", &n);
		
		if(n!=99){		
		
			printf("Inserisci un valore a scelta: ");
			scanf("%d", &c);

			if(n^2==c)
				CQ+=1;
		}				
	
	}while(n!=99);
	
	printf("Le coppie consecutive di numeri risultano: %d \n", CQ);
	return 0;	
}
