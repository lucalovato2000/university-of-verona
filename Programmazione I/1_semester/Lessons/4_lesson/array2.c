/*

Ciclo dalla fine verso l'inizio

*/

#include <stdio.h>
#define N 10  //Definire ogni volta numero di valore nell'array

int main(){

	int a[N];
	int min, somma, i;

	for(i=0;i<N;i++)		//for usato con array
		scanf("%d", &i);        //Non puoi inserire in A[N] perchè non esiste

	for(i=N-1;i>=0;i--)
		printf("%3d", a[i]);

	return 0;
}
