/*

Scrivere un programma che dato un array di caratteri lungo 10 (con i valori letti da standard input) lo stampa, lo stampa invertito, sostituisce ogni vocale minuscola con un ’*’.

*/

#include <stdio.h>

int main(){

	char c[10];
	int i;

	for(i=0;i<10;i++){
		printf("Inserisci una lettera a scelta: ");
		scanf(" %c", &c[i]);
	}
	
	for(i=9;i>=0;i--){
		if(c[i] >= 'a' && c[i] <= 'z')
			printf("%c", '*');
		else
			printf("%c", c[i]);
	}
	
	return 0;
}
