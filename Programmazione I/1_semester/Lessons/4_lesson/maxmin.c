/* 

Scrivere un programma C che richiede all’utente 10 voti (compresi tra 18 e 30) e li memorizza all’interno di un array, quindi stampa l’array, il voto massimo, minimo e medio.

*/

#include <stdio.h>

int main(){

	int array[10];
	int i;
	
	for(i=0;i<10;i++){
		do{
			printf("Inserisci un numero a scelta: ");
			scanf("%d",&array[i]);
			
			if(array[i]<18 || array[i]>30){
				printf("Errore inserimento");
			}
			
		}while(array[i]<18 || array[i]>30);
	}
		
	int max=array[0];
	int min=array[0];
		
	for(i=0;i<10;i++){
		if(array[i]<min)
			min=array[i];

		else if(array[i]>max)
			max=array[i];
	
		printf("%d", array[i]);
	}	
		
	printf("max: %d \n", max);
	printf("min: %d \n", min);
	
	return 0;
}
