/*

Scrivere un programma che dato un array di interi A ed un intero x determina quante occorrenze di x sono in A.

1. se x==1 il programma stampa 2
2. se x==3 il programma stampa 3
3. se x==5 il programma stampa 0

*/

#include <stdio.h>
#define N 10

int main(){

	int c[N];
	int i, j, num, cont=0;

	printf("Inserisci un valore: ");
	scanf("%d", &num);
	
	for(i=0;i<N;i++){
		printf("Inserisci un valore a scelta: ");
		scanf("%d", &c[i]);
	}
	
	for(i=0;i<N;i++){
		for(j=0;j<N;j++){
			if(c[i]==c[j])
				cont++;
		}
	}
	
	if(cont==1)
		printf("%d \n", 2);
	if(cont==3)
		printf("%d \n", 3);
	if(cont==5)
		printf("%d \n", 0);
	else
		printf("Non ci sono occorrenze \n");

	return 0;
}
