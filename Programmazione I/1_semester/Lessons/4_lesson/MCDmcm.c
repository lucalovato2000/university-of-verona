/*

MCD mcm fra due numeri

*/


#include <stdio.h>

int main(){

	int a,b,MCD,mcm;

	
	do{
		printf("Inserisci il primo numero: ");
		scanf("%d", &a);
	}while(a<=0);


	do{
		printf("Inserisci il secondo numero: ");
		scanf("%d", &b);
	}while(b<=0);

	if(a<b)
		MCD=a;
	else
		MCD=b;

	while((a%MCD!=0) || (b%MCD!=0))
	MCD--;
	
	mcm=(a*b)/MCD;
	printf("MCD risulta: %d \nmcm risulta: %d \n", MCD,mcm);

	return 0;		
}
