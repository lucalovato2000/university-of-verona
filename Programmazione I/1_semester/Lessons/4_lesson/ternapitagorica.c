/*

Programma stampa 1 se i tre numeri inseriti formano una terna pitagorica, altrimenti stampa 0

*/	

#include <stdio.h>

int main(){

	int n1,n2,n3;
	int q=0;
	
	printf("Inserisci il primo numero: ");
	scanf("%d", &n1);

	if(n1==0)
		printf("Risultato: %d", q);

	else{
		printf("Inserisci il secondo numero: ");
		scanf("%d", &n2);

		if(n2==0)
			printf("Risultato: %d",q);

		else{
			printf("Inserisci il terzo numero: ");
			scanf("%d", &n3);
			
			if(n3!=0 && n1*n1+n2*n2==n3*n3){
				q+=1;     
				printf("Risultato: %d", q);
				
			}else
				printf("Risultato: %d", q);				
		}			
	}
	return 0;	
}
																	