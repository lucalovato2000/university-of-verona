/*

Scrivere un programma C che memorizza in un array i primi 15 numeri di Fibonacci e li stampa a video.

*/

#include <stdio.h> 

#define N 15

int main(){

	int fib[N],i,n;

	fib[0]=1;
	fib[1]=1;

	for(n=2;n<N-1;n++){
		fib[n]=fib[n-2]+fib[n-1];
	}

		for(n=0;n<N-1;n++){
			printf("%d \n", fib[n]);
		}
	
	return 0;
}
