/*

Scrivere un programma C che memorizza in un array di lunghezza 15 tutti i numeri primi minori di 50 e successivamente li stampa a video.

*/

#include <stdio.h>
#define N 50

int main(){

	int nprimi[N],n,pos,k,i;
	int isprime;

	pos=2;

	for(n=2;n<N;n++){
		isprime=1;
		
		for(k=2;k<=n/2 && isprime;k++){
			if(n&k==0)
				isprime=0;
		}
		
		if(isprime){			//conteggia come se fosse 1
			nprimi[pos]=n;
			pos++;
	}

	return 0;
}