/*

DUPLICATI NELL'ARRAY C 			

*/

#define N 10
#include <stdio.h>

int main(){

	int AY[N],i,j;
	int flag=0;

	for(i=0;i<N;i++){
		printf("Inserisci un valore a scelta: ");
		scanf("%d", &AY[i]);
	}

	for(i=0;i<N;i++){
		for(j=0;j<N;j++){
			if(AY[i]==AY[j])
				flag=1;
		}
	}
	
	if(flag==1)
		printf("Ci sono elementi duplicati \n");
	else
		printf("Tutti gli elementi sono differenti fra di loro \n");
}
