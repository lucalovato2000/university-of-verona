/*

Scrivere un programma che dato un array di interi (lungo 10) con i valori inseriti da tastiera determina quanti massimi locali ci sono nell’array stampando il risultato.

Dato l’array int A[10]; A[i] è detto massimo locale se uno dei seguenti casi è verificato

i=0 e A[0]>A[1]
i=9 e A[8]<A[9]
0<i<9 e A[i-1]<A[i]<A[i+1]

*/

#include <stdio.h>
#define N 10

int main(){

	int AY[N], i, n, ris=0;

	for(n=0;n<N;n++){
		printf("Inserisci un valore a scelta: ");
		scanf("%d", &AY[n]);
	}	

	if(AY[0]>AY[1])
		ris+=1;
		
	if(AY[N-2]<AY[N-1])
		ris+=1;

	for(i=0;i<N-2;i++){
		if(AY[n]<AY[n+1] && AY[n+1]>AY[n+2])
			ris+=1;
	}
	
	printf("%d", ris);
	
	return 0;
}
