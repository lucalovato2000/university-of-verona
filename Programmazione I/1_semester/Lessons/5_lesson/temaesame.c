/*

Dato un array ordinato di 10 valori, contare ogni volta che un valore viene inserito, stampando il numero di volta affianco al numero
scorrendo l'array dalla fine

*/

#include <stdio.h>
#define N 10

int main(){

	int v[N],n,m;

	for(n=0;n<N;n++){
		printf("Inserisci un valore a scelta: ");
		scanf("%d", &v[n]);
	}
	
	n=N-1;
	
	while(n>=0){
		for(m=0;n-m>=0 && v[n] == v[n-m];m++); //m contatore
		
		printf("%d  %d\n", v[n],m);
		n=n-m;
	}
	
	return 0;
}
