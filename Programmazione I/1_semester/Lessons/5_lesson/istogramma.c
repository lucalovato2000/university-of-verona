/*

STAMPARE ISTOGRAMMA A BARRE CON NUMERI DATI DA UTENTE

*/

#include <stdio.h>
#define N 10

int main(){

	int n, i, j, cont[N]={0}; // azzera tutte le celle

	printf("Inserisci un numero: ");
	scanf("%d",&n);

	while(n>=0 && n<N){

	cont[n]++;
	printf("Inserisci un numero: ");
	scanf("%d", &n);
	
	}

	for(i=0;i<N;i++){
		printf("%d ", i);

		for(j=0;j<cont[i];j++){
			printf("*");
		}
	}
	
	printf("\n");
	return 0;
}