/*

Chiedere all’utente una sequenza di numeri interi che termina con l’inserimento dello 0 (e in ogni caso lunga al massimo 100 elementi). Creare un array che contenga tutti e soli valori distinti della
sequenza (ovvero omettendo i duplicati).

Visualizzare l’array e il numero di elementi unici inseriti.

Variante: creare un array che contenga tutti e soli i valori che nella prima sequenza non ammettono duplicati.

*/

#include <stdio.h>
#define N 10


int main(){

	int AY[N],n,m;
	int num,temp;

	printf("Inserisci un valore a scelta: ");
	scanf("%d", &temp);

	if(temp==0){
		printf("Valore di fine inserimento, non valido \n");
	}else{
		AY[0]=temp;
			
		for(n=1;n<N;n++){
			printf("Inserisci un valore a scelta: ");
			scanf("%d", &AY[n]);
			if(AY[n]==0){
				for(n;n<N;n++){
					AY[n+1]=0;
				}
			}				
		}

		for (n=0; n<N-1; n++){
			for (m=n+1; m<10; m++){
				if (AY[n]>0 && AY[n]==AY[m]) 	
					AY[m]=-AY[m];
			}
		}
		
		for (n=0; n<N; n++){
			if(AY[n]>0)
				printf("%d ", AY[n]);
		}
	}
	return 0;
}
