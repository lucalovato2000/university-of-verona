/*

Scrivere un programma C che richiede all’utente di inserire 20 caratteri che memorizza in un array, e altri due caratteri c e r. 
Restituisce una array costruito a partire dall’array iniziale in cui ogni occorrenza di c è sostituita con r.

*/

#include <stdio.h>
#define N 20

int main(){

	char AY[N], n, c, r;

	printf("Inserisci un carattere: ");
	scanf(" %c", &c);

	printf("Inserisci un carattere: ");
	scanf(" %c", &r);

	for(n=0;n<N;n++){
		printf("Inserisci un carattere a scelta: ");
		scanf(" %c", &AY[n]);
			
		if(AY[n]==c)
			AY[n]=r;		
	}

	for(n=0;n<N;n++){
		printf("%c", AY[n]);
	}
	
	return 0;
}
