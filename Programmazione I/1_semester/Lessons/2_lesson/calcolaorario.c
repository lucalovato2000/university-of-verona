/*

Scrivere un programma in C che richiede all’utente di inserire ore e minuti, calcola l’ora a
San Francisco (9 ore indietro) e a Teheran (+3:30) e la stampa a video in formato «0-24 h : min». Se a
Teheran è già il giorno successivo o a San Francisco ancora quello precedente, il programma
corregge l’ora e avvisa l’utente con un opportuno messaggio a schermo.

*/

#include <stdio.h>

int main(){

	int ore, min;

	printf("Inserisci l'ora: ");
	scanf(" \n %d", &ore);

	printf("Inserisci i minuti: ");
	scanf(" \n %d", &min);

	int sf = ore - 9;

	int th1 = ore + 3;
	int th2 = min + 30;

	if (sf < 0)
		printf("A SAN FRANCISCO SONO LE ORE: %d:%d DI IERI \n", 24 + sf, min);
	else
		printf("A SAN FRANCISCO SONO LE ORE %d:%d \n", sf, min);

	if (th1 > 24)
		printf("A THERAN SONO LE ORE: %d:%d DI DOMANI \n", th1 - 24, th2);
	else
		printf("A THERAN SONO LE ORE: %d:%d \n", th1, th2);
}
