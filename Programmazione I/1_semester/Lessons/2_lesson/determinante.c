/*

Scrivere un programma in C che richiede all’utente di inserire i coefficienti a, b, c di un
polinomio di secondo grado ax 2 + bx + c = 0. Se a è pari a zero, il programma informa che il polinomio
è di grado inferiore al secondo. In caso contrario, il programma calcola il determinante (det) dell’equazione di secondo grado e informa l’utente se le soluzioni sono reali e distinte (det>0),
coincidenti (det=0) o complesse coniugate (det<0).

*/

#include <stdio.h>

int main(){

	int a, b, c, det;

	printf(" \n Inserisci il valore a: ");
	scanf("%d", &a);

	printf(" \n Inserisci il valore b: ");
	scanf("%d", &b);

	printf(" \n Inserisci il valore c: ");
	scanf("%d", &c);

	det = ((b ^ 2) - (4 * a * c));

	if (a == 0)
		printf("Il polinomio è di grado inferiore al secondo");
	
	else{
		if (det > 0)
			printf("\n Determinante maggiore di 0");		
		else if (det == 0)
			printf("\n Determinante uguale a 0");
		else if (det < 0)
			printf(" \n Determinante minore di 0");
	}
}
