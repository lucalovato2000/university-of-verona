/*

Scrivere un programma C che classifica un carattere immesso da tastiera come: alfabetico (a-z oppure A-Z), cifra (0-9), speciale (tutti gli altri). 

*/

#include <stdio.h>

int main(){

	char c;

	printf("Inserisci un carattere a scelta: ");
	scanf("%c", &c);

	if((48<=c)&&(c<=57))
		printf("Il carattere inserito è di tipo numerico!");

	if((65<=c)&&(c<=90) || (97<=c)&&(c<=122))
		printf("Il carattere inserito è una lettera");
	else
		printf("Il carattere che hai inserito è speciale!");
}
