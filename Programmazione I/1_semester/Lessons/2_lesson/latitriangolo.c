/*

Scrivere un programma in C che acquisisce tre numeri. Il programma verifica se i tre numeri
possono rappresentare le dimensioni dei lati di un triangolo: devono essere valori positivi e la somma
di due numeri deve essere maggiore del terzo. In caso il controllo fallisca, il programma deve
stampare un apposito messaggio di errore. Se il controllo è stato superato con successo, il
programma stabilisce che tipo di triangolo è (isoscele, equilatero o scaleno) e stampa un
apposito messaggio a video.

*/

#include <stdio.h>

int main(){

	float n1, n2, n3;

	printf("Inserisci un numero a scelta: ");
	scanf(" \n %f", &n1);
	
	printf("Inserisci un numero a scelta: ");
	scanf(" \n %f", &n2);
	
	printf("Inserisci un numero a scelta: ");
	scanf(" \n %f", &n3);

	if (n1 > 0 && n2 > 0 && n3 > 0){

		if (n1 != n2 && n2 != n3 && n1 != n3)
			printf("I numeri corrispondono ai lati di un triangolo scaleno \n");
		
		else if (n1 == n2 && n2 == n3)
			printf("I numeri non corrispondono ad unn triangolo equilatero! \n");
		
		else if (n1 == n2 || n1 == n3 || n2 == n3)
			printf("I numeri corrispondono ai lati di un triangolo isoscele! \n");
	
	}else
		printf(" \n numeri non validi! RITENTA");
}
