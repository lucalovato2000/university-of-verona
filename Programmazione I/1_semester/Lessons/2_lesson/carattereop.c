/*

Scrivere un programma C che chiede all’utente due numeri interi n1 e n2 e un carattere op
(appartenente all’insieme {‘+’,’-’,’*’,’/’}). Il programma calcola e stampa a video il risultato
dell’operazione corrispondente a op applicata ai numeri n1 e n2.

*/

#include <stdio.h>

int main(){

	int n1, n2, sm, st, m;
	float d;
	char o;

	printf("Inserisci un numero intero: ");
	scanf("\n %d", &n1);

	printf("Inserisci un numero intero: ");
	scanf("\n %d", &n2);

	printf("Inserisci una operazione da eseguire (‘+’,’-’,’*’,’/’):  ");
	scanf("\n %c", &o);

	sm = n1 + n2;
	st = n1 - n2;
	m = n1 * n2;
	d = n1 / n2;

	if (o == '+')
		printf("SOMMA, n1+n2:  %d \n", sm);
	
	else if (o == '-')
		printf("SOTTRAZIONE, n1-n2=  %d \n", st);
	
	else if (o == '*')
		printf("MOLTIPLICAZIONE, n1*n2= %d \n", m);
	
	else if (o == '/')
		printf("DIVISIONE, n1/n2= %f \n", d);
}
