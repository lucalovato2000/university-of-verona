
#include <stdio.h>

int fattoriale(int num){	//parametro formale

	int i,j;

	int p=1;

	for(i=1;i<num;i++)
		p=p*i;
	
	return(p);
}


int main(){

	int n,k;
	int f1,f2,f3;
	int r;

	scanf("%d%d", &n, &k);

	f1=fattoriale(n); //invocazione //parametro attuale
	f2=fattoriale(k);
	f3=fattoriale(n-k);
	
	r=f1/(f2*f3);
	printf("%d", r);

}
