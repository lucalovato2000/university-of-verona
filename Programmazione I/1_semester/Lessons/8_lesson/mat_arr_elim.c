/*

Si scriva un programma che copi tutti i valori di una matrice int M[R][C] in un array di dimensione R*C

*/

#include <stdio.h>
#define R 3
#define C 3
#define N 9

int main(){

	int M[R][C]={  {1,2,3},
				{3,8,5},
				{6,7,8}	};
	
	int V[N];

	int i,j,l;
	int k=0;
	
	for(i=0;i<R;i++){
		for(j=0;j<C;j++){
			V[k]=M[i][j];
			k++;
		}
	}
	
	for(k=0;k<N;k++){
		printf("%d ", V[k]);
		
	}printf("\n");
}
