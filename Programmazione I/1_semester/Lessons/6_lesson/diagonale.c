
//stampare valori diagonale principale 

#include <stdio.h>
#define N 2

int main(){

	int m[N][N];
	int i,j;

	for(i=0;i<N;i++){
		for(j=0;j<N;j++){
			scanf("%d", &m[i][j]);
		}
	}
	
	for(i=0;i<N;i++){
		printf("%3d", m[i][N-1-i]);
	}
	return 0;
}
