/*

Scrivere un programma che data una matrice int M[D1][D2] ed un valore intero x calcoli quante volte x occorre in M.

*/

#include <stdio.h>
#define D1 4
#define D2 4

int main(){

	int i,j,n,cont=0;
	int m[D1][D2]={{1,2,3,4},{5,6,7,8},{9,10,1,2},{3,4,5,6}};

	printf("Inserisci un numero a scelta: ");
	scanf("%d", &n);

	for(i=0;i<D1;i++){
		for(j=0;j<D2;j++){
			if(n==m[i][j])
				cont+=1;
		}
	}

	printf("Il numero inserito è presente nella matrice %d volte \n", cont);

	return 0;
}
