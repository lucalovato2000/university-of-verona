
//contare numero di volte che un elemento si ripete	

#include <stdio.h>
#define R 3
#define C 2


int main(){

	int m[R][C];
	int i,j,k,cont=0;
	
	int c[11]={0};

 
	for(i=0;i<R;i++){
		for(j=0;j<C;j++){
			do{
				scanf("%d", &m[i][j]);

			}while(m[i][j]<0 || m[i][j]>10);
		}
	}

	for(k=0;k<=10;k++){
		cont=0;
			for(i=0;i<R;i++){
				for(j=0;j<C;j++){
					if(m[i][j]==k)
						cont++;
				}
			}

		printf(" %d: %d ", k, cont);
		printf("\n");
	}
}
