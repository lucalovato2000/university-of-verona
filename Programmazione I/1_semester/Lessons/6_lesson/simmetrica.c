
//matrice simmetrica  m[i][j]==[j][i]	

#include <stdio.h>
#define N 3


int main(){

	int m[N][N]={{1,2,3},{2,4,7},{3,7,8}};
	int i,j,simm;

	simm=1;  //flag messo a vero

	for(i=0;i<N &&(simm==1);i++){
		for(j=i+1;j<N && (simm==1);j++){
			if(m[i][j]!=m[j][i])
				simm=0;
		}
	}
		printf("%d",simm);
	return 0;
}
