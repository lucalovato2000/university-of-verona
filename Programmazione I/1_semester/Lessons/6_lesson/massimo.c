
//stampare il massimo matrice

#include <stdio.h>
#define N 2

int main(){

	int m[N][N];
	int i, j, max=0;

	for(i=0;i<N;i++){
		for(j=0;j<N;j++){
			scanf("%d", &m[i][j]);
		}
	}

	for(i=0;i<N;i++){
		for(j=0;j<N;j++){
			if(m[i][j]>max)
				max=m[i][j];
		}
	}

	printf("%d", max);
	return 0;
}
