/*

Scrivere un programma che data una matrice quadrata int M[D][D] calcoli la sua trasposta int N[D][D].

NB: N è la trasposta di M se N[i][j]=M[j][i] per ogni j∈[0,D-1] e i∈[0,D-1]

*/

#include <stdio.h>

#define R 4
#define C 4

int main(){

	int i,j;
	int m[R][C]={{2,3,4,5},{5,4,3,7},{2,3,4,7},{1,8,9,6}};
	int t[R][C]={{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};
	
	for(i=0;i<R;i++){
		for(j=0;j<C;j++){
			t[j][i]=m[i][j];
		}
	}
	
	for(i=0;i<R;i++){
		for(j=0;j<C;j++){
			printf("%d", t[i][j]);
		}
		printf("\n");
	}

	return 0;
}
