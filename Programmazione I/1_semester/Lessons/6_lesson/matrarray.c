/*

Scrivere un programma che memorizza tutte le
righe di una matrice di interi 4x6 in un unico
array di interi lungo 24.

*/

#include <stdio.h>
#define R 4
#define C 6
#define N 24

int main(){
	
	int i,j,v,u=0,k[N];
	int m[R][C]={	{1,2,3,4,5,6},
				{7,8,9,10,11,12},
				{13,14,15,16,17,18},
				{19,20,21,22,23,24}	};

	for(i=0;i<R;i++){
		for(j=0;j<C;j++){
				k[u]=m[i][j];
				u++;
		}
	}
		
	for(v=0;v<N;v++){
		printf("%d ", k[v]);
	}
	printf("\n");

	return 0;
}
