/*

Inserire un numero e calcolare il fattoriale

*/

#include <stdio.h>

int main(){

	int num, fatt, i;
	
	printf("Inserisci un numero a scelta: ");
	scanf("%d", &num);

	while(num<0){
		printf("Inserisci un numero a scelta compreso maggiore di 0: ");
		scanf("%d", &num);
	}
	
	fatt=1,i=2;
	
	while(i<=num){
	
		fatt=fatt*i;
		i++;
	}
	printf("Il numero risultante è: %d \n", fatt);
}
