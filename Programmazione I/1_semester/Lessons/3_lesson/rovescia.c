/*

Scrivere un programma che inverta la posizione delle cifre di un numero intero inserito dall’utente (4321 > 1234). 
Inoltre, il programma avvisa se il numero inserito è palindromo (si legge allo stesso modo da sinistra a destra e viceversa, es: 121,32123, …). 

*/


#include <stdio.h>

int main(){
	 
	int n, cifradx;
	
	printf("Inserisci un numero a scelta: ");
	scanf("%i", &n);

	while(n!=0){
	
		cifradx= n%10;
		printf("%i", cifradx);
		n=n/10;
	}
	printf("\n");	
}
