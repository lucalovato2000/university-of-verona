/*

Inserire un carattere minuscolo, stampare a video il caratter successivo, se inserisce Z deve stampare A. Se il carattere non è una lettere minuscolo il programma termina.

*/

#include <stdio.h>

int main(){

	char c;

	printf("Inserisci un carattere minuscolo a scelta: ");
	scanf(" %c", &c); //lascia spazio 

	while(c>='a' && c<='z'){   //termini per eseguire il ciclo
		if(c=='z')
			printf("Ecco il tuo carattere %c \n", 'a');
		else
			printf("Ecco il tuo carattere %c \n", c+1);

	printf("Inserisci un carattere minuscolo a scelta: ");
	scanf(" %c", &c); //lascia spazio
	
	}
}
