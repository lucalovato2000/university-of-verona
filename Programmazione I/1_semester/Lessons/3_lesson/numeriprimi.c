/*

Definire quando un numero dato riulta primo

*/

#include <stdio.h>

int main(){

	int n,d;

	printf("Inserisci un numero: ");
	scanf("%d", &n);

	d=2;

	while(d<n && n%d!=0)
		d++;

	if(d<n)
		printf("Numero non primo \n");
	else 
		printf("Numero primo \n");
}
