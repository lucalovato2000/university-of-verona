/*

Scrivere un programma che acquisisca un indice X tra 0 e 9 (controllare validità del valore inserito) e una sequenza di 10 numeri interi.
Il programma dovrà stampare la somma dei numeri in posizioni minori di X e il prodotto dei numeri in posizioni successive a X.

*/


#include <stdio.h>

int main(){

	int n, i, in=0, sm, mp;

	do{
		printf("Inserisci un valore: ");
		scanf("%d", &i);

	}while(i<0 || i>10);

	while(in<10){

		printf("Inserisci un valore intero: ");
		scanf("%d", &n);

		in++;	
		
		if(n<i)
			sm+=n;
		else
			mp*=n;
	}
	printf("Somma risultante: %d \n", sm);
	printf("Moltiplicazione risultante: %d \n", mp);
}
