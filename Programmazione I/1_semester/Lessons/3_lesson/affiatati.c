/*

Scrivere un programma che, chiesti all'utente due numeri interi positivi a e b comunica all'utente se si tratta di una coppia di numeri affiatati oppure no. 
Definiamo due numeri affiatati se sono diversi tra loro e la loro somma è pari al prodotto delle cifre che li compongono. 
Ad esempio (14, 34) e (63, 81) sono coppie di numeri affiatati perché

14 + 34 = 1 x 4 x 3 x 4 = 48
63 + 81 = 6 x 3 x 8 x 1 = 144

*/

#include <stdio.h>

int main(){

	int num, num2, sm, pr=1, pr1=1, pr2=1;

	do{
		printf("Inserisci due numeri interi positivi: ");
		scanf("%d", &num);
		
		printf("Inserisci un valore intero positivo: ");
		scanf("%d", &num2);

	}while(num <0 || num2<0);

	sm=num+num2;
	
	while(num!=0){
		pr1*= num % 10;
		num = num / 10;

		pr2*= num2 % 10;
		num2 = num2 / 10;	
	}

	pr=pr1*pr2;		
		
	if(sm==pr)
		printf("Sono affiatati");
	else
		printf("Non sono affiatati");
}
