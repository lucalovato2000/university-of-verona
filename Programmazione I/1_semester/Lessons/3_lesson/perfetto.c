/*

Stampo i numeri perfetti minori di n maggiori di 0.
Un numero è perfetto se la sommatoria dei dei divisori è minore di k.

*/

#include <stdio.h>

int main(){
	
	int n, i=1, div=1, sum=0;

	do{
		printf("Inserisci un numero: ");
		scanf("%d", &n);

	}while(n<0);   //finchè n<0 chiedi il numero

	while(i<n){

		while(div<i){
		
			if(i%div==0)
				sum+=div;
			div++;	
		}
		i++;	
	}

	if(sum==i)
		printf("Numero perfetto \n");
	else
		printf("Numero non perfetto \n");
}
	