/*

Stampare asterischi quanto il valore del numero inserito, -1 per uscire

*/

#include <stdio.h>

int main(){

	int i, n;
	
	while(n!=-1){ 	//termina con valore -1
		
		printf("Inserisci il numero maggiore di 0: ");
		scanf("%d", &n);		
		
		if(n>0){
			i=1;
		
			while(i<=n){
				printf("*");
				i++;
			}	
			printf("\n");
		}
	}
}
