/*

Scrivere sottoprogramma conta che preso come parametro un array di caratteri e la sua dimensione, conta quante vocali minuscole sono presenti nell’array e restituisce il valore al chiamante.
Il main chiede in ingresso all’utente un array di 10 caratteri, richiamando il sottoprogramma conta stampa il numero di vocali nell’array.

*/


#include <stdio.h>
#define N 10

int conta(char vm[]){
	
	int i;	
	int cont=0;
	
	for(i=0;i<N;i++){
		if(vm[i]=='a'|| vm[i]=='e'|| vm[i]=='i'|| vm[i]=='o'|| vm[i]=='u')
			cont++;
	}

	return(cont);
}


int main(){

	int i,ris;
	char vm[N];
	
	for(i=0;i<N;i++){
		printf("Inserisci una lettera a scelta:");
		scanf(" %c", &vm[i]);
	}

	ris=conta(vm);
	printf("Ha inserito all'interno del programma %d vocali", ris);
}
