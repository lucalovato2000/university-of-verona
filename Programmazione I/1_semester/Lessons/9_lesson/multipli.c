/*

Programma che legge multipli di tre 

*/

#include <stdio.h>
#define N 10


void leggi(int v[N]){
	
	int i;

	for(i=0;i<N;i++){
		scanf("%d", &v[i]);
	}
}	


void stampa(int x[N]){
	int i;
	
	for(i=0;i<N;i++){
		printf("%3d", x[i]);
	}
}


int conta(int a[N]){

	int num=0;
	int i;

	for(i=0;i<N;i++){
		if(a[i]%3==0)
			num++;
		
	}
	return num;
}

int main(){
	
	int v[N];
	int c;
	
	leggi(v);
	stampa(v);
	
	c=conta(v);
	
	printf("%d", c);
}
