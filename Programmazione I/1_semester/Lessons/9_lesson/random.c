/*

Libreria che genera valori random (usato per generare valori di matrici e array)

*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int main(){
	int random;	//valore random	
	srand(time(NULL));	//seme random
	random=rand();	//numero random
	printf("%d", random);	

}


/*
casuale = rand()%11; //da 0 a 10

casuale = 30+rand()%41;

DIFFERENZA FRA I VALORI +1 E SOMMA L'ESTREMO INFERIORE
*/
