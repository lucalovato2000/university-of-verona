/*

Scrivere un sottoprogramma C che riceve come parametri due matrici quadrate 4x4 di numeri interi, chiamate m1 e m2. Il sottoprogramma calcola e restituisce al chiamante il numero di volte che il valore
m1[i][j]=m2[i][j]

Esempio: Date in ingresso le due matrici sotto il sottoprogramma restituirà il valore 4.

9 4 9 2 	1 2 3 4
3 4 6 8		3 4 1 1
5 2 3 4		1 1 7 4
3 4 5 2		3 6 7 8

*/


#include <stdio.h>
#define N 4
#define M 4


int conta(int M1[M][N], int M2[M][N]){
	int i,j;
	int cont=0;
	
	for(i=0;i<M;i++){
		for(j=0;j<N;j++){	
			if(M1[i][j]==M2[i][j])
				cont++;
		}	
		return(cont);
	}
}


int main(){

	int M1[M][N]={{9,4,9,2},{3,4,6,8},{5,2,3,4},{3,4,5,2}};
	int M2[M][N]={{1,2,3,4},{3,4,1,1},{1,1,7,4},{3,6,7,8}};
	int ris;
	
	ris = conta(M1,M2);

	printf("Il valore risultante è: %d \n", ris);
}
