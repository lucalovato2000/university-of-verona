#include <stdio.h>

#define N 5

//prototipi
void leggi(int m[N][N]);	
void stampa(int m[][N]);
int cerca(int m[][N]);


void leggi(int m[N][N]){
	int i,j;

	for(i=0;i<N;i++)
		for(j=0;j<N;j++)
			scanf("%d", &m[i][j]);
}

void stampa(int m[N][N]){
	int i,j;

	for(i=0;i<N;i++){
		for(j=0;j<N;j++){
			printf("%3d", m[i][j]);
		}
		printf("\n");
	}
}

int cerca(int m[N][N]){
	int r,c;
	
	do{
		scanf("%d", &r);
	}while(r<0 || r>N);
	
	do{
		scanf("%d", &c);
	}while(c<0 || c>=N);

	return(m[r][c]);
}

void main(){

	int m[N][N];
	int val;

	leggi(m);
	stampa(m);
	val=cerca(m);
	
	printf("%d", val);
}
