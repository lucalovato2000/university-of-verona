/*

Scrivere un programma C composto da:

● Una funzione leggi che richiede all’utente di inserire da standard input una matrice 4x5.

● Una funzione trasposta che riceve come parametro una matrice a 4x5 e una matrice b 5x4. La funzione calcola la trasposta di a e la memorizza in b.

○ Una matrice a con i righe e j colonne può essere trasposta in una matrice b con j righe e i colonne, dove ogni elemento b(j,i) = a(i,j).

*/


#include <stdio.h>
#define R 4
#define C 5


void leggi(int M1[R][C]){
	int i,j;

	for(i=0;i<R;i++){
		for(j=0;j<C;j++){
			scanf("%d", &M1[i][j]);
		}	
	}
}	


void stampa(int M1[R][C]){
	int i,j;

	for(i=0;i<R;i++){
		for(j=0;j<C;j++){
			printf(" %d", M1[i][j]);
		}
		printf("\n");
	}
	printf("\n\n");
}	



void trasposta(int M1[R][C], int M2[C][R]){
	int i,j;

	for(i=0;i<R;i++){
		for(j=0;j<C;j++){
			M2[j][i]=M1[i][j];
		}
	}
}

int main(){

	int M1[R][C];
	int M2[C][R];
	
	int i,j;

	leggi(M1);
	stampa(M1);
	
	trasposta(M1,M2);

	for(i=0;i<C;i++){
		for(j=0;j<R;j++){
			printf(" %d", M2[i][j]);
		}
		printf("\n");
	}
}
