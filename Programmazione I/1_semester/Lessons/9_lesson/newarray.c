/*
Scrivere un programma C che acquisisce un array a1 di 10 interi. Il programma crea e visualizza un nuovo array a2 contenente in ciascuna posizione i:

● la somma dei numeri di a1 fino alla posizione i
inclusa, se i è pari
● il prodotto dei valori di a1 nelle posizioni i e i-1, se i
è dispari.

Per esempio se l’utente inserisce l’array:
1 2 4 2 8 6 6 8 8 1

programma creerà e visualizzerà l’array:
1 2 7 8 17 48 29 48 45 8

*/

//la somma viene fatta solo fra il valore all'indice e il valore dell'indice meno 1

#include <stdio.h>
#define N 5

int main(){

	int A1[N],A2[N];
	int i,j,n;

	for(i=0;i<N;i++){
		printf("Inserisci un valore a scelta: ");
		scanf("%d", &A1[i]);

		if(i%2==0)
			A2[i]=A1[i]+A1[i-1];
		else
			A2[i]=A1[i]*A1[i-1];
	}

	for(i=0;i<N;i++){
		printf(" %d", A2[i]);
	}
	
	return 0;
}
