//dictionary.c
#include <stdio.h>
#define L_KEY 20
struct entry{
    char word[L_KEY];
    char definition[100];
};


struct entry * creazione_dizionario(){
    //definizione statica del dizionario
    static struct entry dictionary[]= {
        {"Alambicco", "Strumento utilizzato per la distillazione"},
        {"Albero", "Struttura dati efficiente"},
        {"All-grain","Metodo di produzione per la birra"},
        {"Cane", "Animale che abbaia"},
        {"Gatto", "Animale peloso miagolante"},
        {"Ritardatario", "Persona non puntuale"},
        {"Smartphone", "telefono intelligente a batteria"},
        {"Studente", "Persona per bene molto studiosa"},
        {"ZTL", "Zona a traffico limitato"}
    };
    return dictionary;
}


int compare_strings(char *s1, char *s2 ){
    int pos=0;
    while( *(s1+pos) == *(s2+pos) && *(s1+pos) && *(s2+pos) )
        pos++;

    return *(s1+pos) - *(s2+pos); //ritorna 0 se s1==s2
}

int lookup_binary(char *key, struct entry dictionary[], int length, int start){
    int left = start, right= length-1, middle;
    int comp;
    
    while(left <= right){
        middle = (left+right) / 2;
        comp = compare_strings(key, dictionary[middle].word);
        if(comp == 0)
            return middle; //l'ho trovato!
        else if(comp <0)
            right = middle-1;
        else
            left = middle +1;
    }
    return -1; //non c'e'!!
}


int lookup_linear(char *key, struct entry dictionary[], int length, int start){
    
    if (start == length) //passo base, non c'e'!
       return -1;
    else if( compare_strings(key, dictionary[start].word ) == 0 ) //passo base, c'e'!
        return start;
    else
        lookup_linear(key, dictionary,9, ++start); //passo ricorsivo
        };
    


int main(void) {
    char key[L_KEY];
    struct entry *dictionary = creazione_dizionario();
    int start=0; //rappresenta l'indice iniziale da dove iniziare la ricerca nel dizionario. Per default è 0.
    
    //inserimento chiave 1 da tastiera:
    printf("Inserire la parola da ricercare: ");
    scanf("%19s", key);
    //ricerca della chiave 1 nel dizionario:
    printf("La parola cercata di trova in posizione: %i\n", lookup_linear(key, dictionary,9, start));
    
    //inserimento chiave 2 da tastiera:
    printf("Inserire la parola da ricercare: ");
    scanf("%19s", key);
    //ricerca della chiave 2 nel dizionario:
    printf("La parola cercata di trova in posizione: %i\n", lookup_binary(key, dictionary,9, start));
    
  return 0;
}

