#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define END -1

struct item {
    int value;
    struct item *next;
};

struct collection_t {
    struct item *head_even;
    struct item *head_odd;
};

struct item *append_item(struct item *head, int value);
void set_collection(struct collection_t *collection, int v[]);
void print(struct collection_t *head, bool even);

/**
 * Program entry point
 * Output:
 * Collezione vuota
 * 10 6 0 8 4
 * 1 9 5 1 3
 */
int main() {
    struct collection_t *collection = (struct collection_t *)malloc(sizeof(struct collection_t));
    int values[] = {1, 9, 10, 6, 5, 1, 3, 0, 8, 4, END};

    collection->head_even = NULL;
    collection->head_odd = NULL;
    print(collection, true);
    set_collection(collection, values);
    print(collection, true);
    print(collection, false);
    return 0;
}

/**
 * Popola una collection con gli elementi di un vettore di interi terminato da END:
 * i numeri pari nella lista even e quelli dispari nella lista odd.
 */
void set_collection(struct collection_t *collection, int v[]) {
    int i = 0;

    while (v[i] != END) {
        if (v[i] % 2 == 0) {
            collection->head_even = append_item(collection->head_even, v[i]);
        } else {
            collection->head_odd = append_item(collection->head_odd, v[i]);
        }
        i++;
    }
}

/**
 * Aggiunge un elemento in fondo alla lista
 */
struct item *append_item(struct item *head, int value) {
    struct item *nuovalista = (struct item *)malloc(sizeof(struct item));
    nuovalista->value = value;
    nuovalista->next = NULL;

    struct item *current = head;

    if (head == NULL) {
        head = nuovalista;
    } else {
        while (current->next != NULL) {
            current = current->next;
        }

        current->next = nuovalista;
    }

    return head;
}

/**
 * Se even è true, stampa solo gli elementi della lista pari, altrimenti stampa quelli della lista dispari.
 * Se la lista completa oppure quella che si chiede di stampare (pari o dispari) non ha elementi stampa "Collezione vuota"
 */
void print(struct collection_t *head, bool even) {
    if (head == NULL) {
        printf("Collezione vuota");
    } else {
        struct item *current = (even) ? head->head_even : head->head_odd;

        if (current == NULL) {
            printf("Collezione vuota");
        } else {
            while (current) {
                printf("%i ", current->value);
                current = current->next;
            }
        }
    }
    printf("\n");
}
