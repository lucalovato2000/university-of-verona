#include <stdio.h>
int higherdigit(int, int);

int higherdigit(int n, int m){
	int max=0;
	int cont1, cont2;
	cont1=0;
	cont2=0;
	
	// caso numero nullo
	if(n==0)	// se n == 0 cont1++
		cont1++;
	// conto le cifre
	while(n>0){	// se n > 0 (1 cifra)
		if(n%10>max)	// (n%10)   5%10 = 5 > max ? (max = 0) si
						// 			13 % 10 = 1 > max ? (max = 0) si
			max=n%10;	// 			max = 1
		n/=10;	
		cont1++;
	}
	// secondo numero nullo
	if(m==0)
		cont2++;
	//conto le cifre
	while(m>0){
		if(m%10>max)
			max=m%10;
		m/=10;
		cont2++;
	}	
	// restituisco la risposta
	if(cont1==cont2)
		return(max);
	else return(-1);
}



int main(){
	int a, b, r;
	
	printf("\n Inserisci due interi non negativi: \n");
	do{
		scanf("%d",&a);
	}while(a<0);
	do{
		scanf("%d",&b);
	}while(b<0);
	
	r=higherdigit(a,b);
	
	printf("\n Esito su %d e %d: %d",a,b,r);
	
	return 0;
}
