#include <stdlib.h>
#include <stdio.h>

/* ci sono diverse soluzioni al problema. Una tra queste è rappresentata dal seguente codice:
 */


struct list {
    int head;
    struct list *tail;
};

int * f(){
    int tmp;
    return &tmp;
}

struct list *construct_list(int head, struct list *tail){
    struct list *this = (struct list *) malloc(sizeof(struct list));
    this->head = head;
    this->tail = tail;
    return this;
}


void recursive(struct list *this) {
    if(this != NULL)
        recursive(this->tail);
    else {
    //qui ci si trova nella situazione rappresentata in figura.
    };
}

int * x;

int main(void) {
    struct list * a = construct_list(1,
                                     construct_list(2, NULL));
    x = f();
    recursive(a);
}
