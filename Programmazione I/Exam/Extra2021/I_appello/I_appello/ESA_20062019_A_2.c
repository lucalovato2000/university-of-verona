#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

struct chord_t *append_chord(struct chord_t *head, char note, bool minor);
void strtosong(struct chord_t *head, char *chords);
void print_song_r(struct chord_t *head);

struct chord_t {
    char note;
    bool minor;
    struct chord_t *next;
};

/*
 * Create a chord and append it to an existing song or a new one
 */
struct chord_t *append_chord(struct chord_t *head, char note, bool minor) {
    struct chord_t *new = (struct chord_t *)malloc(sizeof(struct chord_t));
    new->note = note;
    new->minor = minor;
    struct chord_t *chord = head;

    if (chord == NULL) {
        // Empty list
        head = new;
    } else {
        while (chord->next != NULL) {
            chord = chord->next;
        }
        chord->next = new;
    }

    return head;
}

/*
 * Add chords to an existing song from a string
 * i.e. "G Cm G"
 */
void strtosong(struct chord_t *head, char *chords) {
    while (*chords) {
        char note = *chords;
        chords++;
        bool minor = false;

        if (*chords == 'm') {
            minor = true;
            chords++;
        }
        append_chord(head, note, minor);

        if (*chords) {  // If not at the end of the string
            chords++;   // skip the space between notes
        }
    }
}

/**
 * Print a song with recursion
 */
void _print_song_r(struct chord_t *head) {
    if (head != NULL) {
        printf("%c", head->note);
        if (head->minor) {
            printf("m");
        }
        printf(" ");

        _print_song_r(head->next);
    }
}

/**
 * Print a song or "Empty song\n" if NULL is given
 * with recursion
 */
void print_song_r(struct chord_t *head) {
    if (head == NULL) {
        printf("Empty song\n");
    } else {
        _print_song_r(head);
    }
}

/*
 * Program entry point
 * Output atteso:
 * Em G D C Em
 * Em G D C Em G Cm G
 */
int main() {
    struct chord_t *song = NULL;
    song = append_chord(song, 'E', true);
    append_chord(song, 'G', false);
    append_chord(song, 'D', false);
    append_chord(song, 'C', false);
    append_chord(song, 'E', true);
    print_song_r(song);
    printf("\n");

    char chords[] = "G Cm G";
    strtosong(song, chords);
    print_song_r(song);
    printf("\n");
    return 0;
}
