#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

struct model_t *insert_on_head(struct model_t *list, char letter, bool extended);
struct model_t *strtolist(struct model_t *list, char *models);
void print_list_r(struct model_t *list);

struct model_t {
    char letter;
    bool extended;
    struct model_t *next;
};

/*
 * Create a model and append it to an existing list or a new one
 * with a on head insertion
 */
struct model_t *insert_on_head(struct model_t *list, char letter, bool extended) {
    struct model_t *new = (struct model_t *)malloc(sizeof(struct model_t));
    new->letter = letter;
    new->extended = extended;

    if (list != NULL) {
        // Not an empty list
        new->next = list;
    }

    list = new;
    return list;
}

/*
 * Add models to an existing list from a string
 * i.e. "Ax Cx B"
 */
struct model_t *strtolist(struct model_t *list, char *models) {
    while (*models) {
        char letter = *models;
        models++;
        bool extended = false;

        if (*models == 'x') {
            extended = true;
            models++;
        }
        list = insert_on_head(list, letter, extended);

        if (*models) {  // If not at the end of the string
            models++;   // skip the space between letters
        }
    }
    return list;
}

/**
 * Print a list or "Empty list\n" if NULL is given
 * with recursion
 */
void print_list_r(struct model_t *list) {
    if (list != NULL) {
        printf("%c", list->letter);
        if (list->extended) {
            printf("x");
        }
        printf(" ");

        print_list_r(list->next);
    }
}

/*
 * Program entry point
 * Output atteso:
 * Ex B Xx Ax A
 * B Cx Ax Ex B Xx Ax A
 */
int main() {
    struct model_t *list = NULL;
    list = insert_on_head(list, 'A', false);
    list = insert_on_head(list, 'A', true);
    list = insert_on_head(list, 'X', true);
    list = insert_on_head(list, 'B', false);
    list = insert_on_head(list, 'E', true);
    print_list_r(list);
    printf("\n");

    char models[] = "Ax Cx B";
    list = strtolist(list, models);
    print_list_r(list);
    printf("\n");
    return 0;
}
