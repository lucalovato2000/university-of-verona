#include <stdbool.h>
#include <stdio.h>

void print_dividers(int n, int whitelist[], int size_white, int blacklist[], int size_black);
bool in(int n, int numbers[], int size);

/*
 * Stampa i divisori interi di n, seguiti da uno spazio,
 * che sono presenti in whitelist e non sono presenti in blacklist.
 * Usa la funzione in() per verificare la presenza in blacklist.
 */
void print_dividers(int n, int whitelist[], int size_white, int blacklist[], int size_black) {
}

/*
 * Restituisce true se n è presente in numbers, false altrimenti,
 * utilizzando la ricorsione.
 */
bool in(int n, int numbers[], int size) {
}

/*
 * Program entry point
 * Output atteso:
 * 2 3 5
 */
int main() {
    int whitelist[] = {2, 3, 4, 5, 10};
    int blacklist[] = {4, 10, 20};
    print_dividers(120, whitelist, 5, blacklist, 3);
    printf("\n");
    return 0;
}
