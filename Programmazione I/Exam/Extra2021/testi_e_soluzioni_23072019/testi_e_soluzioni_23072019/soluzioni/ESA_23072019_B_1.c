#include <stdio.h>
#include <stdlib.h>

// struttura dati
struct node_t{
	int value;
	struct node_t * next;
};

int conta(struct node_t *, int);
struct node_t* delfromlist(struct node_t *, int);
struct node_t * rimuovi(struct node_t *, int);



// conta quante occorrenze di x sono presenti in lista. Scrivere funzione ricorsiva

int conta(struct node_t * lista, int x){
	
	if(lista==NULL)
		return 0;
	
	if(lista->value==x)
		return 1+conta(lista->next, x);
	else return conta(lista->next, x);	
}


// cancella dalla lista tutte le occorrenze di num

struct node_t * rimuovi(struct node_t * lista, int num){
	  struct node_t  *curr, *prec, *canc;

	  curr = lista;
	  prec = NULL;      
	  while(curr){
	    if(curr->value==num){
	      canc = curr;
	      curr = curr->next;     
	      if(prec!=NULL)
	        prec->next = curr;
	      else
	        lista = curr;
	      free(canc);
	    }
	    else{
	      prec=curr;
	      curr = curr->next;     
	    }
	  }
	  return lista;

	}


/* cancella dalla lista l tutti i nodi presenti piu' di n volte. Richiama al suo interno conta e rimuovi (questo e' un suggerimento, si puo' fare altrimenti, ma conta deve
	essere definita) */

struct node_t* delfromlist(struct node_t * l, int n){
	struct node_t *curr;
	int cont;
	int val;
  
	curr=l;
	
		while(curr!=NULL){
			cont=conta(curr,curr->value);
			if(cont>n){
				val=curr->value;
				//la lista cambia a causa di rimuovi, bisogna trovare in anticipo quale sara' il prossimo nodo da visitare
				do{
					curr=curr->next;
				}while(curr!=NULL && curr->value==val);
				l=rimuovi(l,val);
			}
			else curr=curr->next;
		}
			
	return l;		
}
		

// stampo la lista: usato nel main

void stampa_lista(struct node_t * n){
	struct node_t * c = n;
	while(c != NULL){
		printf("%d \t", c->value);
		c = c->next;
	}
	printf("\n");
}

// inserimento in coda: usato nel main per creare la lista di partenza

struct node_t * inserisciInCoda(struct node_t * lista, int num){
  struct node_t *prec;
  struct node_t *tmp;

  tmp = (struct node_t *) malloc(sizeof(struct node_t));
  if(tmp != NULL){
    tmp->next = NULL;
    tmp->value = num;
    if(lista == NULL)
      lista = tmp;
    else{
      /*raggiungi il termine della lista*/
      for(prec=lista;prec->next!=NULL;prec=prec->next);
      prec->next = tmp;
    }
  } else
      printf("Memoria esaurita!\n");
  return lista;
}


int main(){
	struct node_t * head=NULL;
	struct node_t * new=NULL;
	int num;
	head = inserisciInCoda(head,7);
	head = inserisciInCoda(head, 3);
	head = inserisciInCoda(head, 2);
	head = inserisciInCoda(head, 3);
	head = inserisciInCoda(head, 3);
	head = inserisciInCoda(head, 2);
	head = inserisciInCoda(head, 4);
	printf("\n Lista originale: ");
	stampa_lista(head);
	printf("\n Inserisci un numero positivo: ");
	do{
		scanf("%d", &num);
	}while(num<=0);
	head=delfromlist(head,num);
	
	printf("\n Lista finale: ");
	stampa_lista(head); 
	return 0;
}