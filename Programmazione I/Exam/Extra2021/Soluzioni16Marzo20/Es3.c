#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

typedef struct{
    char nome[10]; // il nome della citt�
    char data[6];  // la data rappresentata con una formato gg-mm
    float rain; // quantit� pioggia in mm
}city;

typedef struct{
    char nome[10]; // il nome della citt�
    float totRain; // quantit� totale pioggia in mm
}cityTot;


void inizializza_strutture_dati(char * nomiCitta[],int ln,char * date[],int ln_date,city c[], cityTot t[]);
void leggi_dati_pioggia(city c[],int ln);
void crea_datiTot(city c[], cityTot t[], int ln_t );
void stampa_datiTot(cityTot t[],int ln_t );
float max_pioggia(cityTot t[],int ln_t );
void stampa_citta_max_piovose(cityTot t[],int ln_t);
void max_scarto (cityTot t[],int ln_t);

int main(void){
    char * nomi_citta[10]={"Verona","Milano", "Napoli","Palermo", "Venezia", "Kyoto", "Rio", "Livorno","Gotham", "Parigi"};
    char * date_rilev[4]={"01-01","13-04","22-06","05-10"};
    city dati[10*4];
    cityTot datiTot[10];
    inizializza_strutture_dati(nomi_citta,10, date_rilev, 4, dati, datiTot );
    leggi_dati_pioggia(dati, 40);
    crea_datiTot(dati,datiTot,10);
    stampa_datiTot(datiTot, 10);
    stampa_citta_max_piovose(datiTot,10);
    max_scarto(datiTot,10);
}

void inizializza_strutture_dati(char * nomiCitta[],int ln,char * date[],int ln_date,city c[], cityTot t[]){
    int n=0;
    for(int i=0; i< ln;i++ ){
        strcpy(t[i].nome, nomiCitta[i]);
        t[i].totRain=0.0;
        for (int k=0;k<ln_date;k++,n++){
            strcpy(c[n].nome, nomiCitta[i]);
            strcpy(c[n].data, date[k]);
        }
        
    }
}

void leggi_dati_pioggia(city c[],int ln){
    for (int i=0;i<ln;i++){
        printf("inserisci la quantita` di pioggia di %s nel giorno %s =>>", c[i].nome, c[i].data);
        scanf("%f", &(c[i].rain));
    }
}
void crea_datiTot(city c[], cityTot t[], int ln_t ){
    int n=0;
    for(int i=0; i< ln_t;i++ ){
        while(strcmp(t[i].nome, c[n].nome)==0){
            t[i].totRain+=c[n].rain;
            n++;
        }
    }
}

void stampa_datiTot(cityTot t[],int ln_t ){
    for(int i=0; i< ln_t;i++ ){
        printf("nella la citta` %s sono piovuti %f mm di pioggia\n", t[i].nome,t[i].totRain);
    }
}

float max_pioggia(cityTot t[],int ln_t ){
    float max=0.0;
    for (int i=0;i<ln_t;i++){
        if(max< t[i].totRain)max= t[i].totRain;
    }
    return max;
}

void stampa_citta_max_piovose(cityTot t[],int ln_t){
    float max= max_pioggia(t,ln_t);
    puts("ecco le citta` piu` piovose");
    for (int i=0;i<ln_t;i++){
        if(max== t[i].totRain)
            printf("%s ha piovosita` massima con %f mm\n", t[i].nome, max);
    }
}

void max_scarto (cityTot t[],int ln_t){
    float max, scarto;
    max=0;
    for (int i = 0; i < ln_t; i++){
        for (int j=i+1; j < ln_t; j++){
            scarto= fabs(t[i].totRain - t[j].totRain); // fabs � il valore assoluto
            if (scarto>max) max=scarto;
        }
    }
    printf("lo scarto massimo tra le piovosita` e` %f\n",max);
}




