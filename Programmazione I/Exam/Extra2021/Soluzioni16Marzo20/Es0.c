#include<stdio.h>

typedef struct{
	int codice;
	float prezzo;
	int sconto;
} t_articolo;

int main(){
	t_articolo art;
	int nscontati=0;
	float spesatotale=0;

	do{
		scanf("%d", &art.codice);
		if(art.codice!=-1){
			scanf("%f", &art.prezzo);
			scanf("%d", &art.sconto);
			if(art.sconto>0)
				nscontati++;
			spesatotale += art.prezzo * (1-art.sconto/100);
		}
	} while (art.codice!=-1);

	printf("\nNumero articoli scontati: %d\nSpesa totale: %f", nscontati, spesatotale);
}
