#include <stdio.h>
#include <stdbool.h>

// ============================================================================

typedef struct{
  int hour;
  int minutes;
  int seconds;
}time;

// ============================================================================

time insertTime();
bool checkTime(time t);
time elapsedTime(time t1, time t2);

// ============================================================================

int main(void) {
  time t1,t2,t3;
  t1 = insertTime();
  t2 = insertTime();
  t3 = elapsedTime( t1, t2 );

  printf( "Il tempo trascorso tra %.2i:%.2i:%.2i e %.2i:%.2i:%.2i e' %i:%.2i:%.2i.\n",
          t1.hour, t1.minutes, t1.seconds,
          t2.hour, t2.minutes, t2.seconds,
          t3.hour, t3.minutes, t3.seconds);

  return 0;
}

/********************************************************************
 * Richiede di inserire un istante temporale e restitiusce la 
 * corrispondente struttura. Se l'istante inserito non e' corretto 
 * viene richiesto nuovamente.
 ********************************************************************/
time insertTime(){
  int hour, minutes, seconds;
  time t;
  bool end = false;

  while( !end ){ 
    printf( "Inserire un istante temporale nel formato h:m:s.\n" );
    scanf( "%i:%i:%i", &hour, &minutes, &seconds );
    t.hour = hour;
    t.minutes = minutes;
    t.seconds = seconds;
    end = checkTime( t );
    if( !end ){
      printf( "L'istante temporale non e' valido.\n" );
    }
  }
  return t; 
}

/********************************************************************
 * Verifica se l'istante temporale e' corretto.
 ********************************************************************/
bool checkTime(time t ){
  int n;

  // secondi compresi tra 0 e 59
  if( t.seconds < 0 || t.seconds > 59 ){
    return false;
  }
  // minuti compresi tra 0 e 59
  if( t.minutes < 0 || t.minutes > 59 ){ 
    return false;
  }
  // minuti > 0 e < 23
  if( t.hour < 0 || t.hour > 23 ){ 
    return false;
  }
 
  return true;
}

/********************************************************************
 * Calcola la differenza tra due istanti temporali
 ********************************************************************/
time elapsedTime(time t1, time t2 ){
  int n1 = t1.seconds + t1.minutes * 60 + t1.hour * 60 * 60;
  int n2 = t2.seconds + t2.minutes * 60 + t2.hour * 60 * 60;
  // calcolo la differenza in secondi   
  int diff = n2 - n1;   
  int h, m, s;
  s = diff % 60;
  diff = diff / 60;
  m = diff % 60;
  h = diff / 60;
  time t = {h, m, s};
  return t;
}

