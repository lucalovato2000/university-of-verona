#include <stdlib.h>
#include <stdio.h>

/* ci sono diverse soluzioni al problema. Una tra queste è rappresentata dal seguente codice:
 */


struct list {
    int head;
    struct list *tail;
};

struct list *static_pointer_to_list;    // [DATA] variabile globale

struct list *genera_lista_din(int i){
    struct list *list;

    if (i<1)    // se intero passato è minore di 1 (0, -1, ...)
	    return NULL;
    
    else{
        list = (struct list *)malloc(sizeof(struct list));   // crea un nodo di nome list con una malloc
        list->head=i;    // lista->head = numero inserito in input 
        i--;    // decrementa numero
        list->tail=genera_lista_din(i);  // lista->tail = funzione ricorsiva finchè i < 1
    }
	return list;
}



int main(void) {
    struct list *pointer_to_list=genera_lista_din(1);   // [STACK] variabile nel main
    
    pointer_to_list=genera_lista_din(2);
    // doppia invocazione, senza free, memory leak



    // dopo questa invocazione, non avendo effettuato una free dello spazio allocato con la precedente invocazione di genera_lista_din(1), si è creato un memory leak (indirizzi 0x000164-0x000168).

    static_pointer_to_list = pointer_to_list;   // [DATA indirizzo uguale a STACK]
    // In questo preciso punto la memoria si trova come in immagine.
}
