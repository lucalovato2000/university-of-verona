#include <stdio.h>   // printf()
#include <stdlib.h>  // malloc(size)
#include <string.h>  // strcpy(dest, src), strlen(string)

struct site_t {
    char *name;
    struct site_t *next;
};

struct path_t {
    struct site_t *start;
    char *name;
};

struct path_t *create_path(char *name);
void print_path(struct path_t *path);
void append_site(struct path_t *path, char *name);
void append_sites(struct path_t *path, char *names[], int length);

/**
 * Program entry point
 * 
 * Output:
 * Via per il Brennero:
 * Verona
 * Rovereto
 * Trento
 * Bolzano
 * Brennero
 */

int main() {
    struct path_t *path = create_path("Via per il Brennero");
    append_site(path, "Verona");
    append_site(path, "Rovereto");
    char *sites[] = {"Trento", "Bolzano", "Brennero"};
    append_sites(path, sites, 3);
    print_path(path);
    return 0;
}

/**
 * Crea e ritorna un nuovo percorso (path) di nome name.
 */
struct path_t *create_path(char *name) {
    struct path_t *path = (struct path_t *)malloc(sizeof(struct path_t));

    path->name = malloc(sizeof(char) * strlen(name));
    strcpy(path->name, name);
    path->start = NULL;
    
    return path;
}

/**
 * Stampa il percorso specificato.
 */
void print_path(struct path_t *path) {
    printf("%s:\n", path->name);
    struct site_t *current = path->start;

    while (current != NULL) {
        printf("%s\n", current->name);
        current = current->next;
    }
}

/**
 * Aggiunge un nodo (site) in fondo al percorso.
 */
void append_site(struct path_t *path, char *name) {
    struct site_t *new_site = (struct site_t *)malloc(sizeof(struct site_t));
    new_site->name = malloc(sizeof(char) * strlen(name));
    strcpy(new_site->name, name);

    if (path->start == NULL) {
        path->start = new_site;
    } else {
        struct site_t *current = path->start;
        struct site_t *prev = path->start;

        while (current != NULL) {
            prev = current;
            current = current->next;
        }
        prev->next = new_site;
    }
}

/**
 * Aggiunge una lista di nodi (site) in fondo al percorso.
 */
void append_sites(struct path_t *path, char *names[], int length) {
    for (int i = 0; i < length; i++) {
        append_site(path, names[i]);
    }
}
