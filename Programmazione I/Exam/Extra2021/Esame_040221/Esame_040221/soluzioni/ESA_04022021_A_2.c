#include <stdio.h>
int coppier(int);
int coppiei(int);


int main(){
	int a, r1,r2;
	
	printf("\n Inserisci un intero non negativo: \n");
	do{
		scanf("%d",&a);
	}while(a<0);
	
	r1=coppier(a);
	r2=coppiei(a);
	
	printf("\n Numero coppie cifre uguali su %d (ricorsiva): %d",a,r1);
	printf("\n Numero coppie cifre uguali su %d (iterativa): %d",a,r2);
	
	return 0;
}


int coppier(int n){
	int c1, c2;
	
	// caso numero <10
	if(n<10)
		return 0;

	// altrimenti confronto coppie di cifre
	c1=n%10;
	n=n/10;
	c2=n%10;
	if(c1==c2)
		return 1+coppier(n);
	else return coppier(n);
}

int coppiei(int n){
	int num=0;
	int c1, c2;
	
	// caso numero <10
	if(n<10)
		return 0;

	// altrimenti confronto coppie di cifre
	while(n>=10){
		c1=n%10;
		n=n/10;
		c2=n%10;
		if(c1==c2)
			num++;
	}
	return num;
}