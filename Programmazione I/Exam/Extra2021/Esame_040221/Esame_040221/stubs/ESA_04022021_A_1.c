#include <stdio.h>   // printf()
#include <stdlib.h>  // malloc(size)
#include <string.h>  // strcpy(dest, src), strlen(string)

struct site_t {
    char *name;
    struct site_t *next;
};

struct path_t {
    struct site_t *start;
    char *name;
};

struct path_t *create_path(char *name);
void print_path(struct path_t *path);
void append_site(struct path_t *path, char *name);
void append_sites(struct path_t *path, char *names[], int length);

/**
 * Program entry point
 * 
 * Output:
 * Via per il Brennero:
 * Verona
 * Rovereto
 * Trento
 * Bolzano
 * Brennero
 */
int main() {
    struct path_t *path = create_path("Via per il Brennero");
    append_site(path, "Verona");
    append_site(path, "Rovereto");
    char *sites[] = {"Trento", "Bolzano", "Brennero"};
    append_sites(path, sites, 3);
    print_path(path);
    return 0;
}

/**
 * Crea e ritorna un nuovo percorso (path) di nome name.
 */
struct path_t *create_path(char *name) {
}

/**
 * Stampa il percorso specificato.
 */
void print_path(struct path_t *path) {
}

/**
 * Aggiunge un nodo (site) in fondo al percorso.
 */
void append_site(struct path_t *path, char *name) {
}

/**
 * Aggiunge una lista di nodi (site) in fondo al percorso.
 */
void append_sites(struct path_t *path, char *names[], int length) {
}
