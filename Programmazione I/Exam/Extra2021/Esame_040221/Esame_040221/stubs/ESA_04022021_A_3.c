#include <stdlib.h>
#include <stdio.h>

struct list {
    int head;
    struct list *next;
};

struct list *static_pointer_to_list; // [DATA]

struct list *genera_lista_din(int i){
    struct list *list;


    if (i < 1){
        return NULL;
    }
    else{
        list = (struct list*)malloc(sizeof(struct list));   // alloco nuovo nodo puntato
        list->head = i; // assegno al nodo puntato il valore in input
        i--; // decremento il valore
        struct list *next = genera_lista_din(i);   // creo un nuovo nodo col valore successivo decrementato
    }
    return list;
}



int main(void) {
    
    struct list* pointer_to_list = genera_lista_din(1);

    // Nella chiamata a genera_lista_din(2) si nota un memory leak tra due indirizzi (024 -- 168)
    
    struct list* pointer_to_list = genera_lista_din(2);

    // Qua ci troviamo come nella immagine

    return 0;
}
