#include <stdio.h>
#include <stdlib.h>

struct nodo_t{
	char valore;
	struct nodo_t * next;
};


char * leggi();
struct nodo_t * crea_lista(char * s);
void stampa(struct nodo_t * nodo);


void main() {

	printf("Inserire una stringa:\n");
	char * s = leggi();

	printf("Stringa inserita: %s\n", s);
	struct nodo_t * testa = crea_lista(s);

	printf("La lista e': \n");
	stampa(testa);
}


char * leggi() {
	// alloca dinamicamente spazio per massimo 256 caratteri della stringa inserita in input
	char *s = (char *)malloc(256*sizeof(char));

	scanf("%s", s);

	return s;
}


struct nodo_t * crea(char valore, struct nodo_t * next){
	
	struct nodo_t * n = (struct nodo_t *)malloc(sizeof(struct nodo_t));	// crea un nodo dinamicamente utilizzando la MALLOC !!!!
	
	n->valore = valore;
	n->next = next;

	return n;
}


struct nodo_t * crea_lista(char * s){

	if(*s == '\0') {						// stringa finita 'ciao\0'
		return 0;
	}
	if(*s == '1') {							// se il valore inserito nella stringa risulta 1 ignora e vai avanti
		return crea_lista(s+1);				// se il char risulta uno
	}
	else{
		return crea(*s, crea_lista(s+1));
	}
}

void stampa(struct nodo_t * nodo){

	while(nodo != NULL){

		if(nodo->next != NULL){				// se il prossimo valore è valido
			printf("%c->", nodo->valore);
			nodo = nodo->next;
		}
		else{
			printf("%c", nodo->valore);
			nodo = nodo->next;
		}
	}	
	printf("\n");
}