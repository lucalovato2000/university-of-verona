#include <stdio.h>
#include <stdlib.h>

#define DIM 3

char *encode(char ma[][DIM], int mi[][DIM], int size);
void print_ma_mi(char ma[DIM][DIM], int mi[DIM][DIM], int size);
int get_size(int mi[DIM][DIM], int size);

void main(){
    
    char ma[DIM][DIM] = {{'a', 'f', 'k'}, {'o', 'u', 'p'}, {'w', 'e', 'j'}};
    int mi[DIM][DIM] = {{3, 0, 2}, {2, 0, 1}, {0, 3, 2}};
    int size = DIM;
    
    print_ma_mi(ma, mi, size);
    
    char *s = encode(ma, mi, size);
	printf("Codifica: %s\n", s);
}

char *encode(char ma[][DIM], int mi[][DIM], int size) {
    
    char *s;
    int i, j, frequenza, index = 0, count = get_size(mi, size); // get the size of the mi matrix
    s = (char *)malloc(sizeof(char) * (count + 1)); // alloca dinamicamente spazio per count + 1
    
    for(i=0;i<size;i++){
        for(j=0;j<size;j++){
            frequenza = mi[i][j];

            while(frequenza > 0){
                s[index] = ma[i][j];
                index++;
                frequenza--;
                frequenza = 0;
            }
        }
    }
    return s;
}

int get_size(int mi[DIM][DIM], int size) {
  
    int i, j, cont = 0;

    for(i=0;i<size;i++){
        for(j=0;j<size;j++){
            cont+=mi[i][j];   
        }
    }
    printf("Occorenze totali: %d\n", cont);
    
    return cont;
}

void print_ma_mi(char ma[DIM][DIM], int mi[DIM][DIM], int size) {
    
	int i,j;

    for(i=0;i<DIM;i++){
        for(j=0;j<DIM;j++){
            printf("%c", ma[i][j]);
        }
        printf("\n");
    }

    printf("\n");
    for(i=0;i<DIM;i++){
        for(j=0;j<DIM;j++){
            printf("%d", mi[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}