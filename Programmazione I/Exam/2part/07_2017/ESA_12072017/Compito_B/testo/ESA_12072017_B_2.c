#include <stdio.h>
#include <stdlib.h>

struct sprite_t {
    char *name;
    int costumes[10]; // Up to 10 costumes
    int size; // costumes' number
    struct sprite_t *next;
};

struct sprite_t *new_sprite(char *name, int costumes[], int size);
struct sprite_t *add_sprite(struct sprite_t *head, char *name, int costumes[], int size);
void print_sprites(struct sprite_t *sprite);

void main(){
    struct sprite_t *head = NULL;
    int costumes_cat[] = {1, 2, 34, 7, 9};
    int size = 5;
    head = new_sprite("My Cat", costumes_cat, size);

    int costumes_dog[] = {21};
    size = 1;
    head = add_sprite(head, "Super Dog", costumes_dog, size);

    int costumes_mouse[] = {4, 8};
    size = 2;
    head = add_sprite(head, "Micky Mouse", costumes_mouse, size);

    print_sprites(head);
}

struct sprite_t *new_sprite(char *name, int costumes[], int size) {
}

struct sprite_t *add_sprite(struct sprite_t *head, char *name, int costumes[], int size) {
}

void print_sprites(struct sprite_t *head) {
}
