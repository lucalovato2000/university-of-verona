#include <stdlib.h>
#include <stdio.h>

struct studente{
    char matricola[20];
    int voto;
    int anno_immatricolazione;
};


void controllo_attività(struct studente *s){
    printf("Validazione matricola studente.\n");
    
}

int main(void) {
//Inserimento:
    struct studente *p_studente = malloc(sizeof(struct studente));
    printf("Inserisci matricola studente: \n");
    scanf("%s", p_studente->matricola);
    printf("Inserisci voto esame (1-30): \n");
    scanf("%i", &(p_studente->voto));
    printf("Inserisci anno immatricolazione: \n");
    scanf("%i", &(p_studente->anno_immatricolazione));
    
    struct studente *p_studente_copy = p_studente;
//Controllo:
    printf("Matricola: %s\n", p_studente->matricola);
    printf("Voto: %i\n", p_studente->voto);
    printf("Anno immatricolazione: %i\n", p_studente->anno_immatricolazione);
    free(p_studente);
//Controllo copia:
    printf("Matricola: %s\n", p_studente_copy->matricola);
    /*errore1: si è creato un dangling pointer (p_studente_copy) con la precedente free, e lo si sta usando per accedere ad una zona di memoria non piu' riservata. Questo puo' causare comportamenti del codice non prevedibili.*/
    printf("Voto: %i\n", p_studente_copy->voto);
    printf("Anno immatricolazione: %i\n", p_studente_copy->anno_immatricolazione);
    

//Erasmus:
    int *mesi_in_erasmus = NULL;
    {
        int mesi;
        printf("Inserire il totale mesi in erasmus: ");
        scanf("%i", &mesi);
        controllo_attività(p_studente);
        mesi_in_erasmus = &mesi;
    }
    /*errore2: si è creato un dangling pointer (mesi_in_erasmus) a causa della variabile automatica dichiarata nel blocco {} e lo si sta usando per accedere ad una zona di memoria non piu' riservata. Questo puo' portare a comportamenti del codice non prevedibili*/
    
    printf("Totale mesi in erasmus = %i\n", *mesi_in_erasmus);
    
    return 0;
}
