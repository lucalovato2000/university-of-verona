#include <stdio.h>
#include <stdlib.h>

struct sprite_t {
    char *name;
    int costumes[10]; // Up to 10 costumes
    int size; // costumes' number
    struct sprite_t *next;
};

struct sprite_t *new_sprite(char *name, int costumes[], int size);
struct sprite_t *add_sprite(struct sprite_t *head, char *name, int costumes[], int size);
void print_sprites(struct sprite_t *sprite);

void main(){
    struct sprite_t *head = NULL;
    int costumes_cat[] = {1, 2, 34, 7, 9};
    int size = 5;
    head = new_sprite("My Cat", costumes_cat, size);

    int costumes_dog[] = {21};
    size = 1;
    head = add_sprite(head, "Super Dog", costumes_dog, size);

    int costumes_mouse[] = {4, 8};
    size = 2;
    head = add_sprite(head, "Micky Mouse", costumes_mouse, size);

    print_sprites(head);
}

struct sprite_t *new_sprite(char *name, int costumes[], int size) {
    struct sprite_t *new = NULL;
    
    new = (struct sprite_t *) malloc(sizeof(struct sprite_t));
    new->size = size;
    new->name = name;
    new->next = NULL;
    
    for(int i = 0; i < size; i++) {
        new->costumes[i] = costumes[i];
    }
            
    return new;
}

struct sprite_t *add_sprite(struct sprite_t *head, char *name, int costumes[], int size) {
    struct sprite_t *new = NULL;
    struct sprite_t *current = NULL;
    struct sprite_t *prev = NULL;
    
    new = new_sprite(name, costumes, size);
    new->next = head;
    
    return new;
}

void print_sprites(struct sprite_t *head) {
    struct sprite_t *current = head;

    while(current != NULL) {
        printf("%s:\n", current->name);

        for(int i = 0; i < current->size; i++) {
            printf("%i ", current->costumes[i]);
        }
        
        printf("\n\n");
        current = current->next;
    }
}
