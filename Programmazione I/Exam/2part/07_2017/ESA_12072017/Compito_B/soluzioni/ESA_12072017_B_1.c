#include <stdio.h>

#include <stdlib.h>
#include <time.h>

#define N 5
#define X 10


void stampa_array(int * array, int lunghezza){
	int i;
	for(i=0; i<lunghezza; i++){
		printf("%i ", array[i]);
	}
	printf("\n");
}
void stampa_M(int M[N][N]){
	int i,j;
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			printf("%i ", M[i][j]);
		}
		printf("\n");
	}
}
void stampa_T(int T[X][2]){
	int i,j;
	for(i=0; i<X; i++){
		printf("(%i,%i) ", T[i][0], T[i][1]);
	}
	printf("\n");
}


void riempi_matrice(int M[N][N]);
void riempi_tentativi(int T[X][2]);
int * calcola_colpiti(int M[N][N], int T[X][2]);
void media_cumulativa(int * colpiti, int lunghezza);

int main(){
	srand(time(NULL));
	
	/* Da utilizzare se e solo se la funzione riempi_matrice non sia stata implementata
	int M[N][N] = { {0,0,1,1,0},
					{0,1,1,0,0},
					{1,0,1,0,1},
					{1,0,0,1,0},
					{0,1,0,1,1}};
	*/
	/* Da utilizzare se e solo se la funzione riempi_tentativi non sia stata implementata
	int T[X][2] = {{0,2},{0,4},{1,0},{1,1},{2,2},{3,1},{3,3},{4,1},{4,2},{4,4}};
	*/
	/* Da utilizzare se e solo se la funzione calcola_colpiti non sia stata implementata
	int colpiti[X] = {2,0,0,2,4,0,6,5,0,8};
	*/
	
	int M[N][N];
	riempi_matrice(M);
	int T[X][2];
	riempi_tentativi(T);
	int * colpiti = calcola_colpiti(M,T);
	
	printf("la matrice e':\n");
	stampa_M(M);
	printf("la lista dei tentativi e':\n");
	stampa_T(T);
	printf("il vettore colpiti e':\n");
	stampa_array(colpiti, X);
	
	printf("stampa della media cumulativa:\n");
	media_cumulativa(colpiti, X);
	
	free(colpiti);
	
	return 0;
}


void riempi_matrice(int M[N][N]){
	int i,j;
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			M[i][j] = rand() % 2;
		}
	}
}

void riempi_tentativi(int T[X][2]){
	int i;
	for(i=0; i<X; i++){
		T[i][0] = rand()%N;
		T[i][1] = rand()%N;
	}
}

int * calcola_colpiti(int M[N][N], int T[X][2]){
	int * colpiti = (int *)malloc(X * sizeof(int));
	int i;
	for(i=0; i<X; i++){
		colpiti[i] = M[ T[i][0] ][ T[i][1] ] == 1 ?  T[i][0] + T[i][1] : 0;
	}
	return colpiti;
}

void media_cumulativa_ric(float * medie, int * colpiti, int lunghezza, float denominatore){
	if(lunghezza > 0){
		*medie += (float)(*colpiti);
		if(lunghezza > 1){
			*(medie + 1) = *medie;
		}
		*medie /= denominatore;
		media_cumulativa_ric(medie+1, colpiti+1, lunghezza-1, denominatore+1.0);
	}
}
void media_cumulativa(int * colpiti, int lunghezza){
	float * medie = (float *)malloc(lunghezza * sizeof(float));
	media_cumulativa_ric(medie, colpiti, lunghezza, 1.0);
	
	/*int i,j;
	for(i=0; i<lunghezza; i++){
		medie[i] = 0.0;
		for(j=0; j<=i; j++){
			medie[i] += (float)colpiti[j];
		}
		medie[i] /= (float)(i+1);
		printf("%i %.4f \n", i, medie[i]);
	}*/
	
	int i;
	for(i=0; i<lunghezza; i++){
		printf("%.4f ", medie[i]);
	}
	printf("\n");
	free(medie);
}