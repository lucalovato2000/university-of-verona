#include <stdlib.h>
#include <stdio.h>


/*
LEGENDA ERRORI:

    - errore 1: dangling pointer causato da aliasing 
    - errore 2: dangling pointer causato da variabile automatica creata nel blocco 
    - errore 3: dangling pointer causato dalla creazione di una variabile di ritorno (indirizzo di memoria) da parte di una funzione
    - errore 4: memory leak causato da mancata deallocazione su puntatore creato tramite malloc()
*/


struct macchina{
    char tipo[20];
    int anno_immatricolazione;
};

int rottamazione(struct macchina *p){
    return p->anno_immatricolazione + 10;
}

int* tagliando(struct macchina *p){
    int anno = p->anno_immatricolazione + 4;
    return &anno;
}


int main(void) {
//Fase 1: creazione
    struct macchina *p = (struct macchina*) malloc(sizeof(struct macchina));
    struct macchina *q = p; //ALIASING, la zona di memoria occupata dalla variabile p è accessibile dal puntatore q mentre il puntatore p diventa 
    //dagling pointer poichè si riferisce ad una zona di memoria non  più valida
    
    p->anno_immatricolazione = 2015;
    free(q);
    printf("Anno immatricolazione = %i\n", p->anno_immatricolazione);
    /*errore1: si è creato un dangling pointer (p) e lo si sta usando per accedere ad una zona di memoria non piu' riservata. 
    Questo puo' portare a comportamenti del codice non prevedibili*/
    
    struct macchina *macchina2 = malloc(sizeof(struct macchina));
    macchina2->anno_immatricolazione = 2017;
    printf("Anno immatricolazione = %i\n", macchina2->anno_immatricolazione);

//Fase 2: informazioni di rottamazione
    int *p_anno_rottamazione = NULL;
    {
        int anno_rottamazione = rottamazione(p);
        p_anno_rottamazione = &anno_rottamazione;
    }
    printf("Anno rottamazione = %i\n", *p_anno_rottamazione);
    /*errore2: si è creato un dangling pointer (p_anno_rottamazione) a causa della variabile automatica dichiarata nel blocco {} 
    e lo si sta usando per accedere ad una zona di memoria non piu' riservata. Questo puo' portare a comportamenti del codice non prevedibili*/
    
//Fase 3: informazioni tagliando
    int *p_anno;
    p_anno = tagliando(p);
    /*errore3: la funzione tagliando() crea un dangling pointer (indirizzo di ritorno) e lo si sta usando 
    per accedere ad una zona di memoria non piu' riservata. Questo puo' portare a comportamenti del codice non prevedibili*/
    printf("Anno tagliano = %i\n", *p_anno);
    
    /*errore4: deallocazione mancata su *macchina2. Questo causa memory leak. */
    
  return 0;
}

