#include <stdio.h>

#include <stdlib.h>
#include <time.h>


void stampa_array(int * array, int lunghezza){
	int i;
	for(i=0; i<lunghezza; i++){
		printf("%i ", array[i]);
	}
	printf("\n");
}

int * crea_array(int lunghezza);
int * corrispondenze(int * campo, int * tentativi, int lunghezza);
void media_cumulativa(int * colpiti, int lunghezza);

int main(){
	srand(time(NULL));
	
	int lunghezza = 10;
	
	
	int * campo = crea_array(lunghezza);
	int * tentativi = crea_array(lunghezza);
	int * colpiti = corrispondenze(campo, tentativi, lunghezza);
	
	
	/* Da utilizzare se e solo se le funzioni crea_array e/o corrispondenze
	   non siano state implementate
	  
	int campo[] = 	  {0,1,1,0,0,0,1,0,1,1};	
	int tentativi[] = {1,1,0,1,1,0,0,0,1,0};
	int colpiti[] =   {0,1,0,0,0,5,0,7,8,0};
	*/
	
	printf("il campo e':\n");
	stampa_array(campo, lunghezza);
	printf("i tentativi sono:\n");
	stampa_array(tentativi, lunghezza);
	printf("le corrispondenze sono:\n");
	stampa_array(colpiti, lunghezza);
	printf("stampa della media cumulativa:\n");
	media_cumulativa(colpiti, lunghezza);
	
	free(campo);
	free(tentativi);
	free(colpiti);
	
	return 0;
}



int * crea_array(int lunghezza){
	int * campo = (int *)malloc(lunghezza * sizeof(int));
	int i;
	for(i=0; i<lunghezza; i++){
		campo[i] = rand() % 2;
	}
	return campo;
}

int * corrispondenze(int * campo, int * tentativi, int lunghezza){
	int * colpiti = (int *)malloc(lunghezza * sizeof(int));
	int i;
	for(i=0; i<lunghezza; i++){
		colpiti[i] = campo[i] == tentativi[i] ? i : 0;
	}
	return colpiti;
}

void media_cumulativa_ric(float * media, int * colpiti, int lunghezza){
	if(lunghezza == 1){
		*media = (float)(*colpiti);
	}
	else{
		media_cumulativa_ric(media+1, colpiti+1, lunghezza-1);
		*media = (  (float)(*colpiti) + 
					(*(media+1) * (float)(lunghezza-1) )
			     ) / (float)(lunghezza);
	}
}
void media_cumulativa(int * colpiti, int lunghezza){
	float * medie = (float *)malloc(lunghezza * sizeof(float));
	media_cumulativa_ric(medie, colpiti, lunghezza);
	/*int i, j;
	for(i=0; i<lunghezza; i++){
		medie[i] = 0.0;
		for (j=i; j<lunghezza; j++){
			medie[i] += (float)colpiti[j];
		}
		medie[i] /= (float)(lunghezza - i);
	}*/
	
	int i;
	for(i=0; i<lunghezza; i++){
		printf("%.4f ", medie[i]);
	}
	printf("\n");
	free(medie);
}