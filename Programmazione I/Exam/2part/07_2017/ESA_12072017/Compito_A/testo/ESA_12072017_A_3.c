#include <stdlib.h>
#include <stdio.h>

struct macchina{
    char tipo[20];
    int anno_immatricolazione;
};

int rottamazione(struct macchina *p){
    return p->anno_immatricolazione + 10;
}

int* tagliando(struct macchina *p){
    int anno = p->anno_immatricolazione + 4;
    return &anno;
}


int main(void) {
//Fase 1: creazione
    struct macchina *p = (struct macchina*) malloc(sizeof(struct macchina));
    struct macchina *q = p; //aliasing
    
    p->anno_immatricolazione = 2015;
    free(q);
    printf("Anno immatricolazione = %i\n", p->anno_immatricolazione);

    struct macchina *macchina2 =malloc(sizeof(struct macchina));
    macchina2->anno_immatricolazione = 2017;
    printf("Anno immatricolazione = %i\n", macchina2->anno_immatricolazione);

//Fase 2: informazioni di rottamazione
    int *p_anno_rottamazione = NULL;
    {
        int anno_rottamazione = rottamazione(p);
        p_anno_rottamazione = &anno_rottamazione;
    }
    printf("Anno rottamazione = %i\n", *p_anno_rottamazione);
    
    
//Fase 3: informazioni tagliando
    int *p_anno;
    p_anno = tagliando(p);
    printf("Anno tagliano = %i\n", *p_anno);
    

  return 0;
}
