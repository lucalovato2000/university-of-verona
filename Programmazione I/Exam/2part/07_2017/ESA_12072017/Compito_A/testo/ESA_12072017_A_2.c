#include <stdio.h>
#include <stdlib.h>

struct character_t {
    char *name;
    int tools[10]; // Up to 10 tools
    int size; // Tools' number
    struct character_t *next;
};

struct character_t *new_character(char *name, int tools[], int size);
void add_character(struct character_t *head, char *name, int tools[], int size);
void print_characters(struct character_t *character);

void main(){
    struct character_t *head = NULL;
    int tools_wizard[] = {9, 8, 1};
    int size = 3;
    head = new_character("Wizard", tools_wizard, size);

    int tools_hobbit[] = {2, 4};
    size = 2;
    add_character(head, "Hobbit", tools_hobbit, size);
    print_characters(head);
}

struct character_t *new_character(char *name, int tools[], int size) {
}

void add_character(struct character_t *head, char *name, int tools[], int size) {
}

void print_characters(struct character_t *head) {
}






