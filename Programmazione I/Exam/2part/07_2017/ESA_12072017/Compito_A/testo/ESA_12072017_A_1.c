#include <stdio.h>



void stampa_array(int * array, int lunghezza){
	int i;
	for(i=0; i<lunghezza; i++){
		printf("%i ", array[i]);
	}
	printf("\n");
}

int * crea_array(int lunghezza);
int * corrispondenze(int * campo, int * tentativi, int lunghezza);
void media_cumulativa(int * colpiti, int lunghezza);

int main(){
	
	int lunghezza = 10;
	
	
	int * campo = crea_array(lunghezza);
	int * tentativi = crea_array(lunghezza);
	int * colpiti = corrispondenze(campo, tentativi, lunghezza);
	
	
	/* Da utilizzare se e solo se le funzioni crea_array e/o corrispondenze
	   non siano state implementate
	   
	int campo[] = 	  {0,1,1,0,0,0,1,0,1,1};	
	int tentativi[] = {1,1,0,1,1,0,0,0,1,0};
	int colpiti[] =   {0,1,0,0,0,5,0,7,8,0};
	*/
	
	printf("il campo e':\n");
	stampa_array(campo, lunghezza);
	printf("i tentativi sono:\n");
	stampa_array(tentativi, lunghezza);
	printf("le corrispondenze sono:\n");
	stampa_array(colpiti, lunghezza);
	printf("stampa della media cumulativa:\n");
	media_cumulativa(colpiti, lunghezza);
	
	return 0;
}