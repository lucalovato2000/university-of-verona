#include <stdio.h>
#include <stdlib.h>

typedef struct list{
  int val;
  struct list *next;

} list;


list *list_concat(list *L1, list *L2){

  struct list *newlist = NULL;

    while(L1 != NULL) {

      newlist = inserisciInTesta(newlist, L1->val);
      L1 = L1->next;
    }
    return newlist;
}

list* inserisciInTesta(list* lista, int val){

  list *tmp = (list*) malloc(sizeof(list)); // crea un nodo

  if(tmp != NULL) {       // se nodo vuoto
    tmp->val = val;       // assegna al valore l'int ricevuto in input
    tmp->next = lista;  

    lista = tmp;          // assegna alla lista il nodo tmp (tmp->value: valore in input, tmp->next: lista)
  }
  else {
    printf("Memoria esaurita!\n");
  }
  return lista;
}

int main(){

    int val, val2;
    int *L1 = NULL, *L2 = NULL;

    do{
		  scanf("%d", &val);
    
    if(val != -1){
      L1 = inserisciInTesta(L1, val);
    }
  }while(val != -1);

  do{
		scanf("%d", &val2);
    
    if(val != -1){
      L2 = inserisciInTesta(L2, val2);
    }
  }while(val2 != -1);

  list L3 = *list_concat(L1, L2);
}
