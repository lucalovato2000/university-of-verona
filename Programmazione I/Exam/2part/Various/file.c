#include <stdio.h>


int main(void){

    FILE *fp;
    int numero, prec;

    fp = fopen("test.txt", "r");   // fopen utilizzata per aprire i file

    if(fp){ // se il filedescriptor da esito positivo
        
        while(!feof(fp)){   // finchè non sei alla fine del file
            
            fscanf(fp, "%d", &numero);  // esegui una scanf per ottenere un %d e mettilo dentro numero
        
            if(numero == prec*2 && numero != 0) // se numero == prec*2 & numero != 0 ALLORA STAMPALO A VIDEO !!!
                printf("%d, %d\n", prec, numero);
                
            prec=numero;    // assegna a prec il numero appena letto
        }
    }
    return 0;
}
