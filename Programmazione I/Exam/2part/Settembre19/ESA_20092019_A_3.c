//dictionary.c
#include <stdio.h>
#include <string.h>
#define L_KEY 20

struct entry{
    char word[L_KEY];
    char definition[100];
};


struct entry * creazione_dizionario(){
    //definizione statica del dizionario
    static struct entry dictionary[]= {
        {"Alambicco", "Strumento utilizzato per la distillazione"},
        {"Albero", "Struttura dati efficiente"},
        {"All-grain","Metodo di produzione per la birra"},
        {"Cane", "Animale che abbaia"},
        {"Gatto", "Animale peloso miagolante"},
        {"Ritardatario", "Persona non puntuale"},
        {"Smartphone", "telefono intelligente a batteria"},
        {"Studente", "Persona per bene molto studiosa"},
        {"ZTL", "Zona a traffico limitato"}
    };
    return dictionary;
}

/*
int compare_strings(char *s1, char *s2 ){
    int pos=0;
    while( *(s1+pos) == *(s2+pos) && *(s1+pos) && *(s2+pos) )
        pos++;

    return *(s1+pos) - *(s2+pos); //ritorna 0 se s1==s2
}
*/

int compare_strings(char *s1, char *s2){

    int ris=-1;

    ris= strcmp(s1, s2);  //zero se sono uguali

    return ris;
}


int lookup_binary(char *key, struct entry dictionary[], int length){
    int left = 0, right= length-1, middle;
    int comp;
    
    while(left <= right){
        middle = (left+right) / 2;
        comp = compare_strings(key, dictionary[middle].word);
        if(comp == 0)
            return middle; //l'ho trovato!
        else if(comp <0)
            right = middle-1;
        else
            left = middle +1;
    }
    return -1; //non c'e'!!
}


int lookup_linear(char *key, struct entry dictionary[], int length){
    int pos;
    
    for(pos = 0 ; pos < length ; pos++){
        if( compare_strings(key, dictionary[pos].word ) == 0 )
        return pos;
    }
    
    return -1; //non c'e'!
    
}


int main(void) {
    char key[L_KEY];
    struct entry *dictionary = creazione_dizionario();
    
    //inserimento chiave 1 da tastiera:
    printf("Inserire la parola da ricercare: ");
    scanf("%19s", key);
    //ricerca della chiave 1 nel dizionario:
    printf("La parola cercata di trova in posizione: %i\n", lookup_linear(key, dictionary,9));
    
    //inserimento chiave 2 da tastiera:
    printf("Inserire la parola da ricercare: ");
    scanf("%19s", key);
    //ricerca della chiave 2 nel dizionario:
    printf("La parola cercata di trova in posizione: %i\n", lookup_binary(key, dictionary,9));
    
  return 0;
}
















