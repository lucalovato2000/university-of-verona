#include <stdio.h>
#include <stdlib.h>
#define N 6

int kmassimo(int *,int, int);
int contadispari(int *,int);

int kmassimo(int a[],int len, int k){
	int i,j;
	int flag;
	
	/* controllo se la dimensione dell'array non permette di trovare il k-massimo e restituisco -1 */
	if(len<(2*k +1))
		return -1;	
	
	/* altrimenti cerco il primo k-massimo  */
	for(i = k ; i<len-k; i++){
		flag = 0;
		for(j = i-k; j<= i+k ; j++){
			if(a[i]<=a[j] && i!=j){
				flag = 1;
			}	
		}
		if(flag == 0)
			return i;		
		
		else flag=0;
	}
	return -1;

}

/* funzione contadispari ricorsiva: restituisce quanti valori dispari sono presenti in un array a di dim interi */

int contadispari(int a[],int dim){
	if(dim==0)	// se dimensione zero allora ritorna zero
		return(0);
	if(a[dim-1]%2!=0)	// se il valore di array[dim-1] è dispari 
		return 1+contadispari(a,dim-1);	// ritorna 1+richiamafunzione
	else return contadispari(a,dim-1);	// altrimenti richiama funzione con valore zero
}



int main(){
	int k,i,r;
	int a[N];
	printf("Inserisci array: ");
	for(i=0; i<N; i++){
		scanf("%d",&a[i]);
	}
	printf("\n Inserisci k: ");
	scanf("%d", &k);
	printf("\n Indice k-massimo: %d\n",kmassimo(a,N,k));
	
	r=contadispari(a,N);
	printf("\n Ci sono %d valori dispari nell'array",r);
	
	return 0;
}