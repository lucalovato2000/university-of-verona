#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

struct fp_digit_t {
    char digit;
    struct fp_digit_t *next;
};

struct fp_number_t {
    struct fp_digit_t *ipart_head;  // Integer part
    struct fp_digit_t *fpart_head;  // Fractional part
};

struct fp_digit_t *append_digit(struct fp_digit_t *head, char digit);
struct fp_number_t *new_fp_number(char *number);
void println_fp_number(struct fp_number_t *fp_number);

/**
 * Append a digit, represented by a char, to a fp_digit_t list.
 * Return the head of the fp_digit_t list.
 */

struct fp_digit_t *append_digit(struct fp_digit_t *head, char digit) {
    
    struct fp_digit_t *new = (struct fp_digit_t *)malloc(sizeof(struct fp_digit_t));    // crea un nodo da appendere [OBBLIGO UTILIZZO MALLOC !!!]
    struct fp_digit_t *current = head;  // crea una lista current che punta alla testa della lista entrante

    new->digit = digit; // assegna al valore del nodo il valore digit in input
    new->next = NULL;   // assegna al valore next del nodo il valore null

    if (head == NULL) { // se head NULL return nodo new
        return new;
    } else {
        while (current->next != NULL) { // altrimenti finchè current->next != NULL
            current = current->next;    // scorri la lista
        }
        current->next = new;    // assegna in fondo il nuovo nodo creato    
        return head;    // ritorna la lista
    }
}

/*
 * Create fp_number_t and return a pointer to it.
 */

struct fp_number_t *new_fp_number(char *number) {
    struct fp_number_t *new = (struct fp_number_t *)malloc(sizeof(struct fp_number_t)); // crea un nuovo nodo new [obbligo utilizzo malloc!!!]
    new->ipart_head = NULL; // parte i NULL
    new->fpart_head = NULL; // parte f NULL

    if (*number!=NULL) {    // se number != NULL
        while (*number != '.') {    // finchè non trovi un punto
            new->ipart_head = append_digit(new->ipart_head, *number);   // aggiunge alla lista (quindi la parte integer)
            number++;   // incrementa contatore
        }
        number++;   
        while (*number!=NULL) {
            new->fpart_head = append_digit(new->fpart_head, *number); // aggiunge alla lista (quindi la parte fractional)
            number++;
        }
    }

    return new;
}

/**
 * Print a fp_number_t number.
 * If the integer part is empty, print "Value not defined".
 */

void println_fp_number(struct fp_number_t *fp_number) {
    struct fp_digit_t *icurrent = fp_number->ipart_head;
    struct fp_digit_t *fcurrent = fp_number->fpart_head;

    if (icurrent == NULL && fcurrent == NULL) {
        printf("Value not defined\n");
    } else {
        while (icurrent != NULL) {
            printf("%c", icurrent->digit);
            icurrent = icurrent->next;
        }

        printf(".");

        while (fcurrent != NULL) {
            printf("%c", fcurrent->digit);
            fcurrent = fcurrent->next;
        }
        printf("\n");
    }
}

/*
 * Program entry point
 * Output:
 * 123.456
 * 0.
 * .0
 * Value not defined.
 */

int main() {
    struct fp_number_t *fp_number = NULL;
    fp_number = new_fp_number("123.456");
    println_fp_number(fp_number);
    println_fp_number(new_fp_number("0."));
    println_fp_number(new_fp_number(".0"));
    println_fp_number(new_fp_number("."));
    return 0;
}
