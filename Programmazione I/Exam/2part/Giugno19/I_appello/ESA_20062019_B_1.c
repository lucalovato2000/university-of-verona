#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 30

// data una stringa statica in input, restituisce una stringa dinamica utilizzando il minor numero di caratteri

char* creadin(char src[]){
	int l;
	int i;
	char *dest;
	
	l=strlen(src);
	dest=(char*)malloc((l+1)*sizeof(char));
	for(i=0;i<l;i++)
		dest[i]=src[i];
	dest[i]='\0';
	
	return dest;	
}

// la funzione inverti prende una stringa e la inverte inserendo * alla fine

char* inverti(char src[]){
	char tmp;
	int i,l;
	
	l=strlen(src);
	src=realloc(src, (l+2)*sizeof(char));
	for(i=0;i<l/2;i++){
		tmp=src[i];
		src[i]=src[l-i-1];
		src[l-i-1]=tmp;
	}
	
	src[l]='*';
	src[l+1]='\0';
	
	// restituisci src
	return src;	
}

// data una stringa, crea una nuova stringa ottenuta eliminando tutte le sue vocali, deallocando eventuale spazio nello heap non utilizzato
char* elimina(char src[]){
	char* new;
	int i,j,l;
	int voc=0;
	
	l=strlen(src);
	for(i=0;i<l;i++)
		if(src[i]=='a'||src[i]=='e'||src[i]=='i'||src[i]=='o'||src[i]=='u')
			voc++;
	new=(char*)malloc((l+1-voc)*sizeof(char));
	
	for(i=0,j=0;i<l;i++){
	
		if(src[i]!='a' && src[i]!='e'&& src[i]!='i'&& src[i]!='o'&& src[i]!='u')
		{
			new[j]=src[i];
		    j++;
		}
	}
	src[i]='\0';
	// libera memoria eventualmente non necessaria
	free(src);
	
	// restituisci la nuova stringa 
	return new;	
}




int main() { 
  
  char src[N]="esame";
  char * dest;
  char * doppia;

  dest=creadin(src);
  printf("\n Stringa dinamica %s",dest);

  dest=inverti(dest);
  printf("\n Stringa dinamica invertita %s",dest);
  
  doppia=elimina(dest);
  printf("\n Stringa senza vocali %s",doppia);
  
  return(0); 
}
