#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

struct list_t *append_list(struct list_t *list, char note, bool minor);
void strtosong(struct list_t *list, char *lists);
void print_song_r(struct list_t *list);

struct list_t {
    char note;
    bool minor;
    struct list_t *next;
};

/*
 * Create a list and append it to an existing song or a new one
 */
struct list_t *append_list(struct list_t *list, char note, bool minor) {

    struct list_t *new = (struct list_t *)malloc(sizeof(struct list_t));    // crea un nuovo nodo per la lista

    new->note = note;   // assegna la char note
    new->minor = minor; // assegna il bool minor

    if (list == NULL) {
        // Empty list
        list = new; // assegna alla lista il new
    } else {
        while (list->next != NULL) {    // altrimenti finchè non punta a NULL scorri 
            list = list->next;
        }
        list->next = new;   // scorri comunque
    }

    return list;
}

/*
 * Add lists to an existing song from a string
 * i.e. "G Cm G"
 */
void strtosong(struct list_t *list, char *lists) {

    while (*lists) {
        char note = *lists;
        lists++;
        bool minor = false;

        if (*lists == 'm') {
            minor = true;
            lists++;
        }
        append_list(list, note, minor);

        if (*lists) {  // If not at the end of the string
            lists++;   // skip the space between notes
        }
    }
}

/**
 * Print a song with recursion
 */
void _print_song_r(struct list_t *list) {

    if (list != NULL) {                // Utilizzare IF se presente ricorsione, WHILE se non presente ricorsione
        printf("%c", list->note);
        if (list->minor) {
            printf("m");
        }
        printf(" ");

        _print_song_r(list->next);
    }
}

/**
 * Print a song or "Empty song\n" if NULL is given
 * with recursion
 */
void print_song_r(struct list_t *list) {
    if (list == NULL) {
        printf("Empty song\n");
    } else {
        _print_song_r(list);
    }
}

/*
 * Program entry point
 * Output atteso:
 * Em G D C Em
 * Em G D C Em G Cm G
 */
int main() {
    struct list_t *song = NULL;
    song = append_list(song, 'E', true);
    append_list(song, 'G', false);
    append_list(song, 'D', false);
    append_list(song, 'C', false);
    append_list(song, 'E', true);
    print_song_r(song);
    printf("\n");

    char lists[] = "G Cm G";
    strtosong(song, lists);
    print_song_r(song);
    printf("\n");
    return 0;
}
