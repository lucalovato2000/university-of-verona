#include <stdio.h>
#include <stdlib.h>

void stampa_lista(struct nodo_t * nodo);
char * crea_stringa(struct nodo_t * lista);
void stampa_stringa(char * s);

struct nodo_t{
	char valore;
	struct nodo_t * next;
};

struct nodo_t * crea(char valore, struct nodo_t * next){

	struct nodo_t * n = (struct nodo_t *)malloc(sizeof(struct nodo_t));	// malloc per nuovo nodo

	n->valore = valore;		// assegna al n->value il valore ricevuto
	n->next = next;			// assegna al n->next il valore ricevuto
	return n;
}


void main(){
	struct nodo_t * testa = crea('c', crea('0',
							crea('a', crea('0',
							crea('9', crea('s',
							crea('e', crea('3',
							crea('t', crea('3',
							crea('6', crea('7',
							crea('t', crea('8',
							crea('a', NULL)))))))))))))));
	
	printf("La lista di caratteri e':\n");
	stampa_lista(testa);
	
	char * s = crea_stringa(testa);
	printf("La stringa e':\n");
	stampa_stringa(s);
}

void stampa_lista(struct nodo_t * nodo){
	
	if(nodo != NULL){
		printf("%c", nodo->valore);

		if(nodo->next != NULL){			// a - b - c - d
			printf("-");
			stampa_lista(nodo->next);	// chiamata ricorsiva passando nodo->next, dato che è presente un if iniziale 
										// con un while iniziale serve solo scorrere la lista con nodo = nodo->next;
		}
		else{
			printf("\n");
		}
	}
}

char *crea_stringa(struct nodo_t *lista) {
	
	int length = 0;

	while(lista != NULL) {
		if(!(lista->valore >= '0' && lista->valore <= '9')) {	// se il valori della lista sono validi aumenta length
			length++;
		}
		lista = lista->next;
	}
	char * s = (char*)malloc(sizeof(char) * length);	// alloca spazio in memoria dinamica di length
	int i = 0;
	
	while(lista != NULL) {
		if(!(lista->valore>='0' && lista->valore <='9')) {
			s[i] = lista->valore;	// assegna ad s il valore della lista valido !
			i++;
		}
		lista = lista->next;
	}
	return s;
}

void stampa_stringa(char * s){

	if(*s){
		printf("%c", *s);

		if(*(s+1)){					// se il valore successivo di s è valido
			printf("+");			// inserisci un +
			stampa_stringa(s+1);	// passa alla funzione stampastringa() il char successivo
		}
		else{
			printf("\n");
		}
	}
}