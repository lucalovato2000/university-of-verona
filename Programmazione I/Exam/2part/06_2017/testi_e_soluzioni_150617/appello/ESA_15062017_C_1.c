#include <stdio.h>
#include <stdlib.h>

#define N 3

void stampa_matrice(int M[N][N]){
	int i,j;
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			printf("%i ", M[i][j]);
		}
		printf("\n");
	}
};
void stampa_vettore(float * a, int length){
	int i;
	for(i=0; i<length; i++){
		printf("%f ", a[i]);
	}
	printf("\n");
};

float * media_colonne(int M[N][N]);
float * media_righe(int M[N][N]);
void somma(float * C, float * R, float * S,int length);

void main(){
	int M[N][N] = { {1,3,6}, {2,4,8}, {5,7,9} };
	printf("La matrice e':\n");
	stampa_matrice(M);
	
	printf("La media sulle colonne e':\n");
	float * C = media_colonne(M);
	stampa_vettore(C,N);
	
	printf("La media sulle righe e':\n");
	float * R = media_righe(M);
	stampa_vettore(R,N);
	
	printf("La somma di C end R e':\n");
	float S[N];
	somma(C,R,S,N);
	stampa_vettore(S,N);
}


float * media_colonne(int M[N][N]){
	float * C = (float*)malloc(N * sizeof(float));
	int i,j;
	for(i=0; i<N; i++){
		C[i] = 0.0;
		for(j=0; j<N; j++){
			C[i] += (float)M[i][j];
		}
		C[i] /= (float)N;
	}
	return C;
}


float * media_righe(int M[N][N]){
	float * R = (float*)malloc(N * sizeof(float));
	int i,j;
	for(i=0; i<N; i++){
		R[i] = 0.0;
		for(j=0; j<N; j++){
			R[i] += (float)M[j][i];
		}
		R[i] /= (float)N;
	}
	return R;
}

void somma(float * C, float * R, float * S,int length){
	if(length > 0){
		*S = *C + *R;
		somma(C+1,R+1,S+1,length-1);
	}
}