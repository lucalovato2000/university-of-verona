#include <stdlib.h>
#include <stdio.h>

/* ci sono diverse soluzioni al problema. Una tra queste è rappresentata dal seguente codice:
 */


struct list {
    int head;
    struct list *tail;
};

struct list *x;

struct list *recursive_f(int i){
    struct list *n = (struct list *)malloc(sizeof(struct list));
    //passo base raggiungo il 4 'elemento' ovvero NULL
    if (i>2){
	    return NULL;
    }
    //passo ricorsivo crea un nodo con head=i, incremento i e chiamo ricorsivamente la funzione
    else{
        n->head=i;
        i++;
        n->tail=recursive_f(i);
    }
	return n;
}



int main(void) {
    struct list *a = recursive_f(1);
    { // entrerà mai ? [memory leak ...]
        struct list *n = (struct list *)malloc(sizeof(struct list));
        n->head = 3;
        n->tail = NULL;
    }
    x=a;    // assegnare indirizzo a cui puntano uguale 
    // IN QUESTO PUNTO CI SI TROVA NELLO STATO RAPPRESENTATO NELL'IMMAGINE
}
