#include <stdio.h>
#include <stdlib.h>

struct point_t {
    double x;
    double y;
    struct point_t *next;
};

struct point_t *create_points(double a_x[], double a_y[], int size);
int count_points(struct point_t *points);

void main(){
    double a_x[] = {6, -0.4, -4.5, 9.3};
    double a_y[] = {0, 0.4, 1, 4};
    int size = 4;
    //double a_x[] = {};
    //double a_y[] = {};
    //int size = 0;
    
    struct point_t *points = create_points(a_x, a_y, size);
	printf("Numero di punti nel primo quadrante: %i\n", count_points(points));
}

struct point_t *create_points(double a_x[], double a_y[], int size) {
    struct point_t *new = NULL, *current = NULL, *prev = NULL, *head = NULL;
    
    for(int i = 0; i < size; i++) { // scorri fino a size che hai in input
        
        new = (struct point_t *) malloc(sizeof(struct point_t));    // alloca un nuovo nodo new (creazione del point)
        
        new->x = a_x[i];    // assegna valori dati in input      
        new->y = a_y[i];    // assegna valori dati in input  
        new->next = NULL;        
        
        if(head == NULL) {  // se head == NULL
            head = new;     // new diventa nuova head
        } else {
            current = head;     // altrmenti current diventa head
            
            while(current != NULL) {    // scorri current assegnando a prev i valori 
                prev = current; 
                current = current->next;
            }
            
            prev->next = new;   // assegna al next del prev il nodo new 
        }        
    }
    return head;
}

int count_points(struct point_t *points) {
    int count = 0;
    
    while (points != NULL) {

        if(points->x >= 0 && points->y >= 0) {    // se i valori sono maggiori di zero incrementa il contatore count++
            count++;
            printf("%i: (%.2lf, %.2lf)\n", count, points->x, points->y);
        }
        points = points->next;    // scorri la lista con point = point -> next
    }
    return count;
}
