#include <stdlib.h>
#include <stdio.h>

/* ci sono diverse soluzioni al problema. Una tra queste è rappresentata dal seguente codice:
 */


struct list {
    int head;
    struct list *tail;
};

struct list *x;

struct list *recursive_f(int i){

    struct list *n = (struct list *)malloc(sizeof(struct list));
    //passo base raggiungo il 4 'elemento' ovvero NULL
    if (i>3)
	    return NULL;
    //passo ricorsivo crea un nodo con head=i, incremento i e chiamo ricorsivamente la funzione
    else{
        n->head=i;
        i++;
        n->tail=recursive_f(i); // per il next passo alla funzione la sua recursive()
    }
	return n;
}



int main(void) {

    struct list *a = recursive_f(1);
    x = a;    // indirizzo al quale puntano uguale nell'immagine

    // IN QUESTO PUNTO CI SI TROVA NELLO STATO RAPPRESENTATO NELL'IMMAGINE
}
