#include <stdio.h>
#include <stdlib.h>

struct prime_t {
    int n;
    struct prime_t *next;
};

int is_prime(int n);
struct prime_t *prime_list(int numbers[], int size);
void print_list(struct prime_t *list);

void main(){
    //int numbers[] = {7, 9, 4, 11, 17, 21, 83};
    //int size = 7;
    int numbers[] = {9};
    int size = 1;
    struct prime_t *list;
    
    list = prime_list(numbers, size);
    print_list(list);
}

struct prime_t *prime_list(int numbers[], int size) {
    struct prime_t *new;
    struct prime_t *prev;
    struct prime_t *head = NULL;
    
    for(int i = 0; i < size; i++) {
        if(is_prime(numbers[i])) {
            new = (struct prime_t *) malloc(sizeof(struct prime_t));
            new->n = numbers[i];
            
            if(head == NULL) {
                head = new;
            } else {
                prev->next = new;
            }
            
            prev = new;
        }
    }
    
    return head;
}

void print_list(struct prime_t *list) {
    struct prime_t *current = list;
    
    while(current != NULL) {
        printf("%i", current->n);
        current = current->next;
        
        if(current != NULL) {
            printf(" -> ");
        }
    }
    
    printf("\n");
}

// ritorna 0 (falso) se l'argomento non e' primo
// ritorna un valore positivo (vero) se l'argomento è primo
int is_prime(int n) {
	int d;
	
	// verifico tutti i possibili divisori di n
	// itero fino alla radice quadrata di n compresa
	for (d = 2; d * d <= n; d++) {
		if (n % d == 0) {
			return 0;
        }
    }
			
	return n >= 2; // 0 e 1 non sono primi
}
