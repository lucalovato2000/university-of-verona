#include <stdlib.h>
#include <stdio.h>

/* ci sono diverse soluzioni al problema. Una tra queste è rappresentata dal seguente codice:
 */


struct list {
    int head;
    struct list *tail;
};


struct list *construct_list(int head, struct list *tail){
    struct list *this = malloc(sizeof(struct list));
    this->head = head;
    this->tail = tail;
    return this;
}


void recursive(struct list *this) {
    if(this != NULL)
        recursive(this->tail);
    else {
    //qui ci si trova nella situazione rappresentata in figura.
    };
}



int main(void) {
    struct list * a = construct_list(1,
                                     construct_list(2, NULL));
    
    recursive(a);
}
