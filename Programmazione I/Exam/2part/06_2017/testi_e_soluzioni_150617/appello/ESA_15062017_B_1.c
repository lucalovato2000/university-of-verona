#include <stdio.h>
#include <stdlib.h>

struct nodo_t{
	char valore;
	struct nodo_t * next;
};


char * leggi();
struct nodo_t * crea_lista(char * s);
void stampa(struct nodo_t * nodo);


void main(){
	printf("Inserire una stringa:\n");
	char * s = leggi();
	
	struct nodo_t * testa = crea_lista(s);
	printf("La lista e':\n");
	stampa(testa);
}


char * leggi(){
	char * s = (char *)malloc(256 * sizeof(char));	// crea nella memoria dinamica lo spazio per una stringa !
	scanf("%256s", s);	// forzare scanf a massimo 256 caratteri
	return s;
}

struct nodo_t * crea(char valore, struct nodo_t * next){

	struct nodo_t * n = (struct nodo_t *)malloc(sizeof(struct nodo_t));
	n->valore = valore;
	n->next = next;

	return n;
}

struct nodo_t * crea_lista(char * s) {

	if(*s) {
		if(*s >= '0' && *s <= '9') {
			return crea_lista(s+1);
		}
		else{
			return crea(*s, crea_lista(s+1));	// richiama ricorsivamente la funziona crea_lista e crea il nodo valido (non è un numero)
		}
	}
	else{
		return NULL;
	}
}

void stampa(struct nodo_t * nodo){

	if(nodo != NULL){
		printf("%c", nodo->valore);

		if(nodo->next != NULL){			// se il prossimo nodo non è NULL allora stampa la ->
			printf(" -> ");				// ( a -> b -> c -> d -> e )
		}
		stampa(nodo->next);				// richiama in modo ricorsivo la funzione di stampa
	}
	else{
		printf("\n");
	}
}