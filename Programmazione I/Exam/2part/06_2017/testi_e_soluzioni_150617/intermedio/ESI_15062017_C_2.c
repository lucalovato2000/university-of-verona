#include <stdio.h>
#include <stdlib.h>

/* ci sono diverse soluzioni al problema. Una tra queste è rappresentata dal seguente codice:
 */


int w = 10;

void f2() {
    long t;
    // qui la memoria si trova nello stato descritto dalle specifiche.
}



void f1(long * z) {
    f2();
}

void main() {
	long x = 0;
    long * y = malloc(sizeof(long));
    *y = 17;
    y = malloc(sizeof(long));
    *y = 27;
    y = malloc(sizeof(long));
    *y = 27;
    f1(y);
}
