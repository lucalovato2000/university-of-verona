#include <stdio.h>
#include <stdlib.h>

struct element_t {
    int value;
    struct element_t *next;
};

struct element_t *create_odd_list(int values[], int size);
void print_list(struct element_t *list);

void main(){
    int values[] = {8, 3, 1, 3, 9, 4, 6, 5};
    int size = 8;
    
    struct element_t *list = create_odd_list(values, size);
	print_list(list);
}

struct element_t *create_odd_list(int values[], int size) {
    struct element_t *current = NULL;
    struct element_t *prev = NULL;
    struct element_t *new = NULL;
    struct element_t *head = NULL;
    
    for(int i = 0; i < size; i++) {
        if (values[i] % 2 != 0) {
            new = (struct element_t *) malloc(sizeof(struct element_t));

            if (new) {
                if(head == NULL) {
                    head = new;
                    new->next = NULL;
                } else {
                    current = head;
                    prev = head;
                    
                    while(current) {
                        prev = current;
                        current = current->next;
                    }
                    
                    prev->next = new;
                }         
                
                new->value = values[i];
            }
        }
    }
    
    return head;
}

void print_list(struct element_t *list) {
    struct element_t *current = list;
    
    while(current != NULL) {
        printf("%i ", current->value);
        current = current->next;
    }
    
    printf("\n");
}
