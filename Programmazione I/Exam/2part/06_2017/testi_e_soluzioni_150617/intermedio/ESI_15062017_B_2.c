#include <stdio.h>
#include <stdlib.h>

/* ci sono diverse soluzioni al problema. Una tra queste è rappresentata dal seguente codice:
 */


int z = 0;
int w = 10;
int v = 0;

void f1() {
    int c;
    // qui la memoria si trova nello stato descritto dalle specifiche.
}

void main() {
	int a = 0;
    int * b = malloc(sizeof(int));
    *b = 17;
    b = malloc(sizeof(int));
    *b = 27;
    b = malloc(sizeof(int));
    *b = 27;
    f1();
}
