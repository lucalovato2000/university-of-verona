#include <stdio.h>
#include <stdlib.h>


char * replace(char * s);


void main(){
	char * s = "P.R.O.G_R_A_M_m-a-z-i-o_n_e_ _1_";
	printf("%s\n", s);
	char * m = replace(s);
	printf("%s\n",m);
	free(m);
}

char * replace(char * s){
	int length = 0, new_length = 0;
	char * c = s;
	while(*c){
		if(*c>='a' && *c<='z'){
			if( (*c - 'a') > 9){
				new_length++;
			}
		}
		else if(*c>='A' && *c<='Z'){
			if( (*c - 'A') > 9){
				new_length++;
			}
		}
		new_length++;
		length++;
		c++;
	}
	
	char * r = (char*)malloc(new_length * sizeof(char));
	
	int i,j;
	int rep;
	for(i=0, j=0; i<length; i++, j++){
		rep = -1;
		if(s[i]>='a' && s[i]<='z'){
			rep = (s[i] - 'a') + 1;
		}
		else if(s[i]>='A' && s[i]<='Z'){
			rep = (s[i] - 'A') + 1;
		}
		if (rep != -1){
			if( rep < 10){
				r[j] = '0' + rep;
			}
			else{
				r[j] = '0' + (rep/10);
				j++;
				r[j] = '0' + (rep%10);
			}
		}
		else{
			r[j] = s[i];
		}
	}
	
	return r;
}