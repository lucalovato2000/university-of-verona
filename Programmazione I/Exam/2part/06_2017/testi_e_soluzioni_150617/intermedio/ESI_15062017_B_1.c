#include <stdio.h>
#include <stdlib.h>

#define N 4

char * replace(char * s, char map[N][2]);

void main(){
	char * s = "P.R.O.G_R_A_M_m-a-z-i-o_n_e_ _1_";
	printf("%s\n", s);
	
	char M[N][2] = { {'.','1'}, {'_','2'}, {'-','3'}, {' ','@'}};
	
	char * t = replace(s, M);
	printf("%s\n",t);
	free(t);
}


char get_mapping(char c, char map[N][2]){
	int i;
	for(i=0; i<N; i++){
		if(c == map[i][0]){
			return map[i][1];
		}
	}
	return c;
}

int length(char * s){
	int l = 0;
	while(*s){
		l++;
		s++;
	}
	return l+1;
}

char * replace(char * s, char map[N][2]){
	char * r = (char*)malloc( length(s) * sizeof(char) );
	int i;
	for(i=0; i<length(s); i++){
		r[i] = get_mapping(s[i], map);
	}
	return r;
}