#include <stdbool.h>
#include <stdio.h>

void print_prime_numbers(int n);
bool is_prime(int n, int div);

/*
 * Stampa i numeri primi <= n utilizzando la ricorsione e
 * la funzione is_prime(n, 2)
 */
void print_prime_numbers(int n) {
}

/*
 * Restituisce true se n è primo, false altrimenti,
 * utilizzando la ricorsione
 */
bool is_prime(int n, int div) {
}

/*
 * Program entry point
 * Output atteso:
 * 2 3 5 7 11 13 17 19
 */
int main() {
    print_prime_numbers(20);
    printf("\n");
    return 0;
}
