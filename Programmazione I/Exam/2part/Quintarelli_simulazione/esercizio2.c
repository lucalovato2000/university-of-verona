#include <stdio.h>

#define ARR_SIZE 5
#define TRUE 1
#define FALSE 0

/* prototipi */
int tuttiMaggiori(int a[], int n, int m);
void print_array(int a[], int size);
void sommadispari(int arr[], int m);


int tuttiMaggiori(int A[], int n, int m){
    
    if (n < 1) {             // caso base: array vuoto
        return TRUE;
    }
    else if (A[n-1] <= m) {
        return FALSE;       // FALSE se esiste un valore <=
    }
    else {
        return tuttiMaggiori(A, n-1, m);
    }
}

void sommadispari(int arr[], int m){

    int i, ris = 0;

    for(i = 0; i < ARR_SIZE; i++) {

        if(i%2 != 0) {  // se risulta un numero dispari (modulo di 2 != 0)          // PARI i%2 == 0, DISPARI i%2 != 0
            ris += arr[i];        
        }
    }
    printf("La somma risulta: %d\n", ris);
}

voidsommadispariric(int arr[], int m){
    int ris = 0;

    if (m >= ARR_SIZE){
        return ris;
    }

    if ( m < 1){
        return TRUE;
    }
    if (arr[m]%2 != 0){
        ris += arr[m];
        return voidsommadispariric(arr, m+1);
    }
}

void print_array(int a[], int size) {
    
    int i;
    printf("\n[ ");
    
    for(i=0; i<size; i++){
        printf("%d ", a[i]);
    }
    printf("]\n");
}


int main() {
    
    int m, risultato, arr[ARR_SIZE], numeri_inseriti = 0;
    
    /*
     * Ricevi in input da tastiera una serie di caratteri finchè non arrivi ad ARR_SIZE
     */
    
    do{
        printf("Inserire valore numero %d\n", numeri_inseriti + 1);
        scanf("%d", arr + numeri_inseriti);
        numeri_inseriti++;
    }while (numeri_inseriti < ARR_SIZE);
    
    printf("Array inserito:\n");
    print_array(arr, ARR_SIZE);
    
    printf("Inserire il valore di m:\n");
    scanf("%d", &m);
    risultato = tuttiMaggiori(arr, ARR_SIZE, m);
    
    if(risultato == TRUE) printf("Tutti i valori sono >%d\n", m); 
	else printf("Alcuni valori sono <=%d\n", m);
	
	sommadispari(arr, m);
}

