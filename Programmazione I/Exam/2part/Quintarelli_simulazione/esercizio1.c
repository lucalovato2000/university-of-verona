#include <stdio.h>
#include <stdlib.h>

/*definizione della struttura per la lista concatenata*/
typedef struct elem_{
  int num;
  struct elem_ *next;
} elem;


/*prototipi delle funzioni*/
elem* inserisciInCoda(elem*, int);
elem* inserisciInTesta(elem*, int);
elem* inserisciOrdinato(elem*, int);
elem* rimuovi(elem*, int);
elem* distruggiLista(elem*);
elem* copia(elem*);

void visualizza(elem*);
int esisteElemento(elem*, int);
int massimoLista(elem*);


int main(int argc, char* argv[]){
  
  int r, val;
  elem *listav = NULL, *listav2 = NULL; 
  
  do{
		scanf("%d", &val);
    
    if(val!=-1){
      listav=inserisciInCoda(listav, val);
    }
  }while(val!=-1);

  visualizza(listav);
  
  r = massimoLista(listav);
  printf("Massimo numero risulta: %d\n",r);
  
  int v = esisteElemento(listav, 2);
  printf("esiste elemento 2 nella lista? %d\n\n", v);
  
  listav2 = copia(listav);
  visualizza(listav2);
    
  listav = distruggiLista(listav);

  return 0;
}

elem* inserisciInCoda(elem* lista, int num){
  
  elem *prec;
  elem *tmp = (elem*) malloc(sizeof(elem));
  
  if(tmp != NULL){
    tmp->num = num;
    tmp->next = NULL;
  
    if(lista == NULL){                    // se la lista è vuota fai puntare lista al nodo tmp
      lista = tmp;
    }
    else{                                 /* raggiungi il termine della lista */
      while (lista != NULL){
        prec = lista;                     // fai puntare a prec la lista
        lista = lista->next;
      }
      prec->next = tmp;                   // quando hai finito di scorrere la lista attacca al prec -> next il nodo tmp
    }
  }else{
    printf("Memoria esaurita!\n");
  }
  return lista;
}


// INSERIMENTO IN TESTA MOLTO PIU EASY DELL'INSERIMENTO IN CODA !!!!!!

elem* inserisciInTesta(elem* lista, int num){

  elem *tmp = (elem*) malloc(sizeof(elem)); // alloca un nuovo nodo alla lista per aggiungerlo in testa

  if(tmp != NULL){        // se tmp è diverso da NULL
    tmp->num = num;       // tmp->num = numero inserito in input
    tmp->next = lista;    // tmp->next = lista in input

    lista = tmp;          // lista in input = tmp
  } 
  else{
      printf("Memoria esaurita!\n");
  }
  return lista;
}

/* inserisce un nuovo numero in testa alla lista */
elem* inserisciOrdinato(elem* lista, int num){
  elem *tmp, *prec, *prox;

  tmp = (elem*) malloc(sizeof(elem));   // alloca spazio per un nuovo nodo

  if(tmp != NULL){                      // se nodo è NULL
    tmp->num = num;                     // assegna num al nodo

    if(!lista){                         // se lista vuota fai puntare il next del nodo alla lista, e lista diventa nodo
    	tmp->next = lista;
	    lista = tmp;   
    } 
    else {  
    	if(tmp->num <= lista->num){       // se il numero è minore/uguale al numero della lista
    		tmp->next = lista;              // tmp->next punta alla lista
    		lista = tmp;
    	} 
      else if (!lista->next) {          // se la lista non ha nodo successivo
    		tmp->next = lista->next;  
    		lista->next = tmp;
    	} 
      else {                            // altrimenti scorri la lista ed assegna tmp -> next a prox -> next
    		//for(prox = lista; prox->next && tmp->num > prox->next->num; prox = prox->next);
        while (lista != NULL){
          lista = lista->next;
        }
        tmp->next = prox->next;
        prox->next = tmp;
    	}
    }
  }else{
    printf("Memoria esaurita!\n");
  }

  return lista;
}

void visualizza(elem* lista) {

  while(lista != NULL) {

    printf("%d ", lista->num);      // stampa il numero della lista
    lista = lista->next;            // va avanti col ciclo della lista
  }
  printf("\n");  
} 

/*--------------------------------------
  if (lista != NULL){
    printf("%%d\n", lista->num);
  }
  else{
    if (lista->next != NULL){
      visualizza(lista->next);
    }
  }
--------------------------------------*/
  
elem* distruggiLista(elem* lista){
  elem* tmp;                      // crea una lista temporanea

  while(lista != NULL){
    tmp = lista;                  // assegna a tmp la lista in input
    lista = lista->next;          // MOLTO IMPORTANTE SCORRERE PRIMA DI FARE LA FREE !!!

    free(tmp);                    // free di tmp assegnato in precedenza
  }
  return NULL;
}


int esisteElemento(elem* lista, int num){
  int flag = 0;

  while(lista != NULL){         // finche lista non punta a null
    if(lista->num == num) {     // se il numero a cui punta lista->num == num (input)
      flag = 1;                 // assegna 1 alla variabile flag = 0
    }
    lista = lista->next;        // continua a scorrere la lista
  }
  return flag;
}


elem* rimuovi(elem* lista, int num){
  elem *curr, *prec, *canc;
  int found = 0;

  curr = lista;
  prec = NULL;    

  while(curr && !found){        // !found = found == 0
    if(curr->num == num){       // se lista->num == num in input
      found = 1;
      canc = curr;              // canc = curr
      curr = curr->next;        // curr = curr->next scorro avanti "manualmente"
      
      if(prec != NULL){         // se non ho precedenti, allora prec->next = curr
        prec->next = curr;
      }
      else{
        lista = curr;
      }
      free(canc);
    }
    else{                       // se il corrente non è uguale a num
      prec = curr;              // salva curr in prec e scorri la lista
      curr = curr->next;     
    }
  }
  return lista;

}


int massimoLista(elem* lista){
	
  int cont = 0;                  // valore max = cont = 0

  while(lista != NULL){

    if(lista->num >= cont){     // se il valore della lista è >= alla variabile cont 
      cont = lista->num;        // assegna a cont il numero maggiore uguale di cont
      
      lista = lista->next;
    }
    else{ 
      return 0;
    }
  }
  return cont;
}


  
elem* copia(elem* lista){
	
  elem *newlist = NULL;                                 // crea lista vuota

  while(lista != NULL){

    newlist = inserisciInCoda(newlist, lista->num);     // passa nodo alla funzione inserisciInCoda
    lista = lista->next;                                // scorri la lista
  }
  return newlist;
}