#include <stdlib.h>
#include <stdio.h>

struct automobile {
    char targa[6];
    int anno_immatricolazione;
    char modello[20];
};


int * anni_da_immatricolazione_auto(int anno_immatricolazione){
    int totale = 2018 - anno_immatricolazione;
    return &totale;
}

void modello_auto(char modello_auto[]){
    printf("Inserire modello auto: ");
    scanf("%c", &modello_auto[0]);
}


int *inserire_anno_immatricolazione_auto_cliente(){
    int immatricolazione_auto;
    printf("Inserire anno immatricolazione auto: ");
    scanf("%i", &immatricolazione_auto);
    return &immatricolazione_auto;
}

int main(void) {
    struct automobile *auto_depoca = (struct automobile *) malloc(sizeof(struct automobile));
    if(auto_depoca == NULL)
        printf("ERRORE di allocazione dinamica\n");
    
    printf("Inserire modello auto: ");
    modello_auto(auto_depoca->modello);
    
    struct automobile *alias_auto = auto_depoca;
    auto_depoca->anno_immatricolazione = 1988;
    
    free(alias_auto);
     printf("Anno immatricolazione massimo per essere considerata d'epoca nel 2019: %i\n", auto_depoca->anno_immatricolazione);
    
    
    struct automobile *auto_cliente = (struct automobile *)malloc(sizeof(struct automobile));
    if(auto_cliente == NULL)
        printf("ERRORE di allocazione dinamica\n");
    
    printf("Inserire anno immatricolazione auto: ");
    scanf("%i", &auto_cliente->anno_immatricolazione);
    
    printf("Inserire modello auto: ");
    modello_auto(auto_cliente->modello);
    
    if(auto_cliente->anno_immatricolazione <= auto_depoca->anno_immatricolazione)
        printf("L'auto inserita è considerata d'epoca poichè è dell'anno: %i\n", auto_cliente->anno_immatricolazione);
    else
        printf("Non è considerata d'epoca e deve pagare il bollo intero!\n");
 
    int *eta_auto_depoca, *eta_auto_cliente;
    eta_auto_depoca = anni_da_immatricolazione_auto(auto_depoca->anno_immatricolazione);
    printf("L'età di un'auto per essere d'epoca è: %i\n", *eta_auto_depoca);
    
    eta_auto_cliente = inserire_anno_immatricolazione_auto_cliente();
    printf("L'età di un'auto per essere d'epoca è: %i\n", *eta_auto_depoca);
    printf("L'età della nuova auto è: %i\n", *eta_auto_cliente);
    
    
    return 0;
}








