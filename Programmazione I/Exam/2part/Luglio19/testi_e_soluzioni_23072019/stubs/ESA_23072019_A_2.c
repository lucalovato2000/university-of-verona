/*
Completare il file ESA23072019A2.c scrivendo un sottoprogramma rightshift che ricevuta in ingresso unastringa sorgente 
e un numero intero senz’altro positivo n,  modifica la stringa in modo tale che la stringa finalesia quella iniziale, 
fatta scorrere di n posizioni a destra (con gli ultimincaratteri riportati all’inizio).  
Se peresempio la stringa iniziale `e Esempio ed n `e 1, la stringa finale sar`a oEsempi, se n `e 2, la stringa finale sar`aio
Esemp
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 50

void rightshift(char*, int);


void rightshift(char src[], int n){
  
}


int main(){
 char src[N];
 int num;

  printf("Inserisci una stringa: ");
  scanf("%s",src);
   
  printf("\n Inserisci un numero positivo minore della lunghezza della stringa ");
  do{
	  scanf("%d",&num);	
  }while(num<=0 || num>strlen(src));
  rightshift(src,num);
  printf("\n Stringa con shift a destra di %d posizioni: %s\n", num, src);
 
  return(0); 
}