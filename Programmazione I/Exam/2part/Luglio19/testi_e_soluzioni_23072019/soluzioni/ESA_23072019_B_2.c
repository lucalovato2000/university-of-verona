#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 50

void scorrimentodx(char*, int);


void scorrimentodx(char src[], int n){
	int i,j;
	int l;	
	char *tmp=(char*) malloc((n+1)*sizeof(char));
	
	l=strlen(src);
	
	for(i=0;i<n;i++){
		tmp[i]=src[i];
	}
	for(i=n;i<l;i++){
		tmp[n]=src[i];
		src[i]=tmp[0];
		for(j=0;j<n;j++)
			tmp[j]=tmp[j+1];
	}
	for(j=0;j<n;j++)
		src[j]=tmp[j];	
}


int main(){ 
  
	char src[N];
	int num;

	printf("Inserisci una stringa: ");
	scanf("%s",src);

	printf("\n Inserisci un numero positivo minore della lunghezza della stringa ");
	do{
		scanf("%d",&num);	
	}while(num<=0 || num>strlen(src));
	scorrimentodx(src,num);
	printf("\n Stringa con shift a destra di %d posizioni: %s", num, src);

	return(0); 
}