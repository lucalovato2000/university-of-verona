#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 50

void rightshift(char*, int);


void rightshift(char src[], int n){
	int i, j, l;	
	char *tmp=(char*)malloc((n+1)*sizeof(char));
	
	l = strlen(src);

	for(i=0;i<n;i++){			// per n (dato dall'utente) copia la stringa in tmp[i]
		tmp[i]=src[i];
	}
	for(i=n;i<l;i++){			// per n (dato dall'utente) ad l (lunghezza stringa) copia stringa in tmp[n] [CONCATENARE STRINGHE !!!]
		tmp[n]=src[i];			// copia la restante stringa in tmp (senza modificare valori)
		src[i]=tmp[0];			// stringa[i] = tmp[0]
		for(j=0;j<n;j++)		// da zero a n dato dall'utente, copia tmp[j] in tmp[j+1]
			tmp[j]=tmp[j+1];
	}
	for(j=0;j<n;j++){
		src[j]=tmp[j];
	}	
}


int main(){ 
	
	char src[N];
	int num;

	printf("Inserisci una stringa: ");
	scanf("%s",src);

	printf("\n Inserisci un numero positivo minore della lunghezza della stringa ");

	do{
		scanf("%d",&num);	
	}while(num<=0 || num>strlen(src));

	rightshift(src,num);

	printf("\n Stringa con shift a destra di %d posizioni: %s", num, src);
 
  	return(0); 
}