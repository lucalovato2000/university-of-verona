#include <stdlib.h>
#include <stdio.h>

struct automobile {
    char targa[6];
    int anno_immatricolazione;
    char modello[20];
};


void modello_auto(char modello_auto[]){
    printf("Inserire modello auto: ");
    //errore 1: viene salvato solo il primo char della stringa inserita da tastiera in modello_auto[0]. 
    //Il resto rimane nel buffer, causando malfunzionamenti nelle seguenti scanf().
    scanf("%c", &modello_auto[0]);
}



int main(void){
    struct automobile *auto_depoca = (struct automobile *) malloc(sizeof(struct automobile));
    if(auto_depoca == NULL)
        printf("ERRORE di allocazione dinamica\n");
    
    printf("Inserire modello auto: ");
    modello_auto(auto_depoca->modello);
    
    struct automobile *alias_auto = auto_depoca; //aliasing
    auto_depoca->anno_immatricolazione = 1988;
    
    free(alias_auto);
    //errore 2: leggere/scrivere in una zona di memoria deallocata da un alias...
    //auto_depoca è un dangling pointer!!
    printf("Anno immatricolazione massimo per essere considerata d'epoca nel 2019: %i\n", auto_depoca->anno_immatricolazione);
    
    
    //Inserimento prima auto:
    struct automobile *auto_cliente = (struct automobile *)malloc(sizeof(struct automobile));
    if(auto_cliente == NULL)
        printf("ERRORE di allocazione dinamica\n");
    
    printf("Inserire anno immatricolazione auto: ");
    scanf("%i", &auto_cliente->anno_immatricolazione);
    
    printf("Inserire modello auto: ");
    modello_auto(auto_cliente->modello);
    
    if(auto_cliente->anno_immatricolazione <= auto_depoca->anno_immatricolazione)
        printf("L'auto inserita è considerata d'epoca poichè è dell'anno: %i\n", auto_cliente->anno_immatricolazione);
    else
        printf("Non è considerata d'epoca e deve pagare il bollo intero!\n");
 
    //Inserimento seconda auto:
    //errore 3: andando a salvare l'indirizzo dello slot di memoria appena creato sullo heap nel puntatore auto_cliente, si crea un memory leak.
    // NON VA BENE RICHIAMARE AUTO CLIENTE
    auto_cliente = (struct automobile *)malloc(sizeof(struct automobile));
    if(auto_cliente == NULL)
        printf("ERRORE di allocazione dinamica\n");
    
    printf("Inserire anno immatricolazione auto: ");
    scanf("%i", &auto_cliente->anno_immatricolazione);
    
    printf("Inserire modello auto: ");
    modello_auto(auto_cliente->modello);
    
    if(auto_cliente->anno_immatricolazione <= auto_depoca->anno_immatricolazione)
        printf("L'auto inserita è considerata d'epoca poichè è dell'anno: %i\n", auto_cliente->anno_immatricolazione);
    else
        printf("Non è considerata d'epoca e deve pagare il bollo intero!\n");
    
    return 0;
}
