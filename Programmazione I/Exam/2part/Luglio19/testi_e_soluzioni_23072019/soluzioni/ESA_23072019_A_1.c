#include <stdio.h>
#include <stdlib.h>

// struttura dati
struct node_t{
	int value;
	struct node_t * next;
};

int conta(struct node_t *, int);
struct node_t* delfromlist(struct node_t *, int);
struct node_t * rimuovi(struct node_t *, int);



// conta quante occorrenze di x sono presenti in lista. Scrivere funzione ricorsiva
int conta(struct node_t * lista, int x){
	
	if(lista == NULL) {
		return 0;
	}
	if(lista->value == x) {					// se il valore lista->value è uguale al valore in input x
		return 1+conta(lista->next, x);		// return 1 + conta(next, x);
	}
	else{
		return conta(lista->next, x);	
	}
}


// cancella dalla lista tutte le occorrenze di num
struct node_t * rimuovi(struct node_t * lista, int num){
	  struct node_t *prec, *canc;

	  prec = NULL;    
	    
	  while(lista != NULL){
	    if(lista->value == num){	// se il valore della lista è quello da cancellare
	      canc = lista;	// canc = lista
	      lista = lista->next;     	
	      if(prec != NULL)
	        prec->next = lista;	// se prec == null attacca prec alla lista
	      else
	        lista = lista;	// altrimenti assegna lista a lista e pulisci
	      free(canc);
	    }
	    else{
	      prec=lista;
	      lista = lista->next;     
	    }
	  }
	  return lista;

	}


/* cancella dalla lista l tutti i nodi presenti almeno n volte. Richiama al suo interno conta e rimuovi (questo e' un suggerimento, si puo' fare altrimenti, ma conta deve
	essere definita) */

struct node_t* delfromlist(struct node_t * l, int n){
	struct node_t *lista;
	int cont;
	int val;
  
	lista=l;
	
	while(lista != NULL){
		cont = conta(lista,lista->value);
		if(cont>=n){
			val=lista->value;
			//la lista cambia a causa di rimuovi, bisogna trovare in anticipo quale sara' il prossimo nodo da visitare
			do{
				lista=lista->next;
			}while(lista!=NULL && lista->value==val);
			
			l=rimuovi(l,val);
		}
		else lista=lista->next;
	}
			
	return l;		
}
		

// stampo la lista: usato nel main
void stampa_lista(struct node_t * n){

	while(n != NULL){
		printf("%d \t", n->value);
		n = n->next;
	}
	printf("\n");
}


// inserimento in coda: usato nel main per creare la lista di partenza
struct node_t * inserisciInCoda(struct node_t * lista, int num){

	struct node_t *prec;
	struct node_t *tmp = (struct node_t *)malloc(sizeof(struct node_t));
  
	if(tmp != NULL){
		tmp->next = NULL;
		tmp->value = num;

		if(lista == NULL){
			lista = tmp;
		}
		else{
			/*raggiungi il termine della lista*/
			for(prec=lista;prec->next!=NULL;prec=prec->next);
			prec->next = tmp;
		}
	} 
	else{
		printf("Memoria esaurita!\n");
	}
	
	return lista;
}


int main(){
	struct node_t * head = NULL, * new = NULL;
	int num;

	head = inserisciInCoda(head,7);
	head = inserisciInCoda(head, 3);
	head = inserisciInCoda(head, 2);
	head = inserisciInCoda(head, 3);
	head = inserisciInCoda(head, 3);
	head = inserisciInCoda(head, 2);
	head = inserisciInCoda(head, 4);

	printf("\n Lista originale: ");
	stampa_lista(head);

	printf("\n Inserisci un numero positivo: ");
	do{
		scanf("%d", &num);
	}while(num<=0);
	
	head=delfromlist(head,num);
	
	printf("\n Lista finale: ");

	stampa_lista(head); 

	return 0;
}