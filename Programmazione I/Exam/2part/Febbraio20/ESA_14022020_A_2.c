#include <stdio.h>
#include <stdlib.h>
#define N 6

int maggioredik(int *,int, int);
int simmetrico(int *,int,int);

int maggioredik(int a[],int len, int k){
	int i,j;
	int flag;
	
	/* controllo se la dimensione dell'array non permette di trovare il valore e restituisco -1 */
	if(len<=0)
		return -1;	
	
	/* altrimenti cerco il primo valore >k che non ammette valori minori  */
	for(i = 0; i<len; i++){
		if(a[i]>k){
			flag=1;
			for(j = i+1; j<len && flag; j++){
				if(a[j]<a[i] && a[j]>k){
					flag = 0;
				}	
			
			}
			if(flag == 1)
				return a[i];	
		
	
		}
	}
	
	return -1;

}
	

/* funzione simmetrico ricorsiva: restituisce 0 o 1 a seconda che l'array non possa/possa essere letto allo stesso modo da sinistra a destra e viceversa */

int simmetrico(int a[], int dim, int pos){
	
	// casi base: se array vuoto o con un elemento allora e' simmetrico
	if(dim==0 || pos==dim-1)
		return 1;

	
	// se due elementi simmetrici sono diversi non può essere simmetrico
	if(a[dim-1]!=a[pos])
		return 0;
	// altrimenti mi sposto sia a sx che dx 
	else return simmetrico(a, dim-1, pos+1);
	
}



int main(){
	int k,i,r;
	int a[N];
	printf("Inserisci array: ");
	for(i=0; i<N; i++){
		scanf("%d",&a[i]);
	}
	printf("\n Inserisci k: ");
	scanf("%d", &k);
	printf("\n Valore maggiore di %d: %d\n",k, maggioredik(a,N,k));
	
	r=simmetrico(a,N,0);
	printf("\n Esito su array: %d",r);

	return 0;
}