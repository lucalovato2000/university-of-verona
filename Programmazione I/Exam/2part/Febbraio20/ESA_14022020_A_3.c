#include <stdlib.h>
#include <stdio.h>

/* ci sono diverse soluzioni al problema. Una tra queste è rappresentata dal seguente codice:
 */


struct list {
    int head;
    struct list *tail;
};

struct list *static_pointer_to_list;    //variabile globale, DATA

struct list *genera_lista_din(int i){
    struct list *tmp;
    //passo base raggiungo il 4 'elemento' ovvero NULL
    if (i<1)
	    return NULL;
    //passo ricorsivo crea un nodo con head=i, decremento i e chiamo ricorsivamente la funzione
    else{
        tmp = (struct list *)malloc(sizeof(struct list));
        tmp->head=i;
        i--;
        tmp->tail=genera_lista_din(i);
    }
	return tmp;
}



int main(void) {
    struct list *pointer_to_list=genera_lista_din(2);   //memory leak poichè pointer_to_list NON DEALLOCATA
                 
                 pointer_to_list=genera_lista_din(1);

    static_pointer_to_list=pointer_to_list;
    // IN QUESTO PUNTO CI SI TROVA NELLO STATO RAPPRESENTATO NELL'IMMAGINE
    //Si è creato un memory leakage agli indirizzi 0x000164-0x000170
}
