#include <stdio.h>   // printf
#include <stdlib.h>  // malloc, NULL
#include <string.h>  // strlen

struct mylist_t {
    char value;
    struct mylist_t *next;
};

struct mylist_t *binary_to_mycharlist(const char *binary);
struct mylist_t *new_char(char c);
void print_mycharlist(struct mylist_t *list);

/*
 * Entry point
 * Output:
 * Hello
 */

int main() {
    char binary[] = "0100100001100101011011000110110001101111";
    struct mylist_t *list = binary_to_mycharlist(binary);

    print_mycharlist(list);
    printf("\n");

    return 0;
}

/*
 * Crea e restituisce un puntatore a un elemento mylist_t
 */
struct mylist_t *new_char(char c) {

    struct mylist_t *mychar = (struct mylist_t *)malloc(sizeof(struct mylist_t));   // creo un nodo di tipologia mylist_t

    mychar->value = c;      // assegno c in input
    mychar->next = NULL;    // next = NULL

    return mychar;
}

/*
 * Stampa una lista di mylist_t, a partire dall'elemento indicato
 */

void print_mycharlist(struct mylist_t *list) {

    while (list != NULL) {

        printf("%c", list->value);
        list = list->next;  // scorro lista [presenza del while!!]
    }
}

/*
 * Crea una lista concatenata di elementi di tipo mylist_t
 * e restituisce un puntatore all'elemento iniziale (list).
 * Versione 1.
 */
struct mylist_t *binary_to_mycharlist1(const char *binary) {
    
    struct mylist_t *list = NULL;
    struct mylist_t *mychar;
    struct mylist_t *prev;
    int i = 0;

    while (i < strlen(binary)) {
        int pot = 128;
        int ord = 0;

        for (int j = 0; j < 8; j++) {
            if (binary[i++] == 49) {
                ord += pot;
            }
            pot /= 2;
        }
        mychar = new_char(ord);

        if (list == NULL) {
            list = mychar;
            prev = mychar;
        } else {
            prev->next = mychar;
            prev = mychar;
        }
    }
    return list;
}


/*
 * Crea una lista concatenata di elementi di tipo mylist_t
 * e restituisce un puntatore all'elemento iniziale (list).
 * Versione 2.
 */
struct mylist_t *binary_to_mycharlist(const char *binary) {
    struct mylist_t *list = NULL;
    struct mylist_t *mychar;
    struct mylist_t *prev;
    int i = 0;

    while (i < strlen(binary)) {
        int ord = 0;

        for (int j = 0; j < 8; j++) {
            ord += binary[i++] - 48;
            ord <<= 1;
        }
        ord >>= 1;
        mychar = new_char(ord);

        if (list == NULL) {
            list = mychar;
            prev = mychar;
        } else {
            prev->next = mychar;
            prev = mychar;
        }
    }
    return list;
}
