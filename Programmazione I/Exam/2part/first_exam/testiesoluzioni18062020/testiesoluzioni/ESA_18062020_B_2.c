#include<stdio.h>
#include<string.h>
#define FNAME "testoB.txt"
#define MAXS 15
#define SOGLIA 3

// prototipi 
int conta(char *, int *);
void trova(FILE *);

int main(){
  FILE *fp;

  fp=fopen(FNAME,"r");
  if(fp){
	  trova(fp);
    
    fclose(fp);
  } 
  else
    printf("Errore di apertura del file\n");
  return 0;
  
}

// cerca nel file fp le parole con almeno 3 vocali di cui almeno una a e le stampa 
void trova(FILE * fp){
	char p[MAXS+1];
	int quante=0;
	int ok, i;
	
    fscanf(fp,"%s",p);
    while(!feof(fp)){
		quante=0;
		ok=conta(p,&quante);
    if(ok>=SOGLIA && quante>=1){
        printf("%s", p);
        printf("\n");
      }
      fscanf(fp,"%s",p);
    }
	
	
}

// conta quante vocali ci sono in s e inserisce nella variabile num quante di queste sono 'a'
int conta(char *s, int *numa){
	if(*s=='\0')
		return 0;
	if(*s=='a' || *s=='e'|| *s=='i' || *s=='o' || *s=='u'){
		if(*s=='a')
			(*numa)++;
		return 1+conta(s+1,numa);
	}
	return conta(s+1,numa);
}
	
	
	
	
