#include<stdio.h>
#include<string.h>
#define FNAME "testo.txt"
#define MAXS 15
#define SOGLIA 4

// prototipi

void trova(FILE *);
void inverti(char *,int,int);

int main(){
  int i;
  FILE *fp;

  // apro il file testo.txt in lettura
  fp=fopen(FNAME,"r");
  if(fp){
	  trova(fp);
	  fclose(fp);
  } else
    printf("Errore di apertura del file\n");
  
  return 0;
}

// trova nel file fp tutte le parole con più di 4 caratteri ripetute 2 volte e ne stampa a video un'occorrezza invocando inverti
void trova(FILE * fp){
	char p1[MAXS+1], p2[MAXS+1];
	
	fscanf(fp,"%s %s",p1, p2);
	
	while(!feof(fp)){
  	  if(strcmp(p1,p2)==0 && strlen(p1)>4){
   	 	  inverti(p1,0,strlen(p1));
	 	  printf("%s\n",p1);
	  }
  	  strcpy(p1,p2);
  	  fscanf(fp,"%s",p2);
	}
}

// data la stringa s, l'indice corrente pos e la dimensione dim di s, inverte la stringa s

void inverti(char * s, int pos, int dim){
	char tmp;
	if(pos<dim/2){
		tmp=*(s+pos);
		*(s+pos)=*(s+dim-pos-1);
		*(s+dim-pos-1)=tmp;
		inverti(s,pos+1,dim);
	}	
	
}