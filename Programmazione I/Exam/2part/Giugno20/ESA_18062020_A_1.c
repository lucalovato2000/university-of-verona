/**
 * Programma per la gestione di una lista concatenata di numeri interi, es. 3 -> 6 -> 10 -> 2 -> 8.
 * In particolare, la funzione find() riceve in ingresso una lista e un valore intero k e restituisce il riferimento all'elemento in posizione k
 * a partire dall'inizio della lista. Il primo elemento della lista è identificato da k=1, il secondo da k=2, e così di seguito.
 * Se nella lista non esiste l'elemento in posizione k, il sottoprogramma restituisce NULL.
 */
#include <stdio.h>
#include <stdlib.h>

typedef struct _node {
    int value;
    struct _node *next;
} node_t;

node_t *new_node(int n);
node_t *append(node_t *head, int n);
void print_at(node_t *head, int pos);
void println(node_t *head);
node_t *find(node_t *head, int pos);

/*
 * Program entry point
 * Ouput:
 * 3 -> 6 -> 10 -> 2 -> 8
 * list[2] = 6
 * list[7] = index out of bounds
 */

int main() {
    node_t *head = NULL;

    head = append(head, 3);
    head = append(head, 6);
    head = append(head, 10);
    head = append(head, 2);
    head = append(head, 8);

    println(head);
    print_at(head, 2);
    print_at(head, 7);

    return 0;
}

/**
 * Creates a new node with the given value.
 */
node_t *new_node(int value) {
    node_t *node = (node_t *)malloc(sizeof(node_t));

    node->value = value;
    node->next = NULL;

    return node;
}

/**
 * Appends an element with the given value at the end of the list.
 */
node_t *append(node_t *head, int value) {
    node_t *new = new_node(value);

    if (head == NULL) {
        head = new;
    } else {
        node_t *current, *prev;
        current = prev = head;

        while (current) {
            prev = current;
            current = current->next;
        }
        prev->next = new;
    }

    return head;
}

/**
 * Prints the value's node at a given position.
 * If the position is out of bounds of the list, prints a warning message.
 */
void print_at(node_t *head, int pos) {
    int cont = 1, flag = 0, tmp;

    while(head!=NULL){
        tmp=head->value;
            if(cont==pos){
                flag=1;
                printf("list[%d]=%d\n", pos, tmp);
            }
        head=head->next;
        cont++;
        
    }
    if(flag==0)
        printf("list[%d]=%s\n", pos, "index out of bounds");
}

/**
 * Prints a representation of the list.
 */
void println(node_t *head) {

    while(head!=NULL){
            printf("%d", head->value);
            if(head->next!=NULL)
                printf(" -> ");
            head=head->next;
    }
    printf("\n");
}

/**
 * Returns a pointer to the element in the list at the given position.
 * If the position exceeds the list index range, NULL is returned.
 */
node_t *find(node_t *head, int pos) {
   
    
    
    return NULL;

}
