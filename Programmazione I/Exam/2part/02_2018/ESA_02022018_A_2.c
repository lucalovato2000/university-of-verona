/*
Si consideri un percorso (path) costituito da una sequenza di tratte descritte dalla seguente struct:
struct section {
    double value;
    struct section *next;
};

Realizzare le seguenti funzioni:
is_valid() determina se un valore double appartiene ad un intervallo di valori specificato.
create_path() dato un array di valori, costruisce una lista concatenata di section per valori appartenenti all’intervallo [from, to] e ritorna il puntatore al primo elemento (head).
print() stampa una lista di section

Se tutto  ́e corretto, l’esecuzione del sequente programma:
const double from = 4;
const double to = 24;

int main(void){
    double values[] = {3.4, 22.7, 27.2, 16.4, 21.3};
    struct section *head = create_path(values, 5, from, to);
    print(head);
}

stamperà:
22.70
16.40
21.30
*/

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

const double from = 4;
const double to = 24;

struct section {
    double value;
    struct section *next;
};

bool is_valid(double value, double from, double to);
struct section* create_path(double values[], int size, double from, double to);
void print(struct section* head);

// Entry point
int main(void){

    double values[] = {3.4, 22.7, 27.2, 16.4, 21.3};

    struct section *path = create_path(values, 5, from, to);

    print(path);
}

// is_valid() determina se un valore double appartiene ad un intervallo di valori specificato.
bool is_valid(double value, double from, double to) {
    // return value >= from && value <= to;
    if (value >= from && value <= to) {
        return true;
    }
    else{
        return false;
    }
}

// create_path() dato un array di valori, costruisce una lista concatenata di section per valori appartenenti all’intervallo [from, to] e ritorna il puntatore al primo elemento (head).
struct section* create_path(double values[], int size, double from, double to) {

    struct section *temp, *prev, *head = NULL;
    int i;

    for(i = 0; i < size; i++) {
        if(is_valid(values[i], from, to)) { // se il valore è valido

            temp = (struct section *)malloc(sizeof(struct section));  // alloca lo spazio per un nodo temp
            
            temp->value = values[i];    // temp->value è il valore valido
            temp->next = NULL;          // temp->next è NULL

            if(head == NULL) {  
                head = temp;    // sostituisci la testa entrante con il nodo temp
            }else {
                prev->next = temp;  // se la testa non è NULL scorri assegna prev->next = temp
            }

            prev = temp;    // previous = tmp  
        }
    }
    return head;
}

// print() stampa una lista di section
void print(struct section* head) {

    if(head == NULL) {
        printf("%s", "Lista vuota");
    }else {
        struct section* temp = head;
        int i = 1;

        do {
            printf("%i: %.2f\n", i, temp->value);
            temp = temp->next;
            i++;
        } while(temp != NULL);

    }
}
