// prodotto tra matrici quadrate

#include <stdio.h>
#define SIZE 3

int main(void) {

    // const indica che non può essere modificata durante l'esecuzione
    const int matriceA[3][3] = {    
        {1, 2, 3},
        {4, 5, 6},
        {7, 8, 9}
    };

    // const indica che non può essere modificata durante l'esecuzione
    const int matriceB[3][3] = {    
        {1, 2, 3},
        {1, 2, 3},
        {1, 2, 3}
    };

    int riga, colonna, i, tmp;
    int matriceR[3][3];
    
    // calcolo il risultato A*B:
    for (riga = 0 ; riga < SIZE ; riga++) {
        for(colonna = 0 ; colonna < SIZE ; colonna++) {

            tmp = 0;
            for (i = 0 ; i < SIZE ; i++) {
                  tmp = tmp + matriceA[riga][i] * matriceB[i][colonna];
             }
            matriceR[riga][colonna] = tmp;
        }
    };
    
    
    //stampo il risultato
    for (riga = 0 ; riga < SIZE ; riga++){
        for(colonna = 0 ; colonna < SIZE ; colonna++)
            printf("%i ", matriceR[riga][colonna]);
        printf("\n");
    };
    return 0;
}
