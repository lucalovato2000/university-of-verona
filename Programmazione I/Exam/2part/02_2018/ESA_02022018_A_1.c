#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define TOT 100
#define N 10


int conta_vocali(char *s) {
	int conteggio = 0;

	while(*s){ 	// finchè (*s != NULL)

		if( *s == 'A' || *s =='E' || *s == 'I' || *s=='O' || *s=='U') {
			conteggio++;
		}
		s++;
	}
	return conteggio;
}

int main(void){

	srand(time(NULL));
	
	char stringhe [TOT][N + 1];
	float perc;
	int i, j, p;
	
	do{
		printf("inserire la percentuale di variazione, tra 0 e 1: \n");
		scanf("%f", &perc);

	}while(perc < 0.0 || perc > 1.0);
	
	for(i = 0; i<N; i++) {

		stringhe[0][i] = (rand() % ('Z' - 'A')) + 'A';
		stringhe[0][N] = '\0';
	}
	//printf("la stringa base e': %s \n", stringhe[0]);
	
	int alterazioni = N * perc;

	for(i = 1; i < TOT; i++){

		for(j = 0; j < N + 1; j++){
			stringhe[i][j] = stringhe[0][j];
		}
		for(j = 0; j < alterazioni; j++){
			p = rand() % (N - 1);
			stringhe[i][p] = (rand() % ('Z' - 'A')) + 'A';
		}
	}
	
	printf("le stringhe sono:\n");

	for(i = 0; i < TOT; i++){
		printf("%s\n", stringhe[i]);
	}
	printf("il numero di vocali per ogni stringa e':\n");
	
	for(i = 0; i < TOT; i++){
		printf("%i\n", conta_vocali(stringhe[i]));
	}
	
	return 0;
}