#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TOP 1
#define MEDIUM 2
#define STANDARD 3

struct sticker_t {
    char *name;
    int type;
    struct sticker_t *next;
};

struct collection_t {
    char *name;
    struct sticker_t *head;
};

struct collection_t *create_collection(char *name);
void add_sticker(struct collection_t *collection, char *name, int type); 
int print_collection(struct collection_t *collection, int type);

void main(){
    struct collection_t *collection1 = create_collection("Collezione calciatori");
    add_sticker(collection1, "Rossi", MEDIUM);
    add_sticker(collection1, "Verdi", TOP);
    add_sticker(collection1, "Bianchi", MEDIUM);
    print_collection(collection1, MEDIUM);
}

struct collection_t *create_collection(char *name) {
    struct collection_t *new_collection = (struct collection_t *) malloc(sizeof(struct collection_t));    
    new_collection->name = (char *)malloc(sizeof(strlen(name)));
    strcpy(new_collection->name, name);    
    new_collection->head = NULL;
    return new_collection;
}

void add_sticker(struct collection_t *collection, char *name, int type) {
    struct sticker_t *current;
    struct sticker_t *prev;
    
    struct sticker_t *new_sticker = (struct sticker_t *) malloc(sizeof(struct sticker_t));
    new_sticker->name = (char *)malloc(sizeof(strlen(name)));
    strcpy(new_sticker->name, name);
    new_sticker->type = type;
    
    if(collection->head == NULL) {
        collection->head = new_sticker;
    } else {
        prev = collection->head;
        current = collection->head->next;

        while(current != NULL) {
            prev = current;
            current = current->next;
        }
        
        prev->next = new_sticker;
    }    
}

int print_collection(struct collection_t *collection, int type) {
    struct sticker_t *current;
    
    printf("%s:\n", collection->name);
    printf("type %i:\n", type);
    
    current = collection->head;
    
    while(current != NULL) {
        if(current->type == type) {
            printf("\t%s\n", current->name);
        }
        
        current = current->next;
    }    
}


