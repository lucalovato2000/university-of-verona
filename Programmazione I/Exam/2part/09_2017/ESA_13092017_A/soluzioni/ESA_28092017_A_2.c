#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 20


void stampa_array(int * a, int lunghezza){
	int i;

	for(i=0; i<lunghezza; i++){
		printf("%i ", a[i]);
	}
	printf("\n");
}

int * crea_array(int lunghezza){
	int * a = (int *)malloc(lunghezza * sizeof(int));
	int i;
	for(i=0; i<lunghezza; i++){
		a[i] = (rand() % 21) - 10;
	}
	return a;
}

int cerca(int * a, int lunghezza){
	int max = 0;
	int i;
	int count = 0;
	for(i=0; i<lunghezza; i++){
		if(a[i] >= 0){
			count++;
			if(count > max){
				max = count;
			}
		}
		else{
			count = 0;
		}
	}
	return max;
}


void main(){
	srand(time(NULL));
	int * a = crea_array(N);
	stampa_array(a, N);
	printf("lunghezza sequenza positiva piu' lunga: %i \n", cerca(a,N));
	free(a);
}