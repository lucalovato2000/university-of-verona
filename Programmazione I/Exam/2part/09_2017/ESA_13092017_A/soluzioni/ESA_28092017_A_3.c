//main.c
#include <stdlib.h>
#include <stdio.h>


struct list {
    int head;
    struct list *tail;
};

struct list *construct_list(int head, struct list *tail){
    struct list *this = malloc(sizeof(struct list));
    this->head = head;
    this->tail = tail;
    return this;
}

int list_sum(struct list *this) {
    if (this == NULL)
        return 0;
    else
        return this->head + list_sum(this->tail);
}

void list_print_aux(struct list *this){
    if(this == NULL){}
    else{
        printf("%i", this->head);
        list_print_aux(this->tail);
    }
}

void list_print(struct list *this){
    printf("[");
    list_print_aux(this);
    printf("]");
}


void list_add_followers(struct list *this){
    if(this != NULL){
        this->head += list_sum(this->tail);

        list_add_followers(this->tail);
            }
}



int main(void) {
  
    struct list * l = construct_list(11,
                                     construct_list(22,
                                                    construct_list(33, NULL)));
    
    printf("Nella lista ");
    list_print(l);
    printf("\n");
    
    printf("La somma dei suoi elementi è: %i\n", list_sum(l));
    
    printf("Incrementando ogni elemento con la somma dei suoi elementi seguenti, diventa: ");
    list_add_followers(l);
    list_print(l);
    printf("\n");
  return 0;
}
