/*

Scrivere un sottoprogramma che riceve in ingresso un array di valori interi v e qualsiasi altro
parametro ritenuto necessario, ed altri due valori interi da e a. Il sottoprogramma verifica se
nell’array sono presenti tutti e soli i valori inclusi nell’intervallo [da,a], senza ripetizioni. In
caso positivo il sottoprogramma restituisce 1, 0 in caso contrario.

*/

#include <stdio.h>
#define N 5


int controllo(int V[N], int da, int a){

	int i,j;
	int ot=0;
	for(i=0;i<N;i++){	//doppio ciclo cosi vedo se un valore con indice i è uguale ad un valore di indice j
		for(j=0;j<N;j++){
			if((V[i]!=V[j])&& V[i]>da && V[i]<a)
				ot=1;
		}
	}
	
		return(ot);
}


//V[i]>=da && V[i]<=a)
int main(){

	int V[N];
	int i;
	
	int da;
	int a;

	int ris;

	for(i=0;i<N;i++){
		printf("Inserisci un valore: ");
		scanf("%d", &V[i]);
	 }
	
	printf("Inserisci un valore: ");
	scanf("%d", &da);

	printf("Inserisci un valore: ");
	scanf("%d", &a);
	
	ris= controllo(V,da,a);

	printf("Risulato: %d \n", ris);
}
