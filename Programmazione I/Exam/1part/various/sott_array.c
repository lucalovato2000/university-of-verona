/*

Si scriva un programma in linguaggio C che riceva in ingresso da tastiera due array di char.
Si consideri che ciascun array può contenere al massimo 30 caratteri. 
Il programma deve verificare se il secondo array inserito è contenuto almeno una volta all’interno
del primo (ossia se il secondo array è un sottoarray del primo).

*/

#include <stdio.h>

#define N 10
#define M 5

int sottoarray(char V[N], char V1[M]){
	
	int i,j;
	int flag=0;

	for(i=0;i<M;i++){
		for(j=0;j<N;j++){
			if(V[j]==V1[i] && V[j+1]==V1[i+1])
				flag=1;
		}
	}

	return(flag);
}


int main(){

	int i,j;
	char V[N];
	char V1[N];

	int ris;

	for(i=0;i<N;i++){
		printf("Inserisci un carattere del primo array: ");
		scanf(" %c", &V[i]);
	}
	for(i=0;i<M;i++){
			printf("Inserisci un carattere del secondo array: ");
			scanf(" %c", &V1[i]);
	}

	ris=sottoarray(V,V1);
	
	if(ris=1)
		printf("Il secondo array inserito è contenuto nel primo");
	else
		printf("Il secondo array inserito non è contenuto nel primo");
}
