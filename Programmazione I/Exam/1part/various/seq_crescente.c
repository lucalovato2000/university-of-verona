/*

Scrivere un programma C che legge da tastiera una sequenza di numeri (di lunghezza a priori
indefinita e che termina con uno 0 – che non fa parte della sequenza) e stabile se si tratta di
una sequenza crescente di numeri.

*/

#include <stdio.h>

int main(){

	int input;
	int temp;
	int flag=1;

	
	printf("inserisci un valore: ");
	scanf("%d", &input);

	temp=input;

	do{
		printf("Inserisci un valore: ");
		scanf("%d", &input);
		
		if(input<temp && input!=0){
			flag=0;
		}
		
		temp=input;

	}while(input!=0);

	if(flag==0)
		printf("la sequenza inserita non è crescente");

	if(flag==1)
		printf("la sequenza inserita è crescente");
	

}
