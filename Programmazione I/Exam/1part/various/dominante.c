/*

In un array bidimensionale di valori interi, si definisce dominante ogni elemento dell’array
che è strettamente maggiore di tutti gli elementi dell’array bidimensionale che si trova in
basso a destra rispetto all’elemento stesso (si veda la figura), non considerando però tutti gli
elementi presenti nell’ultima colonna e nell’ultima riga. Si realizzi un sottoprogramma che
ricevuto in ingresso un array bidimensionale e qualsiasi altro parametro ritenuto strettamente
necessario calcoli e restituisca al chiamante il numero di elementi dominanti presenti. Nel
contesto di utilizzo del sottoprogramma, sono presenti le seguenti direttive/istruzioni riportate
di seguito.

*/

#include <stdio.h>
#define R 4
#define C 8

int dominante(int M[R][C]){

	int i,j,m,n;
	int flag=0;
	int dom=1;
	
	for(i=0;i<R-1;i++){
		for(j=0;j<C-1;j++){
	
			for(m=i+1;m<R;m++){
				for(n=j+1;n<C;n++){
					if(M[i][j]<=M[m][n])
						dom=0;
				}
				if(dom)	
					flag++;
			}
			dom=1;
		}
	}
	return(flag);
}

int main(){
	
	int i,j;
	int ris;

	int M[R][C]={	{5,9,2,4,1,7,2,4},
				{3,5,6,2,5,6,1,2},
				{1,3,4,7,8,8,3,0},
				{1,3,5,6,7,8,2,1}	};
	
	ris= dominante(M);	

	printf("I valori dominanti risultano: %d", ris);
}
