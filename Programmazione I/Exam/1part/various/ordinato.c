/*
Scrivere programma C che dati due array di interi ORDINATI (al massimo di 10 elementi), costruisce un
terzo array ordinato che è la fusione dei due di partenza.
*/

#include <stdio.h>
#define N 10
#define M 20

int main(){

	char V[N];
	char V1[N];
	
	char V2[M];
	int i,m, j=0;

	for(i=0;i<N;i++){
		printf("Inserisci un carattere del primo array: ");
		scanf(" %c", &V[i]);
	}

	for(i=0;i<N;i++){
		printf("Inserisci un carattere del secondo array: ");
		scanf(" %c", &V1[i]);
	}
			
	for(m=0;m<11;m++){
		V2[m]=V[m];
	}
	

	for(m=11;m<M;m++){
		V2[m]=V1[j];
		j++;
	}


	printf("Array fusione: ");
	for(m=0;m<M;m++){
		printf("% c", V2[m]);
		
	}
	printf("\n");
}
