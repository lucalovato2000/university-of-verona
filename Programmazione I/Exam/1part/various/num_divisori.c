/*

Sia dato un numero intero positivo N inserito da tastiera. Si scriva un programma in
linguaggio C che calcoli i numeri interi che sono divisori di N. Dire inoltre se N è un numero
primo.

*/

#include <stdio.h>

int main(){

	int num;
	int i;

	int cont=0;
	
	do{
		printf("Inserisci un valore positivo: ");
		scanf("%d", &num);
	}while(num<=0);

	for(i=1;i<=num;i++){
		if(num%i==0)
			cont=cont+1;
	}

	if(cont>2)
		printf("Il numero inserito ha %d divisori \n", cont);
	else
		printf("Il numero risulta primo \n");

}
