/*

Scrivere un programma C che legge da tastiera una sequenza di numeri (di lunghezza a priori
indefinita e che termina con uno 0 – che non fa parte della sequenza) e stampare quanti di
essi sono primi.

*/

#include <stdio.h>

int main(){

	int num;
	int i;
	int cont=0;
	int primo=0;
	
	do{
		printf("Inserisci un valore: ");
		scanf("%d", &num);
		
		if(num!=0){
			for(i=1;i<=num;i++){
				if(num%i==0){
					cont++;
				}	
			}
				if(cont<3)
					primo++;
			}
				cont=0;
	}while(num!=0);

	printf("Valori primi nella sequenza: %d \n", primo);	
}
