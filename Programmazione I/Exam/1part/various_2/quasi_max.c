/*
Si completi il seguente programma scrivendo la funzione quasi_max, che riceve un array di
interi e la sua lunghezza e ritorna il quasi massimo dell’array, cioè il più grande numero
contenuto nell’array che non sia il massimo. Per esempio, sotto dovrà venire stampato 23, che
è il quasi massimo di arr. Nota: per semplicità si assuma che il quasi massimo dell’array esista,
quindi non ci si preoccupi di gestire i casi in cui esso non esiste, per esempio quando l’array è
vuoto o ha un solo elemento. Suggerimento: definite funzioni ausiliarie, se vi risultano di
aiuto.
*/


#include <stdio.h>
#define N

int quasimax(int arr[N]){

	int i;
	int max=0;
	int quasimax=0;	

	for(i=0;i<10;i++){
		if(arr[i]>max)
			max=arr[i];
		if(quasimax<arr[i]<max)
			quasimax=arr[i];
	}
	return(quasimax);
}


int main(void) {
	
	int ris;	

	int arr[N] = { 28, 10, 7, 9, 14, 22, 23, 28, -4, 23 };

	ris= quasimax(arr);

	printf("Quasi massimo: %d\n", ris);

	return 0;
}
