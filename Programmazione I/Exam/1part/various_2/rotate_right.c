/*

Si scriva una funzione rotateright che riceve un array di interi e la sua lunghezza e modifica
l’array spostando i suoi elementi di una posizione a destra (cioè verso la fine dell’array);
l’elemento che esce da destra deve rientrare da sinistra. Scrivere anche il main se si vuole
testare il programma.


*/


#include <stdio.h>
#define N 5

void rotateright(int A[N], int lg){

	int i,j;
	int B[N];
	
	B[0]=A[4];
	for(i=0;i<N-1;i++){
		B[i+1]=A[i];
	}

	for(i=0;i<N;i++){
		printf("%d ", B[i]);
	}
	printf("\n");
}


int main(){
	
	int A[N];
	int lg=5;
	int i;

	for(i=0;i<N;i++){
		printf("Inserisci un valore: ");
		scanf("%d", &A[i]);
	}	

	for(i=0;i<N;i++){
		printf("%d ", A[i]);
	}	
	printf("\n");
	rotateright(A, lg);
}
