/*
Si scriva un programma C che permetta all’utente di inizializzare una matrice di interi di dimensione NxN (con N dato)
con numeri interi compresi tra 0 e 99 (estremi inclusi).
Dopo aver stampato la matrice a video, il programma deve chiedere all’utente di inserire il valore di un intero val e deve
poi contare e stampare a video le occorrenze di val di ogni riga della matrice. Infine, il programma deve sostituire con
uno 0 le occorrenze di val nelle sole righe della matrice con più di due occorrenze di val .
*/

#include <stdio.h>
#define N 4

void sostituisci(int M[N][N], int val){

	int i,j;
	
	for(i=0;i<N;i++){
		for(j=0;j<N;j++){
			if(M[i][j]==val)
				M[i][j]=0;
		}
	}
}

int stampa(int M[N][N]){

	int i,j;
	for(i=0;i<N;i++){
		for(j=0;j<N;j++){
			printf("%d ", M[i][j]);
		}
		printf("\n");
	}
}

void cerca(int M[N][N], int val){

	int i,j;
	int cont=0;

	for(i=0;i<N;i++){
		for(j=0;j<N;j++){
			if(M[i][j]==val)
				cont++;
		}
		printf("Riga %d:  %d occorrenze \n", i+1,cont);
		cont=0;
	}
}

int main(){

	int M[N][N];

	int i,j;
	int val;
	
	for(i=0;i<N;i++){
		for(j=0;j<N;j++){
			do{
				printf("Inserisci un valore intero: ");
				scanf("%d", &M[i][j]);
			}while(M[i][j]<0 || M[i][j]>99);		
		}
	}
	stampa(M);
	printf("Inserisci un valore val: ");
	scanf("%d", &val);

	cerca(M, val);
	sostituisci(M, val);
	printf("\n");
	stampa(M);
}
