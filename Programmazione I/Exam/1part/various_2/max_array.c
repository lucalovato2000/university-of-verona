/*
Si scriva una funzione max_array che riceve due array di interi original e result e la loro
lunghezza length (uguale per entrambi). La funzione deve modificare result in modo che ogni
suo elemento di indice i diventi il massimo degli elementi di original con indice compreso fra
0 e i inclusi. La funzione non deve modificare gli elementi di original. Se length è minore o
uguale a 0, la funzione non deve fare nulla. Per esempio, se original è l’array {-2, 6, 7, 5, 8, -3, 0,
-4, 0, -1} e quindi length è 10, alla fine della funzione result deve contenere {-2, 6, 7, 7, 8, 8, 8,
8, 8, 8}.

*/

#include <stdio.h>
#define N 7

void max_array(int A[N], int B[N]){

	int i;
	int max=A[0];

	for(i=0;i<N;i++){

		if(A[i]>max){
			max=A[i];
			B[i]=A[i];
		}else
			B[i]=max;
	}

	printf("Risultato: ");

	for(i=0;i<N;i++){
		printf("%d ", B[i]);
	}
	printf("\n");
}

int main(){

	int A[N];
	int B[N];

	int i;

	for(i=0;i<N;i++){
		printf("Inserisci un valore del primo array: ");
		scanf("%d", &A[i]);
	}

	for(i=0;i<N;i++){
		printf("Inserisci un valore del secondo array: ");
		scanf("%d", &B[i]);
	}

	printf("Primo array inserito: ");
	for(i=0;i<N;i++){
		printf("%d ", A[i]);
	}
	printf("\n");

	printf("Secondo array inserito: ");
	for(i=0;i<N;i++){
		printf("%d ", B[i]);
	}
	printf("\n");

	max_array(A,B);
}
