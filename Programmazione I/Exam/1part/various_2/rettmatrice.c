/*
Si scriva un programma C che permetta all’utente di inizializzare una matrice di interi di dimensione NxN (con N fissato).
Dopo aver stampato la matrice a video, il programma deve richiedere all’utente le coordinate x e y (comprese tra 0 e N-
1) di un elemento della matrice (rifiutando eventuali valori non validi come coordinate) e stampare la somma degli
elementi compresi nel rettangolo avente come angolo inferiore destro l’elemento identificato dalle coordinate x e y.
Ad esempio, se N=6 e l’utente inserisce la seguente matrice e come coordinate 2,3 (riga di indice 2 e colonna di indice
3):
*/


#include <stdio.h>
#define N 6

void stampa(int M[N][N]){
	
	int i,j;

	for(i=0;i<N;i++){
		for(j=0;j<N;j++){
			printf("%d ", M[i][j]);
		}
		printf("\n");
	}
}

void stampa2(int M[N][N], int cx, int cy){
	
	int i,j;

	for(i=0;i<=cx;i++){
		for(j=0;j<=cy;j++){
			printf("%d ", M[i][j]);
		}
		printf("\n");
	}
}


void somma(int M[N][N], int cx, int cy){

	int i,j;
	int somma=0;

	for(i=0;i<=cx;i++){
		for(j=0;j<=cy;j++){
			somma+=M[i][j];
		}
	}
	printf("La somma risulta: %d \n", somma);
}

int main(){

	int M[N][N];
	int cx,cy;
	int i,j;

	for(i=0;i<N;i++){
		for(j=0;j<N;j++){
			printf("Inserisci un valore: ");
			scanf("%d", &M[i][j]);
		}
	}
	
	stampa(M);
	printf("\n");
	
	do{
		printf("Inserisci la riga: ");
		scanf("%d", &cx);
	}while(cx<0 || cx>5);
		
	do{	
		printf("Inserisci la colonna: ");
		scanf("%d", &cy);
	}while(cx<0 || cx>5);

	stampa2(M, cx, cy);
	somma(M, cx, cy);

}
