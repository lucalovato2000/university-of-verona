/*
Si scriva una funzione divide che riceve tre parametri: un array original di int, un array
destination di double e la lunghezza dei due array (che si assume essere la stessa per
entrambi). La funzione deve modificare destination in modo che ogni suo elemento in
posizione i diventi l’elemento di original in posizione i diviso per i+1 (risultato con la virgola).
Per esempio, l’esecuzione del seguente programma:

dovrà stampare 5.00 2.00 1.00 0.50 0.20 0.00.
*/

#include <stdio.h>
#define N 6

void divide(int original[N], double destination[N]){

	int i;

	for(i=0;i<N;i++){
		destination[i]=((original[i])/(i+1));	
	}

	for(i=0;i<N;i++){
		printf("%.2f ", destination[i]);
	}
	printf("\n");
}


int main() {
	
	int i;
	int original[N] = {5,4,3,2,1,0};
	double destination[N];
	
	divide(original,destination);
}
