#include <stdio.h>
#include <stdlib.h>
#include <time.h>


void main(){
	srand(time(NULL));
	
	int volte, numero;
	int precedente;

	do{
		printf("Inserisici un numero di volte: ");
		scanf("%i", &volte);
	}while(volte < 0);

	int i;
	for(i=0; i<volte; i++){
		numero = (rand() % 10) + 1;
		if(i == 0){
			printf("%i\n",numero);
		}
		else{
			if(numero <= precedente){
				printf("%i <= %i\n", numero, precedente);
			}
			else{
				printf("%i > %i\n", numero, precedente);
			}
		}
		precedente = numero;
	}
}
