#include <stdio.h>

int MCD(int a, int b){
	int m;
	m = (a <= b) ? a : b;

	while(m > 1){
		if( (a % m == 0) && (b % m == 0) ){
			break;
		}
		m--;
	}

	return m;
}


void main(){
	int a,b;
	a = 30;
	b = 12;
	printf("MCD tra %i %i: %i\n", a, b, MCD(a,b));
}
