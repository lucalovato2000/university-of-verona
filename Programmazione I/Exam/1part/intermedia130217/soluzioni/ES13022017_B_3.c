#include <stdio.h>

#define DIM 3

/**
 * Ritorna 1 se la matrice contiene tutti gli interi 
* a partire da 0 fino al numero di celle matrice -1,
  * 0 in caso contrario
 */
int contains_sequence(int matrix[][DIM], int size) {    
    unsigned int i, row, col;

    for(i = 0; i <= size*size - 1; i++) {    
        int present = 0;
        
        for(row = 0; row < size; row++) {
            for(col = 0; col < size; col++) {
                if(matrix[row][col] == i) {
                    present = 1;
                }
            }
        }
        
        if(!present) {
            return 0;
        }
        
    }
    return 1;
}

/**
 * Stampa se una matrice è "Contiene una sequenza" o "Non contiene una sequenza"
 */
void print_contains_sequence(int matrix[][DIM], int size) {
    if(contains_sequence(matrix, size)) {
        printf("Contiene una sequenza\n");
    } else {
        printf("Non contiene una sequenza\n");
    }
}

/**
 * Entry point
 */
int main() {
    int matrix1[DIM][DIM] = {{4, 2, 1}, {8, 0, 6}, {5, 7, 3}};
    int matrix2[DIM][DIM] = {{1, 2, 8}, {5, 7, 4}, {2, 3, 0}};
    
    print_contains_sequence(matrix1, DIM);
    print_contains_sequence(matrix2, DIM);
    
    return 0;
}
