#include <stdio.h>

#define DIM 3

/**
 * Ritorna 1 se la matrice contiene tutti gli interi 
 * a partire da 1 fino al numero di celle della matrice,
 * 0 in caso contrario
 */
int is_complete(int matrix[][DIM], int size) {    
    unsigned int i, row, col;

    for(i = 1; i <= size*size; i++) {    
        int present = 0;
        
        for(row = 0; row < size; row++) {
            for(col = 0; col < size; col++) {
                if(matrix[row][col] == i) {
                    present = 1;
                }
            }
        }
        
        if(!present) {
            return 0;
        }
        
    }
    return 1;
}

/**
 * Stampa se una matrice è "Completa" o "Non completa"
 */
void print_is_complete(int matrix[][DIM], int size) {
    if(is_complete(matrix, size)) {
        printf("Completa\n");
    } else {
        printf("Non completa\n");
    }
}

/**
 * Entry point
 */
int main() {
    int matrix1[DIM][DIM] = {{4, 2, 1}, {8, 3, 6}, {5, 7, 9}};
    int matrix2[DIM][DIM] = {{1, 2, 3}, {4, 7, 4}, {7, 8, 9}};
    
    print_is_complete(matrix1, DIM);
    print_is_complete(matrix2, DIM);
    
    return 0;
}
