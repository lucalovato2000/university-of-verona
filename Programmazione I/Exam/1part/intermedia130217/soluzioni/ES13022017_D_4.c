#include <stdio.h>

#define SIZE 3

/*
 * Ritorna 1 se il numero di valori dispari contenuti nelle celle della matrice 
 * è maggiore del numero dei valori pari, ritorna 0 in caso contrario
 */
int is_odd(int matrix[][SIZE], int rows) {    
    unsigned int row, col;
    int count = 0;
    
    for(row = 0; row < rows; row++) {
        for(col = 0; col < SIZE; col++) {
            if (matrix[row][col] % 2 != 0) {
                count++;
            }
        }
    }
 
    return count > (SIZE*rows) / 2;
}

/*
 * Stampa la matrice specificata.
 */
void print_matrix(int matrix[][SIZE], int rows) {
    unsigned int row, col;

    for(row = 0; row < rows; row++) {
        for(col = 0; col < SIZE; col++) {
            printf("%i\t", matrix[row][col]);
        }
        printf("\n");
    }
}


/**
 * Entry point
 */
int main() {
    int matrix1[SIZE][SIZE] = {{7, 2, 7}, {5, 0, 3}, {4, 1, 9}};
    int matrix2[SIZE][SIZE] = {{1, 0, 2}, {4, 2, 3}, {6, 4, 8}};
    
    print_matrix(matrix1, SIZE);
    printf("Matrice dispari? %i\n***\n", is_odd(matrix1, SIZE));

    print_matrix(matrix2, SIZE);
    printf("Matrice dispari? %i\n***\n", is_odd(matrix2, SIZE));
    
    return 0;
}
