#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define MIN 10
#define MAX 100

void main(){
	srand(time(NULL));

	int volte, numero;
	int precedente;

	do{
		printf("Inserisici un numero di volte: ");
		scanf("%i", &volte);
	}while(volte < 0);

	int i;
	for(i=0; i<volte; i++){
		numero = (rand() % (MAX - MIN) ) + MIN;
		if(i == 0){
			printf("%i\n",numero);
		}
		else{
			if(numero < (precedente / 2)){
				printf("%i < %i\n", numero, precedente/2);
			}
			else{
				printf("%i >= %i\n", numero, precedente/2);
			}
		}
		precedente = numero;
	}
}
