#include <stdio.h>

float dpow(int a, int b){
	float r = a * b;
	int i;
	for( i = 0; i < a - 1; i++){
		r *= (float)(a * b);
	}
	if(a % 3 == 0)
		r = r / (float)(a + b);
	else
		r = r / (float)(a - b);
	return r;
}


void main(){
	int a,b;
	a = 2;
	b = 7;
	printf("dpow(%i,%i): %f\n", a, b, dpow(a,b));
}
