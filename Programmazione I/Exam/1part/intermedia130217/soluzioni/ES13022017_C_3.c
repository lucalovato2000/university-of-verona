#include <stdio.h>

#define SIZE 3

/*
 * Utilizzando la struttura di controllo ciclo, ritorna 1 se tutti gli elementi 
 * che non appartengono alla diagonale principale sono uguali a zero.
 * Ritorna 0 in caso contrario.
 */
int is_diagonal(int matrix[][SIZE], int rows) {    
    unsigned int row, col;
    
    for(row = 0; row < rows; row++) {
        for(col = 0; col < SIZE; col++) {
            if(row != col && matrix[row][col] != 0) {
                return 0;
            }
        }
    }
 
    return 1;
}

/*
 * Stampa la matrice specificata.
 */
void print_matrix(int matrix[][SIZE], int rows) {
    unsigned int row, col;

    for(row = 0; row < rows; row++) {
        for(col = 0; col < SIZE; col++) {
            printf("%i\t", matrix[row][col]);
        }
        printf("\n");
    }
}


/**
 * Entry point
 */
int main() {
    int matrix1[SIZE][SIZE] = {{1, 0, 0}, {0, 0, 0}, {0, 0, 8}};
    int matrix2[SIZE][SIZE] = {{0, 2, 0}, {4, 0, 6}, {0, 8, 9}};
    
    print_matrix(matrix1, SIZE);
    printf("Matrice diagonale? %i\n###\n", is_diagonal(matrix1, SIZE));

    print_matrix(matrix2, SIZE);
    printf("Matrice diagonale? %i\n###\n", is_diagonal(matrix2, SIZE));
    
    return 0;
}
