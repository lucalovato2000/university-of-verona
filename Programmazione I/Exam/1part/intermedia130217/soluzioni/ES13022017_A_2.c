#include <stdio.h>

void main(){
	int a,b;
	char scelta;
	
	do{
		printf("Inserisci il primo numero intero posivito: ");
		scanf("%i", &a);
	}while(a <= 0);

	do{
		printf("Inserisci il secondo numero intero posivito: ");
		scanf("%i", &b);
	}while(b <= 0);


	do{
		printf("Scegli una delle seguenti operazioni:\n");
		printf("[A] operazione_A tra %i e %i\n", a, b);
		printf("[B] operazione_B tra %i e %i\n", a, b);
		printf("[C] esci\n:");
		scanf(" %c", &scelta);
		
		switch(scelta){
			case 'A':
				printf("operazione_A(%i,%i)\n", a, b);
				break;
			case 'B':
				printf("operazione_B(%i,%i)\n", a, b);
				break;
			case 'C':
				break;
			default:
				printf("Selezione non valida!\n");
		}
	}while(scelta != 'C');

	printf("Arrivederci.\n");
}

