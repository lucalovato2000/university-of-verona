#include <stdio.h>

#define SIZE 3

/*
 * Utilizzando la struttura di controllo ciclo, ritorna 1 se tutti gli elementi 
 * che non appartengono alla identitye principale sono uguali a zero e quelli
 * appartenti alla identitye principale sono uguali a 1.
 * Ritorna 0 in caso contrario.
 */
int is_identity(int matrix[][SIZE], int rows) {    
    unsigned int row, col;
    
    for(row = 0; row < rows; row++) {
        for(col = 0; col < SIZE; col++) {
            if(row != col && matrix[row][col] != 0) {
                return 0;
            }
        }
    }
 
    return 1;
}

/*
 * Stampa la matrice specificata.
 */
void print_matrix(int matrix[][SIZE], int rows) {
    unsigned int row, col;

    for(row = 0; row < rows; row++) {
        for(col = 0; col < SIZE; col++) {
            printf("%i\t", matrix[row][col]);
        }
        printf("\n");
    }
}


/**
 * Entry point
 */
int main() {
    int matrix1[SIZE][SIZE] = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};
    int matrix2[SIZE][SIZE] = {{1, 0, 0}, {0, 2, 0}, {0, 0, 1}};
    
    print_matrix(matrix1, SIZE);
    printf("Matrice identita'? %i\n###\n", is_identity(matrix1, SIZE));

    print_matrix(matrix2, SIZE);
    printf("Matrice identita'? %i\n###\n", is_identity(matrix2, SIZE));
    
    return 0;
}
