#include <stdio.h>


int poww(int a, int b){
	float r = a;
	int i;
	for( i = 1; i < b - 1; i++){
		r *= a;
	}
	if(a % 2 == 0)
		r += a + b;
	else
		r += a - b;
	return r;
}


void main(){
	int a,b;
	a = 21;
	b = 6;
	printf("poww(%i,%i): %i\n", a, b, poww(a,b));
}
