#include <stdio.h>

void main(){
	int a,b;
	char scelta;
	
	printf("Inserisci il primo numero intero: ");
	scanf("%i", &a);

	printf("Inserisci il secondo numero intero: ");
	scanf("%i", &b);


	do{
		printf("Scegli una delle seguenti operazioni:\n");
		printf("[+] somma %i piu' %i\n", a, b);
		printf("[*] moltiplica %i per %i\n", a, b);
		printf("[-] sottrai %i da %i\n", a, b);
		printf("[/] dividi %i per %i\n", a, b);
		printf("[e] esci\n:");
		scanf(" %c", &scelta);
		
		switch(scelta){
			case '+':
				printf("%i\n", a + b);
				break;
			case '-':
				printf("%i\n", a - b);
				break;
			case '*':
				printf("%i\n", a * b);
				break;
			case '/':
				printf("%f\n", (float)a / (float)b);
				break;
			case 'e':
				break;
			default:
				printf("Selezione non valida!\n");
		}
	}while(scelta != 'e');

	printf("Arrivederci.\n");
}
