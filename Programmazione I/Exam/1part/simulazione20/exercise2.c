/*
Nel file ESA 160120 A 2.c viene dato un main in cui è già inizializzato un array di interi m.
Si completi la separa che sposta nella parte iniziale dell’array tutti i suoi elementi pari 
e nella parte finale tutti i suoi elementi dispari.
L’ordine tra gli elementi della stessa categoria deve essere mantenuto uguale a quello originale, 
esempio 1,2,7,6,5 viene ordinato in 2,6,1,7,5.
*/


#include <stdio.h>

#define SIZE 10

void separa(int m[SIZE]){
		
	int a[SIZE];
	int b[SIZE];
	int k=0,l=0;
	int i,j;
		
	for(i=0;i<SIZE;i++){
		
		if(m[i]%2==0){
			a[k]=m[i];
			k++;
		}else{
			b[l]=m[i];
			l++;
		}
	}

	for(i=0;i<6;i++){
		m[i]=a[i];
	}

	j=0;

	for(i=5;i<10;i++){
		m[i]=b[j];
		j++;
	}
}


int main(){
	
	int m[SIZE]={4,7,6,5,0,5,7,8,9,2};
	
	separa(m);

	int i;
	
	for(i = 0; i < SIZE; i++){
		printf("%i ",m[i]);
	}
	
	printf("\n");
	return 0;
}
