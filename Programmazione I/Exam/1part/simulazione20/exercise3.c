/*
Scrivere un sottoprogramma che riceve come parametri due matrici quadrate di dimensioni 5x5 di interi,
 m1 ed m2. 
Il sottoprogramma valuta per ciascuna riga di entrambe le matrici, 
quanti elementi di m1 sono maggiori ai corrispondenti in m2.
Il sottoprogramma restituisce l’indice di riga che ne massimizza il valore.
Nel caso ci siano più righe con pari valore massimo, restituire per semplicità soltanto il primo indice. Ad esempio, date in
ingresso


*/


#include <stdio.h>
#define N 5

int rigamaggiore(int M1[N][N], int M2[N][N]){

	int i,j;
	int flag=0;
	int cont=0;
	
	for(i=0;i<N;i++){
		for(j=0;j<N;j++){
			if(M1[i][j]>M2[i][j])
				cont++;
		}	
			if(cont>flag)	
				flag=i;
				cont=0;
	}
	return(flag);
}



int main(){
	
	int ris;
	int M1[N][N]={	{1,2,4,3,5},
				{1,6,7,2,4},
				{5,4,3,2,1},
				{2,2,3,3,1},
				{2,4,5,8,7}	};
	
	int M2[N][N]={	{1,2,4,3,2},
				{9,4,9,9,9},
				{1,6,3,5,3},
				{2,2,4,1,0},
				{1,3,5,1,6}	};

	ris=rigamaggiore(M1,M2);

	printf("Riga maggiore risulta: %d", ris);
}
