/*
Scrivere un programma che inizializzi un array di 10 elementi con numeri casuali
compresi tra due valori min e max inseriti dall’utente. 
Il valore max non può essere minore del valore min.
Il programma deve stampare l’array come da esempio: 
Inserisci un valore min: 1 
Inserisci un valore max: 0
Inserisci un valore max: 100 23
56 100 78 1 24 8 12 77 64
*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 10

int main(){

	int V[N];
	int i;
	
	int min,max;

	srand(time(NULL));

	printf("Inserisci il valore minimo: ");
	scanf("%d", &min);

	do{
		printf("Inserisci il valore massimo: ");
		scanf("%d", &max);
	}while(max<=min);


	for(i=0;i<N;i++){
		V[i]=rand()%(max-1-min)+min;
	}

	for(i=0;i<N;i++){
		printf("%d", V[i]);
	}
	printf("\n");
}
