/*

Si scriva una funzione in C, denominata cerca, che ricerchi la presenza di un elemento in un vettore
di interi.
La funzione riceve in ingresso tre parametri:
1. un vettore di interi v[] nel quale ricercare il valore;
2. un un valore intero N che indica quanti elementi contiene il vettore;
3. il valore intero x che deve essere ricercato.La funzione deve restituire un valore intero, ed in particolare:
• se il valore x è presente nel vettore, allora la funzione restituisce l’indice della posizione
alla quale si trova tale valore;
• se il valore x è presente più volte, si restituisca l’indice della prima occorrenza;
• se il valore x non è presente nel vettore, si restituisca -1.

*/

#include <stdio.h>
#define N 5

int cerca(int v[], int x){

	int i;
	int r=-1;
	int cont=0;
	
	for(i=0;i<N;i++){
		if(v[i]==x && cont==0){
			r=i;
			cont++;
		}
	}
	
	return(r);
}


int main(){

	int x,i;
	int v[N];
	int ris;

	printf("Inserisci il valore che vuoi cercare: ");
	scanf("%d", &x);

	for(i=0;i<N;i++){
		printf("Inserisci un valore a scelta: ");
		scanf("%d", &v[i]);	
	}
	
	ris= cerca(v,x);
	
	if(ris==-1)
		printf("L'output risulta: %d, nessuna corrispondenza \n", ris);
	else
		printf("Corrispondenza trovata, indice corrispondente: %d \n", ris);

}

