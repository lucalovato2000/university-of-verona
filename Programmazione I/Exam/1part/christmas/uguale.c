/*
Scrivere un programma in linguaggio C che riceve in ingresso una sequenza di N numeri interi. I numeri sono memorizzati in un vettore. Il valore N è inserito dall’utente, ma il vettore può
contenere al massimo 5 numeri. Terminato l’inserimento della sequenza di numeri, il programma deve verificare se gli elementi del vettore sono tutti uguali tra loro.
*/


#include <stdio.h>
#define N 5

int main(){

	int v[N];
	int i,j;
	int ris=1;
	
	for(i=0;i<N;i++){
		printf("Inserisci un valore a scelta: ");
		scanf("%d", &v[i]);
			
	}
	for(i=0;i<N;i++){
		if(v[0]!=v[i])
			ris=0;
	}
	
	if(ris==1)
		printf("Tutti gli elementi sono uguali fra di loro nell'array \n");
	else
		printf("Ci sono elementi differenti nell'array \n");
}
