/*

Generare un numero a caso e chiedere all'utente un numero fino a quando non e' uguale a quello generato casualmente. Dire ogni volta se il numero immesso e' > o < di quello iniziale.
Per generare un numero a caso invocare la funzione rand(). Ad esempio se r è una variabile intera r = rand(); assegna a r un valore casuale.

*/

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main(){

	int n;

	srand(time(NULL));	//genera un seme random
	int tr= rand()%10;	//genera numeri random da 0 a 10
	
	while(n!=tr){
		printf("Indovina il numero: ");
		scanf("%d", &n);
  		
		if(n>tr)
			printf("Il numero inserito è alto, ritenta \n");
		
		if(n<tr)
			printf("Il numero inserito è basso, ritenta \n");
		if(n==tr)
			printf("Bravo, Hai vinto! \n");
	}
	return 0;
}
