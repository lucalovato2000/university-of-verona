/*

Si scriva un programma in linguaggio C che legga da tastiera un numero intero A, lo riduca ad un
valore compreso tra 0 e 127 mediante sottrazione ripetuta di un adeguato numero di volte del valore
128 (non si utilizzi l’operatore modulo o and), lo interpreti come caratteri ASCII e lo stampi sul
video.

*/

#include <stdio.h>

void main(){

	int a,b;
	char ris;


	printf("Inserisci un valore a scelta: ");
	scanf("%d", &a);

	b=a%128;

	ris=(char)(a-(128*b));

	printf("Il carattere ASCII del valore inserito risulta: %c \n", ris);

}
