/*

Scrivere un programma in linguaggio C che legga da tastiera una sequenza di numeri positivi e ad ogni numero letto ne stampi la somma progressiva. 
Il programma termina quando si introduce un numero minore o uguale a zero.

*/

#include <stdio.h>


int main(){

	int n,sm;

	do{
		printf("Inserisci un valore a scelta:");
		scanf("%d", &n);

		sm+=n;
	}while(n>0);

	printf("La somma progressiva dei numeri è la seguente: %d", sm);
}
