/*

Trovare il valore quasi massimo fra due array

*/

#include <stdio.h>

int max(int arr[], int length){

	int max=arr[0],i;

	for(i=1;i<length;i++){
		if(arr[i]>max)
			max=arr[i];
	}
	return (max);
}

int quasimax(int arr[], int length){

	int i,m,t=arr[0];

	m=max(arr,length);
	
	for(i=1;i<length;i++){
		if(arr[i]>t && arr[i]<m)
			t=arr[i];
	}
	return (max);
}


int main(){

	int r;
	int a[]={23,10,24,25,75,45,23};

	r=quasimax(a,10);
	printf("%d", r);

}
