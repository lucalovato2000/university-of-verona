/*

Matrice di interi, chiamiamo k sequenza, una sequenza con tutti valori uguale a k consecutivi

*/


#include <stdio.h>

#define N 5
#define M 4

int main(){

	int m1[N][M];
	int i,j;
	int r,c,k;
	int lunghezza=0, flag=1;

	for(i=0;i<N;i++){
		for(j=0;j<M;j++){
			printf("Inserisci un valore a scelta: ");
			scanf("%d", &m1);	
		}
	}

	for(i=0;i<N && flag;i++){
		for(j=0;j<M && flag;j++){
			if(lunghezza>0){
				if(m1[i][j]==k)
					lunghezza++;
				else
					flag=0;
			}else{
				if(m1[i][j]==k){
					lunghezza=1;
					r=i;
					c=j;	
				}
			}
		}
	}

	printf("Lunghezza %d in posizione %d %d ", lunghezza,r,c);

	return 0;

}
