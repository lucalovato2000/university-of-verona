/* 

MATRICE 5X5, matrice routata di 90° in senso antiorario

*/


#include <stdio.h>

#define N 5

int main(){

	int m1[N][N];
	int m2[N][N];

	int i,j;
	
	//richiedo dati per matrice m1
	for(i=0;i<N;i++){
		for(j=0;j<N;j++){
			printf("Inserisci un valore: ");
			scanf("%d", &m1[i][j]);
		}
	}

	for(i=0;i<N;i++){
		for(j=0;j<N;j++){
			m2[N-1-j][i]=m1[i][j];
		}
	}
	
	for(i=0;i<N;i++){
		for(j=0;j<N;j++){
			printf("%2d", m2[i][j]);
		}
		printf("\n");
	}

	return 0;

}
