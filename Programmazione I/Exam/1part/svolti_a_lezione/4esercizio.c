/*

array di n interi, x, y compreso fra 0 e N
almeno y multipli di x consecutivi, se non esistono stampare messaggio

*/



#include <stdio.h>

#define N 6

int main(){

	int a[N];
	int x,y,j,i,mul=0,trovato=0,pos;

	for(i=0;i<N;i++){
		scanf("%d", &a[i]);
		scanf("%d", &x);
	}

	do{
		scanf("%d", &y);

	}while(y>N || y<=1);

	i=0;

	while(i<N-1 && trovato==0){
		pos=i;
		mul=0;
			while(a[i]%x==0 && a[i+1]%x==0){
				i++;
				mul++;	
			}
			if(mul!=0 && mul+1>y)
				trovato=1;
		i++;
	
	}if(trovato==0)
		printf("Non trovato");
	else
		printf("in posizione %d", pos);
	return 0;
}
