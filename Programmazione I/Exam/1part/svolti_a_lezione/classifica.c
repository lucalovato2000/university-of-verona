/*
Data una matrice NXM (con N e M costanti simboliche) di numeri interi che memorizza risultati ottenuti da N squadre di calcio in M giornate di campionato, si scriva un programma C in grado di:

Riempire la matrice da tastiera. In ogni cella della matrice dovrà comparire soltanto 0, 1 o 3 (si devono rifiutare i valori non appartenenti all’insieme {0,1,3}).

Infatti, ogni colonna della matrice rappresenta i punti acquisiti da una squadra di calcio nelle partite disputate nelle diverse giornate del campionato: 
3 punti per le partite vinte, 
1 punto per quelle pareggiate 
 0 punti per le sconfitte. 

I risultati della giornata k-esima sono contenuti nella riga di indice k-1.
Stampare la matrice a video.

Chiedere all’utente l’inserimento da tastiera di un numero di giornata g (rifiutando eventuali valori non ammissibili). 
Trovare l’indice della squadra capolista alla giornata g e il punteggio totalizzato da tale squadra fino a quella giornata.
 Si noti che il punteggio di una squadra totalizzato fino alla giornata g si ottiene sommando i punteggi della squadra in tutte le giornate fino alla g. 
E’ possibile utilizzare un array di supporto. Se alla giornata g esistono più squadre capolista, stampare l’indice di tutte le squadre capolista.

*/

#include <stdio.h>

#define N 4
#define M 10

int main(){

	int ris[N][M];
	int parz[N]={0};
	
	int i,j,g,itop=0;

	
	for(i=0;i<N;i++){
		for(j=0;j<M;j++){

			do{
				scanf("%d", &ris[i][j]);
			}while(ris[i][j]<0 || ris[i][j]>2);
			
		}
	}

	for(i=0;i<N;i++){
			for(j=0;j<M;j++){
				printf("%3d", ris[i][j]);
			}
		printf("\n");
	}

		

	do{
		scanf("%d", &g);
	}while(g<1 || g>M);
	
	for(i=0;i<N;i++){
			for(j=0;j<g;j++){
				parz[i]+=ris[i][j];
			}
		
	}

	itop=0;

	for(i=1;i<N;i++){
		if(parz[i]>parz[itop])
			itop=i;
	}

	for(i=0;i<N;i++){
		if(parz[i]==parz[itop])
			printf("%3d", i);
	}
	return 0;
}


	
