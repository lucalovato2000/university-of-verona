/*
Scrivere una funzione int sequenza(int a[], int dim) che prende in ingresso un array di interi di massimo 20 valori ORDINATI anche con ripetizioni 
(si assume che l’input sia corretto, non serve verificare) e la sua dimensione effettiva e restituisce il valore che si presenta il maggior numero di volte (o il primo di tali valori).
*/

#include <stdio.h>
#define N 20

void sequenza(int a[N], int dim){

	int i,j;
	int cont=1;
	int max=0,flag=0;
	
	for(i=0;i<dim;i++){
		if(a[i]==a[i+1]){
			cont++;
			
			if(cont>max){
				max=cont;
				flag=a[i];
			}	
		}else
			cont=1;
	}
	
	printf("Numero maggiormente frequente: %d\n", flag);
}

int main(){
	
	int a[N]={2,2,2,3,3,3,3,4,5,5,5,5,9};
	int dim=14;

	sequenza(a,dim);
}
