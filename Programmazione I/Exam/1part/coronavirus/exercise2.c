/*
Scrivere un programma che riceve una sequenza di interi a priori illimitata (e che termina con l’inserimento del valore 0). Per
ogni intero letto il sottoprogramma stampa tutti i valori primi strettamente inferiori del numero.
*/


#include <stdio.h>

void primi(int n){

	int i;
	
	for(i=1;i<n;i++){
		if(n%i==0)
			printf("Numero primo strettamente inferiore: %d \n\n", i);
	}
}

int main(){

	int n;

	do{
		printf("Inserisci un valore: ");
		scanf("%d", &n);
		primi(n);
	}while(n!=0);
}
