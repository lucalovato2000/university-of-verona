/*
Scrivere una funzione int confrontacifre(int num1, int num2) che prende in ingresso due interi e restituisce 1 se num1 ha più
cifre di num2, 0 se i due numeri hanno lo stesso numero di cifre, 2 se num2 ha più cifre di num1. Ad esempio, se num1 vale 123 e
num2 vale 5 la funzione restituirà 1. Scrivere anche il main per testare la funzione.
*/

#include <stdio.h>

void confrontacifre(int n1, int n2){

	int min=0;
	int cont1=0;
	int cont2=0;
	int cifradx;

	while(n1!=0){
		cifradx= n1%10;
			cont1++;
		n1=n1/10;
	}

	cifradx=0;
	while(n2!=0){
		cifradx= n2%10;
			cont2++;
		n2=n2/10;
	}

	if(cont1>cont2)
		printf("1\n");
	else if(cont2>cont1)
		printf("2\n");
	else
		printf("0\n");
}

int main(){

	int n1, n2;

	printf("Inserisci un numero: ");
	scanf("%d", &n1);

	printf("Inserisci un numero: ");
	scanf("%d", &n2);

	confrontacifre(n1,n2);
}
