/*
Si scriva un programma C opportunamente scomposto in funzioni e procedure che permetta all’utente di:
● inizializzare una matrice di interi di dimensione NxN (con N pari a 3) con numeri positivi (maggiori di zero).
● stampare la matrice a video.
● individuare il primo indice di riga della matrice i cui elementi danno per prodotto un valore minore di 50.
*/

#include <stdio.h>
#define N 3

void im(int M[N][N]){
	
	int i,j;

	for(i=0;i<N;i++){
		for(j=0;j<N;j++){
			do{
				printf("Inserisci un valore positivo: ");
				scanf("%d", &M[i][j]);
			}while(M[i][j]<0);
		}
	}
}

void vm(int M[N][N]){

	int i,j;

	for(i=0;i<N;i++){
		for(j=0;j<N;j++){
			printf("%d", M[i][j]);
		}
		printf("\n");
	}
}

int cercaind(int M[N][N]){

	int i,j;
	int tot=0;
	int max;

	for(i=0;i<N;i++){
		for(j=0;j<N;j++){
			tot+=M[i][j]*M[i][j+1];	
			if(tot<50){
				max=i;
				return(max);	
			}
		}tot=0;
	}
}

int main(){

	int M[N][N];
	int ris;

	im(M);
	vm(M);
	ris= cercaind(M);
	printf("Primo indice di riga nel quale il prodotto risulta minore di 50 risulta: %d \n", ris);	
}
