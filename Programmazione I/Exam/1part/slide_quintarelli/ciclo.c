/*
Si realizzi un programma in linguaggio C che legga un numero intero N e visualizzi un quadrato di * (fino alla diagonale principale inclusa) e + (sopra la diagonale principale) come segue
(esempio con N = 5):
*++++
**+++
***++
****+
*****
*
*/

#include <stdio.h>


int main(){

	int num;
	int i,j;
	
	printf("Inserisci un numero: ");
	scanf("%d", &num);

	for(i=0;i<num;i++){
		for(j=0;j<num;j++){
			if(j<=i)
				printf("*");
			else
				printf("+");
		}
		printf("\n");
	}
}
