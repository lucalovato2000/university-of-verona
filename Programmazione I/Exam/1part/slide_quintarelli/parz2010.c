/*

Scrivere un programma C che:
○ Richiede all’utente di inserire 20 caratteri e li salva in un array.

○ Produce due risultati:
● Un array costruito con i caratteri inseriti in cui ogni
vocale (maiuscola o minuscola) è sostituita da una
cifra corrispondente (A=1, E=2, I=3, O=4, U=5).
● Un numero contenente la somma delle cifre (caratteri da 0 a 9) contenute nella stringa prodotta.

*/

#include <stdio.h>
#define N 10


void sostituisci(char A[N]){

	int i;

	for(i=0;i<N;i++){
		if(A[i]=='A' || A[i]=='a')
			A[i]='1';
		else if(A[i]=='E' || A[i]=='e')
			A[i]='2';
		else if(A[i]=='I' || A[i]=='i')
			A[i]='3';
		else if(A[i]=='O' || A[i]=='o')
			A[i]='4';
		else if(A[i]=='U' || A[i]=='u')
			A[i]='5';	
	}
	printf("Array risultante: ");
	for(i=0;i<N;i++){
		printf("%c", A[i]);
	}
}

int numero(char A[N]){

	int i;
	int sum=0;
	for(i=0;i<N;i++){
		if(A[i]=='1'||A[i]=='2'||A[i]=='3'||A[i]=='4'||A[i]=='5')
			sum+=A[i];
	}
	return(sum);
}

int main(){

	char A[N];
	int ris;

	int i;

	for(i=0;i<N;i++){
		printf("Inserisci un carattere: ");
		scanf(" %c", &A[i]);
	}

	sostituisci(A);
	ris=numero(A);
	printf("\nNumero risultante: %d \n", ris);
}
