/*
Si scriva un programma C che definisce la funzione int cifra_minima(int num) la quale deve restituire la cifra più bassa nella rappresentazione decimale di num.
Tale programma dovrà inoltre definire un main che
(1) chiede all’utente di inserire un numero non negativo,
(2) chiama la funzione cifra_minima per calcolarne la cifra più bassa
(3) stampa tale cifra minima trovata.
*/

#include <stdio.h>

void ciframinima(int num){

	int i;
	int min=100;
	
	int cifradx;
	
	while(num!=0){
	
		cifradx= num%10;
		if(cifradx<min)
			min=cifradx;
		num=num/10;

	}
	printf("%d\n", min);
}

int main(){

	int num;

	do{
		printf("Inserisci un numero positivo: ");
		scanf("%d", &num);
	}while(num<=0);

	ciframinima(num);
}
