/*

Un numero è detto “troncabile primo destro” se il numero stesso e tutti i numeri ottenuti rimuovendo suc-
cessivamente la cifra più a destra sono primi. Scrivere nel file ESA 11022019 A 2.c il sottoprogramma int
rightprime(int val) che ricevuto in ingresso un intero val sicuramente positivo restituisce 1 se il numero è
troncabile primo destro, 0 altrimenti. Ad esempio, se al sottoprogramma viene passato in ingresso il numero
317 esso restituirà 1, infatti 317 è primo, 31 è primo e 3 è primo.

*/

#include <stdio.h>

int prime(int num){
	
	int i;

	int cont=0;	
	int flag1=0;

		for(i=1;i<=num;i++){
			if(num%i==0){
				cont++;
			}
		}

		if(cont<3)
			flag1=1;		
	
	return(flag1);
}
	
int rightprime(int num){
		
	int a,b,c;
	int ris1;

	a=prime(num);
	b=prime((int)num/10);
	c=prime((int)num/100);

	if(a && b && c == 1 )
		ris1=1;
	
	return(ris1);
}

int main(){

	int num;
	int ris;

	do{
		printf("Inserisci un numero: ");
		scanf("%d", &num);
	}while(num<=0);

	ris=rightprime(num);
	
	if(ris==1)
		printf("Il numero risulta troncabile destro \n");
	else
		printf("Il numero non risulta troncabile destro \n");
}
