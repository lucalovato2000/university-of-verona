#include <stdio.h>
#include <stdlib.h>

#define N 2
#define M 3

int main(void) {


	int i,j;

	int Matrix[N][M];
	float Filter[M];
	float Result[N]={0};
	
	srand(1);



	//Inizializzazione Matrix
	for(i=0;i<N;i++){
		for(j=0;j<M;j++){
			if(i%2==0)
				Matrix[i][j]=rand()%6*2;	//[0,5]*2 genera numero pari
			else
				Matrix[i][j]=rand()%6*2-1;	//genera numeri dispari | %5*2+1
		}
	}


	//Inserimento valori utente in Filter
	for(j=0;j<M;j++){
		scanf("%f", &Filter[j]);
  	 }

	//Stampe di Matrix e Filter
	for(i=0;i<N;i++){
		for(j=0;j<M;j++){
			printf("%3d", Matrix[i][j]);
		}
		printf("\n");
	}

	for(j=0;j<M;j++){
		printf("%.2f",Filter[j]);
	}
	
	//Stampa prodotto fra matrice
	for(i=0;i<N;i++){
		for(j=0;j<M;j++){
			Result[i]+= Matrix[i][j]*Filter[j];	
		}
	}
/*
stampa risultato
*/
    
  return 0;
}
