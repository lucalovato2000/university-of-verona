/*
++TEMA ESAME++
NUMERO TRONCABILE PRIMO DESTRO:
317 e' primo
31 3' primo
3 e' primo

*/
#include<stdio.h>
int rightprime(int);
int main()
{
	int n,r;
	do{
		scanf("%d", &n;
	}while(n<=0);
	
	r=rightprime(n);
	printf("Esito sul numero:%d \t: %d", n,r);
}

int prime(int n)
{
	//definisco la funzione e se necessario definisco altre funzioni
	int i;
	if(n==0 || n==1)
		return 0;
	for(i=2; i<n; i++)
		if(n%i==0)
			return 0;
	
	return 1;//numero primo, vero
}
int rightprime(int val)
{
	while(val>0)
		{
			if(prime(val))
				val=val/10;
			else
				return 0;
		}
	return 1;
}



