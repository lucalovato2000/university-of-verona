/*

Si completi il file ESA 11022019 A 1.c in modo che che il programma:
-) dichiari una matrice di nome Matrix, di dimensione NxM (dove N e M sono definiti a inizio programma
come N=2, M=3), di numeri interi;
-) dichiari un vettore di decimali di nome Filter di dimensione M;
-) inizializzi la matrice t.c. le righe pari (incluso riga 0) abbiano numeri random pari e le righe dispari abbiano
numeri random dispari, sia pari che dispari compresi tra 0 e 10 (si ricorda che 2*x è sempre un numero pari e
che 2*x+1 è sempre un numero dispari);
-) chieda all’utente l’inserimento dei M valori per Filter;
-) stampi Matrix e Filter.

-) implementi il prodotto matrice x vettore e stampi il risultato. Si ricorda che il risultato è dato da un vettore
Result[] di dimensione N, ove ogni elemento c i è calcolato come:
c i
M
−1
P
m i,j ∗ f j = m i,1 ∗ f 1 + m i,2 ∗ f 2 + ... + m i,M −1 ∗ f M −1
j=0
dove m i,j è l’elemento di Matrix riga i, colonna j mentre f j è il j-esimo elemento del vettore Filter.

*/


#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 2
#define M 3

int main(){
	
	int Matrix[N][M];
	float Filter[M];
	float Result[N]={0};

	int i,j;

	srand(time(NULL));	

	//Inizializza Matrix
	for(i=0;i<N;i++){
		for(j=0;j<M;j++){
				if(i%2==0)
					Matrix[i][j]=2*(rand()%5);	//random pari 	N.B le parentesi!!
				else
					Matrix[i][j]=1+2*(rand()%5);	//random dispari
		}
	}

	//Inizializzazione dell'array Filter
	for(i=0;i<M;i++){
		printf("Inserisci un valore a scelta: ");
		scanf("%f", &Filter[i]);
	}
	
	printf("Matrix: \n");
	
	//stampa matrice Matrix
	for(i=0;i<N;i++){
		for(j=0;j<M;j++){
			printf("%3d", Matrix[i][j]);
		}
		printf("\n");
	}

	printf("\n");
	printf("Filter: \n");
	
	//Stampa array Filter
	for(i=0;i<M;i++){
		printf("%3.1f ", Filter[i]);		//3 posti di memoria, 1 dopo la virgola
	}
	printf("\n\n");


	//Prodotto fra Matrix e Filter
	for(i=0;i<N;i++){
		for(j=0;j<M;j++){
			Result[i]+= Matrix[i][j]*Filter[j];
		}
	}
	
	printf("Risultato del prodotto: ");
	//Stampa prodotto
	for(i=0;i<N;i++){
		for(j=0;j<M;j++){
			printf("%5.1f", Result[i]);
		}
	}
	printf("\n");
}
