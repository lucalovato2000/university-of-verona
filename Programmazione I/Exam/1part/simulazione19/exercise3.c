/*
I punti del piano cartesiano possono essere definiti utilizzando due vettori monodimensionali di uguale dimen-
sione, il primo per memorizzare i valori delle ascisse e il secondo per i corrispondenti valori delle ordinate. Si
completi il file ESA 11022019 A 3.c in modo tale che il seguente programma (già presente nello file stub):

produca il seguente risultato:
(0.00, 0.50)
(-1.10, 0.00)
(2.00, 1.00)
(0.90, 0.00)
Numero di punti dentro il cerchio di raggio 1.00 centrato nell’origine: 2
*/

#include <stdio.h>
#define N 4


int main(){

	double xs[N] = {0, -1.1, 2, 0.9}; // Ascisse
	double ys[N] = {0.5, 0, 1, 0}; // Ordinate

	int size = 4;
	double radius = 1;

	printf(xs, ys, size);

	printf("Numero di punti dentro il cerchio di raggio %.2lf centrato nell’origine: %i\n",
	radius, count_in(xs, ys, size, radius));

	return 0;
}

