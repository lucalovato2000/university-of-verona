// Generated from C:/Luca/UniVR/UniVR_gitlab/Linguaggi/Lezione06/src\Imp.g4 by ANTLR 4.9.2
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ImpLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.9.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		NAT=1, BOOL=2, STRING=3, PLUS=4, MINUS=5, MUL=6, DIV=7, MOD=8, POW=9, 
		DOT=10, AND=11, OR=12, EQQ=13, NEQ=14, LEQ=15, GEQ=16, LT=17, GT=18, NOT=19, 
		IF=20, THEN=21, ELSE=22, WHILE=23, SKIPP=24, ASSIGN=25, OUT=26, TOSTR=27, 
		LPAR=28, RPAR=29, LBRACE=30, RBRACE=31, ALBRACE=32, ARBRACE=33, SEMICOLON=34, 
		ID=35, WS=36;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"STR", "QUIT", "NAT", "BOOL", "STRING", "PLUS", "MINUS", "MUL", "DIV", 
			"MOD", "POW", "DOT", "AND", "OR", "EQQ", "NEQ", "LEQ", "GEQ", "LT", "GT", 
			"NOT", "IF", "THEN", "ELSE", "WHILE", "SKIPP", "ASSIGN", "OUT", "TOSTR", 
			"LPAR", "RPAR", "LBRACE", "RBRACE", "ALBRACE", "ARBRACE", "SEMICOLON", 
			"ID", "WS"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, null, null, null, "'+'", "'-'", "'*'", "'/'", "'mod'", "'^'", "'.'", 
			"'&'", "'|'", "'=='", "'!='", "'<='", "'>='", "'<'", "'>'", "'!'", "'if'", 
			"'then'", "'else'", "'while'", "'skip'", "'='", "'out'", "'tostr'", "'('", 
			"')'", "'{'", "'}'", "'['", "']'", "';'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "NAT", "BOOL", "STRING", "PLUS", "MINUS", "MUL", "DIV", "MOD", 
			"POW", "DOT", "AND", "OR", "EQQ", "NEQ", "LEQ", "GEQ", "LT", "GT", "NOT", 
			"IF", "THEN", "ELSE", "WHILE", "SKIPP", "ASSIGN", "OUT", "TOSTR", "LPAR", 
			"RPAR", "LBRACE", "RBRACE", "ALBRACE", "ARBRACE", "SEMICOLON", "ID", 
			"WS"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public ImpLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Imp.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2&\u00da\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\3\2\3\2\5\2R\n\2\3\3\6\3U"+
		"\n\3\r\3\16\3V\3\4\3\4\3\4\7\4\\\n\4\f\4\16\4_\13\4\5\4a\n\4\3\5\3\5\3"+
		"\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5l\n\5\3\6\3\6\7\6p\n\6\f\6\16\6s\13\6\3"+
		"\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13\3\13\3\13\3\f\3\f\3\r"+
		"\3\r\3\16\3\16\3\17\3\17\3\20\3\20\3\20\3\21\3\21\3\21\3\22\3\22\3\22"+
		"\3\23\3\23\3\23\3\24\3\24\3\25\3\25\3\26\3\26\3\27\3\27\3\27\3\30\3\30"+
		"\3\30\3\30\3\30\3\31\3\31\3\31\3\31\3\31\3\32\3\32\3\32\3\32\3\32\3\32"+
		"\3\33\3\33\3\33\3\33\3\33\3\34\3\34\3\35\3\35\3\35\3\35\3\36\3\36\3\36"+
		"\3\36\3\36\3\36\3\37\3\37\3 \3 \3!\3!\3\"\3\"\3#\3#\3$\3$\3%\3%\3&\6&"+
		"\u00d0\n&\r&\16&\u00d1\3\'\6\'\u00d5\n\'\r\'\16\'\u00d6\3\'\3\'\2\2(\3"+
		"\2\5\2\7\3\t\4\13\5\r\6\17\7\21\b\23\t\25\n\27\13\31\f\33\r\35\16\37\17"+
		"!\20#\21%\22\'\23)\24+\25-\26/\27\61\30\63\31\65\32\67\339\34;\35=\36"+
		"?\37A C!E\"G#I$K%M&\3\2\7\6\2\f\f\17\17$$^^\5\2\13\f\17\17\"\"\3\2\63"+
		";\3\2\62;\3\2c|\2\u00df\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2"+
		"\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2"+
		"\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2"+
		"\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2"+
		"\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2"+
		"\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2"+
		"\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\3Q\3\2\2\2\5T\3\2\2\2\7`\3\2\2\2\tk"+
		"\3\2\2\2\13m\3\2\2\2\rv\3\2\2\2\17x\3\2\2\2\21z\3\2\2\2\23|\3\2\2\2\25"+
		"~\3\2\2\2\27\u0082\3\2\2\2\31\u0084\3\2\2\2\33\u0086\3\2\2\2\35\u0088"+
		"\3\2\2\2\37\u008a\3\2\2\2!\u008d\3\2\2\2#\u0090\3\2\2\2%\u0093\3\2\2\2"+
		"\'\u0096\3\2\2\2)\u0098\3\2\2\2+\u009a\3\2\2\2-\u009c\3\2\2\2/\u009f\3"+
		"\2\2\2\61\u00a4\3\2\2\2\63\u00a9\3\2\2\2\65\u00af\3\2\2\2\67\u00b4\3\2"+
		"\2\29\u00b6\3\2\2\2;\u00ba\3\2\2\2=\u00c0\3\2\2\2?\u00c2\3\2\2\2A\u00c4"+
		"\3\2\2\2C\u00c6\3\2\2\2E\u00c8\3\2\2\2G\u00ca\3\2\2\2I\u00cc\3\2\2\2K"+
		"\u00cf\3\2\2\2M\u00d4\3\2\2\2OR\n\2\2\2PR\5\5\3\2QO\3\2\2\2QP\3\2\2\2"+
		"R\4\3\2\2\2SU\t\3\2\2TS\3\2\2\2UV\3\2\2\2VT\3\2\2\2VW\3\2\2\2W\6\3\2\2"+
		"\2Xa\7\62\2\2Y]\t\4\2\2Z\\\t\5\2\2[Z\3\2\2\2\\_\3\2\2\2][\3\2\2\2]^\3"+
		"\2\2\2^a\3\2\2\2_]\3\2\2\2`X\3\2\2\2`Y\3\2\2\2a\b\3\2\2\2bc\7v\2\2cd\7"+
		"t\2\2de\7w\2\2el\7g\2\2fg\7h\2\2gh\7c\2\2hi\7n\2\2ij\7u\2\2jl\7g\2\2k"+
		"b\3\2\2\2kf\3\2\2\2l\n\3\2\2\2mq\7$\2\2np\5\3\2\2on\3\2\2\2ps\3\2\2\2"+
		"qo\3\2\2\2qr\3\2\2\2rt\3\2\2\2sq\3\2\2\2tu\7$\2\2u\f\3\2\2\2vw\7-\2\2"+
		"w\16\3\2\2\2xy\7/\2\2y\20\3\2\2\2z{\7,\2\2{\22\3\2\2\2|}\7\61\2\2}\24"+
		"\3\2\2\2~\177\7o\2\2\177\u0080\7q\2\2\u0080\u0081\7f\2\2\u0081\26\3\2"+
		"\2\2\u0082\u0083\7`\2\2\u0083\30\3\2\2\2\u0084\u0085\7\60\2\2\u0085\32"+
		"\3\2\2\2\u0086\u0087\7(\2\2\u0087\34\3\2\2\2\u0088\u0089\7~\2\2\u0089"+
		"\36\3\2\2\2\u008a\u008b\7?\2\2\u008b\u008c\7?\2\2\u008c \3\2\2\2\u008d"+
		"\u008e\7#\2\2\u008e\u008f\7?\2\2\u008f\"\3\2\2\2\u0090\u0091\7>\2\2\u0091"+
		"\u0092\7?\2\2\u0092$\3\2\2\2\u0093\u0094\7@\2\2\u0094\u0095\7?\2\2\u0095"+
		"&\3\2\2\2\u0096\u0097\7>\2\2\u0097(\3\2\2\2\u0098\u0099\7@\2\2\u0099*"+
		"\3\2\2\2\u009a\u009b\7#\2\2\u009b,\3\2\2\2\u009c\u009d\7k\2\2\u009d\u009e"+
		"\7h\2\2\u009e.\3\2\2\2\u009f\u00a0\7v\2\2\u00a0\u00a1\7j\2\2\u00a1\u00a2"+
		"\7g\2\2\u00a2\u00a3\7p\2\2\u00a3\60\3\2\2\2\u00a4\u00a5\7g\2\2\u00a5\u00a6"+
		"\7n\2\2\u00a6\u00a7\7u\2\2\u00a7\u00a8\7g\2\2\u00a8\62\3\2\2\2\u00a9\u00aa"+
		"\7y\2\2\u00aa\u00ab\7j\2\2\u00ab\u00ac\7k\2\2\u00ac\u00ad\7n\2\2\u00ad"+
		"\u00ae\7g\2\2\u00ae\64\3\2\2\2\u00af\u00b0\7u\2\2\u00b0\u00b1\7m\2\2\u00b1"+
		"\u00b2\7k\2\2\u00b2\u00b3\7r\2\2\u00b3\66\3\2\2\2\u00b4\u00b5\7?\2\2\u00b5"+
		"8\3\2\2\2\u00b6\u00b7\7q\2\2\u00b7\u00b8\7w\2\2\u00b8\u00b9\7v\2\2\u00b9"+
		":\3\2\2\2\u00ba\u00bb\7v\2\2\u00bb\u00bc\7q\2\2\u00bc\u00bd\7u\2\2\u00bd"+
		"\u00be\7v\2\2\u00be\u00bf\7t\2\2\u00bf<\3\2\2\2\u00c0\u00c1\7*\2\2\u00c1"+
		">\3\2\2\2\u00c2\u00c3\7+\2\2\u00c3@\3\2\2\2\u00c4\u00c5\7}\2\2\u00c5B"+
		"\3\2\2\2\u00c6\u00c7\7\177\2\2\u00c7D\3\2\2\2\u00c8\u00c9\7]\2\2\u00c9"+
		"F\3\2\2\2\u00ca\u00cb\7_\2\2\u00cbH\3\2\2\2\u00cc\u00cd\7=\2\2\u00cdJ"+
		"\3\2\2\2\u00ce\u00d0\t\6\2\2\u00cf\u00ce\3\2\2\2\u00d0\u00d1\3\2\2\2\u00d1"+
		"\u00cf\3\2\2\2\u00d1\u00d2\3\2\2\2\u00d2L\3\2\2\2\u00d3\u00d5\t\3\2\2"+
		"\u00d4\u00d3\3\2\2\2\u00d5\u00d6\3\2\2\2\u00d6\u00d4\3\2\2\2\u00d6\u00d7"+
		"\3\2\2\2\u00d7\u00d8\3\2\2\2\u00d8\u00d9\b\'\2\2\u00d9N\3\2\2\2\13\2Q"+
		"V]`kq\u00d1\u00d6\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}