import value.*;

import java.util.ArrayList;

public class IntImp extends ImpBaseVisitor<Value> {

    private final Conf conf;

    public IntImp(Conf conf) {
        this.conf = conf;
    }

    private ComValue visitCom(ImpParser.ComContext ctx) {
        return (ComValue) visit(ctx);
    }

    @Override
    public ComValue visitProg(ImpParser.ProgContext ctx) {
        return visitCom(ctx.com());
    }

    @Override
    public ComValue visitIf(ImpParser.IfContext ctx) {
        if (visitBoolExp(ctx.exp())) {
            return visitCom(ctx.com(0));
        } else {
            return visitCom(ctx.com(1));
        }
    }

    @Override
    public ComValue visitAssign(ImpParser.AssignContext ctx) {
        String id = ctx.ID().getText();
        ExpValue<?> value = visitExp(ctx.exp());

        conf.update(id, value);

        return ComValue.INSTANCE;
    }

    @Override
    public ComValue visitSkip(ImpParser.SkipContext ctx) {
        return ComValue.INSTANCE;
    }

    @Override
    public ComValue visitSeq(ImpParser.SeqContext ctx) {
        visitCom(ctx.com(0));

        return visitCom(ctx.com(1));
    }

    @Override
    public ComValue visitWhile(ImpParser.WhileContext ctx) {
        if (!visitBoolExp(ctx.exp())) {
            return ComValue.INSTANCE;
        }
        visitCom(ctx.com());

        return visitWhile(ctx);
    }

    @Override
    public ComValue visitOut(ImpParser.OutContext ctx) {
        System.out.println(visitExp(ctx.exp()));

        return ComValue.INSTANCE;
    }

    @Override
    public ComValue visitArr(ImpParser.ArrContext ctx) {

        String id = ctx.ID().getText();

        if (conf.obtain(id) == null || !(conf.obtain(id) instanceof ArrayValue)) {
            conf.update(id, new ArrayValue(new ArrayList<>()));
        }
        ((ArrayValue) conf.obtain(id)).set(visitNatExp(ctx.exp(0)), visitExp(ctx.exp(1)));

        return ComValue.INSTANCE;
    }

    /* ------------------------------------------------------------------------------------------- EXP */

    private ExpValue<?> visitExp(ImpParser.ExpContext ctx) {
        return (ExpValue<?>) visit(ctx);
    }

    private Integer visitNatExp(ImpParser.ExpContext ctx) {
        try {
            return ((NatValue) visitExp(ctx)).toJavaValue();
        } catch (ClassCastException e) {
            System.err.println("Mismatch exception!");
            System.err.println("@" + ctx.start.getLine() + ":" + ctx.start.getCharPositionInLine());
            System.err.println("-------------------");
            System.err.println(ctx.getText());
            System.err.println("-------------------");
            System.err.println("> Natural expression expected");
            System.exit(-1);
        }

        return 0; // unreachable code
    }

    private boolean visitBoolExp(ImpParser.ExpContext ctx) {
        try {
            return ((BoolValue) visitExp(ctx)).toJavaValue();
        } catch (ClassCastException e) {
            System.err.println("Mismatch exception!");
            System.err.println("@" + ctx.start.getLine() + ":" + ctx.start.getCharPositionInLine());
            System.err.println("-------------------");
            System.err.println(ctx.getText());
            System.err.println("-------------------");
            System.err.println("> Boolean expression expected");
            System.exit(-1);
        }

        return false; // unreachable code
    }

    private String visitString(ImpParser.ExpContext ctx) {
        try {
            return ((StringValue) visit(ctx)).toJavaValue();
        } catch (ClassCastException e) {
            System.err.println("Mismatch exception!");
            System.err.println("-------------------");
            System.err.println(ctx.getText());
            System.err.println("@" + ctx.start.getLine() + ":" + ctx.start.getCharPositionInLine());
            System.err.println("-------------------");
            System.err.println("> String expression expected");

            System.exit(-1);
        }
        return null; // unreachable code
    }

    @Override
    public NatValue visitNat(ImpParser.NatContext ctx) {
        return new NatValue(Integer.parseInt(ctx.NAT().getText()));
    }

    @Override
    public BoolValue visitBool(ImpParser.BoolContext ctx) {
        return new BoolValue(Boolean.parseBoolean(ctx.BOOL().getText()));
    }

    @Override
    public ExpValue<?> visitParExp(ImpParser.ParExpContext ctx) {
        return visitExp(ctx.exp());
    }

    @Override
    public NatValue visitPow(ImpParser.PowContext ctx) {
        int base = visitNatExp(ctx.exp(0));
        int exp = visitNatExp(ctx.exp(1));

        return new NatValue((int) Math.pow(base, exp));
    }

    @Override
    public BoolValue visitNot(ImpParser.NotContext ctx) {
        return new BoolValue(!visitBoolExp(ctx.exp()));
    }

    @Override
    public NatValue visitDivMulMod(ImpParser.DivMulModContext ctx) {
        int left = visitNatExp(ctx.exp(0));
        int right = visitNatExp(ctx.exp(1));

        return switch (ctx.op.getType()) {
            case ImpParser.DIV -> new NatValue(left / right);
            case ImpParser.MUL -> new NatValue(left * right);
            case ImpParser.MOD -> new NatValue(left % right);
            default -> null;
        };
    }

    @Override
    public NatValue visitPlusMinus(ImpParser.PlusMinusContext ctx) {
        int left = visitNatExp(ctx.exp(0));
        int right = visitNatExp(ctx.exp(1));

        return switch (ctx.op.getType()) {
            case ImpParser.PLUS -> new NatValue(left + right);
            case ImpParser.MINUS -> new NatValue(Math.max(left - right, 0));
            default -> null;
        };
    }

    @Override
    public BoolValue visitEqExp(ImpParser.EqExpContext ctx) {
        ExpValue<?> left = visitExp(ctx.exp(0));
        ExpValue<?> right = visitExp(ctx.exp(1));

        return switch (ctx.op.getType()) {
            case ImpParser.EQQ -> new BoolValue(left.equals(right));
            case ImpParser.NEQ -> new BoolValue(!left.equals(right));
            default -> null;
        };
    }

    @Override
    public ExpValue<?> visitId(ImpParser.IdContext ctx) {
        String id = ctx.ID().getText();

        if (!conf.contains(id)) {
            System.err.println("Variable " + id + " used but never instantiated");
            System.err.println("@" + ctx.start.getLine() + ":" + ctx.start.getCharPositionInLine());

            System.exit(-1);
        }
        return conf.obtain(id);
    }

    @Override
    public BoolValue visitCmpExp(ImpParser.CmpExpContext ctx) {
        int left = visitNatExp(ctx.exp(0));
        int right = visitNatExp(ctx.exp(1));

        return switch (ctx.op.getType()) {
            case ImpParser.GEQ -> new BoolValue(left >= right);
            case ImpParser.LEQ -> new BoolValue(left <= right);
            case ImpParser.LT -> new BoolValue(left < right);
            case ImpParser.GT -> new BoolValue(left > right);
            default -> null;
        };
    }

    @Override
    public BoolValue visitLogicExp(ImpParser.LogicExpContext ctx) {
        boolean left = visitBoolExp(ctx.exp(0));
        boolean right = visitBoolExp(ctx.exp(1));

        return switch (ctx.op.getType()) {
            case ImpParser.AND -> new BoolValue(left && right);
            case ImpParser.OR -> new BoolValue(left || right);
            default -> null;
        };
    }

    @Override
    public Value visitConc(ImpParser.ConcContext ctx) {
        return new StringValue(visitString(ctx.exp(0)) + visitString(ctx.exp(1)));
    }

    @Override
    public Value visitAccess(ImpParser.AccessContext ctx) {

        String id = ctx.ID().getText();
        int pos = visitNatExp(ctx.exp());

        if (conf.obtain(id) == null || !(conf.obtain(id) instanceof ArrayValue)) {    // invalid ID
            System.out.println("Value ID is invalid!");
            System.exit(-1);
        }
        if (((ArrayValue) conf.obtain(id)).get(pos) == null) {     // invalid pos
            System.out.println("Value position is invalid!");
            System.exit(-1);
        }

        return ((ArrayValue) conf.obtain(id)).get(pos);
    }

    @Override
    public Value visitTostr(ImpParser.TostrContext ctx) {
        return new StringValue(visitExp(ctx.exp()).toString());
    }
}
