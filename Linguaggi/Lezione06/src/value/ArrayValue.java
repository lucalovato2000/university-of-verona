package value;

import java.util.ArrayList;
import java.util.List;

public class ArrayValue extends ExpValue<List<ExpValue<?>>> {

    List<ExpValue<?>> array = new ArrayList<>();

    public ArrayValue(List<ExpValue<?>> array) {
        super(array);
    }

    // set the value
    public void set(int pos, ExpValue<?> expValue) {
        array.set(pos, expValue);
    }

    // get the value
    public ExpValue<?> get(int pos) {
        return array.get(pos);
    }
}
