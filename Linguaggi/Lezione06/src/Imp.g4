grammar Imp;

prog : com EOF ;

com : IF LPAR exp RPAR THEN LBRACE com RBRACE ELSE LBRACE com RBRACE    # if            // if (exp) then {com} else {com)
    | ID ASSIGN exp                                                     # assign        // ID = exp
    | SKIPP                                                             # skip          // skip
    | com SEMICOLON com                                                 # seq           // com ; com
    | WHILE LPAR exp RPAR LBRACE com RBRACE                             # while         // while (exp) {com}
    | OUT LPAR exp RPAR                                                 # out           // out (exp)
    | ID ALBRACE exp ARBRACE ASSIGN exp                                 # arr           // ID[exp] = exp
    ;

exp : NAT                                 # nat
    | BOOL                                # bool
    | STRING                              # string
    | LPAR exp RPAR                       # parExp
    | <assoc=right> exp POW exp           # pow
    | NOT exp                             # not
    | exp op=(DIV | MUL | MOD) exp        # divMulMod
    | exp op=(PLUS | MINUS) exp           # plusMinus
    | exp op=(LT | LEQ | GEQ | GT) exp    # cmpExp
    | exp op=(EQQ | NEQ) exp              # eqExp
    | exp op=(AND | OR) exp               # logicExp
    | ID                                  # id              // id
    | exp DOT exp                         # conc            // exp . exp
    | ID ALBRACE exp ARBRACE              # access          // ID[exp]
    | TOSTR LPAR exp RPAR                 # tostr           // tostr(exp)
    ;

fragment STR     : ~["\\\r\n] | QUIT ;
fragment QUIT    : [ \t\r\n]+ ;

NAT     : '0' | [1-9][0-9]* ;
BOOL    : 'true' | 'false' ;
STRING  : '"' (STR)* '"' ;     // at least one

PLUS    : '+' ;
MINUS   : '-';
MUL     : '*' ;
DIV     : '/' ;
MOD     : 'mod' ;
POW     : '^' ;
DOT     : '.';

AND     : '&' ;
OR      : '|' ;

EQQ     : '==' ;
NEQ     : '!=' ;
LEQ     : '<=' ;
GEQ     : '>=' ;
LT      : '<' ;
GT      : '>' ;
NOT     : '!' ;

IF      : 'if' ;
THEN    : 'then' ;
ELSE    : 'else' ;
WHILE   : 'while' ;
SKIPP   : 'skip' ;
ASSIGN  : '=' ;
OUT     : 'out' ;
TOSTR   : 'tostr';

LPAR      : '(' ;
RPAR      : ')';
LBRACE    : '{' ;
RBRACE    : '}' ;
ALBRACE   : '[' ;
ARBRACE   : ']' ;
SEMICOLON : ';' ;

ID : [a-z]+ ;

WS : [ \t\r\n]+ -> skip ;