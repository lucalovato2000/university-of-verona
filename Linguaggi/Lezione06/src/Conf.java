import value.ExpValue;

import java.util.HashMap;
import java.util.Map;

public class Conf {

    private final Map<String, ExpValue<?>> map = new HashMap<>();

    // get the value
    public ExpValue<?> obtain(String id) {
        return map.get(id);
    }

    // put the value
    public void update(String id, ExpValue<?> value) {
        map.put(id, value);
    }

    // check if contains the value
    public boolean contains(String id) {
        return map.containsKey(id);
    }
}
