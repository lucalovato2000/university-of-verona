grammar IntExp;

main : prog EOF ;

prog : init exp ;

init : (ID EQUAL exp SEMICOLON)* ;          // (ID = exp;)  * at least zero instance, + at least one instance

exp : FLOAT                                 # float
    | ID                                    # id
    | exp op = (MUL | DIV | MOD) exp        # mulDivMod
    | exp op = (PLUS | MINUS) exp           # plusMinus
    | <assoc=right> exp POW exp             # pow
    | LPAR exp RPAR                         # lrPar
    ;

FLOAT : INT | (INT | '-' '0') '.' DIGIT + ;
fragment INT : NAT | '-' POS ;
fragment NAT : '0' | POS ;
fragment POS : POSDIGIT DIGIT * ;
fragment DIGIT : '0' | POSDIGIT ;
fragment POSDIGIT : [1-9] ;

LPAR      : '(' ;
RPAR      : ')' ;
PLUS      : '+' ;
MUL       : '*' ;
MINUS     : '-' ;
DIV       : '/' ;
MOD       : 'mod' ;
POW       : '^' ;
EQUAL     : '=' ;
SEMICOLON : ';' ;

ID    : [a-z]+;

WS    : [ \t\n\r]+ -> skip ;