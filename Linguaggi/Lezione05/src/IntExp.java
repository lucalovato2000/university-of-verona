import java.util.HashMap;
import java.util.Map;

public class IntExp extends IntExpBaseVisitor<Double> {

    private final Map<String, Double> memory = new HashMap<>();

    @Override
    public Double visitMain(IntExpParser.MainContext ctx) {
        return visit(ctx.prog());       // call prog
    }

    @Override
    public Double visitProg(IntExpParser.ProgContext ctx) {
        visit(ctx.init());
        return visit(ctx.exp());        // call exp
    }

    @Override
    public Double visitInit(IntExpParser.InitContext ctx) {
        int i = 0;

        while (i < ctx.exp().size()) {

            String id = ctx.ID(i).getText();
            Double value = visit(ctx.exp(i));

            memory.put(id, value);
            i++;
        }
        return null;
    }

    @Override
    public Double visitFloat(IntExpParser.FloatContext ctx) {
        return Double.parseDouble(ctx.FLOAT().getText());
    }

    @Override
    public Double visitId(IntExpParser.IdContext ctx) {
        return memory.getOrDefault(ctx.ID().getText(), 0.0);
    }

    @Override
    public Double visitPlusMinus(IntExpParser.PlusMinusContext ctx) {

        switch (ctx.op.getType()) {
            case IntExpParser.PLUS:
                return visit(ctx.exp(0)) + visit(ctx.exp(1));
            case IntExpParser.MINUS:
                return visit(ctx.exp(0)) - visit(ctx.exp(1));
        }
        return 0.0; // unreachable code statement
    }

    @Override
    public Double visitMulDivMod(IntExpParser.MulDivModContext ctx) {

        switch (ctx.op.getType()) {
            case IntExpParser.MUL:
                return visit(ctx.exp(0)) * visit(ctx.exp(1));
            case IntExpParser.DIV:
                return visit(ctx.exp(0)) / visit(ctx.exp(1));
            case IntExpParser.MOD:
                return visit(ctx.exp(0)) % visit(ctx.exp(1));
        }
        return 0.0; // unreachable code statement
    }

    @Override
    public Double visitPow(IntExpParser.PowContext ctx) {
        return Math.pow(visit(ctx.exp(0)), visit(ctx.exp(1)));
    }

    @Override
    public Double visitLrPar(IntExpParser.LrParContext ctx) {
        return visit(ctx.exp());
    }
}
