grammar IntExp;

main : prog EOF ;

prog : init exp ;

init : ID EQUAL exp SEMICOLON init    # idinit      // ID = exp;
     |                                # nilinit     // epsilon
     ;

exp : NUM                        # num
    | ID                         # id
    | LPAR exp PLUS exp RPAR     # plus
    | LPAR exp TIMES exp RPAR    # mul
    | LPAR exp MINUS exp RPAR    # sub
    | LPAR exp DIV exp RPAR      # div
    | LPAR exp MOD exp RPAR      # mod
    ;

LPAR      : '(' ;
RPAR      : ')' ;
PLUS      : '+' ;
TIMES     : '*' ;
MINUS     : '-' ;
DIV       : '/' ;
MOD       : 'mod' ;
EQUAL     : '=' ;
SEMICOLON : ';' ;

NUM   : NAT | NEG ;     // i numeri possono essere negativi oppure positivi
NAT   : '0' | [1-9][0-9]* ;
NEG   : '-' [1-9][0-9]* ;

ID    : [a-z]+;

WS    : [ \t\n\r]+ -> skip ;