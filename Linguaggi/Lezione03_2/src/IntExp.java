import java.util.HashMap;
import java.util.Map;

public class IntExp extends IntExpBaseVisitor<Integer> {

    private final Map<String, Integer> mem = new HashMap<>();

    @Override
    public Integer visitMain(IntExpParser.MainContext ctx) {
        return visit(ctx.prog());
    }

    @Override
    public Integer visitProg(IntExpParser.ProgContext ctx) {
        visit(ctx.init());
        return visit(ctx.exp());
    }

    @Override
    public Integer visitIdinit(IntExpParser.IdinitContext ctx) {
        String id = ctx.ID().getText();
        Integer val = visit(ctx.exp());

        mem.put(id, val);

        return visit(ctx.init());
    }

    @Override
    public Integer visitNilinit(IntExpParser.NilinitContext ctx) {
        return null;
    }

    @Override
    public Integer visitNum(IntExpParser.NumContext ctx) {
        return Integer.parseInt(ctx.NUM().getText());
    }

    @Override
    public Integer visitId(IntExpParser.IdContext ctx) {
        return mem.getOrDefault(ctx.ID().getText(), 0);
    }

    @Override
    public Integer visitPlus(IntExpParser.PlusContext ctx) {
        return visit(ctx.exp(0)) + visit(ctx.exp(1));
    }

    @Override
    public Integer visitMul(IntExpParser.MulContext ctx) {
        return visit(ctx.exp(0)) * visit(ctx.exp(1));
    }

    @Override
    public Integer visitSub(IntExpParser.SubContext ctx) {
        return visit(ctx.exp(0)) - visit(ctx.exp(1));
    }

    @Override
    public Integer visitDiv(IntExpParser.DivContext ctx) {
        return visit(ctx.exp(0)) / visit(ctx.exp(1));
    }

    @Override
    public Integer visitMod(IntExpParser.ModContext ctx) {
        return visit(ctx.exp(0)) % visit(ctx.exp(1));
    }
}
