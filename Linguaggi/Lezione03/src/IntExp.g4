grammar IntExp;

main : exp EOF ;

exp  : NAT                       # nat
     | LPAR exp MUL exp RPAR     # mul
     | LPAR exp PLUS exp RPAR    # plus
     | LPAR exp SUB exp RPAR     # sub
     | LPAR exp DIV exp RPAR     # div
     | LPAR exp MOD exp RPAR     # mod
     ;

LPAR : '(' ;
RPAR : ')' ;
PLUS : '+' ;
MUL  : '*' ;
SUB  : '-' ;
DIV  : '/' ;
MOD  : 'mod';

NAT  : '0' | [1-9][0-9]* | SUB [1-9][0-9]*;

WS   : [ \t\n\r]+ -> skip ;

