// Generated from C:/Luca/UniVR/UniVR_gitlab/Linguaggi/Lezione04/src\bFGrammar.g4 by ANTLR 4.9.2
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link bFGrammarParser}.
 */
public interface bFGrammarListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link bFGrammarParser#main}.
	 * @param ctx the parse tree
	 */
	void enterMain(bFGrammarParser.MainContext ctx);
	/**
	 * Exit a parse tree produced by {@link bFGrammarParser#main}.
	 * @param ctx the parse tree
	 */
	void exitMain(bFGrammarParser.MainContext ctx);
	/**
	 * Enter a parse tree produced by the {@code lt}
	 * labeled alternative in {@link bFGrammarParser#com}.
	 * @param ctx the parse tree
	 */
	void enterLt(bFGrammarParser.LtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code lt}
	 * labeled alternative in {@link bFGrammarParser#com}.
	 * @param ctx the parse tree
	 */
	void exitLt(bFGrammarParser.LtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code gt}
	 * labeled alternative in {@link bFGrammarParser#com}.
	 * @param ctx the parse tree
	 */
	void enterGt(bFGrammarParser.GtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code gt}
	 * labeled alternative in {@link bFGrammarParser#com}.
	 * @param ctx the parse tree
	 */
	void exitGt(bFGrammarParser.GtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code plus}
	 * labeled alternative in {@link bFGrammarParser#com}.
	 * @param ctx the parse tree
	 */
	void enterPlus(bFGrammarParser.PlusContext ctx);
	/**
	 * Exit a parse tree produced by the {@code plus}
	 * labeled alternative in {@link bFGrammarParser#com}.
	 * @param ctx the parse tree
	 */
	void exitPlus(bFGrammarParser.PlusContext ctx);
	/**
	 * Enter a parse tree produced by the {@code minus}
	 * labeled alternative in {@link bFGrammarParser#com}.
	 * @param ctx the parse tree
	 */
	void enterMinus(bFGrammarParser.MinusContext ctx);
	/**
	 * Exit a parse tree produced by the {@code minus}
	 * labeled alternative in {@link bFGrammarParser#com}.
	 * @param ctx the parse tree
	 */
	void exitMinus(bFGrammarParser.MinusContext ctx);
	/**
	 * Enter a parse tree produced by the {@code dot}
	 * labeled alternative in {@link bFGrammarParser#com}.
	 * @param ctx the parse tree
	 */
	void enterDot(bFGrammarParser.DotContext ctx);
	/**
	 * Exit a parse tree produced by the {@code dot}
	 * labeled alternative in {@link bFGrammarParser#com}.
	 * @param ctx the parse tree
	 */
	void exitDot(bFGrammarParser.DotContext ctx);
	/**
	 * Enter a parse tree produced by the {@code comma}
	 * labeled alternative in {@link bFGrammarParser#com}.
	 * @param ctx the parse tree
	 */
	void enterComma(bFGrammarParser.CommaContext ctx);
	/**
	 * Exit a parse tree produced by the {@code comma}
	 * labeled alternative in {@link bFGrammarParser#com}.
	 * @param ctx the parse tree
	 */
	void exitComma(bFGrammarParser.CommaContext ctx);
	/**
	 * Enter a parse tree produced by the {@code loop}
	 * labeled alternative in {@link bFGrammarParser#com}.
	 * @param ctx the parse tree
	 */
	void enterLoop(bFGrammarParser.LoopContext ctx);
	/**
	 * Exit a parse tree produced by the {@code loop}
	 * labeled alternative in {@link bFGrammarParser#com}.
	 * @param ctx the parse tree
	 */
	void exitLoop(bFGrammarParser.LoopContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nil}
	 * labeled alternative in {@link bFGrammarParser#com}.
	 * @param ctx the parse tree
	 */
	void enterNil(bFGrammarParser.NilContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nil}
	 * labeled alternative in {@link bFGrammarParser#com}.
	 * @param ctx the parse tree
	 */
	void exitNil(bFGrammarParser.NilContext ctx);
}