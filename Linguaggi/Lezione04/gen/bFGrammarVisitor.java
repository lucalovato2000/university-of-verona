// Generated from C:/Luca/UniVR/UniVR_gitlab/Linguaggi/Lezione04/src\bFGrammar.g4 by ANTLR 4.9.2
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link bFGrammarParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface bFGrammarVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link bFGrammarParser#main}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMain(bFGrammarParser.MainContext ctx);
	/**
	 * Visit a parse tree produced by the {@code lt}
	 * labeled alternative in {@link bFGrammarParser#com}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLt(bFGrammarParser.LtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code gt}
	 * labeled alternative in {@link bFGrammarParser#com}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGt(bFGrammarParser.GtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code plus}
	 * labeled alternative in {@link bFGrammarParser#com}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPlus(bFGrammarParser.PlusContext ctx);
	/**
	 * Visit a parse tree produced by the {@code minus}
	 * labeled alternative in {@link bFGrammarParser#com}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMinus(bFGrammarParser.MinusContext ctx);
	/**
	 * Visit a parse tree produced by the {@code dot}
	 * labeled alternative in {@link bFGrammarParser#com}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDot(bFGrammarParser.DotContext ctx);
	/**
	 * Visit a parse tree produced by the {@code comma}
	 * labeled alternative in {@link bFGrammarParser#com}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComma(bFGrammarParser.CommaContext ctx);
	/**
	 * Visit a parse tree produced by the {@code loop}
	 * labeled alternative in {@link bFGrammarParser#com}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLoop(bFGrammarParser.LoopContext ctx);
	/**
	 * Visit a parse tree produced by the {@code nil}
	 * labeled alternative in {@link bFGrammarParser#com}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNil(bFGrammarParser.NilContext ctx);
}