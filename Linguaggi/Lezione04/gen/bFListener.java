// Generated from C:/Luca/UniVR/UniVR_gitlab/Linguaggi/Lezione04/src\bF.g4 by ANTLR 4.9.2
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link bFParser}.
 */
public interface bFListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link bFParser#main}.
	 * @param ctx the parse tree
	 */
	void enterMain(bFParser.MainContext ctx);
	/**
	 * Exit a parse tree produced by {@link bFParser#main}.
	 * @param ctx the parse tree
	 */
	void exitMain(bFParser.MainContext ctx);
	/**
	 * Enter a parse tree produced by the {@code lt}
	 * labeled alternative in {@link bFParser#com}.
	 * @param ctx the parse tree
	 */
	void enterLt(bFParser.LtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code lt}
	 * labeled alternative in {@link bFParser#com}.
	 * @param ctx the parse tree
	 */
	void exitLt(bFParser.LtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code gt}
	 * labeled alternative in {@link bFParser#com}.
	 * @param ctx the parse tree
	 */
	void enterGt(bFParser.GtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code gt}
	 * labeled alternative in {@link bFParser#com}.
	 * @param ctx the parse tree
	 */
	void exitGt(bFParser.GtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code plus}
	 * labeled alternative in {@link bFParser#com}.
	 * @param ctx the parse tree
	 */
	void enterPlus(bFParser.PlusContext ctx);
	/**
	 * Exit a parse tree produced by the {@code plus}
	 * labeled alternative in {@link bFParser#com}.
	 * @param ctx the parse tree
	 */
	void exitPlus(bFParser.PlusContext ctx);
	/**
	 * Enter a parse tree produced by the {@code minus}
	 * labeled alternative in {@link bFParser#com}.
	 * @param ctx the parse tree
	 */
	void enterMinus(bFParser.MinusContext ctx);
	/**
	 * Exit a parse tree produced by the {@code minus}
	 * labeled alternative in {@link bFParser#com}.
	 * @param ctx the parse tree
	 */
	void exitMinus(bFParser.MinusContext ctx);
	/**
	 * Enter a parse tree produced by the {@code dot}
	 * labeled alternative in {@link bFParser#com}.
	 * @param ctx the parse tree
	 */
	void enterDot(bFParser.DotContext ctx);
	/**
	 * Exit a parse tree produced by the {@code dot}
	 * labeled alternative in {@link bFParser#com}.
	 * @param ctx the parse tree
	 */
	void exitDot(bFParser.DotContext ctx);
	/**
	 * Enter a parse tree produced by the {@code comma}
	 * labeled alternative in {@link bFParser#com}.
	 * @param ctx the parse tree
	 */
	void enterComma(bFParser.CommaContext ctx);
	/**
	 * Exit a parse tree produced by the {@code comma}
	 * labeled alternative in {@link bFParser#com}.
	 * @param ctx the parse tree
	 */
	void exitComma(bFParser.CommaContext ctx);
	/**
	 * Enter a parse tree produced by the {@code loop}
	 * labeled alternative in {@link bFParser#com}.
	 * @param ctx the parse tree
	 */
	void enterLoop(bFParser.LoopContext ctx);
	/**
	 * Exit a parse tree produced by the {@code loop}
	 * labeled alternative in {@link bFParser#com}.
	 * @param ctx the parse tree
	 */
	void exitLoop(bFParser.LoopContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nil}
	 * labeled alternative in {@link bFParser#com}.
	 * @param ctx the parse tree
	 */
	void enterNil(bFParser.NilContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nil}
	 * labeled alternative in {@link bFParser#com}.
	 * @param ctx the parse tree
	 */
	void exitNil(bFParser.NilContext ctx);
}