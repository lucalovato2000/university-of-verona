// Generated from C:/Luca/UniVR/UniVR_gitlab/Linguaggi/Lezione04/src\bF.g4 by ANTLR 4.9.2
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link bFParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface bFVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link bFParser#main}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMain(bFParser.MainContext ctx);
	/**
	 * Visit a parse tree produced by the {@code lt}
	 * labeled alternative in {@link bFParser#com}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLt(bFParser.LtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code gt}
	 * labeled alternative in {@link bFParser#com}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGt(bFParser.GtContext ctx);
	/**
	 * Visit a parse tree produced by the {@code plus}
	 * labeled alternative in {@link bFParser#com}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPlus(bFParser.PlusContext ctx);
	/**
	 * Visit a parse tree produced by the {@code minus}
	 * labeled alternative in {@link bFParser#com}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMinus(bFParser.MinusContext ctx);
	/**
	 * Visit a parse tree produced by the {@code dot}
	 * labeled alternative in {@link bFParser#com}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDot(bFParser.DotContext ctx);
	/**
	 * Visit a parse tree produced by the {@code comma}
	 * labeled alternative in {@link bFParser#com}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComma(bFParser.CommaContext ctx);
	/**
	 * Visit a parse tree produced by the {@code loop}
	 * labeled alternative in {@link bFParser#com}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLoop(bFParser.LoopContext ctx);
	/**
	 * Visit a parse tree produced by the {@code nil}
	 * labeled alternative in {@link bFParser#com}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNil(bFParser.NilContext ctx);
}