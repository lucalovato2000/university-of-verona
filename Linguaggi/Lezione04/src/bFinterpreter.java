import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

class bFInterpreter extends bFBaseVisitor<Integer> {

    private int pointer = 0;
    private final Map<Integer, Integer> map = new HashMap<>();
    private static final Scanner scanner = new Scanner(System.in);

    public bFInterpreter() {
    }

    @Override
    public Integer visitMain(bFParser.MainContext ctx) {
        return visit(ctx.com());
    }

    @Override
    public Integer visitLt(bFParser.LtContext ctx) {
        pointer--;

        return visit(ctx.com());
    }

    @Override
    public Integer visitGt(bFParser.GtContext ctx) {
        pointer++;

        return visit(ctx.com());
    }

    @Override
    public Integer visitPlus(bFParser.PlusContext ctx) {
        int value = map.getOrDefault(pointer, 0);
        map.put(pointer, value + 1);

        return visit(ctx.com());
    }

    @Override
    public Integer visitMinus(bFParser.MinusContext ctx) {
        int value = map.getOrDefault(pointer, 0);
        map.put(pointer, value - 1);

        return visit(ctx.com());
    }

    @Override
    public Integer visitLoop(bFParser.LoopContext ctx) {
        if (map.getOrDefault(pointer, 0) == 0) {
            return visit(ctx.com(1));
        }
        visit(ctx.com(0));

        return visit(ctx);
    }

    @Override
    public Integer visitDot(bFParser.DotContext ctx) {
        System.out.print(map.getOrDefault(pointer, 0) + ", ");

        return visit(ctx.com());
    }

    @Override
    public Integer visitComma(bFParser.CommaContext ctx) {
        int value;

        System.out.print("Insert a number: ");
        value = scanner.nextInt();
        map.put(pointer, value);

        return visit(ctx.com());
    }

    @Override
    public Integer visitNil(bFParser.NilContext ctx) {
        return pointer;
    }
}