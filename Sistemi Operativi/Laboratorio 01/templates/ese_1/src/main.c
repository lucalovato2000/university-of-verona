#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "errExit.h"

#define BUFFER_SZ 100   // max size buffer

char buffer[BUFFER_SZ + 1];

int main (int argc, char *argv[]) {

    // For each file provided as input argument
    for (int i = 1; i < argc; ++i) {
        // open the file in read only mode
        int file = open(argv[i], O_RDONLY);    // passare argv[i] per inserire file.txt e la flag O_RDONLY
        // check error of open system call
        if (file == -1) {
            printf("File %s does not exist\n", argv[i]);
            continue;
        }else
            printf("File exist.. continue \n");

        ssize_t bR = 0;
        do {
            // read the file in chunks of BUFFER_SZ
            bR = read(file, buffer, BUFFER_SZ);    
            // check error of read system call
            if (bR > 0) {
                printf("System call ok.. continue \n");
            
            // add the character '\0' to let printf know where a string ends
            buffer[bR] = '\0';

            //printf("%s", buffer);   // si puo fare una write
            write(STDOUT_FILENO, buffer, sizeof(BUFFER_SZ));    // quando buffer_SZ non è pieno si usa sizeof(BUFFER_SZ)
            }
        // until we read something from file
        } while (bR > 0);
        
        printf("\n");   // new line
        close(file);    // close the file descriptor
    }
    return 0;
}
