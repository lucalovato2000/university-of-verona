#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "errExit.h"

#define BUFFER_SZ 100   // max buffer size
char buffer[BUFFER_SZ + 1];     // new buffer

int main (int argc, char *argv[]) {

    // Check command line arguments
    if (argc != 3 && argc != 4) {
        printf("Usage: %s [option] <sourceFile> <destinationFile>\n", argv[0]);
        printf("option:\n");
        printf("-a: append\t<sourceFile> to <destinationFile>\n");
        printf("-s: overwrite\t<destinationFile> with <sourceFile>\n");
        return 1;
    }

    // flag variable, which is used to store either the option -a, or -s
    char *flag = NULL;
    // the pathnames to the source and destination file
    char *fileSource, *fileDestination;

    // if argc is 4, then my_cp <flag> <source> <destination>
    if (argc == 4) {
        flag = argv[1];
        fileSource = argv[2];
        fileDestination = argv[3];
    // if argc is 3, then my_cp <source> <destination>
    } else {
        fileSource = argv[1];
        fileDestination = argv[2];
    }

    // open the source file for only reading
    int fileS = open(fileSource, O_RDONLY);    // [SYSTEM CALL] open in only reading 
    if (fileS == -1)
        errExit("open fileSource failed");

    int fileD = -1;
    //if flag is NULL, then no flag was provided as input argument
    if (flag == NULL) {
        if (access(fileDestination, F_OK) == 0) {   // [SYSTEM CALL] check if the destination file already exists
            printf("Il file %s gia' esiste!\n", fileDestination);
            return 1;
        }
        // N.B. we must be sure we created the file (O_EXCL)
        // [SYSTEM CALL] create and open the destination file for only writing
        //                            crea il file, esegui, modalita' solo scrittura  e dando permessi di lettura scrittura utente
        fileD = open(fileDestination, O_CREAT | O_EXCL | O_WRONLY, S_IRUSR | S_IWUSR); 
    // if flag is "-s", then we have to overwrite the destination file
    } else if (strcmp(flag, "-s") == 0) {

        // [SYSTEM CALL] open new or existing file for only writing. If the file
        // destination already exists, truncate it to zero bytes
        //                            crea il file, se esite tronca
        fileD = open(fileDestination, O_CREAT | O_TRUNC | O_WRONLY, S_IRUSR | S_IWUSR);
    // if flag is "-a", then we have to append file source to the bottom of
    // file destination
    } else if (strcmp(flag, "-a") == 0) {
        // check if source and destination are different.
        if (strcmp(fileSource, fileDestination) == 0) {
            printf("<sourceFile> and <destinationFile> must be different\n");
            return 1;
        }
        if (access(fileDestination, F_OK | W_OK ) != 0) {   // [SYSTEM CALL] check if the destination file already exists
            printf("Il file %s non esiste, o non puo' essere scritto!\n",
                   fileDestination);
            return 1;
        }

        // open the destination file for only writing, and move current offse to the file's bottom
        fileD = open(fileDestination, O_WRONLY | O_APPEND); // APPEND apre il file e sposta il cursore alla fine

        /*  
         *  fileD = open(fileDestination, O_WRONLY);   // [SYSTEM CALL] open the destination file for only writing
         *  // [SYSTEM CALL] move the current offset to the bottom of destination file
         *  // SEEK_END c'e' zero perche'
         *  if (fileD != -1 && lseek(fileD, 0, SEEK_END) == -1)    
         *      errExit("lssek failed");
         */

    } else {
        printf("The flag %s is not recognized!\n", flag);
        return 1;
    }
    // if fileD is -1, then something went wrong with the open system call
    if (fileD == -1)
        errExit("open fileDestination failed");

    ssize_t bR;
    do {
        bR = read(fileS, buffer, sizeof(BUFFER_SZ));    // [SYSTEM CALL] read a chunk of bytes from file source
        // check if read completed successfully
        if (bR == -1)
            errExit("read failed");
        // [SYSTEM CALL] check if write completed successfully
        if (bR > 0 && write(fileD, buffer, bR) != bR)   // bR perche' numero di byte che leggo
                errExit("write failed");
    // until end-of-file is reached
    } while (bR > 0);

    // close file descriptors
    // close(fileS); close(fileD);

    if (close(fileS) == -1 || close(fileD) == -1)
        errExit("close failed");

    return 0;
}
