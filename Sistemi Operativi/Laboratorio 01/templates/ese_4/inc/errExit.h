#ifndef _ERREXIT_HH
#define _ERREXIT_HH

#include <errno.h>

void errExit(const char *msg);

#endif
