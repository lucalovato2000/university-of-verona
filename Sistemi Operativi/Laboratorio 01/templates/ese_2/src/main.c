#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "errExit.h"

#define BUFFER_SZ 100   // max buffer size
char buffer[BUFFER_SZ + 1]; // new buffer

int main (int argc, char *argv[]) {

    // For each file provided as input argument
    for (int i = 1; i < argc; ++i) {
        int file = open(argv[i], O_RDONLY);   // [SYSTEM CALL] open the file in read only mode
        if (file == -1) {   // check error of open system call
            printf("File %s does not exist\n", argv[i]);
            continue;
        }else
            printf("File exist.. continue\n");
        
        // [SYSTEM CALL] move the offset location to the end of the file  
        off_t currentOffset = lseek(file, -1, SEEK_END);

        char c;
        while (currentOffset >= 0) {
            ssize_t bR = read(file, &c, sizeof(char));  // [SYSTEM CALL] read a char from the file
            // write the read char on standard output
            // printf("c-> [%c]\n", c);
            if (bR == sizeof(char)) {   // se la dimensione è uguale a quella di un carattere
                // write on standard output the char
                if ( write(STDOUT_FILENO, &c, sizeof(char)) != sizeof(char))    // se la write non da come risultato sizeof(char) stampa errore
                    errExit("Write failed");
            }
            // move backwards the fie offset
            currentOffset = lseek(file, -2, SEEK_CUR);  // N.B -2 perche' leggo un char che ha dimensione 2 
        }
        close(file);  // [SYSTEM CALL] close the file descriptor

        c = '\n';
        write(STDOUT_FILENO, &c, sizeof(char));   // [SYSTEM CALL] print a new line before starting the next file
    }
    return 0;
}
