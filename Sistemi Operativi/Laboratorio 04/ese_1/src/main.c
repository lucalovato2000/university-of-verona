#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sem.h>

#include "semaphore.h"
#include "errExit.h"

char *messages[] = {"Operativi\n", "Sistemi ", " di ", "Corso"};

int main (int argc, char *argv[]) {

    // check command line input arguments
    if (argc != 2) {
        printf("Usage: %s <number>\n", argv[0]);
        return 0;
    }

    // get how many times the programma has to write on standard out the
    // string "Corso di Sistemi Operativi\n"
    int n = atoi(argv[1]);
    if (n < 0) {
        printf("The input number must be > 0!\n");
        return 1;
    }

    // Create a semaphore set with 4 semaphores
    int semid = semget(IPC_PRIVATE, 4, S_IRUSR | S_IWUSR);  // ipc_private genera una chiave privata per il set di semafori, set creato da 4 semafori
    if (semid == -1)
        errExit("semget failed");

    // Initialize the semaphore set
    unsigned short semInitVal[] = {0, 0, 0, 1};
    union semun arg;
    arg.array = semInitVal;

    if (semctl(semid, 0 /*ignored*/, SETALL, arg) == -1)    // semctl setta tutti i valori dei semafori in base al semInitVal[]
        errExit("semctl SETALL failed");

    // Generate 4 child processes:
    // child-0 prints message[0], ... child-3 prints message[3]
    for (int child = 0; child < 4; ++child) {
        pid_t pid = fork();
        // check error for fork
        if (pid == -1)
            printf("child %d not created!", child);
        // check if running process is child or parent
        else if (pid == 0) {
            // code executed only by the child
            for (int times = 0; times < n; times++) {   // n quante volte deve essere stampata la stringa di sistemi operativi
                // wait the i-th semaphore
                semOp(semid, (unsigned short)child, -1);    // -1 su valore zero si blocca, succede a tutti i figli tranne che al 4
                // print the message on terminal
                printf("%s", messages[child]);
                // flush the standard out
                fflush(stdout);
                // unlock the (i-1)-th semaphore.
                // child is equal to 0, then the fourth child is unlocked
                semOp(semid, (unsigned short) (child == 0)? 3 : child - 1, 1);  // aggiungo 1 per sbloccare figlio
            }

            exit(0);
        }
    }
    // code executed only by the parent process

    // wait the termination of all child processes
    while (wait(NULL) != -1);

    // ipcs per vedere su terminale quali processi sono attivi e chiuderli


    // remove the created semaphore set
    if (semctl(semid, 0, IPC_RMID, 0) == -1)
        errExit("semctl IPC_RMID failed");

    return 0;
}
