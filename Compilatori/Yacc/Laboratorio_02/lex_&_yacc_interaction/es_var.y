%{
#include <ctype.h>
#include <stdio.h>
#include <string.h>

  int yylex();
  int yyparse();
  void yyerror(char const*s);
  int memory=0;
  int regs[26];

  
%}
%error-verbose
%token INTEGER LETTER
%left '+'
%%

lines: lines line
|line
;

line: expr'\n'     {printf("=%d\n",$1);
   memory=$1;}
|LETTER '=' expr'\n' {regs[$1] =$3;}
;

expr: expr'+'expr    {$$=$1+$3;}
|INTEGER    {$$=$1;}
|LETTER   {$$=regs[$1];}



%%
void main(){
  yyparse();
 }
void yyerror(char const*s) {
  fprintf(stderr,"%s\n",s);

}
