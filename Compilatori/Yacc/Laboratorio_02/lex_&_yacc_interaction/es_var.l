%{
#include <stdio.h>
#include "y.tab.h"

int c;

%}
%%

[0-9]+    {yylval=atoi(yytext); return INTEGER;}


[a-z]   {c=yytext[0];
yylval=c-'a';
return LETTER;
}


[ \t\r]

\n   return '\n';

.   return yytext[0];

%%
