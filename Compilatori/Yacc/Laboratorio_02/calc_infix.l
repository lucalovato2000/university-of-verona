%{
#include <stdio.h>
#include "y.tab.h"
%}

%%

0|[1-9]+[0-9]*   {yylval=atoi(yytext); return INTEGER;}

[ \t\r]

\n     return '\n';
.      return yytext[0];
%%
