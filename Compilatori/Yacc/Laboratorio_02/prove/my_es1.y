%{ 
   
#include <ctype.h>  
#include <stdio.h> 
#include <string.h> 
  
int yyparse(); 
void yyerror(char const *s); 
int yylex();
int my_pow(int base, int exp); 
           
%} 
 
%token INTEGER
%left '+'
%left '-'
%left '*'
%left '/'
%left '^' 
 
%% 
 
lines:lines line 
  |line 
; 
 
line:expr'\n'               { printf("= %d\n", $1); } 
; 
   
expr:expr '+' E             { $$ = $1 + $3; }
| E                         { $$ = $1; }
;
E : E '-' F                 { $$ = $1 - $3; }
| F                         { $$ = $1; }
;
F : F '/' G                 { $$ = $1 * $3; }
| G                         { $$ = $1; }
;
G : G '*' H                 { $$ = $1 / $3; }
| H                         { $$ = $1; }
;
H: H '^' INTEGER            { $$= my_pow($1,$3); }
|INTEGER         { $$ = $1;}  
; 
 
%% 
 
void yyerror (char const *s) { 
   fprintf(stderr, "%s\n", s); 
 } 
 
int my_pow(int base, int exp){ 
        int result=1; 
        while (exp != 0) { 
          result *= base; 
          exp=exp-1; 
        } 
        return result; 
      } 

void main(){ 
  yyparse(); 
}