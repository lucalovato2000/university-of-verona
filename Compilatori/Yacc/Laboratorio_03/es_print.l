%{
#include <stdio.h>
#include "y.tab.h"
int c;
%}

%%
exit exit(0);
print {return PRINT;}
0|[1-9]+[0-9]*   {yylval.numero = atoi(yytext); return INT;}
[a-zA-Z_]+[a-zA-Z_0-9]* {yylval.stringa = strndup(yytext, yyleng); return STRING;}

[ \t\r]

\n     return '\n';
.      return yytext[0];
%%
