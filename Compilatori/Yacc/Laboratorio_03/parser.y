%{
#include <stdio.h>
#include <math.h>

void yyerror(const char *s);
int val = 0;
%}

%define parse.error verbose

%union {
    int integer;
    char* string;
}

%token <integer> NUM
%token <string> NAME
%token <string> PRINT

%type <integer> input line expr term factor

%%

input:
  input line
| line
;

line:
  expr '\n'                { printf("> %d\n", $1); val = $1; }
| PRINT NAME '\n'          { printf("number of %s: %d\n", $2, val); }
;

expr:
  expr '+' term             { $$ = $1 + $3; }
| expr '-' term             { $$ = $1 - $3; }
| term                      { $$ = $1; }
;

term:
  term '*' factor           { $$ = $1 * $3; }
| term '/' factor           { $$ = $1 / $3; }
| factor                    { $$ = $1; }
;

factor:
  '(' expr ')'            { $$ = $2; }
| NUM                     { $$ = $1; }
;


%%

int main() {
    yyparse();
}

void yyerror(const char *s) {
    fprintf(stderr, "%s\n", s);
}

