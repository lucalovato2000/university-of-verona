
%{
#include <stdlib.h>
#include <string.h>
#include "y.tab.h"

char *getpastr(char *l);

%}

spazi      [ \t\n\r]

%%

"UNA"         {yylval.str=getpastr(yytext);return ARTICOLO;}
"FUORI"       {yylval.str=getpastr(yytext);return AVVERBIO;}
"CINEMA"      {yylval.str=getpastr(yytext);return LUOGO;}
"BIRRA"       {yylval.str=getpastr(yytext);return NOME;}
"ANDARE"      {yylval.str=getpastr(yytext);return VERBO;}
"MANGIARE"    {yylval.str=getpastr(yytext);return VERBO;}
"BERE"        {yylval.str=getpastr(yytext);return VERBO;}
"AL"          {yylval.str=getpastr(yytext);return PREP;}
	

{spazi}+  	  ;

[;]	          {return SC;}

.             {printf("\nPAROLA NON RICONOSCIUTA: %s",yytext);}

%%

int yywrap()
{
   return(1);
}

