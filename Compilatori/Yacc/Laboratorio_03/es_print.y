%{
  #include <stdio.h>
  #include <math.h>

  void yyerror();
  int memory = 0;
  int yylex();

%}

%define parse.error verbose
%left '+'
%left '-'
%left '*'
%left '/'
%left '^'
%left '('

/*
union mi permette dato in input stabilire il tipo, 
posso ricevere un int o una string in questo caso
*/
%union{
    int numero;
    char *stringa;
}

%token <numero> INT
%token <stringa> STRING
%token <stringa> PRINT

%type <numero> expr line lines

%%

lines:lines line
|line
;

line:expr'\n'	 {printf("= %d\n", $1); memory = $1; }
| PRINT STRING '\n' {$$ = memory; printf("number of %s = %d\n", $2, memory);}
;

expr: expr '+' expr	{$$ = $1 + $3;}
| expr '-' expr		{$$ = $1 - $3;}
| expr '*' expr		{$$ = $1 * $3;}
| expr '/' expr		{$$ = $1 / $3;}
| expr '^' expr		{$$ = pow($1, $3);}
| INT			{$$=$1;}
;

%%

void yyerror(char const *s){
  fprintf(stderr,"%s\n",s);
}

void main(){
  yyparse();
 }



