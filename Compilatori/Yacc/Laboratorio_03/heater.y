
%{
#include <stdio.h>
#include <string.h>

  int yylex();
  int yyparse();

  void yyerror(const char *str){
    fprintf(stderr,"error %s\n",str);
  }
  int yywrap(){
    return 1;}
  int main(){
    yyparse();
  }

  char *heater="default";
  int prev_status=-1;//inizialmente non e' ne' on ne' off
  %}
%token TOKHEATER TOKHEAT TOKTARGET TOKTEMPERATURE

%union{
  int number;
  char *string;
 }
%token <number> STATE
%token <number> NUMBER
%token <string> WORD

%%
commands:

|commands comand
;

comand:
   heat_switch | target_set |heater_select
   ;
heat_switch:
TOKHEAT STATE
{if ($2==prev_status)
    printf("Already with status %s",$2?"on":"off");
  else if ($2) {  
    printf("\tHeather '%s' turned on\n", heater);
    prev_status=1;
  }
  else{
    printf("\tHeat '%s' turned off\n", heater);
    prev_status=0;
  }
};

target_set: TOKTARGET TOKTEMPERATURE NUMBER
{
  printf("\tHeater '%s' temperature set to %d\n", heater, $3);
};
heater_select: TOKHEATER WORD
{printf("\tSelected heater '%s'\n",$2);
  heater=$2;
};
    

