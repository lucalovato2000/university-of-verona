%{
#include <stdio.h>
#include "y.tab.h"
%}


%%

0|[1-9][0-9]*       { yylval.integer = atoi(yytext); return NUM; }
print               return PRINT;
[a-z]+              { yylval.string = strndup(yytext, yyleng); return NAME; }
[\t ]
\n                  return '\n';
.                   return yytext[0];

%%
