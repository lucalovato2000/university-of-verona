%{ 
	#include <ctype.h> 
	#include <stdio.h>
	#include <string.h>
	
	int yylex();
	int yyparse();
	void yyerror(char const *s);

int my_pow(int base, int exp){
          int result=1;
          while (exp != 0) {
            result *= base;
            exp=exp-1;
          }
          return result;
        }
		
	void main(){
		yyparse();
	}	  
%}

%token DIGIT

%%

lines:lines line
	 |line
;

line:expr'\n'	 			{ printf("= %d\n", $1); }
;
	 
expr:expr expr '+'			{ $$ = $1 + $2; }
|expr expr '-'              { $$ = $1 - $2; }
|expr expr '*'              { $$=$1*$2; }
|expr expr '/'              { $$=$1/$2; }
|expr expr '^'              { $$= my_pow($1,$2); }
|DIGIT 				        { $$ = $1; }  
;

%%

void yyerror (char const *s) {
   fprintf(stderr, "%s\n", s);
 }

int yylex(){
 	int c = getchar();
	
	while (c == ' ' || c == '\t')
		c = getchar ();
	
	if (isdigit(c)){
		yylval = c - '0';
		return DIGIT;
	}
	return c;
} 
