%{
#include <stdio.h>

  int yylex();
  void yyerror();  
%}

%define parse.error verbose
%token TOKDIGIT

%%
line: expr '\n' {printf("=%d", $1);}
expr:expr expr '+' {$$=$1+$2;}
| expr expr '-' 
| expr expr '*' 
| expr expr '/' 
| expr expr '^' 
| TOKDIGIT  {$$=$1;}
;
%%

void yyerror(char const *s){
  fprintf(stderr,"%s\n",s);
}

void main(){
  yyparse();
 }


int yylex(){
  int c=getchar();
  while (c==' ' | c=='\t')
    c=getchar();
  if (c>='0'  && c <='9')
    return TOKDIGIT;
    
  return c;
 }