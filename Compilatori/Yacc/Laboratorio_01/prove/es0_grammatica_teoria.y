%{
#include <stdio.h>

  int yylex();
  void yyerror();  
%}

%define parse.error verbose
%token TOKC
%token TOKD

%%
S:C C '\n'
;
C:TOKC C
| TOKD
;
%%

void yyerror(char const *s){
  fprintf(stderr,"%s\n",s);
}

void main(){
  yyparse();
 }


int yylex(){
  int c=getchar();
  while (c==' ' | c=='\t')
    c=getchar();
  if (c=='c')
    return TOKC;
  if (c=='d')
    return TOKD;
  return c;
 }
