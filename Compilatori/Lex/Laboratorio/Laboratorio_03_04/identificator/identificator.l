KEYWORDS if|else|while|for|def
ID 
FLOAT [0-9]+\.[0-9]*
INTEGERS [0-9]+


%%
exit                                    return 0;

{KEYWORDS}                              {printf("KEY ");}
{INTEGERS}                              {printf("INT ");}
{FLOAT}                                 {printf("FLOAT ");}

.                                       ;
\n                                      ECHO;
%%

int main(int argc, char **argv){
    if (argc > 1) {
        yyin = fopen(argv[1], "r");
    }
    else{
        printf("Inserisci un testo: \n");
        yyin = stdin;
    }
    yylex();
}