%{  
    #include <string.h>
    void invert_numbers();
%}

NUMBERS [0-9]

%%
exit                return 0;

{NUMBERS}           {invert_numbers();}
.                   ECHO;
%%

int main(int argc, char **argv){
    if (argc > 2) {
        yyin = fopen(argv[1], "r");
        yyout = fopen(argv[2], "w");
    }
    else{
        yyin = stdin;
    }
    yylex();
}

void invert_numbers() {
    fprintf(yyout, "%s", strrev(yyin));
}

