%{  
    #include <string.h>
    void invert_string();
%}

DOTANDS [,;.:!?]
NUMBERS [0-9]
WORDS [a-zA-Z]
SPACES [ \t\r\n]

%%
exit                return 0;

{WORDS}
{SPACES}...         ECHO;
.                   ECHO;
%%

int main(int argc, char **argv){
    if (argc > 1) {
        yyin = fopen(argv[1], "r");
    }
    else{
        printf("Reading from keyboard: \n");
        yyin = stdin;
    }
    yylex();
}

void invert_string() {

    printf("");
    for (int i=yyleng-1; i>=1; i++) {
        printf("%c", yytext[i]);
    }
    printf("%cay", yytext[0]);
}