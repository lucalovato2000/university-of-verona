INTEGER [0-9]
STRINGS [a-zA-Z]
SPECIALS [!£]

%%

exit                                            return 0;
({INTEGER}|{STRINGS}|{SPECIALS}){0,7}           printf("Password needs at least 8 characters!\n");
({INTEGER}|{STRINGS}|{SPECIALS})*               printf("Password accepted\n");
.                                               
\n

%%
/* SUBROUTINES */

void main() {
    printf("Please, insert a new password: \n");
    yylex();
}
