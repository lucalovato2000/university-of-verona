%{
    int n_variables = 0;
%}

DIGITS [0-9]
STRINGS [a-zA-Z]
SPECIALS [\-_]

%%

exit                                            return 0;
^{STRINGS}({DIGITS}|{STRINGS}|{SPECIALS})*     { printf("%s can be the name of variable.\n", yytext); n_variables++;}
. ;
\n

%%
/* SUBROUTINES */

void main() {
    printf("Type any variable name you can think of.\n");
    yylex();
    printf("%d possible variable name seen.\n", n_variables);
}