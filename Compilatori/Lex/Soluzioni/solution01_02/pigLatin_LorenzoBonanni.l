
%{
    #include <string.h>
    void print_sub();
%}
VOWEL [aeiou]
SPACES [ \t\r\n]
%%
{VOWEL}[a-zA-Z]*           {printf("%say", yytext);}
[a-zA-Z]+                  {print_sub();}
{SPACES}                    ECHO;
.                           ECHO;
%%

/*SUBROUTINES*/
void print_sub() {
    for (int i=1; i<strlen(yytext); i++) {
        printf("%c", yytext[i]);
    }
    printf("%cay", yytext[0]);
}
