
%{
    #include <string.h>
    void print_sub();
%}
TAG <[^<]+>
SPACES [ \t\r\n]
%%
{TAG}               {print_sub();}
{SPACES}+           ECHO;
.                   
%%

int main(int argc, char **argv) {
    if (argc > 1) {
        yyin = fopen (argv[1], "r");
    }
    else {
        printf("Reading from Keyboard:\n");
        yyin = stdin;
    }
    yylex ();
}

void print_sub() {
    for (int i=1; i<strlen(yytext)-1; i++) {
        printf("%c", yytext[i]);
    }
}