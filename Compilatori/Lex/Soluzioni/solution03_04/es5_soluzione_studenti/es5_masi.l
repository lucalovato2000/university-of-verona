%%

if                                      printf("<IF>");
else                                    printf("<ELSE>");
for                                     printf("<FOR>");
while                                   printf("<WHILE>");
[+\-*\/]                                printf("<OP, %s>", yytext);
=                                       printf("<ASSIGN, %s>", yytext);
([<>!]=?|==)                            printf("<CMP, %s>", yytext);
[a-zA-Z][a-zA-Z0-9\_]*                  printf("<ID, %s>", yytext);
[+-]?(0|[1-9][0-9]*)                    printf("<INT, %s>", yytext);
[+-]?(0|[1-9][0-9]*)\.[0-9][1-9]*       printf("<DOUBLE, %s>", yytext);
