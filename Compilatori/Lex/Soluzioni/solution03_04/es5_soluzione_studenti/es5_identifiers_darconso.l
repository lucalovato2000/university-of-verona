
CHARACTERS  [a-zA-z]
DIGITS      [0-9]
IF          "if"
ELSE        "else"
COMP        (<|>|<=|>=|!=|==)
OP          [+\-]
PUN         [.,]
SPACES      [ \t\r]
APICES      \"

%%
exit                                            return 0;
{IF}                                            {printf("if");}
{ELSE}                                          {printf("else");}
^{CHARACTERS}({CHARACTERS}|{DIGITS})*           {printf("id: %s", yytext);}
^{DIGITS}{DIGITS}*{PUN}?"e"?{DIGITS}+           {printf("numero: %s", yytext);}
{DIGITS}+{COMP}{DIGITS}+                        {printf("comp: %s", yytext);}
{DIGITS}+{OP}{DIGITS}+                          {printf("op: %s", yytext);}
{APICES}{CHARACTERS}*{APICES}                   {printf("parola: %s", yytext);}
.                                               ECHO;
%%

int main(int argc, char **argv) {
    if (argc > 2) {
        yyin = fopen (argv[1], "r");
        yyout = fopen(argv[2], "w");
    }
    else {
        printf("Reading from keyboard: \n");
        yyin = stdin;
    }
    yylex();
}