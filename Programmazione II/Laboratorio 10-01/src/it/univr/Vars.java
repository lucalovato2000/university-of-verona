/* 
 * Tipi primitivi: non sono oggetti, sono tutti scritti minuscoli
 * boolean, byte, char, short, int, long, float, double
 * byte: numero scritto in binario senza segno 
 */

package it.univr;

public class Vars {
	
	public static void main(String[] args) {
		
		int i = 13;
		System.out.println("int = " + i);
		
		long l = 123456;
		System.out.println("long = " + l);
		
		float f = 3.14f;	// f NECESSARIA altrimenti letto come double
		System.out.println("float = " + f);
		
		double d = 3.14;	// NON VUOLE LA F
		System.out.println("double = " + d);
		
		boolean b = true;	
		System.out.println("boolean = " + b);
		
		char c = 'a';
		System.out.println("char = " + c);
		
		i = (int) l; // Senza cast non compila perche' long non sta in int

		/* assegnamento: leftvalue = rightvalue
		 * rightvalue e' una qualsiasi espressione
		 * leftvalue ammette solo alcune espressioni:
		 * 1) variabile: v = 13
		 * 2) elemento di array v[3] = 17
		 * 3) campo di oggetto: v.f = 13
		 */ 
		
		i += 5; // i = i + 5;
		i++;    // i = i + 1; postincremento
		++i;    // i= i + 1; preincremento
		
		int k = 5;
		System.out.println(k++);  // Stampa 5!!, incrementa dopo la stampa il numero
		
		long x = i + l;
		double media = (i + k) / 2.0; // Per evitare di perdere quello che segue la virgola
									  
		int j = 2;
		double media2 = (i + k / (double) j);
		
		int w = j++ + j;
		System.out.println("w = " + w);
		
		System.out.println(String.valueOf(i) + j); // Stampare due variabili
		
		final int numero = 173838929;
		// Una costante di tipo FINAL non e' possibile ri-assegnarla, numero++ non si puo' fare
		
		int y;
		
		if (i > 13) {
			y = 13;
		} else {
			y = 0;
		}
		//System.out.printfln(y);  non posso stampare perche' non inizializzata
		
		switch(y) {
			case 0: System.out.println("ok"); break;
			case 1: System.out.println("ko"); break;
			case 2: System.out.println("unknown"); break;
			default: System.out.println("default"); break;
		}
	}
}
