package it.univr;

import java.util.Scanner;	// da importare per utilizzare lo scanner

public class Input {
	
	public static void main(String[] args) {

		String frase;	// frase e' un oggetto di tipo String

		System.out.print("Frase da ripetere: ");		
		
		Scanner keyboard = new Scanner(System.in);	// keyboard e' un oggetto di tipo Scanner
		frase = keyboard.nextLine();	// acquisisce la frase inserita da tastiera	
		
		System.out.println(frase);	// stampa la frase inserita
		
		keyboard.close();
	}
}
