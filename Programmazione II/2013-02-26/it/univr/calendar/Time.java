package it.univr.calendar;

public class Time implements Comparable<Time> {

	private final int year;
	private final int month;
	private final int day;
	private final int hour;
	private final int minute;

	public Time(int year, int month, int day, int hour, int minute) {
		this.year = year;
		this.month = month;
		this.day = day;
		this.hour = hour;
		this.minute = minute;
	}

	@Override
	public String toString() {
		return day + "/" + month + "/" + year + ", " + hour + ":"
				+ (minute < 10 ? ("0" + minute) : minute);
	}

	@Override
	public boolean equals(Object other) {
		return other instanceof Time && 
				year == ((Time)other).year &&
				month == ((Time)other).month &&
				day == ((Time)other).day &&
				hour == ((Time)other).hour &&
				minute == ((Time)other).minute;
	}

	@Override
	public int hashCode() {
		return year ^ month ^ day ^ hour ^ minute;	// tutti int quindi non serve .hashCode()
	}

	@Override
	public int compareTo(Time other) {
		int diff = year - other.year;
		if (diff != 0)
			return diff;
		
		diff = month - other.month;
		if (diff != 0)
			return diff;
		
		diff = day - other.day;
		if (diff != 0)
			return diff;
		
		diff = hour - other.hour;
		if (diff != 0)
			return diff;
		
		return minutes - other.minutes;	
	}
}