package it.univr;

import java.util.HashMap;
import java.util.Map;

// 30 minuti

/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa classe dentro il package it.univr.esame
 * 4) si modifichi questa classe dove indicato con TODO
 * 5) l'esecuzione del main deve stampare "fausto ha in banca 140"
 *    e fermarsi con una BankException
 */

public class Bank {
	private final Map<String, Integer> accounts = new HashMap<>(); //TODO: inizializzate
	
	/**
	 * Aggiunge howMuch soldi all'utente who della banca.
	 * Lancia BankException se howMuch e' negativo.
	 * @throws BankException 
	 */
	public void deposit(String who, int howMuch) throws BankException {
		if (howMuch < 0) {
			throw new BankException();
		} else {
			if (accounts.containsKey(who)) {
				int money = accounts.get(who) + howMuch;
				accounts.put(who, money);
			} else {
				accounts.put(who, howMuch);
			}
		}
		
	}

	/**
	 * Rimuove howMuch soldi all'utente who della banca.
	 * Lancia BankException se howMuch e' negativo oppure
	 * se l'utente who non esiste nella banca oppure
	 * se in banca l'utente who ha meno di howMuch soldi.
	 */
	public void withdraw(String who, int howMuch) throws BankException {
		if (!accounts.containsKey(who) || howMuch < 0 || accounts.get(who) < howMuch) {
			throw new BankException();
		} else {
			int remaining = accounts.get(who) - howMuch;
			accounts.put(who, remaining);
		}
	}
	

	/**
	 * Dice quanti soldi ha in banca l'utente who.
	 * Lancia una BankException se l'utente non e'
	 * presente nella banca.
	 * @throws BankException 
	 */
	public int get(String who) throws BankException {
		if (!accounts.containsKey(who)) {
			throw new BankException();
		} else {
			return accounts.get(who);
		}
	}

	public static void main(String[] args) throws BankException {
		Bank b = new Bank();
		b.deposit("fausto", 100);
		b.deposit("fausto",  50);
		b.withdraw("fausto", 10);
		System.out.println("fausto ha in banca " + b.get("fausto"));
		System.out.println("giorgio ha in banca " + b.get("giorgio"));
	}
}

//TODO: completate qui sotto una classe di eccezione chiamata BankException
class BankException extends Throwable { }
