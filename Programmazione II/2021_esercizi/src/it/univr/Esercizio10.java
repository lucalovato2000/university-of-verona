package it.univr;
import java.util.HashMap;
import java.util.Map;

// 30 minuti

/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa classe dentro il package it.univr.esame
 * 4) si completi questa classe dove indicato con TODO
 * 5) l'esecuzione di questa classe deve stampare "Il piu' ricco e' fausto"
 */

public class Esercizio10 {

	public Map<String, Integer> cont = new HashMap<>();
	
	public static void main(String[] args) {
		Esercizio10 es = new Esercizio10();
		es.put("fausto", 100);
		es.put("guido", 50);
		es.put("fausto", 30);
		es.put("roberto", 60);
		es.put("alessandra", 10);

		String richest = es.getRichest();
		if (richest != null)
			System.out.println("Il piu' ricco e' " + richest);
		else
			System.out.println("Nessuno e' piu' ricco di tutti gli altri insieme");
	}

	// Aggiunge howMuch ai soldi posseduti da name
	public void put(String name, int howMuch) {
		//this.cont.put(name, this.cont.getOrDefault(name, 0) + howMuch);

		if (cont.containsKey(name)) {
			int num = cont.get(name);
			cont.put(name, num + howMuch);
		}else {
			cont.put(name, howMuch);
		}
	}

	// Restituisce il nome di chi, secondo il campo money, da solo ha piu' soldi di tutti gli altri
	// messi insieme. Se non esistesse, restituisce null
	public String getRichest() {
		
		String result = " ";
		
		int max = 0, tmp, tmp2 = 0;
		
		for (String persona : cont.keySet()) {
			tmp = cont.get(persona);
			if (tmp > max) {
				max = tmp;
				result = persona;
			}
		}
		
		for (String persona : cont.keySet()) {
			tmp2 += cont.get(persona);
		}
		
		tmp2-=max;
		
		if (max > tmp2) {
			return result;
		}else {
			return null;
		}
	}
}