package it.univr;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.function.Predicate;

// 35 minuti
/**
 * 1) si crei un progetto Java dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questo file dentro il package it.univr.esame
 * 4) si definisca una sottoclasse SetOfShort di FilteredSet
 *    che implementa un insieme di stringhe palindrome, minuscole, di lunghezza minore o uguale a un parametro
 *    int passato al suo costruttore (se sono piu' lunghe, se non sono minuscole,
 *    o se non sono palindrome, add() non le aggiunge)
 */

/**
 * Un insieme senza ripetizioni di elementi di tipo T, che soddisfano il filtro fornito al costruttore.
 * Due elementi sono considerati uguali se sono equals(), esattamente come in un java.util.Set.
 */
// NON MODIFICATE QUESTA CLASSE
public class FilteredSet<T> implements Iterable<T> {
	private final Predicate<T> filter;
	private final Set<T> elements = new HashSet<>();

	/**
	 * Costruisce un insieme vuoto. Le sottoclassi possono chiamare
	 * questo costruttore passando il filtro che vogliono usare.
	 * 
	 * @param filter il filtro che viene usato quando si prova ad aggiungere
   *               un elemento: se il filtro non e' soddisfatto, add() non
	 *               aggiunge l'elemento
	 *
	 * SI NOTI CHE L'INTERFACCIA DI LIBRERIA Predicate HA UN UNICO METODO
	 */
	public FilteredSet(Predicate<T> filter) {
		this.filter = filter;
	}

	/**
	 * Aggiunge l'elemento indicato solo se soddisfa il filtro passato al costruttore.
	 * Se non lo soddisfa, non fa nulla.
	 * 
	 * @param element l'elemento da aggiungere, se soddisfa il filtro
	 */
	public final void add(T element) {
		if (filter.test(element))
			elements.add(element);
	}

	@Override
	public final Iterator<T> iterator() {
		return elements.iterator();
	}

	public static void main(String[] args) {
		SetOfShort ss = new SetOfShort(15); // conterra' stringhe lunghe al massimo 15 caratteri

		ss.add("ada");
		ss.add("abddba");
		ss.add("casa");
		ss.add("ADA");
		ss.add("Ada");
		ss.add("abcdefghijklkjihgfedcba");

		System.out.print("\nset of short: ");
		for (String s: ss)
			System.out.print(s + " ");

		System.out.println();
	}
	
	public static class SetOfShort extends FilteredSet<String>{

		public SetOfShort(int i) {
			super(new MyFilter(i));
		}

	public static class MyFilter implements Predicate<String> {

		private final int max;
		public MyFilter(int max) {
			this.max = max;
		}

		@Override
		public boolean test(String t) {
			return t.length() < max && Palindrome(t) && LowerCase(t); 
		}

		private boolean LowerCase(String t) {
			int i = 0;
			while(i < t.length()) {
				if (t.charAt(i) >= 'a' && t.charAt(i) <= 'z') {
					i++;
				}else {
					return false;
				}
					
			}
			return true;
		}

		private boolean Palindrome(String t) {
			int i = 0;
			int j = t.length() -1;
			while( i <= j) {
				if (t.charAt(i) == t.charAt(j)) {
					i++;
					j--;
				}else {
					return false;
				}
					
			}
			return true;
		}
	}
	}
	
}