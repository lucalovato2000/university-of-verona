package it.univr;

import java.util.Random;

// 30 minuti

/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa interfaccia dentro il package it.univr.esame
 * 4) si scriva dentro it.univr.esame un'implementazione "StringsImpl" di questa interfaccia
 * 5) se tutto e' corretto, l'esecuzione del main deve stampare dieci stringhe,
 *    senza ripetizioni, ciascuna senza caratteri ripetuti, in ordine alfabetico crescente
 */

/**
 * Un contenitore, senza ripetizioni, di stringhe con caratteri distinti. Iterando su di esso,
 * si devono ottenere le stringhe contenute, in ordine alfabetico crescente.
 */
public interface Strings extends Iterable<String> {

    /**
     * Aggiunge un elemento s, ma soltanto se s e' fatto da caratteri tutti diversi fra loro.
     * Se i caratteri di s sono tutti diversi fra loro ma s era gia' presente in questo
     * contenitore, non fa nulla.
     *
     * @param s l'elemento da aggiungere
     * @throws IllegalArgumentException se s ha caratteri ripetuti
     */
    void add(String s) throws IllegalArgumentException;

    /**
     * Restituisce la quantita' di stringhe distinte aggiunte con successo dentro questo contenitore.
     */
    int size();

    public static void main(String[] args) {
        Random random = new Random();
        Strings strings = new StringsImpl(); // non compila perche' dovete ancora scrivere questa classe
        while (strings.size() < 10)
            try {
                strings.add(randomString(random));
            }
            catch (IllegalArgumentException e) {
                // valore casuale scartato perche' contiene caratteri ripetuti
            }

        for (String s: strings)
            System.out.println(s);
    }

    /**
     * Restituisce una stringa di 10 caratteri casuali (possibilmente con ripetizioni).
     */
    private static String randomString(Random random) {
        String result = "";
        while (result.length() < 10)
            result += (char) ('a' + random.nextInt(26));

        return result;
    }
}