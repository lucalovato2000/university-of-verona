package it.univr;

// 30 minuti

import java.util.Iterator;

/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa classe dentro il package it.univr.esame
 * 4) si completi questa classe dove indicato con TODO, senza aggiungre nessun campo alla classe!
 * 5) l'esecuzione di questa classe deve stampare:
 * 
 * h
 * e
 * l
 * l
 * o
 *
 * w
 * o
 * r
 * l
 * d
 * !
 * h
 * e
 * l
 * l
 * o
 *  
 * w
 * o
 * r
 * l
 * d
 * !
 * h
 * e
 * l
 * l
 * o
 *  
 * w
 * o
 * r
 * l
 * d
 * !
 */

//TODO: rendete questa classe iterabile, in modo che iterandoci si
// ottengano i caratteri (java.lang.Character) del suo messaggio, dal primo all'ultimo
public class Characters implements Iterable<Character>{ // TODO: rendere iterabile
	private final String message;

	public static void main(String[] args) {
		Characters cs = new Characters("hello world!");
		Characters cs2 = new Characters(cs, cs, cs);

		for (char c: cs2)
			System.out.println(c);
	}

	public Characters(String message) {
		// TODO: il messaggio e' quello passato come argomento
		this.message = message;
	}

	public Characters(Characters... cs) {
		// TODO: il messaggio e' la concatenzione dei messaggi dei cs passati come argomento
		String result = "";
		for(Characters c : cs) {
			result += c.message;
		}
		
		this.message = result;
	}

	@Override
	public Iterator<Character> iterator() {

		class MyIterator implements Iterator<Character> {
			int counter = -1;

			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return counter < message.length();	// esegui finchè counter risulta minore della lunghezza del messaggio
			}

			@Override
			public Character next() {
				counter++;	// incrementa counter
				
				return message.charAt(counter);	// ritorna il character nella posizione counter
			}
			
		}
		return new MyIterator();
	}
}