package it.univr;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class StringsImpl implements Strings {
	private SortedSet<String> set = new TreeSet<>();	// creato TreeSet<>()

	@Override
	public Iterator<String> iterator() {
		return set.iterator();	// iteratore delegato a struttura dati
	}

	@Override
	public void add(String s) throws IllegalArgumentException {
		Set<Character> tmp = new HashSet<>();
		int i;
		boolean isPresent = false;
		
		for (i = 0; i < s.length(); i++) {
			if (tmp.contains(s.charAt(i)))
				throw new IllegalArgumentException();
			tmp.add(s.charAt(i));
		}
		
		if (set.contains(s))
			isPresent = true;
	
		if (i == s.length() && isPresent == false)
			set.add(s);
	}

	@Override
	public int size() {
		return set.size();
	}

}
