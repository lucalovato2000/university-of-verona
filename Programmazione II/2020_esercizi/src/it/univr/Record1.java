package it.univr;

import java.util.ArrayList;
import java.util.List;

//TODO: aggiungete gli import

/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa classe dentro il package it.univr.esame
 * 4) si completi questa classe dove indicato con TODO
 * 5) l'esecuzione di questa classe deve stampare:
 * 
 * 1: [Gabriele, D'Annunzio] also known as il vate
 * 2: [Alessandro, Manzoni]
 * 3: [Camillo, Benso, conte, di, Cavour]
 */

public class Record1 {
	private List<String> namess = new ArrayList<>(); // obbligatorio, mai null
	private final int n;
	private final String nickname; // opzionale, potrebbe essere null

	public Record1(int n, String nickname, String... names) {
		this.n = n;
		this.nickname = nickname;
		//this.names = Arrays.asList(names);
		for (String name : names) {
			namess.add(name);
		}
	}

	public Record1(int n) {
		// TODO: scrive il campo n, lascia nickname a null e scrive "Alessandro", "Manzoni" dentro names
		// TODO: per implementarlo dovete usare la concatenazione dei costruttori
		this.n = n;
		this.nickname = null;
		this.namess.add("Alessandro");
		this.namess.add("Manzoni");
	}

	public static void main(String[] args) {
		Record1 record1 = new Record1(1, "il vate", "Gabriele", "D'Annunzio");
		Record1 record2 = new Record1(2);
		Record1 record3 = new Record1(3, null, "Camillo", "Benso", "conte", "di", "Cavour");
		
		System.out.println(record1);
		System.out.println(record2);
		System.out.println(record3);
	}

	// TODO: implementate equals (stesso n, stessi names nello stesso ordine e stesso nickcname, se esiste)
	// TODO: implementate hashCode non banale
	public boolean equals(Object other) {
		return other instanceof Record && n == ((Record1)other).n && namess == ((Record1)other).namess && nickname == ((Record1)other).nickname;
	}
	
	public int hashCode() {
		return n ^ namess.hashCode() ^ nickname.hashCode();
	}

	@Override
	public String toString() {
		return n + ": " + namess + (nickname != null ? " also known as " + nickname : "");
	}
}