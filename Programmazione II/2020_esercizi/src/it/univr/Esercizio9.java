package it.univr;

import java.util.ArrayList;

/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa classe dentro il package it.univr.esame
 * 4) si completi questa classe dove indicato con TODO
 * 5) l'esecuzione di questa classe deve stampare [ciao@@, buona@@notte, buon@giorno, buon@giornissimo, @hello]
 */

public class Esercizio9 {

	public static void main(String[] args) {
		String[] arr = { "ciao", "buon@giornissimo", "buon@giorno", "ciao@@",
				"@hello", "buona@@notte" , "buongiornissimo" };

		System.out.println(filterAndSort(arr)); // TODO: fate compilare
	}

	// TODO: deve restituire gli elementi di arr, in ordine alfabetico
	// decrescente, ma solo quelli che contengono il carattere @
	private static ArrayList<String> filterAndSort(String[] arr) {
		//Comparator<String> cmp;
		
		final String c = "@";
		String [] resres = null;
		ArrayList <String> res = new ArrayList<String>();	// List per creare una ArrayList<>()
		
		for (String str : arr) {
			if (str.contains(c)) {
				 res.add(str);	// inserisco str in res 
			}
		}
		//res.sort(cmp);
		return res;
	}
}