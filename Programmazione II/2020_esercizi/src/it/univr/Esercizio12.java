package it.univr;
/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa classe dentro il package it.univr.esame
 * 4) si completi questa classe dove indicato con TODO
 * 5) l'esecuzione di questa classe deve stampare 20.0 2.0 1000.0 e poi andare in eccezione
 */

public class Esercizio12 {
	
	abstract static class Vehicle {	
		abstract float rent(int km);
	 }
	 
	static class Train extends Vehicle {
		float rent(int km) {
			return (float) (km * 0.1);
		}
	 }
	 
	 static class Bike extends Vehicle {
		 float rent(int km) {
			 return (float) (km * 0.01);
		 }
	 }
	 
	 static class Car extends Vehicle {
		 float rent(int km) {
			 throw new java.lang.UnsupportedOperationException();
		 }
	 }
	 
	 static class Taxi extends Car {
		 float rent(int km) {
			 return (float) (km * 5);
		 }
	 }
	 
	public static void main(String[] args) {
		Vehicle[] vehicles = { new Train(), new Bike(), new Taxi(), new Car() };
		
		for (Vehicle vehicle : vehicles) {
			float res = vehicle.rent(200);
			System.out.println(res);
		}
	}
}