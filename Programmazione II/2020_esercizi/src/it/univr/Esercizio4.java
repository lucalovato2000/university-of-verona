package it.univr;
import java.util.ArrayList;
import java.util.List;
/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa classe dentro il package it.univr.esame
 * 4) si completi questa classe dove indicato con TODO
 * 5) l'esecuzione del main deve stampare "profondita' dell'albero: 3"
 */
public class Esercizio4 {
	public static void main(String[] args) {
		Node tree = new Inner(new Inner(new Leaf(13), new Leaf(17)), new Inner(new Leaf(42)), new Leaf(-51));
		System.out.println("profondita' dell'albero: " + tree.depth());
	}

	private interface Node {
		List<Node> children();
		int depth(); // calcola la profondita' dell'albero: cioe' la distanza massima da radice a foglia
	}

	private static class Leaf implements Node {
		public final int value;

		private Leaf(int value) {
			this.value = value;
		}

		@Override
		public List<Node> children() {
			return new ArrayList<Node>();
		}
		
		//TODO: aggiungete depth()
		@Override
		public int depth() {
		    // se il nodo è una foglia, allora è uno
		    return 1;
		}
	}

	private static class Inner implements Node {
		private final List<Node> children;

		private Inner(Node... children) {
		    if (children.length == 0)
			//TODO: aggiungete tutti gli argomenti children al campo children
			//TODO: lancia una IllegalArgumentException se non c'e' nessun children
			throw new IllegalArgumentException();

		    this.children = new ArrayList<>();
		    for (Node child: children)
			this.children.add(child);
		}

		@Override
		public List<Node> children() {
			return children;
		}

		//TODO: aggiungete depth()
		@Override
		public int depth() {
		    // se il nodo è interno, allora è uno in più del massimo fra le profondità dei figli.
		    int max = 0;

		    for (Node child: children) {
			int h = child.depth();
			if (h > max)
			    max = h;
		    }

		    return max + 1;
		}
	}
}
