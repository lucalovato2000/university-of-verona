package it.univr;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa classe dentro il package it.univr.esame
 * 4) si completi questa classe dove indicato con TODO
 * 5) l'esecuzione di questa classe deve stampare [Bike, Taxi, Train, Train, Train]
 */

public class Esercizio13 {

	public static void main(String[] args) {
		
		Vehicle[] vehicles = { 
				new Train(100),
				new Bike(), 
				new Taxi(), 
				new Train(150), 
				new Train(78)
		};
		SortedSet<Vehicle> ss = new TreeSet<>();

		// TODO: si aggiungano ad ss tutti gli elementi di vehicles, tenendoli in ordine
		// crescente per capacity
		for (Vehicle vehicle: vehicles) {
			ss.add(vehicle);
		}
		System.out.println(ss);
	}

	public abstract static class Vehicle {
		
		abstract int capacity();

		@Override
		public String toString() {
			return this.getClass().getSimpleName(); // il nome della classe di "this"
		}
	}
	
	public static class Bike extends Vehicle {

		@Override
		int capacity() {
			return 1;
		}
	}
	
	public static class Train extends Vehicle {
		
		private int num;

		public Train(int num) {
			this.num = num;
		}
		
		@Override
		int capacity() {
			return num;
		}
	}
	
	public static class Car extends Vehicle {

		@Override
		int capacity() {
			return 4;
		}
	}
	
	public static class Taxi extends Car {

		@Override
		int capacity() {
			return 4;
		}
	}
}