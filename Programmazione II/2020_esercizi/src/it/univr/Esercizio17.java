package it.univr;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa classe dentro il package it.univr.esame
 * 4) si completi questa classe dove indicato con TODO
 * 5) l'esecuzione di questa classe deve stampare:
 * 
 * 2 gennaio: INVERNO
 * 30 aprile: PRIMAVERA
 * 20 agosto: ESTATE
 * 23 dicembre: INVERNO
 */

public class Esercizio17 {

	public static void main(String[] args) {
		Data d1 = new Data(23, Mese.DICEMBRE);
		Data d2 = new Data(30, Mese.APRILE);
		Data d3 = new Data(2, Mese.GENNAIO);
		Data d4 = new Data(20, Mese.AGOSTO);

		SortedSet<Data> ss = new TreeSet<>();
		ss.add(d1);
		ss.add(d2);
		ss.add(d3);
		ss.add(d4);
		for (Data d: ss)
			System.out.println(d + ": " + d.stagione());
	}

	public static enum Stagione {
		PRIMAVERA, // 21 marzo - 21 giugno
		ESTATE, // 22 giugno - 22 settembre
		AUTUNNO, // 23 settembre - 21 dicembre
		INVERNO // 22 dicembre - 20 marzo
	}
	public static enum Mese {
		GENNAIO,
		FEBBRAIO,
		MARZO,
		APRILE,
		MAGGIO,
		GIUGNO,
		LUGLIO,
		AGOSTO,
		SETTEMBRE,
		OTTOBRE,
		NOVEMBRE,
		DICEMBRE
	}

	// aggiungete i metodi hashCode (non banale) e compareTo;
	// l'ordinamento fra le Data deve essere cronologico
	public static class Data implements Comparable<Data> {
		private final int giorno;
		private final Mese mese;
		
		public Data(int giorno, Mese mese) {
			this.giorno = giorno;
			this.mese = mese;
		}

		@Override
		public String toString() {
			return giorno + " " + mese.toString().toLowerCase();
		}

		@Override
		public boolean equals(Object other) {
			return other instanceof Data && ((Data) other).giorno == giorno && ((Data) other).mese == mese;
		}

		public int hashCode() {
			return giorno ^ mese.hashCode();
		}
		
		public int compareTo(Data other) {
			int diff = mese.compareTo(mese);
			if (diff != 0) {
				return diff;
			}else {
				return giorno - other.giorno;
			}
		}

		public Stagione stagione() {
			if ((giorno >= 21 && mese == Mese.MARZO) || (mese == Mese.APRILE) ||(mese == Mese.MAGGIO) || (giorno <= 21 && mese == Mese.GIUGNO)){
				return Stagione.PRIMAVERA; // 21 marzo - 21 giugno
			}
			if ((giorno >= 22 && mese == Mese.GIUGNO) || (mese == Mese.LUGLIO) ||(mese == Mese.AGOSTO) || (giorno <= 22 && mese == Mese.SETTEMBRE)){
				return Stagione.ESTATE; // 22 giugno - 22 settembre

			}
			if ((giorno >= 23 && mese == Mese.SETTEMBRE) || (mese == Mese.OTTOBRE) ||(mese == Mese.NOVEMBRE) || (giorno <= 21 && mese == Mese.DICEMBRE)){
				return Stagione.AUTUNNO; // 23 settembre - 21 dicembre
			}
			//if ((giorno >= 22 && mese == Mese.DICEMBRE) || (mese == Mese.GENNAIO) ||(mese == Mese.FEBBRAIO) || (giorno <= 20 && mese == Mese.MARZO)){
			else return Stagione.INVERNO; // 22 dicembre - 20 marzo
		}
	}
}