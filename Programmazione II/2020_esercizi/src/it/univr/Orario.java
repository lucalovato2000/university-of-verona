package it.univr;

import java.util.SortedSet;
import java.util.TreeSet;

/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa classe dentro il package it.univr.esame
 * 4) si completi questa classe dove indicato con TODO,
 *    facendo in modo che tutto compili senza errori
 * 5) l'esecuzione del main deve stampare:
 * 
 * 04:30:00: NOTTE
 * 05:06:09: MATTINO
 * 13:15:59: GIORNO
 * 20:25:08: SERA
 */

public class Orario implements Comparable<Orario> {
	private final int ore;
	private final int minuti;
	private final int secondi;

	public static void main(String[] args) {
		Orario t1 = new Orario(13, 15, 59);
		Orario t2 = new Orario(5, 6, 9);
		Orario t3 = new Orario(20, 25, 8);
		Orario t4 = new Orario(4, 30, 0);

		SortedSet<Orario> ss = new TreeSet<Orario>(); // TODO: inizializzate
		ss.add(t1);
		ss.add(t2);
		ss.add(t3);
		ss.add(t4);

		for (Orario t: ss)
			System.out.println(t + ": " + t.getFase());
	}

	public Orario(int ore, int minuti, int secondi) {
		this.ore = ore;
		this.minuti = minuti;
		this.secondi = secondi;
	}

	@Override
	public int hashCode() {
		return ore ^ minuti ^ secondi;
	}
	
	public boolean equals(Object other) {
		return other instanceof Orario && ore == ((Orario)other).ore && minuti == ((Orario)other).minuti && secondi == ((Orario)other).secondi;
	}
	
	public int compareTo(Orario other) {
		int diff = ore - other.ore;
		if (diff != 0) {
			return diff;
		}
		diff = minuti - other.minuti;
		if (diff != 0) {
			return diff;
		}
		return secondi - other.secondi;
	}

	@Override
	public String toString() {
		return String.format("%02d:%02d:%02d", ore, minuti, secondi);
	}
	
	public enum Fase {
		MATTINO,
		GIORNO,
		POMERIGGIO,
		SERA,
		NOTTE
	}
	// TODO: definite un'enumerazione Fase con cinque elementi: 
	// MATTINO dalle 05:00:00 alle 10:29:59
	// GIORNO dalle 10:30:00 alle 14:29:59
	// POMERIGGIO dalle 14:30:00 alle 18:59:59
	// SERA dalle 19:00:00 alle 22:39:59
	// NOTTE dalle 22:40:00 alle 04:59:59

	public Fase getFase() {
		if ((ore >= 5 && ore <= 9) || (ore == 10 && minuti <= 29 && secondi <= 59)) {
			return Fase.MATTINO;
		}else if ((ore >= 11 && ore <= 13) || (ore == 10 && minuti >= 30 && secondi >= 00) || (ore == 14 && minuti <= 29 && secondi <= 59) ) {
			return Fase.GIORNO;
		}else if ((ore >= 15 && ore <= 18) || (ore == 14 && minuti >= 30 && secondi >= 00) || (ore == 18 && minuti <= 59 && secondi <= 59) ) {
			return Fase.POMERIGGIO;
		}else if ((ore >= 19 && ore <= 21) || (ore == 2 && minuti <= 29 && secondi <= 59)) {
			return Fase.SERA;
		}else {
			return Fase.NOTTE;
		}
	}
}