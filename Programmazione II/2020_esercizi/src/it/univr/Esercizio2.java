package it.univr;
// Fatto 8/6/2020
/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa classe dentro il package it.univr.esame
 * 4) si modifichi questa classe aggiungendo il metodo pubblico max dentro Node
 *    in modo che calcoli il massimo dei valori del Node e dei suoi successori
 * 5) l'esecuzione del main deve stampare 72 e 42
 * 6) si sposti la classe Node dentro Esercizio2, come classe privata statica,
 *    eliminado i suoi due metodi getValue e getNext
 */

public class Esercizio2 {
	public static void main(String[] args) {
		
		class Node {
			private int value;
			private Node next;

			Node(int value, Node next) {
				this.value = value;
				this.next = next;
			}
			
			int max() {
				
				int max = 0;
				
				while (next != null) {
					if (next.value > max) {
						max = next.value;
					}
					next = next.next;
				}
				return max;
			}
		}
		
		Node n1 = new Node(13, new Node(17, new Node(42, null)));
		System.out.println(n1.value+ n1.next.value + n1.next.next.value);
		System.out.println(n1.max());
	}
}