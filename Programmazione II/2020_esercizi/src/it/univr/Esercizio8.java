package it.univr;

import java.util.Arrays;

/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa classe dentro il package it.univr.esame
 * 4) si completi questa classe dove indicato con TODO
 * 5) l'esecuzione di questa classe deve stampare [@hello, ciao@@]
 */

public class Esercizio8 {

	public static void main(String[] args) {
		String[] arr = { "ciao", "buon@giornissimo", "buon@giorno", "ciao@@",
				"@hello", "buona@@notte" , "buongiornissimo" };

		System.out.println(Arrays.toString(filterAndSort(arr))); // TODO: fate compilare
	}

	// TODO: deve restituire gli elementi di arr, in ordine alfabetico
	// crescente, ma solo quelli che contengono il carattere @
	// e che siano lunghi al massimo 10 caratteri
	private static String[] filterAndSort(String[] arr) {
		
		String[] res = null;
		String s = "@";
		int i = 1;
		
		for (String string : arr) {
			if (string.contains(s) && string.length() <= 10) {
				res[i] = arr.toString();
				i++;
			}
		}
		return res;
	}
}