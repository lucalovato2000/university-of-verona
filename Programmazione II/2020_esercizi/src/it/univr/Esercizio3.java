package it.univr;
// Fatto 8/6/2020
import java.util.ArrayList;
import java.util.List;

/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa classe dentro il package it.univr.esame
 * 4) si completi questa classe dove indicato con TODO
 * 5) l'esecuzione del main deve stampare "numero foglie: 4"
 */

public class Esercizio3 {
	public static void main(String[] args) {
		Node tree = new Inner(new Inner(new Leaf(13), new Leaf(17)), new Inner(new Leaf(42)), new Leaf(-51));
		System.out.println("numero foglie: " + tree.countLeaves());
	}

	private interface Node {
		List<Node> children();
		int countLeaves(); // conta quante foglie ci sono nell'albero radicato in this
	}

	private static class Leaf implements Node {
		public final int value;

		private Leaf(int value) {
			this.value = value;
		}

		@Override
		public List<Node> children() {
			return new ArrayList<Node>();
		}

		public int countLeaves() {
			return 1;
		}
	}

	private static class Inner implements Node {
		private final List<Node> children;
		
		private Inner(Node... children) {
			
			if (children.length == 0) {
				throw new IllegalArgumentException();
			}
			
		    this.children = new ArrayList<>();

		    for (Node child: children) {
				this.children.add(child);
		    }
		}

		@Override
		public List<Node> children() {
			return children;
		}

		public int countLeaves() {
			int sum = 0;
			
			for (Node child: children) {
				sum += child.countLeaves();
			}
			return sum;
		}
	}
}