package it.univr;
import java.util.HashMap;
import java.util.Map;

/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa classe dentro il package it.univr.esame
 * 4) si completi questa classe dove indicato con TODO
 * 5) l'esecuzione di questa classe deve stampare qualcosa del tipo:
 * 
 * party1: 5 votes (38%)
 * party4: 2 votes (15%)
 * party3: 3 votes (23%)
 * party2: 3 votes (23%)
 *
 * party1: 7 votes (53%)
 * party3: 3 votes (23%)
 * party2: 3 votes (23%)
 */

public class Esercizio14 {

	public static void main(String[] args) {
		
		Election election = new Election();
		registerVotes(election);
		System.out.println(election.toString());
		
		ElectionCheat electionCheat = new ElectionCheat();
		registerVotes(electionCheat);
		System.out.println(electionCheat.toString());
	}

	private static void registerVotes(Election election) {
		election.vote("party1");
		election.vote("party4");
		election.vote("party3");
		election.vote("party3");
		election.vote("party1");
		election.vote("party1");
		election.vote("party2");
		election.vote("party3");
		election.vote("party2");
		election.vote("party1");
		election.vote("party1");
		election.vote("party2");
		election.vote("party4");
	}

	public static class Election {
		// i voti, per ciascun partito
		protected final static Map<String, Integer> votes = new HashMap<>();
				
		public void vote(String party) {
			// int count = votes.containsKey(party) ? votes.get(party) : 0;
			int count = 0;
			
			if (votes.containsKey(party)) {
				count = votes.get(party);
			}else {
				count = 0;
			}
			
			votes.put(party, count + 1);
		}

		@Override
		public String toString() {
			String result = "";
			int total = votes.values().stream().mapToInt(Integer::intValue).sum();

			for (String party: votes.keySet())
				result += party + ": " + votes.get(party) + " votes (" + votes.get(party) * 100 / total + "%)\n";

			return result;
		}
	}
	
	 public static class ElectionCheat extends Election {
		
		 protected final static Map<String, Integer> votes = new HashMap<>();

		 public void vote(String party) {
			
			if (party == "party4"){
				party = "party1";
			}
			 
			int count = votes.containsKey(party) ? votes.get(party) : 0;
			votes.put(party, count + 1);
			
		}
		 
		@Override
		public String toString() {
			String result = "";
			int total = votes.values().stream().mapToInt(Integer::intValue).sum();

			for (String party: votes.keySet())
				result += party + ": " + votes.get(party) + " votes (" + votes.get(party) * 100 / total + "%)\n";

			return result;
		}
	}
}