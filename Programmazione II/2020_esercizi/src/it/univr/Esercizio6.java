package it.univr;
/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa classe dentro il package it.univr.esame
 * 4) si completi questa classe dove indicato con TODO
 */

public class Esercizio6 {

	public static void main(String[] args) {
		Time t1 = new Time24(9, 12, 5);
		System.out.println(t1);
		Time t2 = new Time12(9, 12, 5, When.MORNING);
		System.out.println(t2);
		Time t3 = new Time12(1, 5, 55, When.AFTERNOON);
		System.out.println(t3);
	}

	public abstract static class Time {
		// TODO: si aggiunga un metodo astratto chiamato next
		public abstract Time next(Time this);
	}

	public static class Time24 extends Time {
		private int hh, mm, ss;

		// riceve ore (hh, 0-23), minuti (mm) e secondi (ss)
		private Time24(int hh, int mm, int ss) {
			this.hh = hh;
			this.mm = mm;
			this.ss = ss;
		}

		@Override
		public String toString() {
			return String.format("%02d:%02d:%02d", hh, mm, ss);
		}

		@Override
		public Time next() {
			Time24 newtime;
			return newtime = new Time24(hh, mm, ss+1);
		}
	}

	private enum When { MORNING, AFTERNOON };
	
	public static class Time12 extends Time {
		private int hh, mm, ss;
		private When when;

		// riceve ore (hh, 0-11), minuti (mm), secondi (ss) e fase della giornata (when)
		private Time12(int hh, int mm, int ss, When when) {
			this.hh = hh;
			this.mm = mm;
			this.ss = ss;
			this.when = when;
		}

		@Override
		public String toString() {
			return String.format("%02d:%02d:%02d %s", hh, mm, ss, when);
		}

		@Override
		public Time next() {
			Time12 newtime;
			return newtime = new Time12(hh, mm, ss+1, when);
		}
	}
}