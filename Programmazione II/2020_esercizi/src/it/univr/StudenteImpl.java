package it.univr;

public class StudenteImpl implements Studente {
	
	private Integer matricola;
	private String nome;
	private String cognome;

	public StudenteImpl(Integer matricola, String nome, String cognome) {
		this.matricola = matricola;
		this.nome = nome;
		this.cognome = cognome;
	}

	public StudenteImpl(Studente s1) {
		this.matricola = s1.getMatricola();
		this.nome = s1.getNome();
		this.cognome = s1.getCognome();
	}
	
	@Override
	public String getNome() {
		return nome;
	}

	@Override
	public String getCognome() {
		return cognome;
	}

	@Override
	public int getMatricola() {
		return matricola;
	}

	@Override
	public int compareTo(Studente other) {
		int diff = matricola - other.getMatricola();
		if (diff != 0) {
			return diff;
		}
		diff = nome.compareTo(getNome());
		if (diff != 0) {
			return diff;
		}
		return cognome.compareTo(getCognome());
	}
}
