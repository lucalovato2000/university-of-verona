package it.univr;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa interfaccia dentro il package it.univr.esame
 * 4) si scriva dentro it.univr.esame un'implementazione "StudenteImpl" di questa interfaccia
 *    con due costruttori: il primo riceve matricola, nome e cognome;
 *    il secondo riceve uno Studente e crea una sua copia, usando la concatenazione dei costruttori
 * 5) se tutto e' corretto, l'esecuzione del main deve stampare [VR153535 Fausto Spoto, VR351134 Albert Einstein]
 */

// gli studenti sono ordinati per matricola, poi per cognome, poi per nome
public interface Studente extends Comparable<Studente> {
	String getNome();
	String getCognome();
	int getMatricola();
	String toString(); // "VR" + matricola + nome + cognome
	boolean equals(Object obj); // stessa matricola, stesso nome e stesso cognome

	static void main(String[] args) {
		Studente s1 = new StudenteImpl(351134, "Albert", "Einstein");
		Studente s2 = new StudenteImpl(153535, "Fausto", "Spoto");
		Studente s3 = new StudenteImpl(s1);
		SortedSet<Studente> ss = new TreeSet<>();
		ss.add(s1);
		ss.add(s2);
		ss.add(s3);
		System.out.println(ss);
	}
}