package it.univr;

/**

 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa classe dentro il package it.univr.esame
 * 4) si completi questa classe dove indicato con TODO
 * 5) l'esecuzione di questa classe deve stampare:
 * 
 * sono il Bounded#0
 * sono il Bounded#1
 * sono il Bounded#2
 * sono il Bounded#3
 * sono il Bounded#4
 * sono il Bounded#5
 * sono il Bounded#6
 * sono il Bounded#7
 * sono il Bounded#8
 * sono il Bounded#9
 * Exception in thread "main" NoMoreObjectsAvailableException
 */

/**
 * Una classe che ammette solo una quantita' limitata di sue istanze: non piu' di MAX.
 */
public class Bounded {
	public final static int MAX = 10;
	private final int num; // il numero dell'istanza, dalla 0 alla MAX-1
	private static int somma = 0;

	/**
	 * Crea la prossima istanza, se possibile. Se ne sono gia' state create MAX,
	 * fallisce lanciando una NoMoreObjectsAvailableException.
	 */
	public Bounded() throws NoMoreObjectsAvailableException {
		// TODO: implementate
		// TODO: scrivete la classe di eccezione controllata NoMoreObjectsAvailableException (in fondo a questo stesso file)
		if(somma >= MAX) {
			throw new NoMoreObjectsAvailableException();
		}
		this.num = somma++;
	}

	@Override
	public String toString() {
		return "sono il Bounded #" + num;
	}

	public static void main(String[] args) throws NoMoreObjectsAvailableException {
		while (true) {
			System.out.println(new Bounded());
		}
	}
}

//TODO: completare
class NoMoreObjectsAvailableException extends IllegalArgumentException{

	public NoMoreObjectsAvailableException() {	// importante, sbagliato richiamare solo il metodo super()
		super("Sono stati inizializzati già il numero massimo di elementi");
	}
}