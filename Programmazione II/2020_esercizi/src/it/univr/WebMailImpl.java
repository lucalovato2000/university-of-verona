package it.univr;

import java.util.ArrayList;
import java.util.List;

public class WebMailImpl implements WebMail {
	
	private List <Message> messages = new ArrayList<Message>();
	private List <Message> result = new ArrayList<Message>();

	@Override
	public void send(Message message) {
		messages.add(message);
	}

	@Override
	public List<Message> search(String sender) {
		
		for (Message msg: messages) {
			if (sender == msg.getSender()) {
				 result.add(msg);
			}
		}
		return result;
	}
}
