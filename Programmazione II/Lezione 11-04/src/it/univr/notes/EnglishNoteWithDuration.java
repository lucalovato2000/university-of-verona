package it.univr.notes;

public class EnglishNoteWithDuration extends EnglishNote {
	private final Duration duration;

	public EnglishNoteWithDuration(int semitone, Duration duration) {
		super(semitone);
		
		this.duration = duration;
	}

	public Duration getDuration() {
		return duration;
	}

	public String toString() {
		// usa l'implementazione di toString() che c'e' nella
		// superclasse EnglishNote e IN PIU' concatena con la duration
		return super.toString() + " " + duration;
	}
}
