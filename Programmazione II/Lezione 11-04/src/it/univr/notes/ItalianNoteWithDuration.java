package it.univr.notes;

public class ItalianNoteWithDuration extends ItalianNote {
	private final Duration duration;

	public ItalianNoteWithDuration(int semitone, Duration duration) {
		super(semitone);
		
		this.duration = duration;
	}

	public Duration getDuration() {
		return duration;
	}

	public String toString() {
		// usa l'implementazione di toString() che c'e' nella
		// superclasse ItalianNote e IN PIU' concatena con la duration
		return super.toString() + " " + duration;
	}
}
