package it.univr.notes;

import java.util.Random;

public class Main {

	public static void main(String[] args) {
		Note[] arr = new Note[10];
		arr[0] = new ItalianNote(5);
		arr[1] = new EnglishNote(3);
		arr[2] = new ItalianNoteWithDuration(5, Duration.CROMA);
		// principio di Liskov:
		// tutto quello che posso fare con un supertipo
		// lo devo poter fare anche con un suo sottotipo
		arr[3] = new ItalianNote(11);
		arr[4] = new EnglishNoteWithDuration(5, Duration.BREVE);
		arr[5] = new EnglishNote(1);
		arr[6] = new ItalianNote(5);
		arr[7] = new EnglishNote(4);
		arr[8] = new ItalianNote(10);
		arr[9] = new ItalianNoteWithDuration(3, Duration.SEMICROMA);

		System.out.println("Metto l'array in ordine crescente");
		sort(arr);
		
		for (Note x: arr)
			print(x);

		String[] arr2 = { "ciao", "amico", "come", "va?" };
		sort(arr2);

		Random[] arr3 = { new Random(), new Random() };
		//sort(arr3); // non compila
	}

	/**
	 * Bubblesort.
	 * 
	 * @param arr l'array da ordinare
	 */
	private static <T extends Comparable<T>> void sort(T[] arr) {
		while (swap(arr));
	}

	/**
	 * Scandisce l'array, se trova due elementi contigui
	 * con ordine errato li scambia.
	 * 
	 * @param arr l'array
	 * @return true se e solo se ha effettuato almeno uno scambio
	 */
	private static <T extends Comparable<T>> boolean swap(T[] arr) {
		boolean swapped = false;
		for (int pos = 0; pos < arr.length - 1; pos++)
			if (arr[pos].compareTo(arr[pos + 1]) > 0) {
				swap(arr, pos, pos + 1);
				swapped = true;
			}

		return swapped;
	}

	private static <T> void swap(T[] arr, int pos1, int pos2) {
		T temp = arr[pos1];
		arr[pos1] = arr[pos2];
		arr[pos2] = temp;
	}

	private static void print(Note n) {
		// la chiamata a toString() e' implementato come
		// una ricerca (look-up per il late-binding)
		// dell'implementazione corretta di toString()
		// a partire dal tipo dinamico di n
		// risalendo sulla catena delle superclassi
		// finche' non trovo l'implementazione
		System.out.println(n.toString());
	}
}
