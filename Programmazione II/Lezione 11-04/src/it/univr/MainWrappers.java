package it.univr;

public class MainWrappers {

	public static void main(String[] args) {
		Integer i1 = new Integer(13);
		Integer i2 = new Integer(17);
		Integer i3 = new Integer(13);
		
		System.out.println("i1 == i2? " + (i1 == i2));
		System.out.println("i1 == i3? " + (i1 == i3));
		System.out.println("i1.equals(i2)? " + (i1.equals(i2)));
		System.out.println("i1.equals(i3)? " + (i1.equals(i3)));

		Character.isLowerCase('a');
		Character.isLowerCase('A');

		// boxing manuale
		Integer i4 = new Integer(13); // deprecato
		// boxing manuale
		Integer i5 = Integer.valueOf(13); // consigliato

		// boxing automatico
		Integer i6 = 13; // abbreviazione di Integer.valueOf(13)

		// unboxing manuale
		int i7 = i5.intValue();

		// unboxing automatico, abbreviazione di i5.intValue();
		int i8 = i5;

		Integer i10 = i5;

		Integer i9 = null;
		// quanto segue va in NullPointerException
		//int i10 = i9; // [.intValue()]
	}

}
