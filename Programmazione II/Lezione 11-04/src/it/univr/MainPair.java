package it.univr;

import java.util.Random;

import it.univr.notes.ItalianNote;
import it.univr.notes.Note;

public class MainPair {

	public static void main(String[] args) {
		// coppia di String e Note
		Pair<String,Note> p1 = new Pair<String,Note>("ciao", new ItalianNote(5));
		String f = p1.getFirst();
		Note s = p1.getSecond();
		System.out.println(f);
		System.out.println(s);

		// coppia di String e Random
		Pair<String,Random> p2 = new Pair<>("hello", new Random());
		String fp2 = p2.getFirst();
		Random sp2 = p2.getSecond();
		System.out.println(fp2);
		System.out.println(sp2);

		Pair<int[], Note[]> p3 = new Pair<int[], Note[]>(new int[] { 2, 3 }, new Note[] {});
		
		// non compila perché le variabili di tipo generico devono
		// essere sostituite con tipi riferimento e non con tipi primitivi
		//Pair<int, Note> p4 = new Pair<int, Note>(13, new ItalianNote(5));
		
		// ci sono tipi riferimento corrispondenti ai primitivi:
		// (classi wrapper)
		// boolean   [java.lang.]Boolean
		// byte      Byte
		// char      Character
		// short     Short
		// int       Integer
		// long      Long
		// float     Float
		// double    Double

		Integer i = new Integer(13);
		Note n = new ItalianNote(7);
		Pair<Integer, Note> p4 = new Pair<Integer, Note>(i, n);
	}




}