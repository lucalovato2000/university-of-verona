package it.univr;

public class MainWrappers2 {

	public static void main(String[] args) {
		// factory methods
		Integer i1 = Integer.valueOf(13);
		Integer i2 = Integer.valueOf(17);
		Integer i3 = Integer.valueOf(13);
		
		System.out.println("i1 == i2? " + (i1 == i2));
		System.out.println("i1 == i3? " + (i1 == i3));
		System.out.println("i1.equals(i2)? " + (i1.equals(i2)));
		System.out.println("i1.equals(i3)? " + (i1.equals(i3)));

	}

}
