package it.univr;

public enum Nationality {
	ITALIAN,
	AMERICAN,
	GERMAN
}
