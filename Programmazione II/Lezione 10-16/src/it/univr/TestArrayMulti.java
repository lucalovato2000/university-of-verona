package it.univr;

public class TestArrayMulti {

	public static void main(String[] args) {
		int[][] arr1 = { {2,3}, {3,6}, {8,-2} };
		int[][] arr2 = new int[5][8]; // inizializzato a 0
		
		for (int riga = 0; riga < arr1.length; riga++) {
			for (int colonna = 0; colonna < arr1[riga].length; colonna++)
				System.out.print(arr1[riga][colonna] + " ");
			
			System.out.println();
		}

		for (int riga = 0; riga < arr2.length; riga++) {
			for (int colonna = 0; colonna < arr2[riga].length; colonna++)
				System.out.print(arr2[riga][colonna] + " ");
			
			System.out.println();
		}
		
		int[] riga0 = arr1[0];
		int[] riga1 = arr1[1];
		int[] riga2 = arr1[2];
		arr1[1] = new int[4]; // inizializzati a 0
		
		for (int riga = 0; riga < arr1.length; riga++) {
			for (int colonna = 0; colonna < arr1[riga].length; colonna++)
				System.out.print(arr1[riga][colonna] + " ");
			
			System.out.println();
		}
		
		for (int[] riga: arr1) {
			for (int element: riga)
				System.out.print(element + " ");

			System.out.println();
		}

		arr1[2] = null;
		for (int riga = 0; riga < arr1.length; riga++) {
			if (arr1[riga] == null)
				System.out.println("null");
			else
				for (int colonna = 0; colonna < arr1[riga].length; colonna++)
					System.out.print(arr1[riga][colonna] + " ");
			
			System.out.println();
		}
	}
}
