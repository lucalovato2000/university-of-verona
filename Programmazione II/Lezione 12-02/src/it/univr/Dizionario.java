package it.univr;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Dizionario {

	public static void main(String[] args) {
		Map<String, String> dizionario = new HashMap<>();
		
		dizionario.put("casa", "house");
		dizionario.put("gatto", "cat");
		dizionario.put("cane", "dog");
		
		Scanner keyboard = new Scanner(System.in);
		while (true) {
			System.out.print("Parola da tradurre: ");
			String italiano = keyboard.nextLine();
			if (italiano.equals("fine"))
				break;

			System.out.println(italiano + " si traduce " + dizionario.get(italiano));
		}
		keyboard.close();
	}
}