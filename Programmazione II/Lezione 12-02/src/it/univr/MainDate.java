package it.univr;

import java.util.Scanner;

public class MainDate {

	public static void main(String[] args) {
		Date d = null;
		
		Scanner keyboard = new Scanner(System.in);

		boolean legale;

		do {
			System.out.print("day: ");
			int day = keyboard.nextInt();
			System.out.print("month: ");
			int month = keyboard.nextInt();
			System.out.print("year: ");
			int year = keyboard.nextInt();

			try {
				d = new Date(day, month, year);
				legale = true;
			}
			catch (IllegalDateException e) {
				System.out.println("i dati non sono legali");
				legale = false;
			}
		}
		while (!legale);

		System.out.println(d);
		keyboard.close();
	}
}
