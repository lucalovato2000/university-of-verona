package it.univr;

import java.util.ArrayList;

public class TestLists {

	public static void main(String[] args) {
		ArrayList<Date> arr = new ArrayList<>();
		arr.add(new Date()); // random
		arr.add(new Date());
		arr.add(new Date());
		arr.add(new Date());
		arr.add(new Date());
		
		// non compila
		foo(arr); // Date <= Object non implica che ArrayList<Date> <= ArrayList<Object>

		for (Date date: arr)
			System.out.println(date);
	}

	private static void foo(ArrayList<Object> x) {
		x.set(2, "ciao"); // String <= Object
	}
}
