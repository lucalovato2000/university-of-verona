package it.univr;

public enum Season {
	WINTER, // ha ordinal() 0
	SPRING, // ha ordinal() 1
	SUMMER, // ha ordinal() 2
	AUTUMN  // ha ordinal() 3
}