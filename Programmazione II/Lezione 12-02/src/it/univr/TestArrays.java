package it.univr;

public class TestArrays {

	public static void main(String[] args) {
		Date[] arr = new Date[5];
		arr[0] = new Date(); // random
		arr[1] = new Date();
		arr[2] = new Date();
		arr[3] = new Date();
		arr[4] = new Date();
		
		foo(arr); // Date <= Object quindi Date[] <= Object[]  (varianza)

		for (Date date: arr)
			System.out.println(date);
	}

	private static void foo(Object[] x) {
		x[2] = "ciao"; // String <= Object
	}
}
