package it.univr.notes;

public class MainItalianNode {
	public static void main(String[] args) {
		// tipo di dichiarazione di una variabile (tipo statico):
		// quello alla sinistra della variabile
		// quando la dichiariamo
		
		// tipo del valore di una variabile (tipo dinamico):
		// quello dell'oggetto contenuto dentro la variabile
		// a tempo di esecuzione

		ItalianNote n1 = new ItalianNote(5);
		System.out.println(n1.toString());
		Note n2 = new Note(11);
		System.out.println(n2.toString());
		Note n3 = new ItalianNote(6);
		System.out.println(n3.toString());
		ItalianNote n4;
		// quanto sotto non compila perche' il compilatore
		// guarda solo i tipi statici
		//n4 = n3; // ??????
	}
}
