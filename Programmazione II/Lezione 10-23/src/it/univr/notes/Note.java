package it.univr.notes;

public class Note {
	private final int semitone;
	
	/**
	 * Costruisce una nota, specificando il semitono.
	 * 
	 * @param semitone il semitono (tra 0 e 11)
	 */
	public Note(int semitone) {
		this.semitone = semitone;
	}

	/**
	 * Accessor method per il semitono.
	 */
	public int getSemitone() {
		return semitone;
	}

	public String toString() {
		return "nota di semitono " + semitone;
	}

	public boolean equals(Note other) {
		return semitone == other.semitone;
	}

	public int compareTo(Note other) {
		return semitone - other.semitone;
	}
}