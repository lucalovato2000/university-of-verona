package it.univr.figures;

/**
 * Una nota italiana estende una Note generica.
 */

// ItalianNode < Note
public class ItalianNote extends Note { // ItalianNote e' una Note

	public ItalianNote(int semitone) {
		super(semitone);
	}

	private final static String[] notes = {
		"do", "do#", "re", "re#", "mi", "fa", "fa#",
		"sol", "sol#", "la", "la#", "si"
	};

	/**
	 * Ridefinizione (overriding)
	 * del metodo toString() che avrei altrimenti
	 * ereditato da Note
	 */
	public String toString() {
		return notes[getSemitone()]; // sottinteso this.getSemitone()
	}
}
