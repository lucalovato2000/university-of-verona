package it.univr.figures;

public interface NoteWithDuration {
	public Duration getDuration();
}