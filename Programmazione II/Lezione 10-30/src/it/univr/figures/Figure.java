package it.univr.figures;

public abstract class Figure {

	public abstract double area();

	public abstract double perimeter();

	public boolean isBiggerThan(Figure other) {
		return area() > other.area();
	}
}