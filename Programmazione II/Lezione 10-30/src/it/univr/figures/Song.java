package it.univr.figures;

public class Song {
	
	// array
	// in the town where
	private final Note[] notes;
	private final String lyrics;

	/**
	 * @param lyrics è il testo della canzone (una riga)
	 */
	public Song(String lyrics) {
		this.lyrics = lyrics;
		this.notes = new Note[lyrics.length()];
	}

	/**
	 * Posiziona una nota sopra il carattere position dell'unica riga della canzone.
	 */
	public void place(Note note, int position) {
		notes[position] = note;
	}

	/**
	 * Restituisce una stringa di due righe: nella prima riga sono riportate le note posizionate nella canzone,
	 * nella seconda riga è riportato il testo della canzone.
	 */
	public String toString() {
		String firstRow = "";
		for (int pos = 0; pos < notes.length; pos += noteAsStringAt(pos).length())
			firstRow += noteAsStringAt(pos);
			
		return firstRow + "\n" + lyrics;
	}  

	private String noteAsStringAt(int position) {
		if (notes[position] == null)
			return " ";
		else
			return notes[position].toString();
	}
}