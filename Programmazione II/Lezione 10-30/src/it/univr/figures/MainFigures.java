package it.univr.figures;

public class MainFigures {

	public static void main(String[] args) {
		Figure[] array = new Figure[5];
		array[0] = new Rectangle(12.3, 5.6);
		array[1] = new Circle(5.6);
		array[2] = new Circle(4.3);
		array[3] = new Rectangle(11.3, 5.6);
		array[4] = new Rectangle(6.8, 5.8);
		
		for (Figure figure: array)
			System.out.println(figure.area());

		Figure reference = array[2];
		for (Figure figure: array)
			System.out.println("bigger? " + figure.isBiggerThan(reference));
	}

}
