package it.univr.figures;

public class MainSong {

	public static void main(String[] args) {
		Note[] array = new Note[3];
		array[0] = new ItalianNote(5);
		array[1] = new EnglishNote(6);
		array[2] = new ItalianNoteWithDuration(3, Duration.CROMA);
		NoteWithDuration[] array2 = new NoteWithDuration[6];
		
		//new Note(3); // no perché Note è abstract
		//new NoteWithDuration(); // no perché NoteWithDuration è un'interfaccia
		
		Song yellowSubmarine = new Song
				("In the town where I was born lived a man who sailed the sea");
		yellowSubmarine.place(new EnglishNote(1), 7);  // C# su "town"
		yellowSubmarine.place(new EnglishNote(1), 56); // C# su "sea"
		yellowSubmarine.place(new EnglishNote(6), 24); // F# su "born"
		yellowSubmarine.place(new EnglishNote(8), 37); // G# su "man"

		System.out.println(yellowSubmarine);
	}
}