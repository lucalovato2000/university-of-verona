package it.univr.soccer;

public class Main {
	
	public static void main(String[] args) {
		
		SoccerPlayer p1 = new GoalKeeper("Szczesny");
		SoccerPlayer p2 = new Defence("Chiellini");
		SoccerPlayer p3 = new Defence("Alex Sandro");
		SoccerPlayer p4 = new Defence("Rugani");
		SoccerPlayer p5 = new Defence("Dani Alves");
		SoccerPlayer p6 = new Midfield("Fabinho");
		SoccerPlayer p7 = new Midfield("Iniesta");
		SoccerPlayer p8 = new Midfield("Pjanic");
		SoccerPlayer p9 = new Forward("Dybala");
		SoccerPlayer p10 = new Forward("Higuain");
		SoccerPlayer p11 = new Forward("Bernardeschi");
		
		Formation f = new Formation433(new SoccerPlayer [] { 
				p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11});
		
		
		System.out.println(f);
	}
}
