package it.univr.soccer;

public class Formation433 extends Formation {
	
	public Formation433(SoccerPlayer[] players) {
		super(players);
	}
	
	@Override
	protected boolean isValid() {
		return super.isValid() && hasExactlyFourDefence() && hasExactlyThreeMidfield();
	}
	

	private boolean hasExactlyFourDefence() {
		
		int defences = 0;
		
		for (SoccerPlayer player: getPlayers()) {
			if (player instanceof Defence)
				defences++;
		}	
		return defences == 4;
	}
	
	private boolean hasExactlyThreeMidfield() {
		
		int midfields = 0;
		
		for (SoccerPlayer player: getPlayers()) {
			if (player instanceof Midfield)
				midfields++;
		}	
		return midfields == 3;
	}
}
