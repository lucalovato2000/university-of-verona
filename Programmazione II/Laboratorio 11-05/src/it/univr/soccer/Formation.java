package it.univr.soccer;

import java.util.Arrays;

public class Formation {

	private final SoccerPlayer[] players;	// final opzionale, array di SoccerPlayer

	public Formation(SoccerPlayer[] players) {

		this.players = players;

		if (!isValid())
			throw new IllegalArgumentException("invalid formation");
	}
	
	protected SoccerPlayer[] getPlayers() {
		return players;
	}
	
	protected boolean isValid() {
		// ritorna true se e solo se la formazione è fatta da 11 giocatori, di cui esattamente uno è un portiere
		int lengthplayers = players.length;

		if (lengthplayers == 11 && hasExactlyOneGoalKeeper()) {
			return true;
		} else {
			return false;	
		}
	}
	
	private boolean hasExactlyOneGoalKeeper() {
		int goalKeepers = 0;
		
		for (SoccerPlayer player: players) {
			if (player instanceof GoalKeeper)
				goalKeepers++;
		}	
		return goalKeepers == 1;
	}
	
	
	
	public final String toString() {
		// ritorna i nomi dei giocatori della formazione, separati da virgola
		
		/*String finalString = " ";

		for (int i = 0; i < players.length; i++) {
			finalString = players.toString() + ",";
		}
		return finalString;
		
		*/
		
		String result = Arrays.toString(players);
		return result;
	}
}