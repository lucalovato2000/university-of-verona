package it.univr.soccer;

public abstract class AbstractSoccerPlayer implements SoccerPlayer {

	private final String name;	// nome del giocatore final perche' non cambia

	protected AbstractSoccerPlayer(String name) {
		this.name = name;
	}

	public final String toString() {
		return ("Il nome del giocatore risulta: " + name + "\n");
	}

	// non è ancora implementato in AbstractSoccerPlayer, per cui tale classe deve essere abstract.
}