package it.univr.soccer;

public class MySelf implements SoccerPlayer {
	
	// la classe non è astratta perche' ho ridefinito tutti i suoi metodi
	
	@Override
	public String toString() {
		return "Luca";
	}
	
	@Override
	public boolean canUseHands() {
		return false;
	}
}
