package it.univr.soccer;

public class GoalKeeper extends AbstractSoccerPlayer {

	protected GoalKeeper (String name) {
		super(name);
	}
	
	@Override
	public boolean canUseHands() {
		return true;
	}
}
