package it.univr.calendar;

import java.util.Iterator;

public class Calendar implements Iterable<Calendar.Date> {
	private final int year; // l'anno del calendario

	public Calendar(int year) {
		// costruisce il calendario per l'anno indicato
		this.year = year;
	}

	@Override
	public Iterator<Date> iterator() {
		class MyIterator implements Iterator<Date> {
			private int current = 0;
			private Date end = getEnd();

			@Override
			public boolean hasNext() {
				return current <= end.daysFromStart; 
			}

			@Override
			public Date next() {
				return new Date(current++);
			}
		}

		return new MyIterator();
	}

	public boolean isLeapYear() {
		// determina se il calendario è per un anno bisestile
		return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);
	}

	public Date getStart() {
		// restituisce la prima data del calendario (primo gennaio)
		return new Date(0);
	}

	public Date getEnd() {
		// restituisce l'ultima data del calendario (31 dicembre)
		return new Date(isLeapYear() ? 365 : 364);
	}

	private final static int daysInMonth[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

	// classe interna
	public class Date {
		// 0 = primo gennaio, 364 = 31 dicembre (per i bisestili: 365 = 31 dicembre)
		private final int daysFromStart;

		private Date(int daysFromStart) {
			this.daysFromStart = daysFromStart;
		}

		/**
		 * Mi dice quanti giorni ci sono nel mese indicato (tra 1 e 12).
		 */
		private int daysInMonth(int month) {
			if (month == 2 && isLeapYear())
				return 29;
			else
				return daysInMonth[month - 1];
		}

		// ritorna il giorno di questa data, tra 1 e 31
		public int getDay() {
			int days = daysFromStart;
			int month = 1;
			while (days >= daysInMonth(month)) {
				days -= daysInMonth(month);
				month++;
			}

			return days + 1;
		}

		// ritorna il mese di questa data tra 1 e 12
		public int getMonth() {
			int days = daysFromStart;
			int month = 1;
			while (days >= daysInMonth(month)) {
				days -= daysInMonth(month);
				month++;
			}
				
			return month;
		}

		// ritorna l'anno di questa data
		public int getYear() {
			return year;
		}

		public String toString() {
			return String.format("%d/%d/%d", getDay(), getMonth(), getYear());
		}
	}
}