package it.univr.trees;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class BTree {
	private Node root;

	public void add(int value) {
		if (root == null)
			root = new Node(value);
		else
			root.add(value);
	}

	public int size() {
		if (root == null)
			return 0;
		else
			return root.size();
	}

	public void dumpAsXML(String fileName) throws FileNotFoundException {
		try (PrintWriter writer = new PrintWriter(fileName)) {
			writer.println("<tree>"); // prima riga
			if (root == null) {
				writer.println("  <node>");
				writer.println("  </node>");
			}
			else
				root.dumpAsXML(writer, "  ");

			writer.println("</tree>"); // ultima riga
		}
	}

	private static class Node {
		private final int value;
		private Node left;
		private Node right;

		public Node(int value) {
			this.value = value;
		}

		private void dumpAsXML(PrintWriter writer, String nesting) {
			writer.printf(nesting + "<node value = \"%d\">\n", value);

			String moreNesting = nesting + "  ";

			if (left != null)
				left.dumpAsXML(writer, moreNesting);
			else {
				writer.println(moreNesting + "<node>");
				writer.println(moreNesting + "</node>");
			}

			if (right != null)
				right.dumpAsXML(writer, moreNesting);
			else {
				writer.println(moreNesting + "<node>");
				writer.println(moreNesting + "</node>");
			}

			writer.printf(nesting + "</node>\n");

			/*
			<node value = "xyz" >
			  XML del nodo left
			  XML del nodo right
			</node>
			*/
		}

		private int size() {
			if (left != null && right != null)
				return left.size() + right.size() + 1;
			else if (left != null)
				return left.size() + 1;
			else if (right != null)
				return right.size() + 1;
			else
				return 1;
		}

		private void add(int added) {
			if (added < value)
				if (left == null)
					left = new Node(added);
				else
					left.add(added);
			else if (added > value)
				if (right == null)
					right = new Node(added);
				else
					right.add(added);
		}
	}
}