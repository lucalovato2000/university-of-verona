package it.univr.trees;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// crea un albero vuoto
		BTree tree = new BTree();
		
		// legge i valori da data/values.txt e li aggiunge all'albero;
		try (Scanner scanner = new Scanner(new FileReader("data/values.txt"))) {
			while (scanner.hasNextInt())
				tree.add(scanner.nextInt());
		}
		catch (FileNotFoundException e) {
			System.out.println("Il file data/values.txt non esiste");
			return;
		}

		// stampa sul video la dimensione dell'albero (cioè il numero dei suoi nodi)
		System.out.println("L'albero ha dimensione: " + tree.size());
		
		// scrive in un file di testo data/tree.xml la rappresentazione XML dell'albero
		try {
			tree.dumpAsXML("data/tree.xml");
		}
		catch (FileNotFoundException e) {
			System.out.println("Il file data/tree.xml non è scrivibile");
		}
	}

}
