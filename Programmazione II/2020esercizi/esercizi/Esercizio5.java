import java.time.Year;

/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa classe dentro il package it.univr.esame
 * 4) si completi questa classe dove indicato con TODO
 * 5) l'esecuzione del main deve stampare:
 * Fausto born in 1973
 * Fausto born in 1973
 * Samantha born in 1980
 * true
 */

public class Esercizio5 {

	public static void main(String[] args) {
		Person fausto = new Person("Fausto", 1973, "Fuffy", "Fido");
		Person fausto2 = new Person(new String("Fausto"), 1973, "Ermenegilda");
		Person samantha = new Person("Samantha", 1980, "Attila");
		System.out.println(fausto);
		System.out.println(fausto2);
		System.out.println(samantha);
		System.out.println(fausto.equals(fausto2));
	}

	public static class Person {
		private final String name;
		private final int yearOfBirth;
		private final String[] pets; // i nomi dei suoi animali domestici
		private final static int MINIMAL_AGE = 18;

		// pets sono i nomi dei suoi animali domestici
		public Person(String name, int yearOfBirth, String... pets) {
			//TODO: inizializzare i campi
		}

		@Override
		public String toString() {
			return name + " born in " + yearOfBirth;
		}

		public int getAge() {
			int currentYear = Year.now().getValue(); // l'anno in cui siamo adesso
			return currentYear - yearOfBirth;
		}

		public boolean canBuyAlcohol() {
			// TODO: completare: puo' comprare alcol se e' vecchio almeno MINIMAL_AGE
		}

		// aggiungere i metodi equals e hashCode
		// due Person sono equals se e solo se hanno name e yearOfBirth uguali
		// nota: i loro pets non sono usati per implementare equals
	}
}