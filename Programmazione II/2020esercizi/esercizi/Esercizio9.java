/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa classe dentro il package it.univr.esame
 * 4) si completi questa classe dove indicato con TODO
 * 5) l'esecuzione di questa classe deve stampare [ciao@@, buona@@notte, buon@giorno, buon@giornissimo, @hello]
 */

public class Esercizio9 {

	public static void main(String[] args) {
		String[] arr = { "ciao", "buon@giornissimo", "buon@giorno", "ciao@@",
				"@hello", "buona@@notte" , "buongiornissimo" };

		System.out.println(Arrays.toString(filterAndSort(arr))); // TODO: fate compilare
	}

	// TODO: deve restituire gli elementi di arr, in ordine alfabetico
	// decrescente, ma solo quelli che contengono il carattere @
	private String[] filterAndSort(String[] arr) {
		//TODO: implementare
		return null;
	}
}