/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa classe dentro il package it.univr.esame
 * 4) si completi questa classe dove indicato con TODO
 * 5) l'esecuzione di questa classe deve stampare 20.0 2.0 1000.0 e poi andare in eccezione
 */

public class Esercizio12 {

	public static void main(String[] args) {
		Vehicle[4] vehicles = { new Train(), new Bike(), new Taxi(), new Car() };
		
		// TODO: si iteri sugli elementi di vehicle chiamando il metodo float rent(int km) su
		// ciascun elemento con 200 come parametro e stampando il risultato
	}

	/*
	 * TODO: si crei dentro la classe Esercizio12 una gerarchia di classi interne statiche:
	 * - in cima c'e' Vehicle, astratta
	 * - sotto Vehicle ci sono Train, Bike e Car
	 * - sotto Car c'e' Taxi
	 * 
	 * Si aggiunga a questa gerarchia un metodo float rent(int km) che restituisce
	 * il costo in euro di un noleggio di km chilometri:
	 * - un Train costa un euro ogni 10 chilometri
	 * - un Bike costa un euro ogni 100 chilometri
	 * - un Taxi costa un euro ogni 200 metri
	 * - un Car non si puo' noleggiare e quindi lancia una java.lang.UnsupportedOperationException
	 */
}