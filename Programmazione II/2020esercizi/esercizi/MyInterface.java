/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa interfaccia dentro il package it.univr.esame
 * 4) si scriva dentro it.univr.esame un'implementazione "MyClass" di questa interfaccia
 */

import java.util.NoSuchElementException;

public interface MyInterface {

	/**
	 * Aggiunge un elemento.
	 * 
	 * @param s l'elemento da aggiungere
	 */
	void add(String s);

	/**
	 * Restituisce l'elemento piu' piccolo, in senso alfabetico,
	 * fra quelli di cui si e' fatto l'add.
	 *
	 * @return l'elemento piu' piccolo
	 * @throws NoSuchElementException se l'elemento piu' piccolo non esiste
	 */
	String first() throws NoSuchElementException;

	/**
	 * Restituisce l'elemento piu' grande, in senso alfabetico,
	 * fra quelli di cui si e' fatto l'add.
	 *
	 * @return l'elemento piu' grande
	 * @throws NoSuchElementException se l'elemento piu' grande non esiste
	 */
	String last() throws NoSuchElementException;
}