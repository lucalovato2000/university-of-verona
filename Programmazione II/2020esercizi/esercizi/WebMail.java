import java.util.List;

/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa interfaccia dentro il package it.univr.esame
 * 4) si scriva dentro it.univr.esame un'implementazione "WebMailImpl" di questa interfaccia.
 *    Dovete implementare i due suoi metodi in modo che send salvi da qualche parte il messaggio
 *    inviato e search cerchi fra i messaggi inviati in precedenza. Non c'è nessuna spedizione
 *    via rete, e' solo un esercizio di Java!
 * 5) l'esecuzione del main alla fine dovra' stampare:
 *    [Fausto: come va oggi?, Fausto: ho dimenticato l'ombrello]
 */

/*
 * Un webmail, dal quale e' possibile inviare e cercare messaggi.
 */
public interface WebMail {

	// invia il messaggio con questo WebMail
	void send(Message message);

	// restituisce la lista dei messaggi inviati dal sender,
	// tenuti nella lista in ordine di invio dal piu' vecchio al piu' nuovo
	List<Message> search(String sender);

	public static void main(String[] args) {
		WebMail wm = new WebMailImpl(); // dovete scriverla voi
		wm.send(new Message("come va oggi?", "Fausto"));
		wm.send(new Message("spero che non piova", "Giulia"));
		wm.send(new Message("ho dimenticato l'ombrello", "Fausto"));
		wm.send(new Message("la sveglia non ha suonato!", "Alessandro"));
		System.out.println(wm.search("Fausto"));
	}

	public static class Message {
		private final String text;
		private final String sender;

		public Message(String text, String sender) {
			this.text = text;
			this.sender = sender;
		}

		public String getText() {
			return text;
		}

		public String getSender() {
			return sender;
		}

		@Override
		public String toString() {
			return sender +": " + text;
		}
	}
}
