// Fatto 8/6/2020
import java.util.ArrayList;
import java.util.List;

/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa classe dentro il package it.univr.esame
 * 4) si completi questa classe dove indicato con TODO
 * 5) l'esecuzione del main deve stampare "numero foglie: 4"
 */

public class Esercizio3 {
	public static void main(String[] args) {
		Node tree = new Inner(new Inner(new Leaf(13), new Leaf(17)), new Inner(new Leaf(42)), new Leaf(-51));
		System.out.println("numero foglie: " + tree.countLeaves());
	}

	private interface Node {
		List<Node> children();
		int countLeaves(); // conta quante foglie ci sono nell'albero radicato in this
	}

	private static class Leaf implements Node {
		public final int value;

		private Leaf(int value) {
			this.value = value;
		}

		@Override
		public List<Node> children() {
			return new ArrayList<Node>();
		}

		//TODO: aggiungete countLeaves(): l'unica foglia e' se stesso
	}

	private static class Inner implements Node {
		private final List<Node> children;

		private Inner(Node... children) {
			//TODO: aggiungete tutti gli argomenti children al campo children (che va prima inizializzato!)
			//TODO: lancia una IllegalArgumentException se non c'e' nessun children
		}

		@Override
		public List<Node> children() {
			return children;
		}

		//TODO: aggiungete countLeaves(): le foglie sono quelle dei suoi figli (children)
	}
}