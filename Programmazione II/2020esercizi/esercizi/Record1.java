//TODO: aggiungete gli import

/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa classe dentro il package it.univr.esame
 * 4) si completi questa classe dove indicato con TODO
 * 5) l'esecuzione di questa classe deve stampare:
 * 
 * 1: [Gabriele, D'Annunzio] also known as il vate
 * 2: [Alessandro, Manzoni]
 * 3: [Camillo, Benso, conte, di, Cavour]
 */

public class Record1 {
	private final List<String> names; // obbligatorio, mai null
	private final int n;
	private final String nickname; // opzionale, potrebbe essere null

	public Record1(int n, String nickname, String... names) {
		// TODO: scrive i parametri dentro i campi omonimi
	}

	public Record1(int n) {
		// TODO: scrive il campo n, lascia nickname a null e scrive "Alessandro", "Manzoni" dentro names
		// TODO: per implementarlo dovete usare la concatenazione dei costruttori
	}

	public static void main(String[] args) {
		Record1 record1 = new Record1(1, "il vate", "Gabriele", "D'Annunzio");
		Record1 record2 = new Record1(2);
		Record1 record3 = new Record1(3, null, "Camillo", "Benso", "conte", "di", "Cavour");
		
		System.out.println(record1);
		System.out.println(record2);
		System.out.println(record3);
	}

	// TODO: implementate equals (stesso n, stessi names nello stesso ordine e stesso nickcname, se esiste)
	// TODO: implementate hashCode non banale

	@Override
	public String toString() {
		return n + ": " + names + (nickname != null ? " also known as " + nickname : "");
	}
}