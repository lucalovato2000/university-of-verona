import java.util.SortedSet;

/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa classe dentro il package it.univr.esame
 * 4) si completi questa classe dove indicato con TODO
 * 5) l'esecuzione di questa classe deve stampare [Bike, Taxi, Train, Train, Train]
 */

public class Esercizio13 {

	public static void main(String[] args) {
		Vehicle[] vehicles = { new Train(100), new Bike(), new Taxi(), new Train(150), new Train(78) };
		SortedSet<Vehicle> ss;

		// TODO: si aggiungano ad ss tutti gli elementi di vehicles, tenendoli in ordine
		// crescente per capacity

		System.out.println(ss);
	}

	/*
	 * TODO: si crei dentro la classe Esercizio13 una gerarchia di classi interne statiche:
	 * - in cima c'e' Vehicle, astratta
	 * - sotto Vehicle ci sono Train, Bike e Car
	 * - sotto Car c'e' Taxi
	 * 
	 * Si aggiunga a questa gerarchia un metodo int capacity() che restituisce
	 * il numero di persone che possono viaggiare contemporaneamente con il veicolo:
	 * - in un Bike puo' viaggiare una sola persona
	 * - in un Taxi possono viaggiare 4 persone
	 * - in un Car possono viaggiare 4 persone
	 * - in un Train possono viaggiare X persone, dove X e' passato al costruttore di Train
	 */
	
	public abstract static class Vehicle {
		//TODO

		@Override
		public String toString() {
			return this.getClass().getSimpleName(); // il nome della classe di "this"
		}
	}
}