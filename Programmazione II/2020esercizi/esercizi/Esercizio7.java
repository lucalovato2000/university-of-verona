/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa classe dentro il package it.univr.esame
 * 4) si completi questa classe dove indicato con TODO
 */

public class Esercizio7 {

	public static void main(String[] args) {
		Time t1 = new Time24(9, 12, 5);
		System.out.println(t1);
		Time t2 = new Time12(9, 12, 5, When.MORNING);
		System.out.println(t2);
		Time t3 = new Time12(1, 5, 55, When.AFTERNOON);
		System.out.println(t3);
	}

	public abstract static class Time {
		// TODO: si aggiunga un metodo astratto chiamato next
		// che ritorna il Time successivo a questo Time (un secondo dopo)
		// non deve modificare questo Time
	}

	public static class Time24  extends Time {
		private final int hh, mm, ss;

		// riceve ore (hh, 0-23), minuti (mm) e secondi (ss)
		private Time24(int hh, int mm, int ss) {
			this.hh = hh;
			this.mm = mm;
			this.ss = ss;
		}

		@Override
		public String toString() {
			return String.format("%02d:%02d:%02d", hh, mm, ss);
		}

		//TODO: implementate next
	}

	private enum When { MORNING, AFTERNOON };
	
	public static class Time12 extends Time {
		private final int hh, mm, ss;
		private final When when;

		// riceve ore (hh, 0-11), minuti (mm), secondi (ss) e fase della giornata (when)
		private Time12(int hh, int mm, int ss, When when) {
			this.hh = hh;
			this.mm = mm;
			this.ss = ss;
			this.when = when;
		}

		@Override
		public String toString() {
			return String.format("%02d:%02d:%02d %s", hh, mm, ss, when);
		}

		// TODO: implementate next
	}
}