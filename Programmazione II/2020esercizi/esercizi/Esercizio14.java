import java.util.HashMap;
import java.util.Map;

/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa classe dentro il package it.univr.esame
 * 4) si completi questa classe dove indicato con TODO
 * 5) l'esecuzione di questa classe deve stampare qualcosa del tipo:
 * 
 * party1: 5 votes (38%)
 * party4: 2 votes (15%)
 * party3: 3 votes (23%)
 * party2: 3 votes (23%)
 *
 * party1: 7 votes (53%)
 * party3: 3 votes (23%)
 * party2: 3 votes (23%)
 */

public class Esercizio14 {

	// TODO: si scriva una classe interna statica ElectionCheat, sottoclasse di Election, il cui metodo vote
	// sposta i voti di "party4" su "party1": se qualcuno vota per "party4", il voto viene dato a "party1";
	// per gli altri partiti si comporta come nella superclasse

	public static void main(String[] args) {
		/*
		 * TODO
		 * Si crei una Election, la si passi a registerVotes e la si stampi su video
		 * Si crei una ElectionCheat, la si passi a registerVotes e la si stampi sul video
		 */
	}

	private static void registerVotes(Election election) {
		election.vote("party1");
		election.vote("party4");
		election.vote("party3");
		election.vote("party3");
		election.vote("party1");
		election.vote("party1");
		election.vote("party2");
		election.vote("party3");
		election.vote("party2");
		election.vote("party1");
		election.vote("party1");
		election.vote("party2");
		election.vote("party4");
	}

	public static class Election {
		// i voti, per ciascun partito
		private final Map<String, Integer> votes = new HashMap<>();

		public void vote(String party) {
			// TODO. aggiunge un voto per "party"
		}

		@Override
		public String toString() {
			String result = "";
			int total = votes.values().stream().mapToInt(Integer::intValue).sum();

			for (String party: votes.keySet())
				result += party + ": " + votes.get(party) + " votes (" + votes.get(party) * 100 / total + "%)\n";

			return result;
		}
	}
}