/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa classe dentro il package it.univr.esame
 * 4) si completi questa classe dove indicato con TODO,
 *    facendo in modo che tutto compili senza errori
 * 5) l'esecuzione del main deve stampare:
 * 
 * 04:30:00: NOTTE
 * 05:06:09: MATTINO
 * 13:15:59: GIORNO
 * 20:25:08: SERA
 */

public class Orario implements Comparable<Orario> {
	private final int ore;
	private final int minuti;
	private final int secondi;

	public static void main(String[] args) {
		Orario t1 = new Orario(13, 15, 59);
		Orario t2 = new Orario(5, 6, 9);
		Orario t3 = new Orario(20, 25, 8);
		Orario t4 = new Orario(4, 30, 0);

		SortedSet<Orario> ss; // TODO: inizializzate
		ss.add(t1);
		ss.add(t2);
		ss.add(t3);
		ss.add(t4);

		for (Orario t: ss)
			System.out.println(t + ": " + t.getFase());
	}

	public Orario(int ore, int minuti, int secondi) {
		this.ore = ore;
		this.minuti = minuti;
		this.secondi = secondi;
	}

	@Override
	public int hashCode() {
		return ore ^ minuti ^ secondi;
	}

	//TODO: aggiungete equals e compareTo (che ordina in ordine cronologico crescente)

	@Override
	public String toString() {
		return String.format("%02d:%02d:%02d", ore, minuti, secondi);
	}

	// TODO: definite un'enumerazione Fase con cinque elementi: 
	// MATTINO dalle 05:00:00 alle 10:29:59
	// GIORNO dalle 10:30:00 alle 14:29:59
	// POMERIGGIO dalle 14:30:00 alle 18:59:59
	// SERA dalle 19:00:00 alle 22:39:59
	// NOTTE dalle 22:40:00 alle 04:59:59

	public Fase getFase() {
		// TODO: ritornate la fase di questo orario
	}
}