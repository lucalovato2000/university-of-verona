import java.util.SortedSet;
import java.util.TreeSet;

/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa classe dentro il package it.univr.esame
 * 4) si completi questa classe dove indicato con TODO
 * 5) l'esecuzione del main deve stampare:
 * [Poesie scelte of height 22, Il nome della rosa of height 25, Il Decamerone of height 30]
 */

// TODO: rendere questa classe Comparable, in modo che i Book
// vengano ordinate prima per height e poi per title
public class Book {
	private final String title;
	private final int height;

	public Book(String title, int height) {
		this.title = title;
		this.height = height;
	}

	@Override
	public String toString() {
		return title + " of height " + height;
	}

	// TODO: scrivere il metodo equals: due Book sono equals
	// se e solo se hanno stesso title e stessa height

	public static void main(String[] args) {
		Book book2 = new Book(new String("Il Decamerone"), 30);
		Book book3 = new Book("Il nome della rosa", 25);
		Book book1 = new Book("Il Decamerone", 30);
		Book book4 = new Book("Poesie scelte", 22);
		SortedSet<Book> set = new TreeSet<>();
		set.add(book1);
		set.add(book4);
		set.add(book2);
		set.add(book3);
		System.out.println(set);
	}
}