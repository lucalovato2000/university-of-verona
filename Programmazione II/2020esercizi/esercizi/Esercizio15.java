import java.util.HashMap;
import java.util.Map;

/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa classe dentro il package it.univr.esame
 * 4) si completi questa classe dove indicato con TODO
 * 5) l'esecuzione di questa classe deve stampare qualcosa del tipo:
 * 
 * party1: 5 votes (38%)
 * party4: 2 votes (15%)
 * party3: 3 votes (23%)
 * party2: 3 votes (23%)
 * the winner is party1
 *
 * party1: 5 votes (31%)
 * party4: 2 votes (12%)
 * party3: 6 votes (37%)
 * party2: 3 votes (18%)
 * the winner is party3
 */

public class Esercizio15 {

	// TODO: si scriva classe interna statica ElectionCheat, sottoclasse di Election, il cui metodo vote
	// conta due volte i voti per "party3": se uno vota "party3", registra due voti per "party3";
	// per gli altri partiti si comporta come nella superclasse

	public static void main(String[] args) {
		/*
		 * TODO
		 * Si crei una Election, la si passi a registerVotes e la si stampi su video
		 * Si crei una ElectionCheat, la si passi a registerVotes e la si stampi sul video
		 */
	}

	private static void registerVotes(Election election) {
		election.vote("party1");
		election.vote("party4");
		election.vote("party3");
		election.vote("party3");
		election.vote("party1");
		election.vote("party1");
		election.vote("party2");
		election.vote("party3");
		election.vote("party2");
		election.vote("party1");
		election.vote("party1");
		election.vote("party2");
		election.vote("party4");
	}

	public static class Election {
		private final Map<String, Integer> votes = new HashMap<>();

		// aggiunge un voto a party
		public void vote(String party) {
			Integer old = votes.get(party);
			if (old == null)
				votes.put(party, 1);
			else
				votes.put(party, old + 1);
		}

		@Override
		public String toString() {
			String result = "";
			int total = votes.values().stream().mapToInt(Integer::intValue).sum();

			for (String party: votes.keySet())
				result += party + ": " + votes.get(party) + " votes (" + votes.get(party) * 100 / total + "%)\n";

			//TODO si aggiunga a result la riga "the winner is X" dove X e' il nome del partito che ha preso piu' voti

			return result;
		}
	}
}