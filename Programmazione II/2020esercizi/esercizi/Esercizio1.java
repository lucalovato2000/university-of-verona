//Fatto 8/6/2020
/**
 * 1) si crei un progetto Java "esame" dentro Eclipse
 * 2) si crei un package it.univr.esame dentro il progetto "esame"
 * 3) si copi questa classe dentro il package it.univr.esame
 * 4) si modifichi questa classe aggiungendo il metodo pubblico sum dentro Node
 *    in modo che calcoli la somma dei valori nel Node e nei suoi successori
 * 5) l'esecuzione del main deve stampare 72 due volte
 * 6) si sposti la classe Node dentro Esercizio1, come classe privata statica,
 *    eliminado i suoi due metodi getValue e getNext
 */

public class Esercizio1 {
	public static void main(String[] args) {
		Node n1 = new Node(13, new Node(17, new Node(42, null)));
		System.out.println(n1.getValue() + n1.getNext().getValue() + n1.getNext().getNext().getValue());
		System.out.println(n1.sum());
	}
}

class Node {
	private int value;
	private Node next;

	Node(int value, Node next) {
		this.value = value;
		this.next = next;
	}

	int getValue() {
		return value;
	}

	Node getNext() {
		return next;
	}
}
