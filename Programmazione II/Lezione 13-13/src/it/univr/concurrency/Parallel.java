package it.univr.concurrency;

import java.util.concurrent.atomic.AtomicLong;

public class Parallel {
	public final static int HOW_MANY = 100_000_000;
	public final static int NUM_THREADS = 4;
	private AtomicLong nice = new AtomicLong(0), ugly = new AtomicLong(0);

	public Parallel() throws InterruptedException {
		Worker[] workers = new Worker[NUM_THREADS];
		for (int pos = 0; pos < NUM_THREADS; pos++)
			workers[pos] = new Worker();

		for (Worker worker: workers)
			worker.start(); // manda in esecuzione parallela il thread

		for (Worker worker: workers)
			worker.join(); // aspetto che il thread finisca
	}

	private void increase() {
		// read then modify => race condition
		// deve essere una sezione critica in mutua esclusione
		nice.getAndIncrement();
		ugly.getAndIncrement();
	}

	private class Worker extends Thread {

		@Override
		public void run() {
			for (int i = 1; i <= HOW_MANY / NUM_THREADS; i++)
				increase();
		}
	}

	public static void main(String[] args) throws InterruptedException {
		long start = System.currentTimeMillis();
		Parallel parallel = new Parallel();
		long end = System.currentTimeMillis();
		System.out.println("nice = " + parallel.nice + ", ugly = " + parallel.ugly);
		System.out.println("Time elapsed: " + (end - start) + " ms");
	}
}
