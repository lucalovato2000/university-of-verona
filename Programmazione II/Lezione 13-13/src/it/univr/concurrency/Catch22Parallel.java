package it.univr.concurrency;

import java.util.concurrent.atomic.AtomicInteger;

public class Catch22Parallel {
	public final int NUM_THREADS = Runtime.getRuntime().availableProcessors();
	private AtomicInteger candidate = new AtomicInteger(1);
	private AtomicInteger counter = new AtomicInteger(22);

	private Catch22Parallel() throws InterruptedException {
		Worker[] workers = new Worker[NUM_THREADS];
		for (int pos = 0; pos < NUM_THREADS; pos++)
			workers[pos] = new Worker();

		for (Worker worker: workers)
			worker.start();

		for (Worker worker: workers)
			worker.join();
	}

	private class Worker extends Thread {
	
		@Override
		public void run() {
			while (counter.get() > 0) {
				int x = candidate.getAndIncrement();
				if (Narcissistic.isNarcissistic(x)) {
					System.out.println(x);
					counter.getAndDecrement();
				}
			}
		}
	}

	public static void main(String[] args) throws InterruptedException {
		long start = System.currentTimeMillis();
		for (int loop = 1; loop <= 22; loop++)
			new Catch22Parallel();
		long end = System.currentTimeMillis();
		System.out.println("Tempo impiegato: " + (end - start));
	}
}