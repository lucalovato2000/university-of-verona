package it.univr.concurrency;

public class Catch22 {

	private Catch22() {
		for (int candidate = 1, counter = 22; counter > 0; candidate++)
			if (Narcissistic.isNarcissistic(candidate)) {
				System.out.println(candidate);
				counter--;
			}
	}

	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		for (int loop = 1; loop <= 22; loop++)
			new Catch22();
		long end = System.currentTimeMillis();
		System.out.println("Tempo impiegato: " + (end - start));
	}
}