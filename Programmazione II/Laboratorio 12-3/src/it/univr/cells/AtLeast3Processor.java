package it.univr.cells;

public class AtLeast3Processor implements NextAliveProcessor {
	private final Board board;

	public AtLeast3Processor(Board board) {
		this.board = board;
	}

	@Override
	public boolean isAliveNextAt(int x, int y) {
		// guardo dentro board intorno a (x,y) e conto quante celle
		// vive ci sono; ritorno true se e solo se sono almeno 3
		int counter = 0;
		for (int xx = x - 1; xx <= x + 1; xx++)
			for (int yy = y - 1; yy <= y + 1; yy++)
				if (x != xx || y != yy)
					if (xx >= 0 && yy >= 0 && xx < board.getWidth() && yy < board.getHeight())
						if (board.isAliveAt(xx, yy))
							counter++;

		return counter >= 3;
	}
}
