package it.univr.cells;

public class MainConstant {

	public static void main(String[] args) throws InterruptedException {
		Board board = new Board(40, 20, 100);
		NextAliveProcessor processor = new ConstantProcessor(board);
		board.play(processor);
	}
}