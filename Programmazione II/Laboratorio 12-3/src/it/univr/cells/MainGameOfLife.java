package it.univr.cells;

public class MainGameOfLife {

	public static void main(String[] args) throws InterruptedException {
		Board board = new Board(20000, 20000, 100);
		NextAliveProcessor processor = new GameOfLifeProcessor(board);
		board.play(processor);
	}
}