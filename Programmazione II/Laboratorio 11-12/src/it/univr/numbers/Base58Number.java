package it.univr.numbers;

public class Base58Number extends AbstractNumber {
	
	public Base58Number(int value) {
		super(value);
	}

	@Override
	protected int getBase() {
		return 58;
	}

	@Override
	protected char getCharForDigit(int digit) {
		return Character.forDigit(digit, getBase());
	}
}
