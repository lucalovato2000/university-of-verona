package it.univr.numbers;

public class BinaryNumberWithParity extends BinaryNumber {

	public BinaryNumberWithParity(int value) {
		super(value);
	}

	@Override
	public String toString() {
		String s = super.toString();
		return s + parity(s);
	}

	private int parity(String s) {
		int ones = 0;
		for (int pos = 0; pos < s.length(); pos++)
			if (s.charAt(pos) == '1')
				ones++;

		return ones % 2;
	}
}
