package it.univr.numbers;

public class BinaryNumber extends AbstractNumber {
	
	public BinaryNumber(int value) {
		super(value);
	}

	@Override
	protected int getBase() {
		return 2;
	}

	@Override
	protected char getCharForDigit(int digit) {
		return "01".charAt(digit);
		//return Character.forDigit(digit, getBase());
	}
}
