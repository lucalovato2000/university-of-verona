package it.univr.rent;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class Agency {

	private final String name;

	// dentro <> va Model perchè costruttore richiede un Model ..
	private final Set <Model> agenziaLuca = new TreeSet<>();
	private final Set <Model> result = new HashSet<>();
	private final Map <Model, Integer> rentings = new HashMap<>();


	/**
	 * Crea un'agenzia di noleggio col nome indicato,
	 * che fornisce in noleggio i modelli indicati.
	 * 
	 * @param fleet i modelli noleggiabili con l'agenzia
	 * @throws IllegalArgumentException se fleet e' vuoto
	 */
	public Agency(String name, Model... fleet) {
		if (fleet.length == 0) {
			throw new IllegalArgumentException("Agenzia deve avere almeno un veicolo");
		}

		this.name = name;

		for (Model model: fleet) {
			this.agenziaLuca.add(model);
			this.rentings.put(model, 0);	// in map put() equivale ad add() 
		}
	}

	/**
	 * Restituisce l'insieme dei modelli noleggiabili con questa agenzia
	 * e con la patente indicata.
	 * 
	 * @param license la patente
	 */
	public Set<Model> modelsAvailableForLicense(License license) {
		
		for (Model model: agenziaLuca) {
			if (model.canBeDrivenWith(license) == true) {
				result.add(model);
			}
		}
		return result;
	}

	/**
	 * Pende nota che qualcuno ha noleggiato con questa agenzia un dato modello,
	 * per una certa quantita' di giorni, usando la patente indicata.
	 * 
	 * @param model il modello noleggiato
	 * @param license la patente
	 * @param days il numero di giorni di noleggio
	 * @throws IllegalLicenseException se il modello non si puo' guidare
	 *                                 con la patente indicata
	 * @throws ModelNotAvailableException se il modello non e' fra quelli
	 *                                    noleggiabili con questa agenzia
	 */
	public void rent(Model model, License license, int days) {
			
		if (model.canBeDrivenWith(license) == false) {
			throw new IllegalLicenseException();
		}if (agenziaLuca.contains(model) == false){
			throw new ModelNotAvailableException();
		}	
		
		rentings.put(model, rentings.get(model) + days);
	}
	

	/**
	 * Restituisce il modello che e' stato noleggiato in totale per piu' giorni
	 * con questa agenzia.
	 */
	public Model mostRented() {
		
		Model result = null;
		for (Model model: agenziaLuca)
			if (result == null || rentings.get(model) > rentings.get(result))
				result = model;

		return result;
	}

	/**
	 * Restituisce una stringa con in prima riga il nome dell'agenzia e,
	 * sotto di essa, la lista dei modelli noleggiabili con questa agenzia,
	 * ordinati secondo l'ordinamento fra i modelli.
	 */
	@Override
	public String toString() {
		
		String result = "Agency " + name + ". Available models:";
		int length = result.length();
		result += "\n";

		while (result.length() <= length * 2)
			result += "-";

		result += "\n";

		for (Model model: agenziaLuca)
			result += model.getName() + "\t" + model.pricePerDay() + " euros per day\n";

		return result;
	}
}