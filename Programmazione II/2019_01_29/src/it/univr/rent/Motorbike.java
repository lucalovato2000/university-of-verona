package it.univr.rent;

public class Motorbike extends AbstractModel {
	
	protected Motorbike(String name, int pricePerDay) {
		super(name, pricePerDay);
	}

	@Override
	public boolean canBeDrivenWith(License license) {
		return license == License.A;
	}

	@Override
	public boolean equals(Object other) {
		return other instanceof Motorbike && getName() == ((Motorbike)other).getName() && pricePerDay() == ((Motorbike)other).pricePerDay();
	}
}
