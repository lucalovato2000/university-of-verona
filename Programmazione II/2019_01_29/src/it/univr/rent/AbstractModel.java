package it.univr.rent;

public abstract class AbstractModel implements Model {
	
	private final String name;
	private final int pricePerDay;
	
	/**
	 * Crea un modello col nome e prezzo giornaliero indicato.
	 * 
	 * @param name il nome
	 * @param pricePerDay il prezzo giornaliero
	 */
	protected AbstractModel(String name, int pricePerDay) {
		this.name = name;
		this.pricePerDay = pricePerDay;
	}

	/**
	 * Restituisce il nome di questo modello.
	 */
	@Override
	public final String getName() {
		return name;
	}

	/**
	 * Restituisce il prezzo giornaliero di noleggio di questo modello.
	 */
	@Override
	public final int pricePerDay() {
		return pricePerDay;
	}

	/**
	 * Restituisce il nome di questo modello.
	 */
	@Override
	public String toString() {
		return String.format("%s", name);
	}

	/**
	 * I modelli sono ordinati per prezzo di noleggio crescente. A parita'
	 * di prezzo di noleggio, sono ordinati alfabeticamente per nome.
	 */
	@Override
	public final int compareTo(Model other) {
		if (pricePerDay > other.pricePerDay()) {
			return 1;
		}else if(other.pricePerDay() > pricePerDay) {
			return -1;
		}else {
			if(name.compareTo(other.getName()) > 0) {
				return 1;
			}else if (name.compareTo(other.getName()) < 0) {
				return -1;
			}else {
				return 0;
			}
		}
	}

	/**
	 * Due modelli sono uguali se appartengono alla stessa
	 * classe (due Car, due Motorbike...) e se hanno stesso nome
	 * e stesso prezzo di noleggio giornaliero. Si noti che questo metodo
	 * e' abstract, quindi dove verra' implementato?
	 */
	@Override
	public abstract boolean equals(Object other);

	@Override
	public int hashCode() {
		return pricePerDay ^ name.hashCode();
	}
}
