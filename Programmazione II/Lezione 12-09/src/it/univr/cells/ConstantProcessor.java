package it.univr.cells;

public class ConstantProcessor implements NextAliveProcessor {
	private final Board board;

	public ConstantProcessor(Board board) {
		this.board = board;
	}

	@Override
	public boolean isAliveNextAt(int x, int y) {
		return board.isAliveAt(x, y);
	}
}
