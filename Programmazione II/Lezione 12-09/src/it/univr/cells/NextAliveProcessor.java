package it.univr.cells;

public interface NextAliveProcessor {
	boolean isAliveNextAt(int x, int y);
}