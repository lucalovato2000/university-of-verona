package it.univr.cells;

public class MainConstant {

	public static void main(String[] args) throws InterruptedException {
		Board board = new Board(40, 20, 100);
		NextAliveProcessor processor = new ConstantProcessor(board);
		board.play(processor);
	}
	
	// nome completo: it.univr.cells.MainConstant.ConstantProcessor
	private static class ConstantProcessor implements NextAliveProcessor {
		private final Board board;

		public ConstantProcessor(Board board) {
			this.board = board;
		}

		@Override
		public boolean isAliveNextAt(int x, int y) {
			return board.isAliveAt(x, y);
		}
	}
}