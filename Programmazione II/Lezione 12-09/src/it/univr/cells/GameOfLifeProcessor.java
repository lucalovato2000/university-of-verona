package it.univr.cells;

public class GameOfLifeProcessor implements NextAliveProcessor {
	private final Board board;

	public GameOfLifeProcessor(Board board) {
		this.board = board;
	}

	@Override
	public boolean isAliveNextAt(int x, int y) {
		// guardo dentro board intorno a (x,y) e conto quante celle
		// vive ci sono; ritorno true secondo le regole del game of life
		int counter = 0;
		for (int xx = x - 1; xx <= x + 1; xx++)
			for (int yy = y - 1; yy <= y + 1; yy++)
				if (x != xx || y != yy)
					if (xx >= 0 && yy >= 0 && xx < board.getWidth() && yy < board.getHeight())
						if (board.isAliveAt(xx, yy))
							counter++;

		if (board.isAliveAt(x, y) && counter < 2)
			return false;
		if (board.isAliveAt(x, y) && counter >= 2 && counter <= 3)
			return true;
		if (board.isAliveAt(x, y) && counter > 3)
			return false;
		if (!board.isAliveAt(x, y) && counter == 3)
			return true;

		return false;
	}
}
