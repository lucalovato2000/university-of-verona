package it.univr.files;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;

public class CopyText2 {

	public static void main(String[] args) {
		try (Reader reader = new BufferedReader(new FileReader("data/pippo.txt"));
			 PrintWriter writer = new PrintWriter("data/pippo_copy.txt")) {

			int c = reader.read(); // ritorna -1 se il file è finito
			int pos = 0;
			while (c != -1) {
				writer.printf("%d - \"%c\"\n", pos++, c);
				c = reader.read();
			}
		}
		catch (FileNotFoundException e) {
			System.out.println("Non riesco ad aprire data/pippo.txt");
		}
		catch (IOException e) {
			System.out.println("Problema di I/O");
		}
	}
}