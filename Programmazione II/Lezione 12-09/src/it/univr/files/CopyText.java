package it.univr.files;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

public class CopyText {

	public static void main(String[] args) {
		try (Reader reader = new BufferedReader(new FileReader("data/pippo.txt"));
			 Writer writer = new BufferedWriter(new FileWriter("data/pippo_copy.txt"))) {

			int c = reader.read(); // ritorna -1 se il file è finito
			while (c != -1) {
				writer.write(c);
				c = reader.read();
			}
		}
		catch (FileNotFoundException e) {
			System.out.println("Non riesco ad aprire data/pippo.txt");
		}
		catch (IOException e) {
			System.out.println("Problema di I/O");
		}
	}
}