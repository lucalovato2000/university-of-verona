package it.univr.files;

import java.util.Scanner;

public class Input {
	public static void main(String[] args) {
		try (Scanner keyboard = new Scanner(System.in)) {
			System.out.print("Inserisci un intero: ");
			int i = keyboard.nextInt();
			System.out.println("Hai inserito " + i);
		}
	}
}
