package it.univr.files;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class ReadText {

	public static void main(String[] args) {
		// try with resource: chiuse reader SEMPRE alla fine
		try (Reader reader = new BufferedReader(new FileReader("data/pippo.txt"))) {
			int c = reader.read(); // ritorna -1 se il file è finito
			while (c != -1) {
				System.out.print((char) c);
				c = reader.read();
			}
		}
		catch (FileNotFoundException e) {
			System.out.println("Non riesco ad aprire data/pippo.txt");
		}
		catch (IOException e) {
			System.out.println("Problema di I/O");
		}
	}
}