package it.univr.files;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Scanner;

public class ReadLines {

	public static void main(String[] args) {
		try (Reader reader = new BufferedReader(new FileReader("data/pippo.txt"));
			 Scanner scanner = new Scanner(reader)) {

			int line = 0;
			while (scanner.hasNext())
				System.out.println(line++ + ": " + scanner.next());
		}
		catch (FileNotFoundException e) {
			System.out.println("Non riesco ad aprire data/pippo.txt");
		}
		catch (IOException e) {
			System.out.println("Problema di I/O");
		}
	}
}