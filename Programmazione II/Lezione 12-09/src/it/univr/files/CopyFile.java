package it.univr.files;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class CopyFile {

	public static void main(String[] args) {
		try (InputStream reader = new BufferedInputStream(new FileInputStream("data/pippo.txt"));
			 OutputStream writer = new BufferedOutputStream(new FileOutputStream("data/pippo_copy.txt"))) {

			int b = reader.read(); // ritorna -1 se il file è finito
			while (b != -1) {
				writer.write(b);
				b = reader.read();
			}
		}
		catch (FileNotFoundException e) {
			System.out.println("Non riesco ad aprire data/pippo.txt");
		}
		catch (IOException e) {
			System.out.println("Problema di I/O");
		}
	}
}