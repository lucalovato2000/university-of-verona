package it.univr;

public class MainDate {
	
	public static void main(String[] args) {
		
		Date d1 = new Date(7, 10, 2020);	// Nuovo oggetto di classe Date
		Date d2 = new Date(13, 1, 1973);
	
		System.out.println(d1==d2);
		System.out.println(d1.toString());
		
		// System.out.println(d2); Aggiunge automaticamente toString();
		System.out.println(d2.toString());
		
		System.out.println("d1 = " + d1);	// Sottinteso d1.toString()
		System.out.println("d2 = " + d2.toString());
		
		//d1.day++; //errore
		//d1.month++; errore perch� month PRIVATE
		
		Date d3 = new Date(13, 1, 1973);
		System.out.println("d2 e' uguale a d3? " + (d2 == d3));
		
		// d3 � un parametro attuale passato ad equals
		// un parametro attuale � il valore di un'espressione
		System.out.println("d2 e' uguale a d3? " + (d2.equals(d3)));
		
		//System.out.println("d2 viene prima di d3? " + (d2 < d3)); non compila
		System.out.println("d2 viene prima di d3? " + (d2.compareTo(d3)));
		// Dipende dall'implementazione, posso supporre se < 0 ; > 0; == 0
	}
}
