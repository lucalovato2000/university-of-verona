package it.univr.cards;

public enum Ranking {
	// in ordine di ranking crescente
	
	FLUSH,
	THREEOFKIND,
	STRAIGHT,
	FULLHOUSE,
	COLOR,
	FOUROFKIND,
	STRAIGHTFLUSH
}
