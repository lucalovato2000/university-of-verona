package it.univr.cards;

public class MainDeck {

	public static void main(String[] args) {
		Deck d = new Deck(15, Value._7);
		System.out.println(d);

		System.out.println("adesso itero sul Deck");
		for (Card card: d)
			System.out.println(card);
	}
}