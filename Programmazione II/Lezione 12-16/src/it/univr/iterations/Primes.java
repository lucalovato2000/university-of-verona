package it.univr.iterations;

import java.util.Iterator;

public class Primes implements Iterable<Integer> {

	@Override
	public Iterator<Integer> iterator() {
		class MyIterator implements Iterator<Integer> {

			private int current = 2;

			@Override
			public boolean hasNext() {
				return true; // i numeri primi sono infiniti
			}

			@Override
			public Integer next() {
				int prime = current;
				// sposto current in modo che diventi il prossimo numero primo
				for (++current; !isPrime(current); current++);

				return prime;
			}

			private boolean isPrime(int n) {
				for (int d = 2; d < n; d++)
					if (n % d == 0)
						return false;

				return true;
			}
		}

		return new MyIterator();
	}

	public static void main(String[] args) {
		Primes primes = new Primes();
		for (int prime: primes)
			System.out.println(prime);
	}
}