package it.univr.iterations;

import java.util.Iterator;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		System.out.println("for each su un array");
		String[] arr = { "ciao", "amico", "come", "va?" };
		for (String s: arr)
			System.out.println(s);

		System.out.println("for each su un Iterable");
		List<String> list = List.of("Nel", "mezzo", "del", "cammin", "di", "nostra", "vita");
		/*
		for (String s: list) // questo è quello che scriviamo noi
			System.out.println(s);
			*/

		// il for-each sopra viene trasformato dal compilatore in questo codice:
		Iterator<String> it = list.iterator();
		while (it.hasNext()) {
			String s = it.next();
			System.out.println(s);
		}
	}
}