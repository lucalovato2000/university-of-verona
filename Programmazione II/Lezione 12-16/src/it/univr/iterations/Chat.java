package it.univr.iterations;

import java.util.Iterator;

public class Chat implements Iterable<Chat.Message> {
	public final static int MAX = 10;
	private final Message[] messages = new Message[MAX];
	private int pos = 0;

	public void send(String author, String text) {
		if (pos < MAX)
			messages[pos++] = new Message(author, text);
	}

	@Override
	public Iterator<Message> iterator() {
		class MyIterator implements Iterator<Message> {
			private int cursor = 0;

			@Override
			public boolean hasNext() {
				return cursor < pos;
			}

			@Override
			public Message next() {
				return messages[cursor++];
			}
		}

		return new MyIterator();
	}

	public static class Message {
		public final String author;
		public final String text;
		
		private Message(String author, String text) {
			this.author = author;
			this.text = text;
		}
	}

	public static void main(String[] args) {
		Chat chat = new Chat();
		chat.send("Fausto", "Come va oggi?");
		chat.send("Giorgio", "fa freddo");
		chat.send("Fausto", "però c'è il sole");

		for (Message msg: chat)
			System.out.println(msg.author + ": " + msg.text);
	}
}
