package it.univr.iterations;

import java.util.Iterator;

public class Prefixes implements Iterable<String> {
	private final String word;

	public Prefixes(String word) {
		this.word = word;
	}

	@Override
	public Iterator<String> iterator() {
		class MyIterator implements Iterator<String> {
			// il prossimo prefisso, non ancora iterato
			private String current = word;

			@Override
			public boolean hasNext() {
				return !current.isEmpty();
			}

			@Override
			public String next() {
				String next = current;
				// tolgo l'ultimo carattere da current
				current = current.substring(0, current.length() - 1);
				return next;
			}
		}

		return new MyIterator();
	}

	public static void main(String[] args) {
		Prefixes prefixes = new Prefixes("precipitevolissimevolmente");
		for (String prefix: prefixes)
			System.out.println(prefix);
	}
}