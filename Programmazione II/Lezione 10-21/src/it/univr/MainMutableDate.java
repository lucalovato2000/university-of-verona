package it.univr;

public class MainMutableDate {

	public static void main(String[] args) {
		MutableDate d1 = new MutableDate(21, 10, 2020);
		MutableDate copy = d1;

		// chiamo un metodo di mutazione
		d1.increase();

		// la mutazione effettua un side-effect anche su copy
		System.out.println("d1 = " + d1);
		System.out.println("copy = " + copy);
	}
}