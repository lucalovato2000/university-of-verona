package it.univr.collections;

import java.util.PriorityQueue;
import java.util.Queue;

import it.univr.notes.Duration;
import it.univr.notes.EnglishNote;
import it.univr.notes.EnglishNoteWithDuration;
import it.univr.notes.ItalianNote;
import it.univr.notes.Note;

public class MainQueue2 {

	public static void main(String[] args) {
		Queue<Note> q = new PriorityQueue<>();
		q.offer(new ItalianNote(5));
		q.offer(new EnglishNoteWithDuration(5, Duration.CROMA));
		q.offer(new EnglishNote(5));

		Note nota;
		while ((nota = q.poll()) != null)
			System.out.println(nota);
	}
}
