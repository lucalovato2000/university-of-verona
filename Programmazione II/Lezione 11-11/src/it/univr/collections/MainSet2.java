package it.univr.collections;

import java.util.HashSet;
import java.util.Set;

import it.univr.notes.Duration;
import it.univr.notes.EnglishNote;
import it.univr.notes.ItalianNote;
import it.univr.notes.ItalianNoteWithDuration;
import it.univr.notes.Note;

public class MainSet2 {

	public static void main(String[] args) {
		Set<Note> set = new HashSet<>();
		set.add(new ItalianNote(5));
		set.add(new EnglishNote(6));
		set.add(new ItalianNoteWithDuration(7, Duration.CROMA));
		set.add(new EnglishNote(6));

		System.out.println(set);
		
		for (Note nota: set)
			System.out.println(nota);
	}
}
