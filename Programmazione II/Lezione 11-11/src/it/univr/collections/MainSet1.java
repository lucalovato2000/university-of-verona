package it.univr.collections;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class MainSet1 {

	public static void main(String[] args) {
		Set<String> set = new HashSet<>();

		Scanner keyboard = new Scanner(System.in);

		while (true) {
			System.out.print("stringa (fine per finire): ");
			String s = keyboard.nextLine();
			if ("fine".equals(s))
				break;

			set.add(s);  // O(1)
		}
		
		keyboard.close();

		System.out.println(set);
		
		for (String s: set)
			System.out.println(s);
	}
}
