package it.univr.collections;

import java.util.List;

public class MainLists {

	public static void main(String[] args) {
		/*
		List<String> l = new ArrayList<>();
		l.add("hello");
		l.add("ciao");
		l.add("buongiorno");
		*/
		
		// chiamo un metodo varargs
		List<String> l1 = List.of("hello", "ciao", "buongiorno", "come va?");
		List<String> l2 = List.of("hello");
		List<String> l3 = List.of();

		System.out.println(l1);
	}
}
