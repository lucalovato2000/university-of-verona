package it.univr.collections;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;

public class MainQueue1 {

	public static void main(String[] args) {
		Queue<String> q = new PriorityQueue<>();

		Scanner keyboard = new Scanner(System.in);

		while (true) {
			System.out.print("stringa (fine per finire): ");
			String s = keyboard.nextLine();
			if ("fine".equals(s))
				break;

			q.offer(s);
		}
		
		keyboard.close();

		String s;
		while ((s = q.poll()) != null)
			System.out.println(s);
	}
}
