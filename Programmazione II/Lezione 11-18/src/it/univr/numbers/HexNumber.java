package it.univr.numbers;

public class HexNumber extends AbstractNumber {

	public HexNumber(int value) {
		super(value);
	}

	@Override
	protected int getBase() {
		return 16;
	}

	@Override
	protected char getCharForDigit(int digit) {
		return Character.forDigit(digit, 16);
	}
}
