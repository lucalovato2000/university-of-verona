package it.univr.numbers;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class MainNumbers {

	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Inserisci n non negativo: ");
		int n = keyboard.nextInt();

		Number n1 = new DecimalNumber(n);
		Number n2 = new BinaryNumber(n);
		Number n3 = new BinaryNumberWithParity(n);
		Number n4 = new OctalNumber(n);
		Number n5 = new HexNumber(n);
		Number n6 = new Base58Number(n);

		List<Number> list = new LinkedList<>();
		list.add(n1);
		list.add(n2);
		list.add(n3);
		list.add(n4);
		list.add(n5);
		list.add(n6);

		// oppure:
		// List<Number> list = List.of(n1, n2, n3, n4, n5, n6);
		// nel qual caso ottengo una lista immutabile implementata
		// con una struttura dati scelta dalla libreria Java

		System.out.println(list);

		keyboard.close();
	}
}
