package it.univr.collections;

import java.util.HashSet;
import java.util.Set;

import it.univr.numbers.BinaryNumber;
import it.univr.numbers.HexNumber;
import it.univr.numbers.Number;

/* 
 * senza ridefinire hashCode eredita quello di Object 
 * che implementa un test di ugualianza ==
 */

public class MainCollections3 {
	
	public static void main(String[] args) {
		Set <Number> s1 = new HashSet<>();  // ctrl shift o per importare 
		
		// creo due oggetti diversi 
		Number n1 = new BinaryNumber(155);	
		Number n2 = new HexNumber(155);
		
		s1.add(n1);
		if (s1.contains(n2))
			System.out.println("contiene");
		else
			System.out.println("non contiene");	
	}
}
