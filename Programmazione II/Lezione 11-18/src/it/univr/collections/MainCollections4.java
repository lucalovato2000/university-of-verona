package it.univr.collections;

import java.util.ArrayList;
import java.util.SortedSet;
import java.util.TreeSet;

import it.univr.numbers.Base58Number;
import it.univr.numbers.BinaryNumberWithParity;
import it.univr.numbers.DecimalNumber;
import it.univr.numbers.HexNumber;
import it.univr.numbers.Number;

public class MainCollections4 {

	public static void main(String[] args) {
		SortedSet<Number> l1 = new TreeSet<>();
		l1.add(new DecimalNumber(8934));
		l1.add(new HexNumber(34573));
		l1.add(new Base58Number(11123));
		l1.add(new BinaryNumberWithParity(3456));
		System.out.println(l1);
		// voglio modificare l1 in modo da trasformare
		// tutti i suoi elementi in HexNumber
		for (Number n: new ArrayList<>(l1)) {
			HexNumber hn = new HexNumber(n.getValue());
			l1.remove(n);
			l1.add(hn);
		}
		System.out.println(l1);
	}
}