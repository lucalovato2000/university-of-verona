package it.univr.collections;

import it.univr.numbers.BinaryNumber;
import it.univr.numbers.HexNumber;
import it.univr.numbers.Number;
import java.util.ArrayList;
import java.util.List;

public class MainCollections2 {
	
	public static void main(String[] args) {
		List <Number> l1 = new ArrayList <>();  // ctrl shift o per importare 
		
		// creo due oggetti diversi 
		Number n1 = new BinaryNumber(155);	
		Number n2 = new HexNumber(155);
		
		l1.add(n1);
		if (l1.contains(n2))
			System.out.println("contiene");
		else
			System.out.println("non contiene");	
	}
}
