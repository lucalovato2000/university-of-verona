package it.univr.collections;

import java.util.ArrayList;
import java.util.List;

public class MainCollections {
	
	public static void main(String[] args) {
		List <String> l1 = new ArrayList <>();  // ctrl shift o per importare 
		
		// creo due oggetti diversi 
		String s1 = new String("ciao");	
		String s2 = new String("ciao");
		
		l1.add(s1);
		if (l1.contains(s2))
			System.out.println("contiene");
		else
			System.out.println("non contiene");	
	}
}
