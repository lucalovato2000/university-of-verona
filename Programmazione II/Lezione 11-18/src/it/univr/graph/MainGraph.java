package it.univr.graph;

import it.univr.numbers.Base58Number;
import it.univr.numbers.BinaryNumber;
import it.univr.numbers.BinaryNumberWithParity;
import it.univr.numbers.DecimalNumber;
import it.univr.numbers.HexNumber;

public class MainGraph {

	public static void main(String[] args) {
		Node g = new Node(new Base58Number(1234));
		Node n1 = new Node(new BinaryNumber(3456));
		Node n2 = new Node(new HexNumber(3056));
		Node n3 = new Node(new DecimalNumber(8997));
		Node n4 = new Node(new DecimalNumber(7782));
		Node n5 = new Node(new BinaryNumberWithParity(2344));
		g.linkTo(n1);
		g.linkTo(n2);
		n1.linkTo(n2);
		n1.linkTo(n3);
		n1.linkTo(n4);
		n2.linkTo(n3);
		n3.linkTo(n4);
		n3.linkTo(n5);
		n4.linkTo(g);
		int sum = g.sum();
		System.out.println("La somma dei numeri raggiungibili da g è: " + sum);
	}
}
