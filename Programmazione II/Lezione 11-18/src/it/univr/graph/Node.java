package it.univr.graph;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import it.univr.numbers.Number;

public class Node {
	private final Number number;
	private final List<Node> children = new ArrayList<>();

	public Node(Number number) {
		this.number = number;
	}

	public void linkTo(Node child) {
		children.add(child);
	}

	public int sum() {
		// all'inizio non sono passato da alcun nodo
		return sumWithMemory(new HashSet<>());
	}

	/**
	 * Calcola la somma dei Number nei nodi raggiungibili da this
	 * tenendo conto di essere già passato da seen.
	 * 
	 * @param seen i nodi da cui è già passato
	 * @return la somma
	 */
	private int sumWithMemory(Set<Node> seen) {
		seen.add(this);
		int res = number.getValue();
		for (Node child: children)
			if (!seen.contains(child))
				res += child.sumWithMemory(seen);

		return res;
	}
}