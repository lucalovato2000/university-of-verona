package it.univr.notes;

/***
 *                   Note
 *                     |
 *               -------------
 *               |           |
 *     ItalianNote        EnglishNote
 **/
 
public class Note extends java.lang.Object {
	private final int semitone;
	
	/**
	 * Costruisce una nota, specificando il semitono.
	 * 
	 * @param semitone il semitono (tra 0 e 11)
	 */
	public Note(int semitone) {
		this.semitone = semitone;
	}

	/**
	 * Accessor method per il semitono.
	 */
	public int getSemitone() {
		return semitone;
	}

	public String toString() {
		return "nota di semitono " + semitone;
	}

	public boolean equals(Object other) {
		if (other instanceof Note) {
			// se due note hanno semitoni diversi, non sono equals;
			if (semitone != ((Note) other).semitone)
				return false;
			
			boolean thisHasDuration =
					this instanceof ItalianNoteWithDuration
			     || this instanceof EnglishNoteWithDuration;

			boolean otherHasDuration =
					other instanceof ItalianNoteWithDuration
				 || other instanceof EnglishNoteWithDuration;

			// altrimenti se una nota ha una durata e l'altra non ha una durata
			// non sono equals;
			if (thisHasDuration != otherHasDuration)
				return false;

			// altrimenti se nessuna delle due note ha una durata, sono equals
			if (!thisHasDuration && !otherHasDuration)
				return true;

			// altrimenti le due note sono equals se e solo se
			// hanno la stessa durata

			// se sono arrivato qui è perché entrambe hanno una durata
			Duration durationOfThis;
			if (this instanceof ItalianNoteWithDuration)
				durationOfThis = ((ItalianNoteWithDuration) this).getDuration();
			else
				durationOfThis = ((EnglishNoteWithDuration) this).getDuration();

			Duration durationOfOther;
			if (other instanceof ItalianNoteWithDuration)
				durationOfOther = ((ItalianNoteWithDuration) other).getDuration();
			else
				durationOfOther = ((EnglishNoteWithDuration) other).getDuration();

			return durationOfThis == durationOfOther;
		}
		else
			return false;
	}

	public int compareTo(Note other) {
		// se due note hanno semitoni diversi,
		// le mette in ordine crescente per semitono;
		int diff = semitone - other.semitone;
		if (diff != 0)
			return diff;
		
		// altrimenti se una nota ha una durata e l'altra non ha una durata,
		// mette prima la nota senza durata;
		boolean thisHasDuration =
				this instanceof ItalianNoteWithDuration
			 || this instanceof EnglishNoteWithDuration;

		boolean otherHasDuration =
				other instanceof ItalianNoteWithDuration
			 || other instanceof EnglishNoteWithDuration;

		if (!thisHasDuration && otherHasDuration)
			return -1; // prima this
		
		if (thisHasDuration && !otherHasDuration)
			return 1; // prima other

		// altrimenti se nessuna delle due note ha una durata,
		// le considera uguali;
		if (!thisHasDuration && !otherHasDuration)
			return 0; // sono uguali
		
		// altrimenti mette le due note in ordine crescente per durata
		Duration durationOfThis;
		if (this instanceof ItalianNoteWithDuration)
			durationOfThis = ((ItalianNoteWithDuration) this).getDuration();
		else
			durationOfThis = ((EnglishNoteWithDuration) this).getDuration();

		Duration durationOfOther;
		if (other instanceof ItalianNoteWithDuration)
			durationOfOther = ((ItalianNoteWithDuration) other).getDuration();
		else
			durationOfOther = ((EnglishNoteWithDuration) other).getDuration();

		return durationOfThis.compareTo(durationOfOther);
	}
}