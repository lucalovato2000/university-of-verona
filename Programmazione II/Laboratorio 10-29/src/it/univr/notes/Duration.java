package it.univr.notes;

public enum Duration {
	SEMICROMA,
	CROMA,
	SEMIMINIMA,
	MINIMA,
	SEMIBREVE,
	BREVE
}
