package it.univr.notes;

public class EnglishNoteWithDuration extends EnglishNote {
	
	private final Duration duration;
	
	
	// costruttore
	// La classe ha due campi, uno suo e uno ereditato
	public EnglishNoteWithDuration(int semitone, Duration duration){
		super(semitone);
		
		this.duration = duration;
	}
	
	public Duration getDuration() {
		return duration;
	}
	
	public String toString() {
		// usa implementazione di toString() che c'è nella
		// superclasse ItalianNote e in più concatena con la duration
		// caso raro : se uso un metodo di una superclasse SENZA AGGIUNGERE ALTRO non serve il SUPER
		
		return super.toString() + " " + duration; // sottinteso this.getSemitone()
	}
}

