package it.univr.hairbnb;

import java.util.ArrayList;
import java.util.List;

public class Room {
	
	private final String description;
	private final int people;
	private final int pricePerDay;
	
	List <Review> reviews= new ArrayList<>();

	public Room(String description, int people, int pricePerDay) {
		this.description = description;
		this.people = people;
		this.pricePerDay = pricePerDay;
	}

	public void addReview(Review review) {
		reviews.add(review);
	}

	@Override
	public String toString() {
		
		String result;
		
		result = "Descrizione stanza: " + description + "\n" + 
			     "Numero medio di stelle : " + averageStars() + "\n" +
			     "Numero massimo di persone: " + people + "\n" +
			     "Prezzo giornaliero richiesto: " + pricePerDay + "\n";
		
		for (Review review : reviews) {
			result += "Descrizione: " + reviews;
		}
		return result;
	}

	public int getPeople() {
		return people;
	}

	public int getPriceForDay() {
		return pricePerDay;
	}

	public double averageStars() {
		if (reviews.isEmpty())
			return 0.0;

		int sum = 0;
		for (Review review: reviews)
			sum += review.howManyStars();

		return ((double) sum) / reviews.size();
	}
}