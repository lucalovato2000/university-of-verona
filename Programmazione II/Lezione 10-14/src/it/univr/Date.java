package it.univr;

public class Date {
	
	//vengono inizializzati di default a zero
	
	// campi (variabili di ciascun oggetto)
	// formano lo stato degli oggetti di classe Date
	// li definiamo private per garantire l'incapsulazione
	
	private int day;	// si pu� usare questo campo solo dentro Date.java
	private int month;
	private int year;
	
	
	// con static il campo diventa della classe e tutte le nazionalit� cambiano
	private static Nationality nationality = Nationality.ITALIAN;	//tipo Nationality maiuscolo
	//public final static int ITALIAN = 0;	//costante
	//public final static int AMERICAN = 1;	//costante

	// Costruttore: si DEVE chiamare la classe
	public Date(int day, int month, int year) {
		this.day = day;	// se non uso this conflitto di nome
		this.month = month;
		this.year = year;
	}
	
	// Metodo degli oggetti della classe
	public String toString() {
		
		if(nationality == Nationality.ITALIAN )		//italiano
			return day + "/" + month + "/" + year;
		else
			return month + "/" + day + "/" + year;
	}
	
	/*
	 *  0 significa italiano
	 *  1 significa americano
	 *  
	 *  Deve essere static per lavorare con costanti
	 *  Essendo static non esiste this
	 */
	
	public static void setNationality(Nationality nationality) {
		Date.nationality = nationality;
	}
	
	
	// other � un parametro formale di equals()
	// il parametro formale � una dichiarazione di variabile
	public boolean equals(Date other) {
		// ci sono due Date qui dentro: this e other
		return day == other.day && month == other.month && year == other.year;
	 }
	
	public int compareTo(Date other) {
		 // questa versione � corretta ipotizzando che la differenza
		 // dei day, month, year non vada in overflow
		 int diff = year - other.year;
		 if (diff != 0)
			 return diff;
		 
		 //se month fosse il massimo int esistente e se other.month fosse 1
		 diff = month - other.month;
		 if(diff != 0)
			 return diff;
		 
		 return day - other.day;
	 }

 /* 
  * CONVENZIONE:
  *	
  *	ritorna un numero negativo se this viene prima di other
  *	ritorna un numero positivo se other viene prima di this
  *	ritorna 0 se this e other sono semanticamente uguali
  *
  * public int compareTo(Date other) {
  *	 	// ci sono due Date qui dentro: this e other 
  *		if (year < other.year)
  *	 		return -1;	// prima this
  *		else if(year > other.year)
  *		 	return 1;	// prima other
  *		else if(month < other.month)
  *			 return -1;
  * 	else if(month > other.month)
  *			return 1;
  *		else if(day < other.day)
  *			 return -1;
  *		else if(day > other.day)
  *			 return 1;
  *		else
  *			 return 0;
  * }  
  */	
}
