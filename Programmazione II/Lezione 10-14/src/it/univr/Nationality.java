package it.univr;

public enum Nationality {
	ITALIAN,
	GERMAN,
	AMERICAN
}
