package it.univr;

public class ArrayOfDate {
	public static void main(String[] args) {
		// non esiste: Date[100];
		
		// creazione 1: enumerativa
		Date d1 = new Date(13, 1, 1999);
		Date d2 = new Date(17, 3, 2019);
		
		Date[] arr = {d1, d2};
		
		// creazione 2: con una new
		Date[] arr2 = new Date[4]; // lo crea come {null, null, null, null}
		
		arr2[0] = d1;
		arr2[1] = d2;
		arr2[2] = d1;	// {}d1, d2, null, d1

		for (int pos = 0; pos < 2; pos++) {
			System.out.println("arr[" + pos + "] = " + arr[pos]);
		}
		
		//for each:
		for (Date d:arr)
			System.out.println(d);
		
		for (int pos = 0; pos < arr2.length; pos++) {
			System.out.println("arr2[" + pos + "] = " + arr2[pos]);
		}
		
		Dimension[] arr3 = {Dimension.LARGE, Dimension.MEDIUM, Dimension.HUGE, Dimension.LARGE};
		
		for (int pos = 0; pos < arr3.length; pos++) {
			System.out.println("arr3[" + pos + "] = " + arr3[pos]);
		}
		
		// resetto a null gli elementi di un arr
		// for each non puo essere usato in scrittura
		for (int pos = 0; pos < arr.length; pos++) {
			arr[pos] = null;
		}
	}
}
