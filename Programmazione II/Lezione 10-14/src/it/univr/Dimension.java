package it.univr;

public enum Dimension {
	TINY,
	SMALL,
	MEDIUM,
	LARGE,
	HUGE
}
