package it.univr.concurrency;

// 153 = 1^3 + 5^3 + 3^3

public class Narcissistic {

	public final static boolean isNarcissistic(int n) {
		int numDigits = 0;
		int nn = n;
		while (nn > 0) {
			numDigits++;
			nn /= 10;
		}

		int sum = 0;
		nn = n;
		while (nn > 0) {
			int digit = nn % 10;
			sum += power(digit, numDigits);
			nn /= 10;
		}

		return n == sum;
	}

	private static int power(int base, int exponent) {
		int result = 1;
		while (exponent-- > 0)
			result *= base;

		return result;
	}
}