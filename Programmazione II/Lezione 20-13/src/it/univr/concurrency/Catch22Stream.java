package it.univr.concurrency;

import java.util.stream.IntStream;

import it.univr.concurrency.Narcissistic;

public class Catch22Stream extends Narcissistic {

	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		IntStream.iterate(1, n -> n + 1)
			.parallel()
			.filter(Narcissistic::isNarcissistic)
			.limit(22)
			.forEachOrdered(System.out::println);
		long end = System.currentTimeMillis();
		System.out.println("Tempo impiegato: " + (end - start));
	}
}