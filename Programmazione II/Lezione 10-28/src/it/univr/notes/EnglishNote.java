package it.univr.notes;

/**
 * Una nota inglese estende una Note generica.
 */

// EnglishNode < Note
public class EnglishNote extends Note { // EnglishNote e' una Note

	public EnglishNote(int semitone) {
		super(semitone);
	}

	private final static String[] notes = {
		"C", "C#", "D", "D#", "E", "F", "F#",
		"G", "G#", "A", "A#", "B"
	};

	/**
	 * Ridefinizione (overriding)
	 * del metodo toString() che avrei altrimenti
	 * ereditato da Note
	 */
	public String toString() {
		return notes[getSemitone()]; // sottinteso this.getSemitone()
	}
}
