package it.univr.notes;

public enum Duration {
	// enumerate in ordine crescente di durata
	SEMICROMA, CROMA, SEMIMINIMA, MINIMA, SEMIBREVE, BREVE
}