package it.univr.tictactoe;

public class SimpleTicTacToe extends TicTacToe {

	@Override
	public boolean isGameOver() {
		for (int x = 1; x <= 3; x++)
			for (int y = 1; y <= 3; y++)
				if (at(x, y) == Tile.EMPTY)
					return false;

		return true;
	}
}
