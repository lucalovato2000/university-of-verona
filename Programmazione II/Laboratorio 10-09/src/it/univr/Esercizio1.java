/*
 * Si scriva un programma che legge da tastiera un long non negativo n e lo richiede a oltranza se l'utente lo inserisse negativo. 
 * Quindi genera n coppie (x,y) fatte da due numeri casuali di tipo double, fra -1 e 1. 
 * Per ogni coppia controlla se la coordinata (x,y) sta dentro il cerchio di raggio 1 centrato sull'origine degli assi e 
 * in tal caso incrementa una variabile dentro di tipo long. Alla fine stampa il valore della formula dentro * 4 / n senza
 * perdere le cifre che seguono la virgola.
 */


package it.univr;

import java.util.Random;
import java.util.Scanner;

public class Esercizio1 {
	
	public static void main(String[] args) {
		
		long n, m, dentro = 0;	// numero da inserire
		double ris = 0;
		
		Scanner keyboard = new Scanner(System.in);

		do {
			
			System.out.println("Inserisci un numero long non negativo: ");
			n = keyboard.nextLong();
		
		} while (n < 0);
		
		m = n;
		
		while(m != 0) {
			
			double x, y;
			
			Random random = new Random();
			
			x = -1 + random.nextDouble()*2;		//random.nextDouble() fa da 0.0 a 1.0 quindi partendo da -1 e facendolo due volte si arriva a 1
			y = -1 + random.nextDouble()*2;
			
			System.out.println("Primo numero generato: " + x);
			System.out.println("Secondo numero generato: " + y);
			
			System.out.println();
			
			double distance = Math.sqrt(x * x + y * y);
			if(distance <= 1) {
				dentro++; // Incremento variabile long dentro 
			}
			m--;
		}
		
		ris = dentro * 4.0 / n;	// con 4.0 si salvano le cifre dopo la virgola
		System.out.println(ris);
		
		keyboard.close();
	}
}
