package it.univr;

import java.util.Scanner;

public class Esercizio6 {

	public static void main(String[] args) {
		
		int n;
		Scanner keyboard = new Scanner(System.in);

		do {
			System.out.print("Inserisci n >= 1: ");
			n = keyboard.nextInt();
		}
		while (n < 1);

		keyboard.close();
		
		String row;
		
		for (row = ""; row.length() < n; row += '@');

		String result = row;
		
		while (row.indexOf("@@") >= 0) {
			row = ' ' + row.substring(0, n - 1);
			result = row + '\n' + result + '\n' + row;
		}
		System.out.println(result);
	}
}