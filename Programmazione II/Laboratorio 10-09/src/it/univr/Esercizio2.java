/*
 * Si scriva un programma Java che legge un intero non negativo n da tastiera e stampa una cornice n x n:
 * 
 *	@@@@@@
 *	@    @
 *	@    @
 *	@    @
 *	@    @
 *	@@@@@@
 *  
 */

package it.univr;

import java.util.Scanner;

public class Esercizio2 {
	
	public static void main (String[] args) {
		
		int i = 0, j = 0, n;
		
		Scanner keyboard = new Scanner(System.in);
		
		do {
			
			System.out.println("Inserisci un numero intero non negativo: ");
			n = keyboard.nextInt();
			
		} while (n < 0);
		
		
		for(i = 0; i < n; i++) {
			
			for(j = 0; j < n; j++) {
				
				if(i > 0 && i < n - 1 && j > 0 && j < n - 1) {
					System.out.print(" ");
				}else {
					System.out.print("@");
				}
			}
			System.out.println();
		}
		keyboard.close();
	}
}
