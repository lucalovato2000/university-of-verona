/*
 * Si scriva un programma che legge n >= 1 da tastiera e stampa una piramide di altezza n. Per esempio, per n = 4 deve stampare:
 *     @
 * 	  @@@
 *	 @@@@@
 *	@@@@@@@
 * 
 */

package it.univr;

import java.util.Scanner;

public class Esercizio5 {
	
	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		
		int n;
		
		do{
			System.out.println("Inserisci un numero >= 1: ");
			n = keyboard.nextInt();
			
		}while(n <= 1);
		
		keyboard.close();

		String row;
		
		for(row = "@"; row.length() < n; row = " " + row);
		
		for(int i = 0; i < n; i++) { 
			System.out.println(row);
			row = row.substring(1) + "@@"; //ottengo la stringa tranne il primo carattere
		}
	}
}
