/*
 * Si scriva un programma che stampa, una dopo l'altra, nove cornici, da quella 2 x 2 a quella 10 x 10.
 */

package it.univr;

public class Esercizio3 {
	
	public static void main(String[] args) {
		
		int i, j;
		int n, m = 10;
		
		for(n = 2; n <= m; n++) {
			
			for(i = 0; i < n; i++) {
				
				for(j = 0; j < n; j++) {
					
					if(i > 0 && i < n - 1 && j > 0 && j < n - 1) {
						System.out.print(" ");
					}else{
						System.out.print("@");
					}
				}
				System.out.println();
			}
			System.out.println();
		}
	}
}
