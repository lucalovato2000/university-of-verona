/*
 * Si scriva un programma che stampa 26 volte la cornice 10 x 10, usando ogni volta un carattere alfabetico diverso al posto di '@', dalla 'a' alla 'z'.
 */

package it.univr;

public class Esercizio4 {
	
	public static void main(String[] args) {
		
		int i, j;
		int n = 10, m = 26, k;
		
		char carattere = 'a';
		
		for(k = 0; k < m; k++) {
			
			for(i = 0; i < n; i++) {
				
				for(j = 0; j < n; j++) {
					
					if(i > 0 && i < n - 1 && j > 0 && j < n - 1) {
						System.out.print(" ");
					}else {
						System.out.print(carattere);
					}
				}
				System.out.println();
			}
			System.out.println();
			carattere++;	// aumentando carattere, si scorre l'alfabeto
		}
	}
}
