package it.univr;

public class Flower extends AbstractFlower{

	public Flower(int waterRequest) {
		super(waterRequest);
	}
	
	@Override
	public String toString() {
		return "Flower with waterRequest: " + getWaterRequest();
	}
}
