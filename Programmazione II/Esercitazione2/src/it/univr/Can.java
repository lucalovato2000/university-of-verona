package it.univr;

public abstract class Can {
    
	private int capacity; 	// capacita' dell'annaffiatoio
    private int waterLevel; // livello dell'acqua attuale
    private int refills; 	// quanti refill sono stati fatti

    /**
     * Inizialmente il livello dell'acqua e' a 0! e' richiesto sempre un primo refill.
     * @param capacity capacita' dell'annaffiatoio
     */
    public Can(int capacity) {
        this.capacity = capacity;
        this.waterLevel = 0;
    }

    @Override
    public abstract String toString();

    public abstract void doRefill();

    public int getWaterLevel() {
        return waterLevel;
    }

    public void setWaterLevel(int waterQta) {
        this.waterLevel = waterQta;
    }

    public int getRefills() {
        return refills;
    }

    public void setRefills(int refills) {
        this.refills = refills;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }
}
