package it.univr;

public class WateringCan extends Can{
	
    public WateringCan(int capacity) {
		super(capacity);
	}
    
    @Override
    public String toString() {
    	return ("capacity: " + getCapacity() + ", refills " + getRefills());
    }
    
    @Override
    public void doRefill () {
    	setWaterLevel(getCapacity());	// ottiene la capacita' dell'innaffiatoio e setta il livello di acqua massimo
    	setRefills(getRefills() + 1);	// aumenta di 1 il numero di reefills eseguiti
    }

	/**
     * Metodo per azzerare l'innafiatoio
     */
    public void reset() {
    	setRefills(0);
        setWaterLevel(0);
    }
}
