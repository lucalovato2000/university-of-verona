package it.univr;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        WateringCan leftWateringCan = new WateringCan(5);
        WateringCan rightWateringCan = new WateringCan(7);

        int[][] in = {
                {2,4,5,1,2},
                {1,1,6,7,7},
                {1,1,3,1,3,4,0,4,5,6,6,6,6,6,6,6,3,7},
                {0},
                {12},
                {5,7}
        };

	    Algo algo = new Algo();

        for(int testCase = 0; testCase < in.length; ++testCase) {
            System.out.println(String.format("\n-----------\nTest Case: %s", testCase));

            // eseguo il parse
            List<Flower> flowerList = algo.parseData(in[testCase]);

            // reset degli annaffiatori
            leftWateringCan.reset();
            rightWateringCan.reset();

            // primo refill obbligatorio
            leftWateringCan.doRefill();
            rightWateringCan.doRefill();

            // eseguo l'algoritmo
            algo.run(flowerList, leftWateringCan, rightWateringCan);
        }
    }
}
