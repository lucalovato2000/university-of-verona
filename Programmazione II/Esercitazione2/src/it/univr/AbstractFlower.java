package it.univr;

public abstract class AbstractFlower {
   
	private int waterRequest; 	// indica quanta acqua necessita il fiore

    /**
     * Creo il fiore identificato dalla quantita' di acqua richiesta
     * @param waterRequest quantita' di acqua richiesta dal fiore
     */
    public AbstractFlower(int waterRequest) {
        this.waterRequest = waterRequest;
    }

    @Override
    public abstract String toString();

    @Override
    public boolean equals(Object o) {
        return o instanceof Flower && waterRequest == ((Flower)(o)).getWaterRequest();
    }

    public int getWaterRequest() {
        return waterRequest;
    }

    public void setWaterRequest(int waterRequest) {
        this.waterRequest = waterRequest;
    }
}
