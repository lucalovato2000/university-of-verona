package it.univr;

import java.util.ArrayList;
import java.util.List;

public class Algo {

	/**
	 * Esegue il parse dei dati. Trasforma l'array di interi in oggetti Flower.
	 * 
	 * @param flowers array contenete i fiori. Il valore corrisponde alla quantita'
	 *                di acqua richiesta dal fiore
	 * @return Lista di Flower
	 */
	public List<Flower> parseData(int[] flowers) {

		List<Flower> v = new ArrayList<>();

		for (int i = 0; i < flowers.length; i++) {
			v.add(new Flower(flowers[i]));
		}
		return v;
	}

	/**
	 * Esecuzione dell'algoritmo
	 * 
	 * @param flowerList       Lista dei fiori
	 * @param leftWateringCan  Annaffiatoio della persona che parte da sinistra
	 * @param rightWateringCan Annaffiatoio della persona che parte da destra
	 */
	public void run(List<Flower> flowerList, WateringCan leftWateringCan, WateringCan rightWateringCan) {

		// indice di dove si sta annaffiando
		int leftPersonPosition = 0;
		int rightPersonPosition = flowerList.size() - 1;

		// itero finche' le due persone non si incontrano
		while (leftPersonPosition <= rightPersonPosition) {

			Flower leftFlower = flowerList.get(leftPersonPosition); 	// fiore di sinistra
			Flower rightFlower = flowerList.get(rightPersonPosition); 	// fiore di destra

			// eseguo le operazioni necessarie... devo innaffiare!
			if (leftPersonPosition < rightPersonPosition) {				
				if(leftWateringCan.getWaterLevel()>= leftFlower.getWaterRequest()) {
					leftWateringCan.setWaterLevel(leftWateringCan.getWaterLevel() - leftFlower.getWaterRequest());
				}else{
					leftWateringCan.doRefill();
					leftWateringCan.setWaterLevel(leftWateringCan.getWaterLevel() - leftFlower.getWaterRequest());
            	}
            	if(rightWateringCan.getWaterLevel()>= rightFlower.getWaterRequest()) {
            		rightWateringCan.setWaterLevel(rightWateringCan.getWaterLevel() - rightFlower.getWaterRequest());
				}else{
                  	rightWateringCan.doRefill();
                  	rightWateringCan.setWaterLevel(rightWateringCan.getWaterLevel() - rightFlower.getWaterRequest());
                }
			}else {
				if(leftWateringCan.getWaterLevel() >= leftFlower.getWaterRequest()){
					leftWateringCan.setWaterLevel(leftWateringCan.getWaterLevel() - leftFlower.getWaterRequest());
              	}if (rightWateringCan.getWaterLevel() >= rightFlower.getWaterRequest()){
              		rightWateringCan.setWaterLevel(rightWateringCan.getWaterLevel() - rightFlower.getWaterRequest());
              	}if(((leftWateringCan.getWaterLevel() + rightWateringCan.getWaterLevel()) >= leftFlower.getWaterRequest())){
              		leftWateringCan.setWaterLevel(leftWateringCan.getWaterLevel() - leftFlower.getWaterRequest());
              		rightWateringCan.setWaterLevel(rightWateringCan.getWaterLevel() - rightFlower.getWaterRequest());
              	}else {
              		if (leftWateringCan.getWaterLevel() < leftFlower.getWaterRequest()) {
                  		leftWateringCan.doRefill();
                  		leftWateringCan.setWaterLevel(leftWateringCan.getWaterLevel() - leftFlower.getWaterRequest());
              		}
              		if (rightWateringCan.getWaterLevel() < leftFlower.getWaterRequest() && leftWateringCan.getWaterLevel()!= 0) {
                  		rightWateringCan.doRefill();
                  		rightWateringCan.setWaterLevel(rightWateringCan.getWaterLevel() - leftFlower.getWaterRequest());
              		}else {
                  		rightWateringCan.setWaterLevel(rightWateringCan.getWaterLevel() - leftFlower.getWaterRequest());
              		}
                }
			}
			leftPersonPosition++;
			rightPersonPosition--;
		}
		// stampo il risultato
		this.printRefills(leftWateringCan, rightWateringCan);
		this.printBestFlower(flowerList);
	}

	/**
	 * Stampa il totale di refills e il .toString() dell'annaffiatoio che ha avuto
	 * piu' refills. Se i refills sono uguali di entrambi annaffiatoi, stampo quello
	 * di sinistra.
	 * 
	 * @param leftWateringCan  annaffiatoio di chi parte da sinistra
	 * @param rightWateringCan annaffiatoio di chi parte da destra
	 */
	private void printRefills(WateringCan leftWateringCan, WateringCan rightWateringCan) {

		System.out.println("Total refills: " + (leftWateringCan.getRefills() + rightWateringCan.getRefills()));

		if (leftWateringCan.getRefills() >= rightWateringCan.getRefills())
			System.out.println("Watering Can: " + leftWateringCan.toString());
		else
			System.out.println("Watering Can: " + rightWateringCan.toString());
	}

	/**
	 * Stampo se nella collezzione c'era il fiore speciale: ossia quello che non ha
	 * bisogno di acqua.
	 * 
	 * @param flowerList
	 */
	private void printBestFlower(List<Flower> flowerList) {

		Flower bestFlower = new Flower(0);

		if (flowerList.contains(bestFlower))
			System.out.println("I found the best flower: " + bestFlower.toString());
		else
			System.out.println("This input don't contains the best flower");
	}
}
