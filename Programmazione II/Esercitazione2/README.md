# Istruzioni per la consegna

La consegna dovra' essere fatta in un file compresso fromato `.tar` secondo il seguente formato:

- Nome del file compresso: `ES2-VR112233.tar`
- All'interno del `.tar` lasciare la struttura del file, package e cartelle come da consegna!

Le consegne che non rispettano questa sintassi non verranno letti dallo script che esegue una prima scematura che il codice compili, e quindi scartati.

Potete trovare un feedback al seguente gSheet (Vedi tab nr.2): https://docs.google.com/spreadsheets/d/1ktHtVtJf8tvcGEoT9-mM8DDVHgIo0ZOfbgpBXv7aXTU/edit?usp=sharing
