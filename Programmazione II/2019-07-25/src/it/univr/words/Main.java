package it.univr.words;

import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.function.Predicate;

public class Main {
	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		
		Predicate<String> startsWithJ = new Predicate<String>() {

			@Override
			public boolean test(String word) {
				return !word.isEmpty() && 'J' == word.charAt(0);
			}
		};

		Predicate<String> longerThan4 = new Predicate<String>() {

			@Override
			public boolean test(String word) {
				return word.length() > 4;
			}
		};

		System.out.println("Inserisci il nome del file da cui prendere i caratteri: ");
		String name = keyboard.nextLine();
		
		try {
			Words word = new Words(name);
			System.out.println("toString: " + word.toString());
			System.out.println("Più frequente: " + word.mostFrequent());
			
			word = new Words(name, startsWithJ);
			System.out.println("toString: " + word.toString());
			System.out.println("Più frequente: " + word.mostFrequent());
			
			word = new Words(name, longerThan4);
			System.out.println("toString: " + word.toString());
			System.out.println("Più frequente: " + word.mostFrequent());
			
		}catch(IOException e) {
			System.out.println("There was a problem accessing FILENAME");
		}catch (NoSuchElementException e) {
			System.out.println("I have selected zero words!");
		}
		keyboard.close();
	}
}