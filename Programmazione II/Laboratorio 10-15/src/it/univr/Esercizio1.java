/*
 * Si scriva, usando Eclipse, un programma il cui main legge un intero non negativo da tastiera (se negativo, lo richiede ad oltranza) 
 * e lo trasforma in una stringa che rappresenta lo stesso numero in binario, che infine stampa.
 * Per esempio, se viene immesso l'intero 123, deve costruire e poi stampare la stringa "1111011". * 
 */

package it.univr;

import java.util.Scanner;	// import Scanner

public class Esercizio1 {
	
	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		int n;
		
		do {
			
			System.out.println("Inserisci un numero intero non negativo: ");
			n = keyboard.nextInt();
			
		} while(n < 0);
		
		keyboard.close();
		
		int max = 256;
		
		while(max != 0) {
			
			if (n >= max) {
				System.out.print(1);
				n = n-max;
			}else if (n < max) {
				System.out.print(0);
			}
			max = max/2;
			
		}
	}
}

/*
 * 
 * String s = "";
 * 
 * do {
 * 		s = n % 2 + s;
 * 		n /= 2;
 * 
 * }while(n > 0);
 * 
 * System.out.println(s);
 * 
 *  
 */
