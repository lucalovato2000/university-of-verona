/*
 * Si faccia la stessa cosa in ottale.
 */

package it.univr;

import java.util.Scanner;	// import Scanner

public class Esercizio2 {
	
	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		int n;
		
		do {
			
			System.out.println("Inserisci un numero intero non negativo: ");
			n = keyboard.nextInt();
			
		} while(n < 0);
		
		keyboard.close();
		
		String s = "";
	  
		do {
			s = n % 8 + s;
	  		n /= 8;
	  
		}while(n > 0);
	  
	  System.out.println(s);
	}	
}
