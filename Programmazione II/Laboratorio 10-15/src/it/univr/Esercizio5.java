package it.univr;

import java.util.Scanner;

public class Esercizio5 {
	
	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		String frase;
		int lunghezza;	// suppongo sia palindroma
		
		System.out.println("Inserisci una stringa: ");
		frase = keyboard.next();	// ottiene stringa da tastiera
				
		lunghezza = frase.length();
		System.out.println("La lunghezza della frase inserita risulta: " + lunghezza);
		
		int i = 0, j = lunghezza - 1, flag = 1; // suppongo sia palindoma
		
		while(i <= lunghezza/2 && j >= lunghezza/2) {
				if(frase.charAt(i) != frase.charAt(j)) 
					flag = 0;
	
			i++; j--;
		}
		
		if(flag == 1) {
			System.out.println("Frase palindroma");
		}else
			System.out.println("Frase non palindoma");
		
		keyboard.close();
	}
}
