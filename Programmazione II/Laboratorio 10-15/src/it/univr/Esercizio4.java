/*
 * Si faccia la stessa cosa in esadecimale.
 */

package it.univr;

import java.util.Scanner;	// import Scanner

public class Esercizio4 {
	
	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		int n;
		
		do {
			
			System.out.println("Inserisci un numero intero non negativo: ");
			n = keyboard.nextInt();
			
		} while(n < 0);
		
		keyboard.close();
		
		
		String digits = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";
		
		String s = "";
  
		do {
			int digit = n % 58;
			s = digits.charAt(digit) + s;
			n /= 58;
  		
		}while(n > 0);
  
		System.out.println(s);	 
	}	
}