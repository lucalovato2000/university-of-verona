/*
 * Si faccia la stessa cosa in esadecimale.
 */

package it.univr;

import java.util.Scanner;	// import Scanner

public class Esercizio3 {
	
	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		int n;
		
		do {
			
			System.out.println("Inserisci un numero intero non negativo: ");
			n = keyboard.nextInt();
			
		} while(n < 0);
		
		keyboard.close();
		
		String s = "";
	  
		do {
			int digit = n % 16;
			if (digit < 10)
				s = digit + s;
			else
				s = (char) ('a' + digit - 10) + s;
	  		n /= 16;
	  		
		}while(n > 0);
	  
	  System.out.println(s);
	}	
}

/*
 * String digits = "0123456789abcdef";
 * 
 * String s = "";
 *	  
 *		do {
 *			int digit = n % 16;
 *			s = digits.charAt(digit) + s;
 *	  		n /= 16;
 *	  		
 *		}while(n > 0);
 *	  
 *	  System.out.println(s);
 *  
 */