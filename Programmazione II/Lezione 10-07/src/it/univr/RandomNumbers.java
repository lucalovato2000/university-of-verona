package it.univr;

import java.util.Random; // Da importare per generare numeri casuali
import java.util.Scanner;

public class RandomNumbers {
	
	public static void main(String[] args) {	//ctrl + spazio su "main"
		
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Quanti numeri casuali vuoi? ");
		
		int quanti = keyboard.nextInt();
		
		Random random = new Random();
		
		while(quanti-- > 0) {
		
			int i = random.nextInt();	// Restituisce un numero intero a caso
			System.out.println("Numero generato random: " + i);	// Stampa numero generato
		}
		keyboard.close();
	}
}
