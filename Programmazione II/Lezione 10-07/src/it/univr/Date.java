package it.univr;

public class Date {
	
	//vengono inizializzati di default a zero
	
	// campi (variabili di ciascun oggetto)
	private int day;
	private int month;
	private int year;

	// Costruttore: si DEVE chiamare la classe
	public Date(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
	public String toString() {
		return day + "/" + month + "/" + year;
	}	
}
