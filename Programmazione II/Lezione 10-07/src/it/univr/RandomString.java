package it.univr;

import java.util.Random; // Da importare per generare numeri casuali
import java.util.Scanner;

public class RandomString {
	
	public static void main(String[] args) {	//ctrl + spazio su "main"
		
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Quanto deve essere lunga la stringa? ");
		
		int length = keyboard.nextInt();
		
		Random random = new Random();
		String s = "";
		
		while(length-- > 0) {
			// Genero un carattere a caso c
			int index = random.nextInt(26);	// Da 0 a 25
			char c = (char) ('a' + index);
			s += c;	// Concatenazione
		}
		System.out.print(s);	// Stampa stringa generata
		
		keyboard.close();
	}
}
