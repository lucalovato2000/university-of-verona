package it.univr;

import java.util.Scanner;

public class CircleArea {
	
	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.print("Raggio: ");
		
		double radius = keyboard.nextDouble();	// Nuovo numero double da 0.0 a 1.0
		double area = radius * radius * Math.PI;
		
		String s = String.format("L'area del cerchio di raggio %.2f e' %.2f", radius, area);
		System.out.print(s);
		
		keyboard.close();
	}
}
