/*
 * Lancia un dado tante volte, in maniera casuale
 */

// shift + alt + r per rinominare tutto in una volta sola

package it.univr;

import java.util.Random; // Da importare per generare numeri casuali
import java.util.Scanner;

public class RandomDice {
	
	public static void main(String[] args) {	//ctrl + spazio su "main"
		
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Quanti numeri casuali vuoi? ");
		
		int quanti = keyboard.nextInt();
		
		Random random = new Random();
		
		while(quanti-- > 0) {
			// Devo generare un numero intero fra 1 e 6
			int dado1 = 1 + random.nextInt(6);	// Restituisce un numero intero a caso
			int dado2 = 1 + random.nextInt(6);
			// In genere se voglio un numero casuale da x ad y (inclusi)
			// x + random.nextInt(y - x + 1)
			
			System.out.println(dado1 + dado2);	// Stampa numero generato
		}
		keyboard.close();
	}
}
