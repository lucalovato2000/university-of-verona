package it.univr;

import java.util.Random;

public class Card {

	  /**
	   * Il valore della carta.
	   */
	 private final Value value;

	  /**
	   * Il seme della carta.
	   */
	  private final Suit suit;
	
	 
	  /**
	   * Genera una carta a caso con un valore da min (incluso) in su.
	   * 
	   * @param min il valore minimo della carta che può essere generata (0-12)
	   */
	  public Card(Value min) {  
		  
		  Random random = new Random();
		  
		  this.value = Value.values()[(random.nextInt(13) - min.ordinal()) + min.ordinal()];	//da min a 12 compreso
		  this.suit = Suit.values()[random.nextInt(4)]; // da 0 a 3
	  }
	 
	  
	  /**
	   * Genera una carta a caso con un valore da 0 (incluso) in su.
	   */
	   public Card() {
		   this(Value.TWO);	// chiama il costruttore sopra passando come valore minimo la carta due
	   }
	   
	   /**
	    * 
	    * @return il valore della carta (0-12)
	    */
	   public Value getValue() { 
		   return value;
	   }
	  
	   /**
	    * 
	    * @return il seme della carta (0-3) 
	    */
	   public Suit getSuit() {
		   return suit;
	   }
	  
	 
	  /**
	   * Ritorna una descrizione della carta sotto forma di stringa, del tipo 10♠ oppure J♥.
	   */
	  public String toString() {
		  
		  String valueAsString;
		  int value = this.value.ordinal();
		 
		  if (value <= 8)
			  valueAsString = String.valueOf(2 + value);
		  else if(value == 9)
			  valueAsString = "J";
		  else if(value == 10)
			  valueAsString = "Q";
		  else if(value == 11)
			  valueAsString = "K";
		  else
			  valueAsString = "A";

		  
		  char suitAsChar = "♠♣♦♥".charAt(suit.ordinal());
		  
		  return valueAsString + suitAsChar;  
	  }
		  /*
	  	  if(value >= 0 && value <= 8 && suit == 0) {
			  return (2+value + "♠");
	  	  }else if(value == 9 && suit == 0){
	  		  return ("J♠");
	  	  }else if(value == 10 && suit == 0){
	  		  return ("Q♠");
	  	  }else if(value == 11 && suit == 0){
	  		  return ("K♠");
	  	  }else if(value == 12 && suit == 0){
	  		  return ("A♠");
	  		  
	  	  }else if(value >= 0 && value <= 8 && suit == 1) {
			  return (2+value + "♣");
	  	  }else if(value == 9 && suit == 1){
	  		  return ("J♣");
	  	  }else if(value == 10 && suit == 1){
	  		  return ("Q♣");
	  	  }else if(value == 11 && suit == 1){
	  		  return ("K♣");
	  	  }else if(value == 12 && suit == 1){
	  		  return ("A♣");
	  		  
	  	  }else if(value >= 0 && value <= 8 && suit == 2) {
			  return (2+value + "♦");
	  	  }else if(value == 9 && suit == 2){
	  		  return ("J♦");
	  	  }else if(value == 10 && suit == 2){
	  		  return ("Q♦");
	  	  }else if(value == 11 && suit == 2){
	  		  return ("K♦");
	  	  }else if(value == 12 && suit == 2){
	  		  return ("A♦");
	  		  
	  	  }else if(value >= 0 && value <= 8 && suit == 3) {
			  return (2+value + "♥");
	  	  }else if(value == 9 && suit == 3){
	  		  return ("J♥");
	  	  }else if(value == 10 && suit == 3){
	  		  return ("Q♥");
	  	  }else if(value == 11 && suit == 3){
	  		  return ("K♥");
	  	  }else if(value == 12 && suit == 3){
	  		  return ("A♥");
	  		  
	  	  }else return ("Errore nella generazione della carta da Poker");
	  */
	  
	 
	  /**
	   * Determina se questa carta è uguale ad other.
	   * 
	   * @param other l'altra carta con cui confrontarsi
	   * @return true se e solo se le due carte sono uguali
	   */
	  public boolean equals(Card other) {  
		  return (value == other.value && suit == other.suit);
	  }
	  
	  
	  public int compareTo(Card other) {
		  //int diff = value.ordinal() - other.value.ordinal();
		  int diff = value.compareTo(other.value);
		  if (diff != 0)
			  return diff;
		  else
			  //return suit.ordinal() - other.suit.ordinal();
			  return suit.compareTo(other.suit);
	  	
	  }
}