package it.univr;

public enum Suit {
	SPADES,
	CLUBS,
	DIAMONDS,
	HEARTS
}
