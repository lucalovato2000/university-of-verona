package it.univr;

public class MainCard {
	
	public static void main(String[] args) {
		
		boolean ris = false;
		
		do{
			
			Card card1 = new Card(); 
			Card card2 = new Card();
			
			System.out.println("card1: " + card1);
			System.out.println("card2: " + card2);
			
			ris = card1.equals(card2);
			System.out.println(ris);
			
		}while(ris != true);		
	}
}
