package it.univr;

import java.util.Scanner;	// Importare per utilizzare lo Scanner

public class Repeat2 {

	public static void main(String[] args) {
		
		String frase;	// frase � di tipo String
		Scanner keyboard = new Scanner(System.in);
		
		do {
			System.out.print("Inserisci una frase: ");
			frase = keyboard.nextLine();	// Quando legge nuova frase quella prima finisce nel GARBAGE COLLECTOR

		} while (!frase.equals("exit"));	//finch� la frase inserita non risulta uguale a "exit"
		
		keyboard.close(); 	// Altrimenti keyboard da warning perch� processo dello Scanner rimane aperto
	}
}
