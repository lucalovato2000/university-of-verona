/* 
 * compareTo: int esito = a.compareTo(b);	
 * - esito < 0: se a viene prima di b
 * - esito > 0: se a viene dopo di b
 * - esito == 0: se a e b sono equivalenti
*/

package it.univr;

import java.util.Scanner;	// Importare per utilizzare lo Scanner

public class Repeat {

	public static void main(String[] args) {
		
		String frase; // frase � di tipo String
		Scanner keyboard = new Scanner(System.in);
		
		do {
			System.out.print("Inserisci una frase: ");
			frase = keyboard.nextLine(); 
			
			System.out.println("Gli ultimi tre caratteri inseriti sono: " + frase.substring(frase.length() - 3));
			
			System.out.println("Il carattere i si trova in posizione " + frase.indexOf('i'));
			
			System.out.println(frase.compareTo("ciao"));
			
			System.out.println(frase.concat("!!!!")); // Non viene mai utilizzata, si utilizza il +
			
			System.out.println(frase.concat(String.valueOf(13))); // Traduce numero in stringa

			System.out.println("La tua frase � lunga " + frase.length() + " il primo carattere risulta: " + frase.charAt(0));
			
			System.out.println(String.format("La tua frase � lunga %d il primo carattere risulta %c\n", frase.length(), frase.charAt(0)));
			
			
			// Sostituiamo il terzo carattere stringa con una z
			frase = frase.substring(0, 2) + 'z' + frase.substring(3);
			
			
			long millis = System.currentTimeMillis();
			System.out.println("millis= " + millis);
			
			
		} // while (frase.charAt(2) != 'i'); //parte da 0
		while (frase.length() <=2 || frase.charAt(2)!= 'i');
		
		keyboard.close();
	}	
}
