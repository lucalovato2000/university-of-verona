package it.univr.cards;

import java.util.Random;

public class Card implements Comparable<Card>{

  /**
   * Il valore della carta.
   */
  private final Value value;

  /**
   * Il seme della carta.
   */
  private final Suit suit;
 
  /**
   * Genera una carta a caso con un valore da min (incluso) in su.
   * 
   * @param min il valore minimo della carta che può essere generata (0-12)
   */
  public Card(Value min) {
	  Random random = new Random();
	  this.value = Value.values()[random.nextInt(13 - min.ordinal()) + min.ordinal()];
	  this.suit = Suit.values()[random.nextInt(4)];
  }
  
  /**
   * Genera una carta a caso con un valore dalla carta 2 (inclusa) in su.
   */
  public Card() {
	  this(Value._2);
  }
 
  public Value getValue() {
	  return value;
  }
 
  public Suit getSuit() {
	  return suit;
  }
 
  /**
   * Ritorna una descrizione della carta sotto forma di stringa, del tipo 10♠ oppure J♥.
   */
  public String toString() {
	  String valueAsString;
	  int value = this.value.ordinal();

	  if (value <= 8)
		  valueAsString = String.valueOf(2 + value);
	  else if (value == 9)
		  valueAsString = "J";
	  else if (value == 10)
		  valueAsString = "Q";
	  else if (value == 11)
		  valueAsString = "K";
	  else
		  valueAsString = "1";

	  char suitAsChar = "♠♣♦♥".charAt(suit.ordinal());

	  return valueAsString + suitAsChar;
  }
 
  /**
   * Determina se questa carta è uguale ad other.
   * 
   * @param other l'altra carta con cui confrontarsi
   * @return true se e solo se le due carte sono uguali
   */
  public boolean equals(Object other) {
	  return other instanceof Card && value == ((Card)other).value && suit == ((Card) other).suit;
  }
  
  // piu attributi prendiamo in considerazioni meglio e'
  public int hashCode() {
	  return value.ordinal();	// non banale
	  // return suit.ordinal(); // non banale ma preferisco usare value perche' ha piu' varianti (13)
	  // return value.hasCode();
	  // return value.ordinal() ^ suit.ordinal();
	  // return 10; 	// banale
  }

  public int compareTo(Card other) {
	  //int diff = value.ordinal() - other.value.ordinal();
	  int diff = value.compareTo(other.value);
	  if (diff != 0)
		  return diff;
	  else
		  //return suit.ordinal() - other.suit.ordinal();
		  return suit.compareTo(other.suit);
  }
}