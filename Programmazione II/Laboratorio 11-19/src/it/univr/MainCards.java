package it.univr;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import it.univr.cards.Card;

public class MainCards {
	
	public static void main(String[] args) {
		
		int maxnumber = 100_000_000;
		
		Set <Card> s1 = new HashSet<>();	// new HashSet empty
		Set <Card> t1 = new TreeSet<>();	// new TreeSet empty
		
		long startTimeHash = System.currentTimeMillis();
		for (int i = 0; i < maxnumber; i++) {
			Card c1 = new Card();
			s1.add(c1);
		}
		System.out.print("[HashSet] Time elapsed in milliseconds = ");
	    System.out.println(System.currentTimeMillis() - startTimeHash);
		System.out.println("[HashSet size]: " + s1.size());

		long startTimeTree = System.currentTimeMillis();
		for (int j = 0; j < maxnumber; j++) {
			Card c2 = new Card();
			t1.add(c2);
		}
		System.out.print("[TreeSet] Time elapsed in milliseconds = ");
	    System.out.println(System.currentTimeMillis() - startTimeTree);
		System.out.println("[TreeSet size]: " + t1.size());

		System.out.println("HashSet e TreeSet sono equals? " + s1.equals(t1));
	}
}
