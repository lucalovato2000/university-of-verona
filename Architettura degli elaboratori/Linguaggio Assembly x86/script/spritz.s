# Seconda lezione
#
# -----------------------------------------
# ## COMPILARE CON GCC SU MACCHINA 64bit ###
# ----- as -32 -o miofile.o miofile.s ------
# - ld -m elf_i386 -o miofile.x miofile.o --
# ------------------------------------------
# il nome della etichetta deve essere univoco


.section .data

titolo: 
        .ascii "Calcola il numero di auto per andare a bere uno spritz!\n\n"
titolo_len:
        .long . - titolo

risposta:
        .ascii "Totale auto necessarie: "
risposta_len:
        .long . - risposta

numStudenti:
    .long 87

numStudPerAuto:
    .long 5 

numAuto:
    .ascii "000\n"


.section .text
        .global _start

_start:
        movl numStudenti, %eax
        movl numStudPerAuto, %ebx

        divb %bl       # ottengo il risultato in AL  e il resto in AH

        cmpb $0, %ah 
        je continua 

        incb %al
        xorb %ah, %ah  # azzero ah con l'utilizzo dello xor

continua:
    leal numAuto, %esi

    movl $10, %ebx
    divb %bl            # meglio dividere in byte      
    addb $48, %ah
    movb %ah, 2(%esi)       # somma alla locazione esi 2
    xorb %ah, %ah

    divb %bl
    addb $48, %ah
    movb %ah, 1(%esi)
    xorb %ah, %ah

    divb %bl
    addb $48, %ah
    movb %ah, (%esi)    # sempre con le parentesi
   
Stampa:
    movl $4, %eax
    movl $1, %ebx
    leal titolo, %ecx
    movl titolo_len, %edx
    int $0x80

    movl $4, %eax       
    movl $1, %ebx
    leal risposta, %ecx
    movl risposta_len, %edx
    int $0x80

    movl $4, %eax       
    movl $1, %ebx
    leal numAuto, %ecx
    movl $4, %edx
    int $0x80

    movl $1, %eax
    movl $0, %ebx
    int $0x80
