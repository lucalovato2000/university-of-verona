# # Esercizio 1
# In questo esercizio dovete implementare due funzioni, `estremi(lista)` e `somma_estremi(lista)`.
# Entrambe le funzioni operano su liste di numeri interi.
# La prima, `estremi(lista)`, ritorna una tupla contenente la posizione del valore minimo e massimo contenute in lista, in quest'ordine.
# Potete assumere che ogni numero nella lista è unico (ovvero, i valori massimi e minimi sono unici).
# 
# Implementate poi la funzione somma_estremi(lista). Essa trova le posizioni del valore massimo e minimo usando estremi(lista), e ritorna infine la somma di tutti i valori inclusi tra quelle due posizioni, estremi inclusi.
# Notate che la posizione del minimo può essere maggiore o minore di quella del massimo.
# 
# Esempio1:
# `lista = [ 1, 2, 3, 4, 5 ]`. La funzione `estremi(lista)` ritorna (0, 4), ovvero le posizioni di 1 e 5.
# La funzione `somma_estremi(lista)` ritorna 15, ovvero 1 + 2 + 3 + 4 + 5
# 
# Esempio2:
# `lista = [ 3, 8, 2, 0, 6 ]`.La funzione `estremi(lista)` ritorna (3, 1), ovvero le posizioni di 0 e 8.
# La funzione `somma_estremi(lista)` ritorna 10, ovvero la somma 8 + 2 + 0
# 
# 
# Assunzioni:
# * Ogni elemento $i$ di $list$ è un numero intero, $0 \le i \le 1000$
# * Ogni elemento $i$ di $list$ è unico
# * Ogni lista contiene da 2 a 100 elementi

from typing import List, Tuple

def estremi(lista : List[int]) -> Tuple[int, int]:
    pass # TODO

def somma_estremi(lista : List[int]) -> int:
    pass # TODO


if __name__ == '__main__':
    lista = [ 1, 2, 3, 4, 5 ]
    assert estremi(lista) == (0, 4)
    assert somma_estremi(lista) == 15

    lista = [ 3, 8, 2, 0, 6 ]
    assert estremi(lista) == (3, 1)
    assert somma_estremi(lista) == 10


