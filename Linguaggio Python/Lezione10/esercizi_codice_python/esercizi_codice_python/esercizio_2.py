# # Esercizio 2
# In questo esercizio dovete implementare la funzione `miglior_sequenza(nome_file)`. L'input `nome_file` è una stringa che rappresenta il nome di un file. La funzione deve aprire il file e leggerne il contenuto. Ogni riga del file è composta da quattro valori numerici (float) separati da virgola.
# Ognuna delle quattro colonne rappresenta una sequenza, la funzione deve calcolare la media di ogni sequenza e ritorna il numero della sequenza che ha la media più alta (un numero compreso tra 0, la prima colonna, e 3, l'ultima).
# 
# Potete svolgere l'esercizio senza usare moduli, oppure usando numpy e/o pandas.
# 
# Assunzioni:
# * Ogni riga del file contiene esattamente 4 numeri, separati da virgola.
# * Il file contiene da 1 a 1000 righe
# * La colonna con la media più alta è unica

def miglior_sequenza(nome_file : str) -> int:
    pass

if __name__ == '__main__':
    migliore = miglior_sequenza('esempio_es_2.csv')
    assert migliore == 2


