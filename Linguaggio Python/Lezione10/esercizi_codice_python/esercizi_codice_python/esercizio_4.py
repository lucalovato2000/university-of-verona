# # Esercizio 4 - Pandas e Matplotlib
# Questo esercizio serve a esplorare alcune delle feature di pandas e matplotlib. Consiglio di svolgere l'esercizio su jupyter-notebook.

# ## Lettura e scrittura di Dataframe
# 
# Come primo step, caricate il file `'dati_climatici.csv'` usando read_csv.

# In[ ]:


import pandas as pd
clima = # TODO


# Provate a convertire il dataframe in altri formati.

# In[ ]:


# Stampate clima in formato excel
clima.to_... #TODO


# In[ ]:


# Stampate clima in html
# TODO


# ## Indici e slice
# Vogliamo selezionare solo i dati che ci interessano dal dataframe. Potete farlo usando gli indici e lo slicing. Potrebbe essere utile usare l'attributo `.iloc`, che permette di lavorare usando indici numerici (invece che indici ad alto livello, come `'temperatura'`).

# In[ ]:


# esempio iloc: selezionare prima colonna
clima.iloc[:, 0]


# In[ ]:


# selezionare solo le righe in cui la temperatura è esattamente 29 gradi


# In[ ]:


# selezionare le prime 5 righe in cui l'evento è sole o pioggia


# In[ ]:


# selezionare le righe in cui la velocità del vento è massima


# In[ ]:


# costruite una Series pandas che contiene i dati della colonna temperatura e usa i valori della colonna giorno come etichette.
temperature = #todo


# ## Statistica
# Possiamo applicare alcune (semplici) operazioni statistiche sui nostri dati. con `mean()` e `median()` possiamo calcolare rispettivamente media e mediana.

# In[ ]:


# stampare media e mediana dei valori vel_vento


# Possiamo combinare Questi operatori al metodo `groupby`. Questo raccoglie insieme tutte le righe che rispettano un certo parametro. Per esempio, se vogliamo stampare temperatura e velocità vento medie in base all'evento atmosferico, possiamo usare:

# In[ ]:


# crea 3 righe: Neve, Pioggia, Sole
print(clima.groupby('evento').mean())


# In[ ]:


# stampate la stessa tabella, ma riportando solo la colonna 'temperatura'
# dove mi conviene tagliare? Direttamente su clima, oppure sul risultato finale?


# In[ ]:


# provate a combinare groupy() con count(), un metodo che ci permette di contare gli elementi
# quanti giorni di Pioggia ci sono in clima?


# ## Stampa
# Come visto a lezione, possiamo usare pandas per creare grafici. Usiamo `plot()` per fare stampe generiche.

# In[ ]:


# plot() dell'intero dataframe


# In[ ]:


# plot delle sole temperature


# In[ ]:


# plot delle vel_vento, raggruppate per evento.
# cosa stampo se chiamo plot() su groupby?
clima[['vel_vento', 'evento']].groupby('evento').plot();


# Potete creare stampe più specifiche chiamando `plot` come attributo e specificando uno dei suoi metodi. Per esempio, possiamo stampare uno scatterplot nel seguente modo:

# In[ ]:


clima.plot.scatter(x='temperatura', y='vel_vento');


# Potete ispezionare, con metodi d'introspezione, tutti i metodi disponibili in questo modo: 

# In[ ]:


[ method_name for method_name in dir(clima.plot) if not method_name.startswith("_") ]


# Alcuni metodi, come scatterplot, richiedono specifici parametri. Potete trovarli nella documentazione. In Jupyter, potete usare un ? all'inizio di un comando per aprire una finestra con la documentazione

# In[ ]:


get_ipython().run_line_magic('pinfo', 'clima.plot.scatter')


# In[ ]:


# stampate un grafico a torta (pie) contenente il numero di giorni di Sole, Neve e Pioggia.


# In[ ]:


# stampate un boxplot (box) con i dati di temperatura e vel_vento


# In[ ]:


# Extra: Provate a stampare un semplice grafico usando tutti i metodi a disposizione!


# # Per approfondire...

# Potete approfondire ulteriormente pandas seguendo gli esempi proposti nella documentazione ufficiale, che trovate qui: https://pandas.pydata.org/docs/getting_started/index.html#getting-started. Essi si sviluppano tutti attorno ad un unico dataset (i passeggeri del titanic) e coprono pandas nella sua interezza.

# Se servono stampe molto specifiche, possiamo usare direttamente matplotlib.
# Il sito di matplotlib offre tutorial specifici per ogni feature, li potete trovare qui: https://matplotlib.org/stable/tutorials/index.html.
# Potete anche consultare un "ricettario", con molti casi d'uso comune già implementati, al seguente link: https://matplotlib.org/stable/gallery/index.html

