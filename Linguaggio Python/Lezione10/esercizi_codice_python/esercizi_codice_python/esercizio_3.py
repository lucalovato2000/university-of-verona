# # Esercizio 3
# In questo esercizio dovete implementare la funzione `filtra_parole(parole, minima_frequenza, minima_lunghezza)`. Essa riceve in input un dizionario `parole` che mappa stringhe in interi. Le chiavi del dizionario rappresentano le parole contenute in un testo, mentre i valori del dizionario sono il numero di volte che quella parola è stata usata nel testo.
# La funzione `filtra_parole(parole, minima_frequenza, minima_lunghezza)` ritorna una lista di stringhe contenenti le parole di `parole` che sono state usate almeno `minima_frequenza` volte e la cui lunghezza è almeno `minima_lunghezza`. Le parole della lista risultato devono essere ordinate in ordine alfabetico.
# 
# Per esempio, dati il dizionario `d = {'cane' : 100, 'coccodrillo' : 5, 'serpente' : 10,  'panda' : 10,}`, `filtra_parole(d, 10, 5)` deve ritornare la lista `['panda', 'serprente']`. Non posso includere ne `'cane'`, perché è troppo corto, ne `coccodrillo`, perché è stata usata troppe poche volte.
# 
# * Il dizionario contiene da 1 a 1000 elementi.

from typing import Dict, List
def filtra_parole(parole : Dict[str, int], minima_frequenza : int, minima_lunghezza : int) -> List[str]:
    pass

if __name__ == '__main__':
    d = {'cane' : 100, 'coccodrillo' : 5, 'serpente' : 10,  'panda' : 10}
    res = filtra_parole(d, 10, 5)
    print(res)
    assert len(res) == 2
    assert res[0] == 'panda'
    assert res[1] == 'serpente'



