#!/usr/bin/env python
# coding: utf-8

# # Esercizio 1
# Un metodo numerico che si può usare per calcolare, in modo approssimato, la radice quadrata di un numero è il [metodo di newton](https://en.wikipedia.org/wiki/Newton%27s_method).
# Questo metodo parte da una prima stima della radice e migliora iterativamente il valore, avvicinandosi al risultato.
# Se vogliamo calcolare la radice quadrata di $x$, partendo dalla stima $s$, possiamo ottenere una stima migliore $s'$ usando la formula
# \begin{equation}
# s' = \frac{s + \frac{x}{s}}{2}
# \end{equation}
# Per esempio, se vogliamo calcolare la radice di 4 partendo dalla stima 4, otteniamo 2.5. Se applichiamo lo stesso metodo (usando 2.5 al posto di 4 come stima) otteniamo 2.05, un valore sempre più vicino al valore vero (2).
# 
# Scrivete la funzione `migliora_stima(val, stima)`, che implementa la formula del metodo di newton per migliorare (di un passo) la stima della radice del numero val. Esso deve ritornare il valore migliorato della stima.
# Implementate poi la funzione `newton(val, stima)`, che prende un valore e una stima, e continua ad applicare `migliora_stima` finché la differenza tra due passi di miglioramento è < 0.0001. La funzione deve infine ritornare questa stima della radice.
# 
# Potete confrontare la vostra tecnica con la funzione matematica `math.sqrt(val)`.

# In[10]:


import math

def migliora_stima(val, stima):
    # TODO
    pass

def newton(val, stima):
    # TODO
    pass

newton(4, 10)
newton(4, 0.05)
newton(17, 10)

print(math.sqrt(4), math.sqrt(17))


# # Esercizio 2
# Vogliamo indagare la validità dell'[ultimo teorema di fermat](https://en.wikipedia.org/wiki/Fermat%27s_Last_Theorem).
# Esso afferma che, dati tre numeri interi positivi $a, b, $ e $c$ e un numero intero $n$ maggiore o uguale a 2, l'espressione
# \begin{equation}
# a^n + b^n = c^n
# \end{equation}
# può essere vera se e solo se n è uguale a 2.
# Scrivete una funzione `verifica_fermat(a, b, c, n)` che verifica la validità del teorema in una data istanza.
# Questa funzione deve ritornare True se il teorema vale, o False altrimenti.
# 
# Scrivete il metodo `testa_teorema(max_num, max_pot)` che serve a testare il teorema più volte. Esso deve usare `verifica_fermat` per ogni combinazione di $a, b, c$ con i numeri compre tra 1 e `max_num`, e li deve testare provando tutte le potenze comprese tra 2 e `max_pot`. Notate che `max_num` e `max_pot` devono essere inclusi nella ricerca.
# Per esempio, `testa_teorema(3, 3)` proverà a mettere $1, 2, 3$ in $a, b, c$ (in tutte le combinazioni possibili (1, 1, 1), (1, 1, 2), ...); proverà questo combinazioni usando $n = 2$ e $n = 3$.
# Se `testa_teorema` trova un caso in cui `verifica_fermat` è falso, esso deve stampare "ho trovato un controesempio" e fermarsi.
# Se invece alla fine di tutto non ha trovato nessun controesempio, esso deve stampare "non ho trovato controesempi".

# In[26]:


def verifica_fermat(a, b, c, n):
    pass

def testa_teorema(max_num, max_pot):
    pass

testa_teorema(100, 3)
testa_teorema(20, 5)


# # Esercizio 3
# Una tripletta di numeri che soddisfa il teorema quando $n = 2$ è nota come terna pitagoriga. Per esempio, (3, 4, 5) è una terna pittagorica corretta, perché $3^2 + 4^2 = 9 + 16 = 25 = 5^2$.
# Scrivere una funzione `terna_corretta(a, b, c)` che ritorni `True` se $a, b, c$ sono una terna pitagorica. Notate che l'ordine è importante, confronto sempre la somma delle potenze dei primi due elementi con la potenza del terzo elemento.
# 
# Implementate poi la funzione `colleziona_terne(max_n)`. Questa funzione ritorna una lista di tuple che sono terne pitagoriche. Essa testa tutte le terne $(a, b, c)$ instanziate con tutte le possibili combinazioni di valori tra 1 e `max_n` (incluso). Usate la funzione `terna_corretta(a, b, c)` per identificare quali combinazioni vanno aggiunte alla collezione.
# Per esempio, `colleziona_terne(10)` ritorna la lista `[(3, 4, 5), (4, 3, 5), (6, 8, 10), (8, 6, 10)]`

# In[25]:


def terna_corretta(a, b, c):
    return a**2 + b**2 == c**2

def colleziona_terne(max_n):
    risultato = []
    # TODO
    return risultato

print(colleziona_terne(10))


# # Esercizio 4
# Implementate la funzione `nested_sum(l)`. Essa riceve in input una lista di liste di numeri interi $l$ e deve calcolare la somma di tutti i numeri presenti nelle liste contenute in `l`. Per esempio, `nested_sum(([[1,2,3], [5], [-1 , -1, -1]])` mi deve ritornare il valore `8`.
# 
# Implementate la funzione `nested_compact(l)`. Essa riceve in input una lista di liste di numeri interi $l$ e deve costruire una nuova lista che contiene i numeri di $l$, ma senza ripetizioni. L'ordine dei numeri deve essere preservato, se un numero appare più volte, solo la primo occorrenza deve essere inserita nella lista finale.
# Per esempio, `nested_compact([[1, 2, 3], [-1, 3], [-2, -2]])` deve ritornare la lista `[1, 2, 3, -1, -2]`

# In[28]:


def nested_sum(l):
    # TODO
    pass

def nested_compact(l):
    # TODO
    pass

nested_sum(([[1,2,3], [5], [-1 , -1, -1]]))
nested_compact([[1, 2, 3], [-1, 3], [-2, -2]])


# # Esercizio 5
# Scrivere una funzione `esclamazioni(l)` che riceve una lista di stringhe e ritorna una nuova lista che segue i seguenti parametri:
# 1. se una stringa non contiene il carattere `a` deve essere ignorata
# 2. le stringhe che contengono il carattere `a` devono essere modificate, aggiungendo la stringa "a " prima della stringa originale, e la stringa "!!!" alla fine.
# 3. infine, la lista finale deve essere ordinata in ordine alfabetico
# 
# Per esempio, `esclamazioni(["zebra", ,"lion", "gorilla", "tiger"])` deve ritornare la lista `["a gorilla!!!", "a zebra!!!"]`

# In[ ]:


def esclamazioni(l):
    # TODO
    pass

print(esclamazioni(["zebra", ,"lion", "gorilla", "tiger"]))

