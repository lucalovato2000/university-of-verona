# # Esercizio 2
# Vogliamo indagare la validità dell'[ultimo teorema di fermat](https://en.wikipedia.org/wiki/Fermat%27s_Last_Theorem).
# Esso afferma che, dati tre numeri interi positivi $a, b, $ e $c$ e un numero intero $n$ maggiore o uguale a 2, l'espressione
# \begin{equation}
# a^n + b^n = c^n
# \end{equation}
# può essere vera se e solo se n è uguale a 2.
# Scrivete una funzione `verifica_fermat(a, b, c, n)` che verifica la validità del teorema in una data istanza.
# Questa funzione deve ritornare True se il teorema vale, o False altrimenti.
# 
# Scrivete il metodo `testa_teorema(max_num, max_pot)` che serve a testare il teorema più volte. Esso deve usare `verifica_fermat` per ogni combinazione di $a, b, c$ con i numeri compre tra 1 e `max_num`, e li deve testare provando tutte le potenze comprese tra 2 e `max_pot`. Notate che `max_num` e `max_pot` devono essere inclusi nella ricerca.
# Per esempio, `testa_teorema(3, 3)` proverà a mettere $1, 2, 3$ in $a, b, c$ (in tutte le combinazioni possibili (1, 1, 1), (1, 1, 2), ...); proverà questo combinazioni usando $n = 2$ e $n = 3$.
# Se `testa_teorema` trova un caso in cui `verifica_fermat` è falso, esso deve stampare "ho trovato un controesempio" e fermarsi.
# Se invece alla fine di tutto non ha trovato nessun controesempio, esso deve stampare "non ho trovato controesempi".

def verifica_fermat(a, b, c, n):
    pass

def testa_teorema(max_num, max_pot):
    pass

testa_teorema(100, 3)
testa_teorema(20, 5)

