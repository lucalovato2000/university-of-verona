# # Esercizio 5
# Scrivere una funzione `esclamazioni(l)` che riceve una lista di stringhe e ritorna una nuova lista che segue i seguenti parametri:
# 1. se una stringa non contiene il carattere `a` deve essere ignorata
# 2. le stringhe che contengono il carattere `a` devono essere modificate, aggiungendo la stringa "a " prima della stringa originale, e la stringa "!!!" alla fine.
# 3. infine, la lista finale deve essere ordinata in ordine alfabetico
# 
# Per esempio, `esclamazioni(["zebra", ,"lion", "gorilla", "tiger"])` deve ritornare la lista `["a gorilla!!!", "a zebra!!!"]`

# In[ ]:


def esclamazioni(l):
    # TODO
    pass

print(esclamazioni(["zebra", ,"lion", "gorilla", "tiger"]))

