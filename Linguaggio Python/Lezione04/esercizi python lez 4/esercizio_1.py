# # Esercizio 1
# Un metodo numerico che si può usare per calcolare, in modo approssimato, la radice quadrata di un numero è il [metodo di newton](https://en.wikipedia.org/wiki/Newton%27s_method).
# Questo metodo parte da una prima stima della radice e migliora iterativamente il valore, avvicinandosi al risultato.
# Se vogliamo calcolare la radice quadrata di $x$, partendo dalla stima $s$, possiamo ottenere una stima migliore $s'$ usando la formula
# \begin{equation}
# s' = \frac{s + \frac{x}{s}}{2}
# \end{equation}
# Per esempio, se vogliamo calcolare la radice di 4 partendo dalla stima 4, otteniamo 2.5. Se applichiamo lo stesso metodo (usando 2.5 al posto di 4 come stima) otteniamo 2.05, un valore sempre più vicino al valore vero (2).
# 
# Scrivete la funzione `migliora_stima(val, stima)`, che implementa la formula del metodo di newton per migliorare (di un passo) la stima della radice del numero val. Esso deve ritornare il valore migliorato della stima.
# Implementate poi la funzione `newton(val, stima)`, che prende un valore e una stima, e continua ad applicare `migliora_stima` finché la differenza tra due passi di miglioramento è < 0.0001. La funzione deve infine ritornare questa stima della radice.
# 
# Potete confrontare la vostra tecnica con la funzione matematica `math.sqrt(val)`.

import math

def migliora_stima(val, stima):
    # TODO
    pass

def newton(val, stima):
    # TODO
    pass

newton(4, 10)
newton(4, 0.05)
newton(17, 10)

print(math.sqrt(4), math.sqrt(17))

