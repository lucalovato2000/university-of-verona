# # Esercizio 3
# Una tripletta di numeri che soddisfa il teorema quando $n = 2$ è nota come terna pitagoriga. Per esempio, (3, 4, 5) è una terna pittagorica corretta, perché $3^2 + 4^2 = 9 + 16 = 25 = 5^2$.
# Scrivere una funzione `terna_corretta(a, b, c)` che ritorni `True` se $a, b, c$ sono una terna pitagorica. Notate che l'ordine è importante, confronto sempre la somma delle potenze dei primi due elementi con la potenza del terzo elemento.
# 
# Implementate poi la funzione `colleziona_terne(max_n)`. Questa funzione ritorna una lista di tuple che sono terne pitagoriche. Essa testa tutte le terne $(a, b, c)$ instanziate con tutte le possibili combinazioni di valori tra 1 e `max_n` (incluso). Usate la funzione `terna_corretta(a, b, c)` per identificare quali combinazioni vanno aggiunte alla collezione.
# Per esempio, `colleziona_terne(10)` ritorna la lista `[(3, 4, 5), (4, 3, 5), (6, 8, 10), (8, 6, 10)]`

def terna_corretta(a, b, c):
    return a**2 + b**2 == c**2

def colleziona_terne(max_n):
    risultato = []
    # TODO
    return risultato

print(colleziona_terne(10))


