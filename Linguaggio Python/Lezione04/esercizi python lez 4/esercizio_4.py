# # Esercizio 4
# Implementate la funzione `nested_sum(l)`. Essa riceve in input una lista di liste di numeri interi $l$ e deve calcolare la somma di tutti i numeri presenti nelle liste contenute in `l`. Per esempio, `nested_sum(([[1,2,3], [5], [-1 , -1, -1]])` mi deve ritornare il valore `8`.
# 
# Implementate la funzione `nested_compact(l)`. Essa riceve in input una lista di liste di numeri interi $l$ e deve costruire una nuova lista che contiene i numeri di $l$, ma senza ripetizioni. L'ordine dei numeri deve essere preservato, se un numero appare più volte, solo la primo occorrenza deve essere inserita nella lista finale.
# Per esempio, `nested_compact([[1, 2, 3], [-1, 3], [-2, -2]])` deve ritornare la lista `[1, 2, 3, -1, -2]`


def nested_sum(l):
    # TODO
    pass

def nested_compact(l):
    # TODO
    pass

nested_sum(([[1,2,3], [5], [-1 , -1, -1]]))
nested_compact([[1, 2, 3], [-1, 3], [-2, -2]])

