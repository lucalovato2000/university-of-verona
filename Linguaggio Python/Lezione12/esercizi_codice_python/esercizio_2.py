# # Esercizio 2
# Implementate le funzioni `fizz_buzz(n)`. Essa prende un numero intero positivo `n` e ritorna la stringa `"fizz"` se `n` è divisibile per 3, `"buzz"` se `n` è divisibile per 5, `"fizzbuzz"` se `n` è divisibile sia per 3 che per 5. In tutti gli altri casi, la funzione ritorna il numero `n` convertito in stringa.
# Per esempio, `fizz_buzz(2)` ritorna la stringa `"2"`, `fizz_buzz(5)` ritorna `"buzz"`, `fizz_buzz(6)` ritorna `"fizz"`, `fizz_buzz(15)` ritorna `"fizzbuzz"`.
# 
# Implementate poi la funzione `fizz_buzz_serires(n)`. Essa prende un numero intero positivo `n`, e restituisce una stringa che contiene tutti i valori di `fizz_buzz` compresi tra 1 e `n`, estremi inclusi. Questi valori devono essere separati tra loro usando una virgola e uno spazio, i.e. `", "`. Per esempio, `fizz_buzz_series(10)` ritorna `"1, 2, fizz, 4, buzz, fizz, 7, 8, fizz, buzz"`.

def fizz_buzz(n : int) -> str:
    pass # TODO

def fizz_buzz_series(n : int) -> str:
    pass # TODO


if __name__ == '__main__':
    assert fizz_buzz(2)  == "2"
    assert fizz_buzz(5)  == "buzz"
    assert fizz_buzz(6)  == "fizz"
    assert fizz_buzz(15) == "fizzbuzz"

    assert fizz_buzz_series(3)  == "1, 2, fizz"
    assert fizz_buzz_series(10) == "1, 2, fizz, 4, buzz, fizz, 7, 8, fizz, buzz"
    assert fizz_buzz_series(16) == "1, 2, fizz, 4, buzz, fizz, 7, 8, fizz, buzz, 11, fizz, 13, 14, fizzbuzz, 16"
