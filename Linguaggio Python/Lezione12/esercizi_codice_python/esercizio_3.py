# # Esercizio 3
# Implementare la funzione `trova_estensione(base, estesa)`. Essa riceve due liste di numeri interi, `base` e `estesa`. Ogni numero nella lista `base` è un numero intero univoco, ovvero non appare altre volte nella stessa lista. La lista `estesa` contiene tutti gli stessi numeri di `base`, possibilmente ordinati in modo diverso, più un nuovo numero univoco chiamato `estensione`. La funzione deve trovare e restituire `estensione`. La funzione non deve in alcun modo modificare le liste `base` e `estesa`.
# 
# Per esempio, `trova_estensione([1, 2, 3], [3, 0, 1, 2])` restituisce `0`.

from typing import List
def trova_estensione(base : List[int], estesa: List[int]) -> int:
    pass # TODO


if __name__ == '__main__':
    assert trova_estensione([1, 2, 3], [3, 0, 1, 2]) == 0
    assert trova_estensione([ i for i in range(1000)], [ i for i in range(1000)] + [-1]) == -1
    assert trova_estensione([ i for i in range(1000)], [ i for i in range(1000)] + [10000]) == 10000

