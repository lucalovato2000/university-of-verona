# # Esercizio 3
# Il [cifrario di cesare](https://it.wikipedia.org/wiki/Cifrario_di_Cesare) è un noto e semplice algoritmo crittografico. L'algoritmo prende una stringa e una chiave (un numero n), e sposta tutti i caratteri della stringa di n posizioni nell'alfabeto. Se uno spostamento supera la 'z', si ricomincia a contare dalla 'a'. Per esempio, la stringa `abz` e la chiave `2` mi generare la stringa cifrata `cdb`.
# 
# Implementate le funzioni `cripta(testo, chiave)` che cripta un testo usando il cifrario di cesare, e la funzione `decripta(testo, chiave)` che fa l'operazione inversa. Assumete che le stringhe contengano solo caratteri (maiuscoli e minuscoli) e spazi. Gli spazi non devono essere cambiati, mentre i caratteri devono essere spostati (rispettando maiuscole e minuscole).
# 
# In python, ogni carattere è identificato da un numero intero. Potete ottenere questo numero usando la funzione `ord('a')`, che può essere utile per trasformare le lettere in numeri e implementare il cifrario. La funzione `chr(n)` fa l'operazione inversa, trasforma numeri in caratteri.

# In[7]:


def cripta(testo : str, chiave : int) -> str:
    pass

def decripta(testo : str, chiave : int) -> str:
    pass

# esempio di ord e chr
print(ord('a'), ord('A'))
print(chr(ord('a')))

