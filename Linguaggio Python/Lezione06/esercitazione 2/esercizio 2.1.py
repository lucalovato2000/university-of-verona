# # Esercizio 1
# Nel gioco "indovina il numero" il computer sceglie un numero segreto da 1 a 100 (inclusi), e l'utente deve indovinare il numero nel minor numero di tentativi possibili.
# Il gioco deve fornire un prompt all'utente per inserire il numero, e si deve assicurare che l'input sia valido (ovvero deve essere un numero da 1 a 100). In caso di input invalido, il gioco deve chiedere un nuovo numero, senza generare errori.
# Il gioco termina quando il numero inserito è uguale al numero segreto. Se il numero inserito è più basso (risp. alto) del numero segreto, il sistema deve informare l'utente stampando la stringa 'troppo basso' (ris. 'troppo alto').
# 
# Il gioco deve anche tenere traccia del numero di tentativi usati prima di vincere (escludendo casi di input invalido). In caso di vittoria, il programma stampa il numero di tentativi necessari.

# In[3]:


from random import randint

segreto = randint(1, 100)
tentativi = 0

# TODO


