# # Esercizio 2
# Implementate una funzione `invert_dict(d)` che prende un dizionario d e costruisce un nuovo dizionario invertito d', ovvero un dizionario che per ogni coppia chiave-valore di d contiene una coppia valore-chiave, in cui la chiave originale diventa il valore e il valore originale diventa la chiave. Le chiavi di un dizionario sono univoche, mentre i valori non devono esserlo. Se la funzione trova più di una chiave con lo stesso valore, deve sollevare un eccezione di tipo `ValueError("unique inversion is not possible")`.
# Per esempio, il dizionario `numeri = {'uno':1, 'due':2, 'tre':3}` verrà trasformato in `{1:'uno', 2:'due', 3:'tre'}`, mentre `gambe = {'uomo' : 2, 'gatto': 4, 'lampada' :1, 'cane':4 }` non può essere invertito (4 dovrebbe puntare sia a gatto che a cane).
# 
# Implementate poi il metodo `invert_dict_multi(d)` che opera nello stesso modo di `invert_dict(d)`, ma puntando a una lista di chiavi, invece che a una singola chiave. Quindi se più di una chiave punta allo stesso elemento non sollevo eccezioni.
# Per esempio, `gambe = {'uomo' : 2, 'gatto': 4, 'lampada' :1, 'cane':4 }` deve costruire il dizionario invertito `{2: ['uomo'], 4: ['gatto', 'cane'], 1: ['lampada']}`.

# In[4]:


def invert_dict(d : dict) -> dict:
    pass

def invert_dict_multi(d : dict) -> dict:
    pass


numeri = {'uno':1, 'due':2, 'tre':3}
gambe = {'uomo' : 2, 'gatto': 4, 'lampada' :1, 'cane':4 }

try:
    print(invert_dict(numeri))
except ValueError as err:
    print(err.args[0])
    
try:
    print(invert_dict(gambe))
except ValueError as err:
    print(err.args[0])
    
try:
    print(invert_dict_multi(numeri))
except ValueError as err:
    print(err.args[0])

try:
    print(invert_dict_multi(gambe))
except ValueError as err:
    print(err.args[0])


