# # Esercizio 4
# Il file `experiment_data.csv` contiene i risultati di un esperimento. I dati sono raccolti in tre colonne, e i valori sono separati da virgole. Ogni riga rappresenta il risultato di un esperimento, fatta eccezione per la prima riga, che contiene il nome delle colonne (*misure, potenza, guadagno*). La prima colonna contiene numeri interi, mentre le altre due contengono numeri reali.
# 
# Implementate una funzione `carica_esperimento(nome_file)` che riceve il nome di un file e ritorna tre liste, la prima conterrà tutti i valori della colonna *misure* (come interi), la seconda tutti i valori della colonna *potenza* (come float), la terza tutti i valori della colonna *guadagno* (come float).
# 
# Scrivete poi una funzione `media(l)` e `mediana(l)` che ritornano rispettivamente la [media aritmetica](https://it.wikipedia.org/wiki/Media_(statistica)) e la [mediana](https://it.wikipedia.org/wiki/Mediana_(statistica)) di una lista di numeri `l`, e usate queste funzioni per calcolare e stampare a schermo media e mediana di *potenza* e *guadagno*.
# 
# Costruite poi due dizionari. Il primo mappa ogni possibile valore della colonna *misure* (un valore intero da 35 a 45) alla lista di valori di *potenza* associati a quel numero di misure effettuate. Il secondo crea un mapping simile, ma per i valori di *guadagno*. Calcolare infine la potenza media e il guadagno medio per ogni valore di misura.

# In[13]:


#import random
#
#print('misure,potenza,guadagno')
#for i in range(500):
#    print(f'{random.randint(35, 45)},{random.gauss(10.3, 1.5):3f},{random.gauss(0, 2)}')

def carica_esperimento(nome_file):
    pass

def media(l):
    pass

def mediana(l):
    pass

misure, potenza, guadagno = carica_esperimento('experiment_data.csv')

# stampa media e mediana di potenza e guadagno

# costuire i dizionar
mis_potenza = dict()
mis_guadagno = dict()
# TODO

# calcolo media per mis_potenza
for mis, pot in mis_potenza.items():
    avg = media(pot)
    # stampa valore medio...

for mis, guad in mis_guadagno.items():
    avg = media(pot)
    # stampa valore medio...

