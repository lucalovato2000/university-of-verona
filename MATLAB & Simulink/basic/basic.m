%{
. Create the following row vectors twice, using linspace and using the colon operator:
[1 2 3 4 5 6 7 8 9 10]
[2 7 12]
%}

vector=1:10;    %crea un vettore da 1 a 10
vector2=linspace(1,10,10); 
vector3=2:5:12;
vector4=linspace(2,12,3);   %minimo, massimo, numero di elementi

%%linspace crea un vettore di elementi equispaziati

%{
Create a 4 x 2 matrix of all zeros and store it in a variable (mymat). Then, replace the second row
in the matrix with a vector consisting of a 3 and a 6. 
%}

mymat=zeros(4,2);   %matrice 4x2 con tutti zeri al suo interno
mymat(2,:)=[3 6];   %cambia la seconda colonna della matrice con un vettore di 3 e 6

%{
Create a vector x which consists of 20 equally spaced points in the range from �pi to +pi. Create
a y vector which is sin(x).
%}

x=linspace(pi,pi,20);
y=sin(x);

%{
Create a 4 x 6 matrix of random integers, each in the range from -5 to 5; store it in a variable
(mat). Create another matrix that stores for each element the absolute value of the
corresponding element in the original matrix (mat_pos). 
%}

mat=randi([-5,5],4,6);
mat_ps=abs(mat);

%{
Plot exp(x) for values of x ranging from -2 to 2 in steps of 0.1. Put an appropriate title on the
plot, and label the axes. 
%}

x=-2:0.1:2;
y=exp(x);
figure(1);
plot(x,y,'*');
title('exp(x)');    %titolo del plot

%{
Create a vector x with values ranging from 1 to 100 in steps of 5. Create a vector y which is the
square root of each value in x. Plot these points. Now, use the bar function instead of plot to
get a bar chart instead. 
%}

x=1:5:100;  %vettore di elementi da 1 a 100 cons tep di 5 fra uno e l'altro
y=sqrt(x);
figure(2);
bar(x,y);