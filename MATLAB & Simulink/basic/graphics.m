ages=20:29;
students=[2 1 5 3 4 2 0 2 1 0];

%GRAFICO A BARRE CON COMANDO BAR()
figure(3);
bar(ages, students)
axis([19.5 29.5 0 5]) %altezza, larghezza | minima e massima
xlabel('age of students');
ylabel('numer of students');

%GRAFICO A BARRE CON COMANDO BARH() H=HORIZONTAL
figure(4);
barh(ages, students)
axis([0 5 19.5 29.5])
xlabel('age of students');
ylabel('numer of students');


strength = [49 52 37 29 14 17]; 
labels = {'PRM', 'SEC', 'TRZ', 'QRT', 'QNT', 'SES'};

%GRAFICO A TORTA CON COMANDO PIE()
figure(1);
pie(strength, [0 0 0 0 0 0], labels)

%GRAFICO A TORTA CON COMANDO PIE3() 3=3D
figure(2);
pie3(strength, labels);

%GRAFICO A LINEE
    %Generare 100 numeri random da 1 a 10
    t=randi(10, 100, 1);
    %Contare un numero di elementi in ogni caso
    [counts, centers] = hist(t, unique(t));

figure(3);
stem(centers, counts)
xlabel('value');
ylabel('number of events');
axis([0 11, -1 max(counts+1)])


%SOTTOGRAFICI CON DATI CONDIVISI
x=1:6;
y=[33 11 5 9 22 30];

figure(5)
subplot(1,4,1)
bar(x,y)
subplot(1,4,3)
stem(x,y)