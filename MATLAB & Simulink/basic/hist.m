%Istrogramma
y=[92, 94, 93, 96, 93, 94, 95, 96, 91, 93, 95, 95, 95, 92, 93, 94, 91, 94, 92, 93];
x=91:96; %valori da 91 a 96
bar(y, x);
xlabel('asse x');
ylabel('asse y');
title('Esercizio Istogramma'); 

%Al posto di scrivere 91 91 91 91 possiamo scrivere 91*ones[1,4]