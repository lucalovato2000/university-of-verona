x=[3 8 5 3 4 1 8];
[xmean, stdev]=main(x)

%funzioni di MatLab

media=mean(x)
varianza=var(x)
stdev_2=std(x)
moda=mode(x)
mediana=median(x)

%Altre funzioni utili di MatLab

range(x);
max(x);
min(x);
iqr(x); %interquartile
prctile(x);
quantile(x);
y = quantile(x,.50); %mediana

boxplot(); %boxplot
tabulate(); %tabella di frequenza
corrcef(); %coefficiente di correlazione

gscatter  %Diagramma Scatter in vari gruppi
hist3     %Istogramma in 3D


 
%FUNZIONE HIST

hist(y,n) %data,numero di classi
