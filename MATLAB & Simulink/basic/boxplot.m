%BoxPlot and Quartiles
N=10;
data=20*rand(N,1); %rand tra 0 e 20

media=mean(data)
mediana=median(data)
devstandard=std(data)

varianza= devstandard^2 

figure(1);
boxplot(data, 'orientation', 'horizontal');
figure(2);
boxplot(data);


%Quartili
AllQuartiles= quantile(data, [0.25, 0.5, 0.75]);
AllQuartiles2= quantile(data, 3)
P40= prctile(data, 40)