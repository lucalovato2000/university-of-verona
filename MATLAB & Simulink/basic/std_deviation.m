n = 100; % grades
mu_B = 21; % mean
sigma_B = 3; % standard deviation
mu_C = 23; % mean
sigma_C = 2; % standard deviation
rng default % for reproducibility
B = ceil(normrnd(mu_B,sigma_B,1,n))'; %normrnd()  
C = ceil(normrnd(mu_C,sigma_C,1,n))'; % 


students =[B C];
% Central tendency
min_S = min (students);
max_S = max (students);
mean_S = mean (students);
median_S = median (students);
% Dispersion
range_S = range (students);
var_S = var (students);
sd_S = std (students);
% Skewness and Kurtosis
sk = skewness (students);
k = kurtosis (students);

% Quartiles and Percentiles
AllQuartiles = quantile(students,[0.25 0.5 0.75])';
Pall = prctile (students, [1:100]);
figure(1);
hold on
plot(Pall(:,1),'r','LineWidth',3);
plot(Pall(:,2),'g','LineWidth',3);
box on; grid on;
legend('Bioinformatics','Computer Science');
hold off
%Interquartile
Interquartile = iqr (students)


figure(2);
hist(students)
figure(3);
bins = 14;
hist(students,bins)
figure(4);
histfit(students(:,1))
title('Bioinformatics','FontSize',16);
figure(5);
histfit(students(:,2))
title('Computer Science','FontSize',16);


af_B = hist (B, max(B)-min(B)+1);
af_C = hist (C, max(C)-min(C)+1);
% relative frequency
rf_B = af_B/n;
rf_C = af_C/n;
gradesB = [min(B):max(B)]';
gradesC = [min(C):max(C)]';
figure(6);
subplot(1,2,1)
bar (gradesB, af_B,'r')
title('Bioinformatics','FontSize',16);
subplot(1,2,2)
bar (gradesC, af_C, 'g')
title('Computer Science','FontSize',16);


figure(7);
subplot(1,2,1)
% Show the first 10 values in descending order
% Only the first 95% of the cumulative distribution is
displayed
pareto (af_B, gradesB)
title('Bioinformatics','FontSize',16);
subplot(1,2,2)
pareto (af_C, gradesC)
title('Computer Science','FontSize',16);

% boxplot of bioinformatics and computer science students
figure(8);
boxplot (students)
title('Bioinfomatics vs. Computer Science','FontSize',16);