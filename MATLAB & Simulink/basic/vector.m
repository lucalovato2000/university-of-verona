%{
Vector
%}

%Entrambi i comandi generano un vettore che parte da 5 arriva ad 8
%valori all'interno saranno spaziati tra di loro di 0.1
ans2= 5:0.1:8;
ans3= linspace(5,8,31);

%MatLab parte dall'indice 1
a=[12; 34];
A=[123; 456; 789]; 

%Selezionare tutta la prima riga a(1,:)
%Selezionare tutta la seconda colonna a(:,2)

%Crea un matrice mxm             magic(4)
%Riempie con valori zero         zeros(4,4)
%Riempie con valori random       rand(4,4)
%Riempie con valori uno          unos(4,4)

%Elimina la seconda colonna      x(2,:)=[] 

month = 'August';

b=zeros(5);
b(2,3)=4;
[k,j]=find(b);
length(b)   %ritorna dimensione maggiore della matrice

%Creare un grafico con il comando plot()
C=[1 2 5 7 3 6 10 4];
figure(1)
plot(C)

figure(5)
subplot(2,2,1)
plot(C)

subplot(2,2,2)
area(ans2)
area(ans2)
area(A)
subplot(2,2,3)
subplot(2,2,4) 

%Grazie a questi tre comandi posso visvualizzare due grafici uno sopra
%l'altro su un picture singola

%plot(A) 
%hold on 
%plot (B)