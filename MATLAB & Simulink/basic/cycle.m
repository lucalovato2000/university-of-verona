N=100;
weekdays=7;
data=zeros(N,weekdays);
for i=1:weekdays          %i=1:7, scorri da 1 a 7
        data(:,i)= 7+6*sin(i/5)+randn(N,1); 
end

figure(1);
boxplot(data);
set(gca(), 'xtick', [1:weekdays],'xticklabel',{'Mon', 'Tu', 'Wed', 'Thr', 'Fri', 'Sat', 'Sun'});