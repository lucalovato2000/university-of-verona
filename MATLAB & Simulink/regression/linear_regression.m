% A set of 100 pairs of data (x,y)
x = linspace(1,15,100)';
y = 2*x + (x+randn(size(x))).^2;
scatter(x,y) % function to visualize (grafico a bollicine)
individual points
xlabel('x')
ylabel('y')
X = x;
B = X\y; % parameter estimates
scatter(x,y)
hold on
plot(x, X*B)
title('y = \beta_1 x', 'FontSize',18)
hold off
X = [ones(size(x)) x];
B = X\y;
scatter(x,y)
hold on
plot(x, X*B)
title('y = \beta_0 + \beta_1 x','FontSize',18)
hold off

X = [ones(size(x)) x x.^2];
B = X\y;
scatter(x,y)
hold on
plot(x, X*B)
title('y = \beta_0 + \beta_1 x + \beta_2 x^2','FontSize',18)
hold off