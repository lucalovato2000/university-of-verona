% Time data
x = 0:19;
% Population data
y = [6,13,23,33,54,83,118,156,210,282,...
350,440,557,685,815,990,1170,1350,1575,1830]’;
% Linear fit
p1 = polyfit(x,y,1);
% Quadratic fit
p2 = polyfit(x,y,2);
% Cubic fit
p3 = polyfit(x,y,3);
% Residuals
res1 = polyval(p1,x)-y;
res2 = polyval(p2,x)-y;
res3 = polyval(p3,x)-y;

figure(1)
subplot(2,2,1)
stem(res1)
xlabel('t (min)'); ylabel('SS_R')
subplot(2,2,2)
stem(res2)
xlabel('t (min)'); ylabel('SS_R')
subplot(2,2,3)
stem(res3)
xlabel('t (min)'); ylabel('SS_R')
SSR_1 = sum((polyval(p1,x)-y).^2);
SYY = sum((y-mean(y)).^2);
R2_1 = 1 - SSR_1/SYY;
SSR_2 = sum((polyval(p2,x)-y).^2);
R2_2 = 1 - SSR_2/SYY;
SSR_3 = sum((polyval(p3,x)-y).^2);
R2_3 = 1 - SSR_3/SYY;