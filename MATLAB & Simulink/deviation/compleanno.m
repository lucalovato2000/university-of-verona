n = 4; % number of people
d = 365; % days
n_Bday = factorial(n) * nchoosek(365,n);
% The total number of ways in which we choose n birthdays is 365^n.
P_different_Bday = factorial(n).*nchoosek(d,n)./(d.^n);

k = 100;
for i=1:k
P_same_Bday_all(i) = factorial(i).*nchoosek(d,i)./(d.^i); %#ok<SAGROW>
end
plot(P_same_Bday_all);
title('P(no two of them celebrate their birthday on the same day in a room of n people');
xlabel('Number of people in a room');