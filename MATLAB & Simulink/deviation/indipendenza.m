PE=cumsum(nE)./(1:n)'; 
%probabilita'
PEcF=cumsum(nEF)./cumsum(nF)'; 
% P(E)= P(E|F) --> E and F are independent
PF = cumsum(nF) ./ (1:n)';
PFcT = cumsum(nFT) ./ cumsum(nT); % F and T are dependent
figure(1)
hold on
semilogx(1:n, PE, 1:n, ones(n,1)*length(E)/6, 'r--')
semilogx(1:n, PEcF, 1:n, ones(n,1)*length(E)/6, 'r--') % 0.333
box on; grid;
axis([0 n 0.28 0.37])
hold off
xlabel('n')
ylabel('P(E) and P(E|F)')
figure(2)
hold on
semilogx(1:n, PF, 1:n, ones(n,1)*length(F)/6, 'r--') % 0.5
semilogx(1:n, PFcT, 1:n, ones(n,1)*length(F)/6, 'r--') % 0
%axis([0 n 0.4 0.6])
box on; grid;
hold off
xlabel('n')
ylabel('P(F) and P(F|T)')