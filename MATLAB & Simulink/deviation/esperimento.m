%% Ex.1: Dice rolling experiment
n = 10000;
dice1 = randi([1 6],1,n); % generate nintegers between 1 and 6
freqA = hist(dice1, 1:6); % absolute frequency
freqR = hist(dice1, 1:6)/n;
figure(1)
subplot(1,3,1)
bar(1:6 ,freqA);
xlabel('Outcome')
ylabel('Absolute Frequency');
title('Dice (1) rolling experiment');
subplot(1,3,2)
bar(1:6 ,freqR);
xlabel('Outcome')
ylabel('Relative Frequency');
title('Dice (1) rolling experiment');
subplot(1,3,3)
dice2 = randi([1 6],2,n);
freqR2 = hist(sum(dice2),[2:12])/n;
bar(2:12,freqR2);
xlabel('Outcome')
ylabel('Relative Frequency');
title('Sum of two fair dice');