load stockreturns
x = stocks(:,3);
%Test the null hypothesis that the
sample data comes from a population
with mean equal to zero.
[h,p,ci,stats] = ttest(x)
%Test the null hypothesis that the
sample data are from a population with
mean equal to zero at the 1%
significance level.
[h,p,ci] = ttest(x,0,'Alpha',0.01)

h = 1
p = 0.0106
ci = -0.7357 -0.0997
stats =
struct with fields:
tstat: -2.6065
df: 99
sd: 1.6027
h = 0
p = 0.0106
ci = -0.8387 0.0032
