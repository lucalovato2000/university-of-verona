load examgrades
x = grades(:,3);
h = ttest(x,75)
h = ttest(x,65,'Tail','right’)
y = grades(:,4);
h = ttest(x,y,'Alpha',0.01)

load data_bloodpressure_ttest.mat
% giovani
N_giovani = length(pressione_giovani);
m_giovani = mean(pressione_giovani);
sd_giovani = std(pressione_giovani);
fprintf('gruppo giovane --> N = %4.0f m = %4.1f sd
= %4.1f\n', N_giovani, m_giovani, sd_giovani);
% anziani
N_anziani = length(pressione_anziani);
m_anziani = mean(pressione_anziani);
sd_anziani = std(pressione_anziani);
fprintf('Gruppo anziani --> N = %4.0f m = %4.1f sd
= %4.1f \n', N_anziani, m_anziani, sd_anziani);
% test per normalità
[h_g,p_g] = lillietest(pressione_giovani);
[h_a,p_a] = lillietest(pressione_anziani);

alpha = 0.05;
gradi_liberta = N_giovani + N_anziani - 2;
%sp^2 è lo stimatore pooled di sigma^2
sp = sqrt(((N_giovani-1)*(sd_giovani^2) + (N_anziani-1)*(sd_anziani^2))/gradi_liberta);
t = (m_giovani-m_anziani)/(sp*sqrt(1/N_giovani+1/N_anziani));
fprintf('Il valore di t è %1.3f \n',t);
limiti = tinv([(alpha/2) (1-alpha/2)],gradi_liberta);
fprintf('I valori soglia con df = %2.0f sono: %1.3f e %1.3f \n', ...
gradi_liberta,limiti(1),limiti(2))

if t < limiti(1) || t > limiti(2)
fprintf('Esito test: rifiuto H0 (con alpha=%1.2f) \n',alpha)
else
fprintf('Esito test: accetto H0 \n')
end
area_t = tcdf(t,gradi_liberta);
if t < 0
p_value = 2*area_t;
else
p_value = 2*(1-area_t);
end
fprintf('Il p_value è pari a %1.5f \n',p_value)
% Matlab function - ttest
[h,p] = ttest2(pressione_giovani,pressione_anziani,0.05);
fprintf('Il p_value della function di Matlab è pari a %1.5f \n',p)