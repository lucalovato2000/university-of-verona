% 95% TWO-SIDED CONFIDENCE INTERVALS FOR μ – KNOWN VARIANCE
data = [5, 8.5, 12, 15, 7, 9, 7.5, 6.5, 10.5];
N = length(data);
sigma = 2;
alpha = 0.05;
z = norminv(1-alpha/2); %1.96
x_bar = mean(data);
confidence_interval3 = [x_bar - z*sigma/sqrt(N) , x_bar + z*sigma/sqrt(N)];

% 95% ONE-SIDED LOWER CONFIDENCE INTERVALS FOR μ – UNKNOWN VARIANCE
data = [5, 8.5, 12, 15, 7, 9, 7.5, 6.5, 10.5];
N = length(data);
sigma = 2;
alpha = 0.05;
z = norminv(1-alpha);
x_bar = mean(data);
LowerLimit = x_bar + z*sigma/sqrt(N)

% TWO-SIDED CONFIDENCE INTERVALS FOR μ – KNOWN VARIANCE
data = [5, 8.5, 12, 15, 7, 9, 7.5, 6.5, 10.5];
N = length(data);
sigma = 2;
alpha = 0.01;
z = norminv(1-alpha/2); %1.96
x_bar = mean(data);
ConfidenceInterval2 = [x_bar - z*sigma/sqrt(N) , x_bar + z*sigma/sqrt(N)];

% TWO-SIDED CONFIDENCE INTERVALS FOR μ – UNKNOWN VARIANCE
data = [5, 8.5, 12, 15, 7, 9, 7.5, 6.5, 10.5];
N = length(data);
alpha = 0.05;
t = tinv(1-alpha/2,N-1)
x_bar = mean(data);
sigma = std(data);
ConfidenceInterval = [x_bar - t*sigma/sqrt(N) , x_bar + t*sigma/sqrt(N)]
