%{
The central limit theorem can be used to approximate the distribution of the sample
mean:			
'/ ) ~̇ � 0,1 when the sample size � is large.
A. Create a n x ns matrix from an exponential distribution mu = 0.2, where n is
the sample size and ns the number of samples
B. Visualize the histogram of the random values for the first 6 realizations using the
command hist
C. Calculate the sample mean (�?)
D. Demonstrate that the distribution of the sample mean of exponentially distributed
i.i.d. r.v.s approximate to a normal distribution when � is large
§ Use the mle function to find the normal distribution that best fits the means
§ Visualize the distribution of sample means together with the fitted normal
distribution (use the histogram function with 50 bins)
histogram(…,numbins,
'Normalization','pdf')
hold on
x = min(means):0.001:max(means);
y = normpdf(…);
plot(…)

%}

mu = 0.2;
n = 1e3;
ns = 1e4;

sample = exprnd(mu, n, ns);
figure(1)
hist(sample(1:6, :)');

means = mean(sample);
phat = mle(means);

figure(2)
numbins = 50;
histogram(means, numbins, 'Normalization', 'pdf')
hold on
x = min(means):0.001:max(means);
y = normpdf(x, phat(1), phat(2));   %normpdf - normal probability density function
plot(x,y,'r','LineWidth', 2);