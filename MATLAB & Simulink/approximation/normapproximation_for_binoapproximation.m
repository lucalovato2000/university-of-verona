%{

The central limit theorem states that let �1, �! … �"* be a sequence of i.i.d. r.v.s each
having mean µ and variance σ2. For � large, the distribution of �1 + ⋯ + �" is
approximately normal with mean �� and variance ��2.
Suppose that � has the binomial distribution with � trials and success probability �.
When � is large, the distribution of � is approximately normal with mean � = ��
and sd � = ��(1 − �) (see Lecture 5 – Slide 10).


A. Demonstrate that a normal r.v. can be used as an approximation for a binomial r.v.
(let n1 = 5; n2 = 10; n3 = 30)
B. Create a figure with 6 subplots (2 rows x 3 columns): in first row visualize the PDFs,
in the second row the CDFs with the relative normal CDFs

(Note: the normal approximation will, in general, be quite good for values of �
satisfying ��(1 − �) ≥ 10).

%}

p = 0.8; 
n1 = 5;
n2 = 10;
n3 = 30;

mu1 = n1*p;
mu2 = n2*p;
mu3 = n3*p;

sigma1 = sqrt(n1*p*(1-p));
sigma2 = sqrt(n2*p*(1-p));
sigma3 = sqrt(n3*p*(1-p));

x1 = 0:n1;
pdf_bin1 = binopdf(x1,n1,p);
cdf_bin1 = binocdf(x1,n1,p);
pdf_nor1 = normpdf(x1,mu1,sigma1);
cdf_nor1 = normcdf(x1,mu1,sigma1);
x2 = 0:n2;
pdf_bin2 = binopdf(x2,n2,p);
cdf_bin2 = binocdf(x2,n2,p);
pdf_nor2 = normpdf(x2,mu2,sigma2);
cdf_nor2 = normcdf(x2,mu2,sigma2);
x3 = 0:n3;
pdf_bin3 = binopdf(x3,n3,p);
cdf_bin3 = binocdf(x3,n3,p);
pdf_nor3 = normpdf(x3,mu3,sigma3);
cdf_nor3 = normcdf(x3,mu3,sigma3);

figure(1)
subplot(2,3,1)
bar(x1,pdf_bin1,1,'g')
title(' n = 5, p = 0.8')
ylabel('PDF')
grid on
subplot(2,3,2)
bar(x2,pdf_bin2,1,'g')
title(' n = 10, p = 0.8')
grid on
subplot(2,3,3)
bar(x3,pdf_bin3,1,'g')
title(' n = 30, p = 0.8')
grid on
subplot(2,3,4)
hold on
stairs(x1,cdf_bin1,'g');
plot(x1,cdf_nor1,'k');
hold off
title(' n = 5, p = 0.8')
ylabel('CDF')
grid on
subplot(2,3,5)
hold on
stairs(x2,cdf_bin2,'g');
plot(x2,cdf_nor2,'k');
hold off
title(' n = 10, p = 0.8')
grid on
subplot(2,3,6)
hold on
stairs(x3,cdf_bin3,'g');
plot(x3,cdf_nor3,'k');
hold off
title(' n = 30, p = 0.8')
grid on
legend('binomial','normal');
