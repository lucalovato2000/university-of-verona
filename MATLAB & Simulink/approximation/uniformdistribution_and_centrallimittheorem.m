%{

Create a figure to exemplify the central limit theorem by averages of simulated uniform
random variables
§ The figure should show the histograms of 10000 simulaRons of averages of � =
1, 2, 3, 5, 10, and 100 independent uniform (0,1) random variables
(Hint: single uniform distribuRon (� = 1); hat distribuRon � = 2 ; bell-shaped
distribuRons close to normal (� = 100))
§ Create 6 subplots (3 rows x 2 columns) using the funcRon hist (let the number
of bins = 40)

%}

figure(1);
subplot(3,2,1)
hist(rand(1,10000), 40)
title('n=1')

subplot(3,2,2)
hist(mean(rand(2, 10000)), 40)
title('n=2')
subplot(3,2,3)
hist(mean(rand(3, 10000)), 40)
title('n=3')
subplot(3,2,4)
hist(mean(rand(5, 10000)), 40)
title('n=5')
subplot(3,2,5)
hist(mean(rand(10, 10000)), 40)
title('n=10')
subplot(3,2,6)
hist(mean(rand(100, 10000)), 40)
title('n=100')

