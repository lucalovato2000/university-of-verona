%{
Compare the PDFs and the CDFs of the following Uniform continuous r.v.s
!1 ~ > 0, 1
!2 ~ > −3, 2
!3 ~ > 2, 4
!4 ~ >(0.8, 2.5)
B. Create a new figure with two subplots (first row and first column: PDFs, and second
row and first column: CDFs)
C. Determine the mean and variance

%}

x=-10:0.01:10;
y1=unifpdf(x,0,1);
y2=unifpdf(x,-3,2);
y3=unifpdf(x,2,4);
y4=unifpdf(x,0.8,2.5);

subplot(2,1,1),
plot(x,[y1' y2' y3' y4'])
title('uniform distributions - pdf')
legend('a=0, b=1', 'a=-3, b=2', 'a=2, b=4', 'a=0.8, b=2.5')
axis([-4 8 0 1.1])

subplot(2,1,2)
yc1=unifcdf(x,0,1);
yc2=unifcdf(x,-3,2);
yc3=unifcdf(x,2,4);
yc4=unifcdf(x,0.8,2.5);
plot(x,[yc1' yc2' yc3' yc4'])
legend('a=0, b=1','a=-3, b=2','a=2, b=4','a=0.8, b=2.5'),
axis([-4 8 0 1.1])
title('uniform distributions - cdf')

%%media e varianza
[y1m y1v] = unifstat(0,1);
[y2m y2v] = unifstat(-3,2);
[y3m y3v] = unifstat(2,4);
[y4m y4v] = unifstat(0.8,2.5);