x = linspace(-3,3);
p1 = tpdf(x,1);
p2 = tpdf(x,2);
p5 = tpdf(x,5);
p10 = tpdf(x,10);
pnorm = normpdf(x,0,1);
figure (1)
plot(x,p1,x,p2,x,p5,x,p10,x,pnorm)
title('Student-t distribution');
axis([-3 3 0 0.5])
legend('\nu=1','\nu=2','\nu=5','\nu=10','normal','location','northwest')