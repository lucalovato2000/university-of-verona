%{
Explore the normal distribution L(mu, sigma)
mu = 100; % the mean
sigma = 15; % the standard deviation
xmin = 70; % minimum x value for pdf and cdf plot
xmax = 130; % maximum x value for pdf and cdf plot
n = 100; % number of points on pdf and cdf plot
k = 10000; % number of random draws (for histogram)
A. Create a set of values ranging from xmin to xmax
B. Draw k random numbers from a L(mu, sigma) distribution
C. Create a new figure with three subplots (PDF, CDF, histogram of random values with
bins=20)
%}

% create a set of values ranging from xmin to xmax
x = linspace(xmin, xmax, n);
p = normpdf(x, mu, sigma); % calculate the pdf
c = normcdf(x, mu, sigma); % calculate the cdf
% draw k random numbers from a N(mu,sigma) distribution
y = normrnd(mu, sigma, k, 1);
22
SOLUTIONS
figure(1);
subplot(1,3,1);
plot(x, p, 'r-' );
xlabel('x'); ylabel('pdf');
title('PDF');
subplot(1,3,2);
plot(x, c, 'b-');
xlabel('x'); ylabel('cdf');
title('CDF');
subplot(1,3,3);
hist(y, 20 );
xlabel('x'); ylabel('frequency’); title('Histogram of random values');