% Plot the (absolute frequency) histogram of the data
y_raw = [64*ones(1,1) 66*ones(1,2) 66.5*ones(1,4)
67*ones(1,5) 67.5*ones(1,4) 68*ones(1,8) 68.5*ones(1,11)
69*ones(1,12) 69.5*ones(1,10) 70*ones(1,9) 70.5*ones(1,8)
71*ones(1,7) 71.5*ones(1,5) 72*ones(1,4) 72.5*ones(1,4)
73*ones(1,3) 73.5*ones(1,1) 74*ones(1,1) 75*ones(1,1)];
binwidth = 0.5;
bins = 64:binwidth:75;

% The 23 possible outcomes
figure(1); hist(y_raw,bins); axis([64 75 0 13])
ylabel('Absolute Frequency');
xlabel('Heights of 100 men (in.)');
title('Absolute Frequency Histogram');

% Relative frequency histogram using the bar function.
tests = 100;
y_rel=[1,0,0,0,2,4,5,4,8,11,12,10,9,8,7,5,4,4,3,1,1,0,1]/tests;
% Plot the (relative frequency) histogram of the data
figure(2); bar(bins,y_rel)
ylabel('Relative Frequency');
xlabel('Heights of 100 men (in.)');
title('Relative Frequency Histogram for 100 Tests');

% Compute scaled frequency data
% Absolute frequency data
y_abs=[1,0,0,0,2,4,5,4,8,11,12,10,9,8,7,5,4,4,3,1,1,0,1];
area = binwidth*sum(y_abs);
y_scaled = y_abs/area;
% Plot the scaled histogram
figure (3); bar(bins,y_scaled)
ylabel('Scaled Frequency')
xlabel('Height (in.)')

prob = cumsum(y_scaled)*binwidth;
prob67_69 = prob(11)-prob(6);
The result is prob67_69 = 0.4000

% Fill the vector y_raw with the raw data.
% Start with an empty vector.
y_raw2 = [];
for i = 1:length(y_abs)
    if y_abs(i)>0
new = bins(i)*ones(1,y_abs(i));
else
new = [];
    end
y_raw2 = [y_raw2,new];
end
% Compute the mean and standard deviation.
mu = mean(y_raw2);
sigma = std(y_raw2);

% How many are no taller than 68 inches?
b1 = 68;
P1 = (1+erf((b1-mu)/(sigma*sqrt(2))))/2;
% How many are within 3 inches of the mean?
a2 = mu-3;
b2 = mu+3;
P2 = (erf((b2-mu)/(sigma*sqrt(2)))-erf((a2-mu)/(sigma*sqrt(2))))/2;