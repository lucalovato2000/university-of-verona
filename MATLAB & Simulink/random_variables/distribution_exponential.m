x = linspace(0,4);
mu1 = 1;
mu2 = 0.5;
mu3 = 0.1;
p1 = exppdf(x,mu1);
p2 = exppdf(x,mu2);
p3 = exppdf(x,mu3);
figure (1)
plot(x,p1,x,p2,x,p3)
axis([0 4 0 2])
ylabel('pdf(x)');
title('Exponential pdf');
legend('\mu=1','\mu=0.5','\mu=0.1')

cdf1 = expcdf(x,mu1);
cdf2 = expcdf(x,mu2);
cdf3 = expcdf(x,mu3);
figure (2)
plot(x,cdf1,x,cdf2,x,cdf3)
axis([0 4 0 1.1])
ylabel('cdf(x)');
title('Exponential cdf');
legend('\mu=1','\mu=0.5','\mu=0.1')
