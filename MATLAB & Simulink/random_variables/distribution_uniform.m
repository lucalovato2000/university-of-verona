x= linspace(-0.5, 1.5);
y= unifpdf(x, 0, 1);
cp=unifcdf(x,0,1);

figure(1);
plot(x,y,'b',x,cp,'k')
title('distribuzione uniforme');
axis([-0.5, 1.5,-0.1,-1.2])

legend('pdf', 'cdf', 'location', 'northwest'); %%dove posizionare la legenda a piacere

