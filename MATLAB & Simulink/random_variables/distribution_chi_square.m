%si puo' fare fino ad n=4,n=5 ecc

x=linspace(0,12);
pdf1=chi2pdf(x,1);
pdf2=chi2pdf(x,2);
pdf3=chi2pdf(x,3);

figure(1)
plot(x, pdf1, x, pdf2, x, pdf3);
legend('n=1','n=2','n=3');

cdf1=chi2cdf(x,1);
cdf2=chi2cdf(x,2);
cdf3=chi2cdf(x,3);

figure(2)
plot(x, cdf1, x, cdf2, x, cdf3);
legend('n=1', 'n=2', 'n=3');
