%{
A. Determine and plot the values of the PDF for a normal distribution with mean 3 and sd 2
for # values between −1 and 7
B. Determine and plot the corresponding values of the CDF
C. Determine for what values of # the value of the CDF equals 0.025 and 0.975 
%}

x = linspace(-1,7);
p = normpdf(x,3,2);
plot(x,p)

cp = normcdf(x,3,2);
plot(cp)

norminv([0.025 ,0.975], 3 ,2)