%{
A. Plot the histogram of the data using the absolute frequencies
(hint: binwidth = 0.5; bins = 64:binwidth:75;)
B. Plot the histogram of the data using the relative frequencies
C. Produce the scaled histogram
(hint: The scaled frequency histogram is the absolute frequency histogram divided by
the total area of that histogram: y_scaled = y_abs / area;
D. Compute the probability of a height lying between 67 and 69 in
(hint: you can use the cumsum function to calculate areas under the scaled
frequency histogram and therefore to calculate probabilities)
E. Estimate the mean and sd for the height data given in Table, reconstructing the
original (raw) height data from the absolute frequency data (note: these data have
some zero entries: for example, none of the 100 men had a height of 65 in.).
§ To reconstruct the raw data, start with an empty vector y_raw and fill it with the height
data obtained from the absolute frequencies
§ The for loop checks to see whether the absolute frequency for a particular bin is nonzero
§ If it is nonzero, append the appropriate number of data values to the vector y_raw
§ If the particular bin frequency is 0, y_raw is left unchanged
F. How many 20-year-old men are no taller than 68 in?
G. How many are within 3 in. of the mean?
(results: P1 = 0.2137 and P2 = 0.8711)
%}