n=10;
numsamples= 10e6;
samples= randn(n,1);

many_chisquare= sum(randn(n, numsamples).^2);

figure(1)
hist(many_chisquare, 1000)
