%{
Random generation

rand()  [0,1]
randn() standard
 
normrnd()   distribuzione normale [sigma o']
unifrnd()   distribuzione discreta uniforme [A B]
exprnd()    distribuzione esponenziale [lambda]

binornd()   distribuzione binomiale
hygernd()   hypergeometrica
poissrnd()  poissov

%}

%{
randtool - generatore interattivo di numeri random
%}