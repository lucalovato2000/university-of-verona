%{
X binomial r.v. n=18, p=1/3
%}

%P(X=3)
p1 = binopdf(3,18,1/3); % p1 = 0.0690
%P(X>=3)
p2 = 1- binocdf(2,18,1/3); % p2 = 0.9674
%P(1<=X<5)
p3 = binocdf(4,18,1/3) - binocdf(0,18,1/3); % p3 = 0.2304
%P(X>=15)
p4 = 1- binocdf(14,18,1/3); % p4 = 1.8525e-05


%{
Let X be a Poisson r.v. with parameter lambda = 3
%}

%P(X=3)
p1 = poisspdf(3,3); % p1 = 0.2240
%P(X>=3)
p2 = 1- poisscdf(2,3); % p2 = 0.5768
%P(1<=X<5)
p3 = poisscdf(4,3) - poisscdf(0,3); % p3 = 0.7655
%P(X>=15)
p4 = 1- poisscdf(14,3); % p4 = 6.7039e-07
