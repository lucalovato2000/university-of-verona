%{

Demonstrate that a Poisson r.v. may be used as an approximation for a binomial r.v. 
(let lambda= 5; x = 0:20)

A. Let X ∼ Binomial(20,1/4); X ∼ Binomial(30,1/6); X ∼ Binomial(40,1/8); X ∼ Binomial(100,1/20);
B. Compare your answers part (A) with its Poisson approximation.
C. Compare MATLAB plots of the probability mass functions in part (A) and (B).

[Of note that when n is large, binomial distribution becomes difficult to compute directly because of the
need to calculate factorial terms. In such case, a Poisson r.v. with parameter lambda = np can approximate a
binomial distribution with parameters (n, p) when n is large and p is small]

%}

x = 0:20;
z = poisspdf(x,5);

figure (1)
hold on

plot(x,z,'k')
y1 = binopdf(x,20,1/4);

plot (x, y1,'b')
y2 = binopdf(x,30,1/6);

plot(x,y2,'y')
y3 = binopdf(x,40,1/8);

plot (x,y3,'g')
y4 = binopdf(x,100,1/20);

plot(x,y4,'r')
hold off