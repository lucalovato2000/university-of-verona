%{

Generate N = 1000 random numbers from a binomial distribution with n =
9 trials and p = 0.8. Thus each of the 1000 random numbers will be an integer
between 0 and 9.

Plot the probability mass function using the bar function.

%}

N = 1000;
data = binornd(9,0.8,N,1); % generate the random
[freq,centers] = hist(data,unique(data))
figure(1)
bar(centers,freq/sum(freq))
xlabel('Value');
ylabel('Experimental probability')
title('Binomial distribution with n = 9, p = 0.8')
