%{
Poisson distribution
%}

x= -1:10;
lambda= 2.5;
p= poisspdf(x, lambda);     %PDF
cp = poisscdf(x, lambda);   %CDF

figure(1)
stem(x, p, 'b');         %'b' = blue

hold on
stairs(x,cp,'g');

title('Poissov distribution, \lambda=2.5')
legend('pdf', 'cdf');   %legenda 

hold off