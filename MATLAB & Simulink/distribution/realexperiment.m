%{

Suppose there is a 20% chance that an adult American suffers from a psychiatric
disorder.
We randomly sample 25 adult Americans. If we let � represent the number of
people who have a psychiatric disorder, then � is a binomial r.v. with parameters
(25, 0.20).

We are interested in the probability that at most 3 of the selected people have such
a disorder.
A. Use the function binocdf to determine �(� ≤ 3).
B. Get the values of the probability mass function for n = 6, p = 0.3 and then
n = 6, p = 0.7 and plot the results in two subplot using the bar function. 

%}


prob = binocdf(3, 25, 0.2); %prob2 = sum(binopdf(0:3,25,0.2));
% Values for the domain, x.
x = 0:6;
% Values of the probability mass function
pdf1 = binopdf(x, 6, 0.3); % n = 6, p = 0.3:
pdf2 = binopdf(x, 6, 0.7); % n = 6, p = 0.7:
subplot(1,2,1)
bar(x,pdf1,1,'g')
title(' n = 6, p = 0.3')
xlabel('X'),ylabel('f(X)')
axis square
subplot(1,2,2)
bar(x,pdf2,1,'r')
title(' n = 6, p = 0.7')
xlabel('X'),y
