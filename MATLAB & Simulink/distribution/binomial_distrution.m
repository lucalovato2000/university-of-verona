%{
Binomial distribution
%}

x= 0:10;
y= binopdf(x, 10, 0.5);     %PMF
cp= binocdf(x, 10, 0.5);    %CDF

figure(1);
stem(x,y,'b');  %PMF  %vettore x ed uscita y
%bar(x,y) e' possibile utilizzare il comando bar 

hold on  %resta sempre su figure(1)
stairs(x,cp,'k') %CDF  %vettore x ed uscita cp

title('binomial distribution, n= 10, p= 0.5')
legend('pdf','cdf', 'location', 'northwest');
hold off


%cambia solamente il paramentro P della probabilita'
x= 0:10;
y= binopdf(x, 10, 0.8);
cp= binocdf(x, 10, 0.8);


figure(2);
stem(x,y,'b');
hold on
stairs(x,cp,'k')

title('binomial distribution, n= 10, p= 0.8')
legend('pdf','cdf', 'location', 'northwest');
hold off

%{
Nel grafico utilizzando bar() per la PMF possiamo notare come la colonna 
piu alta risulti il valore di P. 

es. (x,10,0.6) -> 6 colonna piu' alta
%}